RL469 - Culmination
2022-08-08

This week, Merlin and John talk about:

**The Problem:** There was a very dirty guy, referring to someone John met when he drove up the mountain in Washington to mine some gems.

The show title refers to a word the military people used a lot at the Army War College when they were referring to a project realising all of its components and coming to a conclusion.

+ New construction work starting soon outside of Merlin’s office and the related project management (RL469)

It is a splendid morning, but Merlin wishes the air would move more. It is muggy, but it is hot everywhere and you shouldn’t complain, but it matters because it is me. Merlin runs fans because he is a big believer in ventilation, but you can’t do that when you are making audio for the Internet, although [[[what is in the show is in the show]]]. Merlin has embraced that existentially, but also in a very realistic hands-on sense there are new road work things outside of Merlin’s office.

Merlin had a nice visit with a PG&E guy this morning as he was out there with his staple gun, stapling up new signs, and they had a little visit about that and Merlin commiserated a bit about the difficulties of project management, and he quoted Marco Arment with one of the greatest quotes from the last 5 years: ”It is not my fault, but it is my problem”, which is really what parenthood or project management is all about.

During Merlin’s short pseudo-career as a project manager he got giddy with what he learned, but he never got great at it because he lacked the people skills for that. There are immutable things about project management that a lot of the population overlook or are not fully aware of. It is boring because life is boring. For example, something involving cooking: You can’t have a Campfire Spaghetti Party //(see RL69)// in your kitchen and you can’t serve the food until it is all plated, and it can’t be plated until the food is cooked, you can’t put the sauce on the pasta until the pasta is done. It takes 9 months to make a baby, no matter how many women you put on the job, and if you forgot to buy spaghetti, it is going to push your date out.

John spent some time with some military people recently and they used the word ”culmination” all the time. It wasn’t just the ending of things, but they want things to culminate, to come to an ending, and to have all of the components realized, a culmination of all the factors to arrive at the same place at the same time. Merlin’s road work involves replacing street car tracks, and his friend Chris Coltrane who was his sherpa for getting a VW Bus when they needed to change the gaskets on the engine and pull out the engine he suggested that at the same time they should do those three other things because you don’t want to pull out the engine again for something you should have caught before.

Things don’t just happen, they have to be caused to happen, and if you have to be there for the culmination of something, everybody who is wanting your time or attention is not collaborating on how busy you are this week, but you have to be the one who steps up and coordinate all of that, because it is very unlikely that happenstance will cause it to turn out the way you want in the time you want at the budget you want. Merlin loves that proactive idea of culmination.

When John’s baby was smal, the whole business of swaddling it, the pediatrician always talked about it as ”collecting”, as in: ”You wan to get the baby collected!”, but you also want to collect yourself and you want to collect the situation. The baby does want to be collected, it wants its arms pulled in, and as John would collect the baby he would also feel like they are all getting collected and the collection is happening all around because the collection reverberates out. You need to get collection before you culminate. Merlin also likes the idea of making people slow their roll and talk about culmination because all the corporate language gets so flabby unless they are imbued and inspired with the right amount of verb inside of that noun.

Words can be good, but as the Thompson Twins said: words are stupid things //(from the song Lies)//. Merlin’s girlfriend once passed out at a Thompson Twins concert, but she culminated on her own. Somebody had decided to pierce somebody else’s ear with a pin, back when they were the Punk Rock kids in Tampa who went to shows.

+ John’s daughter looking forward to getting her ears pierced at the age of 12, going to a piercing and tattoo parlor (RL469)

John’s daughter was told when she was very young that she was going to get her ears pierced not before her 12th birthday. A lot of her friends of the Catholic faith, like her little Mexican best friend, had their ears pierced when they were born, and she has always been curious about it, she is very fancy, she wants to, and clip-on earrings are very uncomfortable. Already when she was 7 years old she started talking about that the ear-piercing day was coming up, but it was still 5 years away, but now she is 6 months out and they talk about it a lot now: ”When I get my ears pierced I am going to… X”.

A lot of her friends recently got their ears pierced, and they concluded that if you are getting it done at the mall you are almost certainly going to get an infection. Everybody they know who got an infection got it at the mall, which technically doesn’t mean that everybody who did it at the mall got an infection. In the Northwest you will typically take your daughter to the Piercing and Tattoo place, they got their shit together, they got autoclaves, that is all they do, and they started talking about going to the kick-ass Tattoo Parlor Piercing Spot and now she is really thinking about that a lot.

She got a furrowed brow and she is stroking her chin about it because at the mall you are surrounded by pink plastic lala because those mall stores are full of that, and at the piercing place all senses are on and everything happens. It feels adult. John is going to call ahead, telling them that he is about to bring his 11-year old in, so that they preferrably don’t do any genital piercings at the next table while she is getting her ears done. Hopefully they are going to get a private booth, but John does not have any experience with it because he has never had a tattoo.

Early on in John’s life at 16/17 he had a friend who got a tattoo of a skull in a top hat smoking a joint on the front of his arm //(see RW35, RL213)//, which was like the Top Tobacco logo, except turned into a skull, and he was not a Hippie, but he was a Punker. He and John were talking about it because he was actually too young, but he had found a way to get one anyway, and part of the reason was to communicate that he owns his body, he could do with it whatever he wanted, like asserting ownership over himself. At lot of 16-year old Punk Rock kids are the smartest kids in the school, but then they all go…

This friend James, who is still alive, although a lot of the other people John knew at that time are not still alive, and he now works in dirt and is a custom dirt person. When James talked about ownership of his body, John thought about it and he came to the conclusion that he is leasing his body and he is not ready to put any permanent markings on it because there might be a surcharge later. He never wanted to do any customization or big mods because he wasn’t sure who was going to come later and tell him that his body was just a loaner. John still feels that way today.

John has thrashed his body accidentally, he has gone through so many bushes, the paint is all scratched up on the sides, he has hit his rearview mirror a couple of times, the rims aren’t original and he couldn’t find the old ones, and when he will turn his body back in he will be in trouble anyway and he could just as well get knuckle tattoos that say ”Piece” and ”Love”, or ”Fuck” and ”You” or ”Jake” and ”Elwood”. The whole tattoo thing really passed Merlin by and he also never got one.

+ Fountains of Wayne, Adam Schlesinger (RL469)

Merlin says he is going to do that thing that makes people roll their eyes. That Thing You Do!, which was written by the guy from Fountains of Wayne //(Adam Schlesinger, performed by the fictional band The Wonders)//, he also wrote some of the very good songs of Josie and the Pussycats //(e.g. the song Pretend to be Nice)//, and for the very good Rachel Bloom show Crazy Ex-Girlfriend. Merlin watched [https://youtu.be/jrsUeuBPFhM a really good YouTube video] how one of the things that broke up the band was that he was getting all this work writing ”fake” hit songs for movies.

He is the one who died from COVID, which sucks. It was Merlin’s first COVID deaths in early April of 2020, and Merlin was very emotionally vulnerable in the spring of 2020. They are not his //favorite// band, but he adores Welcome Interstate Managers, and the Red Dragon Tattoo one. Sometimes things just really hit you, and Adam’s death really hit Merlin, not only because they were roughly the same age, but it was just such a fucking bullshit. He started a thing where on his way to work every Monday he started listening to Mexican Wine, his favorite Fountains of Wayne song, and sometimes he cries a little bit.

John is pretty Fountains of Wayne adjacent because their drummer Brian Young played on the first Long Winters record, but also Chris Collingwood was on the first JoCo Cruise and John became very good friends with him, he liked him a lot, and they collaborated, he is a sweet guy.

+ John explaining the labor movement to his daughter, she explaining her take on her friends’ style (RL469)

The other day while they were driving John’s daughter asked very proudly: ”Would you like to know my assessment of all of my friends’ style?” - ”I would like to hear it!” and she started at the top: ”Talia is very sophisticated and she wears the following colors, but there is some aspect to it that is mainstream fashion”, and she moved on to the next person, like: ”Their style is very sporty…” It came out of nowhere. As they both drive in the car there are times when they are in conversation, but other times someone takes the floor.

This morning when they were driving to the School of Rock and they passed a shipping yard where the gate was open with a big sign on the inside directed at truck drivers coming out of the yard: ”At some point when you are trying to egress (leave) this yard in your truck you might be greeted by a picket line. Activists are entitled to block you for 2 minutes, but no more, and you are not entitled to gun your motor or agressively lurch at them!” It was a big 5x5 sign made by a professional sign company. Usually she will ask for a word or two, but this time she asked what all of that meant. It was all using euphemistic language!

The text was 3 sentences long, and why did they use ”egress” and then ”(leave)” instead of just using ”leave”? Somebody must have crafted the wordage of that sign and nobody edited them, they might have had a legal document in front of them, it might have come from higher management. During the same drive they had already been talking about trucking, and John talks about trucking enough that she understands that trucks are a very important part of how the world works, and we are greatful for trucks, but trucks are also a pain in the ass, they are exploring that dichotomy all the time.

John used this as an opportunity to explain the labor movement to her, starting all the way back in the 19th century, back to Marx and the power of the worker, and then forward to the labor movement of the mid-20th century, what a picket line or a scab is, why we traditionally support organized labor, what happened to organized labor in the 1980s/90s, when Reagan fired all the air trafic controllers. John’s dad was a union organizer.

She is used to John talking about things like that and she took the floor and described 9 of her friends in pretty well thought-out detail as far as: ”Here is their style, here is where I think it comes from, and here are what I consider their blindspots in terms of their relationships to their own style!” She said that two of the girls are wearing visirs as part of their personal style, where the shade is colored clear plastic, one girl with a green one and one with an orange one, but she did not accuse either girl of ripping off the other girl’s style.

+ John’s daughter wanting to get her ears pierced (cont)

Her interest in getting her ears pierced is entirely for having another place of expression and she said very proudly that all her friends would agree that she has the most style. She does say things like: ”In this case everybody had an idea, but my idea was the best idea!” - ”I am sure it was, darling, but I would like to hear more about how you didn’t say that out loud!” All of this was happening because on Saturday…

+ John and his daughter going up the mountain to dig for gems (RL469)

Up in the mountains in Washington there are gem fields where the gems are there in the ground to be gathered. There are places like that all over the US where you pay $5 and you can go in and pick gems up off the ground //(see OM390)//, but in this case you just drive up to the pass, go up a logging road 2 miles under the old railroad trestle that is now falling down, up past the 4 guys that are standing at the side of the road, chewing tobacco, pull over, go past the boulders, hike up a trail a kilometer, and then turn and go straight up a forested slope until you start to see holes that have been dug in the ground by other crazy people, and you can just start digging a hole and pull out gems.

As John pulled up there were some trucks on the side of the road, and in front of one of them with a bunch of pick-axes in the back there was a very dirty guy who was probably 55 but looked 65, and John rolled down the window and asked him if he knew anything about mining some gems. Eventually he got him talking and he pointed John in some direction. He also said that there is a fellow who has a claim and he will chase you off his claim. 

They pulled over, got their stuff out, and went up the trail. John had a shovel over his shoulder, she had some garden tools, they got some buckets, and as they were walking along, every 10 minutes they came across yet another small group of people carrying sifters and pans, and of course John stopped and asked them: ”How did you do? How is the pickens?” because he worked on a Gold Mine when he was young //(in Circle Hot Springs, see RW119, RW185, OM262)// and spent a lot of time up in Alaska dealing with miners. A lot of people up there are making a living pulling gold out of the streams.

John knows a little bit how miners talk and they all play it down even when their pockets are bulging with gold. One time John and his dad were flying and they found a runway carved out of the trees up in the Yukon territory, and maybe they had to pee or some other reason, but John’s dad decided to land, there was no-one around, and they brought it in. John’s dad loved to land on a sand bar, they were taxiing toward the end of the rundway to the forest, and a guy stepped out of the forest, cradling a shotgun //(see RL106)//, so John’s dad flipped the tail of the plane around and off they went. It was clearly somebody mining a claim up there who didn’t want them coming around, peeing.

At a certain point John’s daughter asked if he really had to talk to everybody, which he obviously had to because he was looking to get some tips & tricks. This is a public gem searching area, there was no-one there protecting their claim, but they are still gem hunters and they will naturally play it down and tell him that they didn’t get much. As they got there and climbed up the hill they started digging a hole and started sifting through stuff, they found a little patch in the trees where the sun was shining, so if you sift the dirt and you find something it would glisten. They got a little can full of gems, including one Amethyst, not some high-grade one that makes any money, but that is not why they were doing it. She is a fancy lass who has a little collection of precious rocks that extends to sea shells and stuff she picks up on the beach, she likes gems and jewelry.

+ John and Merlin not wearing many adornments (RL469)

Merlin is born in November, which means his birth stone is Topaz, and apparently also pearls. John is born in September and his stone is Saphire, that whole things goes back to the 15th century. Merlin doesn’t wear any kind of adornment. He got his Apple Watch and his father’s wedding ring, which is now his wedding ring. When he got baptised he got a cross of our risen lord, he never even had puka shells, he had an ill-advised earring when he was 19, but otherwise he is not an adornment guy.

John tried many times in the 1970s, he wore a puka shell necklace, in the 1980s he wore friendship bracelets for a while, he was wearing a puka shell necklace in the Western State Hurricanes local access show because his sister in the 1990s got him some puka shells when she was living in Hawaii, and he wore them both out of respect for her and ironically, but on top of that he wore them because it gave him pleasure, and he got so much shit for it! But like with all the glasses frames he wore in the 2000s he looks back at it now and realizes that it was just dumb looking.

Merlin’s bête noire at a certain point in the late 1970s / early 1980s was that he wanted parted-in-the-middle feathered hair like Andy Gibb and he did not have the culture, the technology, or the tools to even begin to know how to do it. He was dealing with a home haircut and didn’t even know what cut to get together with that. He got a soup bowl haircut like the guy from The Stooges and he was trying to rock a Gibb, he was: ”No soup!” Every time he has tried adornment he feels like it looks like he is a guy who is trying an adornment.

One time John was on a train in Italy and talked to a guy from Firenze in his train car who had three rings that were intertwined, he called it a rolling ring //(actually Cartier Trinity Ring, see RL198)//. When you put it on or take it off they roll over each other like a little device. John watched him roll the rings on and off and thought he had to get one of those. When he was in Firenze (Florence) he went to the bridge over the Arno where they do the jewelry and asked about it and they told him that in gold it costed some insane number, which he had totally forgotten about that things cost money.

But later John was on a street fair where people from Greece put blankets out in the middle of the town square and sell you all kinds of leather pouches and whatnot, and one of them was a silversmith who had a silver rolling ring that fit John perfectly and he could haggle them down to $10 and wore that silver ring for a long time until one day it slipped off into a box and right now he is wearing nothing at all, he is in his birthday suit!

Merlin is mostly wearing podcast T-shirts, and he thinks John is much more into the culture of Uniform of the Day and knowing what to wear, trying on an idea like a jacket, and trying things on in general being a big part of John’s whole deal. Merlin would try something on in the hope of looking like the person in Tiger Beat Magazine, while John seems much more likely to try something on just for the purpose of trying something on, and if it doesn’t work it is not necessarily a failure, but it is part of the process. Merlin is much more into the music, TV, and movies part of culture, and he doesn’t adorn himself very much.

John has always been curious about the fact that men’s jewelry is used to display wealth. ”It is a rolex, David!” //(referring to John’s dad’s best friend Jack Tanner, see RW58)//, like a Sopranos thing. Brad Pitt always got something expensive on, even when it is subtle. In Hip Hop you always got something made out of a precious metal that is so big that you can feel the weight of it, it is significant. John doesn’t have that. Keith Richards always looks like a real mess, like he is his own department in a Goodwill where it is all under glass, but it is the craziest assortment of jewelry.

Merlin brings up the Italian Horn from the late 1970s, John Siracusa probably used to have one of these, it was a necklace that the Italian guys would have, and Merlin thinks it is a sex thing, it is a 15-years later version of Austin Powers’ male symbol.

Keith Richards’ look (and mannerisms) were inspiration for Johnny Depp’s Jack Sparrow in Pirates of the Carribean. There was a time in the 1980s when John had enough friendship bracelets made by friends, and then he had a girlfriend who gave him a black crystal on a pendant and he wore that for a while. He got some leather friendship bracelets in Spain and one of them stayed on for years, he even forgot who gave it to him. It was the type of thing Daniel Lorca //(from Nada Surf)// would have given him in 1989 if he had known him back then. ”Could you roll down a window? It is really smoky in here!”


















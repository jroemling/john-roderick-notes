OM461 - Potsdamer Platz (Entry 972.NE0406)
2022-05-10

This week, Ken and John talk about:

+ Things aging more or less well, the distance between events in the past (OM461)

During the Fulda Gap episode they scoffed at a big tank war in Europe, which hasn’t aged very well now after the Russian invasion of Ukraine. 2019 was a simpler time when the president of the United States still loved Russia, and it didn’t seem like that could ever happen again.

The Berlin Wall has now been down longer than it was up, and it seemed like it was up forever. In the 1980s the division of Europe was to John as old as unleavened bread, but to his father it was just something that was true in his 40s and 50s. It is probably the distance between now and the first Justin Bieber record. John was watching Pirates of the Caribbean: The Curse of the Black Pearl last night for the first time, his daughter has never seen a Pirates of the Caribbean movie, and he realized the movie is 20 years old. Ken loves to watch movies from another generation!

Francis Fukuyama was talking about The End of History and the Last Man after the end of the Cold War, and that part turned out to be hillariously wrong, but culturally? In 1978 when John was 10 years old, a movie that was made in 1958 couldn’t have been more archaic, but his daughter watching Pirates of the Caribbean probably sees it as a contemporary movie. Hairstyles haven’t changed that much since 1990 or surely not since 2003, clothes and style hasn’t changed. You don’t look at the fashion from 2003 and go: ”Oh my God, look at that!” It is all Normcore.

At some point in 1990 we had done all the fashion and they all coexist simultaneously. In the 1980s fashion and so many things went full chock. When Madonne was showing her belly button in 1981 everybody was freaking out. The fashion of the 1970s was the fashion of 1974, whereas by 1994 you could wear everything and it would be either archaic or ahead of your time. The guys in Hippie Big Buckle in 1990 were wearing those clothes and it was cool and Punk Rock somehow.

Ken feels it refreshing that his kids don’t have any stigma towards old music or old media in any way, but the generation gap doesn’t form in either direction as long as old people are willing to try to keep up with the music of the young people, which is increasingly easy to do because no matter what you hear it sounds like bands from the past and it smooths out all the generational rifts, and now the rifts are all caused by questions like which generations put the most hydrocarbons in the atmosphere or which generation repealed Roe v. Wade. Generation X is absolved of all responsibility.

+ John’s uncle Jack knowing George H.W. Bush (OM461)

The Supreme Court is all 38 year old federalist society tweebs now. There are now two justices younger than John. The first president younger than John’s dad was George H.W. Bush, he was John’s Uncle Jack’s age and they went to Yale together. Uncle Jack got tapped for Skull & Bones and rejected them because he was a West Coast anti-snob who thought he was supperior because he was from the hearty West and held an axe. Ken does make fun of some of his Ivy League friends, which is part of being from Seattle, it is reverse snobbery, except not based on class, but on inferiority complex.

One time Uncle Jack was on an airplane with Bush during the time Bush was in Congress, not heading to CIA yet, and Bush wanted Jack to sit with him, but Jack told him he had a friend in the back he needed to talk to. This was when there would be a lounge on the plane with somebody playing the piano. They were both involved in the oil business and later on there was some situation where Bush was in Texas and Jack was in Alaska and there would have been some oil handshake, but Jack thought Bush was mad because he had snubbed him on that American Airlines flight in 1966.

+ How Berlin was not always a political and cultural center (OM461)

Ken was reading a novel the other day from the early 1980s and there was a plot point that Berlin would be a place where you would go to hide out. It is not on the tourist track, and if you were afraid of a Getty-style kidnapping you would head to Berlin, which 40 years earlier and 40 years later is one of the world’s great cities, but it was briefly a weird asterisk nomansland. The Federal Republic of Germany had moved their capital to Bonn, so it was no longer a political center, and it was an era even in the United States where major cities were no longer where things were happening, like Detroit or Philadelphia.

John went to Berlin in the 1980s and it felt like the center of U2 videos, but it didn’t feel like the center of the universe. It was a super-weird vibe. But if you are watching roaring 1920s videos from Berlin, in 1924 it was the 3rd biggest city on Earth just behind New York and London. It was a cultural center and an arts center, and people were doing very sexy things there.

+* Various

Ken had not been in Berlin until a couple of years ago and he stayed at Potsdamer Platz. It is one of the most interesting 150 acres in Europe and had quite the adventure, especially in the last century.

The last time John drove from Slovenia to Austria he got stopped at the border because his papers weren’t in order. It was clearly a case where for 50 EUR they could make this problem go away. Ken had that between Guatamala and Belize and he just happily paid it because he wanted to see the ruins. John didn’t get that it was a shakedown and he offered to just sit and wait until they would get this problem resolved, and they pulled him over and parked him there and they were talking to each other in the distance, pointing at John, while he was sitting there with his arms folded in his car, not being clever. Eventually they got bored and a guy came over and disgustedly told John to just go ahead.

For Ken it was a 9-year old kid doing it and you know that you don’t need to make a photocopy of your passport and there is absolutely no 200 Centavo fee for this, but it was a 9-year old kid and it was probably $8.

John’s phone made a noise like a sonar on a submarine, which means that Mike Squires is calling. Sunny Day Real Estate is getting back together and they need a bass player because Nate Mendel is not going to leave Foo Fighters to play with them, and Mike Squires wants to audition for them to play the bass, but he doesn’t know anybody in the band and he had asked John yesterday to put his bass audition tape into somebody in Sunny Day Real Estate’s hands, preferrably Jeremy Enigk, and John is now avoiding his phone calls because he doesn’t want to deal with this //(when they played for the first time in September of 2022 their bass player was Chris Jordan)//.

This show was presented by Patreon-donor Jochen, a German who has decamped to Scandinavia. He requested anything about Germany or Sweden and Ken was sorely tempted to wait until somebody else requested any show having to do with Germany or Sweden and double up on it. Jochen is not a Berliner, he is from the South there, somewhere around Weimar //(actually he is from Schweinfurt)//. He gets talked about a lot by John and he is surely bored, so let’s get all the biographically details wrong. What is bad is when they //don’t// talk about you on the podcast!

In the outro Ken encourages Jochen to send in all of his corrections, but John thinks that Jochen is going to be fine with it because like most Europeans they going into any conversation with Americans assuming that 45% of what they say is wrong. The one thing they don’t like about Americans is that they are too enthusiastic.

+ John’s first visit to East Berlin in 1989 (OM461)

John first went to East Berlin in the summer of 1989 when it was still a divided city and he did all of the stuff: He went out on a platform and surveyed over it, and it was an incredible and spooky place. We went through the whole thing, he went through the weird little maze where they put you in a room and then somebody looks at you through a window, you have to talk to somebody, they open a door, you go into the next room.

They forced you to change money, but on the other side after a little ways somebody would sidle up to you: ”Do you want to //really// change money?” and they changed $50 or something into Ostmarks and they had more money than they could spend. They ended up going up to the restaurant in the TV tower, stupid American 21-year olds going to a restaurant where all the high muck-a-mucks were having proper dinner in suits, while John and his friends were getting wasted, eating Schnitzel with egg on top and some kartoffel. Thinking back on that now John is kind of embarrassed, and there in not much from his youth that embarrasses him. People around him were appalled, probably Putin was there and he formed his opinion about America.

Ken recommends John to see the movie Wings of Desire that is set in Potsdamer Platz.

John went back to Berlin a few times in the 1990s and Potsdamer Platz was till an empty lot and even spookier by virtue of crossing this nomansland in an open city. Ken stayed there recently without knowing the history and he stayed very close t the Sony Center and was amazed at the German Times Square. There is a scene in the second Jason Bourne movie, The Bourne Supremacy, where one plot point is that he is running around the busy Potsdamer Platz, escaping some agents. Actually it is in Alexanderplatz. The star of the movie is Berlin.
















RL400 - Abberdabber
2020-10-12

This week, Merlin and John talk about:

**The Problem:** John still wants to live on the space station, referring to John wanting to live in the larger world of ideas and not in just a small village somewhere.

The show title refers to the nickname of John’s great uncle Al who used to do magic tricks and say: ”Abracadabra”, but John couldn’t say that word when he was a child.

+ Top of the Pops from the 1970s (RL400)

Merlin was up late, feeding his Komodo Dragon (lizard) and going down a Roderick-adjacent rabbit hole watching a series on BBC about the history of Top of the Pops for certain years, not just a highlight reel, but narrated by the wonderful Sue Perkins ([https://www.imdb.com/title/tt2613148/ 1978], [https://www.imdb.com/title/tt2173007/ 1977], [https://www.imdb.com/title/tt1881095/ 1976]) and Merlin watched 1981 and most of the 1970s. To Merlin it seems that England is always culturally at war with itself and they seem to be embarrassed about everything they have ever done. Considering what a small country it is they sure produce an outsized amount of Western angst.

There is a great line in Ted Lasso where he says to the other coach: ”How many countries are in this country?” When Merlin was 17 he disliked who he was as a little child, and as pubescent him grew he disliked current him and he had no idea how he could ever like future him, which felt very English.

+ John’s daughter being nostalgic for the song Tiny Dancer (RL400)

Yesterday the music-box, the other one, not the one that plays the music when you say its name, but the one where you put up your phone and it bleeps over, the one that is shaped like a soup can, started to play Tiny Dancer //(by Elton John)// and John’s daughter was walking across the room and she paused and gave a big shoulder-shrug deep sigh because this song brings up so many memories for her. It was the first time she was ever saying something about nostalgia and she is 9 years old! What nostalgia? Merlin had extreme premature nostalgia as a kid.

John felt nostalgia for times that had come long before he was born, and deeply in his heart he just longed for these times that he had no personal experience of. Both neither her mom or John talk like that, but she was having an authentic experience of Tiny Dancer being the type of song her mom would have played every morning on their way to school when she was in Kindergarten and 1st grade, and she was having her first wave of ”Wow, this song really reminds me of times!” and you don’t have to be a certain age to be eligible to emotions.

John is watching his kid come online and realizing that they have a complicated independent life now. The role that nostalgia has played in John’s life is so strong, and seeing it in her this times was very different from: ”I miss my friends!” or ”I remember when we used to come here!”, but there was this involuntary sigh: ”Ahh, this song!” and John was also going: ”Ahhh”, but not about Tiny Dancer.

Merlin has several specific recollections of Tiny Dancer just from the last 1.5 years. They were at a retreat that his wife had put up for her work up in Point Reyes and Merlin got up really early to get coffee in the lobby, but they hadn’t started making coffee yet and Merlin hung out and started listening to his headphones and an episode of Strong Songs about Tiny Dancer came on, a really good show about the writing and the arrangement of the song. Merlin also remembers that this was the weekend they started playing Pokémon. Merlin also remembers being in the lift on the way to an event for their podcast network last August and he was listening to the Florence + The Machine version, which is also very good.

It is very Boomer to say: ”You think life is hard now? Just you wait!” and we come up with all of these ways to write people off because they have not achieved the level of existence that we think is necessary to be able to express a certain kind of feeling. Merlin does that, but he wants to stop because it is gross and being a person is complicated and you turn oldie when things get much more simple, and not in a good way. Enjoy the complexity!

+ Merlin decanting his kitchen oils into smaller containers.

In Merlin’s continuing Tiny Life Improvement Project he recently has been decanting the various oils that they use into bespoke containers so that they don’t have to use the big clunky Costco containers. He got those in different colors for $3 and they are condiment containers that he is repurposing. He went into the bedroom, holding two of those containers, and one them had a Sharpie ”C” on it and the other one a Sharpie ”O”, and he held them up to his lady friend to see if he did the intuitive thing, like that Spike Lee movie, and she said: ”C is for canola and O is for olive! I like olive oil!” Merlin likes olive oil, too, but he uses them for different purposes because canola oil can tolerate much higher temperatures, but it is not as tasty.

To be a wise person you need a big tool box and you must not always pick the flat-head screwdriver because that is the nicest one you got, let along the hammer. You need a big ass tool box and you need to know when something you need is not in your toolbox or when to throw the tool box away. It takes situational awareness and experience and metaphors under your tires. Time to Throw the Toolbox Away is John’s favorite Harry Chapin songs.

This means that Merlin now has another level of storage where he has the big mothership containers that are now behind darkened class. Merlin argues you don’t throw away your wallet when you get a suitcase. You Can’t Roller Skate With a Buffalo Herd and you can’t go swimming in a watermelon patch //(song by Roger Miller)//. Merlin does not have a decanter caddy because he has an intuitive sense of what he can get away with. Having meant-to-be ketchup and mustard containers is one bridge, but having a caddy for those with pigs on it might be a bridge too far, although it would make Merlin happy. Merlin explains how he stores all those container on his kitchen island.

Roger Miller was a great singer. ”Trailers for sale or rent” //(lyrics from King of the Road by Roger Miller)// Merlin grew up on that. He was also the Rooster in the 1973 Robin Hood movie. John knew that, but he pretended not to and was playing a game with Merlin, which is what makes this show magic. John called his grand uncle Aberdaber because he would do magic tricks and would say Abracadabra, but John couldn’t pronounce that. He had learned his magic tricks in the trenches of World War I and a lot of those were using match sticks and trained fleas.

+ Naive - wise - cynical  (RL400)

The other day John’s daughter asked him what naive meant and you can either see it as a spectrum between naive and cynical with wise in the middle, or a progression from naive to cynical to wise and inevitably you are going from naive to overcorrecting and becoming cynical and then ultimately you are wise.

John believes that nature plays a role in a lot of things, but less so in this. It is easy to believe that when you are starting out that people are who they represent themselves to be. If someone seems friendly, they are friendly, if someone seems honest, they are honest, and there are different levels of intuition so you can tell a creep, even if they seem friendly, but in general very few people think they are the villain of their own story, but most people believe they are doing right.

If they see an advantage they believe that taking that advantage is right, not that they are trying to hurt you, but they are trying to do what they think is right and in that case it is being selfish or greedy. She was making a Star Wars connection and said it was like the Trade Federation because one of the confusing things about them is that they appear to be in service of evil, but in fact they are just trying to get good trade deals and they feel like Padmé is naive, but later on //they// get played by Palpatine. Nute Gunray is his name.

Naive is to believe that everybody is as they say they are. Most people are going to present themselves as honest and they are excited to work with you, they have your interests at heart, but not all of them will follow through. After you have experienced this a lot of times you will develop cynicism, which is assuming that no-one is telling the truth and everybody has a false face. It feels wise, because they are not dummies and they have learned.

When we call a child naive we mean it more in the sense of Game of Thrones of: ”My sweet summer child” who only ever has experienced summer and who never experienced winter and who are unspoiled. Increasingly the word implies: ”Don’t act like you don’t know better!” John never hears naive used in the sense that a person is being intentionally a rube, but Merlin means that it is shaded with a bit of optimism like: ”Here is how I hope this will turn out! Things really keep going well in Pennsylvania!” - ”Don’t be naive! Look what happened in 2016!” //(referring to the 2016 and 2020 presidential election in the US)//.

That use of the word ”naive” is precisely the description of a cynic: ”Don’t be naive! Haven’t you seen through the gauze like I have?” Oscar Wilde says: ”The cynic knows the price of everything and the value of nothing!”, and that is a perfect example of it. Merlin says that, too! John doesn’t think wisdom is the middle between naive and cynical, but you have to go through both and wisdom is bringing all your faculties to bear. If you are going into every situation assuming the worst, then that is the life you are going to lead.

Merlin thinks that explaining words to children teaches you how much you know about stuff and you have to adjust your screwed-up idea of the world to be meaningful to this child. A great way to stall for time is to ask: ”What do you think it means?” because then you also sound wise. What is wisdom? What is good? To hear the lamentations of women, to drive you enemy before you! //(quotes from Conan the Barbarian)// John told his daughter all about that: ”The thing about victory is: It is never as sweet as when your enemy is at your feet, begging for mercy.”

John doesn’t try to eliminate bias, but he tries hard to contextualize, also with adults, which is why he never gives a simple answer to a simple question. He is not afraid to put his own opinion or his own bias in there, but he wants that bias revealed: ”Here is how I came to this... Here is why I feel this way...” Expose your bias! It is the antidote of just shouting at each other because it is like: ”Show your work!” If you feel like you are old enough to start lecturing other people about anything, then show your work!

One of John’s complaints against modern journalism and against people who try to adopt that lofty tone is that they try to do describe things like ”naive” with zero specificity in the most general language making the description very opaque, believing that they are giving a non-biased answer and that they are being scientific by reducing these concepts to manageable knowledge junks, but ”naive” is nothing if not described with specificity and for a 9-year old you can tell a story that will root the word ”naive” in something she really understands.

The example John used was when her friend Katie said that John’s daughter could come to her birthday party, but it turned out that she didn’t go because Katie was just using her to get to Brenda. In that situation Katie thought that she was naive for ever thinking she was coming to the party and she was being cynically manipulative. Katie’s mom would not have described that situation that way, and that is where John’s daughter’s and Katie’s education differ, and being exposed to these situations is part of growing up. Your take-away is rooted in your learning of vocabulary, your learning of civics, it is all very specific and personal at first.

People think they can denature or neuter language (John said ”deneuter” at first) by taking the specificity out of things, but they are not skating across the top of the world untouched, but words started to loose their meaning.

+ Context is more important than Content (RL400)

Merlin is a sucker for context. John almost thought Merlin was going to say ”sucker for content” and he was going to virtually high-five him with two emoji thumbs-up. It is a perfect example of the corollary. He and John Siracusa have their beefs about how ratings and reviews work and what they are useful for, and Merlin thinks that a single 5-star rating for a thing not very useful. Nobody sits down and looks forward to watch that 3-star movie tonight. 3 stars for what? We need some more context! Merlin loves context, it is so much more important than the content.

What we know about Christopher Columbus is not useful without context. What else was happening at this time? When you put this stuff in context it makes it richer. One of Merlin’s favorite podcasts is the Blank Check Podcast where they talk about movies. [https://soundcloud.com/griffin-and-david-present/back-to-the-future Last weeks episode] was about Back to the Future, but they talk ”about about” Back to the Future for an hour before they talk ”about” Back to the Future. It is one of Merlin’s Top-10 favorite movies and he doesn’t need somebody to give him the plot or some critical analysis, but he wants to hear the hosts nerd out to things related to this.

John’s award-winning podcast [[[Friendly Fire]]] has the same idea. You come for the content and stay for the voices and if you don’t like the voices you are going to bounce. Some people are really super gay-bones for content or the specificity of the lesson you instill in your child vs a more liberal arts approach of how we know what we know and how we learn more of what we need to learn and how we know when to throw away our toolbox.

+ The listeners of Roderick on the Line, you can’t please everybody (RL400)

For years they have been saying [[[What is in the show is in the show]]], but in John and Merlin’s pond, their Bay of Fundy, they are making content, everything John has ever done, the music and every show he has ever done, and in general the conversation they are part of, their corner of the discourse and the people that listen to their show and the conversations they are having with each other. There are tendrils that go out into the whole world and when they mention something on the show John will get an email within a day from Israel or Turkey or Korea.

They have listeners to the show in Saudi Arabia who are not expatriates, but who are Saudis who have discovered Roderick on the Line and emailed John, and it is real and they have some completely amazing social media account of some young Saudi guys who are living their lives, not something somebody drummed up to mess with him. The pond that they are in is also somewhat a discrete space, which is confusing because they do feel connected to the larger world and they grew up in a world with a megalithic culture or a monoculture and they were trained that the goal was to get your voice into the big stadium, to get up there and make it over the line and be heard and be synced up with a school of thought. Like life was a railway and you are going to ride it whether you like it or not and you are going to be either on or off.

Merlin never felt like there were ever multiple options available to him for anything, like a job, an education. Maybe that was a failure of his imagination, but in a lot of cases that was what was encouraged by a very risk-averse family that had a lot of loss and a school-system that was trying to prepare him to be a good worker. Merlin didn’t even have the temerity to dream very big, otherwise he would have thought about how to get this to an editor or an agent or somebody in a position to give him approval that will become an entrée to the next thing.

John’s dad told John to go to law school because that was his idea of what it took to step outside the hermetically sealed culture bubble and breathe water for long enough to get to some other pod. When Merlin was a Junior his assigned guidance counsellor pitched him the idea that the Air Force was actually a pretty good deal. You get three hots and a cut, free clothes and hair cuts, you get to look at planes.

John and Merlin are part of a culture and they are even culture-makers. We are at a turning point in history where the ship has sailed where they are ever going to get all the Americans back into one box. Peter Sagal said on Twitter earlier today that the thing that made America exceptional was not that they have a constitution, because a lot of countries have constitutions, but in America the tradition was that the losers in any political dispute agreed to abide by the law. In other countries the problem is often that people don’t abide by the constitution and when the people in power lose they don’t consent to lose.

We are now living in a time where it seems that we are for a while not going to make it back to a whole, which is troubling because a big part of that is that we have splintered into a universe of 1000 little ponds of people who all get to have whatever believes they in their pond agree are real and they consume only media that supports their ideas, and (as John Siracusa would say) they evolve within their little ponds and live in a state of altered reality. Just for our own sanity and for peace we have increasingly resigned ourselves to disengage from the idea that we can help everybody or that everybody is going to get it.

Merlin was the one who taught John not to try to please everybody on the internet and not to try to be everybody’s friend. You don’t want to make a thing that everybody likes! Having a lot of followers on Twitter is a blessing and a curse, and the curse is that you have a bunch of people on there who are not your people and who are just following you because you are on a list and now they are shitting on everything you say and you are dealing with static all the time. In the last 10 years John and Merlin have shrunk their worlds around themselves a little bit by muting people that suck and by not reading other media.

This does not mean that they have shrunk into a bubble where they are feeding of a tailored media tit that only tells them half-truths, but the challenge is: How can John not be constantly at war and not be constantly nauseous from trying to convince people who believe that Hilary Clinton is at the head of a pedophile ring, while at the same time still believing in truth and still being part of a culture that is trying to preserve truth and justice and at the same time having a culture that is personally gratifying and that isn’t just constantly bashing their shields with their swords.

Merlin thinks that even if everything you do is great and above board, he can still really not like the way you do it. It is not selfish or naive to have your own idea of the rules by which you will play and what you will tolerate and allow in your own life. We can still go out and try to find diverse opinions. On a highway where the speed limit is 75 you can get away with going 90 or 55, but please don’t go 200 or 5, that is not how this is going to work and if you are not willing to be somewhere in the range of normal parameters for using this road, maybe this is not the place for you and we should not be ashamed of saying: ”Hey, maybe 200 is a little fast for this!”

Merlin doesn’t think it is fancy to decide that you have standards in life, whether those are standards for the content you create, like: ”Who do you want to please?” He is not going to try to be everything to everybody or to be all the way available to everybody. John Dickersen [https://twitter.com/hotdogsladies/status/1311020515646078977 said last week] in the context of the Vice Presidential Debate //(related to the 2020 election)//: ”With so much good food on the table, why would you take the bait?” It is so important that (Kamala) Harris not take the bait. Biden took the bait a little bit and ended up wrestling a pig. You don’t want to end up talking to no-one else except the Vice President as you get really wound up.

When they first starting doing this program, talking on the phone and arguing about The Beatles and Hitler and putting those conversations on the Internet, their early listeners were a lot of tech people in San Francisco. John was telling stories with this picture in mind of people in San Francisco whose shirt collars were too small and whose pants were too tight, who were listening on very expensive headphone that Marco Arment had told them to buy, and they were living vicariously. 

As the years have gone by and John has met their listeners and has communicated with them, he gradually realized that those people in San Francisco were not the only people they were talking to anymore. John has certainly had several identity crisis over the years about who his listeners were and who he is making things for, and that has been a really bumpy and unsettling trip. Every time somebody says: ”I am listening to your show anymore because of X”, it breaks his heart, even if he then would study that person and wonder why they ever listened to the show.

+ Gary’s Van (RL400)

John always wanted the largest possible tent because he wants everyone to get along and for everybody to see that they have more in common than they have differences. There is a Facebook group called Gary’s Van, which is devoted to discussing... because there is not much of a Roderick on the Line fan universe on Reddit, where people are trying to enjoy the show and the suggestion is that by listening to the show you would want to take these conversation to a third location with somebody, not a hippie, but to go sit over coffee with somebody.

Gary’s Van is a place where people get together after the show to have a cup of coffee and talk about the ideas.

+ The audio quality of the show (RL400)

Merlin is trying to get better on muting because people who claim to like the show think it is too noisy, but if he reduces the noise any more there won’t be any sound. Merlin was muting because it was morning and he was still clearing his throat. John makes so much noise on this program that people must think he is made out of phlegm. It is the Trump problem: John lowered the bar and now he clears it while Merlin is still playing by the rules.

+ Ted Lasso (RL400)

Merlin has been talking with Jason (Finn) about Ted Lasso and they are over the moon for going back and forth about it. Someone on Gary’s Van was saying they were agreeing with Merlin about Ted Lasso and John was thinking that Merlin is recommending a lot of content and now they were talking about it on Gary’s Van in a way that this did not sound like something that John would like and if should probably not exist, let alone be successful, but he asked Merlin again about it again and he watched the first episode with his sister and she was climbing the walls in joy and insisted that they would watch 3 episodes back-to-back and last night she came over again to watch 3 more.

John is now deep in the Ted Lasso verse and he is enjoying it very much. There is more drama, Ted is experiencing a lot of personal turmoil. He doesn’t tend to jump on episodic television, but his sister and daughter’s mother both do, so now he is open to it. Coronavirus really got us watching a lot of TV. Having this show come along when it did and be based on a TV commercial is all pretty weird.

The song from Ted Lasso (called Forever, not Steppin’ Stone as John suggested) is sung by Marcus Mumford and there is a lot of Mumford-y music in it. John drove those guys around Seattle one time in his dad’s car.

+ How the Internet left the content to the architects (RL400)

John wants to be a part of the larger world. He doesn’t want to live in a village, he never did. Increasingly he doesn’t want to live in a city either, which is a weird thing he never thought he would say. Seattle feels small again, like it did in 1992. It always thought it was bigger than it was, but it got to be a big city for a while in an exciting way, but now it feels provincial and puny relative to what it could have been.

When John first heard of the concept of the Internet in 1990 he had the feeling we all did that this is the league of nations, a one-world government, the universe in a pen-cap, and this can only be good and there could not possibly be any downsides with connecting everyone. and having access to all information. John still believes in it and he believes we have done a terrible job of navigating those early years, partly because of late-stage capitalism and partly because the best websites were not for the best products or the best writers, but they were for the product of the guy who knew how to do his own website.

Great bands had shitty websites and the bands that had great websites were the bands that had someone in the band who could design a website, and if Merlin hadn’t designed the Long Winters website there wouldn’t have been one. We seeded the content over to the people who knew how to build the architecture and all of our Internet ethics and habits were determined not by ethicists or by writers or philosophers or thinkers, but they were designed by [[[computer maths]]] guys to be efficient for them, often because of a failure of imagination.

That documentary is: ”We didn’t know how to make money so we went this way and that has created a Hitlerverse!”, that Netflix show.

+ People using the methodology of their enemies for their own purpose (RL400)

John still believes in this larger world and that this social media era is going to go into the garbage can and is going to take a lot of stuff with it and ”Good riddance!” to that rubbish. Seattle, in attempting to make a place, the city has set its sights too low and made a place that is culturally too small. Everybody feels defensive now. It is the constant crisis of liberalism and we will see it when Biden gets elected that a ton of people on the left want the left to be as vindictive and small-minded as the right has been and to go in and do all the same shitty things in reverse, partly to reverse some of the shitty things that have been done, but also to really stick it to them.

That is the classic mistake of thinking that the left and right are equivalents and are just two sides of a coin when they are not. The right is a way and the left is a better way, and the better way requires that you not allow the other side to set the tone and the rules. Not just that you paint everything white that was black, but you have to bring care and compassion back, which are not just the opposites of hate, but they are a completely other thing and they require a different way.

In attempting to be a place of light and to be anti-dark, Seattle has increasingly chosen a methodology of the things that we hate in service of the things that we want. The methodology is the problem! Merlin replies that the answer to tearing down values is not to tear down different values, but to build up values. Once you allow to become a black & white kind of thing you have lost because now you have allowed to other side to turn chess into checkers and checkers into pieces of plastic and keep dumbing the game down instead of saying that it is not a game, but this is life and this is the future we want for the people that we love.

People say that the opposite of love is not hate, but indifference, and that kind of rethink is required to stop reacting to everything and to have an approach that reflects the values we have. There is more to it than getting to be hall monitor for 4 years.

For many years San Francisco was a paradise where progressive law could be put into action, but liberals are more critical of each other than conservatives are of each other, and it is very hard for the left in addressing problems of the city to know what to do when you put policies in action and they create unanticipated negative results that no-one expected. No-one expected 50 years ago that The Mission would become what it became and that The Mission would in the last 15 years go from what it was to what it is now.

Nobody in San Francisco in 1968 said that one side-effect of these programs is going to be that in the 1990s if you walk down on Market Street there are going to be people throwing human poo at you. That was never part of a plan. Mid Market was the thriving theater district with theaters and sporting goods and electronics stores. It is like Back to the Future II right now: You can really see what this would have been like at a different time, but it ain’t that now.

The problem on the left is that we don’t know how to address the problems we create without looking like our conservative critics, and we are so afraid to do anything that might taint us with the brush of appearing to agree with our conservative critics for even a moment that we often can’t address the problems that are intrinsic to applying new policies that are earth-shattering. We put something into place and we realize we need to recalibrate it because people aren’t getting the help they need, but it looks like the solution is going to be too close to what our conservative critics are saying is the problem and we can’t go there.

We have to constantly be at war with that and in the end we don’t find the solutions to our own problems, and that is what has happened in Seattle. Seattle has played whack-a-mole with the problems that our their legislation created, but they can’t truly whack them because (there are no conservatives in Seattle) it looks like what the liberals would propose. They have done a hash-job there! But John wants to live on the space-station, still! He wants to live in a world of ideas and a world that is not on the brink of civil war, but a world where the continuity of ideas that began when human beings first wrote things down continues. It is right there in the word ”continuity”!

One nice thing about living on a space station is that you do have to find a way to get along, and if there is one person who can’t get along? Shoot them out the airlock like freaking John Hurt //(in the movie Alien)//. You have to drink your own pee and then John Gurt goes out the airlock, those are the fundamental principles of space station. They definitely ought to talk about [https://www.thestranger.com/slog/archives/2012/07/26/the-matter-of-bonuses-in-alien the bonus situation].

















RL421 - My Mom’s Ohio
2021-04-12

This week, Merlin and John talk about:

**The Problem:** There are too many stadia in this town, referring to Cincinnati Ohio having more stadiums than John thinks they would need.

The show title refers to Ohio being very different in different parts and John’s mom being from the Northwest and her Ohio leaning towards Columbus.

+ Iron Maiden (RL421)

John was two minutes early to the recording! ”... killing the unborn in the womb” //(lyrics 2 Minutes to Midnight by Iron Maiden)// - ”chasing the redskins back to their holes” //(lyrics Run to the Hills by Iron Maiden)//. Iron Maiden love history, all their songs sound like tropps galloping. The Charge of the Light Brigade was not a great idea. //(The Iron Maiden song The Trooper is about that battle)//. They did the old West thing where the rode into a canyon and they didn’t look up. It is all about position!

+ Merlin’s dad (RL421)

Merlin’s father was a foot soldier in Korea, he had a terrible time and he had PTSD before it was cool. He was a Private and they told him he became a Corporal, but then he refused to paint something when he knew they were bugging out and they busted him back down to Private. He wrote a coded letter to this mother, saying something along the lines of ”It is so hard here, it sure would be great if Betsy were here!” and the story goes that his mother disassembled his pistol and sent it to him in packs of popcorn over time and then he had a way to deal with snipers. Everybody involved in that story is dead and Merlin doesn’t have a way to vet that, but it is a good story.

Merlin’s dad was in at least 3 front-line battles, real trenchy in-the-shit type stuff. He was a gun guy, he was an NRA guy when that was a cool organization, he taught gun-safety classes, he was a marksman, he was an outdoors-guy and he was pretty handy with a firearm. He grew up with guns. First he was in radio and broadcasting, but the job he had then until he died was a sporting goods purchaser for different department store chains in Cincinnati, which in the 1960s sounds like a fun job. He got Merlin a basketball signed by John Havlicek. Back when sporting goods were sporting and good! First he worked at Brenda Morris (?) and then at a place that eventually had to change their name to Van Leunens, but in the early 1970s they were called Chinatown.

Both Merlin’s and John’s dad fought on the side of America in a war in Asia and did stuff with a pistol. John’s dad shot a Zero out of the sky with his sidearm //(see RL34)//. They issued them all M1911 .45s and they were scoring the tops of their bullets with an X to make a dumdum bullet. Merlin shot a .45 once and he nearly shot himself. John was right there and he could tell Merlin was a native gunsman. About halfway through the war the Japanese sent a message by currier, saying: ”If we catch any pilots with this .45s we are going to kill them right out because these dumdum bullets are unfair, it is not sporting!”

After that all of the .45s were recalled by the Navy and they issued you a .38, a revolver instead of one with a magazine. Apparently John’s dad had said that he had lost his .45 and at the end of the war when everyone had to give their guns back he had a gun to give them back, but somehow he was able to bring the other gun home, which he was probably not supposed to do. John still has it and it says ”Property US Navy” on it.

+ John’s guns, his family being very anti-gun (RL421)

John’s mom is extremely anti-gun. John’s daughter’s mother / partner’s dad is an Air Force veteran from Vietnam, a college professor guy who worked in the press office like Private Joker //(from Full Metal Jacket)//, he is a car guy and motorcycle guy, he raced Lancias, he is not a gun guy, but he is a Lancia guy, and he gave John’s mom for Christmas a Ka-Bar knife, which is the Marine Corps combat knife that is 12” long that you would clench between your teeth as you scramble up Hamburger Hill, basically a bayonet.

She didn’t know what to do with this thing, it was a freaking sword, and John offered her to leave it at his house and he is now using it daily to open boxes. When Ariella (John’s daughter’s mother) was growing up he had some pearl-handle pistols, but she is very anti-gun. Everybody is very anti-gun around John, they are all liberal. John has a couple of guns, he has a shotgun he bought at an auction for $50 and it cost him $950 just to sign the papers, he got his one over here, he got this one saying ”No soup!”

When John’s daughter was born he was told in no uncertain terms that he could not have a gun in the house with the baby. Merlin’s dad did all the things. He had a bunch of trigger locks in all the fire-arms and the ammunition was always stored somewhere completely separately, unlike Merlin’s stepfather, you would not have a loaded revolver by your bed, or like John’s brother under your pillow. John’s dad kept his gun on top of the refrigerator, as you do, and it was never a problem because John always took the magazine out before he played with it, and he also mostly checked the chamber.

John’s mom’s dad was a freaking crazy gunsman, although on a farm out in the middle of nowhere it makes a lot more sense to have a firearm around. When he died he had 200 guns and when John’s mom was growing up there was a gun leaning in the corner of every room in the house. She was never afraid to be home on the farm at night because there was a gun in every corner, but it might have done a little number on her because she is very much anti-gun. She is also anti-police and she doesn’t believe that there should be armies, and John does a lot of tut-tut-ing, which is the only thing you can do.

When his daughter was born John bought a gun safe that has a fingerprint ID combination and when you open it a little light goes on inside and only he can access it with a secret code and he told everybody that the gun is now in a gun box, but it was still not enough and he had to hide the gun safe. Every extra step that John puts in between him and retrieving this sidearm when the zombies come is making them all unsafe, but his was scoffed to death by the army of women that tell him what to do.

They believe that they whole argument of home defense and hand guns is specious and holds no water and they are quick to point out that John had a burglar in his house that he thought was a possum. If he had been awake he could have just stood at the top of the stairs and yelled: ”Get the hell out of my house!” and they would have dropped everything and ran. If there had been a gun in every corner of the house then that burglar would have stolen 15 guns. Merlin still blames the possum!

+ The purpose of possums, evolution, bugs (RL421)

John has not seen a possum in a long time and he is wondering if there was a possum plague but nobody cared and it didn’t even make the newspapers because nobody cares if there are possums or not. If had been was Pandas then people would care, even if it was other gross things like bass players, but nobody would miss them until you realized the guy playing bass on the synthesizer was dressed like a doctor who was playing a keytar. John has never done enough research on possum to know what they are there for. He would consult Kipling’s Just So stories, but they are cancelled and he might not even have written one about a possum.

Merlin thinks they should loop John Siracusa into this and he would talk about evolution: There are challenges and opportunities that over time lead to the survival of the fittest. The question is now what the possum did surplant. Merlin heard that the only truly useless animal is the mosquito. As Hüsker Dü says: The rats eat the cats and the cats eat the rats //(lyrics from the song How To Skin a Cat)//. There are bird that sit on cows and eat the bugs. John always thought the the mosquito fed the bats, but maybe the only reason for bats is to eat the mosquitos.

The worst mosquito attack Merlin ever experienced was in a tent in Gainesville Florida. On time they were visiting Merlin’s lady’s family in Providence //(Rhode Island)// and they went to the Y and there was a beautiful little grove over there and they decided to take a walk there, but as soon as they stepped off the concrete into the grove, which was just a bunch of trees and some standing water... holy shit! Tora, tora, tora! John is from Alaska and there the mosquitos lift a moose right off the ground.

Some of their listeners are chiropterologists //(study of bats)// who came up to John at events and talked to him. In those cases he always likes to learn what happens in a persons life that they choose that career. Even Merlin would be interested to learn more from them although it otherwise is not at all interested in what people do for a living.

In the case of a possum ”God” / Evolution makes a thing for every thing, for every purpose under heaven. John has talked to Siracusa about this quite a bit. There are needs and there are must-needs and nature turns into nurture and makes a thing to do evolution for every thing. The possum can only exist because there was a niche that needed to be filled. It can’t be purposeless, it can’t just wander around, [[[find your duck |looking for its duck]]]. Thing in nature have their ducks, only we and our poor dogs haven’t found their duck. Possums know what their duck is and in most cases it is a bowl of cat food.

During rainy season in San Francisco there has always be the problem of Sugar Ants, but now it isn’t even rainy season and Merlin has the ant problem anyway. When it rains a lot of critters get displaced in the park with the confederate ghosts, like for example snails that are driven out of their house. Ditto sugar ants: When it rains it is driving them out of the ground and they are going up, seeking shelter and food, and they are going after Merlin’s heinous and truly grievous grotesquery of a cat’s food, even in April, which is way past rain season.

When John was fighting sugar ants he learned that ants can drown, which he didn’t know before. You can put a marmorated stinkbug //(see OM108)// in the toilet and it will swim around all day, and if you flush them they will become somebody else’s problem. John has never found them to be particularly stinky, but that is because he doesn’t crush them. He just grabs them and flushes them down the toilet. In Florida there are a lot of euphemisms for bugs, like you don’t call it a cockroach but they are called waterbugs. They are africanized or asianized, not to be [[[ping pong]]], some of them can fly, and they are hard-wired to fly directly into your fucking face.

Someone told John once that he was partying in Florida and was taking a big toke out of a bong, but there was a big waterbug in the bong. One time Merlin’s mom was playing tennis with a woman and she was drinking Pepsi, like a monster, and there was a bee in it. The question is if it was a bee or a Yellowjacket, and it bit the roof of her mouth when she was playing tennis. John still wipes off the top of a can of pop because there is trucker pee on there.

One time Merlin was on a school field trip to the pumpkin patch and when the Yellowjackets get bored with the pumpkins they go after the kids and their dads //(see RL323)//. When you smash them it is like Mitch McConnel, it just makes them mad and now they know to go after you. They are also attracted to the color yellow.

They talk for a while about how to pronounce opossum or possum.

+ Ohio, where John’s mom came from (RL421)

John’s mom is from Ohio and hates hillbillies, they are the people that she hates the most, even more than Bonaparte. Her hate-chain in order is people from Kentucky, people of South-Eastern Ohio who are in her estimation people from Kentucky, people from West Virginia and then everyone else. As far as she is concerned there is no greater divide than between the people of North-Western Ohio and the people from South-Eastern Ohio, there is no thicker border in the universe, not even the border between Turkey and Greece.

Merlin knows something about Ohio, he once made a plaster of Ohio with Indian mounts, he had the Ohio education in 4th grade as a Cincinnatian. He knows Toledo, Cleveland, Columbus, Daton. The town where John’s mom grew up was Lima Ohio and there is nothing to recommend that. Those towns are all built on what was formerly the edge of the Great Black Swamp, which was drained over the course of decades by hearty pioneers. That swamp must have been an amazing place.

When John’s mom was growing up, Lima Ohio was the bustling nearby slightly larger town that her father drove the bus back and forth to. If you were going to go to the department store to get some frocks for the dancing season, you would turn the other way and go to Fort Wayne Indiana, but Lima? John has been there and he doesn’t ever need to go back and he doesn’t know why anyone would go there. John had gotten into a minor traffic accident in Lima and he just wanted the heck out of there. He might even have told the cop to write that ticket faster.

You would think that John’s mom’s Ohio would be oriented toward Toledo, but it was oriented to Columbus and she probably has an abiding love of Columbus. Merlin thinks that Cincinnati is its own thing, it is the cosmopolitan baby of Indiana and Kentucky getting drunk and fucking one night. It is a very conservative area like Indiana and a very hillbilly area like Kentucky, but it is not the union state like the Northeast part. There is a corridor from Columbus to Cincinnati with Daton along the way which is the fancy area of Ohio, although the real money in Ohio is up in the Amish farm country around Akron.

You think of the rust belt cities as all poor, but when you get into the farms around there you will have Big Money Ohio. The Mennonites make a very good pie, Merlin even had a Mennonite pie place in Sarasota, of all things. There is an Amish hardware store in Ohio around where John’s dad’s people are from and every time John’s mom goes back to Ohio where her city was born she goes there and sends him pictures of exotic woods that are naturally green and orange. John has never been there.

John’s experience of Cincinnati is that the radio station WOXY was a huge Long Winters booster. Merlin’s friend Chris Glass who did 43 Folders made their website and Merlin was a member there. They were like a KEXP in a lot of ways, but they are gone now. You hear that Cincinnati is the capital of Kentucky. Their airport is in Kentucky and the venue John’s band used to play at was also in Kentucky. They would take the Brent Spence Bridge over and play the Road House there, which is now gone, too.

Merlin’s Amazon device decided to tell him the top-ranked airports in Kentucky.

Cincinnati did the same thing that Seattle did that was very popular with cities: They built a stadium and then they decided to build a second stadium. The last time John flew over in a biplane he looked down and thought that there were too many stadia in this town.

+ Sneakers (RL421)

Did Merlin have Nikes when he was in 7th grade or knock-off Nikes? Both! His first ones were white canvas high tops with a black Nike swoosh, but eventually he got the Dynasties, which were the coolest athletic shoes he ever owned. Mostly he had to beg because his mom didn’t have a lot of money. He got the fake Adidas from JCPenny with 4 stripes. In the early 1980s John got the ones called Stadia where the swoosh looks like a whale, sold by Kinney //(see RL320)//. It is like the white leather Nikes with the red swoosh, except they had a red whale on them.

John was not status-conscious, he did his own aftermarket alligators on his shirts, but when he got these it was not because he had begged for them, but his mom just went to the store to buy clothes and brought them home. But as John wore them to school it was patiently explained to him by everybody that his shoes were knock-offs, which might have been the first time he understood what a knock-off was. After Polo came out and everybody had a shirt with a little guy on it, but they were always the wrong guy unless you paid $80 for the shirt. But because John has the personality that he has he wore his Stadia with defiance and that just increased his popularity with all the kids.

+ Merlin’s people’s origin (RL421)

John’s dad’s people are from Kentucky. Most of Merlin’s family as well, before they were Cincinnati. His grandfather was from South America, but all his dad’s side of the family mostly, and his grandmother is from Level. John likes how Merlin pronounces that, he goes in between and doesn’t want to go crazy about it, he is not trying to fake a Brooklyn accent //(see RL25, John trying to adapt his accent)//. How many vowels can you take out of Louisville?

Merlin’s grandfather was too young for World War II and Argentina rejected him. His family was from London, they were in the industrial diamond business, but now high up //(see RL47)//, and they were down there gutting all the resources. Merlin had a friend in the diamond business //(probably reference to the movie Snatch)// ”Every kiss begins with Kay” //(slogan by Kay Jewelers)//. Grandpa’s family was in British Guiana right outside of what we would later know as Jonestown. Anybody he could ask about this is dead.

The story goes, like Betsy the pistol in the bag of popcorn, which is a great Warren Zevon song (?), that he came to the US to go to dental school, which seems very strange and sounds like it was just what was on the form. He ended up shutting off people’s electricity for Cincinnati Gas & Electric for 34 years. The story goes that he came from South America to Cincinnati in 1930, which was great timing.

+ John’s cousin going through boxes of old pictures from their ancestors (RL421)

John’s cousin Alfie (Alfred) is a cement contractor. When John’s great-grandfather, [https://www.historylink.org/File/9482 Judge George Alfred Caldwell Rochester] and his wife died they left all of their exciting family history to their children and somehow it all filtered down to two people: Alfie, through his mother Mary Ellen and Junius through his father Alfred Ruffner Rochester, but none of it came to John because his dad was not interested. Alfie lives out in Poulsbo and has been sending John scans of sepia-toned photographs of these people in Kentucky in the middle of the night, some picture that John would walk over hot coals to hold in his hands, like his grandmother and her mother and her mother on a boat to Paris in 1912.

Alfie goes through innumerable boxes of incredible photos that he inherited of all of the people on John’s father’s side and John’s mom just pulled out her boxes recently and John has a shadow-box now in his living room of his great-great-grandfather’s Grand Army of the Republic Reunion medals. After the Civil War those guys got together every summer and made medals that they would pin on each other for the entire rest of their lives until they were 94 years old, but let them have it!

+ John wanting to have some good awards (RL421)

John and Merlin never really made awards for each other. It feels very Finn and Jake //(from Adventure Time)//. Merlin would make John the greatest burrito in the world. Their fans have made them multiple [[[The Phoney Awards |Phoney Awards]]], an Australian podcasting award. Merlin has it in his office and it makes him so happy, they have been nominated a few times. Award-giving is not part of the day-to-day of their relationship. John doesn’t want a white ribbon.

When John and Millennium Girlfriend were planning to cohabitate, there was a brief period where they went house shopping in Seattle, which escalated quickly and all of a sudden it was a 3500 sqft (325 sqm) mansion on the North side of Queen Ann hill, and John was never going to live there. She had really heavy-weight diplomas while John at the time hadn’t even graduated from the University of Washington and he didn’t have any diplomas and nobody had ever given him any awards. He just had some certificates of participation, but he doesn’t want those.

John once asked Jason Isbell to make him a Kentucky Colonel, but it turned out he didn’t have the power. One of their listeners is in Kentucky who was trying to make John a Kentucky Colonel a couple of times, but she keeps proposing him and they don’t reply to her emails either. Merlin thinks they should make an award for John instead of giving him for example the Mark Twain Award because John is not the next Mark Twain.

John almost joined the Sons of the American Revolution a few years back, and the only thing they do is have a dinner every month, but according to the emails John gets where they ask for the head count so they can tell the restaurant how many kotletts to have on the hotbar, and that doesn’t sound like a great dinner. The other thing that they appear to do is to hand out medals to each other, and it was partly John’s desire to have medals that made him think about joining because maybe he could get a medal for something that his 7th-great-grandfather did, which was fight in the American revolution. He wouldn’t have to do anything, but would get a medal for being around. But John doesn’t want to eat a lot of veal out of a hotbar, but that isn’t veal anyway, it is what Merlin in elementary school used to call chicken fish.




















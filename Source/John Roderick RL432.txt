RL432 - Water Taxi Party
2021-07-20

This week, Merlin and John talk about:

**The Problem:** Anything can be a chili, referring to John putting all the leftovers in chili unless someone can explicitly tell him a reason why a specific piece does not belong in chili.

The show title is referring to John hiring a small boat to get his daughter to camp on Orcas Island in the San Juan islands instead of booking a ferry that was only available at 7am. 

+ Weight (RL432)

Merlin starts the episode coughing like Sweat Leaf by Black Sabbath and John chimes in and they sing a few notes of it. Merlin got some allergies, but he is just a little swollen. John thinks Merlin could live with being a little swollen, and his weight stays mostly the same, but it shifts, which he doesn’t love. He has a short rise and that means the world is out to get you with comedy pants sizes. He is getting the bifurcated gut thing. 155 pounds (70kg) is his normal low, but he is probably 168 (76 kg) right now which he can live with. He sent a photo of him in High School to John Siracusa where his Levi’s size was 29/32.

John didn’t have a 29-inch waist since 5th grade. He was a trim little sporto until 5th grade when he went to live with his dad who just kept bowls of M&M’s lying around the house. He liked deserts and he didn’t understand nutrition. John, his sister and their mom would share a single box of Kraft dinner Mac & Cheese, while his dad would give him the whole box and that was the beginning of the Husky phase that lasts until this day. 

+ Cooking all the food, using leftovers for making chili (RL432)

Merlin has inhabited the concept of Make All The Bacon. His daughter’s mother is a saver and makes an amount of food that she thinks a normal person would make. If Merlin has a pack of bacon, or a rasher of bacon, and a stick of butter, "Telephone, telephone” //(reference to a [https://www.youtube.com/watch?v=Im4GwUD1UY8 Sesame Street classic])// Are you going to save two strips of uncooked bacon? Unless you are John’s daughter’s mother / partner. You are saving $0.04 of food and put it in a $0.20 bag and now you are paying rent to store garbage. She will also leave 18-25 spaghetti in a [[[child-speak |scabetti]]] bag because she is tithing it to the ghosts. It is madness!

The cooking is the hard part, not the buying, the owning, or the eating. Once John is in a cooking mode with his chef’s hat on and his Mickey Mouse gloves on, his radio turned to the Oldie station, he got his spatula in one hand, he is Ernest Borgnine in Escape from New York, he wants to bring out all the food that needs to get cooked and cook it all at once! Cooked food is easy to deal with because you put it in the snap-tight thing and put it in the fridge or the freezer. John loves leftovers!

Because John is now splitting his time between 2-6 houses there is food all the time. Everybody is making food and nobody eats all the food, so there is always some extra food, too much to throw away, too little for anybody to pick it for themselves. If they were good at portion control they would look at those containers with one chicken thigh as a perfect snack, but nobody thinks that way. Then John comes in, opens the fridge, takes out all the little containers and the latest innovation is that if you cannot explicitly tell him that for reasons you will elucidate that this particular small ramekin of food does not belong in chili, he will put it in chili.

If you chop up Pad Thai fine enough it will go in chili as long as the Pad Thai is not the majority of the chili. A lot of people have a Le Creuset pot, or at one time everybody wanted a Kitchenaid, you got those things sitting around, and you do want a Le Creuset pot for your house because they are such beautiful things. Merlin got a stand mixer for their wedding by Leslie Harpold and now every time they make a cookie they think of her, rest in peace. A lot of that wedding gift stuff doesn’t get used.

The Le Creuset is very good to make a pound of beef with a little onion and a few peppers and you throw in a can of vegetarian chili because you don’t want to eat their meat that came out of a tube and then you go through the refrigerator and look for other things to put into the chili. The other day John put Refried Beans in the chili for the first time and it was very successful. They stirred right up and they are gone. There are already beans in the chili, so it is just more invisible beans. John also found a thing of Basmati rice and put it right in.

Beef and onion together are a buddy company, and with a bit of garlic it goes a long way, and everybody else is along for the ride, they are in those flat pickup style boats with Tom Hank and the flap goes down and they are running onto the beach towards the machine gun nests and you look over and the guy’s face is blown off. The giants asterisks on the beach, the tank stoppers, are called Czech Hedgehogs. A lot of people feel that represents Slovak erasure, as with the ”Czech New Wave”. There are not going to be tanks coming out of the water. That scene is still very upsetting for Merlin. That movie is still right up there in the pantheon, it was the first movie they watched for the [[[Friendly Fire]]] podcast.

Then you put a little cheese on top of it and let it cook all afternoon. John made a category error the other day when he put hot Italian sausage in the chili which might have not been bad, except somebody had made it a long time ago and put it in the fridge and it got pushed to the back and had been sitting there until it took on a texture that was just a little bit stiffer than anything else in the chili and John should have chopped it up one more time and put it in a scabetti [sic] sauce because you wouldn’t have noticed it, but in the chili it read as a foreign object. It was all but the kitchen sink and everything else in there blended beautifully, it was the nations of the world.

John would not serve this to anyone else and anybody who knew how it was made would not want it, but John can eat and enjoy anything because what is it really? There are a million spices in the world and they all go together except olives and capers. John could put a ponato [sic] in there, even your patatas fritas because once it is in the chili it is gone. Ponatos and potatoes different. Ponatos are tomatos.

There is a diner in town that makes a good pot roast, which is rare. It comes with mashed potatoes every time and John will eat them because they have gravy on them and what he is really eating is gravy and he is using them as a gravy-delivery-device. He doesn’t need them as a grease- or salt-delivery device because he has other things that work in the form of noodles. Merlin won’t look back from noodles. John will put anything on noodles. When it comes to the table as gravy on mashed potatoes, that is the one instance where he will eat potatoes.

+ John living near the Kingston Ferry Terminal, taking the ferry home from when he was born (RL432)

The day John left the hospital as an infant child in 1968 after they had kept him in quarantine for a week as they used to do with babies they drove immediately to the ferry dock and the first thing John ever did that wasn’t just being born and getting in a car was getting on a Washington State Ferry. The ferry lineup back then was the Nisqually, the Klahanie, the San Mateo, the Caloocan (?), and the Leschi, which sounds like John is living in a Richard Hugo poem, and the ferry they took on their way home was most likely the Nisqually.

They lived in a house that overlooked the Kingston Ferry Terminal and for the first 3 years of his life the comings and goings of the Washington State Ferry set the clock of his life. The horn would go off and he would wake up from his nap or it would be time for his snack. Before radar they had a one-long-two-short type of code to indicate what they were doing and as a kid John knew what those all were. Of all the pictures of John as a little kid there was always some ferry in the background because their house was on a cliff and the ferry terminal was right there.

+ Tacoma (RL432)

Merlin knows a rich guy who lives in Gig Harbor, a rich neighborhood down by Tacoma. Tacoma is Seattle’s Oakland. Tacoma is more a timber town while Seattle had a lot more other things going on. It was Weyerhaeuser factory town with a wonderful port. When the Port of Seattle was all fuzzy and full of itself the Port of Tacoma was not fuzzy at all and would take all of their containers, and for ships that had come from freaking China and had been on the ocean for a month and a half, the extra 40 minutes to get to Tacoma was negligible, they just put the transmission in neutral and the boats kept going and all of a sudden the Port of Tacoma was schooling the Port of Seattle. 12 years ago you could buy a 15-room mansion in Tacoma with a ballroom and a waterslide for $250.000.

+ John getting his daughter to her first sleep-away camp (RL432)

The other day he was on a ferry boat and someone had a quarter of a bag of potato chips, and there were no chips for John, he was just staring out the window, looking for an orca, trying to sav (?) his wound, and he took the bag and ate some bottom-of-the-bag potato chip fragments. A few of them is not so bad. It wasn’t actually a ferry boat. His daughter is away at camp and this is the first time she has ever gone to sleep-away camp, which is on the very northern tip on an island in the San Juan Islands archipelago in Northwestern Washington, very close to Canada.

Getting there is increasingly different because it depends on the Washington State ferry system, which is overtaxed right now. It is the largest ferry system in the United States. Merlin just watched an episode of Grey’s Anatomy which takes place in Seattle and there was a terrible accident involving a ferry. One time in the 1980s there was a Punk Rock concert of the band The Accüsed, a wonderful Seattle band, and GBH over in Bremerton and after the show there was a riot on one of the ferry boats that was bringing the Punk Rock crowd back to Seattle. It is known as the [https://mynorthwest.com/1134528/remembering-infamous-kitsap-ferry-riot/ Kitsap Ferry Riot] of 1987 //(see [https://vimeo.com/327380368 animated short film])//.

Merlin saw GBH and  in 1986, right about the same time, and of all the Punk Rock shows he went to it was the scariest one. They had a deeply violent fan base. The Cows scared John pretty badly at a show, he was having thoughts like: ”Oh no, none of us are safe!”, not only those at the show, but nobody.

It used to be a sleepy little region, you put your car on the ferry boat and take it over to the county, drive around, live in a little hut, who cared? Nowadays the line to get on the ferry is 7 hours long, you need a reservation, and it costs money. The ferries go across Puget Sound and they all go East/West, except for one North/South ferry that goes to Victoria Canada and no-one is using that right now. British Columbia is nice! The shoes are cheaper and they have a very famous Canadian cuisine.

Merlin had a First Nations meal in Vancouver. They were sitting on the floor in a lodge type situation. Merlin asked about the drawings on the walls and their waitress replied: ”That is the raven. The raven is a trickster!”, a phrase that has become canonical.

There were some other parents who wanted to coordinate, but the camp didn’t allow that because of COVID and eventually it boiled down to that the only Ferry reservation from Anacortes to Orcas Island that was still available was at 7am, and it is an hour and a half from where John lives to Anacortes and there is no way they were going to get up at 5am to take their sweet-natured little child up to this ferry terminal. Because they are resourceful people there was a lot of talk about hotel rooms and ”Who do you know that has a place on blank?”

John called Joe at Doe Bay //(see RW155)//, but he was sold out and the only thing he could arrange for John is to do a week-long artist’s residency, but that is a lot of work to get a kid to camp. There is a guy who is a listener and who is also a rich person who has a very large house on San Juan island and who said John could use his house any time, all he needed to do is call his helicopter pilot and pick which wing of the house he wanted to stay in, but it felt like calling in too much of a favor.

The San Juan islands are one of the nicest places in America, Oprah has bought a house on them, there is an island owned by the Hemingway family, there are whales, they have wonderful Spot Prawn the size of Chihuahua, they are very delicious. Merlin would find that very upsetting. When he watches his YouTube videos at night to wind down and he is watching Asian street food he has seen prawns that are almost kitten sized. 

John’s daughter’s mother / partner was employing parental anxiety as a method to getting John to find her a vacation home where she could stay for a week. It is one of the defining characteristics of their relationship that John provides access to things and after he has secured access to a thing he becomes fairly superfluous. She was trying to work him to see exactly how deep his rolodex goes.

It would be nice to pivot to a nano-vacation, but John has other things to do right now besides sit in a borrowed house on San Juan island, looking out at the whales. He also doesn’t feel like he deserves a vacation. Merlin feels the same way, and yet he feels compelled to go places.

Until billions of dollars were added very recently the Pacific Northwest, in particular Puget Sound, was largely an aluminum motor-craft economy. A lot of people are out on Puget Sound with a 150 Evinrude //(boat motor)// on an aluminum flat-bottom, the Spot Prawn fisherman, the whale watchers, and all these hee-haws, for them it is basically like having a motorbike.

Some families up there have been running Mosquito Fleet boats for 10 generations and there are all these guys with pilot’s licenses who have a Cessna 172 or 180 that they are just putting around with, and they will get you where you want on the San Juan islands for $70, and it is just a matter of finding a phone book and opening it. 

Let us treat this as though we are from here and rather than line up in a ferry terminal with a bunch of snorks let’s figure this out as though we are not noobs. What they discovered with 2% effort was that while there were people signing up for these 7am ferry boat rides that go online at 7am 2 weeks before the ride and buy 7:05am every single desirable slot has been grabbed up, it is like Bruce Springsteen tickets.

John found a little guide company that will take you out and look for orcas and they would get them to camp for $60 while it is $70 to use the Washington State Ferry now. His boat was just left of the ferry terminal and could maybe carry 10 people and: ”Let’s go!” and he was guiding them along the way, explaining things they saw, they were standing up on the deck with the wind in their hari, and he was driving them right up on the beach at the camp. All the busses were coming in, and it was basically like parachuting naked into a World Series game.

The counselors, who are all resolutely positive young people, went like: ”Whaa? Where did you come from?” As they were walking on the beach they were wondering how parents would get this far down into the camp. They dropped off their daughter with the people and went back on the landing craft, picked some campers up around the top of the island at some tiny little island where a couple had pitched their tent for the weekend and John realized that this was a whole universe.

All these people are signing up to go truck camping in some campground where you are right next door to a Winnebago where they are playing Freedom Rock, but you could have this guy for $70 take you up and put you on an abandoned island. It is Camp Taxi Life! How has John not thought about this earlier? This guy should be on his speed dial! You could literally ask him just to take you somewhere cool. It is problem solved!

The San Juans have felt like getting up there is such a pain in the neck and let’s just forget it exists. John is not going to with a simple Water Taxi Party paper over the idea that he could have found them… but it is a bit of a Jedi mind trick to introduce the new idea that up there is an undiscovered paradise and they could be there all the time. 

John’s daughter was in multiple parades every summer until by the age of 6-7 she actually asked John when their next parade was going to be, and she only thought of parades as things that you are in, riding in the back of a car, waving because she was a young kid during the time when John was on the music council and he was [[[run for office |running for office]]], he was [[[seafair |King Neptune]]] and she started every parade on foot and ended every parade on John’s shoulders. She was very used to waving and throwing candy out to kids and she did not think there was anything unusual about it. 

The first time she watched a parade and wasn’t in it was a new experience. She had been in 40 parades, but she had never sat and watched one, which has its own advantages because things go by. She has been in more parades than most kids had hot meals! 

Arriving by boat at her camp absolutely comports with her experience as the daughter of the guy who makes things like that happen more easily than it might seem. She is not yet at the stage where she goes: ”Dad, can I just ride the bus?”, but when they brought her to Camp Chinook she ran ahead and sat down in a circle with all the other girls in her cabin and as John came there and said: ”Is this Camp Chinook? Hello!” just as his dad would have done.

She fixed him in her gaze and made a sweeping motion with her hands like sweeping dandruff off an imaginary person’s shoulder, like: ”Move along!” - ”Are you waving to me that I should come over and kiss you goodbye or are you telling me to split!” - ”Hit the bricks, buddy!”, which was maybe the first time John got the full: ”Your services are no longer needed here!” and John assuaged his pain by getting back on his personal landing craft like freaking (Douglas) MacArthur and he took a big bit of a spot prawn. He should have worn his King Neptune outfit!

John wants to research how many King Neptunes are still alive. It has been around since 1948 or something. Merlin guesses for 20 maybe because you live pretty hard, with all the trident wounds and such.

+ M*A*S*H, John’s low Kevin Bacon number (RL432)

Meg Ryan was in Top Gun as the wife of one of the other dudes. Another former King Neptune is Tom Skerritt from M*A*S*H and from Alien. He is still alive, the same age as John’s mom, and he has lived a lot. Today MASH is very problematic because it is rapey. They made the shower fall down while Hot Lips was in it, that wasn’t very nice. John watched it again with the dearly departed [[[Friendly Fire]]] podcast and the age difference between John and his younger cohosts, they did not understand the importance of that movie.

You can love MASH, hate MASH, or never have heard of MASH and it would be very interesting to see how this would score among their listenership. Merlin thinks it would be just as interesting to know what movie they most associate Kevin Bacon with. He is a Nazi in the X-Men movie, it could be Animal House. John could not see Kevin Bacon for decades without going: ”All is well!” //(from Animal House)//.

John asks Merlin for his bacon number. He doesn’t know, but he probably knows someone who is just a scant leap away. John has a bacon number because he was in a movie with somebody who was in a movie with Kevin Bacon. There is an actual website called [https://oracleofbacon.org The Orcale of Bacon]. John didn’t realize it until he did an [[[Omnibus]]] about it the other day //(see OM372)// and apparently John was in a movie called Lennon or McCartney and his connection is through Rob Zombie who was in a movie called Super with Kevin Bacon, so John has a very low Bacon number of 2. Merlin is not in there, although he was in a documentary once.

+ Leftovers (RL432)

Merlin bought some Costco Mac & Cheese on a lark because he had also gotten some Costco meats, which is huge and not that costly and he will put them in FoodSaver bags, suck all the air out and freezes them in portion sizes.

Don’t put anything into the fridge if you don’t put it into a desirable package and you don’t have a plan for it, unless you have a John who will suck up all the leftovers. Merlin talked with John Siracusa about this. Merlin doesn’t want to see compost in his crisper because you are just making a pile of garbage for which you pay rent to store. There has to be a plan! You are not going to wake up the next day and want half a chicken thigh. What is the plan? If you have rice, maybe you are going to make Fried Rice the next day, or you put it in a FoodSaver bag and your kid can have a tasty treat in 2 minutes. Sometimes the meal can get undone because it is the wrong kind of rice.

Merlin’s daughter is having a salmon phase and they had salmon the other night, but she didn’t want Teriyaki salmon unless it was with white rice and she didn’t want regular salmon without a wild rice. Merlin feels the same way. Are you going to save a Mastiff’s head of white rice for no reason? Who is that for? Also: Label things clearly even when it is obvious what it is. Don’t trust your brain! Write Filet Mignon 7/21 and you know it is from July.

A practical example is that when he does sous vide and has a leftover of half of a chicken thigh and like some kind of God-damn monster you want to save it. When you put it into the bag, write the current day of the week on the bag because if you write Sunday and you pick it up on Wednesday you will go: ”Holy shit, this has been in here since Sunday?” and if you are not going to eat it then throw it away and you should never have saved it. If you don’t know what Sunday it is from it is too many Sundays and you shouldn’t have saved it.

The same is true for meetings: If your meetings are always running long then you are either scheduling poorly or planning badly.

Merlin made this dressed-up Costco Mac & Cheese and he talks at length about the details of what he has done. It ends up almost like a lasagne and you can portion it up and write Sunday on it. The question is if someone in New York will speak up and tell him that this is not Mac & Cheese anymore. John dresses up everything. A DiGiorno pizza is just a blank canvas.

John knows someone who has just a blank canvas hanging on the wall and it looks great. Not Jason Finn, his canvases are all covered with paint by crazy people, although you don’t say crazy anymore but call them artists. You have to be really good to put a blank canvas on your wall and have it be meaningful and not just look like a blank canvas, but John knows one artist who can do it. Marcel Duchamp called and wants his 1920s back. This is not a pipe! You want to see a pipe? This is a pipe! 



























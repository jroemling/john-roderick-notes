RL456 - The People’s Traffic
2022-03-14

This week, Merlin and John talk about:

**The Problem:** John has some minor electrical problems, referring to his 1976 Suburban that has a few electrical problems.

The show title refers to some countries people having their own traffic rules like driving in 11 lanes on a 3-lane highway.

The audio starts with a voice reciting: ”They fuck you up, your mum and dad. They may not mean to, but they do!” from the poem This Be The Verse by Philip Larkin.

+ Construction noise outside of Merlin’s office (RL456)

When John picks up the call they both laugh heartily. Merlin still has construction noise outside of his office and you hear the beeping of trucks backing up. He tries to maintain a positive attitude about it. Merlin refers to John a long time ago saying that the people of Czechoslovakia don’t like bureaucracy, but bureaucracy is what fixes a hole in the road //(see RL17, it was about missing manhole covers in former Eastern Block countries)//. God makes the rat //(see RL218)//, not the city of San Francisco. 

For one of a nail the lady with the fly was lost. The lady sold he fly in order to buy a nail and then the carpenter sold the hammer in order to buy a flyswatter. Like the guy from Razorfish says: ”People don’t really want a painting on the wall, they want to be happy, and they buy a drill to get a painting on the wall!” Merlin has decided he is going to focus on the positive, as the man says in the song: ”accentuate the positive, eliminate the negatives!” //(lyrics from Ac-cent-tchu-ate the Positive by Johnny Mercer)//. E-liminate should have been a startup name.

We abuse the word Masterpiece, but the road outside of Merlin’s office is truly their masterpiece, the are making the grecian urn that we are going to look at from many sides for many years //(reference to the poem Ode on a Grecian Urn by John Keats)//.

John is a little jittery because his mom has just recently realized that she can get him to do anything if she shows up at his house unannounced with two donuts, he is powerless to resist.

When John was originally hiring a contractor they told him that some contractors would do a bid on the whole project while they prefer to do it on a weekly rate because that gives him better value, and John agreed to that, but then the project goes on and on, another week and another week, because they have no incentive to finish it quickly. John guesses that the people who are doing their masterpiece outside of Merlin’s office are on a weekly rate as well instead of on a project rate.

As a retired project manager Merlin has to imagine that so many steps in that project had to be crazy with meetings over the last 5 years about changes to the street car line where you can’t make changes without a lot of other stuff happening. It looks like they might get a traffic light which he is really excited about. A new addition to it is singing, and the beeping of the trucks might be considered a regional instrument.

+ Snow plowing in Alaska (RL456)

That sound is a very early triggering memory because when he was a very young child at 3 years old they lived in Alaska and that is the sound the snow plows make when they back up, and backing up is a big part of plowing snow. There are a lot of different plows and John tries to explain some of them to Merlin. Every 5th person has a snow plow blade on he front of their truck as a way to earn $50 real fast. Then also as Michael Stipe says: When you put the snow away, where does it go? It will not only snow once, so you have to account in your plan that it keeps happening.

On the big roads they will plow the snow into the middling into a big berm between the lanes and then they come along later with a thing that will scoop up the berm and put it in a dump truck. John and Kevin would go out as teenagers because Kevin’s dad had a Suburban and Kevin got really good at driving along really fast and steer into the giant berm in between the lanes and the truck would launch up into the air and come down high-centered on the berm, sliding like a skateboard on a rail with all 4 tires off the ground. When the real wheels would finally engage they would pop over in a 4-point slide and back over the berm, he was really good at driving. John was 15 and was just along for the right, it was right about the time when his dad would send him out to practice the car //(see RL26)//. 

At the time John and Jim McNeil had the idea to buy a Suburban for $500 and… when Merlin lived in Cincinnati Ohio, in the one house they lived in fro 1976-79 their neighbor had a Suburban, and that thing seemed comically long and large. Now you would call it an SUV, but those terms didn’t exist. You can google 1976 Suburban 4x4 for a picture.

+ John getting his 1979 Suburban detailed in a shady shop (RL456)

John currently has a 1979 Suburban, which looks the same as a 1976, and it has a few electrical problems. John took it to a guy yesterday on Sunday evening to get it detailed, he pulled up out front of a shady place, but it turned out that the place he was looking for was around the back in the basement of the shady place and was three times as shady. The kid was really tall and handsome, but could use some lotion, he was in the 80-95 IQ rating, his name was something like Travis and he promised John’s car would be in showroom condition, but their credit card machine was down and he wanted John to Venmo him, but John didn’t want to do that and asked him to figure it out, so he called some number at the side of his credit card machine.

There were so many red flags and John should have just backed out of there, but he is bad in those situations and didn’t want to let Travis down who even introduced him to his business partner Raul who apparently bullies Travis all the time. Raul said he was in the business since he was 15 years old, implying he was in a chop shop like Adventures in Babysitting.

Merlin uses the mute button to take out the construction noise, but John wants him to leave it in and just let it ride. Now there is a third machine outside, like an asphalt pounder that you push around.

There were all kinds of cars in different states of being chopped up, but no Lamborghinis, just (Pontiac) Fieros, and there was the smell of bondo in the air. When John bought his first car, it was a 1974 Fiat Spider and his friend Jim McNeil who had the plan to buy this Suburban and bash out all the windows including the front window and drive it Demolition Derby style wearing motorcycle helmets, they were going to put netting in all the windows to keep logs from flying in, and they were going to make the bumpers out of literally logs.

Jim was from Arkansas and he explained to John what bondo was and there were a few little rust holes in John’s Fiat that they were going to fix with bondo, but the first thing they had to do was get a disc sander and sand the paint off, and when they did this it was revealed that the paint was holding the car together. This is why John has a very strong feeling every time he smells bond. They eventually did bondo it, although the holes were so big that they had to stuff them with newspaper and chicken wire and bondo over it.

This is why you don’t buy any Vespa from Vietnam, even if you see a really beautiful vintage Vespa from the early 1960s, you need to determine if that Vespa has been to Vietnam, because very few of the 1960s Vespas in the United States have been in the United States the whole time. The average Vespa from Vietnam has a trillion miles on it, like a 1952 Chevrolet that has lived in Havana and where the carburetor is made out of a Pepsi can and all of the hoses have been jerry-rigged so many times that it has turned into a new technology. The have invented a whole new way to do all the functions of a car using found items. 

In Cuba they don’t have Pepsi, so the only Pepsi cans you can make a carburetor out are the ones who float across he oceans and had already been found many years ago. John has seen Vespas that were actually made of cans. Those people are also really good at bondo and paint, and a lot of those countries with tons of Vespas are also the countries where there are two official lanes of traffic and 11 lanes of actual traffic within those two lanes, the people’s traffic, which also means that every vehicle has been hit and dented 100 times, repaired with cans, bondoed and painted so that they are beautiful. 

Right now Travis and Raul are returning John’s 1979 Suburban to showroom condition and John will have to go down there and walk around it and if it isn’t showroom condition he will have to be the one to tell them that. Merlin has seen John negotiate with people, which is extremely upsetting to everyone involved, and he and Sean Nelson have wet each other’s pants while John was negotiating one time. There is a lot of room between low and high showroom condition and John does not expect it to be high showroom condition and he would be incredibly pleased if it were only mid showroom and he will absolutely find it acceptable if it is in low showroom condition.

John has Venmo integrated into his bank app, which is amazing and incredible, Everything is so easy now because technology and people and God and The Bible. John gave Travis the money and at some level there is accountability because the banks in recent years have fully faced up to the fact that money is fake. This happens with Amazon, too: John got a thing, opened it, one of the corners was damaged, and they told him to just keep it and they would refund his money. You can’t depend of them doing that, sometimes they will ask you to repackage it and send it back.

John thinks he can make one phone call to the bank and they will just give him that money back and put a flag on Travis’ account and he wouldn’t be able to use it anymore and will have to ask people to pay in Crypto.

Last night John was driving along and all of a sudden the word Dogecoin popped into his head. He doesn’t even know what that is, but if he had been buying Dogecoin back then he could be paying Travis in Dogecoin and none of it would be real. You can now buy fractional interest in a painting!

In Merlin’s neighborhood someone has been fixing up an 1980s Winnebago on the sidewalk for hours, and it was the first time Merlin heard a vehicle that speaks in a prerecorded voice of a woman in her early 30s, maybe the owner, going: ”The vehicle is backing up! The vehicle is backing up!” 

+ John’s daughter preferring to stay at her mom’s house (RL456)

What John really wants is for the Suburban and Travis to disappear forever. He is at a point in life where he wishes that all of the things that he thinks are worth money, even though he knows that money isn’t real, and the money were just all gone. He also wants the new truck gone and he wants his house gone and he really wants to live in a 1-bedroom apartment. He hit a wall just recently in a very positive way that nothing matters. He probably needs a 2-bedroom apartment so his daughter has a place to keep her crap. She is currently very upfront to him that all her stuff is at mom’s and they could keep a bunch of stuff at his house, and she likes hanging out at his house, but she is going to go home to mom’s if she can.

When John was a kid an lived in two houses he wanted to stay at his mom’s house, even when in 9th grade she turned his room into a guest room and told him to live with his father //(seel RL148)// he wanted to live in the guest room at his mom’s house rather than in the room at his dad’s house that had a cheaper facsimile of what his room would look like. John’s mom took all of his stuff out of the room and decorated it with pictures of flowers, but it still felt more like his room than the room at his dad’s that had video-games and toys and posters. 

He was a misbehaving child and he got kicked out and had to live with his dad 1.5 miles away while she very definitely and purposely turned his bedroom into a guest-room with a quilt on the bed and no art on the walls and all of his clothes in boxes although she never once had a guest in her life even stay one night in her house. For several months John wasn’t allowed to spend the night at her house, he could come for dinner, but then he had to go home to his dad’s.

One night he was there late and everybody went to bed and he snug into the guest-room and spent the night. He tried it again some other time and pretty soon he had snug back in and was sleeping in he guest room. She knew what was happening, but she never said he was welcome back, he just gradually started spending the night at her house more and more and gradually hung up a couple of shirts in the closet over the course of months until eventually he was living there again and his dad didn’t even notice. His room at his mom’s house was his room.

John doesn’t have a television at his house while his mom does. Staying the night at John’s house she would arrive at around 7:30pm where at her mom’s house she would watch Star Trek Rebels while at dad’s house it is time to get out the old UNO cards or sit down and look at a National Geographic together because it is 1955. John made no enticement, but he also remembers how he himself felt so guilty preferring his mom’s house because he loved his dad, but the room in his dad’s house never felt like his room, and he never wanted to put that on his kid.

+ John not wanting to use guilt on his daughter, John’s family having used guilt and shame on him as a kid (RL456)

Guilt was used in John’s family so much, almost entirely by his dad, and he didn’t want any of it and he never has guilted her. It was how John’s dad lived, he had been guilted by his people so badly, it was the way they manipulated him. John never guilted his daughter and that is why she speaks so plainly. He found really cool Peter Max sheets for her room at a crazy vintage store and she finds them amazing and makes a fort out of them, but at 7:30pm she wants to watch Star Trek Rebels and says: ”Good Bye!” and John has a fort made out of Peter Max sheets in one of the rooms and he can’t tear it down because it was made by his darling, but at the same time he was doing archery practice in that room.

John needs to have a room where she can feel like it is her place to do whatever she wants, but he knows he will never going to have a room that feels like the room at her mom’s house because there is no mom there. John still would need to have a 2-bedroom, and one day she will be a teenager and show up, slam the door, and tell him that she will never talk to mom again. This is John’s first kid and his sister’s and mother’s relationship is not a good example to draw on, he never saw a normal mother-daughter relationship because it is so weird how alike they are in the ways that you wish they weren’t, and the ways that they are not alike are so dramatically important for there to be peace in the valley.

They are constantly trying to be friends, but they are really both full of murder and they will lock talons and fall to their death. John’s daughter is the same, but John and her mom are not, and she will try to lock talons and her mother will tell her she doesn’t even have talons and John will tell her he made a fur and also nails. Families are very complicated, and everybody hates dad. Merlin loves that show.

Guilt and shame had such a role in Merlin’s whole life in making him how he is, and from the day John met him he knew Merlin felt guilty and shameful and he suggested he get a shirt that said: ”Governed by fear!” Guilt is such a fucking lazy way to at the least win an argument and to at the most best somebody with potentially long-lasting effects. Guilt is shitty because guilt is lazy. John thinks about this a lot because he was raised with that and it had a profound effect on him.

John wonders how much of it is innate to him and would still have been there if he had been raised by the animals and had been taken by rabbits and raised as a rabbit. We think of guilt as a cultural thing that is shoved into you, but there are so many things that feel like so many emotional things, because emotions are real, and for them to be real they have to exist outside somehow of just what your mom told you. There is talk about trauma being communicated through generations and if that is true, then every single person has it because every single culture has at one point been traumatized. 

Merlin talks about a book about trauma called The Body Keeps The Score //(by Bessel van der Kolk)//

Psychology is still a very young art and it is very compelling to talk about guilt and shame as part of the new part of the brain that we transmit to each other through action and that we can change through behavior, but he increasingly thinks that they are both deep in the old part, they are like breathing, and they are going to find a thing to attach themselves to. It gets you into the psychologist’s office, it gets you to buy he book about it, and it tells you that you can cure your way out of it, but it is more than that and John has been working on it for decades and it is immutable and it is in some more than in others, there are people without guilt and shame and that is not because they were raised perfectly, they just don’t have it.

Like so many mental illnesses the paradox of it is that it is in its very nature to look for reasons for it, and you cannot see it using the same mind that is full of it. Like John used to say about depression: When you are depressed you feel like you deserve to be depressed and you don’t seek a solution because being depressed feels right and legitimate, and with guilt and shame it is like you have guilt and shame and that is the same mind you are using to figure out what they are and how to solve them and your psychologist will ask you what your mother used to say to you, but it is all just a dance about a thing that is more deeply embedded and fills an important role in some of us. Maybe it is not harnessable or curable. 

+ Feeling guilty for not being able to help, even though it is not at all your fault (RL456)

When Merlin’s kid was born he wished that she was healthy, but also that she would like books and music because those things have made his life good. He also tried to promise himself to try to not pass anxieties to his kid, particular those that were very damaging to him, which is its own problem because now you are feeling anxious about being anxious and passing on the anxieties to your kid. John’s mom and dad worked very hard to protect him from things that tormented them, but those things didn’t even exist in his life and a lot of the work they did created a whole new situation in his life.

John will get sad sometimes and he will sit in a chair and stare at a spot on the wall and he is just sad, and there are a lot of people who when their kid was around they would try to never show that they were sad, and that always felt wrong, dishonest, and dangerous to John because your kid can tell and if you are pretending not to be sad and they can tell something is wrong and also that you are being false, it is just bad! John has explained it to her, that it is not about her or anybody else. She will sometimes come into the room without John noticing and put her hand on his head and just stand there, calling it ”giving him care” and John will become aware of her, but not say anything, and then she will go away again.

John has asked her how she feels about that and it pushes a button of guilt in her, even though there is no guilt, but she feels guilt because she can’t help and that is so basic that it feels prehistoric. It is a basic fundamental feeling, that she wishes she could help but can’t and therefore she is responsible. She is not a ”let me solve everything” person either, but it is her animal brain. That is the guilt John feels a lot of the time, and most of the guilt he carries around isn’t even his dad’s guilt anymore, he just feels guilty because he can’t make things better for people, and he can less so all the time, weirdly. That guilt just populates and nobody is putting it on him and he doesn’t even hear his father’s voice anymore. It just feels like an element.















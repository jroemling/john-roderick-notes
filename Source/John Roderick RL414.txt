RL414 - Groomed Into Insanity
2021-02-22

This week, Merlin and John talk about:

**The Problem:** Would you bring me some tape, referring to John asking for some tape in order to tape something in front of the in-flight entertainment screen because it was impossible to turn it off

The show title refers to the modern world insisting that you look like an insane person.

+ John having trouble with his computer (RL414)

John’s computer just went on a journey of its own, but John has entrusted this machine with making the right technical decisions. John had installed macOS Catalina, as they discussed last week, it was very stable since then, but when he restarted his computer to fix some audio problems prior to recording the show all of a sudden a screen popped up that said it was installing Catalina and it was going to take 30 minutes. He already had Catalina, it had shown him the picture of the jagged island!

John just sat there and watched it do its thing and now it says: ”Welcome to macOS Catalina! Take a quick tour to learn about great new features” and the options are ”Show” or ”Later”, there is no ”No” option. Merlin hates that! He doesn’t want to do it later. His microwave oven tells him to enjoy his meal and he is tempted to put black tape over the LED, he doesn’t like fake familiarity with a machine that says things like it was a person and on top of it all it doesn’t even give you an option.

+ Travel hacks, in-flight entertainment screens (RL414)

10-15 years ago John was sitting in an airplane seat with some screen in front of him that didn’t have an off-button, like the screens in the New York City taxi cabs that for a long time didn’t turn off. He called the waitress in he sky and told her that it was getting really inside his brain because it kept moving slightly and kept flashing. She was uncomprehending and didn’t know what John wanted her to do and as he explained it she told him that there was no way to turn this off, so he asked for some tape and taped a page from the in-flight magazine over it.

Merlin recommends to always travel with a roll of gaffer’s tape because that allows you to also cover up the LED on the fire alarm and the peep hole in the door. Merlin nowadays also always brings a bulldog clip to make sure that the curtains close up good. John spent 40 minutes trying to monkey with the hotel room curtains to keep the light out. They make perfect black-out curtains that are perfectly black except for one problem: The are very rigid, and they are probably covered with Bavarian cum because let’s be honest: A German has cum on everything in that room.

Even though Merlin doesn’t travel anymore he still has a whole box of things that have to go with him when he travels and those are two of the things. [[[Keeping a small bag packed]]]! Merlin always did like the experience of Virgin, even after it was acquired. They do give you a screen and you have the ability to turn the screen down or turn it off unless there is an announcement, but then it pauses whatever is on the screen.

Merlin has a lot of crushes on ladies in videos, like the Delta lady in the 1990s. Peaches and cream! You could tell that she was a naughty waitress, she was something else! Then there was the dancing lady and the dancing people and Merlin has a crush on the acrobat woman. When it is over and you just want to stare at the airplane on a map you have to sit through 3 compulsory commercials that you can’t turn off, so as soon as he will sit down Merlin will put something over the screen.

You can use the rest of the tape to make a hat that is going to keep the UV-rays out. The modern world insists that you look like an insane person. Merlin has been groomed into insanity, you can’t help it! Merlin still tapes over the peep hole because he does not want Anthony Kiedis //(singer of the Red Hot Chili Peppers)// giving it away in his room //(reference to a music video where somebody could look into the peep hole from the outside)//.

Merlin is trying to diminish sources of light pollution and he has a whole thing with hotel rooms. He puts away all the literature and the plants, it all goes into a closet, everything with writing on it goes into a drawer, but also sources of light, hence the curtains and the bulldog clip. He also tries to minimize contact with the bed spread because he has seen enough stuff where Gordon Ramsay pulls out the blue light thing and makes you look at all the cum and urine all over it. Don’t watch Hotel Hell, although it is very entertaining!

John has stayed in some terrible hotels and he doesn’t want to know what is on any of that! Merlin does want to minimize noise and light and he covers the red blinky light on the smoke alarm, he smashes up a couple of towels really good to get the crack under the door where they put your bill and there is a half an inch of light coming under the door. Merlin is not a crank, just to be clear! He doesn’t bring a white noise machine because these days he has noise-cancelling headphones. He is going to take a lot of drugs to go to sleep anyway and he CLEPs out of that pretty good.

John’s sister carries a little bluetooth speaker that is the size of an apple, it is USB-MP3-C3PO and she has some brown noise thing. When Merlin’s baby was a baby they did travel with a little cool square-ish bluetooth speaker that everybody had for a while and that Adam gave him, he did an ad for it, it was one of his first ones, the very first being an ad for the Twitter app. The speaker is called chip drop blue boy //(probably not)//.

After much testing they found out that Merlin’s daughter is helped a lot by the sound of the dryer and they had an iPod Mini, the one that is like a tiny thing with a clip that looks like a little button, that was dedicated to just the dryer sound and when they travelled they could play the dryer sound for her.

+ Merlin being comfortable with clutter, active working areas vs storage areas (RL414)

John imagines Merlin using cleaning supplies in his office to keep daemon dogs at bay, wipe them off and bleach them away, but the user experience of the office is not that it is sparkling clean because Merlin is comfortable with clutter and the office has a lot of things in it. Watching the HBO series last night Merlin saw Mia Farrow’s house that she has lived in for 40 years, this incredibly rustic, small cabinesque thing in Connecticut that it delightful chock-a-block with clutter, where she and her 14 kids mostly lived. It is such a nice environment! Merlin loves that shit in a really dark cabin, it reminds him of his mother-in-law’s house before she passed, full of decades of meaningful things like the doll-house she made by hand for her daughter Dylan.

Merlin is very much comfortable with clutter, he is not at all a tidy person. Some people can’t work if their desk isn’t clean, other people can’t use a public restroom, and none of that stuff goes for Merlin but he does have an obsession where he doesn’t want shit all over the floor, which he calls under-foot-clutter. A brown LEGO is a good example or an HDMI cable. He doesn’t want the Roomba to eat it and he also functions better with having a clear path between places. A lot of his cleaning involves the bathroom, he likes moping the floor, he likes scrubbing out the toilet a little bit, he likes doing the dishes and putting them away.

He puts extra water into the ice maker, he has a whole environment here. He will put a bit of Pine-Sol in the bucket and a little bit of boiling water and he will give that a swish before going home on Fridays, that is part of his end-of-week wind-down ritual. John loves picturing Merlin with a little apron on, tending to his toilet ablutions. Sometimes you just need to control a small area, which could be your seat on the airplane, it is why a lot of people get weird haircuts after they break up, why people decide gong on a diet, you get a little bit of control. Merlin’s desk right now is pretty crazy, but it is operational and functional for what he needs to do.

In other areas he tries to just occasionally reassess. For example he needs a battery recharging area, a dedicated area for battery-type things, and these kinds of projects are a huge part of his life. There is a phrase he has coined on his show with John Siracusa: Active Working Area. The canonical example is: If he is making a rib roast that he has been cooking for hours and hours, and he likes his food piping hot and everything has to come out piping hot at the same time, there is an order of operations. Don’t do the broccoli too early, that is yucky! The titular part of the meal is the roast and when he is ready to slice it up he got an area with the big cutting block, and that is the last thing he does before he puts things on the plate. You can put the glass from your room anywhere in world except on the cutting block because it is an active working area and you don’t put things there that don’t belong there.

A storage area on the other hand is not an active working area and in between you can have liminal areas, this is all part of Merlin’s Weltanschauung. When something has turned into a storage area that should be an active working area, that is when he knows he has to refactor things. Now he needs an area to put batteries because he is all-in on rechargeable batteries, which has been huge. He got from Amazon the Battery Daddy, a case that holds hundreds of batteries and that also has a little tester. If you have rechargeable batteries you will find yourself frequently recharging them and you need a station for them, so Merlin needed to turn a clutter area into an active working area and nothing goes there except for battery charging things.

+ John’s remote-controlled truck exploding while charging (RL414)

A couple of days ago John had a very bad battery event. Last Christmas he gave Merlin his heart, but he also bought three remote-controlled trucks and over the past year they have trucked around, they are super-fun and not that expensive, John had a black 1978 Trans Am Firebird and he doesn’t remember ever giving it away, it didn’t get stomped on, but it got sold by someone and it is probably in a landfill like everything else and [[[Supertrain]]] will find it. They had their trucks plugged in for charging and all of a sudden one of them exploded in a huge mushroom cloud of smoke and caught on fire.

He opened the sliding door and put it out in the rain, but it continued smoking for another 45 minutes. John’s daughter was traumatized because she was the one who plugged it in last and she noticed that the charge light that is red when it needs to be charged was flashing between red and green, and she didn’t know what that meant, but she didn’t say or do anything, she just plugged it in, which is what John also would have done. She was apologizing and super-mortified that she had blown up the truck, but John ensured her that it was not her fault.

Apparently these batteries can be overcharged and that is probably why they were checking your cell phones when you went on airplanes for a while and they still ask John about his suitcase that can charge his phone. The airline rules are not very consistent about all of that. You shouldn’t plug in you remote-controlled car in an airplane and charge it, that would be terrible because that is the kind of thing that you plug in and leave. It would lead to a scene like in Tora Tora Tora except it would be a 737. Merlin loves that movie, it is the first 3-hour movie he has watched.

+ Dan being mad via text because of the water outage in Texas (RL414)

John was texting with Dan Benjamin, he is having quite a time right now in Texas //(because of the snow, power outages and no running water)// and he is extremely mad. He was on Rachel Maddow //(see [https://youtu.be/ex5THHBSbsk clip here)// and his name was on the screen. If you ran all of his texts to John through a text aggregator 70-80% of them are hyper-friendly, like ”Hey buddy!”, ”Wow, fun!”, really happy, like a picture of him with a hat on or something interesting he saw, then there are about 6% of texts that are strictly informational, and about 60% of that 6% are informational where the information is a little incorrect, but then there are 4% of texts where he is mad about something. John makes people mad, he makes Merlin mad sometimes, and he likes to feel like he is to blame for things that he is not even related to.

Dan told John that he couldn’t record this week because he didn’t have any power and any water. John is not on the Internet right now and didn’t even know that this was happening, so he looked it up and asked ”You don’t have any water?” - ”What part of ’no water’ isn’t clear?” and this is not the tone that Dan normally takes with John, really flipping it right back at him, and John could tell that he was huddled in a house with no power, licking condensation off the pipes, like Cersei, when the lady from Ted Lasso throws the water at her and she licks it off the floor and cries. Dan was probably shaving his head, too, and walked naked through the street. ”Shame, shame!” //(reference to Game of Thrones where Hannah Waddingham, Rebecca from Ted Lasso, plays Septa Unella)//.

+ Star Trek II: The Wrath of Khan, Kirstie Alley, actors playing much older characters (RL414)

Merlin looks actors up all the time and he looks how tall people are, which John does as well. Actors are always so short. Two nights ago John was watching Star Trek II: The Wrath of Khan, which is actually a very good movie. The hands on the window is [https://www.amazon.ca/Star-Trek-II-Ornament-Hallmark/dp/B010KFYL36 available as a Hallmark Christmas ornament. It is also the one with the worm in the ear and John told his daughter to put the blanket over her head for the next minute, and she knows when he says that to just do it.

What really stands out about this movie, in the vain of the peaches & cream lady from Delta Airlines is young Kirstie Alley as the Vulcan lady. It is also the one with Kobayashi Maru in the beginning and Merlin fucking loves that, it is so important in his life. The idea of being given an impossible situation that is not a test of leadership but a test of character means a lot to a man in his 50s! It is not that Kirstie Alley is tearing up the screen with her acting because she is playing a Vulcan and all she does is raise her one eyebrow every once in a while. John’s daughter also wanted to know why she has regular eyebrows and not Vulcan eyebrows, but he didn’t have a good answer.

Kirstie Alley is very peaches & cream for John in that movie because he likes a Vulcan girl or Romulans. Merlin likes a lot of Star Trek ladies like Green Lady who was played by Batgirl //(Yvonne Craig)//, Nurse (Christine) Chapel //(Majel Barrett)//, the woman with the red dress with the woven-together hair //(Janice Rand played by Grace Lee Whitney)//. John and his daughter haven’t gotten into the TV show yet and she probably doesn’t understand yet that the movies are derived from a TV show.

Last night at dinner John’s daughter and her mother had a long conversation about how Spock died and how they are going to keep making Star Trek movies if Spock is dead and they were looking at John for answers, but he was not going to explain that there was a Star Trek III. In those situations Merlin will say: ”You are watching the same movie I am!” They watched the extremely good I Care A Lot on Netflix with Rosamund Pike whom he loves, and his daughter and wife have watched enough Law & Order that within 30 seconds they are guessing what the twist of the movie is going to be.

What Merlin loves with Star Trek movies, appart from Bones’ //(Leonard McCoy)// outfits is the moment, especially in the early films, when you first see the character, when Kirk walks through the door at the end of that scene and then Bones is laying on the side, asking: ”What do you think of my performance?” - ”I am not an acting critic!”

The problem with Star Trek II is that Kirk’s whole character arc //(John accidentally says Kurtz, like in Colonel Walter E. Kurtz from Apocalypse Now and Merlin finds that very funny)// and is that he is old and he has lost it and ”What is the reason for anything anymore?” John immediately googled how old William Shattner was when they made this movie and he was exactly John’s age. The whole point of the film is: ”What is Captain Kirk going to do now that he is so old that he is irrelevant?” and he was 52 years old!

Merlin looked up how old John Carroll O’Connor was when they started All In The Family //(with Archie Bunker)//. He was in his 40s, but he is Merlin’s idea of a 60 year old guy. Also, in his head Captain Kangaroo is 70 years old.

The problem is that Nevermind //(album by Nirvana)// was recorded 30 years ago this month. In 1985 John was 30 years away from the albums of 1955, which is what Merlin calls a chron-analogy. Peggy Sue //(by Buddy Holly)// was not even out in 1955. When Mia Farrow said she had been living in her house for 40 years and Merlin realized that was 1982, that did not feel good. It was prime Hall & Oates years and now Merlin has to live with that.

+ Tidy vs clean (RL414)

John’s question about Merlin’s active working area: The home owner, his daughter’s mother / partner was raised by nice Bellingham hippies, and she wants things to be tidy, but does not care if they are clean, whereas John does not care about tidy, but really cares about clean. For her the house needs to look very tidy with everything put away and things in their place, and her daughter takes after her. They will take some peanut butter out of the jar with a knife, they will spread it on a piece of toast and then they will put the knife down on the counter and walk away. Is the idea that you come back and use it again?

Merlin says that if you know in your heart that you are not going to eat the leftovers, don’t waste a bag and put it in the fridge, you are just storing garbage! John eats everything. The other day Merlin asked his daughter if she was going to save the piscetti [sic] sauce and eat it tomorrow, but then nobody ate it tomorrow and he salvaged out the sausage bits and put it in his mac & cheese. His kid was in a sausage mood and he made her some fancy Kielbasa, even nicer than Hill Shire farm, Polish sausage, but we don’t say that anymore, but Central European tube foods.

The mac & cheese is the greatest mac & cheese that Merlin’s wife has ever made, she tweaked the recipe with a little less milk and no nutmeg and it was perfect. Merlin reheated the sausages a little bit int he microwave and some of the red sausage slides off and he covered that with mac & cheese, put that in the microwave and heated that up and not he got himself a treat with the sausage on the bottom and the mac on top. It is like the yoghurt where the fruit is the bottom, like reverse Danone.

John runs everything through the dishwasher, even if it doesn’t need it. If somebody looks at something funny, he will put it in the dishwasher. On the other hand he will always keep a water glass next to the sink because until you can see finger prints on the glass it is mostly clean if you are just drinking water. But if you use something like a knife it ought to go through the dishwasher, which is something that comes from working in restaurants where you had big industrial dishwashers. Merlin appreciates the beauty of John’s system.

In this house, on one side of the gauze there is someone who is straightening magazines so that they are all perpendicular and on the other side there is cottage cheese smeared on the counter and left to be there for 1000 years. John is always walking around with a wet rag, wiping everything down, because you can take any space in this house that looked clean and if he goes over it with a wet rag he will find a seemingly invisible, but extremely sticky layer of what was once Sprite.

Merlin cooks with a lot of meats and oil splattering around, and he doesn’t have an actual hood, so he gets a lot of in-the-air particulate greasy stuff. Furthermore San Francisco it is very dusty town and dust is mostly skin //(which is not true)//. In Seattle it rains all the time and rains it all down. Like crawdads: They are tiny little things, but if you look at them under a microscope they look like lobsters.

Merlin’s wife is very clean, but it is complicated. She is better than Merlin in every way, first of all. They both play similar roles to what John was describing. She likes things tidy and Merlin likes things optimized for efficiency. In addition to the Active Working Area he wants to be able to do almost anything in the physical world with one hand and he shouldn’t have to move anything. If he needs to grab a regular use thing he shouldn’t have to move something and he shouldn’t need two hands, like when he has to lift plates to get to another plate.

Merlin’s biggest breakthrough of the last year is realizing that one reason their relationship works is to acknowledge that when there is friction it is because she is all about forest and Merlin is all about trees. Everything made so much more sense! He has ADHD and anxiety, he is obsessed with all the little bits and pieces that he thinks need to be a certain way, while she just sweeps in and takes care of shit and it will be clean in minutes. She does the dishes in 3 minutes and she doesn’t fret about it and she is great at that, while Merlin is the karma-suck and the risk-assessor and the person who says: ”Yeah, but if we pack it this way...”

It is a nice contrast between them, but it leads to things like when you put a folded-up grocery bag into the grocery bag of grocery bags, could we do it so the flaps always go the same way so that you can do things with one hand and without moving anything? Imagine the beauty of a grocery bag that contains 10 beautifully folded up grocery bags with the flap going the same way! You could grab any grocery bag in that pile sigh unseen in the dark. If even one of those is wrong you get an explosion of bags and now you are cleaning up bags.

Sometimes they have friction about those things, especially because he is a tinkerer and he is all about the trees and that is why a lot of his underfoot clutter is his HDMI cables and brown LEGO. Then he will put it in a box and store it and they will ask where the good scissors are and Dad freaked out, grabbed a banker’s box and cleared off an entire surface because it was an active working area.

It is like John’s dad’s old coffee table, the magic table that contains last week’s Time Magazine, a phone bill from 6 months ago and his passport. You can’t mix those things! There are people who lost a MacBook Air because they put it in a pile of magazines. That starts with not putting your computer in a pile. John is still going through banker’s boxes and he will find a Bearer Bond for $100.000 right next to 35 receipts for paint. ”Great, dad!” John just wishes he will find a $100.000 Bearer Bond, but he just finds insane stuff.

+ John’s locally sourced bowls that only stack up in a certain way (RL414)

Over the last year John decided that he was going to transition from having all of his dishes and bowls and cups be random things that he found at thrift stores, all of his bowls, plates and cups are different from one another, and rather than solve that problem when transitioning to a new home by getting a set of dishes he is going to go the other direction and he is only going to buy plates and bowls and cups and saucers from local potters that are hand-thrown on spinning wheels and then given a lava-dipped glaze. They are all made by craftspeople who live on Vashon Island who also make their own cheese and you can buy those at the same farmer’s market.

His plates will not adhere to any kind of geometry other than the whimsical sense of what a circle is as understood by these children of the forest. In Japan they call it Wabi-sabi where beauty derives from the imperfections. John is a bowl-eater, if you can put it in a bowl he will eat it out of a bowl, he will even cut stuff with a knife in a bowl, but he likes his [[[child-speak |scabetti]]] [sic] in a bowl. The food rolls back down to the bottom. On a plate you can chase a pea all around all day!

The problem is now that John has 6-10 bowls and there is really only one way you can stack them. If they get off in any way it is a Jenga that can’t support its own weight. Everything has to go in either perfectly or pure chaos, he could almost put stickers 1, 2, 3, 4, and 5 on the bowls because he figured out which set of ovals is the one where you could put these things together so they can support their own weight. It is making him crazy, he is thinking about it right now although it is a mile from here behind a locked door in a cupboard.

He already had problems with his thrift-store plates and bowls, stacking in a way that felt precarious. By using these bowls he is aspiring to an aesthetic that he does not fully comprehend and he does not what it is to live with these bowls, but he is choosing the bowls first and then lets the lifestyle follow. Let the bowls be your guide! He does not think that he has OCD, he is not even on that spectrum, he just things that there are certain ways that things should be. He has a close friend where if she walks into a room and the tiles on the floor are different colors she has to go from one side of the room to the other side in a certain way to account for it.

John is not like that, but he is fighting something. It is the same thing like never taking the same route across town twice or never ordering the same thing on the Mexican food menu, even though there are only 5 ingredients, and now he has these bowls and: ”Who am I?” Merlin thinks that John believes that the introduction of friction is what makes one feel truly alive. Keep paddling! Maybe that is because he doesn’t float?

+ Keeping empty peanut butter or jelly jars (RL414)

John introduced the concept into this house that you don’t throw away peanut butter or jelly jars because they are very useful jars to keep thumb tacks in. If you have a bunch of Apple USB wall warts, you can put them in a peanut butter jar! You can put pens in a scabetti [sic] sauce jar and turn it into a pen jar. Why would you throw that out? This concept had formerly never occurred to anyone at the house and those things went immediately into the recycling. You could even put a jam jar into a peanut butter jar. Merlin has a collection of good boxes that he nests inside other boxes, which is very satisfying.

The problem is that those jars start stacking up and at one point you have enough of them to last a lifetime and at one point they went into the cupboard with the drinking glasses and no-one except John spends an hour getting the last bit of adhesive glue and ripped label off of the things, so the jars go into the dishwasher and then right into the cupboard with their shredded labels and glue still on them like tattered curtains in the wind and now they have filled up an entire shelf of the cupboard.

Nobody is using them as cups and they are supposed to be kept as tools in the basement. One or three rusty nails are not worth a jar, you should probably throw them away, but if you have 16 rusty nails you have to keep those! Merlin has a thing in his office of metal things he found in the street, and now whenever he finds a metal thing he knows exactly where it goes. John has now started drinking out of jam jars just because they are up there and somebody has to do it, otherwise they would be going to waste! The problem is just that it is not as good, they are not glasses. He is not having an Arnold Palmer at a Cracker Barrel! 



















RL380 - Impossible Me
2020-04-27

This week, Merlin and John are talking about: 

**The Problem:** John is trying not to paddle alone in life, referring to him buying two kayaks so he didn’t have to paddle alone.

The show title refers to John constantly comparing himself to the Impossible Me, the person he could be if he were perfect, and beating himself up that he is not good enough.

The audio starts with some sad R2-D2 sounds from Star Wars.

+ John accidentally answering Skype with the camera on (RL380)

Merlin starts laughing out loud saying: ”Is that how we are going to do it?” because John answered the phone with his camera on and he is wearing a very colorful shirt. Merlin turned on his camera as well. They have never looked at each other, although had tried one time accidentally in his first episode because he didn’t know how to Skype. Merlin got three X-Men posters in the background and one listener sent him a poster of Elektra //(from the book Elektra, Assassin)// signed by Bill Sienkiewicz who is one of the greats. He also had a poster from Sex Criminals that was done by Howard Chaykin. 

John also had a poster tube on hand and when he wanted to show it Merlin said what he saw was not what he expected the room to look like. ”This is bad!” There is some stuff because this is a guest room / play room, it is not John’s space. It is a big room and even has a life-size inflatable R2-D2. John has a Roland keyboard, some guitars.

John had answered Skype by hitting the space bar and then it will default to a video call. Merlin thought maybe Skype grabbed input focus while John was typing somewhere else.

+ John’s tweet about R2-D2 going viral (RL380)

> ”My daughter just asked, “R2-D2 was there for the whole story and never had his memory wiped so why didn’t he just tell Luke that Vader was his father at some point?” I just sat there dumbfounded until she shrugged and walked away.” — [https://twitter.com/johnroderick/status/1253850623491010563 John Roderick]
> 
> ”I read this tweet, thought long and hard... then just shrugged and walked away. #NoPlausibleAnswer” — [https://twitter.com/HamillHimself/status/1254164333459734528 Mark Hamill]

John’s tweet about his daughter asking about Star Wars got over 200k faves and he said he got 4000 replies on it, one of them from Mark Hamill, the actor of Luke Skywalker. Merlin joined Twitter in February of 2007 but that it his second and he had an original account before it was public. John joined in November of 2008 and in all that time he had been churning out some pretty good content, at one point he even had a book of his tweets that he would sell at his Rock shows. Merlin remembers specifically seeing a table with it at Bimbo’s (365 Club San Francisco).

John also once got an award from the Seattle Weekly for best tweet of 2010 //(see [[[Awards]]])// He even got a plaque for it! John has been on Twitter and has invested in his content, he got a lot of miles on those tires. Back then there was a countable celebrities in Seattle who were on Twitter and the fact that John was on Twitter was novel. The other Rock musicians were wondering what John was doing over there, and among that class of people he almost certainly had the best tweet because he was really good at Twitter.

It was during the period when John thought that all tweets //had// to be 140 characters long. Merlin also spend hours ever day making his tweets exactly 140 characters. John got yelled at because in order to retweet somebody you had to put ”RT” in front of it and he didn’t leave people any room to RT him. Merlin got often criticized for having more people following him than he followed. John got that two. Those were the salad days! Merlin was there, a lot of John’s friends were there, and John always had a lot fewer followers, but that was fine because he is used to it, he is always going to hit that ceiling where people don’t like him any more.

John sees this all the time: Somebody says a tweet, you look at it, ”Ah, it is a good tweet!” and you see that it went crazy and got 50.000 likes, but when you look at the person’s account they have 2000 followers and you wonder what nerve they stroke and what they did to have their tweet go viral. Over the years John would many times write a tweet, push send, and think: ”This one!” while rubbing his hands because this one was such a good tweet. Three hours later it would have 80 faves.

Ken Jennings of course has a lot more followers than John and he will fart in a glass and it will get 5000 likes. John is always comedy-raging at him which Ken loves and he will apologize for putting up his laundry list and getting 8000 new followers from that.

John will all the time give his tweets a little kiss like a white dove, like: ”Go! Fly! Be free!” and he had a few that did a little bit of traffic, but the other day his daughter came in and said one of her things that she says as she is walking through the room and John thought that was cute and put it into a kids-say-the-darnedest-things style tweet about R2-D2 and now as of this recording it had 206.000 faves, 4000 replies,  and 23.700 retweets. Merlin doesn’t like the number of replies because no matter what John will say from now on he will be a dingeling sink and they will find him and release their heat.

All of John’s friends knew more than John knew it that there are so many theories about R2-D2! We also know that Luke is one of the few people who appears to speak with R2-D2. John hasn’t read all 4000 replies, but he has read a lot of them, probably because Marlo came sauntering in every once in a while, asking how her tweet is doing and ask him to pull up some of those replies that are like: ”She is a brilliant little girl!” She also loves all the memes people are replying with. He got schooled that because R2-D2 is an Astromech he is just going according to his programming and he is not programmed to reveal or image or think for itself.

On the other hand, R2-D2 is coming to life when Luke comes back. They have a special relationship and go on adventures together to the Dagobar system. You can’t tell John that an Astromech can have disappointment, but R2-D2 has disappointment and makes a little noise every time he gets left behind. Second of all, so many ”Turns Out” neckbeards out there want to tell John that the droids have their memories wiped by Bail Organa at the end of Episode III, but John watched the footage now elevnteen thousand times and he has specifically the protocol droid wiped and does not mentioned the Astromech. You can take that theory and stick it right in your ear!

The theories that are interesting are the ones where R2 is actually the puppet master of the whole thing. He is there from the beginning, knows everything, and is playing a super-long game along with his partner in crime Chewbacca. They are rebel leaders who recognize that Luke is a hot-headed whiny ding-dong, Han is a callow rogue, but those two will get R2 and Chewbacca into all the spots they need to lay the groundwork for their big plan. A lot of this is just people having watched those movies too many times.

The latest reply that John got 30 seconds ago says: ”He’s a more simplistic droid than C3PO. Maybe he can only respond, and not engage in critical thinking like C3PO seems to. I.e. Luke would have to ask R2 about his dad. Since Obi Wan told him his dad is dead, he has no reason to even assume R2 knew his dad.” — [https://twitter.com/Ray_Scullz/status/1254826200096215047?s=20 @Ray_Scullz] But isn’t the thing that makes C3PO and R2’s relationship funny the fact that C3PO believes that he is smarter than R2, but we know it isn’t true?

It is a classic Keith Johnstone Impro System thing, a difference in status, like Jeeves and Wooster or Hong Kong Phooey and his dog: ”Number One Superguy Hong Kong Phooey, quicker than the human eye”, but it turns out that it is the dog running around behind him that is cleaning up everything while Hong Kong Phooey gets to take all the credit. Sometimes it is funny when you don’t know what somebody is saying, like with Bob Newheart you only hear one side of the phone call. It is about status and limited amounts of information and Merlin has never gotten the sense that R2-D2 is just a sophisticated echo device that you talk to.

It is not a Dracula situation either where he is not allowed to mention it unless you bring it up. People are trying to use some Asimov-ian rules about robot programming, but in a galaxy far far away a long time ago, really? Merlin really hopes that John Siracusa doesn’t hear what he says. They have to get to Tatooine and R2 got ways to wangle that because C3PO thinks he is so much smarter, which might be part of his programming because he was built and programmed by Anakin. Also, John learned that the reason that Obi-Wan went to Tatooine with the young Skywalker is that Dark Vader [sic] hated it so much there, having grown up there, that it was the last place he would look for anything. That is why they didn’t even bother to change his name from Skywalker.

Merlin is not sure he would place those twins on the same planet where Jabba the Hutt lives and give one of them to a senator who is going to make a lot of public appearance and it would be strange that he has an adopted daughter now who nobody knows who that is. John questions to take Luke back to Dark Vader’s home planet.

One way John tried to weed through the tweets is that he started to only read the verifieds. It is frustrating because the verifieds were supposed to be the ones who would take one of John’s solid gold tweets and send it out into the world, which probably happened here. A tweet gets big not when it is liked a lot by your followers, but when it gets retweeted by somebody whom lots of people liked. Once it got to Mark Hamill and he commented on it that he had to think about it all afternoon but also could not answer this question and shrugged his shoulders and walked away that of course attracted all kinds of people.

Jordan Kervind (?), Merlin’s good friend from San Francisco with the Japanese soaking tub from M.C. Hammer’s 40th birthday party just faved it. Nathan Schneider, universal health care advocate, public education proponent, organizer, campaign manager, political consultant and former House candidate, says: ”I think R2-D2 felt a sense of power and superiority by withholding information from everyone.” — [https://twitter.com/NSchneiderAZ13/status/1254631509102702593 NSchneiderAZ13]. Tork Mason, USA Today Wisconsin photo journalist:  ”I mean the actual answer is it was retconned. But technically, he never owned them.” — [https://twitter.com/CoachMason33/status/1254629028721577991 @CoachMason33].

Claudia Aponte from Puerto Rico who covers Brooklyn for The City, NY, quoted the tweet and said: ”This is fucked up” [https://twitter.com/clauirizarry/status/1254579370049654788 @clauirizarry]. [https://twitter.com/siracusa John Siracusa] chimed in and is sad that [https://twitter.com/siracusa/status/1254561387680075777 his tweet] is not as popular with only 1400 likes. John would have considered any tweet with 1400 likes a viral tweet before today. Dan McLaughlin, a senior writer for NRO: ”Go back and watch how Obi-Wan looks (eyes darting in the direction of R2, who had just beeped madly when he pulled out the old lightsaber, while 3PO insisted on being shut down during the conversation) when Luke asks who killed his father.” — [https://twitter.com/baseballcrank/status/1254528112550981633 @baseballcrank]. This might have been in the hut in the first one where he says: ”If you don’t mind, I am going to shut down!” Merlin thought that was because whoever wrote that scene didn’t want to do C3PO things and let us focus on the three more important characters.

John has learned a lot. He has learned that in 12 of being on Twitter his legacy is going to be a ”kids say the darnedest things about R2” tweet, he has learned that his daughter is pretty unimpressed with 207.000 likes because she has no context and doesn’t know how daddy slaves away for 30 likes at a time. Also, it adds to her sense that everywhere she goes the door is just open, which was also the problem with John as a kid: He would chill along behind his dad and the doors were all open and there was always shrimp cocktail there, so this must be life! It is going to be worse for John’s daughter because in her world when she gets 207.000 likes she will get on stage with Blondie. How the fuck is she going to have a normal life? Merlin would love to be on stage with Blondie!

Merlin’s daughter loves podcasts, mostly the McElroys, and she told him yesterday that she was looking at new podcasts and it had recommended to her three of his shows. He told her to close that tab and to not look at daddy because daddy doesn’t want to be noticed anymore. He wants to reach the micro-audience of his peers and he wants everyone else to not notice him, please!

This is still a lot better than it could have been because John could have said an angry non-thought-through political tweet. The Erma Bombeck material can really fly and Merlin has had great success with home-spun child anecdotes and is riding on his daughter’s coattails!

A lot of people wanted John to know that a) it is just a movie and not real, b) movies have plot-holes. Indiana Jones didn’t make a difference in the life of the arch, for example, and some other stupid shit like that. A lot of people wanted John’s daughter to know that the first three Star Tracks [sic] are stupid and she shouldn’t have watched them. Anyway, John didn’t think about a single tweet: ”Oh, asshole!”, but people just thought they knew how to talk on the Internet and they don’t.

That was John’s life in the last couple of days: Watching his whole 12-year Twitter investment finally pay off in the form of a tweet that didn’t really get him any new followers and was the type of things that ended up on a lot of content aggregators for that hour of that day and now he will fade back into his world where the only people... 

+ Twitter algorithm (RL380)

Merlin doesn’t even want to know how many followers he has and who follows him, but he just wants to know if somebody cool follows him, but how do you design an algorithm that discerns coolness? If you could do it, then it would be the most valuable commodity in the world. Merlin looks for something equivalent to Google’s Page Rank algorithm, but for coolness. Give him 3 cool people a day!

There are accounts that Merlin thinks are a delight, like [https://twitter.com/darth @darth] who is a red panda with a Darth Vader hat who engages with everybody and is just delightful. But there are also normal people Merlin likes a lot, like [https://twitter.com/scottsimpson Scott Simpson]. Merlin’s algorithm is that he discovers people in replies to things he has written when people build on the bit and yes-and it in ways he finds delightful. Merlin doesn’t mind people bringing a joke, but the joke should be good and based on a similar premise that the original post. That is how he discovers good people regardless of follower count and it governs the way he responds to other people’s things: He wants to engage with them personally and build on the bit. 

The things is that when [https://twitter.com/fireland @Fireland] stopped tweeting, a quarter of the Internet went away from John. What happened to [https://twitter.com/badbanana Tim Siedell] (badbanana)? He made John’s da every day. People like that get gobbled up for making other content in other places. They become writers for TV and movies, in particular Fireland. But what are you going to do? Just live on a farm? In this economy?

+ Coronavirus pandemic, companies saying they are there for you (RL380)

Like so many American companies, Merlin is somehow here for John in these uncertain times. John has not yet found a company who really truly is there for him. All his friend have been totally there for him. Merlin finds the television commercials from the last week and a half insufferable in their volume, similarity and disingenuousness. He also gets a lot of email from companies with subject lines like: ”Is there anything we can do for you?” Of course so many things can be done for him and he kind of wants to test this out if he at least can get a Coke from them!

At least a couple of the companies John does business with have been there for him in the sense that they have continued to do business uninterrupted without making a big stink about it. Merlin is impressed with local places near his house that are taking extraordinary efforts to make a fraction of the business that they used to have. John wants to give those companies all the smiles. Merlin is so fat with Italian food and he is buying so many books from independent book stores.

+ John buying two kayaks (RL380)

John bought two kayaks because you should not paddle alone in life and he is trying to kayak because: ”Why not?” It is more fun than it seems, especially for kids. To experience the cities in the Northwest or even San Francisco from the water is so different! Imagine the first ship coming in through the Golden Gate and thinking: ”Holy shit! This is so nice! This would be a good place to have a Salesforce building!”

Think about Europeans arriving through that hole in California who have been on this boat for a long time, going on shore, looking around, noticing that this whole area really smells like seal shit or sea lion pee, but how far on that first day did they go from the shore until they went back on their boat? Merlin’s first thought, because he has been in Navy ROTC, would be to see if this was a safe place to take a giant-ass ship through, if this is deep enough, sounding the depths. You see the Headlands, Angel Island and Alcatraz, wherever Coit Tower eventually was, it was so verdant, it must have been just beautiful!

For a long time people in San Francisco and Seattle were primarily oriented toward the sea. It was the main way of traveling and how commerce got done. There were no roads or any little trucks. Look at the geography of any city and you can learn so much! There is a reason that Pittsburgh and Cincinnati are river towns. You are not going to go to the basin and go to Sacramento, but you are going to hang out near the hole in California. The reason you would get to Sacramento would be that you went up the river until you got up the air. Colonel Kurtz running a civilization! ”I saw the bug!” - ”I don’t see any method at all!” John thinks he ruined that movie.

When John goes on the water in Seattle, which happens way less than he wants to, all of a sudden you are transformed and you went from the hustle and bustle of a major metropolitan area to the land of gulls and little fishes and even in a noisy town it is quiet out there and all your cares go away. These kayaks cost $80 on an e-commerce site that creates a lot of problems in the world and John has fought them for a long time but eventually he got subsumed into the borg. John bought local because he supports local companies and these was one of these daily deals that is special every day.

It is a two-person kayak that can hold 300 pounds worth of people or more and it comes with two paddles and a pump because it is inflatable. The 10.000 reviews said that this thing is stupidly amazing. John also bought life jackets, personal floatation devices. He hasn’t taken them out of the box yet because they just arrived, but he is really feeling that the kayak could be the thing that makes a big difference in his life this week.

+ John being relieved that he doesn’t have to do things during the pandemic (RL380)

John had an interesting conversation with a mutual friend (Jonathan Coulton) who runs a cruise that is local to the Internet nerds about how their quarantines are going. John is relieved about not having to do things anymore and a lot of that relief is down to the fact that no-one else can do anything either. We are actually helping by not doing anything! They got a lot of nice response for last week’s episode. For the first time in our lives we are not only allowed to not do things, but we are actually doing the right thing, and it is rare that this is true for everybody at once, but it won’t be true forever and it is going to get complicated.

Jonathan is having a more complicated reaction to it than John has because it is rough to have a 4-story house to be in. His sister in law lived in the full apartment on the top floor, but she just got her own house, so now the top floor is reintegrated into their space. He is running a cruise and who knows if that is going to happen next year! It is like owning a concert venue, what are you going to do? He also feels responsible for the 30 other people he employs who make this possible. 

He said that it has quieted the judging voices in his head who are always yelling at him that he is not doing enough and he should not just sit around, but be out pitching a sitcom or writing a Broadway hit. That is true for John as well and part of why John feels better right now is the simplicity of his day and that it is actually virtuous right now to not do things, so he doesn’t need to beat himself up about it. He could always do this, to wake up, make breakfast, do a little math at the kitchen table with his kid, do a show, go work in the garden...

Merlin says that Comparison is the death of joy, which is credited to Mark Twain and the constant need to compare to others is something we do to ourselves. It is probably the greatest curse of his life! Those are the voices that cause John the greatest pain, not comparison to other people, but comparison to the Impossible Me, and that is tamped down now, although in the first few days of the quarantine people would talk about that artists were now going to make their greatest work and there was a lot of pushback from artists. That is not how this works! That expectation went away pretty fast and most musicians tried to do some Instagram shows and things like that.

+ Performing as part of the Space Songs event for the Smithsonian Air and Space museum (RL380)

Yesterday John, his sister and his daughter went to make a video at the request of Adam Savage for a thing that is coming up sponsored by the Smithsonian Air and Space museum //(see event here:[https://airandspace.si.edu/events/space-songs Space Songs], [https://youtu.be/grPBYQ_a6Cg YouTube video])//, which is one of the best museums, and the show is airing the following Thursday live on YouTube. Each artist is doing their own performance at home, sending it in to the museum and they are combining it into a live show.

For whatever reason John would expect that the voices in his head would be screaming at him that he needed to be writing and do this and that because he could have done it this whole time, but they are not. John does a little math problem, he talks about memorizing your times tables, John records a podcast, they go out in the garden, they ride bikes for a while, and no-one is yelling at him from inside his own head.

+ Not having to do anything (cont)

Merlin keeps saying to look for little projects. He is doing a little project with one of their cruise friends in the next couple of weeks, some creative things, and lots of little dumb things. The last thing he would want right now is on top of all of this uncertainty to pile on a huge amount of expectations about something he has no business of expecting of himself. It is difficult not to as a maker and as a content creator.

John has a new take on his Europe book, but it has not solidified yet. There are a lot of things he could be beating himself up for right now. He feels a little bit like when he first started taking Lamictol where the room cleared a little bit, although at the time he got hit by a sack of flour. To just reflect on the fact that John has fewer cares and how much of his cares were the feeling that the world was moving fast and he wasn’t catching up. Having that sense be gone and knowing that the world is also struggling commensurate with how he is struggling, we are all on a level playing field, how can John keep that and have that his new baseline when everyone else is back in the discothec?

One of Merlin’s favorite phrases is ”Stupid can’t stick to me!”, which means that it is entirely up to him to decide what stupid in the universe he will allow to velcro to his ass for the rest of his life. All that is required for him to simply open his hand or butt and just drop the thing. If stupid sticks to him, then it is only because he let it happen.

+ Hoping for societal change after the pandemic - not going back to the same ”normal” (RL380)

John really hopes that we are able to make a societal change, even a small-scale one, as we transition back. Reading the news media, there is almost no conversation around the idea that we should not go back to normal ever, but that we should learn from this and should take from this a lot of new information. All the talk on the level of governors or Internet talk is about returning to normal, assuming that there is a normal and that that is what we want and we want to get back there as soon as we can.

John is not reading Anil Dash on this topic right now or seeking the usual Internet punditry, but where are the big social theorists, the people saying: ”Hey, wait a minute! Hold everything!” We are not going to reach the CEO of Shell British Petroleum with this kind of talk, that this is the opportunity to pivot to solar, because the guy at Shell BP is not going to pivot to solar right now, but there are people who can and why isn’t there more conversation about the pivot and why we are all in this place that we are always stupidly in, like: ”I don’t know what the bigshots are going to say we should do, but I am going to make a weird little stand about a weird little aspect of it!”

When you go into government you learn that the bigshots are just a dingeling who decided that this is what they were going to do. The governor of Washington Jay Inslee is doing a wonderful job. He is just some guy that instead of going on and smoking weed he decided he was going to join the Young Democrats in college. He was a noob until he got elected to state office for the first time. It is the same with military officers: They are guys where people would tell them: ”Wow, that is what you are doing?” until one day everybody was saluting them and then everybody thinks that they must know a lot of stuff, but No! Nobody knows anything, none of those bigshots do!

The CEO of your company is only the CEO because he is the most sociopathic person who happened to be standing around that day. The number of truly talented people... If you are a narcissist who shows up, you are going to do fine, but why are we listening to them? We never should have! The only reason they have authority is that we give it to them. John is hoping that in some little way, even just talking to the influencers that make up the [[[Roderick on the Line]]] universe, that we can just promulgate this conversation:

a) there is no normal, b) we all agreed normal sucked, c) we don’t want to go back! Of course there are many people who want to go back to work, and more power to them! Go back to work, just go back to work differently!

This is the same feeling Merlin has had for the last 3.5 years: There are so many impulses, especially in his own political party, that we just have to get rid of the weird guy and get back to normal like the good old days. Where you there in the 1990s or did you just CLEP out of the 1990s? There is no normal to go back to! There are times when crazy shit is the only answer and having a comfortable doddering grandpa as president is not going to get us back to normal. We are still going to have the Mitch McConnells and the Jeff Sessions all they are going to be even worse than they are now!

The governor of Iowa just [https://www.usnews.com/news/best-states/iowa/articles/2020-04-24/iowa-governor-talks-of-reopening-state-as-virus-cases-soar wanted to open everything up again], but they have not even reached the peak there. If this comes back super-hard, there is not going to be an economy to go back to! Holy shit, man! That is not normal, that is mental! We really need to let go of the idea that there is going to be a normal to go back to.

We think so often as American life as a thing that is happening on a major scale and when we say things on the Internet about politics we are always imagining some well-meaning person in Iowa who is looking out her kitchen window, thinking: ”Who am I going to vote for this time?” and we think about the CEO of British Petroleum or whatever, but John and Merlin in their very small world are not talking to any of those people, but they are talking directly to a group of people who have never thought of themselves as constituting a political class, but they do!

There is an identity, let’s call it the Merlin-verse, the Fans of Merlin Mann. Within that group there is a lot of power, some of those people are in positions of responsibility, and if that group would start to think of themselves as a group, like the Fans of the McElroys think of themselves as a group, and what they do as a collective political unit John has a sense of, and Fans of Merlin Mann would do a different thing, but expanding it from there to the general world John and Merlin live in, if just this world said that they were going to start doing it differently!

Just like the West Coast governors work together in lockstep and [https://www.opb.org/news/article/west-coast-governors-announce-pact-for-reopening-economies/ have formed a pact], the fans of John, Merlin, Hodgman and Patton Oswalt, if just that group of people in their own respective silos would say: ”You guys go back to work! We are just not going to do it the same way and we can because it has been shown that we can!” All the people with the mylar balloons: Start doing what you are doing, but you are going to hear from us differently!

John doesn’t want our people to eeyore back into the world, like ”I guess I have got to go back! Thanks for the mylar balloons!” Don’t do it! If you can! Of course John got emails from people who said they have to work for a living, but go work! If you can and your imagination and circumstances allow for it, don’t go back the same way!

Everybody started to work from home because we were forced to and the governor had superseded our boss’s authority and that authority came from somewhere, like our belief in science. Our King County executive Dow Constantine didn’t want to shut down the town, but science demanded it. The authority of the disease superseded your boss’s ability to say: ”Well, I am going to need you to come in this weekend because you are having trouble with your TPS reports!” They were no longer the boss and not even the governor was the boss, but there was a boss above all of that that was science.

Every one of these bosses is right now really chomping at the bit to re-establish their authority. How soon before they can tell people what to do again? The more days that go by without them being able to tell people what to do erodes their ability to ever tell them what to do again. They are right and their authority is eroded by this and we have seen how little we need all the trappings and all of the theater of going to work Downtown, of meeting rooms, desks, cubicles, open plan offices, and endless Cheetos. All of that is theater and a lot of it is authority theater and it has been busted!

Everyone listening to this is going to get the email that says: ”Hey everybody! Awesome news: We are opening up the office again on Monday! Can’t wait to see you there! We have so many birthday parties we have to have because of all the birthday parties we missed!” and you have to reply all: ”I won’t be coming back to work!” You can email: ”Deez Nuts!”





















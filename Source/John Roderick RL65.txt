RL65 - Trumpet Discrimination
2013-02-07

This week, Merlin and John talk about:

**The Problem:** Legislating a change of heart in someone who’s not you, referring to people wanting to change everybody else and make them better instead of doing their own part according to their ability.

The show title is referring to a question at the Pop music conference John attended from a woman who wanted to play the trumpet in school but was guided towards the clarinets and flutes because she was a girl.

It is early! As John is coughing he and Merlin transition into the song Sweat Leaf by Black Sabbath that actually starts with a cough on the album.

Merlin’s daughter has a persistent cough and it has gotten productive, which is hard for a kid. He is totally find with drugging her up, but his wife is prude about it. It is going around, everybody has got it, and kids are hotbeds of sick, they are like little sponges. Merlin throws out his sponges all the time.

+ Scott Simpson’s Japanese sponge (RL65)

Scott Simpson bought John some magic Japanese sponge, he does that for everybody, and it is great except it is a white sponge. If you are going to make a sponge a color, don’t make it white! Scott Simpson probably throws the sponge out the window after he has used it once because he is extremely rich from making techs, but he is from a very modest background, too. His mom had a Rock band and they had uniforms when they would play at friends’ events. He was in an amazing situation where he could just walk downstairs and in their basement there was a whole setup.

+ John being at a Pop music conference in Bellingham (RL65)

Last weekend John went to a Pop music conference in Bellingham Washington where he was the keynote speaker //(called [https://whatsup-magazine.com/2013/01/pop-music-industry-conference-music-is-a-movement/ Western’s Pop Music Industry Conference], hosted by AS Productions)//. He got to the conference early in contravention of his normal method of showing up to an event one second before he takes the stage, which is better for security reasons, if nothing else. Malcolm X did not hang out in the Green Room for 5 hours either.

John went to different panels and every one of them was blowing his mind. They were talking about Pop music and the Pop music business, but the expectation of the panelists was that they would address the issue of social justice, community building, and inclusiveness as they were answering questions how to get your band signed and how to promote your record via social media.

There was a predictable amount of mild college bubbling hostility that goes along with both the askers and the answerers of questions like that. The whole day the premise was: ”Hm, ja, hi! I am just wondering: Why are there not more Iñupiat Rock bands? Has anybody really thought about that? Maybe we should all look at our playlists and see that we don’t have any Iñupiat bands? Is that a problem? I don’t know, I am not sure what my question is.” and the person on the panel is someone who works in the music business and is absolutely flabbergasted.

John’s remarks had no element of social justice consideration in them and he was getting ready to give a keynote speech at this conference that was basically a hate crime. Therefore on the fly he threw his notes on Merlin Mann 3x5 cards  in the garbage can, although he should have recycled them. He had been to 10 panels by the time and every one of them had ”We need to search our souls to see how we can make it easier for everybody in the world to become a Rock star and then also: How do I get my band on the radio?”

John could not speak to that, partly because: ”Being a Rock star? Really? That is still a thing we do?”, but he wanted to address it because there had actually been some very thoughtful panelists. Questions like: ”People with allergies should be allowed to play Coachella, but there is a lot of dust in the air. How do we make Coachella accessible to people with allergies?” are almost reflexive in students now. They are not invested in the question, but they are sitting in the lecture hall and are thinking how they could phrase a question that is like a mic drop: ”Nobody thought of that! I found an underrepresented group of people that nobody thought of and now you are all wishing that you thought of people with allergies at Coachella!”

People are not actually seeking equality anymore for anybody, but it is just a game of: ”I have exposed the inequality and so: Gold star for me! I asked a question and I can go back to Facebooking!” and the professor in every case is forced by convention to consider that question legitimately and is not in a position to say: ”That is an irrelevant question!” and the ultimate answer is: ”It is impossible to make the world completely accessible to everyone!”

Every question phrased within the umbrella of: ”Our endgame is to make everything absolutely even and fair!” is the same question and it doesn’t matter whether you are talking about Iñupiats or people with allergies or people with social anxiety disorder, which is: ”How do we make the world completely fair?” and we cannot. We can aspire in certain places, but the game is not to keep finding a smaller and smaller aperture. 

John got up for his keynote speech and said that the reality about making art is that the best art is made in reaction to restriction or limitation. The culture in Prague in the 1960s/70s produced a whole generation of playwrights and artists that were working in reaction to the regime and as soon as the regime fell disappeared. Name a Czech playwright now? There was that one and he became the president of the country. That is the role that art played in an oppressive regime! Now there are probably one million Czech playwrights and none of them stand out.

We have made it incredibly easy to make Pop music. There is no difficulty at all and anyone of any age or gender can do as much Pop music as they want, but it has not improved the quality of Pop music. There are a million bands now and none of them are good because having a barrier to entry, having it be difficult is what makes people make good art. They make good art exactly because they are experiencing the challenge of being forced to make art as their primary voice.

Merlin guesses ironically that the attendees were surely extremely diverse, and John confirms that there was the white kid making Hip Hop, there was a half-Asian guy, but he was on a panel. 

//the following segment was the end of the show, double-check!//

When John got to the end he asked for the best questions from the audience because he spent the second half of his talk asking what the end-goal of forcing the issue of inclusiveness in Rock music was. For the whole length of its life Rock music has been a radicalizing force in the culture. It says: ”We are going to move our hips and show our belly buttons and we are going to talk about the war and about drugs!” and it has done it in advance of the rest of the culture. Now this new realm in which Rock music is its own mission field and people are turning inwards and asking how they can make the music world more representative and how they can force the music world to be more diverse and more of an institution. You can’t go into the arts like that because then you get Marxist art that doesn’t offend anybody and isn’t representative and people are not making art to challenge or overturn, but to satisfy requirements and to appease, and that ultimately is not art, but it is design or graphic art, not to piss off any graphic artists who are mad and are going to throw a straight edge at him or make an illustration of him that is unflattering.

Merlin suggests that when in doubt John should do the Herman Blume speech from Rushmore. ”But here’s my advice to the rest of you: Take dead aim on the rich boys. Get them in the crosshairs and take them down!” You can buy anything, but they can’t buy backbone. Take aim at the rich boys with your sarcasm!

The answer is what Merlin has been promulgating: Change yourself first, your immediate family and your small circle of friends second, and if that doesn’t occupy you your entire life John will be astonished. If you have completely changed your way of living and that of your family and your immediate circle, the write a book about it because you will be an example to us all!

Sympathy and empathy for your opponent is what is needed most now in America, and a genuine desire to see what the underlying problem is. We are not making the world a better place. The liberal project is working in a lot of ways, but nobody likes the feeling. It is so early! John thinks they shouldn’t even post this one because it is not fun!

+ John’s sister wanting to be in a Duran Duran cover band (RL65)

When John was growing up he bought his first guitar from his sister’s friend who had bought it at a swap meet. They were going to start an all-girl Duran Duran cover band and John’s sister bought a keyboard and her friend Tracy bought a guitar. They set up in the basement, put their Fedoras, their scarfs, and their pirate shirts on, they put on Rio, plugged in their keyboard and their guitar, and realized they couldn’t play these instruments and didn’t have a real interest in learning an instrument, they just wanted to be in a Duran Duran cover band. They stacked the instruments in a corner where they sat gathering dust and John walked past them every day and looked at them.

A month later he asked the two girls: ”Hey, you guys going to do anything with those?” and Tracy sold him her guitar for $25 and his sister out of spite said of course: ”Don’t touch my keyboard!” and it sat in the corner, they both never touched it, it had an inch of dust on it eventually and then at one point she owed him some money and John offered her to take the keyboard and she was happy to have it go.

+ You cannot solved inequality by law or by forcing others to change (RL65)

Institutionalized inequality is a concept first introduced during the Civil Rights Movement. We had to be educated to the fact that just because Blacks had the right to vote did not necessarily mean that they had equal opportunity in the country. It was a process of education of people who had never experienced deprivation and they had to learn that if you grow up in a place where racism is institutionalized, even if on the surface everybody has the same rights you really have a very different experience of living in America and it does present a tremendous disadvantage.

As time has gone on that franchise of disenfranchisement has been extended: When the women’s rights movement happened it was a similar process: It was a new mental technology that well-to-do women who live a comfortable life in the city are also disenfranchised and it took years to sink in. Now people with allergies, people with myopia, people with social anxiety disorder, people with whatever, and they are making themselves equivalent to people who actually struggle.

There is a tendency among everybody and in particular among those of us who are constantly reminded that we benefit from the structure, which is to say: white men. We benefit from the inequality unconsciously and there is a tendency to say: ”We have been hearing about this our whole lives, can we stop fighting the same battle?”

On one of the panels there was a thoughtful woman who said when she was in 4th grade she joined band and she wanted to play the trumpet, but she was guided by the hand over to the clarinets and flutes. John thought: ”Of course you were! The clarinets and flutes are girls instruments and the trumpet is a boys instrument!” and it was the most resonant example of institutionalized gender-role based inequality that he had heard in years.

A friend of John came home and quote the oft-quoted statistic that women are paid $0.72 on the dollar. John asked if that was true at her office and she said: ”Of course not!” and she didn’t know anyone for whom that was the case. John understands the statistic and she said it with a certain amount of pique and was angry about it, but it isn’t something she or anybody she knows are personally experiencing, so who are those people who are paid $0.72 on the dollar? John is wondering if there are two people working the same job and one of them is because of her gender really being paid that much or if it is a statistic that has been massaged and repeated so many times that we take it as given without examining it. //(this was the part that John was talking about in RL165 during his campaign)//

John walked away from this experience both newly curious about the Trumpet discrimination and also his feeling was reinforced that no-one is really talking about the long-term plan for this movement that has become an institution now, a revolutionary movement to extend equal rights to all that is ultimately a Marxist argument: ”To each according to his need, from each according to his ability!” and we are very happy to fight little tiny battles about it all day, every day, John’s Twitter feed is full of people saying there is another example of the war on women, but then he clicks through to the link and it is just an interesting news article.

Merlin thinks that if you want a fucking raise, why don’t you ask for one? He understands why you want to adapt that attitude because it can be very powerful and help bring people together, but so frequently it seems to be about legislating a change of heart in people that aren’t you, to set an agenda for how to make other people better. If you want social justice, why don’t you get off your ass? If you are getting $0.72 on the dollar, ask for a fucking raise! If you can find a way to bring social justice in Pop music, why don’t you leave the fucking conference and get out there and do it? Get your hands around the problem, learn which parts of it you can do something about, and quit worrying about how to change the rest of the world!

Ultimately all these questions are resolved when you raise your kids right and when you raise your kids to think differently and behave differently in the world. You change your own heart, you evangelize yourself, you are the primary mission field, and then your family.

A few years ago John had a very interesting exchange on Twitter where he asked a group: ”Can you through application of law really change the hearts and minds of a people?” A lot of the clear division we have in America right now between the two halves of the nation is the product of trying to use prescriptive law to change the hearts and minds of half the population.

We have been fighting a bloody cultural battle about abortion rights that takes up the whole center of our national dialog.Roe v. Wade was a badly decided piece of law and that is why it is constantly under assault because the anti-abortion people think they can beat it and it is just a matter of time before the right combination of Supreme Court justices look at it in the narrow confines of the legal decision and overturn it not on a moral basis, but because it was a shitty piece of legal thinking.

If Roe v. Wade was overturned there would be an immediate and mad rush to write new law in its place that was better, it would be the best thing that could happen to the abortion rights movement, because the next piece of law would be ironclad. It is absolutely true that the arc of history is long, but it points towards social justice //(originally by Theodore Parker)// and it is inevitable that we are becoming more liberal and more inclusive and more accessible to everyone, but we keep trying to use the lever of law when all we need to do is wait for those people to die and wait for their kids to approve gay marriage.

We could have made gay marriage legal by force of law 25 years ago and it would have sparked an armed insurrection in the South, but we suffered those 20 years and now it is happening, not because the law forced people, not because we really changed anybody’s minds, but because the young people came up and they liked MTV and they watched Sheena: Queen of the Jungle and it seems reasonable and attitudes changed. 

John was very persuaded by Malcolm X and by Martin Luther King when they said: ”All we are ever told by Whitey is that we have to wait and it is not time yet, and we are done waiting. The time is now!” It produced real action and real motion where culturally we were stalled in a weird separate but equal miasma. When you say: ”After 50.000 years, maybe give it another 10?”about any sea change in the culture, they are so quick to say: ”That is what every white man says! Wait!”

Merlin asks the question: When was the last time that somebody calling //you// stupid has changed //your// mind? You can shoot fish in a conservative barrel, but if you are so God-damn smart when was the last time that you watched Fox News and heard about how stupid you are? It is not effective! You don’t change hearts and minds by legislation or by making fun of people. You can sit around in a room all day long and come up with some bill of rights for people who play mandolins, but that is not going to make black people any better off when it comes time to go to Bonnaroo. There are unintended consequences to all those things, but it comes down to getting off your ass, get off Twitter, and do something about it!

John doesn’t care about the chattering classes on Twitter, but the real difference that is happening amongst the people who really are off their asses and doing things is that the fundamental liberal project since the 1960s to imagine that we are creating a utopia in America and there a lot of people who don’t want it or don’t get it and we are going to help them along by forcing them to open their minds.

+ Liberalism should look back and learn from failed projects (RL65)

Through a lot of big projects like bussing or housing project and a legislative approach and a social engineering approach liberalism set about to create a utopia by dragging the recalcitrant people along, kicking and screaming. There was a moment in the 1970s where crackpot conservatism became mainstream, an overflow point right around the time when governments were saying that you can no longer have an all-male smoking club because it is discriminatory that you have a requirement that it is all dudes.

At the moment where we reached down into the culture and started saying that you can no longer be just some dudes in a club, it is against the law now, that was the personal moment where conservatism became the ascendent movement. There are a lot of people in the world who do not want to be told who they can hang out with. Their response wasn’t coming from a place of sexism or racism, but from a deeply personal place where somebody with a clipboard like the EPA agent in Ghost Busters was saying: ”Venkman, shut it down!” //(from the movie)//

People already felt like the liberal project was pushing them against their will with their heels dug in towards a world they couldn’t see and didn’t understand, and then it got personal and came down to their street. ”Now I can’t even hire my brother in law? You are down here telling me how to do my business!” From up high liberalism said that this is how it has to happen, like that scene in Doctor Zhivago where all of a sudden post-revolution Dr. Zhivago and his family who used to live in a 40-room mansion and all of a sudden there is a knock on the door and a woman with a clipboard is moving families into their home and pretty soon Dr. Zhivago’s family is living in one room, curtained off with sheets and the premise is: How could you have been living in such luxury while people were poor on the street? Now everybody is living together in a rat’s warren in your old family mansion and we have arrived at Marxist equality, which is squalor for all.

It is very hard now to hear somebody from Arkansas or even from Seattle how has a large Dodge truck that maybe has some truck nuts hanging from the back bumper, listening to them bitch an moan with a ton of vitriol about how Obama is coming to force them to accept medical insurance. It is very hard for liberal sensibility to not feel like: ”What is the matter with you cry babies? My God! Man up!”, but the fact is that they are expressing a discontent that is 40 years old that is a discontent with the premise of the whole liberal project, which is: "We are headed toward a utopia and you are coming whether you like it or not!”

Merlin doesn’t think you care about something unless you have sacrificed for it. If you care that much, why don’t you put your life where your mouth is? If you just stipulate for one week that you are stupid and you need other people to tell you what to do, your eyes open to how the world operates.

Liberalism is very self-reflective in some narrow corridors, but there is not a historic self-reflection that applies to what we have been doing since World War II. If we were able as a culture to look back and say: ”We tore down all of the Tenement buildings on the Lower East Side //(in New York City)// and we built massive brick housing projects to solve hygiene problems and the incredible unruly mess of Downtown New York that was full on immigrants and everybody was just wading through sewage. There was a watch shop next to a hat shop. How can this stand?”

We bulldozed it block after block and built neat and tidy housing projects, a giant government undertaking, and we provided a clean and secure neighborhood environment for people and it was going to uplift the people. To look at that now, you see what a misguided approach that was. The Cabrini-Green project in Chicago became the most dangerous place in the universe where the police advises you not to stop at red lights. We had the mandate to tear down the old neighborhood and build this, but we did not the mandate to follow through on that.

You don’t just tear down people’s neighborhoods and put them in high-rises and automatically they have become philosopher scholars, but you then also need the money to make the schools in that neighborhood the best schools there are and have job-training programs. It does not work if you are also experiencing a nation-wide decline in manufacturing and close all the factories.

It is very hard for us to look back at that and admit that this was a liberal project with the end-goal of creating a utopia, but it created a blight and it precipitated the death of the inner city that we are still trying to recover from. The story of Detroit is that when they were building the Interstate Highway System they slammed it right through the heart of Detroit’s most vibrant black neighborhood. They just cut it in half and killed it in one swoop and it produced so much anger in the city Detroit, which at the time was a very vibrant and alive black community of music and art that Detroit went through 40 years where it was recreational to burn down abandoned houses because they had completely lost their feeling that the city belonged to them.

The Interstate Highway System was not just a liberal project, but an Eisenhowerian project, a big social engineering concept where we instead of being stuck in traffic all day, wending our way into this little neighborhood where the hat shop is next to the watch shop, but we are just going to tear it all down and build a big space port out of brick and it is going to improve the quality of everybody’s life, and 50 years from now everybody was going to be dancing like in those Buck Rogers television shows in the early 1980s where people were holding onto ribbons and pleasure balls and they are dancing around to Brian Eno music, if we just get rid of the center of the city where people are living as people naturally do, which is like mud pigs, but that is who we are.

John wishes that Liberalism had the capacity as a forward-thinking ideology to do a little bit of backward-looking and realize that we have tried to engineer an utopia multiple times where education and government action enlightened the people, and once it was patiently explained to them that equal rights for all was beneficial and economically advantageous, and everybody got it, and we were living in a future world where all the children were Mochaccino-colored and we sit around on the steps of our pantheon and read Aeschylus plays to one another and that is the America that we hope for and aspire to.

A lot of things we have tried have worked great, but there is the resentment that you feel in America about big government telling you what to do. The people who were most affected by those projects are still Democrats. It is the people who were on the outside looking in, the people who got out to the suburbs, that looked back at the city and said that Liberalism is bankrupt.

John still has a very hard time being yelled at by conservatives, but he does sympathizes with some of the evidence that they are able to muster: This mentality, which is: ”We are coming into your homes and we are going to explain to you why you are wrong and then we are going to deprive you of what you consider to be your rights in service of the greater good, and it doesn’t matter whether you are going to like it or not! Go fuck yourself! Also: God is dead. Free pot for everybody!” and John can see why they are bent out of shape.

Merlin equates it to the Civil War that was not just about slavery: You just don’t tell people in the South what to do. We don’t always understand the story and our amount of certainty about how right we are, how wrong everybody else is, and how much they need to be rehabilitated to a learned point of view is an affliction, especially on the left and right coasts. Welfare is an incredibly complex problem to solve and it is going to take more than one angle.

John understands what Merlin is getting at, but the argument that it was about anything else than slavery is revisionism because the United States was fighting a war about slavery for 50 years before the Civil War, it was a thing that he //had// to fight a bloody battle about. The underlying causes, the clash of culture between the cavaliers and the quakers is all true, and maybe you could make an argument that all of these liberal projects of the 20th century have been incremental wars that have prevented what could have been a much bigger and worse war.

In 1967 it was plausible to people that we were headed to a conflict on the scale of the Civil War and people were burning cities and fighting in the streets. Even the biggest conservative zealot is familiar with the concept of every liberal totem, but at the time there were great swaths of normal American people still wrestling with the concept of Conscientious Objectors: ”Why wouldn’t you want to go fight for America in a war?” They couldn’t even get their heads around it. Maybe we have been fighting these small battles and it has released the pressure that would have built up to a larger conflict.

The Civil War was inevitable and there wasn’t any other way. They have tried for 30 years beforehand to legislate their way around the problem, concessions were made to the South like you wouldn’t believe, they were arguing these questions all the way back in the constitutional congress and the South was just contemptuous and immobile and eventually there was going to be a war. The British outlawed slavery in 1802 or something! The Southern argument that the Civil War was really about states rights or whatever people want to say it is about is not true.

In 1957 John can’t imagine standing in that place and not picturing that we are going to have a war every 100 years about this stuff, and somehow we fought a terrible cultural war and we avoided taking up arms. The conspiracy theorists would say that it was because the CIA started a crack epidemic and the FBI definitely worked really hard to get the Black Panthers to fight themselves, but we tried a lot of different things to move the culture forward so we didn’t have to fight a bruising revolution.

Merlin talks about legalizing marijuana and the zoning about it, like not being able to have a weed shop within 500 yards of a school, and if you do that the closest pot dispensary you could build was on the road to Sacramento. It is also hard on sex offenders. If you are getting caught peeing outside you might be a sex offender, which sounds like a Jeff Fox worthy bit.

+ John’s friend getting arrested for peeing in a public park (RL65)

Many years ago John was peeing outside in a public park with a friend and all of a sudden there was a cop car there with its spotlight on, spotlighting John’s friend, and John was standing 15 feet away, which is the socially prescribed distance you should stand from your friend when you are peeing together in the dark in the park and somehow he was outside of the spotlight aura and they didn’t see him, he finished peeing, zipped up his pants, walked around a tree, came out from the other side, and walked up as the two cops were getting out, arresting his friend. John couldn’t even bail him out because he didn’t have any money.

+ John being worried that his daughter will not develop interest in music (RL65)

John has been wrestling since the time his daughter was born that he has a house full of instruments and he is worried that it does not present enough challenge to his daughter. Music is all around her, she is invited to play any instrument at any time, but she is not going to develop any interest in it at all. Now he is thinking about making them all off-limits to her. The famous story about The Jackson 5 was that Dad Jackson had a guitar and he said that no-one was supposed to ever touch, but little by little all of them learned how to play the guitar and they had to hide it from their dad and he only discovered it when one day they broke a string and didn’t know how to fix it.

There has to be a barrier to entry and some difficulty, which is why so many hipster musicians raise their kids to be groovster and the kids end up being cheerleaders and football players, but there is no evidence for that. Merlin was hissing while John was saying that and he explains that there is hissing in theaters in San Francisco during the movie trailers. Some people just do it because it is a thing, like when you go through that one tunnel by the Golden Gate Bridge where everybody honks. Merlin can’t even tell if it is ironic anymore. John says that in the Northwest you wouldn’t make a public display like that without really upsetting people.




















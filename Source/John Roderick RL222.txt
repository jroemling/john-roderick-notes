RL222 - Bastik of Problems
2016-10-31

This week, Merlin and John talk about:

**The Problem:** John is susceptible to his own goof, referring to having talked about the UFOs looking for him as the anchorman so much that he now believes it himself.

The show title refers to John having trouble sleeping and not having taken his medicine and having a handful of problems, not feeling well, which is a ”bastik” (basket) of problems.

The other day John was at a party and a woman said she worked at Zymogenetics, but she was not a biologist, but a biochemist.

+ John drinking light canned coffee with artificial sweetener (RL222)

John is a bit disheveled and not 100% put together. He is drinking canned coffee light that has 45 fewer calories than the other branded Espresso and cream can. Merlin enjoys those. John saw a woman at his local grocery store buying them, he asked her about them and she said she couldn’t live without them, but if John has never had them he better not start. There was a lot of whimsy between them, groceries bring people together. 

She reached for some flavored one, hazelnut or a pumpkin spice or something, but John couldn’t go that far and he grabbed a full octane ones at first, but then also took one that had 45% less of the bad stuff at the same price. This morning John was looking at it in his pantry and he got the full-strength one, but now he is starting to taste the Aspartame in his canned coffee.

You can hear that the coffee maker is going in the background and there is real coffee on its way. A long time ago John talked about the coffee maker he had purchased at Costco and Merlin said that it was a bad coffee maker and the next day a better coffee maker showed up at John’s house, courtesy of Merlin Mann, which is one example of what it is like to be friends with Merlin. He also gave John's mom a wallet one time.

Merlin didn’t want to get into this, but like Michael Stipe says: ”When you throw something away, where is away?” Where are those 45% fewer calories? It is in the Aspartame taste on John’s tongue. Light bulbs are the greatest example of this, where they say that it uses 10% less energy, but most of the time it gives 10% less light. The coffee has Sucralose and that is probably what John is tasting. He is very sensitive to Aspartame or Sucralose or Acacia Berries, he doesn’t like any of them.

At first he thought it doesn’t taste like Coke Zero, which was good. John Scalsi was drinking a lot of that stuff on the cruise (the JoCo Cruise, the only cruise Merlin has been on) and Merlin admires him, so he tried to get into it, but he couldn’t do it. He had no luck with Diet Coke in college, and this is better than the Aspartame, or however you pronounce it, the Prisencolinensinainciusol ”all right!”.

John doesn’t want those espresso cans anymore. Merlin gets into a thing and he has had things in his life, right now Seltzer Water has been a thing for him, for a long time it was Coca Cola, some people had smoking, but there is a go-to thing that you return to for nourishment comfort several times a day. Merlin drinks about 12 cans of Seltzer a day and he has 8 cans on his desk right now in different states of undress, mostly drunk.

Merlin thinks this pushes up against some issues that John has with the world. He smoked a lot of cigarettes at one time and then he stopped that and now he tries to catch himself by saying ”[[[Leave it!]]]” whenever he sees himself having something turn into something he is doing too often or perhaps without thinking about it. You are trying to be the ball! You see the ball and then you be the ball! Merlin thinks that a lot of people say that, but they don’t really want to be the ball. If all you have is a nail, every problem looks like a hammer!

+ Shark Tank, Merlin’s daughter asking him complicated questions (RL222)

Merlin’s daughter has a new thing where she will ask him a question that he doesn’t feel like answering, not because it is always about testicles, but sometimes he has to tell her that the answer is a long story. She told him that she likes long stories, which means Merlin has to find a new euphemism for wanting to watch TV right now. He was watching Shark Tank and Mr. Wonderful said that the guy’s potato costume looked like testicles. Merlin continues to tell John about that specific Shark Tank pitch that was a rare joke project.  They were seeking investment for writing something on a potato for $10 and sending it to people.

There are a lot of things about the body that Merlin would like to have to explain because he doesn’t really understand it although he acts like he does. Sometimes before he uses the toilet he weighs himself and he weighs himself after, and sometimes he weighs more after he has used the toilet. John doesn’t have a scale in his house, nor does he have a TV. Merlin got a WiFi scale that works together with his Fitbit through the API. He also got the Matt Haughey garage door.

When John was in Canada he saw an episode of what was probably the Canadian version of Shark Tank. Shark Tank is an American import of a British show called Dragon Den that is a little more serious. On Shark Tank they will tell you things like: ”That is not a company, that is a product!”, or: ”Take it behind the barn and shoot it!”

+ John having problems sleeping lately (RL222)

The last couple of days John was feeling super-duper weird, partly because he was been waking up in the middle of the night and staying awake for a couple of hours. Right as he is about to go to sleep and has already a toe in being asleep, the feeling when you think you didn’t sleep at all, but you were asleep. Merlin’s Fitbit will tell him how much he was sleeping and it will make him feel bad sometimes. John will often think he was just laying down for a second, but hasn’t been sleeping until he notices that 45 minutes have gone.

John has a close relationship with the in-and-out area of sleep when he is a little bit in and a little bit out. Lately he will get a little bit into sleep, just tip his toe in there, and then somebody in the dream will turn and there will be an uncomfortableness and it feels like something a little off is happening here. He had nightmares when he was young, like anybody, but lately for a long time he hasn’t had any bad dreams at all and hasn’t been remembering his dreams at all. He doesn’t like to go to sleep, but doesn’t dread it, he just doesn’t want to stop being awake.

Lately there will be something that jars him from within the dream space, he becomes self-aware that he is in a dream and he gets a panicky feeling within the dream and he comes out of the half-asleep place in a panic that feels like if you put baking soda and vinegar together, and he accidentally makes a dream volcano and pops up out of sleep and he is breathing fast. He is highly suggestible of causality of that feeling and he thinks there is a reason in the real world that it happened. He has been suggestible for years that there is either an UFO in his room, trying to touch him. Merlin wonders if they are [[[anchorman |looking for the anchorman]]], but John thinks he has spun these yarns to himself such that he has created a lifetime susceptibility to his own goof.

If you are afraid of ghosts, then ghosts exist because you are afraid of them, you are creating them, and you are seeing angels in architecture and you are seeing UFOs in the closet. Merlin says that all that is real because we are pattern-matching things and it only takes a little bit of emotional or intellectual fuel to have you start noticing things in certain ways. Everybody does that!

John has dug a little trench in his imagination and attributes his panic to one of various causes, like Ghouls or a generalized Bedevilment, and he can’t immediately roll over and go back to sleep because he is hyperventilating a bit and searching the room for a) an UFO that has come to touch him, but then ran away like a child-like alien that doesn’t want to reveal itself and isn’t ready to greet him on the international stage like a dignitary.

Sneaking into his house in the middle of the night, tickling his feet and waking him up from a dream seems like a dumb thing to do for an alien, but they are inscrutable, or b) even though he has been living in this house for 10 years it is suddenly haunted by the Ghouls of the 10.000 Civil War soldiers that never marched through this area? Or is it some other premonition. This has only been happening very recently and it has never plagued John before.

The last couple of days he ran out of his medicine and he forgot to go to the drug store two days in a row. It will stay in your system for a little while, but then it is half-lifeing its way out. John is feeling a little tingly, disoriented, and slurry, but he is also not sleeping and cannot identify what his problem is right now. There are a little handful of problems, a [[[child-speak |bastik of problems], a bag of holding full of small problems, and he needs to get out of here, stop drinking his Aspartame coffee and go to town, hitch up the wagon to his dwarf donkey.

+ Dreamscape (RL222)

The movie Dreamscape from the mid-1980s, John’s heyday for cultural references, stars a little guy by the name of Dennis Quaid, also Kate Capshaw, Max von Sydow, and Christopher Plummer who was Captain van Trapp (in The Sound of Music). The science thriller about Cold War paranoia where people can go into the dreams of other people. The bad guy in this movie is David Patrick Kelly from the movie The Warriors and from 48 Hrs. He is an assassin who goes into the dream of the president because he is going to kill him in the dream and then in the real world space it will appear as if the president just had a heart attack in his sleep.

Dennis Quaid has to go into the president’s dream, but they had to get him into the room next door because you have to be proximate to the person who’s dream you are getting into, a bit like the plot of Stranger Vacations (?), the TV show that everybody is watching on Netflix right now //(maybe Stranger Things)//. Merlin thinks it also sounds a bit like the movie Inception with the Batman guy where you get the levels and you spin your top.

The dream assassin plot is pretty heavy and David Patrick Kelly is a super-good bad guy, but what complicates it is that the president is having a dream about a post-nuclear apocalypse that is his fault because he started a nuclear war and now he is living in the blown-out future world. He is already in a hellscape of his own and then the dream assassin comes to kill him and then Dennis Quaid comes in to kill the dream assassin.

It is not a super-good movie, but it imprinted on John the geography that you can be a) the president, b) think you are responsible for a nuclear war every night when you go to sleep and spend your whole night living in an apocalypse that you created and then have to wake up and be the president again. It is a bit of a peacenik movie, too! c) Then assassins can come into your dream and they will just seem like dream characters within the dream, but they are actually a real bad guy that is there to kill you and then another guy can show up. John doesn’t even feel 100% that his dream space is safe from invasion.

+ How Science Fiction writers were wrong about today’s youth culture (RL222)

Despite us moving along on our way to a technological super state everybody is starting to wear garters on their sleeves and straw boaters. John saw a straw boater for sale the other day and he was sorely tempted. We are waxing our mustaches, we are basically turning into Shakey’s Pizza, a thing that defies Science Fiction: There was a lot of genius SciFi prognosticating in the heyday of SciFi in the 1950s and 60s when they were really thinking about the future.

We can point to a lot of authors that were prescient and could see that we were about to develop the talking broom, but what no Science Fiction writer ever conceived was that in 2016 the music and fashion of the young people would be derived from a pre-vaudeville mining aesthetic, like in the early days of photography: Banjo music and waxed cotton, a form of weather proofing that has been obsolete since the invention of rubber, and old-timey leather-bottom shoes. No-one saw the return of the penny-farther!

It is the failure of imagination when a Science Fiction writer of 1959 was trying to think about what 2020 would be, they are putting together all these hovercrafts, but the culture has discovered and is exploiting and mining this crazy vein that isn’t even the sexy part of Victoriana, it is just bizarre and seems to stem from a desire to wear suspenders again.

Thinking about the now, trying to SciFi our own future, of course the dwarf donkey pulling a Conestoga Wagon is the future of public transit because it comports with the weird trendy past-fixation, but really 30 years from now the fashion is probably not going to be frontier anymore, and it strains our minds to imagine the ways that dwarf donkeys are going to be employed.

Merlin thinks a lot about the future and what we know and what we can guess. We can make reckons about the date by which a certain specific thing might happen, but we can never know what happens in between now and then. You see hoverboards in the airports all the time, and kids in John’s neighborhood use ones with a weird blue-glowing ground effects. Every single one of those is dangerous.

+ How the need for your skills can just disappear, the cycles of songwriter popularity (RL222)

When John tries to think about the future he is reminded of how many things he thought were going to be valuable skills that he had acquired, but it turned out that it wasn’t that the skill had surpassed because your skills get old and you need to continually update them, but the need for those skills just went away. John was thinking about this in terms of song writing. His friend Eric (Anderson) who records under the band name Cataldo is a great songwriter, and the art of songwriting is still a viable and wonderful art and you can still be really great at it, but the culture just in the last 10 years has moved away from a single person with a guitar writing songs.

There are still plenty of exceptions that prove the rule, but we are now in a waning of songwriting as the primary way that people are seeking music and entertainment. It waxes and wanes, there was dance music for a while and then it goes back to songwriting and back to dance music and back to songwriting, and right now it feels like to sit and write a song with a guitar is almost an anachronistic thing to pursue when you have so many options of making music and generating beats with your computer, music that is often not even a song but rather a scape.

John thinks that the songwriter will always return, but that might be a prejudice that favors his own past and his own knowledge of the past, and it may be that songwriting never does return, that the technology becomes the art and every once in a while some 20-year old Jewish kid from Minnesota steps up in a straw hat and says: ”I am an old man already and I am going to start singing songs about hopping freight trains!”, but how many more times is that going to work?

John listens to music all the time in different environments where he is astonished that you can hear that the music was expensive to make, even if you are really gifted and you are just making it on a laptop at home, because of the way it sounds. You need expensive equipment and access to good players because you can’t program the playing, but John can’t figure out what the market for this music is. Who would buy this? It is not songs, you don’t buy that record to listen to that song over and over, but it is a musical landscape.

The line between music-maker and producer and songwriter is all very blurred these days, Pop music is still trying out song-based hits, but those are being written by songwriters in a very different way, it is not very Brill Building-y. When John thinks of 30 years from now and people listening to music, there is a very real chance that the way people will think about music then hasn’t occurred to John now yet and he is still thinking that the songwriter is going to come back after this period of dance.

+ Trying to prognosticate about transportation of the future (RL222)

John has been talking about alternative forms of urban transportation for a long time. We are on the cusp and it is about to happen and people are sending John articles all the time about metropolitan gondola projects and funiculars on the rise, literally, but also about the Venetian kind of gondolas with a guy standing in the back with a flat hat and a pole, like bike lanes, but water! Turn everywhere into Venice! How hard can it be?

When John was [[[run for office |running for office]]] the hardest part of prognosticating was talking about transportation with people because they cannot conceive of a time when the paradigms change. The other day he was driving along and realized that as you get just a little bit outside the center of any town all of a sudden the parking lots get so big for things. The parking lot of a Lowe’s is bigger than the Lowe’s. There is so much land that we think of as being under development, it is not abandoned and it is being used, but it is just paved and fallow, except on Christmas Eve. 

If John’s predictions are true, in the very near future parking will no longer be a thing, and our world is going to look bananas to us, particularly when we are talking about a $3 billion levy to expand the system and build it in such a way that it lasts 50 years and this tunnel under the city is going to last for 100 years, but we are only going to need it for another 8 years.

In the 1910s there were still a lot of people who advocated for better ways to accommodate the waste of all the horses, which was a completely sensible thing to say as long as you assume that that curve is going to continue to go up, rather than not just go down, but go away. John is going to be the horse poop mover king of New York City in 1920. That is going to get people to vote!

There was a story on 99% Invisible about Chicago and the incredible problems they had with the amount of waste and they were just dumping it into the lake. It was the episode where they talked about why they raised up the buildings one floor in order to put in sewage //([[https://99percentinvisible.org/episode/episode-86-reversal-of-fortune/ Episode 86])//. There was one week where we went from our last idea of how much we should worry about horse poop to the next idea of thinking about this problem a little differently.

Here we are at the crossroads. There is not going to be parking anymore and we are going to pull up all that asphalt and reuse it, squeeze it, milk it for the oil, and turn it into park benches, we are going to [[[Supertrain]]] it, and we are going to have all this open land. Presumably we are still going to need Lowe’s because what is somebody going to do if they want to tear their classic turn-of-the-century kitchen and bathroom out of their vintage home and turn it into a thing that feels like a extended-stay hotel room on a business trip.

What if you want to AirBnB the place up a little bit and get yourself a new WiFi-broom? You are going to order it on your Amazon/Lowe’s/AOL/MSNBC, or you are going to say: ”Siri, is it raining?” and then Zoey Deschanel is going to start to cry. Siri is like Harvey Korman, in the sense that he is interacting with his phone and every once in a while Harvey Korman opens a closet door and says: ”Hello?” - ”No, Harvey! It is not your turn!” John has never employed any of the voice command systems in his phone voluntarily, but they are Easter eggs if the Easter egg was an angry crocodile. ”No!” Merlin uses it constantly. Matt Haughey is doing it right now and his garage door is going up and down from 2 miles away. ”Siri! Siri! Disconnect!” and the sprinklers in the house are on.

+ Gender-neutral bathrooms (RL222)

In an increasingly large number of Seattle businesses the restrooms are now gender neutral and it is so logical and so reasonable, you couldn’t possibly have a problem with it. There is nothing about it that is difficult, except to imagine ever a time when the bathrooms were segregated by gender. At a baseball game in a stadium when there are 15.000 guys who all want to take a piss at once there should absolutely be troughs where you can herd all those wildebeests into a thing where they can just pee and keep moving.

It should be a [[[Keep moving and get out of the way]]] situation where you enter by this door, you start peeing, you keep moving toward that door, and then you be done peeing by the end. But otherwise? What the hell were we thinking? You can have an anteroom with a mirror and a sink that everybody can use and then just potties, it is a no-brainer!

The bathroom in Merlin’s Junior High, built in 1977/78, seemed weird and revolutionary at the time. Of course they had boys and girls bathrooms, but the bathroom wasn’t really a room, but it was closer to what you see in an airport where there is a doorway in and out. The classic old-school bathroom has a door that says ”boys” or ”girls” and everything is inside, but you could see into the area from outside with 6-8 sinks and paper towel dispensers. Behind that there were a dozen floor-to-ceiling doors that opened up into your own little corridor, you lock the door and you use the bathroom. Why is that not pretty much every bathroom? If you have something private to do, you do it in a stall that is actually private.

The world of 1970 was the decaying world of 1935, just like today we are seeing the decay of the 1980s. In 1970s it was all 1935 garbage with all these little old people were still wearing Fedoras and eating in little railroad diners where you walk in and there is a ham. Proper old Chrysler-building-style Downtown buildings, bus-stations, or most of the court houses and buildings that John’s dad spent a lot of time in had bathrooms with marble floors and the stalls were divided by sheets of marble that were 2 inches thick with giant and heavy wooden doors.

The porcelain thrones, everything about these bathrooms were things of beauty when they were constructed, but by 1975 the marble was discolored and the door had fallen off its hinges and somebody screwed it back in with a different-size bolt, there was always a suspicion that there was somebody in one of the stalls who had been in there all day, and there was the echoing of your leather-soled Wingtips. In the 1970s they started taking those bathrooms away, just gutting them and replacing them with shite bathrooms. But those bathrooms, even in 1935 could have been ”Everybody in!”

In Merlin’s High School the bathroom stalls had no doors and if you wanted to poop you had to do it, which Merlin never did, prison poop, you could make it into prune wine, because that made it easier to maintain and you could see people smoking and fighting and all the aberrant things that boys do in bathrooms, but that is the fixation and there is still this idea that there is going to be some dude in the bathroom who is there to do something with his wiener that ain’t peeing and my daughter is not going in there!

That is such a strange angle, like refusing to go in brick buildings because you think they are haunted. It is our hangups in the same way that we are dealing with the garbage 1980s, our hangups from other decades, and are you sure that the things you are positive about still have any relevance today? Might that point of view that doesn’t make your life better be harming someone else?

+ John burning himself on two hot bolts in the sauna of a Russian bath (RL222)

Recently John was moseying around, as you do, and he felt something on his back and as he reached around he discovered two enormous sores on his back. John’s skin is sensitive, sometimes it is allergic to itself, it gets a little inflamed sometimes if he uses the wrong soap, but also if he doesn’t use soap because he is allergic to his own oils and has reactions if he doesn’t use soap often enough. Even if a truck carrying soap goes by and someone will alert him to the fact that this truck might contain soap, he will probably get a rash.

These were two giant sores in the middle of his back, it looked like he had MRSA or that they weren’t turning him often enough, each one the size of a quarter, and 5 inches apart. Merlin never had shingles, he just had herpes-related things, he had chicken pox, he gets stress bumps, and he has known people with shingles and apparently it is quite unpleasant. John has had shingles, and the confusing thing about them is that they happen on one half of your body, which is probably neurological.

John asked everybody he saw for the next several days to look at his sores and they would look at it and go: ”Eww!”, but 5-6 days into this thing it wasn’t clear to John or anyone whether the condition was getting better or worse. This thing came out of nowhere and he felt he had to go to the doctor because this was going to be one of those things like: ”If you had come in a week earlier, maybe we could have saved you!” He was about to make a doctors appointment because his usual project of ”Wait until this goes away!” produced results, which were that it wasn’t going to go away, and now he needed to go to the doctors fast.

It hurt, but if a cancer was already creating this kind of kaposi sarcoma he should be sicker than he was. Then he remembered that he had gone to the Russian bath and be was thinking that this could be some Spa MRSA, some thing he got from being in the cold salt water tank with a bunch of Ukrainians. Then he remembered that when he goes into a sauna he likes to go into the hottest part and wen he was at the Russian bath he went to the sauna, climbed up to the top bench next to the furnace and he leaned back onto the bench that was very hot, but he likes a certain amount of scalding pain, so he pressed his back into the hot sizzling bench, and the bench was held together with two steel bolts that had heated to a temperature at which they were able to give him burns.

This sample size made John realize that he was not good at self-diagnosis and pretty sure he had crossed a line because for his whole life he believed he couldn’t die, it was impossible to kill him, and so far the evidence stands up because if God wanted to kill him there have been so many opportunities, but each of them proved the premise that John wasn’t meant to die and when he did die it was ordained and was supposed to be a big deal.

In this situation John could have made use of a cancer-sniffing dog because he was having anxiety, which he is now having all the time because he crossed some Rubicon where on one side it was John and the Romans and an Army made up of Barbarians and Carthaginians. John didn’t used to have anxieties, at least he didn’t use to identify it as anxiety, but as a desire to smoke another cigarette, but now he is waking up in the middle of the night because UFOs are touching his toes.

+ John building a relationship with his doctor (RL222)

Every time John goes to the doctor and says: ”What does that mean?” - ”Don’t worry about it!” - ”That is not what doctors are supposed to say!” and the doctor leans in and says... John has a new doctor and he now is developing a relationship with this doctor like his dad used to have. His doctor is 6’5”, he is 60 years old, and he is in very good shape for a 65 year old, he has a fidget and pulls on his fingers, he probably bike-commutes, he is a bit of a hippie, and John asked: ”What is the worst case scenario?” - ”There is no point in thinking about a worst case scenario!” - ”Wrong! The point of a worst case scenario is to think about it and brute on it!”

”Wrong! Don’t think about the worst case scenario because the reality is that anything can happen and why not just not think about it?” - ”That seems dumb!” - ”No, the opposite! Seems dumb to think about worst case scenarios when there is no...” - ”What about this?” - ”Well, we could test for those things, but typically we don’t” - ”Well, but this is a typical situation because it is me you are dealing with, so let’s roll out these tests!” - ”Nah, we test and maybe we find something, maybe we don’t. Typically, you don’t have any of the additional symptoms of anything that would move us in the direction of thinking that you had anything other than that you are perfectly healthy and fine!” - ”Well, says you, mister! What about all the potential heart attacks I haven’t had yet?” - ”Well, anybody could have one at any time! People have them all the time, but generally we think that if you are going to have one you would show those symptoms and you don’t have those.”

+ Follow-up: Comfort animals, dwarf donkeys (RL222)

After last week’s episode //(see RL221 where they talked about Comfort Animals)// somebody sent John a link to the notion of a comfort donkey and John spend a bit of time researching small donkeys. John has a bit of property, he could get a bastik for it. The people who are raising dwarf donkeys are obviously Baby Boomers. John is always going to think that they are 30 years old and he is 15, a Yuppie, but of course Baby Boomers are now 65 years old and they are wearing dad-jeans and they are voting for Trump for the most part and they are raising dwarf donkeys. Most 25-year olds John knows would happily raise dwarf donkeys.

Behind this link was a picture and of course they get you by showing you dwarf donkey babies, which look like they are the size of a Scottie Dog (Scottish Terrier). This is a thing John definitely wants, but then they grow up and become the size of a Great Dane. Merlin and John continue to look at some pictures together. Getting a dwarf donkey is like getting a hot tub: It looks easy at first, but there are all those dependencies that you don’t think of.

If you walk into an airport with a dwarf donkey, it is going to have to be a real vest you are going to have to hold eye contact with people and you are going to have a couple of notarized letters and you have to sit there and look at them and you are going to have to say: ”This fucking donkey is coming on the plane with me!” John knows so many young people that, if what we needed as a culture was comfort donkey ranchers, would be lining up to take this job!

What is it called? 4F? Or 4H? 4F is when you don’t have to go to Korea, that is when you have 5 feet. In the 4H club you have a Macklemore haircut. We could repopulate the Dakotas. On fleek! All those little towns out there that Monsanto bought and plowed under, we could fill them up with Macklemores and raise comfort donkeys, but the problem is that we don’t need them. From within a narrow perspective it seems they could solve a lot of problems. Millenniums today, their parents are an aging population that might have need of this.

We are mocking dwarf animals in general because it seems beneath one's dignity to spend too much time secretly looking at pictures of tea cup poodles and tiny little pigs, but we all do it! Merlin recommends John to look at a Pudú, which is the world’s smallest breed of deer, like a Capybara meets a toy-sized dog.

John wants to know how we are going to employ dwarf animals in the future. We are making more of them all the time and this may be a money-making opportunity. Looking back they will say that the Isaac Asimovs of dwarf animal prognostication were John and Merlin at the tail-end of 2016 because they foresaw the global desperate need for animals that are forever small and young. It is already 40% of the Internet. What if we stopped acting like this is something exceptional and weird that will go away and take it as the thing we never knew we needed?

For example, accessibility is good for everyone. There is no reason not to have it, because that is just the failure of not thinking about it early enough and will lead you to make ugly and costly things. If you think about accessibility from the beginning and stop acting like it is some weird thing to accommodate this one guy in a wheel chair, but seeing it from the beginning as somebody that everybody will need, lots of things start to change.

Maybe they are too specific here in saying that it needs to be a draught donkey or a specific comfort animal because there is a Venn diagram where this heavily correlates with wanting a pet. The millenniums are not going to want kids the same way that the previous generations wanted kids. John doesn’t think that the previous generation wanted kids, but they wanted draught animals and the cheapest ones were kids.

We keep thinking that the purpose of these animals, these tiny deer and these dwarf donkeys pulling little carriages is that they are amusements and we are not thinking of them as practical and we are not envisioning a future. In the same way that 15 years we never could have envisioned that the banjo would become the primary instrument of contemporary pop in 2016. We are not realizing that the Pudú maybe is a real economic driver in a future economy that we can’t even really picture right now. But there is a place for Pudús, we are going to make a place for it, and then retroactively it is going to seem inevitable.

Automobiles have been a weird nuisance that were just scaring the horses. Now it is hard for us to imagine what the next animal is going to be that somebody is going to need on an airplane with them, but the reality is: All the animals and the airplanes are going to be full of animals and that is going to seem totally normal. Everyone is going to travel with a companion animal of some kind. When Merlin was in college, only kids and hippies and people who camped would wear a backpack, everybody else had their Wall Street Journal folded in a briefcase. He had a JanSport style backpack that he was wearing on one shoulder, which was stupid, and he put his books in there. Just like when Michael J. Fox rides his skateboard into the Malt Shoppe and the guy asks why he is wearing a life preserver.

What if it was inevitable that everybody has an animal on a plane? You will need a way to get your animal around town when we don’t have cars. Merlin sends John a link to [https://animalssittingoncapybaras.tumblr.com Animals sitting on Capybaras] and those animals don’t seem stressed out, but they want things on their back. In the same way that we go into the Amazon (rainforest) and pick little rare herbs and grind them up and see if it is Aspirin, we take some bark and some brave scientists wonders what happens if you snort it, or if you put it under your tongue, but they are not that brave and they are doing it to mice, they are the Mengeles of the mice world, they are legend. We are discovering by looking at that website that God already made Capybaras to have things ride on them.

Now we have the data and we know what they are for, that the Capybaras are the comfort animal for any comfort animal. In a world where we are all traveling with our familiars, what do the familiars do when it is their coffee break? They go to chill with Capybaras. This means that one of these Lowe’s parking lots is going to have to be a Capybara landscape that would be full of Capybaras that would totally happy to have things sit on them, and all the animals that are all tuckered out from chilling with their human host just want to sit on a Capybara for a while.

These are economic opportunities for people with foresight and vision. Of course the humans are going to need these support animals because they are going to be wearing VR headsets. Initially, a chariot or a carriage was a pretty expensive thing to build. The roads were not very good, and in Jerusalem in 4AD that wouldn’t be a very useful vehicle and there wouldn’t be a lot of places you could ride in it. Even the roads that the Romans built would be very uncomfortable in a carriage because we hadn’t invented suspension systems yet.

Over time we democratized the carriage. First it was for rich people only, then we used it for moving soldiers and grain, and eventually until the invention of the automobile a carriage was a big part of every family’s wealth. Now we are in a situation where it is very democratized that we have these comfort carriages that can go 80 mph on the freeway, that have air conditioning and stereo systems and you can have sex in the back seat if you have to, you can live in them even, and when we are looking at comfort animals right now we are only seeing the beginning, and the people who are forging the documentation for their comfort animals are the ones that have the long view.

Merlin heard something on the radio this morning about a girl with Cerebral Palsy or Cystic Fibrosis and she needed some help and her family cobbled together money to get her a dog by the name of Wonder and then the school said that she couldn’t have a dog there.

Humans have intervened in the breeding of dogs for a long time, you breed different dogs for different purposes, there are even dogs who can smell cancer and diabetes. John should have one of those dogs for when he feels hypercondriactical he wants that dog to just sniff him and give him the thumbs up.

+ Cancer-sniffing dogs, Pudús that can smell if your are full of shit today (RL222)

John needs one of those dogs and he would let the dog sniff him every morning when he woke up, and if the dog is not singling and is chill, then John would be chill because there were no abnormalities and the dog could go and ride a Capybara for the rest of the day. It doesn’t even have to be a dog, it could be a tiny deer! Because there are no parking lots anymore there are Pudús everywhere.

Imagine a future world where there is a plague of Pudús upon the land because a lot of people bought comfort Pudús, they turn them lose in the yard, and it is like Nutria //(see RL40, RL324)//, and pretty soon they are mating and pretty soon Pudús are out-competing squirrels and you are shooing them with a broom. And yet, what we didn’t realize that like Amazon rainforest Aspirin powder, that Pudús can smell cancer, and they are super-cuddly, so you go out in the yard in the morning, you grab the nearest Pudú, stick it under your arm, and have it smell you.

Merlin wonders if they could tell you if you are full of shit. Today he uses his WiFi scale for that. Sometimes he gains weight when he poops, although he has tested it mostly on peeing. John thinks that people don’t want to know whether they are full of shit and if Pudús could smell that, then they would be eradicated. We don’t know yet how they signal that there is an abnormality at the cellular level, but they know what you need before you know you need it, like a butler scientist.

They are going to brand their Pudú farm Four-Legged Butler. The way that Pudús signal full-of-shittness is the sink or swim for them. If they signal in a way that we can anthropomorphize, if it rolls its eyes for example, then this will endanger the Pudú, but if it signals in a more obtuse way, like if it cuddles you, then John and Merlin are going to be billionaires.




















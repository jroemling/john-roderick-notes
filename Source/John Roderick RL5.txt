RL5 - Carry on, Maude
2011-10-14

This week, Merlin and John talk about:

**The Problems:**
* the Greek dessert crisis (from the root, disagreeos); °
* analyzing the tactical dearth of mousse in the Maginot Line; °
* a shared fate for Ernst Röhm and the Clampett family; °
* disruptive oninonomics behind food that blooms; °
* no Abrahamic sandwiches for Halal the Elder; °
* some phallocentric appeals of flan and its equipment; °
* John’s beef with the compulsory heat of Turkey’s nuts; °
* why hasenpfeffers are expendable; °
* ruined by a young Marlo Thomas; °
* when *M*A*S*H* got all Chachi; °
* line-editing Raymond Carver’s cocktail napkins; 
* accent on the fakeyest Cockneys, Guv’na; °
* formally moving on to African-American-tie racism; °
* efforts to secure Ann-Margret’s hips (and flips) a place on our canonical phalanx; °
* John’s morbid—and ultimately unfounded—fear of Neil Young’s doobie; °
* saving a lovely, dark-haired girl from hipster yodeling; °
* some editorial follow-up on why Merlin could still use a good Pounding

The show title refers to Maude movies on Showtime.

John’s new catch phrase is that he is on the line. Merlin was very susceptible to catch phrases in the past and used to like them a lot, but now they make him extremely angry.

In Seattle there is the Space Needle, the place where they throw fish, that place is kind of cool, but San Francisco? They got Rice-a-Roni, Ghirardelli  Chocolate, and the mobile version of Rice-a-Roni is the cable car, obviously, which is fine. Fisherman’s Wharf. John finds the cable car super-fun, he rode it one time in the front and was hanging off the side and they went by another cable car and everybody gave each other High Fives! *ding ding*

+ 1970s feminists, 1960s TV shows, All in the Family (RL5)

Marlo Thomas in That Girl! That flip! That Girl, Emma Peel, and Marlo Thomas, there is not a woman in the world who could save Merlin from what they have done to him, without a special outfit. Merlin just got a whole truckload of wood from looking at Marlo Thomas’ hair flip.

In the early 1970s it was sexy to be a ballbusting feminist like Marlo Thomas. It hadn’t become trying and deeply profoundly annoying, but it was still hot. ”Tell me that I am Neanderthal and have bad thoughts!” Or Bea Arthur, those Maude movies on Showtime. 

The premier of That Girl was in 1966. Merlin learned the word ”fiancé” from that movie when he was a boy. John learned the word ”moot” from Rick Springfield //(from the song Jessie’s Girl)//, Merlin loves that song and can still play every note of it. John thought that the word he was singing was probably ”mute”, but his 10 year old sister knew better and told him that it was actually ”moot”. That Girl is an important movie because she is trying to strike out on her own and this is the network trying to get into the whole youth culture thing in a way that people will be able to understand.

She is a very important precursor to Mary Tyler Moore. For that show she was initially supposed to be divorced, but eventually it was implied that it was just an engagement that broke off and she was actually never married. The concern on all fronts was that people would think that she had gotten divorced from Dick van Dyke. You don’t want to get him mad because he is an alcoholic. If John was married to Dick van Dyke, just his British accent in Mary Poppins was enough to divorce him. It keeps changing from phoneme to phoneme.

Merlin doesn’t think Norman Lear’s take on things has aged particularly well. You want Archie Bunker //(character from All in the Family)// to be eternal because it was so far ahead at the time. It was like the Sex Pistols or The Clash of TV Shows, people had no idea what to make of it.

John’s mom worked at the oil company at the time and the other guys in her car pool on the way to work would talk about that show not realizing that Archie Bunker wasn’t the hero. It is like with Robert De Niro in Taxi Driver: You can pick which side you are on, you can be a hero or an antihero depending on how much you like political campaigns and Jodie Foster. A lot of people finally found their voice in Archie Bunker and Meathead was very unlikable to a lot of people. Except for Edith they were all deliberately insufferable. Edith was a Dingbat.

John didn’t identify with any of these characters, not even Mrs Jefferson. Every time Meathead brought home some draft dodger or some gay friend there was always some dinner guest with a beard and an Army jacket and halfway through the dinner he revealed that he had been in Vietnam or that he had Hepatitis C. John always felt like that guy. Merlin finds it typified on a day-to-day basis by Lionel Jefferson and he can see John aligning with him: He has an excellent haircut, he is always calm and collected, and he gives as good as he gets.

In the late 1960s Marlo Thomas was part of The Night of the Long Knives (Merlin likes to get a Hitler reference in every episode). Ernst Röhm was gay and they didn’t have a place for the SA anymore, so they decided at CBS that there was too much of this Corn Pone crap like the Beverly Hillbillies, the Petticoat Junction, and all these shows that were turning off the ostensibly urban audience so they cleaned them all out. It was a literal holocaust! Instead they brought in all of these other shows starting with The Mary Tyler Moore Show. John thinks it was Good Times, but that was a little bit later.

Hee Haw hung around for a long time, probably because Roy Clark had a lot of dirt on people. Operation Petticoat was about a pink submarine based on a film. It was about a usual disruption: Now you have women on a boat, Title Nine for uteruses and boats, like Das Boot meets Ms. Magazine. They were painting the submarine and the only color of primer they had was pink and then they had to get out of port really fast and had to sail and were stuck in a pink submarine. You have to work really hard to fuck up a submarine movie. The movie was terrible, but the television show was hilarious, it was like Hogan’s Heroes on a submarine with girls.

Hogan’s Heroes is very near the center of what they are getting at with this program. It may be the bottleneck between the past and the now. When you see that little dog house with the dog in it rise up and LeBeau is underneath there or Kinch (Sgt Kinchloe) or Larry Hovis. John never watched Liar’s Club and doesn’t even know what that is. Merlin thinks it is good. Merlin has been sending John a lot of links which normally bothers John, but this time he girded up his loins.

The other day John made a reference and somebody asked him: ”Oh, you mean like Silver Spoons?” //(TV show)// - ”I never watched Silver Spoons”, the one with the rich kid. Merlin can’t believe that this is even allowed to be a reference. John was too old for Silver Spoons. People two years younger than him were watching it and were thinking that it was as good as a good show. It was a Diff’rent Strokes ripp-off, which was not even a show that deserved to be ripped off. The Facts of Life was a spin-off of Diff’ren Strokes with Mrs. Garrett. 

John liked The Facts of Life because there were some foxy girls in it, especially that one tomboy girl Jo //(played by Nancy McKeon)//, she was tough, she had a Suzi Quatro thing, she was like Pinky Tuscadero //(played by Roz Kelly)//. She fits John’s profile if she just cut her hair and had a little bit more mustache. She was dikey and she looked like she could have been in the band where the girl has a mustache. Queen?

+ Listeners complaining about Merlin's typing noises (RL5)

Merlin got a complaint from a listener who doesn’t like it when he types. You never look at the camera, you don’t talk about the thing you are doing, you don’t clear your throat, and you don’t eat a peanut butter sandwich while recording a podcast. John disagrees!

Merlin’s response to those complaints is that if you don’t want a little moisture on the mattress, maybe you shouldn’t have gone to the bar. John doesn’t get that reference. Merlin needs an Ezra Pound, somebody who goes in there with a big fucking pencil and crosses out whole pages. There is also an online version of that where you can see how Pound went through 5 pages at a time.

That is the Raymond Carver story, the editor who took 900 pages of drunken scribbles and turned it into Where I’m Calling From, but his wife wanted to release the unexpurgated version of the book. It is like 6 cocktail napkins and each one of them says: Alcohol, Suburb, Telephone, Something. ”All work and no play makes Jack a dull boy” Merlin has the loudest keyboard in the entire world that sounds like it is constructed out of pulleys and wheels and John can hear little rubber bands and gears.

+ Flip Camera (RL5)

Merlin had two of the now discontinued Flip cameras, he had also encouraged John to get one, and then they EOL:ed it. Every time John and Merlin meet Merlin holds up some new talismanic electronic device and says: ”OMG, this is amazing, I couldn’t live without this thing!” and then John goes back to Seattle and thinks that maybe one of those might transform his life and he would have more of something he is missing.

He bought the Flip because he pictured himself making 30-second videos of butterflies having sex or that he might become a YouTube phenomenon as the guy on the spot with his little fucking cigarette pack camera and he was going to make these fantastical little videos. He is like Zolat (?) meets (François) Truffaut, he is out there and fucking keeping it real, one frame at a time. He is on the streets of 1880 Paris. Instead he got the thing, he was scrolling through menus trying to set it up, he had to download some access code, he had to read the manual, and he had to download Tor and go on the deep web.

John has a drawer called the Merlin drawer, which is the island of lost fucking PDAs, Blackberries, cameras and a mini disc player, a 4-track that records to double-sided Scotch Tape and records everything into the cloud. The camera really looks like a black package of More 100 cigarettes. Merlin has had several iPhones and to this day the best stuff he has gotten was from toating around that extra Zip. One night they gave their daughter a bath in a little bucket and Merlin shot it all on the Flip and it paid for itself. It was not costly to buy and it was very easy to use.

+ Recording without your pants on (RL5)

Merlin took off his pants for the recording. Maybe John should put his pants on, he never had his pants on for this podcast. It is like San & Jana (?) making the iceberg and the jaguar (?): They are putting their rings together and making something special pantsless in the form of an ice punky (?). That guy was useless! It makes Merlin angry! Was that Justice League? Super Friends? Wonder Twins? Want them (?) and their little fucking dog, was that part of Super Friends? That must be Speed Racer! There is a pretty good Canadian band called Super Friends that the guy from Sloan is in. Merlin should find that for John because John loves Sloan. Anything they touch is solid gold.

+ Having a chili dog from an unlicensed Chinese restaurant (RL5)

Merlin just had a chili dog //(see also RL2)// from a place several doors from here and each week it is getting worse because they are using a relish that is the color of grass in the spring. It is called a Chicago Dog, there is not a fucking license in sight, but those guys are making this shit up like the Carl Sagan thing when they put the gold plate in the Mars spaceship with the people //(Pioneer Plague)//. The song on there was not Chuck Berry, but Twinkle Twinkle Little Star. Mozart didn’t write that, but he wrote variations of that. It is the sound Merlin’s rice cooker makes.

If just somebody had hastily drawn a very simple Chili Dog etched into the gold of the Viking! It came back in Star Trek IV, the whale one that could talk to the whales and told them not to destroy the Earth. It took place in San Francisco. You can still go to that aquarium. Scotty works there now, but he didn’t get any writing credits.

The guys who sold Merlin the chili dog are not licensed to do shit. God bless them! They are Chinese because everybody in Merlin’s neighborhood is Chinese except Merlin’s family and the cops. Is it racist if it is about Chinese people? Probably! What if Merlin gets a Mexican Coke? That is not racist because that is what they are called for some reason. This week Merlin got a Mexican Coke instead of a non-Mexican Coke because he wanted to mix it up. What if you are a Mexican guy and go into a place called Shitty McChiliDogs... 

John would absolutely not order a pesto dog, a hot dog with pesto and cheese that they heat up in a little oven. From the moment Merlin walks into a place he is surveying it if he can work with this place. He always says the same thing: ”A lot of this stuff looks good, what do you recommend?” The red flag answer to that from the cute waitress is: ”I am a vegetarian and haven’t tried anything on the menu, but people say it is good.” They work in a fucking hamburger joint, but they are a vegetarian. That is like working in an abattoir and not liking knives, it doesn’t make sense, like working in a Kristallnacht and liking Jews!

The second worst answer is: ”A lot of people like the gelatin and espresso doglet” That is a West Coast answer that you would never get in New York City or Boston or Philadelphia. People on the West Coast can’t fucking stand their ground while in New York they are already mad at you, which is good, and if you ask them that question they would say: ”What the fuck are you talking about? Get the pizza, asshole!” John went into Katz’s Deli one time and asked to have Swiss Cheese on his sandwich and the guy said: ”If that is what you want? I wouldn’t do it, but if that is what you want!”

+ Different types of restaurants (RL5)

++ Halal and Kosher

Halal is Islam, Kosher is Jewish, it is the same thing from the same book with the same set of voodoo rules. But if you go to either restaurant and ask for an Abrahamic sandwich they are not going to be happy because all the people in both Halal and Jewish restaurants are all from the Dominican Republic now. 

++ Chinese

Is it racist to say Chinese restaurant? No, it is a Chinese restaurant. It is an Oriental carpet, it is Asian cuisine and it is Chinese food. The Ancient Chinese Secret //(laundry commercial from the 1970s)// is dry-cleaning!

What if there is literally a fire drill inside an Asian-American restaurant? You can’t have a fire drill in a place, because by definition a fire drill is when you leave the place. If you are in a car it is a Chinese Fire Drill //(a slang term for a situation that is chaotic or confusing, possibly due to poor or misunderstood instructions)//, but if it is in a Chinese restaurant, then it is a regular fire drill in an Asian restaurant. What if it is a Chines car, but all the cooks are Mexican?

++ South American

Since after High School every single restaurant Merlin has worked in or had familiarity with had been largely populated with people from Mexico, the Dominican Republic and Honduras. It is part of a global plan that one day that last restaurant that still employs an old Italian guy, he is going to retire or die and then the Mexican government will blow a whistle and America will starve.

It is a Manchurian Candidate thing! A lot of the people working in Red Robins and stuff don’t even know that they are sleeper cells. Mexico was the only place you could get a Beetle for a long time when they closed down the Beetle factory. They made these in Argentina, too, which is where you have a steak.

Merlin and John never went to the Churrascaria place when John was visiting, but John has been to Buenos Aires and they are not kidding around when they put steak on fire and carry it around with swords. They are not dressed like Gauchos, but they are dressed in tuxedos because they are 65 year old men that still believe that being a waiter is not just a good job, but a guild.

John and Merlin were together at El Gaucho, which was in the top 5 or even the best steak experience they have had, and that guy was very tolerant and old and he had a shit-ton of dignity. He was not a girl with small ankles and small nipples, but a man near 80 and not in great health. He was not working this job so that he could work on his screen play, but this was his job. In Argentina, Chile, or less in Spain the waiters are old people and they are proud.

Chiaroscuro is an art form and Churrasco is a type of Brazilian steak. Is that one of those things like chocolatey instead of chocolate? They are trying to screw us! At the Espetus Churrascaria in San Francisco you eat food off a sword and they have very large pants. John always confuses Baklava and Balaclava. The later is a face mask that you wear in the Special Air Service or on a snowmobile and the former is some kind of disgusting Greek dessert.

++ Greece

The Greeks are the cradle of civilization, but there is not a single good Greek dessert. Feta? Yoghurt? They have no desserts and that is why their country is fucked! You don’t want to mix up Greek and Turkish people, they do not like that! Merlin was at a place in England and saw a guy with a little coffee pot making a little espresso-style looking coffee thing with a little flame under it, which he would call Turkish Coffee, but the guy said: ”Greek Coffee!”

John has been to the Greek/Turkish border and there isn’t a lot of it because most of the border is the Aegean Sea where they disagree who owns the islands. The word disagree is from the root disagreeos, meaning to contend with. This border is a fortified border with barb wire, Jeeps running up and down on both sides, and machine guns pointing at each other. 

John was walking along this fence line with his little backpack on and it was like walking along the Berlin wall. There were guys with machine guns pointing at him, looking at him through binoculars the whole time. Both of these countries are in NATO, they both aspire to be in the EU, but it is like East Berlin / West Berlin. The Greeks are having a lot of problems with money and a large part of it is that they don’t have a good dessert.

When Merlin was a waiter, all the dough was in dessert, appetizers, and cocktails. He doesn’t know what the margins are on Spanakopita and Gyros or if there is other foods, but they probably have something with grape leaves. You are not going to make a lot of dough if you don’t have a good dessert like a flourless chocolate cake.

The first course at a Greek dinner is tomatoes and cucumbers with some vinegar on it, which is nice, but it is not like a platter of Blooming Onions where you are raking it in! When you look at a Blooming Onion you don’t see how cheap the onion and the grease is, but you see labor and think that a lot of work went in to making that onion bloom whereas if you just cut up some tomatoes and cucumbers everybody can see that this didn’t take anything.

The second course in a Greek meal is some yoghurt with some lemon on it or some Falafel on a bed of lettuce. These are not impressive foods. Also people are constantly breaking plates because they think that is good luck, but that is bringing their margins down. They are shooting blunderbusses in the air and the lead pellets will rain down somewhere. They didn’t learn from Gettysburg.

Greece needs money and asks everybody for money, but they had already gotten money from everybody and it didn’t work out. At this point people could teach them some dessert. The French or the Germans can do that.

Step 2 in the implementation process of Greek gustatory influx is going be to improve the entrées. Merlin doesn’t love Flan //(Spanish cake)//, but some people do. You have to be an 85 year old living in Florida. Merlin thinks it is a fairly phallocentric dessert, he would fuck the shit out of a Flan, but nobody wants to eat a Flan.

++ German

If you do not give a Germans a piece of cake after a meal, they will literally put you into a camp! There has got to be bread because they are crazy with the breads and the sugars. They make pretty good dessert, but they can’t make a fucking entrée to save their lives. A sausage and a potato is a German entrée. They don’t even bother to put a piece of parsley on it.

John has spent months in Germany asking for anything that isn’t a fried pork cutlet and the waiter will tell you that they have a fried pork cutlet with an egg on top. They have Wienerschnitzel, there is the one with riddles on the lid, and you got Hasenpfeffer, but that is rabbit.

++ French

There is so much they could say about the French and Merlin has a hard-on for the French and doesn’t like the French, but they make a hell of a dessert and a pretty killer entrée, too! You can say about the French all you want, but you sit down in a French restaurant, the waiter is going to be a dick, the table cloth is not going to be clean, the place is going to smell like bleach and roaches, but every one of the seven courses they put down in front of you is going to blow your mind and redefine that particular course of food for you. 

One time John was in a restaurant and ordered the chocolate mousse at the end of the meal and the guy came over with a champaign bucket full of chocolate mousse which had spoon-marks in it where it had been spooned out, and he put the bucket on the table and told John to help himself and went back into the kitchen.

John just had a six course meal with probably 40.000 calories and the guy had absolutely put that bucket of mousse on the table as a challenge: ”Here you go! Do your worst, American! Fuck you!” The first spoon full of mousse was like eating a stick of butter that had been warmed by the sun.

John thought that he was going to eat this bucket of mousse, but he could not and after three spoon-fulls he surrendered. The French spent 4 years fighting a trench warfare against the Germans and they had this secret weapon? They were out there, shooting pellets at each other, you had 50.000 men die in one day on the Somme, but you could just have sent this bucket of mousse across? The Germans would have had no defense because they are used to somebody giving them a piece of dry bread and some flourless cake at the end of a meal and they would have capitulated in an hour.

++ Turkey

In Turkey, if you are lucky they will give you some hot nuts. Asking if you would like some hot nuts sounds like a test, but there are store fronts and all they sell is every kind of nut, hot, with a guy behind the counter in a bow tie and a stripy shirt and a little paper cup. After a while John asked for some nuts at room temperature, but you are insulting their culture at that point. John loves nuts, but not hot, and the guy said that he should buy the nuts, take them home, and let them cool. ”Would you like to have a Hertz donut?” and he punches him in the arm //(Hertz donut = Hurts, don’t it?)//.

+ TV shows, characters that destroy the show (RL5)

John can’t stand the British accent of Don Cheadle who tries to do a Cockney accent in the Oceans Eleven movies and every time he is on screen John’s skin is crawling like he is covered with spiders. John has nothing against him, he is a great actor who is in a lot of movies where he plays the same role in every film, which is all right, except for this one movie where he does a Cockney accent. Somebody suggested that John might enjoy the Oceans Eleven movies more if he thought that he was intentionally doing a bad fake British accent, but John hasn’t tested that theory out yet.

Like All in the Family, MASH is another thing that was a little heavy handed pretty quickly and has really not aged well. Particularly the later episodes you can’t even watch because it is like being sent to the principal’s office. Like Happy Days and so many shows that were on for too long they got less and less careful about even trying to make them look like it was that era. Chachi (Arcola, from Happy Days)? That was how people had their hair in 1961? Everybody looked like John Cazale in Dog Dag Afternoon. What are you guys doing with those haircuts? You are in the Army! Chachi was the Scrappy Doo of Happy Days, like Oliver on The Brady Bunch //(see RW106)//.

They had that lame attempt of one episode where they were trying to make a spin-off through multi-racial kids. Oliver (played by Robbie Rist) was like a 3-foot tall John Denver. He was on Big John, Little John with Herb Edelman and he is in bands now as a producer. The ultimate example of this is of course the little Coolie child in the second Indiana Jones movie //(Indiana Jones and the Temple of Doom)//, but he is not Coolie but John just tries to keep up with Merlin’s casual racism. Merlin wants to move on to black-tie racism at some point. No-one will know what the hell they are talking about because they will be drowning in euphemisms.

+ Ann-Margret (RL5)

John wants to talk about Ann-Margret in that Who movie //(probably Who Will Love My Children?)// where she was the mom, or the opening sequence of Bye Bye Birdie. She made some appearances on The Dean Martin Show //(for example [https://youtu.be/cdvT2tAGRrk this one])//. She is every bit as foxy as the foxiest thing that ever walked he Earth. She is a different strain of hot girl for Merlin, she is not in his canonical phalanx of hot girls.

She is so red-headed and so petulant, biting her lower lip, and flouncing her hair around, a thing girls don’t do now anymore, although Zooey Deschanel probably does. Merlin thinks it is something a slutty girl would do if she wants you to pick up the tab, but all girls used to just flip their hair as a thing, just like guys were wearing hats. She flips her hair like an Olympic fencer. Every single little flick of her hair is intentional.

There is somebody with a fan right behind the camera pointing the fan at her and giving her a little bit of wind. The fan guy works for Neil Young now. They all have huge handlebar mustaches, they all look like muppets, it is amazing! It is like being in a biker bar, except everybody is super-friendly.

Merlin is still looking at that video of Ann-Margret and if God would just let him see her hips in a hip-lineup he could tell if she was a red-head, he could tell that from the side, even! You don’t get a lot better than this and Merlin hasn’t even seen Bye Bye Birdie. She is doing a lot just with her face. There is a clip with her and Elvis and inexplicably you got Christina Hendricks from Mad Men. Mad Men does that thing: ”What happened? But now what happened?” and they did a Bye Bye Birdie-ish thing in a commercial, which is what that clip is.

+ What if Neil Young offers you his joint? (RL5)

One time John saw a Neil Young production from the side of the stage and everybody who works for him has worked for him for 45 years. Neil Young is reserved and extremely down to Earth. He is a little shy, some of it is because he is a stoned. John shook his hand and it was so soft and so gentle and they talked about model trains. He is a big model train guy and John knew that and he also likes model trains.

Neil was a figure for John. When you stop doing drugs a lot of stupid shit pops into your head for months and years afterwards, and one of the things that popped into John’s head when he stopped doing drugs was: ”What are you going to do if one day Neil Young offers you a hit off of his joint? Are you going to say No to Neil Young offering you a hit off his doobie?”

For 15 years of not doing drugs it was in the back of John’s mind and he didn’t have a definitive answer. This was the test! It is one thing to not want to search the carpet for that little piece of crack that somebody thought they dropped! It is not like Dennis Wilson is asking you to look through the mats in his car for coke, or Bryan Wilson sitting in a cat box, wondering if this is hash or cat shit.

John had a short test of it when he one night was out to dinner with Mike Mills of R.E.M., a guy he admires, and he ordered probably eleventeen bottles of wine and John was thinking to himself if he should have a glass of wine with Mike Mills because this was a big moment for him, but in the end he did not have a glass of wine because it was not Neil Young offering a hit off his doobie.

When John was backstage at the Neil Young thing, Nick Harmer from Death Cab For Cutie grabbed John and told him to come with him, got him away from the herd and out in the hall he told John to just stand there and talk for a second, just making chit chat because Neil Young was going to walk out of this door and Nick wanted John to meet him.

At the very moment the door opened and Neil young walked out with his wife and his band and everybody was smiling, they walked right over and Neil said: ”Hey Nick, how is it going?” - ”Great, Neil! I want you to meet a good friend of mine, John Roderick!” - ”Hey John, how is it going?” and he put out his hand they were standing there for 10 minutes talking.

This meeting was a gift from Nick Harmer, it says volumes about Nick, but the only thing John could think of was: ”What am I going to do if he offers me a hit off his doobie?” The smell of pot was wafting over them as they were talking and you could hear the crowd chanting ”Neil! Neil! Neil!” from the basement of this stadium, Neil was on his way to the stage, they were not stopping to chat on their way to the bus, but they were in their show clothes, headed to the stage, and he took 10 minutes to talk to John who was just this guy who didn’t know what to say: ”So, the Bridge School Benefit is a cool thing! H0 model trains are a good thing!” John went into Chris Farley mode.

The whole time John was thinking: ”Don’t offer me a hit off of your doobie! Don’t do it! I don’t want it! I want it, but I don’t want it!” and he didn’t, thank God! John was probably not giving the doobie vibe and he has been given the doobie vibe and he knows what that is. In San Francisco where Merlin lives it is the primary vibe. Both of Merlin’s neighbors downstairs must have some very bad glaucoma because it is a kind of medicine.

John wanted to talk about Neil Young’s guitars with him: ”That one guitar, can I touch it sometime?” and eventually they went: ”Okay man, see you guys later!” They didn’t say: ”We got to go because there are 20.000 people chanting my name!” He should have said: ”Hey Neil, one last thing: What the fuck was up with that Wonderin’ song?” John loves that song, that whole era of Neil Young is great and John likes Trance, too! The reason for that record was that he was trying to communicate with his kid and they shouldn’t make too much fun of it. Merlin thinks she shouldn’t have done Broken Arrow or Mr. Soul, come on! John is a fan of all that stuff. 

The other day someone was talking shit on the Internet about the song T-Bone from the Re·ac·tor (Reactor) album like it was some kind of bad thing and John replied. Re·ac·tor and Gene Simmons’ solo record (called Gene Simmons) were in Camelot (Music store) as Cut-out until probably to this day. Re·ac·tor was like the Frank Zappa orchestral record (Orchestral Favorites) with no vocals.

These were the records that were available for $0.99. Ship Arriving Too Late to Save a Drowning Witch (album by Frank Zappa) kept Gene Simmons pretty good company all the way through High School for Merlin. John owned them all, some of them stayed with him and some of them went out into the snow.

One of the best things you could do for a kid today was given them Decade (album compilation by Neil Young), which used to be a triple album. There is not a bad song on there. Each one is better than the last! John dated a Hipster girl for a long back in the old days, when girls would be near him, who was a big fan of the Yodeler and she liked the Cat Power, the autistic Indie Rock yodelers like Neutral Milk Hotel, Bonnie Prince Billy (Will Oldham), the singers who maybe can really sing, but what they do instead is yodel.

John would listen to these records always playing in their house. She was a hipster and she was very smug about music. It was the kind of thing John would tolerate for his cute girlfriend, it was the lovely dark-haired girl hat Merlin met, and they have been dating for years. Merlin thinks she was far too pleasant for John, but he never saw the unpleasantness.

At one point John asked her if she had ever listened to Neil Young and gone to the source and listened to the real thing of this and she went: ”Phhht! Neil Young! Classic Rock!” She had never listened to Neil Young and John got her Decade on vinyl, there was nothing she could say about it and she couldn’t turn her nose up at this.

By the gifting of that single album John had transformed her idea of this thing that she loved. She already loved this kind of thing and then she had this record which moss would not grow on because it is an eternal thing and it became the only thing she listened to. Because she was a Hipster person, the more impenetrable a thing is at first, the assumption is that it is better.

+ Different Riot Grrrl bands (RL5)

Merlin thinks that Broken Arrow and Mr. Soul are both a bit of a slog if you are not ready for it, but if you have been listening to Cat Power B-sides? Merlin just accepts that people love Cat Power, like Sleater-Kinney, and it is good, but he does not get it! It is nothing compared to Heavens to Betsy, oh my Christ! Not Heavenly //(British band)//, which was a great girl singing band.

Don’t get John started! If they are going to start talking shit about Riot Grrrl //(genre)//, they are going to need another 2.5 hours! Merlin thinks that some of it was fine, but there was just a whole lot of bad. John suffered through that era and he was not meant for those times.

Merlin opened for LOWES and Tiger Trap one time back in the day and LOWES was delightful. Depending on who you were working with you would have been sequestered from the backstage area because it was women only and they were throwing their tampons at each other. She was actually really sweet and she signed Merlin’s record, which might be a euphemism.

+ John playing Minesweeper while on the phone (RL5)

John was prepared for having a schizophrenic conversation with Merlin, like those conversations he used to have when he was playing Minesweeper. He would be on the phone and he would be playing Minesweeper, back when he had IBM computers. He could talk about the music business with somebody on the phone for an hour and be playing Minesweeper the entire time.

It was a perfect place to divide his attention because the music business conversation was not enough to have all his attention, but the Minesweeper game took the unused portion of his attention and gave it something to do. Otherwise that unused part would have been trying to sabotage the music business conversation by asking impertinent questions and that is bad.

+ Breaking Bad (RL5)

Merlin tries to reduce the number of shows he is committing to. He is already behind his wife on Breaking Bad and he had to break off. It looks really good, but he can’t keep up. There was a lot of talk about Breaking Bad on the Internet and it was so laudatory that John didn’t know what to do with that information and he has never seen an episode of it. Merlin thinks John should check out Arcade Fire.

+ Various bands, Weezer (RL5)

Handlebar Sue is a band from Olympia, the Olympia near Saskatchewan. Nobody apart from Merlin has ever heard of them. Eskimo Iceberg (?) put out all their records on Scotch Tape, double sided, John loves these guys. Those Scotch Tape tapes were incredible, but you can’t get them anymore.

The vibraphonist from Spunk Flower was originally in it, but not anymore. He was a Drag City //(record label)// guy and he played the other side of the vibraphone very quietly. When he left the band, their biggest fan had learned all the upside-down vibraphone parts and was able to just step in on the job.

They brought him up on stage when Keith Moon passed out. Now he is in the band and is sharing songwriting credit. That is kind of a thing, like that Hoot thing that happened at the Cow Palace in San Francisco. That really happened, but that guy wasn’t very good and they didn’t replace Keith Moon with him. Passed-out Keith Moon would probably have played better.

One oft the lead guitarists of Deerhoof quit the band and they got their biggest fan out of the crowd. Isn’t that what Judas Priest did? They had he fake singer for a while. Journey got the fake singer from the Philippines. What about that Weezer guy that is all muscle-bound? He might be a session guy who works out. The drummer from Weezer and the drummer from Radio Head: Discuss! Merlin is slightly acquainted with the drummer from Weezer (Patrick Wilson), just from doing a podcast with him and he was extremely nice.

Merlin has a fair amount of Weezer knowledge, but it goes way down after their second record. He is one of those Pinkerton //(album by Weezer)// people. One of Merlin’s most moving Pinkerton moments was walking around Seattle last time he was there. He walked all the way to the R.E.I., it was raining, and he had a very moving Pinkerton thing happen. John recalls that Merlin was carrying a flaming torch, it was a thing they were doing for the fan club for breast cancer. Merlin then ran into all those people coming the other way with torches and pitch forks, but he was not one of them, he was not a racist.

John never got into Weezer, it is like Blink 182 or Dora the Explorer: John feels like he was too old. Merlin gets upset because he was such a gentleman about John’s entire turgid Neil Young anecdote earlier in the show by not bringing around Dinosaur Jr. whom he is pretty convinced John has never actually listened to. John listened to them before they even had a Jr. on their name, when they were just Dinosaur.

Merlin thinks that is not a good record, but You’re Living All Over Me and Bug are two records that John should listen to. It sounded like the singer had a Bran Muffin in his mouth, but that was part of the effect, it is called the Boston Bakery, they teach you that at Amhurst. John doesn’t believe that, but he was just too old for that stuff.
















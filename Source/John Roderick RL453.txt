RL453 - Guardstone
2022-02-15

This week, Merlin and John talk about:

**The Problem:** There’s Magic John and Science John, referring to the different internal character voices in John’s head.

The show title refers to a thing in curling and Merlin suggests that it should be the name for the place where John will put Dick Cheney in his shipping container in the desert.

The audio starts with a 2-second clip of the song Mississippi Queen by Mountain.

In the middle of the episode between minutes 26-45 the audio of the two hosts starts drifting apart and is almost unlistenable.

John starts singing Merlin’s name in the usual way. Merlin replies with something undecipherable. John goes: ”Are you a sight for sore eyes!”, ”In this economy?” Merlin asks John if he is a magical thinker, but then diverts for 20 minutes, talking about TV shows like Mr. Show because John says that he has to call upstairs.

+ Mr. Show, different people associating actors with different shows (RL453)

Merlin has been watching a lot of Mr. Show and his kid is pretty into it. He references the sketch ”Change for a dollar” There are some episodes like the Jeepers Creepers episode, No Adults Allowed and Sulu the Australian Hot Neck lizard where Merlin can just about recite the whole sketch. He always looks forward to Bob Odenkirk singing, like: ”The laserbeam of love” He also finds it so funny when Bob Odenkirk goes: ”God dammit!” It is a really special show and so far ahead of its time.

John saw it first in 2000 when he was the keyboard player in Harvey Danger on a VHS tape in the tour bus, brought to them by Jeff Lin, the least likely band member and somewhat humorless guitar player. John had never seen the HBO static before and every time it pops up on any screen he will think about Mr. Show. For Merlin it is pretty close between Sex and the City and Mr. Show. As with everything, like the first time you see the one with the New Zealanders and Eugene Mirman called Flight of the Conchords, or the first time you hear the first Jane’s Addiction record, you are going to think you don’t know about that until later it hits you like a ton of bricks.

Merlin thinks Flight of the Conchords is a good example of that because he is balls deep in What We Do in the Shadows. Like The Mighty Boosh you think: ”What the fuck was this? Who is this for” John tried to show The Mighty Boosh to his little gang of adults, but it did not translate to the modern day. It was too many drugs. 

For Merlin Mr. Show stands next to Monty Python and they quote it a lot, like scenes from The Godfather. At the time he was pretty into Comedy Central, but that was so repetitive and you would see the same ads over and over. He was super into Dr. Katz, and then Mr. Show and The Velveteen Touch of a Dandy Fop might have been his first episode //(S2E06)// and it became the show he would watch every Friday night while his lady friend was getting ready for them to go out on a Friday and when he showed up at the clubs he was already pre-jazzed.

The first time John saw the British Office was in Merlin’s house in his underwear on Merlin’s TiVO and he first had to explain what a TiVO was. How many times did they watch that 3-second segment of Charlie Rose interviewing Jeff Bridges? John doesn’t even smoke pot, but that was top-level smoking-pot-behavior. Merlin had been flipping channels and had landed on that scene and Jeff Bridges seemed super-high and it was very funny when he said: ”I like… errrr… line reading!” and then they all in John’s band had a version of that and Eric Corson used to say it all the time.

Merlin imprinted on Mr. Show so hard and he would videotape it every Friday night. A listener was kind enough to give him all of the DVDs, but Merlin had already gotten them off the back of a truck and they are all on his machine at home. There is one called Life is Precious and God and The Bible where they are in a life boat with 5 people. It all seems like it was ahead of its time, but looking back it was what they were talking about then, and now all that stuff seems new to us, it was what John wrote about in the 1990s, he was saying all the same stuff, except he was calling it The Ukraine.

John can’t hear the voice of The Ice King //(Tom Kenny)// without immediately thinking of the skid Druggachusettes. It was a lot to digest when it first came out and you had to get a feel for it. Merlin loves the show Community, where John met the guy //(Dan Harmon from the [[[All the Great Shows]]] bit)// and got really into it last year, but when he thinks about Chevy Chase he 95% thinks about season 1 of Saturday Night Live where he was the break-out star, or about Fletch or Caddyshack. John still thinks of the two movies Chevy Chase made with Goldie Hawn: Four Play and the other one (Seems Like Old Times). Merlin thinks of Bob Odenkirk 95% as Mr. Show while his kid knows him as Slippin’ Jimmie from Better Call Saul. He was in a crazy action movie that nobody saw called Nobody where he trained for 2 years. 

All of that is predicated on the audience at the time having still only monolithic media in common with each other. Mr. Show makes mockery of 50 media properties that you had to understand in order to really get the jokes, and Community, too! It is a mockery of a kind of thing, a mono-culture, be it Happy Days or The Six Million Dollar Man. That is what they talked about at recess the next day because of course everybody had seen Happy Days, it was what you watched when you were 9 yeas old, no subsequent generation will ever have that kind of referential humor.

Who was even the audience of Happy Days in 1977? They were not making that TV show for kids, it was made for adults. Garry Marshall was cashing in a little bit on the combination of Grease that had been big on Broadway and American Graffiti, which Ron Howard was in. John always thought it was Sha Na Na at Woodstock. Happy Days, Laverne & Shirley, Three’s Company, or The Love Boat was all adult entertainment and it is incredible to think that it was what middle-aged people were sitting down to consume together with their kids. John didn’t get all the innuendo that was on The Love Boat or all the politics that was in every episode of M*A*S*H, but he was there at his parents’ knee. John’s mom loved Magnum, P.I.!

Merlin’s mom hated TV and very overtly did not watch TV and she despised the fact that TV and Coca Cola were Merlin’s favorite things in the world. John’s dad loved TV and his mom talks about him loving TV back in 1959. He loved The Tonight Show and he loved watching sports on TV and he loved the late movie. His mom had strict rules and on Tuesday night they could watch TV from 7-8pm, which was Happy Days and Laverne & Shirley, on Thursday night they could watch it for another hour, which was The Love Boat and Fantasy Island.

Melin thought that was on Saturday, but John never watched TV on Saturday. Saturday was the graveyeard and Merlin remembers it was a big deal when they moved All In The Family to Saturday. Merlin is now much older than Carroll O’Connor was when that show started, and of all the things that one freaks him out the most. He is now 55 and Carroll O’Connor was the canonical 50-year old man when the show started and he was only 47 back then. They are going to [[[x makes y look like z |make the 1970s look like the 1940s]]]!

+ Magical thinking (RL453)

John is conscious of magical thinking, it is an internal conversation between Magic John and Science John. Magic John never says anything where Science John hasn’t something to say and it not necessarily vice versa, but almost. Neither one believes in copper pyramids, he doesn’t believe in manifesting things, but it is variations on scale: This seems regular, at a different scale it would seem magical, at a larger scale than that it would seem regular again, it feels like magic is always a question of how far in or out you zoom. When the tide is out you see all the starfish on the beach, but when you speed the camera up all of a sudden the starfish are crawling around, socializing with each other, and you would never have known how active they are because you can’t see infrared.

Merlin starts talking about the availability heuristic when you say that every time you wash your car it starts raining, and he thinks that it is a kind of magical thinking to let two completely unassociated things guide your logic and cognition in a way that you haven’t interrogated. For John when something goes wrong he always blames it on himself. He doesn’t think God is watching him or puts obstacles in his way or out of his way. He always gives him 10% more than he can handle at any time, he is a real joker! Any kind of magic that is based on the idea that there are sprites in the forest or a God looking down… he never attributes sentience to happenstance, but he definitely goes: ”Well, you crossed the street at exactly the wrong time!” - ”Why are you criticizing me? I crossed the street and I got hit by a water balloon, that is not my fault!” - ”Who’s fault is is then?”

Magic vs science are just two sides of amazement.

+ Merlin banter (RL453)

Merlin says that he may be John’s sin eater because as John’s sleep improved //(see RL450)//, his sleep disimproved and he only slept for 3 hours. Two hearts beat as one //(song by U2)//, Merlin had a 12” of that, there was a dance remix of that. Merlin is into Bangles, while John loves that Bananarama shit. Bananarama didn’t have the charts-topping hits.

Merlin thinks that all of the Olympic winter sports are the same sports, it is just go faster when it is cold. He got into Curling and he finds it all so confusing. John used to watch that for hours on television because they are close enough to Canada. There is a guardstone in Curling and that should be the name for where John puts Dick Cheney //(see [[[Dreams and Fantasies]]])// and you could say: ”We have renditioned him to Guardstone!”

+ John and his daughter’s mother watching a lot of TV recently, recognizing a TV show track as being by Mountain (RL453)

John’s family is in a TV habit right now, which is not a thing he has been in very much, but his daughter’s mother is a TV watcher and loves it in all of its forms and she will watch Love Island Australia all night long. This is why Merlin thinks that he and John should wife-swap because he likes talking about what that person was in. John agrees that Merlin and John’s daughter’s mother would love each other and would sit and talk all day about TV and they would watch House Hunters International six episodes in a row.

They are in a TV habit when they will watch a little Star Wars with Dark Vader with their little girl, then put her to bed, and then they will watch an episode or two of episodic television, but then you run out, you get to the end, you are not really ready to start another thing, they tried watching the Olympics, but that is televised terribly on NBC and YouTube, so John’s daughter’s mother suggested they watch a movie, which they don’t normally do, and John suggested the Tony Soprano movie //(probably The Many Saints of Newark)// and about halfway through there is a track that John didn’t know, but it sounded like Mountain, a bands that had just one undeniable solid killer track Mississippi Queen.

John was playing a lot of shows back East in some of those venues like The Fillmore, which is now only in San Francisco and there is no more Fillmore East in New York, but back then there were big clubs where all the bands like The Allman Brothers or the Grateful Dead did East Coast circuits, and those venues always had a framed pictures of Leslie West //(guitar player of Mountain)// with an inscription like: ”Mountain played here 16 times!” and he was always taken aback because Mountain just has that one song, it is like Funk #49 //(song by James Gang)//.

When that track started playing in the movie John immediately knew that it was Mountain although he had never heard the tune before, and he wondered if this was like Baker Street where you think that it is the only song on the album and then you listen to the whole record and realize that every song on there is just as brilliant. Is every Mountain song just as killer as Mississippi Queen, just none of them ever made it onto the radio?

John went into a deep dive of Mountain 12” and they are an incredible band! In the 1980s they used to party to Steppenwolf! Who puts Steppenwold on? And even Steppenwolf has 5 great party tunes! Why did none of them have a Mountain record? Talk about magical thinking! How would John’s life have been different if he had listened to Mountain instead of Steppenwolf? Merlin had the same experience with the song Smokies by Barefoot Jerry from a Swamp Rock compilation of the early 1970s and it makes him feel like having a very incomplete education.

+ Rock music with swing being outlawed in Seattle (RL453)

The kind of music that in Seattle is called Choogle was banished from the music scene all through the 1990s and 2000s, anything that went chicka-chicka or had any swing at all. It is like the gated snare: When John first started making records in Seattle if you said the word ”gated snare” everybody had to take 15 minutes off and go sit outside and think about their lives. You couldn’t use it even though it was a great idea!

John came up in a time in the 1980s when everything they listened to went chicka-chicka, all of ZZ Top, Aerosmith, it was in all of Rock and it was taken from Funk. Into the early 1990s one of the things that set John outside of the local Rock culture was that everybody was self-consciously coming from the Punk side and Pearl Jam wasn’t, they had a lot of chicka-chicka in their stuff. John was in a band with his best friend Kevin from High School and he really loved Choogle.

Merlin had never heard that term. If you break Pearl Jam down, the rhythm section and everything that is happening at the basic level of the tune is very funky, and Modest Mouse is, too, just that Modest Mouse is a Funk band with a guy on top of it that has been sniffing glue. John’s musical journey through the 1990s was gradually weaning the chicka-chicka out of what he did and by the time you get to The Long Winters there is no Choogle left in him, even though it is native to him.

+ Comparing your problems to everybody else’s problems (RL453)

Merlin watched the big game yesterday //(the Super Bowl)// and when Dr. Dre was playing his very memorable keyboards he thought of John who on his first album has a credit for Dr. Dre keyboards. Dr. Dre also famously told the world that the cello is the most sinister instrument and John carries that with him everywhere he goes.

Merlin has very irregular sleep at the moment, sometimes 12 hours, sometimes 3 hours, and he has a very big omnibus project on the way, a triple-decker, a wizarding-bus, where he is working on a lot of different things in the long term that involve trying to get energy right, but he also needs a unified field theory about time off. He is working on a bigger effort about direction of energy and known amounts of time-away time off, and last night he realized it is not optional and he needs to get better at this and potentially not apologize for it. He and his friend Alex call it <inaudible>, the dependent clause that you have to append to every sentence to express that you know you are really fortunate and other people don’t even have 3 hours of sleep.

John laughs mockingly like a guy who is 27 years old and makes $250.000 a year designing background characters for Angry Birds and has a $2.5 million condo and has one poster on the wall of a white Lamborghini, a bean bag chair and a cube of tungsten, which is very fun to say. 

Merlin tries to avoid Rex Chapman type shit, he doesn’t like ”Oh, here is the thing I found!”, but this one got him yesterday as a name for a thing on his list of things that need a name //(see [https://twitter.com/g_s_bhogal/status/1492255238128025602 this thread of tweets])//: ”4. Relative Privation, an all-too-common fallacy where people dismiss a concern because something else is worse. “How can you talk about X when Y is happening?” By this logic, how can anyone ever talk about anything other than literally the single worst thing in the universe?” //(by [https://twitter.com/g_s_bhogal Gurwinder])//

They used to one-up each other in the Vegan community when someone was wearing leather shoes.

During the last year John’s daughter will be sitting somewhere, doing the thing that all kids do, which is like: ”Meh, I am bored! This is dumb! There is not enough Macaroni and Cheese! This ham has been sitting on the counter for 6 weeks!” and as a parent you inevitably start saying: ”You have 17 Barbies, admittedly 15 of them are hand-me-down, which is a lot of Barbies, and to complain about any aspect of having 17 Barbies in unseemly!” and somehow something got into her from somewhere else and about a year ago when John said something to this effect her face collapsed in shame and she said that she felt awful that there are people in the world who don’t have any Barbies and she was so entitled.

John told her that she didn’t have to go there and everybody in the world is different and has different things about their lives that are great and that are hard, and she is not responsible for that and doesn’t have to carry their weight on her shoulders, but it is just important to think about your context and check in with the fact that she has a very nice life. She doesn’t have to compare it with other people and she is not responsible. Merlin goes on a rant how kids need a way to get their emotions out and how he was constantly ashamed as a kid and is now overcorrecting and doesn’t want his kid to feel shame.

John thinks what Merlin just said: ”I am not trying to send a message right now!” is such a great phrase that is T-shirt worthy. Sometimes you just have a bad day and there is nothing behind it. Merlin doesn’t want people to ask people why they don’t do something or why they stopped doing something because it implies that there was a decision when often there was not. Can’t we just have an emotion that is not moored to something that just happened or is trying to telegraph something about what you want to be different in the future? That is how you make the people around you completely fucking crazy!

+ John’s daughter being very different from him, getting into a manipulative phase where she is performing feelings (RL453)

Merlin refers to his [https://github.com/merlinmann/wisdom/blob/master/wisdom.md Wisdom Project] where he says: ”Your kids are not little versions of you; they are little versions of themselves”. John’s daughter is very different from him and a long time ago they both had to understand that and they have a language for it now which freed John from the thing that plagued his parents: They both could only think of him as little proxies of themselves and when he behaved differently they couldn’t understand how this could even happen.

Now that she is older they have a shared language of ideas and John can explain to her the fact that he cannot get inside her motivations and tell her how to love or whom to love or how to feel, he can only tell her what he would tell himself and he can only listen to what she thinks about that and whether or not that met her needs. They are entering a phase where she is trying to be manipulative and she will for example perform being sad so that the person who just criticized her will get off her back. John explained that this behavior is visible to others and although most people won’t challenge her they will feel resentment because she is manipulating them, and she will think she is getting away with it, but others will store a little kernel of a feeling about her. John’s job is to help her learn how to make herself happy and he can help, but he can’t do it for her because he doesn’t know her emotional life. Merlin always hated when people said it is not his job to make his kid happy.

Merlin talks about the phrase Vatican Cameos from the Sherlock Holmes TV show that is used a bit like a safe-word between Holmes and Watson when shit is about to go down. Sometimes he just wants to be sad without it having a meaning to others.

John’s daughter is old enough now for him to understand that she does not have any of the mental difficulties that he has, but what she has is anxiety. John never felt he had anxiety for decades, and when he did confront it it had morphed into 1000 other things, while hers is still very pure. Yesterday she had a bad day, she broke a thing that belonged to her mom and her mom was mad and a get-together with friends go cancelled and she is burying it all and at some point she knocked over a can of Seltzer and immediately picked it up, but a little tiny bit of Seltzer went on the carpet, which probably only improved the carpet, but she lost it and didn’t know what to do. John is very familiar with that kind of brain freeze of: ”I don’t know what to do and also: This is the worst thing that has ever happened!” and she just completely melted down. 

John explained to her that the Seltzer thing is not what happened, but she had a full day of things that happened to her, and learning how to know what is happening when your brain is actively trying to confuse you about what is happening, is very hard and it requires a long time of thinking and practicing and reflecting back on times like this. Her pouring 2oz of Seltzer (60ml) onto the carpet is not a big deal, he could pour an entire can of Seltzer on the carpet and all it would do is mitigate the fact that the cat probably wiped her butt on the carpet right there and the Seltzer lifted the cat butt into the room. Cat Butt was real in that instance //(see RL62)//!

What then happened to her in her emotions and her mind is not related to the can of Seltzer, it was just a trigger, and she doesn’t need to understand that yet, but she needs to just note that. She is very much an individual person who is already encountering difficulty with the way people respond to her at school and out in the world because she is intense and she feels and John can’t manage her feelings and he can’t even interpret them, all he can do is tell her that these are feelings and feelings are real //(see RL363)//. She is almost 11 and these are just the preliminary feelings, but knowing that they are real and that we have feelings and we also have feelings about feelings, which is one thing about her: She has real feelings about her feelings, and thankfully she is willing to talk about her feelings with John and they have a language, and he hopes that she will keep the level of questioning that she has about it.

John tries to develop this topic a bit more, but Merlin ends the show by asking: ”You wanna swap?” - ”Sure, would be fun!” - ”What do you go?”

















RL205 - Space Clarinets
2016-06-27

This week, Merlin and John talk about:

**The Problem:** You’ll find somebody with a monkey, referring to

The show title refers to vape cigarettes.

+ Merlin using a password manager (RL205)

John really likes Merlin’s new Skype avatar of Dr. Head or Dr. Scarface or what it is called. Merlin is trying a new thing where he changes things periodically. He couldn’t say if he recently changed his passwords. Merlin would never choose a password he can remember and he uses a little computer program that remembers his passwords for him and that has a passphrase, which is a long sentence, which lets him get to individual passwords.

Merlin doesn’t want to talk about it more, maybe the phase is: ”Send me some photos of your keys!” or ”It is not what Mr. Finnell did” because everybody knew what Mr. Finnell did!” //(see RL11)// ”He came prodigiously in his shorts.”, but he changed all the O’s to zeroes because he is leet. John learned about that on 4Chan, he is not a noob. His trick move is the inverted Möbius.

+ Making fun of vapers, knowing if a thing is a fad or will be normal in the future (RL205)

Starting a few months ago, whenever Merlin would see some kid vaping around the neighborhood he would say in his head: ”Sick vape, bro!” and increasingly he would say it under his breath. In the last couple of weeks he would say it to them aloud. Now he is doing it even for skateboarding tricks and says it to them when they don’t land their trick, and it feels so good and so right. There is a guy where you can’t tell if he is tribal or lives in a car, he could be 16 or 60, and he keeps trying to jump his skateboard over the bike rack and he keeps making a big noise and falling down and doing it over and over again. Hakuna Matata, Merlin admires his persistence for feeling the need to ride a children’s toy over a bike rack.

When John thinks about a vaper he is thinking of Scott Simpson, but he is not really a vaper. It is one thing to have a Wizard cigarette and it is quite another thing to have an R2-D2 dildo. This is probably what futurists from the 1940s thought it would look like if you had Polio or Asthma, but in 2016 this is what you would use. John sings the Star Wars Cantina Band melody and then calls a vape for a Space Clarinet.

Merlin thinks it is one of those things that perfectly encapsulates the silliness of a certain generation and what we are going to laugh about in the future. In movies from the late 1980s there is going to be a kid with a tall fade (haircut) and some people in neon clothing, which jumps out at you as being weird that we all did that and it seemed normal for a while. River Phoenix hair seemed like a fad, the long whispy bangs across your face, the Leonardo DiCaprio Titanic hair, but it turns out to be a perennial style and we are always going to have River Phoenix hair. John had it many times.

In the early 1990s the first wave of Ravers were Madchester people, but the second wave all had pacifiers and giant pants and shop glasses, they were very Burning Man with goggles on. House music was a thing and then the Madchester came along with bullshit like Happy Mondays which Merlin can’t believe anybody liked, it was a lot of stomping and screaming and it wasn’t very good and the guy couldn’t really sing and the music wasn’t any particular kind of music, but it was of its time and people would dance around to that because they were English.

With the second wave of Rave John was afraid that, like with the Space Clarinets, this could either be thing that we were all going to laugh about, or if we had turned the page and everybody is going to be doing this in 15 years and we would never remember a time without it. So many things right now are like this where you don’t know if this is going to be the beginning of a thing and this is a lasting idea. Right now there are many fashionable ideas about what the future is going to look like.

John’s whole young life he used to say: ”Dad, you don’t know anything! Herp-a-derp”, but as he got older he realized that his dad knew some stuff. Now we are in a cultural moment where a whole generation says: ”You don’t know anything!” and maybe they are right and maybe some of those ideas are a cultural cul de sac, it is really hard to know. You can’t always judge it by how well they pull off the fashion part because there could be a seed of a fantastic idea buried in all those clown pants. Merlin makes the example that the Civil Rights Movement were all very well dressed.

In the early 1980s John could see a lot of dancing in the TV show Buck Rogers in the 21st Century that revolved around people each grabbing one end of a scarf, sometimes a long gauzy one, waltzing around as though around a May pole, except there was no pole and they were just holding the scarf up in the air dancing. It seemed like a mating dance, but they were obviously very sophisticated space people of the future and it connected to the dancing they did in Bill & Ted’s Excellent Adventure, the guitar swooping, everything in the future as seen from the 1980s involved some slow hand motion dancing.

Now you look at that and wonder if we are closer to the future with the goggles, the clown pants, and the vaping. Looking at a picture of Malcolm X in 1960 or the people in Selma, there is no better fashion in history than 1959-1962. Just like the cars of 1967, why don’t we just go back to that? If John would be doing the nostalgia thing and argue for going back to  1987 cars it would sound crazy, but to say 1967 cars from before he was born doesn’t sound crazy at all. The problem is that people do try to do that, but the things that people make today look like a box that arrives from Amazon and happens to be painted orange.

+ John getting a drawing of himself made on the Charles Bridge in Prague (RL205)

One time John was walking across the Charles Bridge in Prague which is covered with people doing drawings of you, it was not the bridge where he got mugged, that was another town, there are organ grinders and puppets and surely you will find someone with a monkey, a lot of people are drawing, there is juggling, and half of the tourists in Prague only go across the bridge to go back across the bridge. The same is true about the Golden Gate Bridge in San Francisco. Yesterday it was so fogged in and windy and there were so many people taking pictures of themselves being cold and Merlin felt bad for them.

One time John stopped on the Charles Bridge and decided to send a drawing home to his mom because that feels like a nice thing to do. While the guy was doing the drawing he was chatting with the other drawers around him and when he was done it was the spitting image of John’s cousin. He did John’s face just wrong enough that it looked exactly like his cousin Stevie so much that he thought about sending it to Stevie, like: ”Hey, we haven’t talked in a while! I have this drawing done of you!” and he didn’t want to send it to his mom because it wasn’t a likeness of him.

John carried it with him and didn’t want to throw it away because he paid $5-15 for it and it was clear to both of them that it was shite and they parted ways without a handshake. He carried it until it became a talisman of a certain thing, like: ”Here is this shitty drawing that I can’t get rid of!” and eventually he unburdened himself of it and might have even thrown it in a later river. 

Louis C.K. doesn’t masturbate to images on computers anymore but he sits around and waits until a sexy idea pops into his head. He has been self-involved in a much more engaged and intellectual way.

+ RFID (RL205)

Merlin says that he bought some merch from the EFF (Electronic Frontier Foundation) and discovered that they had all kinds of things like RFID-protection for your wallet and passport. What is the actual risk of somebody scanning John as he walks past for RFIDs? Merlin doesn’t know a lot about it and how it works, but he read a fascinating article about Walmart and how their infrastructure works with Just in Time delivery and stuff. They have a network of massive warehouses around the US and like the people at Amazon they have become extremely good at anticipating what is going to be needed where, not just based on what season it is, not like Walgreens putting fucking fans up in an isle in Western San Francisco because it is summertime.

Does Amazon know that John is going to buy new parts for his 7506’s //(Sony MDR7506 headphones)//? Merlin explains that it is not quite as creepy or magical as you would think, but they can anticipate what people in the area that they supply are more likely than not to order, based on stuff. If there is something that is getting super-hot like Cards Against Humanity, which has been the number 1 card game that on a national list goes from number 7000 to number 89 within a few days and they notice another pattern that a bunch of people in San Francisco have added that item to their wishlist or to their cart, it is reasonable to get a few of those into town even though people haven’t ordered it yet. Merlin has a button to reorder Mrs. Meyer’s spray that makes your house smell like a grapefruit. It used to smell like bacon and coffee.

One part of their ability to do this is RFID technology. Every time you buy a dingus from Walmart there are two creepy little white and square strips in there and the story goes that one of them is for loss prevention and the other one is a RFID with a unique identifier for that serial numbered object so that they have an instant idea how much stuff is where on the shelves. It was designed to be for supply chain, but because your credit card has a chip now and your passport has an RFID people have the concern that you are walking by and somebody got a scanner going and they could get your data. You have to be fairly close, you can’t just fly a drone over the city and collect everybody’s RFIDs. 

At the gas station they say that you can just touch your wallet and they will take your money, but that does not seem like a thing John will ever want to do. It is not a fob, but a thing like a credit card with the RFID or some other smart chip, like the app Bump where you would bump your phones and it would share your personal information. John still has 5 people’s information in his phone that says: ”Arrived through Bump”.  John has an enhanced driver’s license, a passport and a TSA supercard and for a while he carried them around in a little foil sleeve like a chemtrails person, but one day he decided this was insane. If some Russian hacker has figured out how to get his passport information from being close to a gas pump, then God bless him!

+ Merlin no longer having his contact link on his website, people wanting to be guests on Back to Work (RL205)

They are going to get a lot of letters, but Merlin has taken down his contact link because it doesn’t bring him joy anymore. He tries to do other things in his life before he looks at his phone when he wakes up in the morning, which is very difficult to do because when you unplug it you can see out of the corner of your eye that it is filled with notifications. He gets a lot of emails from people who want to appear on Back To Work and from people who want to buy a website from him or advertise or write on his website and it is dispiriting because he knows he is not going to write back to them, but even the effort that it takes to archive/delete their email is exhausting and he doesn’t like beginning his day with bullshit.

They never had a guest on Back to Work, not once, and people send him multiple emails with follow-up why they would be a good fit. The first thing they say is that they are a huge fan of Merlin’s podcast about productivity and they never miss it and they think they would be a great guest on his show with their new book. Not too long ago it was a guy named Pek Pongpaet and he had given them some links to things that he had recently done, and buried in there was an episode of a show he was on  where: ”Do you have trouble getting bloggers to respond to your emails?”

One time there was a guy who only works 4 hours a week and in an interview he told somebody about how he had engineered a way to contact Merlin even though he knew that Merlin didn’t want to talk to him. It is the dark podcast net, the podcast about how to get on podcasts. What bugs Merlin is that the whole pretense of this bullshit email is that you have heard the show when in fact that even if you had listened to only part of an episode that they never had anybody on the show in 5.5 years to talk about their book, and having to wake up to that makes him sad.

+ Follow-up: John’s mom being racist to Hillbillies and Shriners (RL205)

John got a tweet today where somebody threatened him to mansplain frisbee golf to him, which is lovely because now John doesn’t need to wikipedia disc golf. He also got a lot of Shreiner emails //(see RL204)//, explaining that ”shrine” is an offshoot or a deeply embedded component of masonic life, although perhaps in decline or maybe ironically in ascendence. John didn’t probe further, which also is a component of shrinerism. What he forgot to say last week //(see RL204)// as he was describing how his mom hates Hillbillies was that they never got back to the second one which is that there is nothing she hates more than Shriners. She worked as a waitress in Ohio in the 1950s and the Shriners kept cattle prods and would prod the waitresses with cattle prods as they were getting drunk in the middle of the day //(see OM247)//.

It is of course a different era of Shriners. Contemporary Shriners ride Vespa scooters and have pork-pie hats. Back then they probably had mirrors on their shoes. Merlin’s mom made that sound like a huge menace in the 1950s and to this day Merlin has never seen that, but he thinks about it. That is why you wear clear underwear, in case you are in an accident.

Merlin is thinking about the Shrine Circus, but he is probably conflating the idea of clowns and Shriners and being scared, but he thinks there was a circus when he was a kid.

You never see Shriners anymore, but when John was a kid you would see them and his mom would under her breath just go: ”Fucking Shriners!” There was a Shrine temple not 4 blocks from their house and there were Shriners all the time, driving their little cars, saving kids from sickness, and John went by there very curious what was happening there because there were no windows in the building, but also with suspicion and fear of the Shriners with their cattle prods. The temple was called the Al Aska Shrine Temple in an orientalist font //(1930 E Northern Lights Blvd, Anchorage)//, like a Chuck Jones cartoon.

John imagined that in there were dancing girls with veils, but there weren’t, there were little cars and cattle prods. There was something spooky oriental about Shriners that seemed self-consciously ridiculous. Their fezs looked Turkish, and the tiny car were like: ”Look at me, I am a dick! I am driving around in a tiny car”, but who knows what a tiny car communicates anymore. People might be into it, just like John would like a 3/4 size Rolls Royce to drive back and forth from school. 

+ Smart cars, futuristic cars from movies (RL205)

The children of Shriners are probably driving little Smart cars or those BMW ones that look like a toy. There was a rash of Smart car flippings in San Francisco [https://edition.cnn.com/2014/04/08/us/smart-car-flipping-san-francisco/index.html a couple of years ago]. John kind of doesn’t hate that, although it is awful.

John thinks that Smart cars should be given away for free because it is just a little transporter for two people, not a thing you should have to buy for $27.000. Merlin’s neighborhood has small garages and the streets were not really designed for street parking and there ware so many spaces where you can’t quite fit even a smaller car. The whole thing about the Smart car was to redefine urban environments and you can back it in to the curb and in the space it takes to park your car you could park 5 Smart cars. John noticed that Merlin says ”Yeah” like Chris Walla.

John doesn’t like Smart cars because he has been waiting for 25 years for people to start driving motorcycles like the ones in the movie Akira. You sit back like you are driving a chopper and it is hinged in the middle somehow like a Green Goblin bike from the 1970s //(called Green Machine, see RW130)//. What a cool bike! All you need to do is extend the canopy a bit and use the big fat tires that you see on the back of Harleys now and it would be a little like the motorcycle from the Batmobile, and if we are going to have a Smart car why not have a fucking one-person light cycle? You put some Segway gyroscope technology into it, you have big fat tires.

There are commuter scooters that are bigger with big fat butts that you can ride on the freeway, but sitting up on it like on a scooter, not quite like a Jonathan Pryce car from the movie Brazil, like a Dykes on Bikes thing almost. People ride them to work, they go 65 mph, but they are open in the middle like a scooter so you can wear your suit. If you can make those, why can’t you take a light cycle and and Akira cycle, think about them as John has done, and then use your Czech engineering education to actually build a cool rad thing that would never tip over and that would protect you from the elements.

You would probably be able to use Segway technology to prevent you from ever getting side-swiped by a little old lady in a Volvo. Then we would be in the future! A Smart car is not the future, it is a cul de sac, nobody wants to ride around in a hard chair, but you want to ride around in your super-cool motorcycle. The Batman motorcycle and the light cycle both have a real aggressive forward-leaning position, but on the Akira bike you are a cooling out almost like in an Eames Chair. What if you co-branded it with Eames. It would be mid-century modern, except in the future!

A lot of the cars we have now are designed after the Woody Allen movie Sleeper, which even then was a chintzy car, like the car in the Tom Petty video where they are out in the desert and find a video game parlor //(You Got Lucky)//. Both of those look like they have recycled some tin foil and put it over a Volkswagen Bug. That is not the future!

There is the Isetta, which probably is the Jonathan Pryce Brazil car. It was the first smart car! It looks cool and is not much bigger than the scooters from Quadrophenia, you get in through the front where you expect the hood and the windshield to be. When John visited Merlin in San Francisco in the [[[GMC RV]]], Merlin was really disoriented when he learned that it only had one door. The Chevy Suburbans until 1972 had 3 doors: Driver’s door, passenger door and one door for the second-row seats. You say: ”Slide, Clyde!” or ”Move over, Rover!” Merlin says that to his daughter about 16 times a day when she sits on his area of the couch that he bought.

John is going to say: ”Move over, Rover!” whenever someone is on his side of the bed because he has a clear side of the bed. It is not complicated: You walk into his room and you know which side of the bed he is on. Everybody seems to feel like they need to be over on his side. Get over to your side, that is why it is called ”your” side! Also in the middle of the night don’t get your feet on John’s side.

+ John’s theory of education, different styles of reading (RL205)

Does Merlin remember learning the word ”nautical”? It seemed old-timey and Jules Verne-y when he first heard it. He doesn’t think of a modern destroyer, but you have a sextant, a big compass, a steam ship, a big captain’s wheel and rigging that gets climbed, and a brig, or it is a scientific room with charts and things. John thinks you even heard and understood the word before making those associations. It is something that has to do with the sea, like a nautical themed bar.

The word bovine is similar. It is pretty funny, but there is no reason to use it, and you know what it means because you read some article or description or it has come up. John is working on a theory of language that is connected to a theory of education. If you have a vocabulary calendar with a new word every day and today’s word is bovine, if you don’t already know what that is, then you are not going to learn it from a vocabulary desk calendar. It is not even a GRE word like lachrymose, which are words that you just learn for the GRE or words. Lachrymose is a word that by the time you read it 15 times and thought: ”What the fuck does that mean?” you look it up in the dictionary, but it is not a word that is going to stick if it is edumacated [sic] onto you. 

One of the problems with a theory of education where there are imbalances in schools that we need to redress through vocabulary tests and so forth, is that there is a vocabulary in the language with words like nautical, bovine, words that you just know what they mean, even if you can’t triangulate to them. That sub-strada of knowledge is a thing that John doesn’t know how to educate into someone, but it is an advantage that certain kids have going into school, they are bathed in a language that is inaccessible, not one that is meant to communicate class, but a vocabulary of people that use words and know words.

Merlin grew up learning math the hard way for no good reason, just memorizing lots of stuff, and the way that his daughter is learning math is a lot smarter and a lot more integrated with how life works, which makes things like algebra much less weird to learn. Her new math is treason //(last line of the song Medicine Cabinet Pirate by The Long Winters)//. The problem with language is that if you get yourself a word-a-day calendar and you can now say that ”this made me lachrymose!”, that is strap-on education.

If you read enough stuff you can learn that words mean things and different words mean different things, like Merlin’s distinction between costly and expensive, and the more you read the better you get at this, but the strap-on education is a way of simulating that through a word-a-day calendar.

John’s lady friend (Millennial Girlfriend) went to law school and was very successful in school and learned how to be in school. Merlin says that Liberal Arts is learning how to go to school. John learned how to be in school on his own and made up his own mind what that meant and when everybody told him that that was not what being in school meant it was too late. She learned how to be in school correctly, and part of that meant to read a certain way, to skim the education out of the book. When John gave her a couple of novels that meant something to him she read them in a day, she did a Merlin on him and listened to the first 20 seconds of each.

There was no way she read The Heart Is a Lonely Hunter //(by Carson McCullers)// in a day, but then John realized that she had a whole different relationship to reading. Both of them came from privileged places where you are expected to go to a good college and you end up in a world of thinking people. John has spent many hours reading literary theory books as though they were novels, reading every word, trying to find a fucking plot in that whole discipline, and 85% of that stew of words were made up in the context. So many of the books that he was exposed to he was reading incorrectly, he should have just looked at the table of contents and the first two chapters and then thrown the book in a pyre.

Even trying to find a philosophy of reading that is common, even with people that share your social and class strata is very interesting. John was trying to expose her to this novel and get her to treasure it, but they had to go back and learn to read a different way. When talking about a theory of education and the need to reform the schools, how are you teaching to read? John can not subscribe to the New York Times because it takes him 6 hours to read. Merlin’s wife Madeleine reads so fast and Merlin has tested her by giving her something about Brexit that was three pages long and it took her 4 seconds to read. She can actually read that fast and comprehend it. Merlin considers himself a not-stupid person. but his reading is ridiculously slow.

For a long time we were trying to get a fair and comparable education to the widest number of students possible. You don’t want to have inequitable access to education so that the kids in the rich school get great educations and the kids in the poor schools get shitty educations. There is a lot of understanding that by the time a kid gets to school they are already on a path and the theory of education then extends to a social theory of how to get that preparation for education in kids before they ever darken the doors of a school.

It is a fairly new idea that African American students don’t ask rhetorical questions because those are not part of the language of the African American communities. There is a language of rhetoricism in the white style of education and black students go: ”This is a stupid question!” and right away there is a language gulf. There are some prejudices way upstream in terms of how teachers talk and approach subjects that put certain students at a disadvantage and makes them to the teacher seem dumb or disengaged.

As we understand that stuff we are trying to modify how we do, but it happens so far back that - other than having schools be an overarching agency that is in people’s homes - it is hard to tailor a system of education that serves everybody. Judging a school system on how well students do on tests is just end-time shit, it is the opposite of education and it drives John bananas! Maybe it is teaching kids to be good at law school, but we don’t need that many lawyers, we need more book readers.

There is a reason why so many of Merlin’s friends from fancy Liberal Arts school became attorneys: It was not just for their love of the law or their desire to be a judge, or giving up and becoming a teacher of law, which happened really in some cases, but there was something very attractive about law school to people who had read a lot, thought a lot, and worked independently and had to figure out relatively impossible amounts of work over a period of time. A dingeling like Merlin would say: ”I have 1600 pages of reading this week, I better get started and make coffee!”, but then there is a certain kind of thinking to find the 20% that are hugely important to getting the most important part of this done.

Merlin has taught his daughter how to do estimations that get you most of the way there in a 20th of the time, like you can weigh your change and estimate how much money you have.

When John read Hegel he was reading 4 sentences and go back to read the first sentence again, while law school students can read 1600 pages in 4.5 hours while John is still on page 150, going: ”Wait a minute! Let me go back to the dictionary…” To read a book half-assedly always left him with a dissatisfied feeling that he had not read every word in that book and understood it, which was worse than to not read the book at all. He did the book a disservice and to honor the book he would not even begin to read it, which is a weird relationship to reading.

Some of his friends read through volumes and volumes and they never put the book down and went: ”God, that was beautiful!” or ”OMG! That was the most beautiful thing I have ever read!”, they are missing that.

Merlin has a friend who reads scripts at LucasFilm. She knows within 2-5 pages pretty much whether or not that is something she needs to keep reading. She has so many scripts to read, she can’t stop and sit and cry about the beauty of the plot of every one of them. She got work to do! Think about being a book editor in New York City! You got stacks and stacks and you can’t be reading these books! Not bringing this back to Karl Ove Knausgård: How many pages of a guy recounting minute by minute what his boring childhood was are you going to read?

The same is true when you watch something on TV. When Merlin is watching something really good, especially if it got dragons and shit he will put everything down and watch it. As we get older we rely more and more heavily on heuristics, shortcuts and eventually clichés, which can get you all the way to [https://en.wikipedia.org/wiki/2016_Sacramento_riot stabbing people in Sacramento] //(one day before this episode was recorded)// because that is how you roll. You don’t want to get too caught up in your fast guesses about what something is, and yet there are other kinds of heuristics that we don’t think about actively enough and how do you even judge this.

+ John’s thrift store heuristic how to find clothes in thrift stores (RL205)

For example when John goes to a thrift store he has a very conscious set of heuristics and he does not look at every item in the store. You already walk in fast and don’t stand in the door of a thrift store like a dummy. You know that if this particular thrift store has already cherry-picked what they think is the nice stuff, which they are usually wrong about because it is stuff that has been chosen as high-value fashion stuff by people who work in thrift stores, and put it over in their little boutique area, but you still have to go through it because sometimes they get it just right enough that you can’t blow past it.

One time John was listening to the little old ladies in the back room who were sorting stuff, and one of them pulled out a baseball tour T-shirt from a late-1970s Journey tour that was battered but still intact and still beautiful. Merlin would kill for that, he is still looking for a Van Halen 1979 Invasion shirt. That shirt on Melrose is worth $250 and in San Francisco it is worth $150 and on eBay it is probably worth $90, but this little old lady was like: ”What do you think, Marge? Do you think this is still wearable?” - ”No, it is pretty worn out!” and John watched her toss into a bin that is either headed to a fabric recycler or it is going to go to Ukraine //(John said The Ukraine again)//.

John is not somebody who was going to get that Journey T-Shirt and sell it on eBay, and he doesn’t care enough about it, so: ”Vaya Con Dios, Journey T-Shirt! You blew it!” There are pearls among the oysters every day. When John goes to a thrift store he goes through their boutique really fast and his number one heuristic is: If you don’t like the fabric of a thing it doesn’t matter if it fits, and those people in the back room don’t know what sizes are either, particularly because a lot of nice shirts are in European sizes and stuff is everywhere all the time. Sometimes John looks in the XL Women stuff because they look at a blazer in an unusual color and think this must be a girl’s blazer.

You run your fingers over everything until you see something that jumps out at you as: ”That is nice!” and then you take extra time with it to see if it is going to fit and if you like the style //(see RW144)//. It is very different if you are looking for a black suit to go to a funeral because there are a lot of black suits. John can go through there really fast and go: ”Nope nope nope nope nope nope nope!” and he knows he is not going to go through every button-down shirt in the store, looking for the one that jumps out at him because he can go almost at a walking pace with his middle finger gracing over everything, looking for the moment when something feels good or the fabric is unusual.

John barely looks at pants because a man needs three pairs of pants and John doesn’t need to wear some other guy’s pants, but he does go through the shoes. Thrift stores are full of square-toed shoes from the 1990s/2000s and you don’t want that. Every once in a while you sees a pair of shoes that is singing to you somehow and only then do you see if it fits. Then John goes to the luggage and he is not even touching stuff, but he is just looking with his eyes, looking for a certain kind of bag that is old and soft.

He doesn’t want a plastic Samsonite, he doesn’t want somebody’s old trumpet case because he has 14 of those already, he doesn’t want a hat box, but he is looking for someone’s old small duffle that they used when they shipped out to Singapore or used as a gym bag back in the day when they belonged to an athletic club.

Then John is moving through the blankets, looking for a Hudson’s Bay blanket or a Mohair blanket made by an American, Canadian, or English mill, not fleece blankets, comforters, or blankets knitted by your grandmother, but those are a certain kind of thing and you can feel them with your finger. The best blankets are those that look and feel uncomfortable, a blanket that at some point might have been on a horse. If there is plenty of time then you also look at the cufflinks, the wallets, and the belt buckles.

+ People who overdo it when running a restaurants (RL205)

Merlin tells the story that Dan is sending him photos of things that concern him (They call it Dan’s Concerns on Back to Work), in this case of a Japanese place across from his office that looks like you are in the back of it, no matter where you are. They are doing something they think it is working by adding more and more signs and things to tease the heuristics, but it makes it more a place where Merlin would never in a million years go to.

If John owned a restaurant he would have a very hard time not over-accessorizing it, not like a Cracker Barrell, there wouldn’t be any plows, but he would maybe not know when to stop with OPEN signs. Pretty soon it would be like a McGinnicanacalis (?) where the waiters had garters on their sleeves and were on roller skates. He is afraid getting into retail or restaurants mostly for that reason: He wouldn’t know when to quit.

There was a place near Merlin’s college that used to be a chain burger place, but now it was owned by a guy who made it into his own burger place. The lighting is not that of a national chain, he was not spending a lot keeping the place up to the standard of even a McDonalds. Behind the counter there was one person working there who had a large console color TV and is sitting in a reclining chair like a Lazy Boy behind the counter. Every burger, sandwich, that this man made he always put a fried egg on it unless you asked him not to and he might seem a little hurt if you didn’t want his fried egg. It is Merlin’s canonical example about what can go wrong if there is no-one else there. People who make sandwiches as though they have never eaten a sandwich are the bane of Merlin’s existence.

+ John’s and Merlin’s ideas for restaurants (RL205)

John’s idea for a restaurant is very small, just railroad sized, like the Soup guy in the Seinfeld show except it would have three big food service sized cooking pots, one with macaroni and cheese, one with chili, and one with stew, for $5, and they never change and that is it! If you don’t want any one of those things, go to a different restaurant! Merlin would be there so much!

Merlin would open a restaurant called Toppers where they give you melted cheese with toppings on it but no pizza, so you can enjoy pizza without having to eat fucking pizza, like the Seinfeld place The Muffin Tops. Another of his ideas is a restaurant called Gravies which is just gravies and sauces and things to put it on. It would be a chance for their listeners to support them when they retire, called Gravie McSausingtons, Merlin & John, like Bartles & Jaymes.

The name needs to incorporate the sauce idea, like Coverband, or FX McGravies. Merlin likes J.J. McGravymen, but he likes J.J. because J.J. Abrams did that incomprehensible TV show that no-one should ever watch //(Lost)//, not Jay J. Armes but J.J. Abrams, he is the one with the space ships and he made a Star Wars. Jay J. Armes was the private detective who had hooks for hands. J.J. Abrams made the TV show with a ghost monster and people were in and out of a sleestack, called Houseman, Big House, Sorority Boy, Ghost House, Doll House, no: Lost. Merlin was thinking of Joss Whedon, J.J. Abrams made Alias, Lost, and My So-Called Life. John keeps seeing Joss Whedon at ComicCon and stuff, but that is Wil Wheaton.


























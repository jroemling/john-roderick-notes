RL66 - If I Could Stop Time
2013-02-22

This week, Merlin and John talk about:

**The Problem:** Becoming absorbed into the Grease universe, referring to John being into the movie Grease when he was 10 years old, but because his younger sister was also into it he was obligated to be dismissive about it.

The show title refers to John’s fantasy in 7th grade that he could stop time.

They both answer the phone in a soft muted voice.

+ Songs that are older than you think, nostalgia in arts and music (RL66)

John was reading the lyrics to De La Soul’s hit song The Magic Number. Merlin thinks it is out of print now because they had a giant problem with clearing their samples for which he blames Gilbert O’Sullivan, but that is not actually true. It is an example where sampling produced a masterwork and all should kneel. This is a hell of a record, that was an amazing summer (1989), but it was already 24 years ago, from the album 3 Feet High and Rising. The 24th anniversary of Abbey Road //(by The Beatles)// was September of 1993.

Merlin was playing this horrible game with his wife last night. He had a Genius mix on an If I Can’t Change Your Mind by Bob Mould came up, which is about 20 years old and that means at that time a 20-year old song came out in 1973, which will mean nothing to the younger listeners, but it is harrowing!

The other day John was thinking that in 1986 it was not just that they both were younger and that time was longer for young people because by that time they had transitioned through 1000 years in the culture from 1966. They had experience the metamorphosis of 1000 years of human consciousness. They had not been to the moon, they were still wearing white Gogo Boots for the love of God and not even ironically, and by 1986 they had been through a Do-Op (Rockabilly) revival, through AM Radio songwriting, they had experienced Punk Rock, Disco, Anti-Disco, Anti-Punk, New Wave, Anti-New Wave, Post-Anti-New Wave Disco Punk, they had then another Rockabilly revival, and in the same 20 years span from 1993-2013 John doesn’t know a single thing that has happened.

Grease was a Broadway show, it is The Word that has feeling, time, space, and emotion. Frankie Valli sung that, but not in falsetto. The movie that people are most familiar with came out in 1978, which means that the nostalgia in Grease was 20 years old, and the movie is now 35 years old. John is so nostalgic for the original nostalgia that he felt for a thing that he was not alive for. There are so many layers of nostalgia between him and anything he likes.

In 1940 his dad went down to The Showbox theater to see Benny Goodman and his orchestra, people were flying through the air, they were dancing that they are causing the sprung dance floor to bounce, and his dad was with his teeth on the stage, watching this band, and thinking to himself: ”This is all I want to do with my life! I don’t want to be a lawyer, go into real estate, business, or government, but I want to go with this band!” There was no aspect of nostalgia in that music, but for its time it was 100% original with no antecedents. They were blowing people’s minds!

Everything John ever loved has been filtered through at least one cheese cloth of nostalgia. There was nothing about New Wave once you stripped the Cloves off of them, which John tried to do at every opportunity, and any one of a number of Beatles songs presaged the whole idea. John is just sitting there in his nostalgia escape pod and looking around the room he can’t think of a single thing that isn’t either actually a thing from the 1960s/70s or a modern thing that has been made to look like it is from the 1960s/70s, except for his Apple products, they are the only modern thing in his whole house, and he is not that happy with them. If this Mac-thing had been made like an Apple IIe he would be very satisfied with it and find it quaint and quirky.

John Hodgman was on Marc Maron’s podcast a while back //(see here in [http://www.wtfpod.com/podcast/episodes/episode_354_-_john_hodgman episode 354])// and there is a wonderful quote from one of them saying: ”Nostalgia used to be better!”, but the sad part is that Merlin instantly understood what they meant.

+ Grease, early 1980s crushes (RL66)

When Grease came out Merlin was torn because he had a split allegiance among the two cool guys on TV that he followed closely, The Fonz //(Henry Winkler from Happy Days)// and Barbarino //(”Vinnie” Barbarino, played by John Travolta)//, but then Barbarino was in Grease and Merlin found common ground by going to the bathroom and taking a giant scoop of Vaseline Petroleum Jelly that they had in their house because his mom is a liberal woman, but putting vaseline in your hair seems way more straight forward than it is.

John’s parents were a little older and they knew about Brylcreem or Greasy Kidstuff //(magazine)//. He actually asked his mom in 1983 how you get your hair to do that and she said what he wanted was Vitalis, so he went to the drug store Longs Drugs in Anchorage Alaska and in the men’s grooming section past the Drakkar Noir, down where only old men shopped, where you could buy a straight razor, there was Vitalis and John bought a bottle of it which he still has to this day. There is not a lot left in it, but he still uses it every once in a while.

The problem was that John’s mom was wrong and that is not how you get Fonzie hair, but it is how you get Bennie Goodman hair. She was an in-between generation and was 20 in the early 1950s and she was still slicking her hair down in the old-fashioned way.

Merlin occasionally uses a bit of hair product when he wants to achieve a certain look and when he is nervous he uses a bit more than he should. He remembered the Brylcreem phrase ”A little dag will do you!”, he did not have a standard to what a dab was and thought it was applicable to vaseline as well, but then he needed more because it wasn’t giving the effect he wanted and pretty soon he basically put lots of literal oil in your hair. Once it gets to the 98,6 degrees (37 °C) it literally melts in your hair and gets really comfortable.

Merlin can’t imagine what Ed Gein’s hair looked like, but if somebody knew enough about Ed Gein you would use vaseline. His mom was really unhappy with him and he washed his hair again and again, but vaseline and water don’t mix and his hair said: ”Fuck you!” to the shower for at least 4 days and he looked as bad as Larry, Darryl, and Darryl and he smelled like a dildo and looked like a serial killer, it was miserable!

It is an ongoing problem for Merlin, especially as a young adult: He always felt like he somehow had not gotten the manual. He and his wife had been watching House of Cards on Netflix and they had accidentally skipped over a very important episode and when the next one came on you doubt yourself and think you are stupid because you can’t figure this out yourself, and obviously he wasn’t paying attention and was playing with his iPad, but two nights later his wife Madeleine said that they had missed an important episode. Merlin feels like there was a 3-page handout that would have made everything 75% better for him. This is why they are doing this podcast: It is the 3-page handout for the next generation.

John’s problem with the movie Grease was that his younger sister was more culturally aware and more of a media consumer than he was when he was 10 and she was 8. He wanted to be absorbed into the Grease universe, he wanted to drive a hot rod car with a plexiglas bonnet, he definitely understood the appeal of being a Rocker girl, but he also understood the appeal of being a good girl and he wasn’t sure which one of those he wanted to be, but then his sister was seriously and heavily into Grease and John felt an obligation as his older brother to be dismissive of it.

That soundtrack was the girl-version Frampton Comes Alive!, every girl had a copy. John’s sister had it memorized, she had watched the movie 25 times, and she learned whatever bad thing you are supposed to learn about being a Barbie doll or whatever it is that people are mad at Grease for. John wanted to enjoy Grease and wanted to be an innocent 10-year old boy who loses his innocence to Frenchy //(Didi Conn)//, the beauty-school dropout, the girl with the nose who was so Jew-y and so cute. He didn’t understand why she wasn’t the sex object of the film because Olivia Newton John was fine and cute, but she is a little plain compared to the exoticism.

Stockard Channing was foxy, too. Of all the girls! She had a TV show after that //(The Stockard Channing Show)// and Merlin was attracted to her. She was hot and she was puckish, although in her character on The West Wing she was not supportive enough of the president. She was a female empowerment figure, but as his wife she should get behind the man!

One family at Merlin’s church in 1978 when Grease came out had brought their child to the movie and they marched out of there in complete and utter outrage in the first minutes. They were smoking and sass-mouth and all the things and they were putting their hands over their kids’ ears and were out of there. That was the word you heard with Groovin Feeling (?): Grease was not where people at white-o Christian church should be spending time with their children. It was like the Holocaust! People were dry-humping and finger-banging through the pants.

For a long time Merlin was very attracted to the weirdly disturbing sexy Olivia Newton John because that is what you were supposed to do, even though she seemed really uncomfortable in her role as fake slutty girl, but in retrospect... John thinks that in those Spandex pants she is a little bone-y and a little boy-hipped. What about the Physical video //(music video of her song)//? John had a very complicated relationship with legwarmers and head bands because they were basically the boundary fences of his whole young sex life. There was a head band here and legwarmers there, and everything in between: How do you get there? Like pubescent DMZ. First of all: How do you get the pants off with the legwarmers guarding the approach, and what was the head band for?

Flashdance blew that wide open, but John was not into Flashdance and he can’t put himself fully in his 13-year old mind because he could not think of a more beautiful woman than Jamie Lee Curtis, but 15 years later when he in the fullness of time came to recognize that she was kind of repulsive. His whole idea of the foxiest girl in the world was Jamie Lee Curtis in Trading Places and now he cannot get there.

Merlin thinks she is a hermaphrodite, but that is one of those stories like that they pumped a gallon of semen out of Rod Steward, John doesn’t believe that, or that he ate bubble gum with spider eggs. He probably ate Pop Rocks and then drank a Coke and it turned into a gallon of semen. 

John can always look at Jill St. John and find exactly what it was that he was thinking the first time he saw her, but with Jamie Lee Curtis it is not the same for him. It was long before he saw her boobies //(in Trading Places)//. She had been in all those slasher films and her face was everywhere for teenage boys, she was a staple of their youth, she had a boyish haircut, she was bubble-gum snapping, but now Ann-Margret in Tommy is actually the peak.

Merlin wonders if John enjoys the appearance of a more womanly woman these days. There is a spectrum between Audrey Hepburn and Marilyn Monroe, a cultural reference that is 60 years old and established a certain standard. A more modern reference would be a spectrum between Carley Ray Jepsen and Ben Gibbard. When John hears her name he pictures Punky Brewster, somebody who is 9 years old.

The other day John learned that the woman who plays Jenna Maroney, the blonde narcissist on 30 Rock //(Jane Krakowski)//, was the girl with the side ponytail in National Lampoon’s Vacation. Merlin’s friend Michael went on a date with a her a couple of months after that! She lived in New Jersey and they went to a carnival together. Merlin has never seen that movie. John thinks it is one of the great films of the last 40 years. ”Can I help you with that, Eddie? Please!” - ”Hamburger Helper” - ”Real tomato ketchup, Eddie?” - ”Nothing but the best!” //(see [https://youtu.be/bkExpbnjsX8 Cousin Eddie and Hamburger Helper])// ”Sorry folks, the park is closed! The moose up front should have told you!” //(see [https://youtu.be/E4Eim2gNczY John Candy])//

Merlin had basic cable plus Showtime and HBO and he saw Stripes six times in a month and a half and and there are still lines that are implanted in his brain, like: ”Lighten up, Francis!” //(see [https://youtu.be/syV2LkGpQB0 here])//. Kids a little younger had a VHS player to watch their favorite movie over and over. Sitting in the dark when he was not supposed to be watching TV and watching Escape from New York and Alien made a huge impression on him. The same is true for John. The first time through he could not even stand the movie Porky’s because it was so dumb. He was just watching it to see the shower scene, but even at the impressionable age when he was the target audience for that film it was just too dumb to believe. It was not a very well-made movie. He would watch Stripes and Escape from New York and National Lampoon’s Vacation over and over.

+ Adult video channels in the hotel, Victoria Secret catalog showing up at Merlin’s house (RL66)

John was in a hotel not very long ago and of course he put his remote in a plastic bag, as you do, and after he had been through the channels 15 times there was nothing on except E! Entertainment Television dubbed into German and he thought about watching a first-run movie, but there was nothing interesting there, so he looked into adult movies and it was all the same stuff and he realized that those things were all $25! For that price he could go down to the nearest McDonalds and meet a couple of girls, buy them both their dinner. Back in the old days on HBO you could potentially see a porn movie where all of the good stuff had been edited out, which was plenty for Merlin.

Back in the salad days Merlin could break a pair of jeans with a ZZ Top video! When John was a teenager he would get off looking at ski wear catalogs. That puffy jacket was so puffy! Merlin learned recently that puffy jackets are a sex thing. One time around 1982, back when the Postal Service was useful and competent, but not the band although he likes that new song, mail used to be a real thing, you didn’t get other people’s mail, like in the movie Hollywood Shuffle: ”There’s always work at the post office!” that Merlin probably didn’t see although it was a very influential movie, but they brought somebody else’s Victoria’s Secret catalog to Merlin’s house and he had never heard of Victoria or what her secret was, but that one little pile of shiny paper changed everything.

John is pretty sure that he got the first ever Victoria’s Secret catalog. Years later Merlin would acquire a Frederick’s of Hollywood catalog, but it was really gross and obviously trailer park garter belts, which is a good band name. That catalog was a very special companion for Merlin and he knew what was on each page. It was like onanistic mana, it just showed up. John is not sure if the early Victoria Secret catalogs would seem mundane now, but at the time of course it blazed a trail through the forest of his mind.

Around 1982 on the back page of some ski magazine there was an ad for Obermeyer ski wear and there was a picture of a girl wearing an Obermeyer ski jacket and she was so beautiful and the idea that she was also a skier  really made a connection with John. She looked at him from the back of that magazine with laser-beam eyes and John kept it under his bed, not that he was jacking off, but he would just take it out and stare at this picture for hours at the time, not even imagining scenarios with her, he was just looking at her face.

Most of the skiers John knew were girls because at their ski club the girls team was very competitive and a lot of the girls he was in love with as a teenager were these really rich ski girls with puffy jackets who every season had brand-new skis, brand-new poles, brand-new ski pants, while John was schlumping his way through, trying to get by on a three-year old Spyder ski jacket with a flight suit underneath it //(see story about John’s orange flight suit)//.

+ Alien, Sigourney Weaver, Jennifer Grey pre/post nose-job (RL66)

Three nights ago Merlin watched Sigourney Weaver in Alien, which is beginning to end an outstanding movie that is still so good. Seeing her in her underwear she was right in Merlin’s wheelhouse. He is watching another program //(probably The Americans)// where the girl from Felicity //(Keri Russell)// plays a Russian spy and she is right in the pocket. He does not have anything against girls who look like women, but he does like a boyish pixie-ish something. He had zaftig girlfriends in the past. Sigourney Weaver was never John’s thing. She is hard without being hebraic.

John’s canonical hebraic celebrity who could get the right haircut and wear combat boots and be his ideal Jewess is Jennifer Grey in Ferris Bueller’s Day Off in the police station with Charlie Sheen who is flirting with her and she is not having it, but then she starts to melt. Of course then she appeared in Dirty Dancing, a movie John has never seen, but he has watched the highlight reel of her dancing a few times because Jennifer Grey pre-nosejob was super-foxy, but post-nosejob it is like she disappeared from the Earth //(see RL331)//. Her face looks completely different, it was a super-high-quality expensive professional nose job and she became the plainest pretty girl. 

It is like Indie Rock: People are stampeding to get rid of the thing they still haven’t realized makes them special. People can’t wait to get to the middle of the curve. That is why John religiously doesn’t exercise because if he were super-fit he would loose what makes him so cuddly and lovable. Also: Why do super-fit guys look so clammy? You could towel them 1000 times and never wipe the sweat all the way off. They seem covered with a greasy moisture, but they are dry and they look like that even in a tuxedo. John doesn’t want to sacrifice the natural flat coloring and appealing dryness of his skin. His cheeks are a little pink and it is hard to tell if it is cold wind or the exertion of walking up a flight of stairs and he doesn’t want to trade that in for a vaseline layer between him and the world.

Their skin probably becomes slightly translucent because the pigment that used to make their skin look solid was redirected somewhere to replace the necessary fats that they are burning off, or as their balls shrank up into their body the pigment from their skin has to go down and fill them back up enough that they don’t create an impaction.

Merlin had a coffee with a really nice guy this morning that he knows from Internet stuff and with a complete lack of irony he asked Merlin what kind of workout he does. Merlin walks around the corner to his office and sometimes he walks home and that is up-hill. It is a whole city block and not a small one.

+ Kevin Seal, MTV (RL66)

John’s favorite 1980s television personality was MTV’s Kevin Seal who did 120 Minutes and Headbanger’s Ball. He was the fun stoned slacker VJ, the fist one that wasn’t some radio-DJ that they plucked and made into a VJ. They had done a talent search and he had showed up: ”What’s up, guys?” and they decided that this guy with the super-stoned eyes should be their new VJ. When he appeared on the screen for the first time John was sitting in his Barcalounger, completely baked, drinking a Schmidt beer and it was like he was looking into a mirror because this was the job John had always wanted, and yet he couldn’t hate him because he was so amazing.

It got to the point where John had no interest in watching MTV unless Kevin Seal was on and he would just sit and laugh and laugh. Merlin didn’t dislike him, but he always seemed a little affected. John is sure he was pretty baked. John was baked and Kevin seemed legit. One time he came on and said: Eddie Van Halen and Valerie Bertinelli had been married for 10 years already and he was trying to imagine what they talk about, and he said he pictured them at home going: ”Coo Coo Coo” and he started doing handpuppets to it and John actually peed. It might have been because John was incredibly baked, he was speaking truth to power and was cracking himself up, John wouldn’t be surprised if he peed a little. That was the heyday of television!

+ 1980s crushes (cont)

Merlin got one of Jamie Lee Curtis’ children’s books. Something has been lost, but it happened in John and has nothing to do with her. A Fish Called Wanda is still very good. The drama that John Cleese brings to the proceeding and the seriousness of his character is what holds the movie up. Merlin thinks that Kevin Kline is pretty great in that, but it is not a subtle role. Merlin feels a certain resonance with John Cleese because he made his career out of being broken in public. He brought so much smart rage to Monty Python.

John thinks that Cleese is like Pete Townshend in the sense that the first time John ever laid eyes on him on screen he was the star of the show and he loved him. He is the star in a democracy of geniuses because he has a super-natural power.

When John was a little kid at 5-6 years old his dad started a college in Alaska for native guys who were at the time being underserved by the university of Alaska and he started a teacher’s college called Pacific Rim, he rented classrooms at Alaska Pacific University and he hired teachers. John can’t imagine what his dad thought he was doing, but he had classrooms full of guys at a time in Alaska when a lot of young guys came to Anchorage from the villages and they were having a hard time coping with city life.

One time John was walking into a classroom with 40 guys between the ages of 17-24, all native Alaskans, and they were in this class where somebody was lecturing them. There was a guy in the middle of the room, just like any of the other 40 guys, and John just went to him and stayed with him all day glued to him. His sister was with him and she was 4 and they were both just glued to this guy and at the end of the day when he had to go you had to pry them away from him. When walking into the room they knew that he was a kind of royalty, a prince.

He was just a dumb teenager, his reaction was: ”I don’t know what these kids want!”, but he was kind to them all day John doesn’t know whatever happened to him, but every once in a while you see these magic people and John Cleese is one of them. He walked onto the set and was immediately the star and he has born it out. He had long black hair and an army jacket, but half the guys in the room did and something was magnetic about him and at that young age John had no social filter that kept him from saying: ”That is the guy I want to go stand next to!”

Half of growing up is learning to implement social filters that keep you from making that terrible tragic error later in life where you just walk up to someone and they are like: ”Can I help you?” - ”Yeah, I just wanted to come stand by you, I don’t know why. It is not sexual or anything, but you are just magnetic” There is a little boy like that at Merlin’s daughter’s school, it goes beyond a crush, his feelings for Merlin’s daughter ar on another level and he always wants to bring her things when they are getting ready to go and talk about their house. He is an adopted kid from China who just learned English in the last 9 months, amazingly fast.

Merlin has met famous people and it isn’t that often that you meet a completely incandescent personality. Barabarino had that effect, the first time Welcome Back Kotter came on and the camera scanned the room, you were thinking immediately: ”Who is the goomba?” Ron Palillo and Robert Hegyes were the superior actors, no question, you could tell they came from a Shakespeare background. That show draged on way too long and when Barbarino left they brought in that one guy who was always the replacement guy on TV shows. There was a very special episode //(of Welcome Back Kotter)// about Horshack and all very special episodes go through different strokes, but always back to All in The Family. 

The Fonz //(Henry Winkler in Happy Days)// is another one like Jamie Lee Curtis where John can’t get back to the headspace where the Fonz was the coolest thing in the universe. He had enough exposure to Henry Winkler and what a soft character he is, for example in the Michael Keaton movie Night Shift. Remember when Diane (?) was on there? Sally Struthers? Johnny Martin (?) Diane? She was pretty cute in that movie. Again: Small cup, little boy top.

Another one of John’s TV hotties is Markie Post. One of the very first songs Merlin ever wrote had Markie Post in it. It was about the apparent differences between us and the Soviets, it was a very political song.

They continue to talk about movies Henry Winkler was in, like The Lords of Flatbush. Square Pegs //(TV show)//! That was a good show. She had a serious, proportionally giant nose at the time //(Sarah Jessica Parker)//. Both Merlin and John found her very attractive.

John just recently tweeted a thing about Pinky Tuscadero //(Roz Kelly in Happy Days)// and her sister Leather //(Suzi Quatro)//. He was wondering what happened to her and through the miracle of crowdsourcing people told him the whole story. The audience response was so positive to Pinky Tuscadero that they were going to make her a major character in the next season and all the promo ads in the off-season were really selling her, but it turned out nobody in the cast liked her and she was hard to get along with and Henry Winkler couldn’t stand her, so they wrote her out of the show. Recently she got to jail because her neighbor’s car alarm went off and she fired a shotgun at it. She also hit a man with her cane. John wants to have a sword cane!

At the age of 23 being cast as the though girl in the pink satin jacket on the number one TV show in the country and for whatever reason, either she is a little crazy or she doesn’t understand how the world works and she acts bigger than her actual station, being lame around the cast and she didn’t realize that she wasn’t a big star yet and that it was not appropriate for her to be a jerk and she blew it and got written out of the show and was a footnote forever after. John can put himself in his 23 year old mind and recall a time when he thought that success meant that he would be able to be a jerk to people and no-one could be mad at him. Only after many years of not being successful did John realize that he needed to learn to be nice to people just out of self-preservation, but that is a fairly recent thing from the last 10 years. At 30 years old he was still not entirely convinced he needed to be nice to people.

Not that long John was mean to somebody, which made him realize he is still mean to people despite trying to stop being mean. Jonathan Coulton is never mean to anybody because according to Merlin he is a pussy, but everybody loves him. He is a very nice and thoughtful guy who thinks a lot and Merlin really admires that. He has formed a cult around himself, which is not John’s goal because the people who terrify him most are the ones who agree with him.

+ John imagining he could stop time (RL66)

One of John’s longstanding fantasies that goes back all the way to 7th grade when he should have been part of a forest service trail building program //(see [[[Bulding trail]]])//, but instead was at Wendler Junior High School in Anchorage Alaska an forced to sit in a room with a bunch of other pimply-faced shitbirds, they all had 10-hour boners, and they were sitting there, being taught Algebra, and during this point in time he developed the technology of imagining that he could stop time.

He had three conflicting desires and they were all equal, he could not prioritize one over the other: 
* He wanted to go back to sleep because he was very tired and had been forced to get up in the morning. If he could stop time, the first thing he would do was to go to sleep and because time would be stopped there would be no penalty and when he was done sleeping he would have lost nothing
* He wanted to see naked girls, and if he could stop time he would first go to sleep, he would go through the whole school where everybody would be frozen, and with the help of a dolly he would move all the pretty girls he wanted to see naked to the gym and take off their clothes and see them naked, then put their clothes back on and get them back to their classrooms. This was a long time before it occurred to him that he would ever actually have sex. He had no interest and could certainly not have sex with somebody without their consent, he just wanted to see them without their clothes on. If he could go back in time and talk to himself he would say that there was a better way to do it than to stop time and moving them all to the gym.
* He wanted chocolate chip cookies. After he had slept and seen all these girls naked he could find and eat as many chocolate chip cookies as he wanted.

Then he would start time again and no-one would notice. The girls would wonder why their panties would be on backwards and somewhere in the town there would be some large woman in a chefs hat wondering where all the cookies went, but that is how he was going to use the most powerful power of all.

John’s other daydream in that era was that the Russians invaded and the yard was full of paratroopers a la Red Dawn, although this was a long time before Red Dawn. They were in Alaska and they were very close to Russia. He imagined that all the kids started screaming and the all ran to the gym and John was put in charge because he was in the Civil Air Patrol and he had the training. He would form the students into troops, do only a very brief training session because the Russians were in the parking lot, and they would mount a defense. Most of the time he was wearing a flight suit under his clothes anyway //(see RL320, RW137)//.

John and Merlin are now actually in a position where they have a high school gym full of confused students and they have the opportunity for a few brief moments every week to form them into hardened shock troops to defend not against Russian paratroopers, but against snorks. John has gone through so many scenarios in his head and has been preparing his whole life for this moment while Merlin was sitting here looking at garter belts. Merlin feels like he has wasted so many fantasies.

John just got a text from a friend who is at a local auction down in Renton and the text says: ”You missed the guns!” Renton is the town to the South of Seattle that used to have just a drug store and a hardware store, but now there is an RC modeling store on main street and that usually means your town is dead.

Right now if there was a naked girl sitting on his couch and a bag of freshly baked chocolate chip cookies he would still be torn. He would probably go to sleep first.




















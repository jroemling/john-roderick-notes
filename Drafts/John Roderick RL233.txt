RL233 - Babies are boring
2017-01-30

This week, Merlin and John talk about: 
* Merlin wants a clock ([[[Merlin Mann]]])
* Visiting open houses ([[[Stories]]])
* John’s cab driver with the boring baby ([[[Stories]]])
* Perfumes and some of John’s and Merlin's ex girlfriends ([[[Style]]])
* Tweeting with your crushes ([[[Internet and Social Media]]])
* Travel tips ([[[Travel]]])
* Robert Plant, does anybody remember laughter ([[[Music]]])
* John is not getting a Subaru ([[[Cars]]])
* Bumper Stickers and guitar case stickers ([[[Objects]]])
* Music banter  ([[[Music]]])

**The problem:** They were far apart on the suave scale, referring to two real estate agents who John thinks might be a couple, but one of them was very suave while the other was not at all. One of them had been shot in the head in Afghanistan and his partner seems to be very proud of him.

The show title refers to John’s cab driver who doesn’t want to stay home with his baby because babies are boring.

It is complicated!

+ Merlin wants a clock (RL233)

Merlin got a dingus for his plant that will tell him if it wants water or a different light. He is not allowed to have the clock he would like where he would like it, but he did find a very cool looking analog clock that sets itself via the radio signal. It also got temperature and humidity in analog with little hands, like our president or a Tyrannosaurus Rex. In the section of items that are frequently bought together it suggested that people are buying three different clocks at the same time. John started to get a lot of panicky email from companies where he had gone to look at something and left it in his cart. If you don’t get it now, you won’t have it! 

+ Visiting open houses (RL233)

Every part of John's existence, both in California //(where his Millennial Girlfriend lives)// and in Seattle is starting to be governed by looking at real estate. He has long lived in a culture where he would periodically go on Zillow and search around the area, see what is up, see what is going on, and see what the going rate is for houses that are for sale. His good friend Chadwick (which is not really his name) likes to go look at property in Seattle and periodically he will just straight up buy a house and he got like 4 houses now. He knows if a house is underpriced, but he also has a strong aesthetic and doesn’t buy houses that he doesn’t like. It is the same with stocks: You should only buy stock for products you believe in, and Warren Buffet says that you should buy things when they are not costly and sell them when they are more costly. 

Chadwick likes to get in his truck and drive around to look at properties not just to buy them, but he likes to look at other people’s properties, too. John admires him, frankly. He wants to see other people’s compounds and wants to see what is going on. There is no road in King County that Chad hasn’t been up. He is not a rich guy, but he is just savvy enough that he knows how to put together a downpayment and how to figure out this and that. He is handy enough that he can get the house in shape, fix it up and put a coat of paint on it, but this work does not seem onerous because he likes it and he is not flipping garbage houses. He is this guy who has this strange little portfolio of four cute little shingled bungalows around the city. What has John been doing with his life? He has 14 guitar, but they don’t generate any revenue. 

John was at an open house the other day where the realtor really had dressed the place up. You make cookies in the morning and right before you open the place up you make fried chicken! They might instead use a fried chicken candle because of the mess. It would be funny if you were a realtor that wasn’t very good, but who likes to cook and your open houses just turn the place upside down like it was Thanksgiving dinner. You are there with your apron on, swiping your sweated brow! Merlin went to a lot of open houses back in the day because his mother was a real estate agent. It was still in the pre-staging days. Now you can have services that will bring furniture and you can make it look however you want. 

Two interesting things happened: The two real estate agents were partners and John was trying to decide if it was a romantic partnership or if this was just purely a partnership. Many of these relationships start with a professional partnership and pretty soon they are a fucking key party. One of the realtors was a man from India in John’s age who was very suave. He kept saying things like ”You know, one of the ways we conduct our business is that we are very low pressure, like no pressure at all. If you just sign into this iPad app that we have and here is my business card and hold my wallet for a while” 

John was just walking through there and he was not going to buy a house. Not only was he not touching an iPad, but he was not even putting his fingerprints on the doorknobs. The other guy was a pale stocky shaved-head fraternity guy in his late 30s/early 40s who was not in possession of any suavedy whatsoever. John was trying to put them together and although opposites attract, they were very far apart on the suave sale. One guy was giving John the hard sell about how he is never giving anybody the hard sell, while the fratty one was super-nice and super-chill.

As they were leaving, they were walking down the front steps of the house with John and although John was trying to shake them off a little bit, they were coming with him. The fratty guy said that ever since he was shot in Afghanistan it had always been a little bit harder for him to get down these stairs. John stopped and took about 3 steps back toward him. He wasn’t leaving now! If somebody is putting out the shot-in-Afghanistan-card, John will be there and draw from that stack! Tell me more! 

It was a long rehab, the bullet went in behind his ear, just missed his brain and came out the other side of his head through his temple. He has a little bit of memory issues, he had to learn to walk again and he is still a bit unstable. If John had gotten a bullet all the way through you like that, he would talk about it, too, and he would find a way to bring it up in conversation. You can see the healed wounds in his head. He grew up in the neighborhood where the Open House was and he now lives three blocks away, selling real estate here. The Indian guy had been a real estate agent for 30 years and their expertise really combines. 

John didn’t want to buy a house before, but now he does, and he wants to buy it from them. He might as well have been in a cab with them, because all he wanted to do is stand there and talk to them about this experience. The Indian surely thinks it is great to pull out that story because John was so close to signing into that iPad. This was the reason why John thought that they might be in a relationship because he seemed kind of proud of his partner and proud of the story as if he hadn’t heard it enough times.  There is no way you can contemplate that enough times. 

John believes by his mane that this man had been an officer in the military and John was so focused on his injury that he did not ask the normal things like branch of service, rank and job. Unless this was a wound he received working on the railroad, that is a hard wound to just drum up sitting in the bar. At that moment it seemed very small to ask what branch of service he was in. The recovery was the interesting part of the story and we will get to how he got shot during the course of their friendship in his capacity as John’s new real estate agent, because even though John is not going to buy a house in this neighborhood, he doesn’t have the money and he doesn’t even want to live here, but he wants him over for breakfast. 

John didn’t find out enough about that story that he can retell it as a whole, but he is going to track him down because he has so many questions left. Just booking a meeting and pretend he wants to look at houses, just to hear this bullet through the head story feel usurious, but in the course of life John will recognize these two when he sees them. He could also sequester his partner and ask how the met and if he already was back on his feet. It all feels very complicated right now.

+ John’s cab driver with the boring baby (RL233)

John's took a cab this morning because his truck blew up, which is only one of many complications he has do deal with every day. The driver was from Senegal and lives in Everett, which is a completely different town and a long and unpleasant drive away from Seattle. He started driving at 4am this morning and he has to get back to his house by noon because his wife starts working at her job at 1pm and he has to care for their 5-months-old. She gets home at 11pm, he gets 5 hours of sleep between 11pm and 4am and then he needs to start cab driving again. 

That seemed unsustainable, but he will go back to Senegal where he is a civil engineer. His folks aren’t here, he has got no support-network, babies are boring, and he doesn’t want to sit around with a baby all day. John saw where he was coming from and he really liked this guy because he was talking John’s language: Babies //are// boring! The number of people John meets in a day is between 5 and 50 and for the most part he likes them. 

John believes in America as a place where someone from Senegal with a little hard work, ingenuity and American sticktoitiveness can plant a flag. A lot of cab drivers will take John home to dinner with their family because after he will get into a conversation with them they don’t want to drop him off, but they want John to tell their wife about the America he keeps talking about. John often tries to strategize before the end of the cab ride what he can do to make their life more sustainable. 

John's driver from this morning made a reference to the current situation. His wife is Senegalese, but she was born here. He has been here for 10 years and he is a US citizen, meaning that none of the current hullabaloo really affects him at all. Senegal is not a bad country like Syria either. He is a man and he should be doing hard work as a civil engineer instead of sitting around with the baby! In Senegal he has his family and everybody around him. To John it seemed that going back to Senegal was not the worst plan he ever had. 

He sounded like he knows what he wants in life. The fact that he is here and not in Senegal is what makes him the type of American that makes America great. The problem is that he lives in Everett and is driving a cab from 4am, he got a 5 month old baby, John knows that babies are boring, and he doesn't want to be there. When John had a baby, he had his mom here and could go wash the car. Merlin wonders how long that ride was, but John is not someone who sits in the back of the cab looking at his phone, which is the reason why people don’t believe he is an introvert. 

When John gets into the back of a cab and he has got someone who is basically trapped there with him. They all have a story they can tell him about themselves and it is the perfect climate for that. This cab ride was going to last 15-20 minutes and they were never going to get to the point where they are going to tell John what their feeling about a flat tax is. Also: John knows how to interview a guy, like ”Where are you from?” and not drop the ball, because he knows where places are. 

On this cab ride, John learned something about a guy, about his problem, and about his baby. Earlier, his wife was home with the baby and he was working 2 jobs which was fine because he likes working 2 jobs, but after the baby was a few months old she told him that she was going crazy and wanted to go back to work. It is not a bad baby, but it is just a normal baby, but they are crazy-making if you have to just sit there and stare at it all day. You can’t even go and wash the car! 

There is a lot of pressure on women to not even show a crack, to take all this stuff and not be bored and frustrated. That whole business gets put on them because they are a woman and this is what they are made to do. She was going to get back to work at her job and he was now staring at this baby? That’s the game? By the time John got out of the car, they had agreed that it was a good tentative plan to go back to Senegal for at least a year or two. John’s sense is that he is then going to want to stay and it will be unclear what should happen to his wife. She was born in America, but her family went back to Senegal when she was a little kid, so she grew up in Senegal and that is where they met. She had the option of going back to America for college and he went with her. Fast forward 10 long years later, he is still here! Time flies! 

+ Perfumes and some of John’s and Merlin's ex girlfriends (RL233)

They talk about the Rally Paris Dakar overland race. Dakar is where Dakkar Noir is made. When Merlin had some money from working as a bus boy, he went to the mall in Tampa and bought a big ass thing of Dakkar Noir and a white Miami Vice jacket on the same day. It changed everything! The show was lauded for a time and although he didn’t go to college in Miami, Merlin watched TV and had NBC like a normal person and he graduated to a more sophisticated scent. It was the era pre Ralph Lauren Polo, but post dad’s Aramis or grandpa’s Hai Karate. Merlin never fell for CK1, one of the first unisex fragrances. John imprinted pretty heavily on Obsession. Merlin’s first college girlfriend used the orange Calvin Klein Obsession and that will always be that smell to him now. 

There was a girl John wanted to go out with, not Laurie Basler and not the read-headed doctor that we don’t mention, but another girl from John’s High School who then went to the same college as John. They had flirted a lot and hard during High School, but they had never put themselves together, partly because John was a late bloomer and partly because she was a very unusual girl and put lots of sugar in her Pepsi like Ally Sheedy from The Breakfast Club. 

She was putting on Calvin Klein Obsession and she was chasing John back, but she used so much Obsession she had to bathe in it. It got on John, it was in every pore and he couldn’t shake it off. Merlin had that experience with one girl in 8th grade who used Jovan Musk and he was stupefied by the way this woman smelled. It laid him low! She had worn a shirt of him at one point and it still smelled like that when he got it back. He wanted to put it in a Donald Trump case because he wanted this smell to always be here. Donald Trump always has his props like his red hat in a plexiglass case. 

This have been really hard few months. They should give themselves a Phoney Award by being so good and not talking about politics on their program. 

+ Tweeting with your crushes (RL233)

There was a girl in John's High School who looked like the character in The Breakfast Club who used to put 40 sugars in her pop. It was either Ally Sheedy or John’s paramour [https://mobile.twitter.com/mollyringwald Molly Ringwald] who never responded to John’s tweets. John is not doing any better with Jane Wiedlin either. She came up in conversation the other day, but John figured ultimately that he had a really good exchange. It is a little bit of an Internet homes-slices-with-[https://mobile.twitter.com/MarthaQuinn Martha-Quinn], but in 2015 he had enough back and forth with Martha Quinn that he was doing okay. [https://mobile.twitter.com/janewiedlin Jane Wiedlin] has maybe replied one time, while Molly Ringwald has never acknowledged John’s existence even though he was following her really early when she had like 1500 followers. She is back in America now and that whole French thing was an interlude. 

Merlin got retweeted by [https://mobile.twitter.com/kdlang K.D. Lang] and [https://mobile.twitter.com/nekocase Neko Case]. He went a little bit political this weekend, but doesn’t feel great about it. The word on the street is that Neko Case doesn’t like John very much. She has strong opinions in people and John engenders strong opinions. 

+ Travel tips (RL233)

In Africa, they say Je Ne Sais Quoi. Nobody is going to like it if you spit on the ground in Africa! In Turkey, don’t shake somebody’s hand with your left hand! Merlin finds it problematic when somebody is handing him their left hand. He would like to teach an online course on how to shake hands and make those Millennials understand how important it is to shake somebody’s hand well. 

+ Robert Plant, does anybody remember laughter (RL233)

Somebody asked Robert Plant in an interview if he has any regrets after all these years of doing crazy things. He has been in an airplane with a bunch of people that all died and he was the only one to walk off. The one thing he regrets is ”Does anybody remember laughter?” That would be what John regretted, too! It was a thing he said out of nowhere and it is his thing now! When they were mixing that song and putting it out, they asked him to come by and check out the mix, but he probably did what John often did, which is ”I’m sure it sounds fine!” and then for 35 years people have gone up to him and said ”Does anybody remember laughter?” It is the one thing he could think of that he regrets.

+ John is not getting a Subaru (RL233)

John's trucks (plural) keep blowing up and yesterday his dear mother rolled her eyes at him for the 10.000th time until he said he will sell everything and buy a Subaru, which made her light up and which she found to be a great idea. Merlin heard on a podcast that Lesbians love Subarus and that Subaru have embraced it. When they first came on the market in Anchorage, they were an enormously popular car because they have 4-wheel drive and, as opposed to an SUV, people buy it to actually use the features it provides. There was an iconic light blue Subaru that came with cross-country skis already on it. 

The Subaru was the car in Alaska that you drove if you coached a teen cross country ski club, whereas the Volvo was much more the NPR car that was sold to them like you could survive any accident and people in Alaska die all the time getting T-boned on icy roads. John had a good friend who’s mom just got creamed by a massive truck in a straight-on T-shot on a major thoroughfare where a guy in a Ford Bronco didn’t even see the light and just hit her at 55 mph (90 km/h). She was in her Volvo TL and the only "injury" she received was that she had to use the other door to get out of the car. The Bronco was wrecked and the Volvo was just a twisted hunk of metal, but she got over to the passenger seat, got out and said ”Well! That wasn’t very fun!” There was no damage to her at all because the Volvo is some kind of miracle car. 

The Subaru is now the national car of Seattle (which is increasingly becoming its own nation). They all have bumper stickers, the most common one being [https://www.amazon.com/Peacemonger-0052PM-BS-COX-V2-Coexist-Bumper-Sticker/dp/B002MAJ5GG COEXIST] with each letter being the symbol of a different religion. John thinks Coexist has some association with Bono, which makes it even worse for Merlin. Others are Wag More Bark Less, Visualize World Peace and Visualize Tacoma.

Even though buying a Subaru would be an unimpeachable decision, it would be the kind of indignity where John would fall in everyone’s esteem a little. Think about the tweets he would be getting if he came on this podcast and tell everybody that he just bought a Subaru! A lot of listeners think that his next car will be some kind of anti-tank vehicle, like he will get a decommissioned [https://www.hammacher.com/product/personal-submarine Hammacher Schlemmer] three-man-submarine and put a massage chair in it. 

For John to reveal himself as being a real human being, like ”Subaru is the best value” or ”I’ve got a kid and it seems like a good move”, would be like finding out that the Wizard of Oz eats at Arby’s. It is not just about the listeners to this program, but John got a rep around his town. People would ask him if he had stolen that thing recently! No-one would stop being his friend or think that he would be any less, but he would fall just that 3% in their esteem. As soon as his mom turned away, he said ”Heck no! I’m going to get an experimental helicopter!” 

There are Subarus with some kind of blower or hyper-charger on the front and a lot of warts on them because they are used in drift-rallies. John can’t quite stand up with pride having a Subaru patch on his jacket and say that he has bolted on some extras. War is not the answer, but peace also takes courage! I’m from the Elizabeth Warren wing of the Democratic Party. It is time to use our outside voices. Well-behaved women seldom make history. Save our bees! Wild women don’t get the blues! In John’s dating life it was always one from column A and then one from column B.

+ Bumper Stickers and guitar case stickers (RL233)

Merlin sent John a photo of a car that he sees a lot near his house and that is full of bumper stickers in a way that only hippies with no taste can do. They mostly bothered to get them horizontally aligned and it isn’t totally crazy like covering your laptop in band-stickers in a totally haphazard way. They are parked in a handicap space and they do have a hang-tag for that, but San Francisco also has a ridonkulously high rate of abuse, because if you have that, you don’t have to pay parking meters. Did they get a hang-tag because they also have a fake comfort-horse? It is a hybrid Civic with a lot of dings in the bumper, meaning that this person is hitting a lot of cars. Those are the people who yell at John on the Internet a lot: People who John ostensibly shares politics with, but who have really a bad bumper sticker aesthetic.

It reminds John of his old guitar case where he constantly slapped other band's stickers on. The way that things fit into the van required that John’s guitar was always the last item to go in the truck and as a consequence Eric Corsan, their chief jigsaw operator, crammed it into a space that was just slightly too small for it. The stickers on John's guitar case were both compressed into the case and also ground up and shredded. John put more stickers on top of the shredded stickers and it was chaos-looking like the bumper of this garbage car. It was John’s Rickenbacker guitar which Merlin loves. He owned a 12-string once but had to sell it for rent.

+ Music banter (RL233)

If you are at a Campfire Spaghetti party in Rumania //(reference to story in RL69)//, the song ”Don’t go back to Rockville” is a good one to pull out. John knows a few people in REM and Peter Bach and everybody except for Mills are very literal. For years and to this day there is always a group of dudes standing right in front of Peter, watching him play guitar and trying to figure out the secret. Here is the secret: DGECF. Every time he gets on stage, he is like ”Sorry! It is just these chords played into a guitar that is plugged straight into the amp.” Maybe there is a tube screamer? 

Peter got that fast picking and he is an arpeggiator, but that is all there is. He is very forthcoming in saying that in the early days he was working in a record store and he barely knew how to play anything on a guitar. He went with his strengths, which is a pretty good rhythm and pickiness for playing open chords. Add a little bit of piano and you get the song Camera out of that. He is no Liberace, but Camera is a pretty fucking good song!

One time John played a show with Mitch Easter and his band Let’s Active including Mitch's wife. They both opened for Ken Stringfellow on the same tour where Merlin and John met, and they played in some weird little Georgia hole-in-the-wall. Ken loved to introduce John to people like Mitch, who said that REM was the only band he ever worked with where every single person in the band asked if he could turn their part down when they were listening to the mixes. That’s not how people are! 

John and Merlin continue to talk about REM. Their first three albums are Merlin’s go-to and John couldn’t love them more. John is not actually close to REM, but proximate to them and he was always happy when people compared The Long Winters to REM. For a little while John got compared to the guy from Counting Crows who did ”Mr Jones” because one person had written that in a review in the Salt Lake City alternative. Counting Crows does a song that Merlin likes and that he heard on the radio years after it came out. John thinks they have some very redeeming features and are very imaginative. Angels of the Silences is from 1996, the year that came right before the year 1997 that didn’t exist. Color in the Shape is a good album.

The other night John went down an Internet rabbit hole and came out all across his lawn without knowing how he got there. He was in Joy Division, ended up in New Order and watched every New Order video he could find. Merlin loves them so much! They have always been a very complicated listen for John because the singing is inarguably bad and not in tune, but somehow it works for them. Morrissey only uses three notes and only ever sings the third. Merlin wonders if John ever goes down Mr Buck Owens and the Buckaroos. John went down the Dan Rich rabbit hole at some point. Buck Owens’ heart was broken for years after he died, and so was John. John talks about Barrett Martin. People sometimes roll their eyes at the Grunge Rock era celebrity name dropping that John sometimes does on this program, but Barrett Martin is actually John’s friend, he is not just somebody he knows. Barrett is a very good drummer. 

Technically John is still a member of The Minus Five, because Scott McCoy always said that anybody who ever played in that band is still in it, like there is no such thing as an ex-Marine. You should however underplay that Minus Five card and not roll up to too many people saying that you are in a band together. It might work with Jeff Tweedy. The time John saw the Wilco (?) movie was a bad time in his life and he walked out of the theater with the taste of batteries in his mouth, but he has come back around since then. 

Hodgman is close with these guys somehow and everybody says he is astonishing. You can’t hate anybody who has been in the Minus Five except Collin Malloy (John is kidding). Unlike John, Merlin is a fan of things and he is a fan of Ula Tango (?). He met them outside an elevator at SxSW one time and thanked them for doing what they do. John has never had a bad experience with them and he has consumed them quite a bit, but he never played with them. John spent a lot of time with Lamp Chop. Merlin saw them at one of those terrible shows at the Great American where you have to sit at a table. 

John used to be a lot more reticent about just throwing band names around, but lately they have been doing it more and it feels like it is coming from himself. [[[What is in the show is in the show]]]! If stuff comes up, it comes up and then it is in the show and that is how you know it is in the show because it is in the show. Nobody ever called Pablo Picasso an asshole! Merlin is trying to come up with all people they have met. Pablo Picasso was a Jonathan Richman song, produced by John Cale. John sometimes goes back to Jonathan Richman and the Modern Lovers. Merlin is frequently surprised how much he enjoys this stuff. There are so many good songs and so many good tones! 

What always astonishes John about tones is when a clean guitar fulfills the function of a distorted guitar, like a hot cool treble-kicking guitar. It comes in a track and bumps the song up. John’s incorrect instinct would be to put a bunch of overdrive on it, but you can have a loud guitar that is not distorted. Both Merlin and John have never met Jonathan Cale. Jonathan Richman played at Bumbershoot not that long ago. He is 65, he still looks terrific, and he loves talking about busses. 

The famous story about him is making his drummer play with rolled-up newspapers at an old-folks home. John thinks he should have been more crazy with his bandmates instead of always wondering if he should have been less crazy. Maybe he should get his band back together just to see how he can fuck with them? No F’s tonight! Or ”no cymbals”, like Peter Gabriel. There are Peter Gabriel albums with no cymbals! John would say: Only kick-drum and snare, that’s it! Go for it, Phil Collins! Merlin has come around on him a little bit. John says that you can’t hate on Phil Collins even though you absolutely can hate on some of his choices. How is it that Peter Gabriel for all of his eccentricity never made a novelty song? Phil Collins made half a dozen novelty song. 

The biggest Dire Straits hit clearly a novelty song and in the 1980s a lot of novelty music was right up there in the charts, but Peter Gabriel’s Sledge Hammer is not a novelty song. Merlin thinks more of Winchester Cathedral or Snoopy and the Red Baron, which is a great song that was right up there on the charts. Idiomatic songs like Billy Joel's ”Do Up” are not novelty songs, because it is so in Billy Joel and it would be more novelty if he did not do ”Do Up”. The name of the song ”D'yer Mak'er” is bad because it is a pun, which you should never do. Before you know what you are doing, never make more than one joke at the time. Long Cool Woman in a Black Dress by The Hollies is not their normal sound. What about He Aint Heavy, He’s My Brother? 

America were two guys who met in Europe and wrote songs that sounded like Los Angeles. John would just like to see those spiral-bound notebooks and do a second pass over those lyrics. He would take alligator lizard out, replace it with other words, and they would not even notice. John will fight anybody who says that Sister Golden Hair isn’t a great tune, but he will not defend the lyrics! The song gets so close. Merlin and John go over the lyrics of Sister Golden Hair.

There is this Neil Young song //(Tell Me Why)// were we all think it says ”Old Enough to Repay, but young enough to sell”, but the actual lyric should be ”Not old enough to repaint and young enough to sell”, which would be the greatest lyric of all time. You would hope the first time somebody accidentally said Repaint, Neil Young would just cross off Repay and write Repaint and hope nobody ever noticed. Stipe has famously changed lyrics because he liked other people’s versions better. Neil Young probably just chuckled and say ”Nah well, moving on!”






















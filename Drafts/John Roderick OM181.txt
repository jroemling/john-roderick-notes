OM181 - Lottery Winners
2019-08-20

This week, Ken and John talk about: 

+ John having played the lottery at $300 million (OM181)

John had never played the lottery until a couple of years ago when he saw the Powerball go above $300 million for the first time. He bought a lottery ticket and then it became a game: Anytime it went over $300 million he would buy a ticket, but so far he has won $300 million zero times.

Ken saw billboards by the Washington State Lottery today with calculator-like numbers that are probably remotely controlled, showing what the Megamillions and the Powerball jackpots are. He was thinking the same thing: There are people who have a line when they will start to buy a ticket.

John has a filter on his eyeballs for all that lottery and gambling stuff, but those $300 million were more than what Mick Jagger is worth. There are super-duper famous people who have contributed enormously to the world and who aren’t worth $300 million and John could beat them! They could be at the same hotel in Saint Tropez and John could walk up to the desk while the person was standing there asking for an upgrade and John could buy them out!

+ John’s dad playing the lottery (OM181)

John’s dad bought lottery tickets and in his imagination he was going to win and then do all these things. As an activist he lived his whole life not pursuing money. He was a lawyer who worked for the government for a lot of his career and only late in life did he see many of his college friends and many of John’s uncles, who //had// pursued money, be in their 80s and live in Palm Springs while he himself was living in his car with three cats. Not really, but close! He lived in Tacoma, which is America’s car full of cats.

Working on behalf of other people is its own reward. You contrast yourself against people who are making money and seem miserable and your identity is: ”Guy who did the right thing”, but then you are 82 and your friends are rich and you are living on a government pension. Many people, John included, imagine a karmic payoff where somebody will walk up to them, hand them a bag of gold, and say: ”Thank you for your service!” John’s dad started to buy lottery tickets because he imagined that maybe this is how it works and if God wanted him to sit by the pool in his dotage he should buy lottery tickets, but it didn’t work out.

+ What would you do with $300 million? (OM181)

Once you realize that other people win at the lottery, you are wondering: ”Why not me?” and you think of how transformative that money would be! Every time John drives by one of those lottery adverts and his mind isn’t occupied, he will look at the number and think about what he would do with $300 million and just drift off. He used to have a filter and didn’t see this stuff, and Ken has that still, but it may be a little bit classist: ”That is what the other America does, but here in my temple of protestant virtue we pretend that cigarettes and scratchers don’t exist!”

Lotteries act as a regressive tax on the lower middle class because they are very easy for states to put up and they can then use the winnings to fund the schools, working around restrictions on how to raise money. The overwhelming majority of people buying lottery tickets are not billionaires. Mick Jagger is not buying lottery tickets, although he would have to start doing so if John pulled ahead of him. Currently he is not aware that John exists, or maybe he stumbled upon him at some point while looking at MySpace 10 years ago and seeing one of John’s songs.

+ Follow-up: Church of the SubGenius (OM181)

John got an email from the daughter of Ivan Stang, head priest of the Church of the SubGenius as a result of their earlier episode about the topic //(see OM172)//. She indicated that Ivan was a fan of John’s other podcast [[[Roderick on the Line]]] and has discovered the Christmas album John had made with Jonathan Coulton.

He became a fan of that album and actually came to see John perform at one of his concerts some time. Now John is in touch with his niece who invited John and Ken to come down to the compound they are building on family land in Texas. They are ready for the end of the world! If you have a church in Texas, even a fake ironic one, you got to have a compound!

+ Selling houses for $1 to fix them up, homesteading (OM181)

Rather than selling their house people sometimes sell lottery tickets for the house for $5 each and the winner wins the house. Sicily has a place where you can get a house for 1€, but you have to commit to not selling it for a certain amount of time and fixing it up a certain inspection standard. Baltimore had that as well. You could buy a row-house or townhouse in Downtown Baltimore for the price of tearing the plywood off the front door, but you had to commit to restoring it.

Maybe that is an option if you are tired of driving into town for crack? Let’s not besmirch the good reputation of Baltimore, one of America’s premier cities and not America's car full of cats anymore, but in the 1990s it was a different time. People in our era love Baltimore row-houses even before the president besmirched them because of The Wire.

If you homesteaded land in Alaska you could just stake off 40 acres and call it yours, but you were required to improve the land and live there, you couldn’t just stake off 40 acres and fly back to Minneapolis. Homesteading is hard! People get up there and try to overwinter, but then the 40 acres they staked off returns to the state of Alaska. Suddenly your job repairing copy machines in the lower 48 doesn’t look so bad!

+ The morality of lotteries (OM181)

The first time religion got angry about lotteries was when the Rialto Bridge and other big municipal projects started to get funded by them because God would not be on board with this kind of gaming, but it is difficult to say what about it should be intrinsically immoral.

Ken grew up in a Mormon tradition with a folk theology against face cards and in the more sheltered parts of the Mountain West you will see to this day LDS families who still happily play UNO, but would never play Pinochle because that has Kings and Queens and Jacks.

You could take the royal family off and replace them with Dogs, Cats and Birds or with Osmonds and other Mormon royalty. You can probably play poker because these people are all playing with Rook and Skip Bo cards, which are essentially isomorphic to a regular deck, but with 11, 12, and 13 instead of Jack, Queen and King.

It could be about the historical associations of gambling with low people, with drinking, horeing and other sins that happen around a Riverboat Casino, or it could be that there is something satanic or fortune-telling or occult imagery encoded in the cards, like the one-eyed Jack or the suicide King. These are the cards that least look like a terror deck with numbered suits and you would think the numbered cards would be just as sinful. Dice gambling would be even worse because it is numerological and if you have three dice, what do you do?

The not-incorrect original idea is that lottery is a vice because it is addictive and a right-thinking person will give their money to the church, not because the instrument of chance itself has a moral or immoral component. There are biblical examples of God’s servants using lottery after Judas hangs himself in the book of Acts and the Disciples are a man down. They need a new 12th guy and they have a lottery where they essentially throw dice to chose the new guy.

+ John drawing his mom’s ticket at a lottery in Elementary School (OM181)

Ken’s kids have an opt-out for raffles at school and instead of reselling the raffle tickets you can just write them a cheque for $50, but John’s school in the 1970s didn’t have that and John’s mom always used to yell about the bake sales where they were expecting her to make a pie and she would rather give them the $20 that they would sell the pie for because it would take her $100 worth of time and effort to make a $20 pie. It is the baked-in idea of the menace of money: Pie is virtuous and money is not, but in modern day Seattle everybody has money and nobody has time.

One time when John was in 5th grade in Elementary School his school sold raffle tickets and then they would have a wheel that you would spin like the one with the ping pong balls and the tickets were in the wheel. When they were looking for a kid to pull the ticket they called John out of the crowd because he had a lot of stage presence. They span the wheel, opened the door, and there were 700 little shards of paper in there.

As John was about to put his hand in he caught a glimpse of his mother’s very distinctive handwriting and he touched the piece of paper with his mom’s handwriting and continued to push his hand deep into the pile, wiggled his hand around, and made a big show out of it, but he had the piece of paper in his fingers. He pulled it out and the whole crowd was like: ”OMG! He pulled out his own mother’s ticket! What are the chances!” Nobody was suspicious because John had really sold it.

They won a side of beef, basically a half of a cow, and they rented a cold-storage place to store it and ate hamburger and steaks out of that side of beef for two years. John didn’t tell his mom about this for years. The few crimes he committed as a child that he confessed later were all crimes like this where everybody only thinks it tarnishes you reputation a little, but John never did anything like stealing or shoplifting.

You would call it statute of limitations because a kid is a different person. John had a very strong sense of honor even then and he carried it around like a monkey or half of a cow on his shoulders. He never would have shoplifted, but this little bit of graft was different. He felt bad immediately, but it was a temptation he could not resist.

In Elizabethan times they would have blindfolded the child and that is what they should have done to John. Today they know children are not to be trusted and they have moved on to an even more naive and untroubled-by-thought demographics: local beauty queens. You don’t want to blindfold them because they would lose half of their appeal or they would fall off the stage. John's hand was already in motion and he just caught a glimpse of his mom's handwriting and it is entirely possible that he fairly grabbed that piece of paper. What made it graft was that he had put on that show.

+ Lotteries being illegal (OM181)

When John was a kid there were no state lotteries because Congress had made them illegal nationally in 1890 and for 50-70 years numbers games were run in neighborhoods by Bowery Boys type kids, world-weary gentle old black men, or whoever runs those things in movies. People would pick their numbers and something in the newspaper would determine the winner. People would stick to their numbers for years! New Hampshire was the first state that legalized a lottery in the form of a horse race that also involved beauty queens.

+ Ken winning $2.5 million at Jeopardy! (OM181)

Ken had a little windfall 15 years ago at a gameshow where he won $2.5 million. He fell into the lottery winner trap and assumed he had this money forever and that he was set, but with inflation and whatnot a seven-figure amount is no longer ”Fuck off and buy an island!”-money. Ken is the real victim here because his $2.5 million don’t go as far as they used to!

People asked him constantly if he will immediately quit his job and every time he called home from Los Angeles after having won 5 more Jeopardies in an afternoon his boss would ask: ”Are you calling to quit?” - ”No, I am not going to quit! I am just telling you I have to play five more tomorrow and I am not going to be at the morning meeting!”

She was terrified that Ken was going to quit his job immediately because that is what lottery winners do. Ken actually has not quit his job to this day.  He went on an indefinite hiatus when he got his book deal because he didn’t have time to do both and he just never went back. He is technically still working there and he could show up tomorrow and say: ”Hey, is my cubicle still empty?”

+ Money doesn’t make happy indefinitely (OM181)

A lot of lottery winners will finally tell their boss they can shove it, but there are also cases where they go back the next day because they have been staring into the existential void all yesterday and would like to come back to the sawmill. Lottery winners are no longer delighted by the little happy-vicissitude of life, like a good cup of coffee, finding a quarter, or a warm sweater. It ruins your brain and nothing feels like a Mitzwah anymore.

Up to $50.000 additional money increases your happiness, but past that line in the sand it stays steady or even goes down. Until you reach a level of middle-class-not-having-to-panic, not even affluence, your happiness will increase because your are not constantly stressed by the question if you are going to make rent this month.

It all depends on how you measure happiness. The things that actually bring people fulfillment in the long run, for example having kids, makes them almost universally less happy and all the measures go down for the obvious reasons, but when they look back that is the thing that brought them the most fulfillment. There is something bigger than happiness that gives value and joy! John waited until he was 40 to have a child and the things it could have ruined for him, like: ”I wish I could just go out on a Friday night!” did not concern him because he had been out on a lifetime of Friday nights and staying home with the baby seemed way better.

+ John getting paid in large chunks for his music (OM181)

Ken remembers being panicky the first time he got a large cheque and he wanted to hold it with kitchen tongues. In his house it is called Wheel of Fortune money.

During the heyday of his band John would get paid in large chunks. One of his songs got used in an ad and suddenly there was a five-figure cheque in the mail and he was impressed. It certainly was the most money he had ever seen and he went from making $9000 a year to every once in a while getting a thing like this where he had to check himself before he wrecked himself and did something his dad would have done, like buy a Porsche.

Money represented leisure and leisure was a scarce commodity up until that point. A certain segment of people imagines that paradise is when you can just wake up whenever you want and do whatever you want, so John’s first money looked like freedom from toil and only later did he realize that this was a path to madness.

To Ken it really was nice that he could leave a day-job and do what he wanted all day. John could do music full-time. Ken could write full-time, which were not things they had time for before. Ken did however try to keep a 9-5 habit because otherwise he would go off the rails. John’s problem is that he doesn’t have any habits except his drugs and sex-addiction which he is constantly trying to combat, but going up in the morning and sitting in his office and working was never one of his vices.

John’s dad //(his ashes in an urn that John still hasn't spread according to his dad's wishes, see RW47)// was sitting on the mantel right there while they were recording.

+ John’s friend day-trading away all his money (OM181)

John has a friend who was in a successful Rock band and got a very large advance back in the day when major labels gave big advances. He thought he was smarter than his other bandmates and the rest of his friends and he decided he was going to teach himself how to play the stock market, specifically taking short positions in the market.

This was the era when everybody with a PC realized they could day-trade and beat the big guys. He sat at his kitchen-table and day-traded away $250.000 by taking short positions on penny-stocks. The whole time his friends told him to knock it off and put a down-payment on a house.

+ Take the lump-sum (OM181)

If given the option of a yearly payout or a lump-sum, even though you lose half your money in taxes, the lump-sum is better because inflation throws off the math. You are responsible for paying your income tax just as if you had made that money in a logging company.

For Jeopardy, California State Tax was deducted from Ken’s cheque, but he did not appeal as a Washington state resident to get some of his money back because California is the Nexus of your earnings and that state will get you and there is no way you are getting out of that. When Ken was making all that money he was living in Utah, a state with an income tax and really no business tax. Now he has a business but no income and he lives in a state with business tax but no income tax.

+ What to do with a lot of money (OM181)

Ken and John talk about examples of people who lost $17 million in a single year and they have a hard time coming up with what they would have to do to lose that amount of money. They focus on how to //not// loose the money, how to //not// fail, how to //not// become a mess, but other people are just listing the things they //would// do instead.

John’s whole lottery fantasy is based on the 25 things that he wouldn’t do or would need to secure. He has a list of 20 people that should benefit in varying increments if he would win his $300 million. Family members, but also old friends, people who helped him coming up and who supported him throughout the years. He feels there should be a windfall for these people as well and this is a component of a winning like this. 

John would immediately put one half of it away and for the remaining half he has a couple of charities he would fund, but he can also imagine setting up a self-flattering charity, something where he would have a charity to solve a problem. He does have the problem picked out, but he is not going to reveal it. Of course his sister and his mom would get $1 million, just run-around money, but then there are all these people for whom $250.000 would make a real difference in their life. It would not be a life-changing amount, but it would help them. There have to be tiers for your different friends to have them compare stock and people at the $250.000 level would recognize that they are at this place for valid reason.

Ken has never in his life bought a lottery ticket, not because of its sinful nature, but because he feels like the smart guy who doesn’t waste his time with this kind of mathematically dubious thing. There is some classism in that. When John said he doesn’t see the ads, Ken immediately thought: ”Yeah, exactly!” When he sees the little signs at the 7-Eleven he thinks: ”Look here! For the grace of God I am not one of those people!” People are not paying $2 for the outcome, but for the entertainment value of imagining to win. Gambling is entertainment.

The first time John went to Salt Lake City he rode his motorcycle to a park in the middle of Downtown, sat down on a picnic bench, pulled out his pack of cigarettes, looked up, and there was a sign: ”No smoking in public parks in Salt Lake City!” In Alaska you could smoke in a church! John was stunned, he didn’t know what to do and he walked out on the sidewalk, stood in the gutter, and smoked a cigarette while looking at the park.

+ Reddit (OM181)

There are three competing reddit-pages for The Omnibus:
* OmnibusProject: https://www.reddit.com/r/OmnibusProject/
* Omnibus_Futurelings: https://www.reddit.com/r/Omnibus_Futurelings/
* Futurelings: https://www.reddit.com/r/futurelings














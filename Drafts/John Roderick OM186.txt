OM186 - Battle of Palmdale
2019-09-05

This week, Ken and John talk about: 

+ War movies (OM186)

Ken is the least into war movies in this room because he does not have a weekly podcast where he watches [[[FF3 |Flying Lethernecks]]] with a bunch of middle aged guys, but he does enjoy a good war movie. John's war movie podcast [[[Friendly Fire]]] does not feature ”a bunch of middle-aged guys”, but John is the only middle-aged guy. There is a Generation Y guy and a 30-year old Millennial because they need someone to comment on every problematic war movie. They don’t have a Baby Boomer.

+ John being accused to being a Baby Boomer (OM186)

At one point one of the other guys' wives had asked John if he was the Baby Boomer on the show, which was infuriating because of Generation X erasure. Ken claims that John is 5 years away from being a Baby Boomer and  perceives that as a huge gulf, but John rejects that notion viscerally and claims that he is 10 years away from being a Boomer. The last possible case you could make for being a Baby Boomer is 1960, but if you are born in 1961 how could you possibly be a Baby Boomer? John Flansburgh from They Might Be Giants was born in 1960 and he is not a Baby Boomer, for the love of God! Obama is not a Baby Boomer and he is born in 1961.

John's dad was a member of the Greatest Generation, the parents of the Baby Boomers. He had John late in life and John is very decidedly not a Boomer, but John still has Boomer adjacency because he has 20 year older brothers and sisters who are Baby Boomers.

John doesn’t mean to disparage their many Baby Boomer listeners more than he normally does. The US Census Bureau has Baby Boomers going to 1964 and who is John to challenge the US Census Bureau on the basis of John Flansburgh’s birthday? The year 1968 is crucial to Baby Boomer culture because it is when that generation had its big celebration and coming-out party: The Summer of Love, Vietnam, the whole kit and caboodle. If you were born in 1964 you were only 4 years old and the definitive moment for you would be the late 1970s Disco Punk schism.

Ken will be the Baby Boomer who comes on Friendly Fire and talks about [https://en.wikipedia.org/wiki/Hellcats_of_the_Navy Hellcats of the Navy]. Ken is less of a boomer than John, but he will do a hilarious John Mulaney (inaudible) character.

A lot of what defines the Baby Boomer generation is the first half of the Cold War. Generation X is defined by Cold War anxiety, which they have talked about on almost every previous episode of the Omnibus. They have talked about The Day After more than the newspapers talked about it at the time because they have thought about it more than the newspapers did. The newspapers already moved on to something else while John and Ken were still bed wetting and still eying the cans of spam in their parents larder, saying: ”Please don’t ever make me eat that spam!”

John’s and Ken’s Cold War was the later 1970s/80s Cold War and not the Vietnam era. The 1950s/60s Cold War was just as paranoid and there was maybe even more of an expectation that there was an inevitability to it. It was the Hipster Cold War: They were into the Cold War before it was cool, although they had all the great film strips and little animated shorts of people trying to duck in cover. John and Ken didn’t have cute turtles on TV, which is the terrible thing about Generation X and why they are easily mistaken for Boomers: They had the terrible Boomer culture in reruns. The most upsetting scene in The Day After was when you see Tucker Turtle duck in cover and then he just dies instead.

+ John being in the Civil Air Patrol and being able to identify aircraft (OM186)

John loves the Cold War, and who doesn’t, because it was a very formative time for him. He had every aircraft identification book with silhouettes of airplane profiles and the idea was that you would be a civilian spotter for the Air Force who could say they saw two F-89 Grumman Hellcats //(The F-89 was not a Hellcat and later John refers to it as the Grumman F6F Hellcat)//. John was in Alaska and could see them coming over the Date Line! He could have been America’s first eyes on the scene, but he never got the opportunity to save the nation.

As a pre-teen and a teen John was a cadet in the Civil Air Patrol, which is a civilian auxiliary of the Air Force that does a lot of stuff that the Air Force doesn’t have time to do, like search & rescue missions. Every time a little airplane goes down somebody will call the Air Force, but they are busy with other stuff and will call the Civil Air Patrol who will take care of it. They will wake up an old guy in a hangar with a greasy oil-stained baseball cap with the brim turned up, like Indiana Jones’ mechanic who will wipe his hand off on a wrench and fire up the biplane. By the time John was in the Civil Air Patrol, Ken was a member of the Star Wars fan club.

John believed that during an armed conflict with the Soviet Union he would be pressed into service, perhaps in the capacity of being in charge of his Junior High, but he didn’t want to be riding the bomb. Instead he hoped to be one of the few who would get saved.

+ John imagining being the sheriff of Twisp, John vs Jim Hopper of Stranger Things (OM186)

Throughout his young life John had imagined a lifetime in politics. After he lost his run for the Seattle City Council he realized that he didn’t want to be in politics, but he still wanted to perform his civic duty and wanted to be in a marshal capacity, which means he needed to run for marshal of some small Western town. For several years he has been toying with the idea of being the Sheriff of Twisp. He thinks he would be a good small town marshal, one of the surprisingly gentle marshals who just takes everybody’s guns when they ride into town, like Wyatt Earp.

When the TV show Stranger Things came out John got a lot of letters telling him that they were doing a show with him as the marshal because that guy does have a very strong John Roderick energy. There was a thread online that pitted John against the Stranger Things marshal (Jim Hopper, played by David Harbour) with the question who would win in a fight (see [https://www.facebook.com/groups/futurelings/permalink/690653144680777/ here]) and the consensus was that the marshal would win. Even though John doesn’t contest that he was offended.

There was a discussion in a Jeopardy chat group about who could rattle off a record-breaking Jeopardy streak and somebody said: Maybe John Roderick from Omnibus could not be the James Holzhauer record breaker, but he would still be a TOC (Tournament of Champions) player, and John was mad that they thought he would just be a Tournament of Champions player and not a record breaker.

John agrees that he could probably not become one of the great Jeopardy champions of all time, but he is mad that the people on the Internet think so. John and Jim Hopper should join forces and beat up Matthew Modine. David Harbour is thicker than John is, he is broader across the beam and he is taller and that is how people see him. John and David are both gentle giants, but John would wear his uniform better.

+ John’s dad being a pilot in World War II (OM186)

In being a Cold War fanboy John became pretty well versed at a young age in all the different kinds of airplane. John did associate World War II with his dad who was a World War II pilot and he felt like he was only one kiss away from that conflict and the fan culture around all the armament and uniforms and culture of World War II.

His dad and his cronies used to sit around the living room and talk about their experiences in the war, but after the war they didn’t continue to be interested in the military, but they reintegrated into civilian life. In the same way that they didn’t make the transition between Big Band Jazz and Modern Jazz they didn’t make the transition between being interested in the military as it existed during World War II and as it made the world safe for democracy, which is how they thought of it.

They did some thing very intensely for 2 or 3 years in their early 20s, but that doesn’t mean it was going to be their hobby for the rest of their life. It set in stone however what they thought the military is and how it works, which is why Vietnam was so confounding for a lot of them: They weren’t used to being critical of the military, they didn’t think of it that way.

+ Rapid development of technology after the war (OM186)

As the world moved into the immediate post war era to what became the jet age there was a tremendous switch in what the military was, how it worked, and what the technology was. Right at the end of the war they transitioned into a completely new universe as a result of technologies that were developed during the war. Five years later they were using all jet airplanes, there was the thread of intercontinental ballistic missiles (ICBMs) and they were working on nuclear warheads, on satellites, and on things being launched into space. It was a universe apart and the threads were incomprehensible relative to a 1945 conception of the war.

At the time technologies developed quickly and a lot of those technologies took warfighting out of the realm of human reaction times. If you are in a propeller airplane, chasing another propeller airplane with a gun, there is a specific idea of what a dog fight is. A propeller aircraft can still go several hundred miles an hour, but if the plane in front of you turns and you turn, then your eyes and reflexes can handle things in real time. You are trying to shoot another airplane down with bullets, which has limitations: It is not guided in any way, but it just goes straight and the wind can blow it, but that is all taken as a given when you are in a dogfight.

As the transition was made to jet aircraft firing missiles that would move faster than human reaction times, jet fighters very quickly realized that if you got in close enough to use a gun against one another you would be very uncomfortably close to another thing moving 700 mph (1130 km/h) and for a while the Air Force took guns off of airplanes and went to a strictly missile-based style of air-to-air combat. This worked against them for a while because there were lots of instances where they had fired all their missiles, but the British still kept coming.

What is hard for people to grock is that these new technologies are still extremely imperfect to this day. There are plenty of instances where a drone pilot over the desert is given the command to fire a missile, but right at the end some child runs out and they realize it was a wedding and not a meeting of Al-Qaeda. This is a failure of intelligence and surveillance and not a failure of the missile, but there are lots of straight-up failures of the weapons. The way those weapons are depicted in movies when Tom Cruise fires a missile and it misses is only for dramatic purposes to let him come in and save the day with his uncanny and eerily powerful missiles. 

The first rockets that were fired from airplanes were basically unguided rocket-powered bullets. They were pretty effective if you were going to aim your plane at tanks and people running around on the ground and you could point your nose to where you wanted the rockets to go and fire a bunch of them at once. Shoot 100 missiles and get out of there! It is pretty effective because people on the ground can’t move away that quickly.

But as an air-to-air weapon an unguided rocket is extremely ineffective because the other person is also moving and they can turn their aircraft away. These rocket salvos weren’t very good as anti-fighter weapons, but they were meant to intercept slow-moving strategic bombers. During this period in the mid 1950s Intercontinental Ballistic Missiles were still not that far developed and it wasn’t really clear that you could fire an ICBM from Kansas and be sure that it reached its target in Slabovistan. It is more or less like using the US Post Office.

Firing warheads from bombers was part of the strategic puzzle of multiple platforms to deliver nuclear weapons. Submarines were a big part of that because they could get in close and be undetected while bombers were detectable by radar, but you could have them airborne all the time, flying around the polar space ready to penetrate, which is the plot of the movie WarGames or Dr. Strangelove.

The Soviets used strategic bombers as a major component of the way they threatened the United States. Early interceptors of the US were populated with rocket launching pads and the idea was that a bomber couldn’t really evade them quite like a jet fighter could and they could hit a bomber with a big cluster of little unguided rockets and generally be assured that the rockets would perform their job.

By the mid-1950s guided rockets and strategic rockets were all fairly reliable technologies and not long thereafter they developed a rocket that put men on the moon. They put their best, most clean-cut, square-jared guys on top of them and they hardly killed any of them, but it is extremely hard to shoot another airplane down from an airplane, neither Ken nor John have ever done it, and they are can-do Americans who have a thumbnail sketch of Physics and Aerodynamics.

+ John being a pyromaniac as a kid (OM186)

As a kid John used little Estes rocket motors, but not only to send model rockets into the air. Testors was the paint and glue you used to build model airplanes and Estes was the rocket motors. John conflated them both and first called the motors for Testies. If you ever combine the two and put your Testors on your Estes you are just left holding your Testies.

Ken never had that blow-stuff-up phase as a kid and he and his friends never even played with fireworks once they learned about the unsafe and insane ones. He hardly ever blew things up, although he had friends who did, but he looked down on them, which is probably a classist thing again.

John was a pyro, and he is saying this for informational purposes only, this is not a suggestion! He was fascinated by pipe bombs and they wanted to perfect pipe bombs. They used little solar ignitors that were meant to be used in model rocketry as the blasting caps to set off black-powder-based pipe bombs and John’s fascination in model rocketry continued all the way into High School although they were using it for very different applications.

+ Having a shelf full of books written by friends (OM186)

John has a shelf of books that were written by his friends, and that is a fairly substantial bookshelf now because he has plenty of smart friends and it is shocking that hasn’t rubbed off on him. Ken alone has written an entire shelf of those books.

John also has a shelf of books that have been sent to him that have been endorsed or autographed, like: ”To John from friend” and he has now a shelf of books that listeners to The Omnibus have sent him, books of 18th century locomotive crashes and books that were sent to them from Israel.

















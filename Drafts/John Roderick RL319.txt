RL319 - Cop Pants
2019-01-11

This week, Merlin and John talk about: 
* It is cold in Seattle ([[[Currents]]])
* Spiders ([[[Pets]]])
* Merlin’s trouble with ad-blockers ([[[Technology]]])
* Tactical clothes ([[[Style]]])
* Jack Dorsey wearing a Muumuu ([[[Style]]])
* John wearing a puka shell necklace and a polo shirt in 1998 ([[[Style]]])
* Sting, Quadrophenia ([[[Music]]])
* Hawaiian shirt, clothes sizes ([[[Style]]])
* Taking an ECG with the Apple Watch ([[[Technology]]])
* Doctors, professional courtesy ([[[Attitude and Opinion]]])
* Ed Brubaker, comic books ([[[Comics]]]) 
* Playing differently live than on the record, Europe being honest ([[[Career]]])
* Going to a 1990s concert, Vanilla Ice ([[[Music]]])

**The problem:** John wasn’t supposed to coast on his laurels, referring to European fans not liking bands who are only coasting on their laurels, but they want to see what the band has done lately.

The show title refers to Merlin thinking about cop pants when he thinks about tactical clothes.

”You may not like the picture of Benjamin Franklin, but you sure like what you can spend with it!” This is more of the home-spun wisdom that people come here for. Abraham Lincoln is both on the penny and on the $5, is it a coincidence? Washington is on the $1. 

+ It is cold in Seattle (RL319)

It is going super! They are recording this episode on a Friday because they had to schedule things. John is having a little coffee and is sitting at his computer with a blanket over his knees because it is a little cold. 47 degrees (8°C) in Seattle is pretty cold. Seattle is in a temperate zone without any ice fields or big storms, but it is just a little chilly.

If it is 47 degrees (8°C) in Seattle, you are going to know pretty quickly if you left the front door open because you feel a draft. If your electric power went off, it wouldn’t take you three days to notice. If John's Nest wouldn’t allow him to open his garage door, he might need to do a reconnection inside IFTTT.

+ Spiders (RL319)

January is not spider-season in Seattle, but the big spiders come in fall. When boy spiders get horny they do a little dance and then the girl spiders kill them and eat them. John has watched the spider courtship ritual pretty avidly and he doesn’t know how long spider intercourse actually lasts, but every single instance ends with her killing him and eating him. 

Merlin wonders if John is a Replicant, because Rachael (from Blade Runner) had that same memory implanted in her. She was watching an insect eating another insect, and watching them build a web all summer. John can barely play the piano. ”Do you like our owl? Home again, home again, jiggidi jigg” //(see Spider mating story in RW2)//.

+ Merlin’s trouble with ad-blockers (RL319)

Merlin has adjusted his microphone, he cleaned his office, and he is looking at new ad-blockers because his Safari ad-blocker has lost its foot and The New York Times, for which he pays money every month, is showing him giant fucking ads for keychain dildos or something. ”The very best umbrella”, ”25 insanely cool gadgets”, how do people live with this?

Before they started recording Merlin searched for Safari Ad-blockers and one of the first things that came up was a slideshow with the best options for 2018 where he had to click "continue" like on a tactical click-farm. Such a gallery makes sense for things like ”Favorite Nipslips of 2002”, but unfortunately most of them are Walter Sobchak or Queen Elisabeth.

John uses both Safari and Chrome, depending on what he is browsing. Merlin used to be all in on Chrome, but now he only opens it with great rue when something isn't working as expected in Safari. They constantly redesigned Chrome to make it look more and more stupid. Tabs are ridiculous now and they make weird colors when it is in the background. Merlin also hates the thing where Chrome really wants to be your default browser and that you can’t hit CMD+Q anymore to quit it, but they changed it to a different key combination. It became annoying! Merlin likes the idea of his Safari activity being synced up between all his devices.

+ Tactical clothes (RL319)

The other day John was at the thrift store and there was a vest with many pockets and loops on it. It wasn’t a Lebowski Walter Sobchak vest because it was black, which is the most tactical of all colors, but John wanted to get it and thought he could rock this super-duper! It actually had ammo-mag compartments where he could put extra flip phones in. John could deploy his Nokias like Chewbacca in a bandolier of feature phones. 

He could have them daisy-chained together, mining bitcoin while he was biking around with a baseball card in his spokes, ”Look at me! I’m a cop!” Sobchack’s vest is khaki, but he also wears those eye-glasses that you wear to shoot people. John will wear shooting glasses with this vest, but the label in the vest said something like ”Johnsville tactical vest company”, which sounds like Murfreesboro Tennessee, they make sausage and vests.

If you put the word ”tactical” on anything in America today it automatically become ”Make America great again!” Merlin’s first association with the word tactical has always been cop pants that you can buy from a website. People love to dress like cops, but Merlin likes to call it ”stealing valor”. 

When John thinks of tactical, he thinks of people in West Texas and of girls from the University of Tennessee who proudly show their concealed carry ([https://eu.usatoday.com/story/news/nation-now/2018/04/10/tennessee-college-student-wearing-women-trump-shirt-shows-gun-graduation-photo/501954002/ see here] or [https://www.foxnews.com/us/tennessee-college-senior-defends-posing-for-graduation-picture-with-gun-in-her-waistband here], [https://twitter.com/BrennaSpencer/status/982703688853741568 tweet]), like the girl with the gun who’s Twitter handle is @girlwithagun or something. Merlin blocked her because he just couldn’t see her anymore and she repeatedly appeared in his world. 

Merlin tells his daughter that you might think you brush your teeth for other people, but really you brush your teeth for yourself. In the same way, Merlin blocks people for himself, not for them, which he learned from John Siracusa: If you see a dingeling, block them the first time you see them. Merlin also likes the idea that in the future when somebody responsible buys Twitter they will comb through everybody’s block list and make an overarching dingeling block list that everybody can use. If you have been blocked by more than 5000 people there is a chance you are a problem. 

They use machine learning to figure out if you want to buy a jockstrap, but why don’t they figure out based on machine learning who the dingelings are? Why is someone not rapidly deploying a master dingeling list? You could have [https://en.wikipedia.org/wiki/Office_Assistant Clippy] make a reappearance, like ”Looks like you are trying to have a healthy mental life!” and it autofills for you ”Block this person”.

+ Jack Dorsey wearing a Muumuu (RL319)

In the early days of the Internet around 2004 Merlin knew a lot of people and those people still remember him because when John meets them now they check up on Merlin. ”Why did Merlin leave the party so early? What happened?” Merlin used to know all the Twitter people including Jack (Dorsey). He didn't used to wear Muumuus, but he was just very quiet. 

Merlin knew those guys before Twitter when it was a different company and they were cool and nice. Merlin knew Jack from an open source Mac service that he had made years before and Merlin talked about it a lot on a website that he used to do. One day he went to the office and Jack came up and introduced himself with his codename on the Internet. He was very quiet. It was so nice to meet him. John saw a picture of him in a muumuu and with bangs, which is a baller move.

+ John wearing a puka shell necklace and a polo shirt in 1998 (RL319)

The other day John watched [https://youtu.be/azYL5FYip14 a video] of the Western State Hurricanes playing on 29 Live in 1998. He had bangs to the point of having a bowl haircut. He was cutting his own hair and he was wearing a puka shell necklace. Merlin has seen photos of John where he looks like a jock that he would avoid. Nobody in 1998 wore puka shell necklaces! When Merlin met John he was still heavily into polo shirts and Stan Smith sneakers and he looked like the antagonist in a John Hughes movie. There was a period when John would wear two polo shirts and he doesn’t know what the hell he was thinking! He would not pop the collar on a polo shirt and he would not do a full Steve Bannon wearing three shirts. 

Steve Bannon may have body image issues, maybe he has dysmorphia, the Michael Jackson disease? When he looks at himself in the mirror he probably thinks ”I should put on yet another shirt!”, or he thinks he looks like a character from Dune, but not the one that he thinks. He thinks that he looks like Kyle MacLachlan, but he is actually Baron Harkonnen, the one with the breathing apparatus who floats around and sucks blood out of the boy’s plughole. John has never seen or read Dune. Merlin bought a copy for his daughter, but hasn’t read it either. They shortly talk about Dune.

+ Sting, Quadrophenia (RL319)

At the end of Quadrophenia (the movie) Sting with the nickname Ace Face is revealed to be just a bell boy at a hotel. The album with the same name was John’s mom’s favorite record for the longest time. It is a somewhat challenging album, but she wanted a certain complexity and that album had just the lack of a melody she desired.

+ Hawaiian shirt, clothes sizes (RL319)

John is always looking for Hawaiian shirts of a certain Batik-y kind where the print is reversed and it looks inside out. He likes the Polo-versions where the buttons only go halfway down. He doesn’t want to have a flashy one or one that has guitars on it. John has always been fan of these shirts. When he broke up with Millennium Girlfriend she stole all his Hawaiian shirts in addition to his Filson bags and some of his Mack Weldon underpants. After that he did a little bit of shopping therapy, trying to fill the hole in his heart by buying some additional Hawaiian shirts, old and cheap ones that he found on eBay, and now he has too many of them. That is John's game: If it is cheap and old, give him a call!

John is looking for long sleeve Hawaiian shirts which don’t exist and if you do see one it is wrong. The other day he actually did find and buy one and the label was ”Andre The Giant Has A Posse”, which is a guy from Rhode Island (Shepard Fairey) who apparently has a clothing company now. It started in 1989 and both Merlin and John remember those stickers that were everywhere. Later they said ”OBEY”. Now John is wearing that shirt and is feeling pretty comfortable in it. It is an extra large and John stopped pretending that he wasn’t an extra large after having had a phase where he was buying large clothes.

Merlin gets large Mack Weldon shirts that fit him like he was a European man after he washed them a couple of times, but when he gets a sweat shirt, he gets extra large because he knows it is going to shrink. Merlin is 5’9” (175 cm) and 158 (72 kg) and he is off weed right now, but that is a different story. When he is really nervous he goes down to 155 (70 kg) and he has been as high as 190 (86 kg), but he is pretty much a straight 160 (73 kg) most of the time. 

Merlin's ankles are getting skinnier and hairless and his middle section is getting more bulbous. He thinks that the reason old men don’t have as much hair on their legs is that they wear those tight World War II socks that pull them down. Some people wear tight socks on airplanes to keep the blood up in their brain because they don’t want an Pulmonary Episode.

+ Taking an ECG with the Apple Watch (RL319)

Merlin has seen ads on NBC for companies that they test you for $150 if you are stroky, but there is a chance they are going to tell you that you are at risk for a stroke and Merlin doesn’t want that. He does not want to know the day he will die, even if some UFO was offering to tell him. The Apple Watch can now do an ECG to test you for atrial fibrillation and although Merlin had it the day it was available, he didn’t open it for 5 days because what if they tell him that he was going to die? John still has SnapChat on there. John is setting up ECG on his watch and his phone while they talk. Merlin just did an ECG and he got a sinus rhythm at 87 bpm, which is a very high heart rate which they have talked about before.

The problem is that John switched his watch over to the left hand side, he Passed the Dutchie (song by Musical Youth), but he also made it flip around so it thinks it is on the right hand side. Merlin’s daughter did a similar thing. John got a sinus rhythm at 79 bpm. He does have an irregular heartbeat with a condition called pre-beat, which is a Dr. Dre thing: warm but punchy. Every once in a while his heart beat has a syncopation and leads the "1" a little bit like a Punk Rock drummer who is a little ahead of the beat. Merlin got the same at a time when he took a lot of Ephedrine. ”And I went to see the doctor... With a poster of Rasputin and a beard down to his knee” (Closer to Fine by Indigo Girls)

John has been to the doctor about his heart rhythm, but it is totally normal and some people have it, some people don’t. It feels weird when it happens, but for John this is not even in the Top 50 of things that feel weird when they happen. He used to think ”Stupid doctors with their diplomas on their wall”, but she didn’t come to see him (see RL258 in Currents and RW53 in Depression). She was the one who got John all fixed up with everything. ”I didn’t come to see you” 

For some reason John doesn’t get to see her anymore and there is always a nurse who does the things. They have probably decided that John doesn’t warrant the doctor’s time. They measure his height and weight, they sit him in a chair and listen to him bitch about whatever it is, they give him some thing and they go laugh about him in the coffee room. Stop it! First of all, stop telling him that he lost an inch in height (see RL93), and let him talk to the doctor because it is really enjoyable. 

+ Doctors, professional courtesy (RL319)

When John was growing up, doctors were the social and professional peak because his family were middle-class people who didn’t know anybody at the C-level of any company. Those were weird people from out of state, while doctors and lawyers were the middle-class aristocracy. John's friends’ parents were all doctors and they all wanted to follow in their father’s footsteps, but John knew he didn’t want to be a doctor. John's parents’ friends were all lawyers. Doctors and lawyers have professional curtesy and will tip their hat, but they don’t want to see each other. 

When John was grown up, he got to sit in the doctor’s office, crack wise with them and talk to them like he was a peer. His dad always told him to just call them ”doc” and you will already be on a good footing with them. John liked talking to his doctor, because they would sit and play the dozens, like ”What’s going on in your life?” and she would giggle. It felt like a peer relationship. Well, she didn't ”giggle” because she is from New Jersey and people don’t giggle in New Jersey, but she was a certain amount of bon ami. 

John likes to have a little bit of professional curtesy with everybody. When he talks to a bus driver, he wants that bus driver to know that John is just like other bus drivers and he could drive a bus and probably should have. It would have been a good job for him with plenty of time to think about stuff. You get the hum of the highway and John loves to drive!

John’s grandfather was a bus driver during World War II and he was not required to join the military because a bus driver was considered crucial to the national welfare. All those people making bombs needed to get to work! He drove long distances, like from Columbus to Lima and back every day. Postal workers were also considered vital to the national welfare. They sent the Hope Diamond from Africa with USPS because it was the safest way, which is probably not true anymore.

+ Ed Brubaker, comic books (RL319)

John doesn’t like tactical stuff, because it reveals too much about yourself. If you are a concealed carry person, the first word is ”concealed” and if you are wearing a tactical vest, it doesn’t matter if your gun is hidden. Ed Brubaker (see RL165 in [[[Comics]]]) didn’t wear a tactical vest, but a jeans jacket with a gun under it. Now he claims he didn’t do that, but he does it in good humor. He did a really good Captain America (probably [https://en.wikipedia.org/wiki/Captain_America:_The_Winter_Soldier this one]). John thinks Ed was going through a lot of things in the 1990s just like we all were and one of them was that he thought he should carry a pistol under his sleeveless vest.

Seattle had a lot of comic book people in the early 1990s and Café Roma was one of their hangouts. John was privy to the ins and outs of the social chemistry between comic book artists. They had Peter Bagge, Jim Woodring, Elen Forney, Meggan Kelso and other artists who ended up becoming pencilers or colorists for the big two //(Marvel and DC)//. Merlin and John wonder what the collective noun for comic book artists is. Maybe it is a depression of sad? For John a group of comic book artists was The Unbearable Lightness of Being where you have to wear a hat to have intercourse. 

They didn’t all get along with each other, but they were catty about each other. Ed Brubaker was a controversial figure because he has a bolder personality than a comic book artist is supposed to have. They are supposed to work in quiet isolation. He was not uppity, but he was out there, he had a loud voice, he was making the scene and in a way he was edging into what musicians were trying to do. Others like Jason Lutes were a bit tisk tisk about that. 

In Seattle in the early 1990s all the comic books were about awkward sex, and hate comics really set the tone for a lot of people. Merlin remembers alternate comics like Eightball. 

At the time John thought that these guys were nerds. This was early-to mid-1990s. [[[x makes y look like z |The 1990s are going to make the 1970s look like...]]] and it was a crazy boom time in comics. At the news stand (see [[[Employment History]]]) they did not sell Marvel comics, but they did sell graphic novels. By the end of the 1990s it was all tanking hard and comics as an investment was a real [https://en.wikipedia.org/wiki/Tulip_mania tulip situation]. At one time everybody had their money tied up in Beany Babies. 

[https://www.huffpost.com/entry/beanie-baby-fever-in-1999_n_58af7d12e4b060480e0661fe There is a picture] of a couple getting divorced and they are splitting up their Beany Babies in the court room. ”Cut the baby in two!” (lyrics Baby in Two by Pernice Brothers). These people look semi-normal. He looks like American dad, but she looks like Margot Kidder, maybe a little bit like Olivia Colman. It seems like maybe she decided that she is attracted to women and that is why they are getting a divorce, but she really cares about Beany Babies. 

+ Playing differently live than on the record, Europe being honest (RL319)

Merlin starts to sing Scared Straight and says that he is so glad about the horn section. One time John was playing that song in Spain and after the show he went backstage and some manager was like ”You know, if you took your music more seriously, I think it would be more popular!” In Europe they are less afraid to tell you the bad news. The Germans shoot straight and will tell you ”Eh, I’m not so sure” Merlin once had a girlfriend from East Germany who was very candid about everything. When an English person says ”We must have you over soon!”, the American thinks it sounds great and is waiting for the invite, but in England that just means that they won’t be seeing each other again. In Seattle ”Let’s hang out sometime!” means ”Let’s never talk about this again!”

Sometimes when John was playing Scared Straight live he would do an extended jam in the middle of the song where he would talk to the audience, like ”Is everybody having a good time?”, ”What’s your problem?”, ”Are you looking at me? You think you are better than me?” There has always been a slice of the record buying or concert-going public who want John to just shut up and play the song like it is on the record and John is a) not going to shut up and b) not going to play the song necessarily like it is on the record. He is sorry if you think that the money you paid to see him was to get some version that you think is the version, but John’s version is doing 40 minutes of free association and every once in a while he plays a song that is not like it is on the record. 

One guy in Germany told John he should write more songs about soccer. This is one reason why Mudhoney didn’t play in Europe for 20 years. John was talking to Steve Turner and told him that they were very popular in Europe, but he just said they never go to Europe because they hated it there. ”Don’t touch me, I’m sick!” They went there only in the recent 10 years and it was a big success. In France, England and a lot of Europe they like music that used to be more popular in America. They like cowboys, they like jazz, and they love the folk music. John’s entertainment business people in Belgium used to tell him that he should play in a more folky style because it was more authentic and more American than whatever the Pop Rock he was doing. John should focus more on his Americaness!

They want John’s songs in a folk voice, that thing that was popular in the 2000s, more like Bonnie ”Prince” Billy, they wanted it to sound fragile and backwards, because backwards was America. It would be a Mumford and Sons kind of thing who decided on a certain style. They were rich people from the midlands who all believed in Jesus, but they were more backwards American than John was. John has more a Pete Townshend live sound, but they didn’t always get that on record, because on record there were always some trumpets or something. 

One of John’s songs, which he said in Adam’s video, has an AC/DC riff and Mike Squires is imminently qualified to play AC/DC riffs. Somebody wrote a thing on the early version of the Internet that said ”I went to a Long Winters concert and they sounded like The Who, so I really loved it and I bought the record, but the record sounds like a bunch of grandmas putting [https://en.wikipedia.org/wiki/Boilie boilies] on the back of their couches, what happened?” - ”Because there were keyboards in the studio!”,  ”Did you notice the gate we put on the high hat?” If you listen to David Bowie’s album ”Low”, put your headphones on, but The Long Winters play like The Who when hey are live, because ”Fuck you!” and because he doesn’t have 11 keyboarders on tour. 

John would go back to Europe in a second if somebody invited him, but they don’t like touring bands who are coasting on their laurels. They want to know what you have done lately whereas in America you can just tour and tour. Still, if Kris Kross is at the county fair and they don’t play the jumping song, people are going to be mad. John mixes it up with House of Pain. The video upset Merlin because the guy gets a bloody nose in it which made him very uncomfortable. It is a good song! He is an Irish Rapper from Boston, it was pre-juggalo, but it was definitely juggalo and they had tattoos before it was a thing.

+ Going to a 1990s concert, Vanilla Ice (RL319)

John went to a 1990s concert this last summer with Vanilla Ice at the headlining slot and with Salt & Pepper, but it didn’t have UB40 because that would have been the 1980s. 9 or 10 artists all came out and played for 20 minutes like a show case with a lot of jamming. They had many dancers on stage, they did a medley, they hit the high notes of their 3 big song and danced off the stage. It was amazing to look at the crowd which was 100% white, the bands on stage were 100% black with the exception of Vanilla Ice and they were somehow satisfied by this 20 minute set that had a medley of their top songs. There were a lot of living color dancers. 

This was a version of the 1990s and although it was not John’s 1990s, it was somebody’s 1990s. Only 1990s kids will get this! Vanilla Ice was the headliner? Wow! Merlin thinks he had two songs. He constantly had to go out there and explain the difference between that and Under Pressure. He also got held upside down by his ankles by Suge Knight (see RL150 in [[[Music]]]) out of a window and shook him until his wallet fell out of his pants because apparently he lives in a Coen Brothers movie.

















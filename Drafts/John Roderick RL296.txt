RL296 - Bowl of Jay
2018-07-09

This week, Merlin and John talk about:
* Merlin's Thursday is his Thursday ([[[Merlin Mann]]])
* People zooming into pictures ([[[Attitude and Opinion]]])
* Making a living $0.25 at a time on eBay, gold miner mentality ([[[Money]]])
* John’s framed first picture of the Earth ([[[Objects]]])
* Jay Leno ([[[TV]]])
* John not getting enough sleep during school time ([[[Early Days]]])
* 4Chan and QAnon ([[[Internet and Social Media]]])
* 2018-July: The boys trapped in the Thailand Cave ([[[Currents]]])

**The problem:** John never had a pinky ring phase, referring to a commenter on Instagram asking John if he still was in his pinky ring phase on that picture, although the artifact was probably no pinky ring.

The show title refers to Jay Leno who is the cilantro of comics: A little bit can be really fun, but you don’t want to eat a bowl of Jay.

It is going pretty chill. They start the show singing. 

John’s mom said the other day that the corn was very high, because it is only supposed to be "knee-high on 4th of July", but that is not true anymore and it is way higher these days. Everything has changed, says Chuck Grassley and the history channel doesn’t show history anymore. John took some selfie with some corn the other day and it is way higher than the knee on Independency Day. Maybe it was Monsanto's fault?

If Merlin lived in a different climate, he would rotate clothes in and out of storage for different seasons. His pig corn sticks are only used for 2 months of the year. 

John’s dad always said that the coldest winter he ever spent was a summer in San Francisco. 

All of Merlin’s clothes are the same. He dresses like a developmentally disabled lumberjack and he always has, like has lesbian lumberjack, which is a strong look.

In order to make Twitter better, you would just need to turn off retweets for everybody. John's friends are not really that bad, but it is what they retweet that is bad.

+ Merlin's Thursday is his Thursday (RL296)

The phrase around Merlin’s house is ”Thursday is my Thursday”. Merlin does all of the organizational and quartermaster activities that he is increasingly responsible for on Thursdays. Those are things around supply and flow, like Don Rickels in CPO Sharky or Sgt Zale, there are always some errants, he will be putting toilet paper back in the bathroom, stacking up on soy drink, and a lot of mosquito tasks that are not worth taking time to do right this second. Instead Merlin is doing them in a batch. 

There is a recurring item to take the things that can stick you out of the kitchen drawer and put them into a box. Thrift Stores usually run masking tape down the blade so they can sell huge bins full of knifes and nobody ever gets stuck! The sharp things in Merlin's box are not inactive knifes, because he has a drawer area for active knives, and you should throw inactive knives away, but he has for example [https://www.thestar.com/life/food_wine/2016/08/03/cute-and-corny-holders-for-your-corn-on-the-cob.html pig corn sticks] that he uses when he eats corn. He also got one of those little dinguses to hone knifes instead of sharpening them. They continue talking about Merlin’s [https://www.amazon.com/HTZ-Mandolin-Vegetable-Julienne-Stainless/dp/B017BX4EV4 Mandolin onion slicer]. Part of Merlin’s Thursday work is also breaking down cardboard boxes and putting them somewhere.

John does not have a Thursday like Merlin, but he wishes that he had one. On Monday, Merlin does a lot of electronic things and he also talks to John on Mondays. Right now it is all calm, because it is summer and camp starts an hour later than school, which is such a blessing! Just when John reminded Merlin to call him, he was running an application that shows him large files, and he checks every Monday if his backups work. Monday can be a difficult thing and you do not have to be Garfield to believe this. A lot of what Merlin does, he is trying to improve as a person. 

+ People zooming into pictures (RL296)

John [https://www.instagram.com/p/BlAIRWBh-t4/ posted a picture] the other day taken in a bar in Innsbruck, Austria with John leaning on a piano from a pretty happy time in John’s life. It was as close to professional happiness as he ever got. The Long Winters were on tour and they were doing well in Europe. It was the most stable version of the band and while there were still problems, John was happy. They even went to Croatia and he just felt good in life. 

One of the comments on Instagram had zoomed way in, which is a thing on the Internet that both Merlin and John hate, and they said: ”You still in a pinky ring phase?” John has never been in a pinky ring phase, although it looks like he has a ring around his pinky in that photo. Maybe it was a string, maybe it was the ring of a keychain, there is no way John will ever know. If you said ”Scan this picture and find one weird thing”, John would not have perceived this ring. 

That is why Merlin doesn't put things on the Internet anymore. People these days stage photos for Instagram and put things in the background that you are supposed to find, like Where is Waldo. If Merlin puts a photo on the Internet, look at the thing that the photo is about and if you find anything that you are curious about, keep it to yourself! 

John’s house is so full of shit that every time John takes a picture you can see everything in his whole life just by examining one square foot of his house. Look at that, here is a Special Forces manual! John posted a [https://www.instagram.com/p/Bk-uEWkBuKH/ picture of his mom] the other day where you can see a lot of things around here. When John posted a picture of his Heisenberg graduation letter, he took the time to obfuscate the address, but there was an envelope behind it that he had overlooked ([https://www.instagram.com/p/BcnPfQ7lKrh/ he must have replaced that picture]). Merlin has concerns about John’s privacy. 

+ Making a living $0.25 at a time on eBay, gold miner mentality (RL296)

There is a viral picture of somebody selling a reflective teapot on eBay where you could see[https://www.snopes.com/fact-check/indecent-exposure/ the reflection of a naked man] with a penis taking a picture of a teapot. John doesn’t understand why people put certain things on the Internet. It is the garage sale problem: They are earning money $0.50 at a time and John doesn’t know how he could make a living like that. 

John is always hoping to make a big score, like a Lufthansa heist where you know somebody on the inside, but the key is to not talk about it afterwards. There are loads of people making a living selling 42 thrift store ties all for $0.25. Merlin thinks that people would stop collecting aluminum cans for return if there was no adequate money for there to be a return on investment for their time. If people go through Merlin's trash to find aluminum cans, he says ”Hakuna Matata, but please don’t make a mess!” Selling $0.25 ties on eBay feels like returning one can at a time. 

It is the gold-mining mentality: Gold miners are working their ass off. If you could get rich mining gold, there would be more rich gold miners. They are busting their hump every day! If you would had been there early you could have gotten a really good claim, but in Alaska that was 1880. The thing about gold mining is that people who are into it feel like the gold is there and the gold is free. You are not paying for the gold, but you just have to get it and in their minds the labor that it takes to get the gold out is different than working a job where somebody pays you. You are looking for a big score and every time you see that little glint of gold in the pan, you are thinking you are getting away with something. People put up all these eBay-ties for $0.25 and one of them one day will sell for $10 and they think they hit the jackpot. If John would open a vintage store, it would be the same thing.

When John was about 10 years old, his dad’s family got together and hiked the Chilkoot-trail, which was a full week of hiking over the golden staircase //(related story in RW71)//. There were horse skeletons, old boots and big steam engines that had fallen off that mountain. The Canadian border is at the top of that big mountain and there was a Mounty station. Every person who came across in that huge line of people would have to weigh their supplies and if they didn’t have enough food to last the winter, the Mounties would turn them around. 

There was also a whole secondary economy of people who would carry your bag or your sack of grain up the hill for you. People would open little shops to sell you the stuff that the Mounties said you needed. The town of Skagway, Alaska turned into a booming town, which is the whole history of Soapy Smith the gunslinger. There is a story of a boat from Klondike that created a frenzy when it arrived in Seattle loaded with gold. Those are the same people who are putting ties and little pots on eBay. 

+ John’s framed first picture of the Earth (RL296)

John has a framed photograph which is a print of the first ever photo of the Earth that had been taken from space from the first ever satellite //([https://www.smithsonianmag.com/smart-news/fifty-years-ago-this-photo-captured-first-view-of-earth-from-the-moon-180960222/ probably this one])//, a very low resolution image that is constructed out of stripes. This picture is 4 feet long by 2 feet high and is framed very elegantly in a mid-century frame of the period. It fills a whole wall and it might be from Boeing where Lunar Orbiter I was made. It might have hung on the wall of the people’s office. This thing is so big and so framed, it was an expensive thing to have had made, even! 

The picture has a story to tell as an artifact of the space race and John doesn’t know how many copies were made that were this big. It is not the Blue Marble picture that went around the world, but the space race went so fast in 1966 that 8 months later this picture would have been obsolete because there were 100 much more beautiful pictures of the Earth taken from space by then. It had to have been framed in the moment. 

John didn’t find it until the mid 2000s in a Thrift Store in a Boeing area. Maybe the person had retired and they finally cleared out something. The engineers there don’t have any sentimentality. John has filled this artifact with context that he can neither confirm nor deny and now he feels that this belongs in a museum. At the same time, if he would put it on eBay he could get $0.50. John doesn’t go to the space cons. You are not supposed to have moon rocks, but you could have a meteoroid.

+ Jay Leno (RL296)

Jay Leno annoys Merlin because he was so good when back in the days when he a guest on Dave’s show, but then he stopped being funny. He is the cilantro comic: A little bit can be really fun, but you don’t want to eat a bowl of Jay. When Dave really liked somebody back then it was such a good feeling and you were always afraid when Dave didn’t like somebody that it could get really weird. They continue to talk about Jay Leno. Merlin still watches the video from when he was on Carson the first time.

+ John not getting enough sleep during school time (RL296)

When John was a kid, he had little red portable black & white TV that he kept in the closet, because he could lay in bed at night and watch the Carson show and when he heard someone in the hall, he could close the closet door, because he didn’t even own a TV! Not then and not now. When his mom would look in, there would be a strange glow from the closet and he would be fake snoring, but everything else was fine. 

John would laugh and laugh watching Carson. He never wanted to go to sleep and always wanted to stay up as long as he could. It is sickening to this day that school for children begins at the times it begins because it is inhumane. Merlin was made to feel bad that he was physically incapable to go to sleep before 12:30am when he was 14. It was one more thing to be ashamed of when you were 14. It is puberty! It is your body! A teenager needs to get 9 hours of sleep at night, and then Merlin would fall asleep in class and it would be more shame. 

When John was in elementary school he would stay up super-late and fall asleep in school. He would tell the teacher that he was feeling bad and went to the nurse’s office. They would tell him to lay down and he would sleep for 1,5 hours. After he did that for the 10th time, they sent a letter to his mom telling her that John needs to get more sleep at night because he is pretending to be sick to come sleep in the nurse’s office. 

John's mom gets up at 4am. Her attitude was always that the schools were fucked. She would march down to the school every once in a while, waving some paper in the principal’s face. John’s sister’s second grade teacher once graded her paper with a C and their mom re-corrected his sister’s test and went down to speak truth to power. In the end everybody got disciplined. You don’t unleash her until you want to see into the 7th level. Don’t turn John’s mom on!

+ 4Chan and QAnon (RL296)

Even though he knows this is wrong, John will still occasionally hang out on 4Chan. They had this discussion so many times, it is as if John would say he came on the cat again. He needs to find another way to unwind, because that cat is going to be fucking mad! The only people who even know what John is talking about are all in agreement that he needs to stop doing it. 98% will say ”What’s that?” and the other 2% will say ”Jesus Christ! Don’t do that! Get off of there” 4Chan is John’s [https://www.theguardian.com/music/2012/sep/28/pete-townshend-internet-child-abuse-images Pete Townshend research]. 

John joined that community a long time ago and some small part of him is thinking that maybe the funny people are still there. He was there in that area of the Internet when [https://en.wikipedia.org/wiki/QAnon ”Q"] arrived and started dropping crumbs. Merlin is kind of obsessed about Q. There was a person on there not very long ago claiming to be a CIA contract killer, he said ”ask me anything!”, people were asking him question and he was answering them. There are people on there who are in the game and John was wondering what he was seeing there. Was this just a bunch of fucking cosplay?

Merlin [https://bigleaguepolitics.com/who-is-q-we-interviewed-the-anons-themselves-to-get-to-the-heart-of-the-mystery/ read an article about Q] by a disinterested third party interviewing Q: 

> This movement — centered around the 17th letter of the alphabet and also the 17th letter of the ancient Greek alphabet — dates back to 345 BC, where “Q” created an underground resistance based in the city of Corinth, home of Apostle Paul and the Book of Corinthians, to study and circumevent the tactics of the Romans. Corinth held its independence as a city-state before finally falling to Rome, but the movement continued.
> 
> Many say President Trump is Q. Is he?
> 
> When I asked the question to all the anons they chuckled.
> 
> “You could be Q and we wouldn’t know but one thing we can tell you is that the President communicates with us. You can see that in his tweets, his speeches and now you see us coming out of the woodwork at the rallies. We wanted him to say “Tippy Top” in a speech to confirm he was listening and low and behold he mentioned those words in a speech. “Tippy Top Shape”. That was our validation that he was listening and so we are making sure every human being on this planet hears us, sees us and joins us.”

Taylor Swift named her new cat //(John says dog)// because [http://imgur.com/r/4chan/mxd1D somebody got trips] on 4Chan. It is pretty well documented that Taylor Swift and Dany DeVito are on 4Chan a lot. They are on there saying ”I’m a celebrity but you don’t know who” and all the channers are like ”Prove it!”. So she said that the first person who will get doubles in their post number, which is a game they play on 4Chan, will name her cat. Each post has a long number because it is anonymous, like a Bitcoin address, and you can try and game it, but it is pretty random, and the goal is to get the last two numbers to be doubles. 22 or 55 or whatever, which seems like a virtuous way to spend your time. 

There are also triples or quads! It gives you standing in the community when you manage to do that. If you get quads on a post, the thing you said in the post has more credibility because of the sequential number they gave your post. Taylor Swift said something like ”Trips names my cat” and you got 80 people in there like ”Your cat is named fuck face”, each person trying be the one who gets trips in their number. Somebody did it and the name was luckily Meredith which is an actual plausible cat name. The next day there was an article in the newspaper that her cat was called Meredith. It is pretty indisputable and maybe Taylor Swift is still there, but nobody in his right mind is still there except John and he is not in his right mind. It has a bad effect. it is a bad place!

+ 2018-July: The boys trapped in the Thailand Cave (RL296)

John follows a guy on Twitter who believes that the [https://en.wikipedia.org/wiki/Igbo_people Igbo tribe] is the smartest tribe in Nigeria, which is like being a Yankees fan in Africa. John thinks it is indisputable that the Igbo are a cradle of a kind of civilization, they have art culture and they may be predominantly Christian now. The guy John follows is super-dismissive of anybody who would say that anyone else in Nigeria has anything good going on and John really loves his take, because he runs everything through the Igbo filter. 

Merlin loves those kinds of people. These days it is Elon Musk people who constantly find a way to make it about him. Everybody wants a hand in this [https://www.theguardian.com/world/ng-interactive/2018/jul/03/thailand-cave-rescue-where-were-the-boys-found-and-how-can-they-be-rescued Thai cave situation] right now. Merlin’s wife is following it very carefully and she couldn’t sleep one night so she stayed up and was learning why it was so difficult to get them out. It is really difficult! 

Now every time Merlin signs off on some permission slip, he is thinking that it might be some fucking cave adventure and he is never going to see his kid again! President Trump jumped in yesterday to let us know that America is working on it very closely. Then you got Elon Musk who is increasingly insufferable. 

Elon Musk used to be a character on this show. Merlin showed his wife [https://9gag.com/gag/anjwbKB/goal-become-so-rich-that-your-hairline-comes-back a picture of him] from 1996 and she couldn’t even recognize him. He got a lot of his hair back now, which led to a very interesting hair discussion with his wife this morning. Elon Musk had to chime in and let everybody know that he is inventing a submarine to get the kids out and several people said on Twitter that it is like he is reaching for his wallet after somebody else has already paid the bill. Elon Musk has become very weird and he seems increasingly touchy, which is not a good look. 

There were really bad pictures of Justin Trudeau when [https://www.buzzfeed.com/samstryker/young-justin-trudeau he was young], wearing a Bob Marley T-shirt. It is like a bad [https://en.m.wikipedia.org/wiki/Tim_and_Eric_Awesome_Show,_Great_Job! Tim and Eric] sketch. His idea what at mook looks like is always somebody from the mid-1990s. In 2000, Justin Trudeau looked like a mid-1990s-mook.  
















FF78 - Raiders of the Lost Ark
2019-07-05

//Intro by John Roderick//

Today on Friendly Fire: The 1981 pre-World War II movie Raiders of the Lost Ark, directed by Steven Spielberg. 

Let's picture this scenario: Hitler starts World War II having captured the actual Ark of the Covenant, which it turns out is an actual mystical and magical artifact of biblical times that awards its possessors the power to melt faces, shoot lightning bolts, and unleash lady-ghosts. That would have been a bad outcome, yes, and a tremendous irony also, but I think most importantly it would have proved the existence of the God of the Jews. This would have been a very big deal at the time, as it would now.

Not only that, but it would have proved that God gave the Jews the Ten Commandments, that the box containing the crumbled-up Ten Commandments had the power to melt faces, and that the Jews actually misplaced or surrendered this amazing weapons to the Philistine after losing to them in battle, which further suggests either that they did not use the face-melting functionality of the Ark during this battle, or they forgot the password, or the Philistines weren't troubled by it, all of which calls into question God's motives in delivering the Ark in the first place.

Now God, as we know, is inscrutable, but this seems like bad project management. All of this would have been amazing to learn in 1936 and especially astounding to have received this knowledge through the archaeologist Hitler. But for Hitler to have pursued and succeeded in this plan, he would have needed to have believed in the Jewish God. We know from the historic record that Hitler was privately very anti-Christian and although he publicly espoused a weird racist pagan hybrid-Christianity it is fairly obvious that that was just a cynical ploy to get the Hausfraus to sign off on Kristallnacht. But Hitler wouldn't have been off, secretly chasing the Ark of the Covenant for publicity reasons, to desire it he would have needed to be convinced of its supernatural power, to believe in the Old Testament God, but not the New.

Sure, this movie does some handwaving about how the Nazis were all over the world, gathering up occult items, but the Ark wasn't some witchy golden idol, it would have been the damn tablets from Mt. Sinai! Those tablets said right on there in the first one or two rules that we should have no other Gods! If the Ark had war-winning power, it by necessity obviated the occult powers of crystal skulls and little scarabs. Let's not play around! 

Following this, we can deduce a few more things about Hitler's relationship to God and his works: Firstly, that Hitler believed the Ark of the Covenant either had these powers in and of itself, like a gun that could be employed by whomever possessed it, which seems like a weird thing to think about the box holding the crumpled-up ten commandments that was specifically given to the children of Israel. or that Hitler believed that God of the Jews must hate the communist Soviet Union so much more that he would have been willing to use no less than Hitler as his temporary vessel of destruction, which also seems unlikely for even Hitler to have honestly believed.

So what is going on here? What becomes clear is that unless Hitler was completely batshit in 1936, a debatable point, pursuing the Ark of the Covenant could only have been a defensive play. He must have known that if he tried to employ it during the invasion of Russia there was as good a chance it would melt the faces of the Wehrmacht rather than the Soviet army and maybe would have sent lightning bolts through everyone on the field of battle, enough that it would have made for a very different post-war Eastern Europe.

Honestly, the God of the Jews has always behaved very mysteriously, never more than during this conflict, and the Ark might have just remained silent unless it works like a gun. It is unclear! Either way, it would have been a risky move on Hitler's part. All by way of saying that the US government within the continuity of the Raiders of the Lost Ark universe probably could have done more damage to the Nazi war effort by just letting the Nazis have the Ark of the Covenant, assuming that in his arrogance Hitler couldn't have resisted opening the box with at least a 50 percent chance of being attacked by ghosts.

See, now that is the kind of low-key strategic move the British MI6 used to know just how to pull off, but the US always fumbled. We have never been good at just sitting back and letting dictators die from ghosts. Now, I can understand why Indiana Jones was very frustrated at the end of this film by the US government's unwillingness to research the Ark of the Covenant, but I can not accept that given what he and Marion Ravenwood had just experienced, that they did not immediately begin going to temple and pursuing conversion to Judaism. They saw more than enough evidence to suggest that this God does not play with dice.

You want to talk to God? Let's go see him together! I've got nothing better to do! Today on Friendly Fire: Raiders of the Lost Ark.

... the war movie podcast that is like a radio show for talking to God, only you download it off the Internet and you can listen whenever you want.


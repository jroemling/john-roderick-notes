RL329 - Useful Hippie
2019-03-25

This week, Merlin and John are talking about:
* John having the movers at home ([[[Mid-century modern]]])
* Hate-listening to John's podcasts ([[[Podcasting]]])
* The Internet is broken ([[[Internet and Social Media]]])
* John fixing his Internet ([[[Internet and Social Media]]])
* John selling his house ([[[Mid-century modern]]])
* John’s sister doing a childhood trauma test ([[[Psychology]]])
* Self-care ([[[Psychology]]])
* Being vulnerable ([[[Psychology]]])
* John’s sister going to Ethiopia ([[[Family]]])
* John’s pool with logs of wood inside ([[[House]]])

**The problem:** You don’t always get the good guy, referring to calling the cable company and mostly not getting the good guy.

The show title refers to Merlin’s hippie ex-girlfriend who told him that he might not experience his full emotions.

+ John having the movers at home (RL329)

Yesterday John called a couple of different service people and some of them came and some of them didn’t. There is this family of dudes from the Marshal Islands who have over the years been really great to call up anytime you need four really big guys to come and take apart an airplane or deal with a bull that got out of its pasture. They come in and reliably make a big problem go away. Ben is the oldest of them and claims that all these men are all his nephews. They were at John’s, moving stuff around, and they are a little bit of a blunt instrument. It turns out you can be great at moving stuff around, but moving someone from one house to another requires a particular and different set of skills.

Good movers are tireless, usually pretty good at following orders, they think in three dimensions, and everything is Tetris for them, which always shocks Merlin because he should have this skill. They can utilize the inside area of a couch to wind around a corner and put it up into the air to get up and down staircases. The do the most appalling things with being able to move things, it is absolutely mind-blowing, but you do have to provide very clear instructions. They are essentially Bull Mastiffs in jump suits!

John’s bull wranglers have not mastered all of that. Yesterday when moving stuff around they got a couple of holes in the walls, a hole in a door and a hole in the ceiling, but John might be responsible partly for the one in the ceiling. He didn’t discharge his weapon because he only uses bladed weapons! At one point they were moving stuff around and sheared the cable connection off the wall, the coaxial cable coming out of the wall. 

+ Hate-listening to John's podcasts (RL329)

Yesterday a guy on Facebook left a post on a fan page for [[[All the great shows]]], saying that he hated John on this one show and it was really nice to see John’s ass handed to him on this other show that he also listened to, and on this third show of John’s that he also listened to this other thing happened that he also hates. He took the time to break it down for John and although he is mad about him he is surely listening to a lot of shows where the only common denominator is John. 

It is like deciding to eat a plate of dog shit every day just to show how bad it tastes. Merlin understands the concept of hate-watching and there are people he follows on Twitter just because they are a basket case, but that takes a real minimum of effort. He does not understand listening to hours of podcasts just to find somebody not entertaining. There might be something else going on and maybe they love John. 

+ The Internet is broken (RL329)

John just had to fix the Internet and he did it all by himself. Merlin agrees that the Internet is broken, but not broken enough, and he wants to show John his slide-deck about how he is going to disrupt the Internet and change the way people think about being sad. The Internet is broken all the way down!

Everything John is doing, all of John and Merlin, the whole universe, all has to go through a coaxial cable. It goes through an Ethernet cable to a dingus which is connected to a another dingus which goes through a coaxial cable into the wall which was installed by a guy who was showing John the back of his butt the whole day, and it goes out to a phone pole that is owned by ComCast and that goes through another dingus. This can’t be it, this can not be real! Even the sewers are better managed! 

John and Merlin's only choice for getting Internet is Xfinity by ComCast. Someone could probably find John’s house by figuring out which house in the city is the one where the other options get very proximate to without going over. The Venn-diagrams are all overlapping and right in the center there is one house that isn’t covered by anybody but ComCast. 

When John was [[[run for office |running for office]]] he made a big push for Seattle to start thinking about Internet as a public utility. There are lots of different movements around the country, some places have it, it is not that hard, and it should be a public utility. Internet libertarians are saying ”Why don’t you make it like the government? The government will do it efficiently! Blah blah” - ”Go screw yourselves!” The government will do it better!

Merlin says that you sure need your water and electric in the house. You need that pipe, you need to take another hit of the crack pipe or hash pipe, what is the band Merlin loves so much? John is trying to make a Weezer joke. The other day Merlin saw a video of somebody offering a vape to a series of people at a party, but it was a Nintendo remote and watching girls at a party take a hit off a Wii-mote is pretty funny. 

+ John fixing his Internet (RL329)

Yesterday John had some movers at home and at one point they were moving stuff around and sheared the cable connection off the wall, the coaxial cable coming out of the wall. Everyone has a giant bin full of cables and John as a musician has 40 giant bins full of various cabling. He could set up a 24-track studio in his house just out of cables, he wouldn’t even need the board, he could just attach the cables together and it would be a fully automated 24-track studio, like a Todd Rundgren style studio. 

John could make a sling that a Bell helicopter could lift a whale with, just out of Quarter Inch cables alone, let alone XLRs and MIDI-cables. Like anyone who has ever lived John had a whole bunch of coaxial cables of various lengths and every time somebody from the cable company came they said that these old cables don’t work anymore because apparently coaxial is the most vulnerable of all systems. John had boxes of boxes! 

This bin of 25 year old coaxial cable was not Aloha and didn’t help John, so at some point in the recent months he got rid of it and now he was wandering around the house with a sheared-off coax cable. Are you serious? This house was practically made of coax cable and he couldn't find a six foot length of it? Eventually he remembered that there was one behind the couch in the panic room underneath the piano between the fireplace and the monk hole. He pulled the couch out and sure enough: There was a 4-foot coax cable fastened to the wall going nowhere.

John flimmed with the jim jam, all the connections were broken and the coupling on both ends got sheared, but now John is on the Internet with Merlin again! John had Marie Kondominianed his house, he had gotten rid of a bunch of coax, but over there back by the monk hole he found a piece of cable, that was amazing! Might there some more somewhere? For every spider you see there are 17 spiders near you! This is why John used to put cigarettes over all the doors. If he was a rich person, he would have put little bindles of cocaine under every lamp, or whatever rich people do.

John's house had six different televisions in it when he bought it. It had been a rooming house and there was a television in every bedroom because every person in the house went to their own room to watch TV in the middle of the night. The outside of the house was wrapped in coaxial cable in order to get it from one location to every room in the house. It was wrapped like a particle accelerator, like the pictures you see of the Large Hadron Colider, like windings on a Humbucker, there was wiring all over this house, except that it had literally the opposite effect.

Every time ComCast came to fix Merlin's Internet it would get worse and worse. Finally he got to that one useful guy who said that he had no idea who did this, but they had added so many yards of unnecessary coaxial cables like layers of the city of Rome that Merlin’s signal was so degraded that it couldn’t make it up two stories. He took out a quarter mile (400m) of coaxial cable and then Merlin’s Internet worked. You don’t always get the good guy, but they are all equally confident! Merlin had even gone out to buy amplifiers! If they wrap your house like a Seymour Duncan //(a type of guitar pickup)// it is not going to work.

+ John selling his house (RL329)

Whenever John talks to somebody in a café and afterwards he comes on the Internet to talk to Merlin he doesn’t remember which conversation was real and which was to his friend who is just a hologram. Merlin is going to keep showing up anyway because he wants to bring John the opportunity to mirror whatever he wants to talk about. John hasn’t seen Merlin in a few months and Merlin might not even exist anymore, but he might be a bot!

One time eight years ago somebody put a [[[Supertrain]]] sticker over a Dead Head sticker on a STOP sign four blocks from John’s house, just so John knew that they had red-conned his place, but not so close that they were in trouble. They weren’t trying to sweat John, but they tipped their Fedora, which was a different kind of sweat. Don’t zoom in on pictures and don’t put stickers near someone’s house! It has to stop! Time for a quick visit from Gary the privacy concern clown!

John is selling his house and now is the one time when John wants people to know where he lives so they can spend their Internet money buying his house. Maybe three of them will get into a bidding war and John will be able to fly out of there in a UFO made out of dreams? He would put his bathrobe off and put it on them and he would take his sword off and hand it over to them and he would say ”Now you are the keeper of the play!”

The realtors told John he couldn’t live in this room anymore because they were going to stage it to make it look like he lived there. Imagine a house where a better version of you lived! It is very flattering that they are using John’s stuff to stage it, albeit very selectively. He knew these realtors for a long time and their general policy is ”Everything out!” because they have a warehouse full of stuff that they use to stage houses. This time they used John’s stuff, it looked amazing, but they wouldn't let him live anywhere.

John is finally relegated to the last room that he could possibly be in and they have given him until Thursday when they were going to transform that room into the master suite. All that is left in this room is a keyboard bench from a piano, a walking stick, the last bed that hasn’t been turned into a model bed, a banjo made of a cigar box, and everything they would take a pass on. John had two cigar box banjos and they used one of them. 

They liked one better than the other and then John had to feel bad for the one that they didn’t like because he feels bad for inanimate objects, like picking a shirt: How do all the other shirts feel, especially when it comes down to two? John is having a super-interesting time and he doesn’t know whether to call it stressed or not. This has been a weird month and a half!

+ John’s sister doing a childhood trauma test (RL329)

Because John is on the Internet and in Show Business, but also just living in the world today, he has a lot of friends who have experienced trauma over the course of their lives. They are dealing with it in different ways and there are strategies and books and stuff about it. 

John’s sister has been on this kick for a long time and the other day she [https://acestoohigh.com/got-your-ace-score/ came with a test] that she had been taken on the Internet that rated your childhood trauma by some standard. She wanted John to take the test and compare scores because they grew up in the same house. John’s score was zero and Susan was very surprised. These questions were not poems, but straight questions that you just answer straight and zero was not the score she thought. 

John would have no reason to lie intentionally and she started to read the questions, asking ”What about this one?” - ”No! What about it?” - ”But this is demonstrably Yes!”, talking about some things from their childhood. ”Oh, if you look at it that way!” - ”Well, no, it is not an interpretation, it is a yes or no question. Did this happen? And it did!” - ”Well, yeah, I mean, I guess!” - ”It literally happened!” - ”But it didn’t really affect me” - ”That is not the question!” 

It was questions like ”Were your parents divorced?” or questions like ”Did anyone ever put a cigarette out on you?” - ”No! No one ever did!” Susan was acting as if someone had put a cigarette out on him and he just didn’t remember it. John was not in denial, but he would not say he was checked out as a teenager, he would have said he was fully present and fully engaged, but he was under a lot of pressure.

For instance: "Was there ever a practicing alcoholic in your home over the course of your childhood?” - ”No” - ”What about mom’s boyfriend from 1980-88 who lived with us?” - ”What about him?” - ”He was an alcoholic!” - ”Yeah?” - ”And he lived in our house!” - ”Yeah?” - ”The question says: Did you ever live in a house with an alcoholic?” - ”Well yeah, but I just avoided him!” - ”That is not what the question asked!” - ”It is not like he was my dad!” - ”Thats. Not. The. Question! He lived. In our house!” - ”Well, I mean, sure, if you put it that way” - ”It is not a question of how I am putting it!” She was going crazy! For eight years they lived in a house with an alcoholic and he wasn’t even the only alcoholic boyfriend who lived with them, it was just the long one and he was a terrible drunk!

John was not trying to be brave or telling some kind of lie, he just didn’t think of it that way. This came up in the Michael Jackson documentary and in the surprisingly-very-good Oprah after-the-documentary interview: In order to understand what is happening to kids, you must account for the seduction part of this, which is: "Yes, it is horrible, but it is also really complicated!" A kid does not have a grown-up’s mind and the way they perceive things in the moment can be persistent for a very long time. They might unconsciously deflect the badness of a thing and then it becomes encoded in how they think about it for years. It is not until you sit down with somebody that the person realizes what happened, that is how we survive life. John is not being evasive, but he is just being human!

This conversation with his sister has been blowing John’s mind! It was part of his Aloha-thing. Susan always has some book that John needs to read and over the course of 45 years she has been deploying them at John like a chef at Benihana, she has just been flicking them at him. For example when they were kids John was supposed to read ”Our Bodies, Ourselves” Lately John has been ”Okay, I’m listening, or whatever!” and he has even left the ”whatever” off, which was new. It is part of the spring-time Aloha!

Over the course of his life, through all the years he had been sober and in the millions of conversations about drinking and drugs he has had with people in all different places, one question always came up: ”Did you grow up in an alcoholic home?” and John always answered that his dad stopped drinking before John was born, which is true. In all these years it never once occurred to him that there was a practicing alcoholic in his house all through Junior High and High School. It was news to him!

Susan went down these 10 questions and asked ”What about this!” - ”No!” - ”But what about this exact scenario? This question basically is the exact scenario of you!” - ”Well, I guess, if you put it that way!” and she was just climbing the walls. She made John see that these were not interpretation questions or essay questions, but it was ”Yes or no!” John had these many layers of ”Well, that doesn’t count because by that point I had already joined the special forces" - ”But you were never in the special forces!” - ”Well, technically!” For the last weeks John has been wandering around, thinking that he scored a four on this thing, which in and of itself is crazy, but even more crazy is that he thought he had scored a zero.

Merlin suggests that it is not in John’s makeup to Monday-morning-quarterback being 12 years old. He can look at it with a sense of humor, he can have a little fun with it and interrogate aspects of his personality, but it isn’t a way of collecting a debt about what he is owed or what he is entitled to in the world because of what he ”suffered” in life. Doing so does not seem like John’s MO and is very off-brand for him. It is really not how John looks at the world!

+ Self-care (RL329)

Lately everybody has been coming at John from all sides. His sister, Aimee Mann and his psychiatrist have all been on him about it and told him he needed to practice self-care, but stop using those words because John has no idea what that means! Merlin had experienced the same when he was getting ready to leave his college town to get a job. He told his very sweet hippie ex-girlfriend that he was not sure about leaving and she said: ”You know Merlin, sometimes you are not fully experiencing your emotions!” - ”I really should go, I think I am double-parked!” Merlin has thought about it since 1990. It was a very memorable line that made him very angry in the moment, but he does still think about it. Are his emotions something he doesn’t feel entitled to for whatever reason? It is just much easier to put it behind him and go to the next thing. It is a not-dissimilar thing when a useful hippie comes along and makes you sad.

Merlin loves Susan and her wonderful improbable combination of deeply caring about John and also just being fucking infuriated that he wasn’t doing it correctly. She was trying to say that she was caring about him and that she wanted him to know that he is good and okay and he can make it. Their relationship has always been in a way that she would say something to John and he would be ”Yeah, yeah, yeah, whatever! Gotta bounce!” 

Then she gets mad and throws an ashtray at him, but lately she hasn’t been throwing ashtrays and John has at least been sitting still and listening to her. He needs to practice self-care? Every three weeks he gets a pint of Ben & Jerry’s and eats it in the bathtub, is that not enough? Has he not self-cared enough? He got his tray, his crossword and a little bit of ice-cream, he got it all! He doesn’t even smoke cigarettes, but he always has one on the tray there with his undressed salad and his three iPads. 

"Self-care” and ”working from home” both sound like euphemisms for masturbation to Merlin. John keeps hearing that there is apparently a child inside of us that needs to be protected and cared for by us, but he has no sympathy for that kid! He needs to get up and fucking do his thing! His dad would say: ”You don’t get a medal for doing what you are supposed to!” Welcome back, Dave! John would ask ”Why does that kid get a medal? I never got a medal!” Susan will tell him that he is very hard on that child, but fuck! What did he ever do? It is a long process!

John is sitting in the only room in his house he is allowed to go, together with his Memento Mori: His walking stick, his cigar-box banjo that his realtor didn’t want and his Internet-mattress. He also got his favorite chair and his paddle-ball game, but when he made that joke yesterday nobody laughed, they all just rolled their eyes. Everybody who gets his jokes from The Jerk or from Caddyshack is always welcome to sit with him. John is thinking about this stuff all the time! He is walking around and although he does not feel he is going to cry, his eyes are welled up with tears all the time, like when you get to the point where you are so sad that you cry.

+ Being vulnerable (RL329)

Several years ago when they interviewed Asher Vollmer, the creator of the iOS game Threes, John said: ”This is not a game for vulnerable people!” It reintroduced the phrase ”vulnerable” into Merlin’s life, which is not a word that people use all that much, except to talk about impoverished children or vulnerable populations. We are scared about the idea of vulnerability and most of the time we don’t want to be vulnerable because we don’t want to be seen as weak. Vulnerable ultimately means that you are just weepy and sad all the time. 

Merlin is very interested in being more vulnerable and he wants to become vulnerable in more interesting ways. Part of that is that he doesn’t turn away from something that makes him feel feelings because he can sit in the moment with that. Some things don’t make him sad or happy, but he is vulnerable to whatever his heart is picking up from what is happening, which sounds super-corny. Vulnerability does not mean that you are a wuss, but it means that your heart is open to what is there right now. 

It is not entirely dissimilar from John’s Aloha-project: John has made himself a little bit vulnerable on purpose and he has become vulnerable in a more interesting way in order to see stuff he hasn’t seen before and notice things he hasn’t noticed before, and in order to learn what the fuck is actually going on here as part of his long-running John Roderick project.

Until John's early 20s all his emotions got compressed down into sadness. No matter what happened or what he felt, it always went into a function-machine and came out the other side as paralyzing sadness. After he got sober there were a couple of years where he was experiencing his feelings for the first time and he didn’t know what they were. As he tried to feel them and not just have them all turn into paralyzing sadness they turned into anger. Almost any feeling can turn into anger if you are not careful!

It took Merlin a long time to realize that part of depression is anxiety. They are actually both compatible and they work great together to a wonderful pairing, but left to their own devices they all will eventually converge toward anger. He found that to be very true and you got to fucking stand over that and really watch it because those things want to become anger and if you are angry you are not vulnerable anymore.

When John was young he couldn’t afford to be angry and he didn’t have the power to be angry because he needed to keep everybody else alive. That is why he learned to be funny and outgoing: He needed keep his mom and sister and everybody laughing. There was absolutely no room in his family for him to be angry because everybody else was so fucking angry. There was not space for him to be angry, dissatisfied or upset at all! 

When John and Susan entered the picture, no-one else in the family wanted them there. Now they laugh about it, but none of their uncles or cousins or aunts or anybody liked them. They had unintentionally fucked up the status quo by being born and they were this second generation that no-one had asked for. John’s family didn’t like each other and was already set in their ways of how they hated one another, but then Susan and John came in, tied to their dad, his second marriage and all these different other strada. They were disliked when they were infants. It wasn’t overt, but nobody ever loved them!

When John started getting mad he felt out of control like a crazy person because he had never felt any emotion that wasn’t just paralyzing sadness. Now he was mad, he was a big full-grown man and he was scary! Merlin says that sadness is very inward-turning and anger is very outward directed. No matter how hard you make your "sad", you might seem a little bit goth or emu, but anger is necessarily explosive. Sadness is not only a way of controlling and reigning in yourself, but it also means that you are not going to break everybody's expectation about how you are supposed to feel and then how you would demonstrate in a public way how you feel. You have to keep that inside or you are going to be at best disappointing.

Whenever the anger came on, John knew it was terrible! Even if you are just standing there, clenching your fists and grating your teeth, you are already scary. You don’t have to be yelling or even moving, you can just sit there and be fucking scary, just in your little dark cauldron, but John didn’t want to go back to being sad, he knew that going back to being sad was to go back to this dead-inside place where everything recapitulates sadness. 

People were telling him that angry was the path forward and he had to be angry and go through to the other side, but John was angry for a long time and he didn’t know how to go to the other side. He was not reading any of these books that his sister was giving him, she and their mom were talking about their past lives, but John was just trying to get to the refrigerator for the love of fucking God. He wanted the morning paper and the evening paper, and he was going to sit in the bathtub while they could talk about fucking everything. Now John does feels vulnerable, but he does not know what to do with any of it.

+ John’s sister going to Ethiopia (RL329)

Last night Susan was going to Ethiopia and John drove her to the airport. She was going there because there is this woman and some other people and cousin Page got involved somehow because she runs a non-profit and this other thing. She was flying to Chicago and then taking a 15-hour flight to Ethiopia to spend 10 days there flying around in puddle-jumpers, doing some kind of thing. None of that made any sense to John, but Godspeed! She was super-anxious and didn’t want to be on an airplane for 15 hours.

She grabbed John by the shirt and said ”Your whole life you never had a home and when you bought your house and got your home it was the first home you ever had that was //your// home and that felt like it was a home, but now you are selling that house and you don’t have another house to move to!” John is living in the one room in his house that he is allowed to live in while they get ready to sell it out from under him. It is all him who is doing this! It is not that it is happening at gun point, but he is initiating this. 

”You are selling the one safe-place you ever had! You never felt safe anywhere in your life and now here it is! I am really proud of you trying to be vulnerable and trying to do all this, but you have to let yourself off the hook and forgive yourself for freaking out because there is a reason you are freaking out!” Merlin was quoting the lyric ”I never had a home where I didn’t need to hide” from the Long Winters song Carparts, which is Merlin’s favorite [[[The Long Winters |Long Winters]]] song!

Susan is wise and John never would have thought of that. If you had asked him about that line in that song only two days ago he would have given you a very general explanation. Merlin says that every single line in that song is perfect and he loves it so much. John wrote that and now he is sitting here, looking around and realizing that the fact that they staged his house to look like he lived here makes it a pretty nice place and he really likes the guy who lives there. He only has a couple of swords, not like 40, and this place is just an ingot short of being really nice.

The realtors asked John if he wanted to stay there and stop the whole process, because he still could, he could just tell everybody to go away, but John had to sell it! He didn't know where he was going to go next, he didn't know what it was going to be like, but he couldn't retreat now. He had to do this for some reason! He is not motivated by running or by a feeling like ”You started it, so now you have to finish it!”

There is no negative voice in that conversation: ”Do you want to sell your house?” - ”Yes, I do!” - ”Why? According to your sister you are selling the only safe place you ever had!” - ”Yes maybe, but I have to! It is time to do the next thing!” Two days from now he will sleep in the bathtub because it will be the only place they will let him be. At least for now. Eventually they will also have to stage the bathtub and put a tray in there and maybe some chili dogs from one of those Japanese plastic-covered...

+ John’s pool with logs of wood inside (RL329)

John's pool is still belumbered, it is really incredible, and yesterday John showed it to Marshal Island’s Ben and his nephews. They told him that if he lit his pool on fire, the Seattle Fire Department would fine him $50.000 and he would burn the neighborhood down because there was no way he could contain a fire like this, but sometimes you will have to destroy the neighborhood to save it. 

John asked Ben how many nephews it would take to clean the pool and Ben was like ”Wow!” He had all these ideas: First of all John should take a picture of it and put it on Craigslist, saying ”Free wood!” The number one thing that Craigslist is for is free wood and this place will be picked clean by people living in the city in an economy where firewood is still how they are trying to save money.

Every tree John ever cut down in his yard is in that pool, it is like a scrapbook for plants: The tree he cut down out of his neighbor’s yard when they sold the house is in that pool. His neighbor’s house was for sale and that neighbor was never coming back, but before the next people bought it John chopped down the tree he hated because it was getting up in his shit. He did it even before the people made an offer on it, so they knew they were buying a house without that tree.

The guy who was selling the house didn’t care and nobody noticed the tree was gone. Once there was a big storm that knocked down all of the cedar fence that John had put up and it all went into the pool. All the cut-down blackberries, everything goes into the pool! Every time John would mow the lawn he would dump the cuttings in the pool.

Ben recommended to put ”Free wood” up on Craigslist and this place was going to be picked clean like a National Geographic sped-up footage of ants on a forest floor. Then they would just have a swimming pool full of moss and dirt and maybe some cinderblocks, boat anchors and whatever, and he would get the nephews in here to clean out all the dirt and spread it around the yard. For the pièce de résistance they will then clean out the pool and paint it blue so that people will recognize it as a pool, which will raise the resell value of his house by $15.000. There is a pool! It is not operational at this moment, but this is Seattle and this is a fixer-upper-pool and it is now spring!

During the whole time John lived there, every few months somebody would tell him that he needed to fix up that pool. Think about the times they could have had! John was thinking about that pool as well! He always said that once the logs in the pool would get up to the top he was going to have a huge bonfire and it will be great, but he didn’t want to get fined $50.000 and burn down the whole neighborhood. 

But paint the pool blue? Ben put a hole in the door and in the wall and he was maybe partly responsible for a hole in the ceiling and he sheared off John’s coax-cable while moving stuff around inside the house, but how much damage could they do moving dirt around outside the house? That is more of their core competence. Like Thomas Edison: He kept failing until he succeeded! It might take 100 bad ideas until you get the good one!

John only has one coffee cup because it is all they let him have, he got his computer and his microphone and his paddleball game. He found a coaxial cable, he should be looking for cigarettes and other shit he might have hidden around, he might have more stuff in that house than he realizes and he should go plum the monk hole. John doesn’t want to sell this house and then have somebody open a door and go ”Is that a silver ingot?”


















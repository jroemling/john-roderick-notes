OM259 - Love
2020-05-19

This week, Ken and John talk about:

+ Trying to figure out the meaning of Love (OM259)

* Love is different from attraction
* Love is different from fidelity
* Love is a great band starring Arthur Lee
* Love is a subjection emotion and John is not sure he has whatever internal thing that is happening in our head. 

John spent his whole life trying to figure out what Love is. It has only brought him pain. How is it different from uncomfortable obsession? John has love for his daughter. The first flush of romanic love is some terrible brain disease, like toxoplasmosis, which makes you think you want cats, but you don’t.

John just barely survived it multiple times until he decided to excise it from his emotional makeup. As soon as he gets into a relationship he is in a state of constant panic because he is subjecting every relationship to the question: ”Is this better than not?”, which is the lowest possible bar, but it is never clear. That was true for many of Ken’s Junior High crushes where he was nervous all the time about her or him.

+ John being an introvert who prefers solitude (OM259)

John is an introvert and a loner. He doesn’t necessarily prefer other people to solitude, and the question: ”Is this better than solitude?” is not easy to answer. There is no better word for it than ”loner”, but it sounds too much like James Dean and like trying to sound cool. John also doesn’t consume media because he prefers his own imagination to the product of anyone else’s imagination. He likes human touch and is happy to be touched.

As soon as John gets into an Uber he thinks: ”Hey, I get to find out all about this guy’s childhood in Madagascar for as long as the drive across town!” He loves everybody, as long as he at the end of the ride can get out, have the tip paid automatically, go into the hotel, have the same experience with the hotel front desk clerk, and then go up to his room. He loves those brief, intense interactions, but that is hard to duplicate in marriage. Intense is hard enough, but when you add brief? Best case scenario is that you are going to see this person for 70 years.

+ John’s approach to relationships (OM259)

John is a confirmed bachelor. The relationship with his daughter is the first one he ever had that feels absolutely indispensable and she is the only thing that ever felt permanent. It is his only reference point. Everybody says that there might be people out there with whom he could feel that bond, and that he just hasn’t met Mrs. Right yet, but he feels like he meets her all the time: ”Hey, you seem great! Anyway: Bye!”

When John met his daughter he realized that he just can’t leave her in the car, but she was a fat little bug when he first met her and there was no reason he would have fallen in love with her except for chemicals. She was pretty cute and John didn’t want to leave her in the car, though. The person needs to give John enough of the brain euphoria to get over that initial hump of ”I should leave her in the car!”, but that is how all his relationships end: He just gets out of the car, or she does.

John should take the bull by the horn and get into an arranged marriage. His mom is still alive, it is not too late! He thinks about it all the time: If only he had just been betrothed to the girl down the street at 16 years old! He liked her just fine and they would probably still be pretty happy. The idea of: ”Here is my wife, I guess!” is the same as: ”Here is my new-born daughter! Better make the best of it!” For a lot of John’s friends who have long-lasting and successful relationships, the story of how they met and how they got married is in a lot of cases: ”Here is my wife, I guess!”

+ How Ken met his wife (OM259)

Ken and and his wife went to see the movie High Fidelity with John Cusack. She described the experience as waking up with a gasp and a startle, going: ”OMG, I am going to marry Ken Jennings!”, a certainty that was accompanied with a lot of surprise and maybe horror, but who wouldn’t be if they had to marry Ken? She is not wrong!

A lot of John’s friends in relationships that John really likes say that they didn’t really passionately fall in love so much as that it seemed right. Those are the good relationships because the brain sickness subsides. If you are fighting and then are having great make-up relations it is a little volatile, but if it turns out it is someone inoffensive that you can hang out with, those relationships survive.

John always put an artificial choke on how romantic he felt about the women he really liked as friends. His best friend has always been a woman, there was lots of paling around and high five and going on adventures and stuff, but he would always resist and tell her they shouldn’t kiss because it will ruin their friendship. 

He was very frustrating to a lot of people over the course of his life. Not that he should have kissed all of his friends, but he should have kissed one of them a long time ago and then said: ”All right, are we in this? Let’s go!” Instead he always just said: ”Can I borrow $30?” He should have just married his High School sweetheart, but she is a pain in the ass. He should have kissed the low-maintenance person he was already going on hikes with. She was great!

When John was in his 20s, a dispute over art could end his relationship, like: ”That experimental theater piece was great!” - ”No, it was trash!” - ”I can’t live with you anymore!”

+ Have a banana! (OM259)

John’s dad used to say: ”Have a banana!” and it became a thing that John and his sister would say to each other. His solution to every problem was to have a banana. It was a simple and nutritious way to get on down the road. When they would be getting ready to leave, he would say: ”Have a banana!” John’s sister started to say it to John ironically and he would reply: ”You have a banana!” and later he started to say it to his kid. They do eat bananas, but they say: ”Have a banana!” an awful lot more than they actually have bananas. It is a signifier for: ”Go to the bathroom and grab a snack!”, but also a way of saying: ”Fuck off!”

+ John playing his songs differently live than on the album (OM259)

John has gotten letters, tweets, and emails from people who saw his show and complained that he didn’t play the songs anything like on the album. He has been to concerts himself where he objected to the band reinterpreting their hit, he went to see Dylan a few years ago and it was impossible to even discover the songs. Dylan will intentionally mess up the cadence so that people can’t sing along, play it a completely different way, or leave out whole verses, and John felt assaulted by that. He watched the first 20 minutes from back in the audience and found it terrible, but then he moved up in the crowd and as he got closer to the stage he was mesmerized by the show, not just that the show was evolving, but by being closer to what was happening he got it more and more.

Ken’s most memorable live experiences were about seeing songs reinvented, as long as it doesn’t just mean: ”Hey, what if it turns into a 9 minute guitar jam?” Ken doesn’t mind changing the vernacular of it. Paul McCartney will do Here Comes The Sun on ukulele every night and that is cute.

John reinvents his songs unconsciously and out of necessity. Part of it is that he finds a better way to play the song because on your record it is how you did it after you played it 15 times, but after you played it 150 times you discover what you should have done. Ken thinks that it is just the artist thinking they find a better way. The Rolling Stones were doing the definitive versions of those songs in their 20s, and even if they would do Satisfaction differently now that doesn’t mean it wasn’t perfect.

A big question about art is: Is this your job or is it a spiritual religious practice? If it is your job, then you show up and do the thing that pays, but in the first 40 years of John’s life the accusation of selling out was the worst thing you could throw at an artist, and all kinds of artists thwarted their success in order to avoid even the appearance of having sold out.


















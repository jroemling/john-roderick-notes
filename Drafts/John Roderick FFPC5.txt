FFPC5 - Edge of Tomorrow
2019-09-01

//Intro by Ben Harrison//

You wake up, eat breakfast, have a cup of coffee, brush your teeth. You drive your kids to school, head to work, eat your lunch at your desk, head home, check your kid's homework, eat dinner, watch an episode or something, take a shower, brush your teeth, go to bed. Rinse. Repeat. Our lives are often profoundly routine. If it weren't for the slow changes of the seasons and the punctuation of occasional holidays we might lose our place in time. 

We make decisions every day but very few of them are momentous enough to truly change the course of our lives, much less history, and yet it can feel like we are doing the same thing over and over again. What small adjustments can we make to change everything? The film we are watching today interrogates this question to great effect. It is a weird bouillabaisse of movie ideas, swapping Xenomorph for Germans on the beach of Normandy and applying a Groundhog Day time-travel mechanic to D-Day, but that question is so compelling and the execution in this movie is so good that it somehow works.

Tom Cruise, our hero in the film and Adam's hero in real life, is a major in the US Army of the future. He is their PR man, selling humanity on a war that doesn't seem to be a matter of choice. Aliens have invaded Europe and are an existential threat to the species. Tom Cruise is a coward and when faced with the prospect of heading to the front lines to make a PR film he tries to escape and he is busted down to private for his trouble.

Now he will have to dawn the exosuit of the future military and head into the wood-chipper. He dies very early in the film, but is soaked in the blood of a particular alien in his death and this gives him the power of resurrection. With this power his character, who was previously so bad at fighting, is suddenly the only person capable of finding a way to defeat the aliens by living the day over and over again, resetting whenever he dies and making small adjustments each time in order to figured out how to brute-force the chess match that humanity is playing with these aliens. 

We should probably have put this film on the main list. It is a war film, not a polemic on the horrors of war or precisely a celebration of it, but war is the setting and the subject of the film. Because it is in the future, the meaning of this war is a little trickier to tease out, but I think we came up with some compelling film-papers in this episode and you are really going to enjoy it.

"Battle is the great redeemer. It is the fiery crucible in which true heroes are forged. The one place where all men truly share the same rank, regardless of what kind of parasitic scum they were going in." Today on Friendly Fire: The Edge of Tomorrow or: Live, Die, Repeat! Not sure which is the title and which is the tag line at this point. Whatever! Just listen to the episode!
RL188 - Crow Meth
2016-02-08

This week, Merlin and John talk about: 
* John spilling coffee on himself ([[[Currents]]])
* John’s extra-pants at the office ([[[Style]]])
* Merlin fixing a T-shirt with a Sharpie ([[[Merlin Mann]]])
* John’s leather jacket with white-out highlights ([[[Objects]]])
* eBay store follow-up ([[[Objects]]])
* Laptop upgrade ([[[Technology]]])
* Captn Mariam ([[[Friends]]])
* John is of Welsh descent ([[[Early Days]]])
* Hankies from Poland ([[[Objects]]])
* Regretsy ([[[Objects]]])
* Pavement, Zooey Deschanel, Katie Perry, Special Victims Unit ([[[Movies]]])
* Crows ([[[Pets]]])
* John not listening to his podcasts ([[[Podcasting]]])
* Panic while snorkeling and the giant tortoise connecting with John ([[[Stories]]])

**The problem:** John is a panicky snorkeler, referring to his experience in Hawaii where he panicked while he was snorkeling and connected to a giant tortoise that swam with him and calmed him down.

The show title refers to crows hiding their stuff like meth heads and if you watch them, they will never feel safe again.

They talk about the pronunciation of John Siracusa’s name who makes a distinction between how the city is pronounced and how his name is pronounced, although he is Italian-American, but he is also John Siracusa. 

+ John spilling coffee on himself (RL188)

John is starting off with a little bit of a complaint. He thought he had come up with a life hack: He really loves his coffee mug, but it is bad to use the same coffee cup over and over without really washing it. There were 1000 disposable white cups In the pantry that people bring to parties and stuff, which is not eco-friendly, but these cups were already made and they were headed to the landfill anyway. John put them in the coffee machine, but they weren't quite big enough for a long shot, so the coffee was pouring over the side a little bit. When he reached for it to pick up his coffee, he noticed the cups were not made to handle hot liquids either, but they were too flimsy and he spilled hot coffee on his pants and on his hands. 

+ John’s extra-pants at the office (RL188)

John does keep extra pants at the office, but he doesn’t know if they are right for this weather. They are some very old, very cool, heavy wool hunting pants, some Last of the Mohican pants. You could down the Saint Lawrence river in a birch bark canoe and at least your legs wouldn’t be cold. Those are the pants of James Fenimore Cooper who wouldn’t have had access to these pants, but who was writing about them. He was wearing Deer Suede Pants instead. John's pants are the ones the people of the 1940s built to not have to wear deerstalker pants. The 1990s are going to make the 1960s look like the 1950s!

+ Merlin fixing a T-shirt with a Sharpie (RL188)

Merlin is out of the life hack racket at this point, but he killed it last night! He dresses the same pretty much every day and one of the staples of his wardrobe is just under half a dozen American Apparel long-sleeve T-shirts. Between baking grease and bleach he ends up ruining a lot of shirts, because you need very little exposure to bleach to ruin a T-shirt. One he really liked had an orange-white bleach stain the size of a pin right on the abdomen, just big enough to be noticeable and to make you look like you are wearing a shirt with bleach on it. Merlin did not speckle the whole shirt to make it look like An American Apparel Bleach Leopard, that would be boiling the ocean, but he fixed it with a Sharpie. It is a beautiful life hack!

+ John’s leather jacket with white-out highlights (RL188)

//John mentioned this jacket in RL184 when he was talking about opening an eBay-shop)// 

John has a leather jacket that has been around since the 1980s. At a certain point in the early 1990s he thought it would be better if that jacket had some white accents and he put white-out all over it and down all the seams like military tunic highlights, like the blazer of the guy in The Prisoner, except John did it with white-out on a leather jacket. He really liked it because it really popped and he wore it to some Soundgarden shows in bars back in the old days. As the years went, the white-out kind of crumbled and he discovered white ink pens that are like a can of paint in the shape of a pen. Some of them have a little pearlescence to them and you push down on the big nib to use them. It works really well and John went over the jacket with this paint pen. It was really popping!

At a certain point in the 2000s, long past the point where he should have been wearing a leather jacket that was like a motorcycle jacket full of flair, he took a sharpie and went back over the liquid paper in select ways to tone it down a little bit, almost like a cover-up tattoo. The sharpie more or less returned the jacket to its original leather color and John might actually put it on his eBay-store because it is a little bit too big for him. He used to wear it with a sweater, even though that is not the canonical motorcycle jacket vibe. When you are big, it is hard to find things that fit you well and when you do find stuff, you will get the biggest, because you are so used to things that don’t fit. It is great for layering, but when John is wearing a motorcycle jacket out now, he just looks like a dad in a motorcycle jacket.

+ eBay store follow-up (RL188)

Far be it from John to come on this program and talk shit about eBay. He recently put a lot of work into getting some of his stuff up there, probably 20 items complete with 7 pictures with long descriptions and he filled in all their categories. Then John didn’t just want to launch it, but let it stew a little bit and think about it, because once you are live it is an auction and you can’t take it back. He didn’t want to let the rabbits out of the gate in the big rabbit race just yet and so he sat on it for a while. 

It said right there very casually that they keep these for 30 days, but it could be a Skype thing where your money has gone dormant, but that doesn’t mean anything, because you can push a button and your money comes back. It must be some tax thing to get it off the books. This is why Merlin doesn’t like banks, because the whole idea of money going dormant is very troubling to him. It wasn’t money if it can go dormant because money is fake. Skype has conned John into putting in like $50 because he wants to call people on their phones which he has to pay money for and then they want you to go over here where your other money was. Merlin still has $10 in Google Voice and he has no idea how to spend it. 

Anyway, John did all this work to get his eBay store ready, but he also wanted some kind of fanfare when he launched it, maybe hire a band and get some Rip Taylor confetti. He finally went back to his eBay store a couple of days ago, he still wasn’t ready to launch it, but he wanted to look at it again and get it more ready, because the eBay store will tell John when it is ready. As soon as he launches it, he wants everybody who is interested in it sitting out there with their virtual catcher’s mitts, ready to receive the pitch with their hopefully non-dormant money, ready to bid on the things, but when he went there all the words were still there, but all the pictures had turned into those little Polaroid-looking placeholders. He was searching all around, but they were gone!

After 30 days, eBay inexplicably decided for no good reason, except that they don’t want to use their memory because they are probably in the cloud, to not make the pictures dormant, but they are gone and now John has to put them all back in! Merlin suggests that Etsy might be more the place John is looking for. John has thought about it and he might even have been contacted by somebody from Etsy who listens to this program, but the thing is that you have to decide what you want to get for your thing and John doesn’t want to tell anybody that this item is worth $100.

Merlin suggests that John needs at least one marquee item, like Elizabeth Taylor’s panties, which is the real fish food, the wowzer that gets people to go like ”OMG! I have been hearing about this thing forever!” There might be 5 people out there who would actually pay a premium for that. Then you would have a handful of medium-prized things in the $50-100 range, and then maybe something John has a lot of, like tie tacks or kazoos. John should make little packages with a tie tack, a kazoo and something else together, but make it AQ, ”as available”. The key is to find out what kind of stuff is selling and what stuff is the biggest pain in the ass. You launch with something like John’s jacket with cover-up tattoos and people will pay $500 because John talked about it on the program. He should sell his bathrobe together with the cimetar. 

There might be some Paul Allen type out there who is just waiting to finish their collection with John’s jacket. They might mount it in a frame and put it on the wall of their Rumpus Room, it could be their Cardiff Giant! They could also put it on a creepy doll or they could put the bathrobe and the cimetar on a dummy. Fish food! John has explored Etsy quite a bit and he almost pulled the trigger on some $60 pocket squares before he realized that pocket squares are just hankies that you can get 20 for $1.50, so John stopped believing in the whole pocket square racket.

John had a big argument with his mom about fulfillment of his eBay store. He explained that he wants to take pictures of his things and box them up with a little post-it note about what is inside. When it is time to fulfill them, he just needs to print the label and off you go! It felt like a life hack! She just wanted him to hang everything on this clothes-hanger rack, but then he would have another thing to do once the auction is up. 

She just casually dismissed his life hack and was not acknowledging what was so great about it, but she was just saying the same thing over and over again. Mike Tyson said ”Everybody has a plan until they get punched in the face” Merlin likes John’s plan a lot, but there is an extremely good chance that there are going to be good-hearted people who want more than one item and it might be frustrating to them to pay the full freight on two separate boxes that could easily be combined into one. Someone might also ask a question on eBay, which is a feature now. 

John doesn’t even want to talk about eBay anymore because he got so frustrated with them and he doesn’t want to give them more publicity. He should instead be directing people to Etsy where they can probably get a toast carving by Capt Mariam. 

Merlin used eBay once in 1998 to buy a Weezer shirt and a remake of a vintage Pavement shirt, the green one with the weird device in yellow on it. 

British people are suspicious of anyone who doesn’t hate themselves, which is also what nobody in Europe likes about Americans, because they don’t hate themselves enough! They all got Kierkegaard in their water and we are here with our Can-Do-spirit and Run-Rabbit-Run, opening an eBay store selling their used clothes. ”USA America Pow Pow Pow Bang Bang!” and people in Europe just roll their eyes.

+ Laptop upgrade (RL188)

John has a pretty new laptop. He bought it after everything got stolen in the burglary maybe 9 months ago. He didn’t get the Air or the one with only one port. John is using Chrome and a message popped up that his Adobe Flash plugin isn’t current, so he went around trying to update his Adobe and he came to a place where it said that if he updated his Chrome, it will automatically update his Adobe. As he was monkeying around it asked if he would like to update his OS to El Capitan, which is usually a mixed bag of fruits and nuts. It was a very new computer, so John felt like if that 7,5 month old computer can’t handle it, it would be a betrayal. 

John is not going to update his desktop computer in his office. He had an old white laptop for a while, but somebody stole that from him in Chile. It was the one they bought when they took the photo of them on the show art that day. It was such a beautiful computer and they really loved it here at the Roderick group, but John took it down to Chile where he was using it in a bus station, somebody saw him use it, he put it in a bag and told his partner to watch the bags, she said ”Of course!”, he went to the bathroom, and when he came back it was gone. She was watching the bags! Make sure to pull over the RV before we come to the mountains //(reference to RL183, see [[[GMC RV]]])//, but that was a different partner. She was probably playing Solitaire or something. 

In his office John is using an iMac with a 1.6 GHz Intel Core 2 Duo and he is not going to put El Capitan on this. It has 4 MB L2 Cache, 2 GB Memory, and a Radeon X1600 with 128 MB VRAM. John wanted to see his eBay-pictures and he wanted to make sure the reason he couldn't see them was not that Adobe had decided he needed a new thing, which they seem to do every week and a half. He had to update Chrome because God Knows! John doesn’t want to have the wrong Chromes and he went the whole hog! 

The computer immediately said it needed an hour to think about this. He installed El Capitan, he got a new Chrome which came with a new Adobe, everything checked out, and he restarted it multiple times just to make sure that everything had settled in. You have to shake it a little bit so all the stuff settles to the bottom, but the pictures were still gone. It wasn’t Adobe. Who knows what Flash does! Every time he upgrades it, all it does is make the intrusive advertisements happier. 

+ Captn Mariam (RL188)

They are recording this episode a little early and Merlin wanted to mention it because otherwise somebody like Captn Mariam will go and check if he got the weather right. She wouldn't even have to check, because she has it all in her mind, which is pretty wild stuff! She has cataloged everything and she will need 20 seconds to access data that is in a salt mine in Utah to pull up some stats that are pretty precise. She is both a scientitian with a laboratory and an artist, look at [https://www.flickr.com/photos/mariamelnaggar/25413449914/ what she has done] with an etch-a-sketch! Before John even realized she was a scientitionist he knew her primarily as an artist who made fantastic art on burned toast. 

Mariam sent Merlin a Wilberforce and John should get that one, although he doesn’t even know what he would do with it. He could put it next to his Headphoney! Do people stroke it with their thumb like Worry Beads? Here is what John wants: ”What I want is a guarantee! No more attempts on my father’s life!” - ”You get it all wrong, kid! I’m the hunted one!” - ”Tattaglia is a pimp, he never could have out-fought Santino! But I didn’t know until this day that it was Brazini all along” - Hey, Tom, for old times sale. Can you give me off? Sorry, Sally!” - ”You can’t do that, it screws up all my arrangements” - ”Do you renounce, evil?” //(reference to the movie The Godfather)//

+ John is of Welsh descent (RL188)

John was on an airplane the other day, telling the woman next to him that his people are from Llantrisant, just outside of Cardiff. She was from Cardiff but didn’t know what John was saying at first, because it is pronounced Llantrísant. They went back and forth a couple of times and like with the French, you will never pronounce anything right. John just wanted to throw his Dasani Water at her. 

When John mentioned Wales last time, Merlin looked up the Cardiff Giant, which he remembered doing an SRA card on in 4th grade. Is it some kind of Gollum made out of clay that avenges the Jews, like Sammy David Junior? It was a great hoax in the 19th century where somebody in Cardiff found what first appeared to be some kind of body of a 10 feet (3 m) tall guy with a giant cock, but it was later revealed to be a hoax. They continue to talk about the Cardiff Giant for a while. It was eventually sold to the farmer’s museum in Cooperstown, NY where it is still on display.

+ Hankies from Poland (RL188)

John went on eBay the other day and he ordered 25 vintage hankies from Poland. They cost $2, they were things of utmost beauty and John now uses them as pocket squares. He challenges anybody to a) find a reason why they are not also effective as pocket squares and b) they are from Poland and c) Fuck you! c is always fuck you in whatever list John is making. a is the main thing, but Merlin always saves the main point for b. 

+ Regretsy (RL188)

There was a site called Regretsy with the description ”Did you make that with your feet?” //(the site is closed, but there is [http://missingregretsy.tumblr.com/ Missing Regretsy] and there is [https://books.google.com/books/about/Regretsy.html?id=v1BKjlqZ2GQC a book])//. It was showing regretful things you could find on Etsy. John doesn’t understand that there are still webpages for auto part stores where a little 8-bit man is down at the bottom waving his hand and there are some kind of balloons. Why are those still up when everything is now in primary colors, but Regretsy is gone? It is a known issue and a big problem! Somewhere on the internet it certainly still exists because the NSA has got to have it to cross-reference things.

John is looking at Regretsy and there is a knitted glove meant for two lovers to hold hands in //([http://missingregretsy.tumblr.com/post/68532393406/couples-glove-lovers-glove-for-him-and-her see here])//, which is very creepy, like the movie where Robert Duvall had to have sex with his wife through a sheet. If you have to do that because you are a member of a patriarchic religious cult, then you would also want a hand-knitted glove to invisibly hold hands. Merlin thinks it feels German because of the forced direction of intimate body parts and because it is cozy. 

There is a [http://missingregretsy.tumblr.com/post/68139400869/chocolate-turkey-on-a-pretzel-rod-1200-turkey Chocolate Turkey on a Pretzel Rod] and a cozy for a turtle that [http://missingregretsy.tumblr.com/post/66652015782/roasted-turkey-tortoise-cozy-2100-drumstick makes the turtle look] like a roast turkey. 

+ Pavement, Zooey Deschanel, Katie Perry, Special Victims Unit (RL188)

John does love Pavement's ”We are underused!”, but how do they get away with singing like that? You //can// sing like a normal person! It is all amazing wonderful music and the fact the singer sings like that is a major advantage to the music, but John cannot understand how he does it. They talk about Robbie Williams who was in a boyband called Take That before. He is the Justin Timberlake of England, but he was snarky and funny like Russell Brand, the comedian who used to be fat, but now is a post-drug-addict who is smart and goes on American talk-shows to make people feel not smart enough. He remade Caught Between the Moon and New York City but it wasn’t any good and now he looks like Weird Al! He was with Katie Perry once //(Merlin says Katie Holmes first)// 

Katie Holmes is the one who married the scientologist, while Katie Perry is the one who married Mycroft Holmes //(Russel Brand))// and who would look like Zooey Deschanel if you turned the volume up on all her features two notches. John went to a Mariners game with Zooey once and when some drunk guy seemed to know who she was, she just said that he probably mixed her up with Katie Perry. The song about the girl kissing the girl! When you put them side by side, like with John and [https://www.cityartsmagazine.com/josh-rosenfeld-barsuk-records/ Josh Rosenfeld], people will ask if they are brothers. 

Josh’s people are from Poland back to the Städl days and John is from Kelt from all the way over on the seaside. They both look very much like the places they are from. Zooey Deschanel is the Skipper to Katie Perry’s Barbie. Katie Perry dating Russel Brand seemed crazy and didn’t work. Celebrities feel more comfortable with each other because they can check into hotel rooms as Zooey and Franny. Zooey Deschanel's sister Emelie is in the good Spiderman movie and in a film called Bones.

John was doing a show at the SF Sketchfest 2016 and he was backstage at a theater party (John talked like a cross feed between Sarah Palin and Yoda, but he is from Alaska). Several of the actresses he was talking to had at one time or another all played some kind of forensic detective on a television show. Adam Savage was there, talking about how Mythbusters just ended and these women were all commiserating with him, saying they know how it feels when your long-time television show is being cancelled. It is like losing a child and Adam said it hurt so much more than he would have thought. Emelie Deschanel also played a pretty science lady who works in a cold room full of cadavers and who every once in a while pulls up some implausible thing on a computer screen that furthers the narrative of the show.

When Merlin’s wife got pregnant, they had to stop watching a lot of TV shows about crime and suffering. Up to 2007 they used to watch Law & Order Special Victims Unit. Merlin had been a big Law & Order fan for 20 years and used to watch it all the time, but Special Victims Unit is horrible, because it is all about innocent people being sexually exploited. It is a Victims Unit that is Special, but a Unit for Special Victims would also be very interesting. 

+ Crows (RL188)

Merlin was listening to a couple of old episodes recently and noticed that they packed a lot of stuff in at the beginning. There was a lot of crow talk where John first introduced Merlin to hydrogen peroxide //(probably RL36)// and when John first told Merlin about Crows //(probably RL38)// John has become a place for people to send things about crows, which technically makes him a post office box. The presumption is that there is stuff about crows happening on the Internet that John hasn’t already seen. If it is about crows and it is on the Internet, John has seen it! 

There is that one with the crow that brings gifts to the little girl, but John has already seen that. Most recently there is a thing about crows being paranoid. If you know where their shit is, they behave differently than if you don’t know where their shit is, kind of like a meth user. If they put crow meth under a bushel and you were watching them, they never feel safe again. If they are doing that with their meth, that means they are doing that all the time. The message is that they are tripping on you all the time, which is what John has been saying for years: When you walk out of the house, the crows are watching you, they are marking what you are doing, they are judging you, and they are deciding about you.

Somebody who thinks he has offended the crows emailed John the other day and asked what to do. He had disrespected them by throwing a McDonalds bag at them and now he feels like they are watching him all the time. You can’t apologize because they won’t accept it, but you can very patiently start to say ”Hello crows! I see you, crows!” and slowly walk your way back. One day they aren’t anymore all going to come down your chimney or whatever they had in store for you. John got a Google Alert and he has seen the crow! 

In the episode Merlin mentioned John got in the car with his mom to find out where the crows go. They are very protective about where they live, but if you find where they hide their meth, you can triangulate where they live and use their meth. Think what that is going to do to a crow! They will pass that on from generation to generation, it is the selfish gene. Crows have [[[spooky action at a distance]]]! There are crows in Pasadena who know not to eat a Cheeto because somebody back in Alaska had a basket and chased them around with a mask on, throwing Cheetos out.

+ John not listening to his podcasts (RL188)

John likes the fact that Merlin goes back to the archives, because there is a tremendous amount of information there, but he has never listened to their program. For a long time he was hoping that people would transcribe them, because he does like reading, which Merlin finds so strange. It is like somebody sending you a transcript of sexual intercourse. You might get all the facts right, but you are missing the feeling! Merlin has read a lot of transcripts of sexual intercourse. John likes a transcript because he likes to go through and figure out all the ways in which the transcriber has misread or misjudged some strange little reference Merlin was making followed by the strange pivot that John was making off it. Then Merlin takes the pivot and runs it in for a touch down, except he gets tackled by some other football player that John has put on the field. 

When you make a dish where people can see all the ingredients on the table, after you cook it and eat it people might be able to identify all the ingredients. But in case of this podcast, they are eating a meal where they still haven’t found the salt. It might be in there, but they don’t read it as salt and it is almost like they are eating a different dish because they can’t know what all the ingredients are, like some kind of Szechuan Battery Acid //(reference to RL182)//. 

John reading transcripts of this show is like religiously reading movie reviews for movies he will never see. John has read reviews of movies he would not see if you paid him $100, but he loves reading the reviews of them. They continue to talk about Forest Whitaker, the movie Battleship Earth and about Ice T, who was the most hated and most feared man in America because of his song Copkiller, but he stared them all down.

+ Panic while snorkeling and the giant tortoise connecting with John (RL188)

John recently had a very hippie experience when he was going snorkeling on Maui in Hawaii a little bit South of Kihei. He has snorkeled quite a bit in the past and he likes to snorkel. It is wonderful because it makes you feel like you are flying in another world, like in that movie where the big blue people fly around and you can inhabit a blue person //(Avatar)//. John loves to be in the water because fish will come up to him and look at him and being in their world is extraordinary. 

However, John prefers very calm seas and doesn’t like to snorkel in rough water because he is a little bit of a panicky snorkeler. It is a foreign environment and he is very accustomed to breathing air. He likes to breathe air and he doesn’t like to breathe water. Every time he puts a snorkel on he has to put his head under the water he will feel some panic, back off, do it again, feel less panic, pull his head out, make some jocular comment to no-one in particular and then put his face back down into the water, regulate his breathing, get calm and the start to fly. 

John has been snorkeling in rough water several times where water was coming down his hose and he was bouncing up and down, unable to get his breathing straight and unable to overcome the panic. As he snorkeled on this reef in Hawaii, he was way way out and he panicked. He hadn’t yet regulated his panic and his panic started to rise. He was not scuba diving and he was not going to die, but the panic compounded upon itself until John was freaking out. 

John popped his head out of the water because he was hyperventilating through a tube, which is not the way to hyperventilate. He realized that he was a long way from shore and he needed to go back because this was a bad situation. He started to swim back to shore, but he didn’t want to put his head back into the water or into the tube and so he was paddling like a dog. Although he had flippers on, the tide made it appear that he wasn’t making any progress and that he was actually pulled out by the flood. 

Now John was really starting to panic, because he was tired, flailing, hyperventilating, panicking, swimming and getting scared all at the same time. He rolled over on his back to float along and regulate his breathing, but he just couldn’t get calm. It was a high sea and the waves were crushing, except that he was in a boogie boat. He barely made it back to the heaving shore and all the people there on the beach trying to have a nice time with their kid were suddenly dealing with this ectoplasm that washed up on shore, some dead whale that was still wheezing a little bit. John was scaring all the kids! It was awful! Not only was he panicking and hyperventilating and felt like he almost drowned, but he was also covered in shame. This is a simple little Wailea snorkel-beach, but daddy couldn’t get his shit together and daddy had a panic attack. 

The next day John was mad at the ocean and mad at himself. He couldn’t go back into the real world with this experience hanging over his head, because he likes to snorkel and he needed to go back on this horse. He went down to a different part of the beach, put his snorkel back on, put his head in the water 3-4 times and got trapped in some adjust-my-face-mask-OCD for 20 minutes because he didn’t want to put his head into the water. When he did he was not in a calm space and he was breathing a little bit too heavy, but he was also not panicking. 

John was a hard on himself to go through this and he got out to the reef, looking around. He saw some reef stuff, some coral and some star fish and some fish that came to look at him. He did it and he was fine, although he was still breathing heavy and not feeling comfortable out there. At least he got back on the horse and permitted himself back to the beach. He swam out parallel to the beach where there was nothing to see and he was done looking at stuff, but he just wanted to keep his breathing calm and get back to the beach. 

Out of the gloaming there was a swimmer coming towards him, which was calming, because it meant that there was another person who was surviving this experience. As it turned out it was a giant tortoise, as big as John, 6 foot (1.8m) in circumference, swimming with his tortoise wings slowly and chill. He looked at John and John's breathing instantly went very calm. He could even hear the whale song that he couldn’t hear before because he had been breathing so hard. The tortoise moved in super-slo-mo, turned a little bit and came straight at John head-on. 

As he came closer, he flew directly underneath John only 6 inches (15 cm) away, which was by choice because there was this whole ocean out there. What was his plan? Was he going to nibble on John’s peanuts? He was sending John some turtle love or something and John was looking at this big shell go underneath him like he would be looking at the opening of Star Wars where they are talking about the Empire being in a trade dispute. Then John flipped around and got up along side of him. John’s kicking and floating matched his. 

They were swimming side by side, both flapping their wings in sync and looking at each other and both being fine with this, because they were both grooving. They swam all the way back across the beach together! John was taking about one breath a minute at this point and all panic he ever felt, even about his 7th grade math tests was gone! 

The tortoise flew John back //into// the reef, over it and into the canyons until they met another tortoise, the mama tortoise that was smaller than John. He handed John off to her and John continued to fly with the mama tortoise. After a while John bid them adieu because they had their own lives to lead and he headed back off the reef and back out across the sand to get parallel to the beach so he could turn and go into the beach. It was not going to get any better than that! That was some heavy-ass tortoise connection where the tortoise noticed that somebody was freaking out and did an intervention. 

As John was swimming back along the beach, thinking about what happened, listening to the whale song, out of the gloaming comes another giant tortoise at the same place John met the previous tortoise, also flying underneath him, basically asking for a trip back to the reef, and John accepted, turned around, and both of them were floating together all the way across the reef, taking about a breath an hour. All was right with the world, those tortoises were doing some heavy lifting with him and the second one actually appeared to be syncing up its movements with John.

At this point John had been out there for more than an hour and as he finally got back to the beach he was in a state of total hippiness, like ”Wow, man!” He went to the grocery store that afternoon to get some food and saw some cheese-ball tortoise candy dish carved out of wood on sale, some real tourist crapola that he sees at thrift stores all the time for $0.99 that he never buys, but it looked just like his tortoise and he had to have it. John was such a hippie that he was buying tortoise crapola because he wanted to stay connected to this guy. Now it is sitting on his piano full of keys and he is looking at it every day and says ”What a piece of crap that is, but it looks like my guy, my spirit tortoise!” Now John doesn’t know anything. There is pre-tortoise and there is post-tortoise!

Those turtles were old and they have seen every kind of thing. They had watched [https://en.wikipedia.org/wiki/Tears_in_rain_monologue attack ships burn off the shoulder of Orion]. They are amazing-looking, like leopard-spotty. John had expected that they would be slow on land and move fast in the ocean, but they are not! They are flying in super-slo-mo, which is what is amazing about snorkeling: You are flying in slow motion! 

John could easily keep pace with them. They must have heard some huffing and puffing over the whale song coming from a panicky snorkeler. Now John feels like he is going to buy a Puka Shell necklace and carry a yoga mat with him. Everything is right with the world and all he needs is some positivity and some Eckhart Tolle and he will be back in the game.

Most of John’s usual experience with wildlife is about crows watching him, being in the woods, ringing a bell a yelling ”Ho, bear!”, or a possum in his walls. John’s mom was not with him in Hawaii and his daughter was at an age where she only believed in things she could see. Her mother is an avid snorkeler she was very supportive and sympathetic and after John’s panic attack, like ”I’m sorry you had a panic attack! I’ve scuba dived in Sumatra, but you had a panic attack off of Waimea, the gentlest of all places.” She was very sympathetic, but not really. 

When John came back and talked about the tortoises, she said that this was fairly unusual and that she never had that experience. John’s tortoise was taking him into his world, so who has scuba-dived off of the coast of Sumatra now? John doesn’t know anything anymore, all of his cynicism is gone, he is breathing once an hour and he is still thinking about the turtle, because it is sitting on his piano full of keys. What is a brother supposed to do now? Seeking out these kind of things is the whole eat, pray, love industry of people seeking transformative experiences, which John does not want to get into! It is similar to substance abuse, like chasing the dragon, and it is a little bit of an [[[subscriptions are eels |eel]]]. John doesn’t want to swim with dolphins, except if a dolphin would come to him. 

















RL297 - Chicken in the House
2018-07-16

This week, Merlin and John talk about: 
* Life-hack channels on YouTube ([[[Merlin Mann]]])
* John having had sleep apnea ([[[Sleep]]])
* Ads following you around on the Internet ([[[Internet and Social Media]]])
* Pink Floyd documentaries ([[[Music]]])
* Merlin's Battersea station story ([[[Merlin Mann]]])
* Nike missile bases ([[[Military]]])
* Newspapers behind a paywall ([[[Internet and Social Media]]])
* John almost buying a Corvette, fitting in small cars ([[[Cars]]])
* Hitchhiking in England ([[[Cars]]])
* Johnny Marr ([[[Music]]])
* Merlin's book about anxiety, not being able to sleep ([[[Sleep]]])
* John getting an offer for a long talk ([[[Shows and Events]]])
* John being anxious about turning 50 years old ([[[Aging]]])
* Riding roller coasters ([[[Dreams and Fantasies]]])

**The problem:** No Plae, referring to a brand of children's shoes called Plae that are not available for adults.

The show title refers to John being bandaged by a woman in England who had chickens in her house.

It is early and John suggests he will sleep through the first 45 minutes of the show. Merlin woke up around 3am and John around 4 am and couldn't get back to sleep. John had some 3 second loop of the Pretenders song Don't Get Me Wrong in his head and it just won't stop. They should get more sleep, because Merlin is so fucking tired. 

Merlin and his daughter watched Die Hard. Twice! Such a good movie!

John used to rewrite Dazed and Confused all the time and thought "This is amazing", but he did it again!  

+ Life-hack channels on YouTube (RL297)

Sometimes Merlin cannot breathe through his left nostril and he should probably get a Cpap, but that seems fairly undignified and there's got to be a better way! He watches a lot of YouTube videos about life hacks, like making your own bug catcher with a plunger and stuff like that. There is a sometimes funny YouTube channel that shows you how to make a bunch of dumb shit that you should probably just go buy it in a store. Maybe Merlin could make a homemade Cpap for himself with some straws, a bellows and a hamster on a wheel, because you can also make candy or your own dish soap at home. John suggests that a home tracheotomy would be better and he is curious about this plunging Flycatcher. 

Merlin's daughter loves these kinds of videos and every morning when Merlin brushes her hair she watches a thing called [https://www.youtube.com/channel/UC295-Dw_tDNtZXFeAPAW6Aw 5-Minute Crafts] that is this Russian farm of content about dumb stuff you can make with hot glue. Another one is called [https://www.youtube.com/channel/UCI4I6ldZ0jWe7vXpUVeVcpg Household Hacker] that has a series called [https://www.youtube.com/watch?v=SWhoAR9_Fqk&list=PLrIRP14xXUYCjNjsUHJADlKBtGfh4eA0G Overcomplicated Life Hacks], but there are also regular life hacks about how to make sticky stuff. Some of the videos are very funny: "Build a much worse mouse trap!", "Make a comb out of plastic forks!", and stuff like that. It is not super-well done, but it is fun and they get a laugh out of it.

+ John having had sleep apnea (RL297)

A long time ago when John was carrying a little bit of extra weight, he was seeing a gal who told him one morning that he had sleep apnea. It was kind of like when when his doctor said "You have bipolar disorder!", because she was not the first one to notice. Who doesn't? That is why he doesn't sleep on his back! John thought about needing to get a Cpap or breathe-right-strips, but it already starts with having to google Cpap which is going to screw up every Instagram ad he will see for the next two years. It is already enough just talking about it on this podcast!

+ Ads following you around on the Internet (RL297)

John was talking to a friend about a brand of children's shoes called Plae, which is some kind of Swedish Finnish brand. John wondered if they also made shoes for grown-ups and she went to Zappos, but could not find any. No Plae! John got home, opened his phone that he had forgotten it at home that day, went on Instagram and within a minute there was an ad for adult Plae shoes. 

John's mind is boggling at how they got to him! Merlin [https://www.gimletmedia.com/reply-all/109-facebook-spying listened to a podcast] that did a couple episodes on this topic of people suspecting their devices listening to them. The Deus Ex Machina explanation would be that this would be true, but it is actually way creepier than that because of the kind of demographics and chronographics and geographics that they use. It is fucked up! Matt Haughey once looked up a porch light (Poor Matt Haughey tried to open his garage door one time and is now their sink for everything that's fucked up) and he got porch light ads for months. 

Merlin hates it when you get email because you put something in your basket and it just keeps coming back. He just wanted to see if they could ship it at a reasonable price and they couldn't! Leave me alone! John spent a lot of time Googling Hentai porn, but he has never seen any ads for it. He was researching it because somebody dared him to see if it is real or not. It is probably still feeding into the big profile and it got their tentacles into John. When John was a child, there was a lot of Scheisse porn in his family and he just wanted to research it for a book he is writing. John is Matt Haughey's garage door opener.

+ Pink Floyd documentaries (RL297)

Merlin watched two documentaries about Pink Floyd this week. One of them was a kind of not very good BBC 1-hour history of Pink Floyd which is not nearly enough time. You could do five hours on 1973 to 1975 alone and they breezed right past most of the interesting stuff. If Merlin had to pick, he would pick "Wish you were here", it is a great record. They told the story about Syd Barrett coming in the studio and they all cried because they didn't recognize him at first. There is a photo of him in the studio in 1975 and he is very sad. 

One time John was in one of those bookstores that put Meg Ryan out of business and there was a big beautiful book with all the beautiful old pictures of beautiful old Pink Floyd from a time when they were beautiful and the world was beautiful. The book was the size of a manhole cover and it was expensive, a coffee table book that //was// the coffee table. You could just put it on four cinderblocks and it would make the room. 

It is a must have book, but John was on his way to a dentist appointment in Manhattan on the 4th of July and he couldn't buy it at the time. It was the largest book in the Rock book section and he told himself that next time he will be in here, he will just get it, but he has never seen it again. This book was not no chaff, there was no big pig outside of Battersea station, but this was all Bell Bottom denim. 

John Siracusa taught Merlin something important about Google: "Stop thinking! Just make finger bangs on the keyboard until a thing comes out!" You can search stuff like "Movie with blonde girl cheerleader kill" and you don't think it does it, but don't treat it like a computer! John asks Google full sentence questions like an old man. "How does the wrench fit onto the socket if it is raining and you don't have gloves?" and more often than not Google is right there with the answer. 

+ Merlin's Battersea station story (RL297)

At one time, within the era of the iPhone, Merlin was on a train in London. "Did you emerge in London rain, and she was waiting there. Swimming through apologies" (Berlin - The Metro) There was a tunnel if memory serves, and he thought to himself: "Ha! I know that album cover is like some old factory.", and as they emerged a few minutes later, "What the fuck?", there was [https://en.wikipedia.org/wiki/Battersea_Power_Station an upside down table]. Merlin went into the maps app and "Boom! Kapow!" It was actually the [https://en.wikipedia.org/wiki/Animals_(Pink_Floyd_album) Pink Floyd factory]. He saw it with his own two big eyes! 

+ Nike missile bases (RL297)

Last night John was scouring the U.S. records of former Nike missile base sites in the West. They are fascinating and of course there is a Wikipedia of former Nike missile sites out there, but it is an incomplete list. When John was growing up there was a Nike missile base outside of Kingston Washington right next to their house and it was still active pretty late in the game. A lot of them were decommissioned in the early 1960s, but this one went all the way into the 1970s. The first flight of the Boeing 747 was 10 days after John was born //(actually the first flight was on February 9th, 1968)// from Paine Field and it flew right over their house. 

John recently picked up one of those magazines for small neighborhoods, like the Ballard Times, that is just a bunch of ads for real estate agencies and fur places. There was a picture from Ye Olden Times, like 1950, of the excavation of the original supermarket before they put in the Food Way and over the back fence you could see the Nike missile base. John thought he knew every Nike missile base in the area and there was not supposed to be one there. He looked at the picture with his little pocket loupe, but he couldn't make out anything pro or con. 

John knows the area and he knows there are some weird old government buildings in that location that are repurposed into some kind of city landscaping spot where they park the driving lawnmowers. Some of this was starting to line up, but the Nike missile database said "No such thing!" John was really tripping because he needed to figure out what was going on here. So far: Internet! Google! He typed in like "Cheerleader kill!" and it came up nothing. "Small boobs! Small boobs!" 

+ Newspapers behind a paywall (RL297)

The Safari browser has some kind of secret search mode now. Merlin says that it is good for lots of things and sometimes you can use it to look at Wall Street Journal articles, which is such a pain in the ass! Merlin finds it very hard to believe that many people subscribe to the Wall Street Journal. Sometimes there is an article in there he wants to look at and sometimes he can get it, but sometimes he can't. You can try the incognito mode or you can follow a link from Google News and sometimes it will let you in.

People in John's timeline want him to check out some article all the time. Are those people subscribe to the Wall Street Journal? John doesn't think so! Maybe Peter Sagal does, but maybe not. There used to be a widely known secret password that journalists could use, but they closed that. On political Twitter many people link to the Wall Street Journal, but really? You subscribe to this? Maybe they just read the first two paragraphs and when it ghosts out they better forward it to everyone. 

A long time ago, following a link from the New York Times Twitter account to an article would let you read it, but then they closed that backdoor, too, and now John gets his 10 free articles a month before they ghost on him. There is probably somebody like Marco Arment yelling at them right now about paywalls and about not skipping ads, because he did that ad-blocker once, but then he felt it wasn't right. 

It is one thing to have an opinion or a thought, but it is another thing to have to deal with all the people who don't like your opinion. Merlin is speaking for himself. Is there a life hack involving a plunger that is going to walk us out of this? Marco is not John's proxy, he is not his stand-in for a garage door opener, but John wrote him right after that and said "Hey, I know you stopped giving that to people, but would you give it to me?"

+ John almost buying a Corvette, fitting in small cars (RL297)

One time John almost bought a 1970s T-top Corvette Stingray from Tiffany Arment's dad //(see story in RW112)//. Her dad tried to sell it and she was thinking of John. Of course John needed it! They went through what they call in the business a back and forth and all John had to do is get the funding through from the network. It would have been a license to print money! Given the dudes that you typically see driving Corvettes, John would fit into one, but he wouldn't fit into an Austin Healey and maybe not even in an old Porsche 911, which breaks his heart. 

Merlin's friend Jeff is an oak of a man at 6'6" (198 cm) and he is Dutch. Dutch are very tall, but strangely not good at basketball. It must be those wooden shoes! Jeff got himself a Mini Cooper and he fits in it just like a hand in a glove. He says it is amazing that this car can accommodate a large man. 

They found a post of person called Dan from Ridgefield Connecticut on a website, saying he is 6'3" (190 cm) and about 185 pounds (83 kg), which is a lot less than John, wondering if he can fit in an Austin Healey 3000 or if he should just scratch this one off the bucket list and move on. Howard from Rittman Ohio replied he is 6'2" (188 cm) and 285 (130 kg), which is bigger than John, and there are certain tricks he must use when entering and exiting a Healey. He does not fit well and it takes him a minute or two to enter, but once he is in, he is comfortable. 

This guy now owns a car that it takes him a minute or two to get into. He even moved the seat bracket and drilled holes in the new floor plan. The point is that you know you can get into an Austin Healey if you are prepared to be in a car that takes you several minutes to get into and if you re-drill the seat brackets. John is not going to do that and he has to cross that off his bucket list. People don't talk about how big men under-report their weight, similar to "I have a drink sometimes on the weekends. Just a beer every night, just to unwind." 

John doesn't just want to get in there because he moved the seating bolts on his Austin Healey, he wants to drive a car that is made to fit him, a grown man's car that shifts itself! Merlin and his wife thought they both like stick shifts and for 10 years they drove a shifter car in San Francisco, which is insane! John has a shifter car in Seattle and that is also insane. For a long time he wanted a motorcycle, but having a motorcycle in Seattle is stupid. If you live in Santa Barbara, San Diego or Bakersfield, you'd be stupid not to have a motorcycle. But Seattle? But John sure wants one! 

+ Hitchhiking in England (RL297)

A long time ago John was hitchhiking in England outside of Bath. He was on his way to Slough where he had a meeting with a guy at a paper company. A kid pulled over in an old 1960s Mini. "Come on! It is wonderful that you are stopping to pick me up, but this is going to be hilarious!" The kid replied "Get in, mate! Put another shrimp on the barbie! That is not a knife!" or whatever they say in England. You could have put an elephant in that car, it was enormous! 

They can get away with it because the bottom of the car is made out of particleboard and it is 7" (18 cm) off the ground, meaning the Flintstones had more of a car than a Mini. In order to honk the horn, you just grab a bird by the ass! It was as low as riding a skateboard, which is scary in modern traffic when there are Lorries. The kid asked if John wanted to be his roommate, but maybe not. 

England is a place where John should have lived, but he never did, although he has tried and has been there a bunch. They got all those different cultures and they have free glasses, called The British Health System. One time John was at a woman's house who bandaged him. She was a nut and had chickens in the house and she asked John for £5 for the bandages she got from National Health, it said it right on them. Her argument was that now she was out of bandages. You also got to pay a TV license there if you want your Doctor Who and your BBC. They used to have these enforcement trucks in the 1960s that would drive around with fake satellite dishes on them to detect if you hadn't paid your tax on the telly. John doesn't even have a TV. 

+ Johnny Marr (RL297)

Johnny Marr is really great and he is not that much older than John is. He could just be hanging out with him and "What's up?" You know him for stuff like "How soon is now?", but just fucking listen to "This charming man". How does he do it? Merlin never got this about him! They have talked about this before, but they did 297 episodes and there have to be some repeats in 297 episodes. Merlin is really fucking tired.

+ Merlin's book about anxiety, not being able to sleep (RL297)

When Merlin woke up around 3am, he took a shower and read in his really good book about anxiety called [https://www.amazon.co.uk/Stopping-Noise-Your-Head-Wilson/dp/0757319068 Stopping the Noise in Your Head]. It is one thing to feel bad and it is another thing to feel bad about feeling bad. The struggle is real! Most of the time we are having trouble distinguishing signal versus noise and a lot of times we are reacting to our reaction about something.

It is one thing to not be able to sleep, but it is another thing to feel bad about not being able to sleeping. When people talk about Insomnia, what you really should be talking about is: "I'm terrified to go to sleep, because they know I won't be able to sleep.", which is a whole meta anxiety about sleep. Merlin's wife told him a little mindfulness trick: When you get up, just decide that it is okay that you are up! Quit freaking out about it and stop obsessing! Be up for an hour and tell yourself that you are going to go back to bed in an hour. Don't feel bad about it, because then you are already doing two things wrong and you are really stressed out.

+ John getting an offer for a long talk (RL297)

Recently John got asked to do a thing that could be a big opportunity with a lot of money involved. He had heard of it, he was open to it and he wanted to know more. The person sent links to performances available online of not two, not six, but ten different people who did it before. After seeing that, John decided he did not want to do it, because it was not one of John's usual 15-20 minute talks, but an hour and a half long talk. 

The guy told John he was going to work with a content producer in order to come up with his talk, which John didn't want to do either. Still, it was a big opportunity and so they e-mailed back and forth multiple times. John said it was super great and he wanted to do this, but the guy's email tone was somewhat imperious and haughty, like "This is how things are done and you are getting a big opportunity here!" Eventually he said that they were firming up the lineup and he needed a firm commitment from John because this will be happening in September.

John has not been through the Merlin Mann-ification of dealing with this stuff and every time he gets a thing like this he still feels an overwhelming sense of responsibility not just to it but to everything. You don't turn down gigs, because how is Steve Agee ever going to take your picture out at Harriet's and Bob's? John has done a lot of good work, but at this point he has not earned a very large coffee table book. You want to put some more numbers on that board! You either get a Cadillac or fuck you! 

John feels the responsibility that if somebody offers him a thing he has to take it seriously and therefore he is talking to the ceiling a lot, walking around, waving his hands. 30 minutes? No problem! He could talk for 45 minutes, but after 65 minutes? Here is the wrinkle: There was no clear sense if this event was going to be $50.000, $5000 or $500. This is the magnitude trick! Just give me the two orders of magnitude your budget is between and you would be amazed how often the call ends right there!

If John is supposed to collaborate with a content producer, how about putting him in touch with the content producer before he will agree to do the thing? John doesn't want to say he will do it and then he get assigned some bottom of the barrel contemporaries, he doesn't even know what a content producer is! John is not going to workshop it, but he is going to make it up as he goes! 

The reply was that John was the first to ask about this, which was really smart! Unfortunately, the content producer was on vacation right now and she was not reachable, but she is super talented! They are now finalizing the lineup for instructors and they are going to need to have a commitment if John is interested in doing this before she returns. Now John feels a little dissed and he is walking around with his hands behind his back in an Napoleon March, like Churchill on D-day minus one: Marching around chewing on his cigar.

John's mom happened to be over at the house while John was marching around muttering with his hands behind his back, chomping on his cigar and she looked up from her magazine, because they still read magazines in this house, John explained it to her, and she took a page out of the Merlin-book and said: "There is nothing about this that you want to do, why are you even putting this amount of emotional energy into it?" - "Because it should work out and it shouldn't be this way." and she said: "Write him a one sentence e-mail that says: No, thank you!" 

Merlin asks if John was reacting to a thing that happened in the world or if he was reacting to how he feels about that thing, his own interpretation of a factor in the world? It sounds like John is reacting to pure interpretation for good reasons: He got a good heart, he wants to be a fucking performer and he wants to do his thing. This guy got ten more suckers like John lined up out the door! 

John replied that he appreciates being asked to do this, but the dates just didn't line up. "Please consider me in the future!" It was a garbage reply, but the burden was released and John was out. It doesn't matter! Maybe he is never going to be at Pappy & Harriet's with Steve Agee and Steve is never going to take John's picture and put it up next to Sarah Silverman's. 

+ John being anxious about turning 50 years old (RL297)

John is super-duper anxious right now, because he he will turn 50 years old in two months and his family wants to celebrate his big 50th birthday party by renting a house on the Oregon coast. John only replied with "That's amazing! Thank you!" They are trying so hard to do something nice, they love him so much and take care of him so much, but A) He doesn't like being celebrated in that way and B) He doesn't feel like being celebrated right now at all.

John is going through a phase where he feels like he is not very much of a success. 50 years old and not much to show for, quite frankly! Sitting in front of a cake with his loving and pretty small nuclear family of people that he matters to, blowing out the 50 candles and then going and staying in some kind of rented house, walking the beach? 

John wrote a letter to his whole family saying he was grateful for this wonderful thought, but he asked them to please cancel it and please not talk about his birthday again until the day before, when somebody can go "Oh isn't tomorrow your birthday?" Then they could get a Safeway cake and John will take the rest of the cake home because it is his birthday and he can eat it when he wants. 

His family wrote him back that they don't want to //make// him go to the Oregon coast, but they are just trying to do something to celebrate his birthday. Then let's celebrate John's birthday with his favorite form of celebration which is not celebrating! John can't tell whether or not they actually feel disappointed or are fine with it, but he is trying to not have that be a problem.

+ Riding roller coasters (RL297)

Merlin is past 51 now and he told his family that if they want to give him a treat, they should let him go to Six Flags Discovery Kingdom by himself for two nights. He wants to go to bed early and then ride roller coasters all day by himself. Taking a roller coaster with kids is no fun! Merlin wants to get the Rain Man pass where you get to skip lines, which you can do at Discovery Kingdom and just ride the fucking Joker roller coaster all day. 

John wants to join Merlin on that, especially because the park has opened in 1968, the year John was born! The Joker and the Superman in particular are very high quality rollercoasters. The only time John is ever at amusement parks is with his kid and she is too little to go on roller coasters. Nobody else in his family wants to go on roller coaster and so John ends up walking past the roller coasters with a sheepish, chagrined head-down look like "I will just go on the roller coaster and I'll meet you guys over at the cotton candy apples!", but he can never quite do it. 

Back when John was a single man with no responsibilities, he spent a summer in New York and went to every roller coaster in New Jersey. New Jersey got so many great roller coasters! They continue to talk about Discovery Kingdom!
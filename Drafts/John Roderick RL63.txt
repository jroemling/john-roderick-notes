RL63 - A Topcoat of Bonkers
2013-01-24

This week, Merlin and John talk about:
* Using an USB drive ([[[Technology]]])
* John finding his way back to graphic novels ([[[Comics]]])
* Getting letters in the mail ([[[Factoids]]])
* Toilet-papering the neighborhood ([[[Early Days]]])
* John passing notes in class to Kelly Keefer ([[[Friends]]])
* John’s dress code in High School ([[[Style]]])
* Going to High School with Jeffrey Dahmer ([[[Depression]]])
* John building pipe bombs in school ([[[Early Days]]])

**The Problem:** One day, you will love me, referring to the notes that John passed to Kelly Keefer in High School, eventually convincing her to be his girlfriend.

The show title refers to the outer layer of John’s dress coat in High School: Preppy, Northwestern wool and a topcoat of bonkers.

Merlin didn't reply to John at first because he had a technical failure on his part. He had the wrong audio input set up in Skype and his USB microphone was not used as the audio input. Beep boop!

+ Using an USB drive (RL63)

The other day Dave Bazan was trying to target disk mode some file into John’s computer and he hot-plugged John's Firewire Interface. If you hot-jacked something in the old days it could frizz your whole motherboard and John was super-concerned that his motherboard was frizzed and he couldn’t talk to Merlin. You got to be careful if you are doing any kind of hotboxing on the pop-board because you want to make sure that you got a full USB bus on your gigabyte!

They couldn’t find the right cable in John’s octopus-garden of 7000 different versions of Firewire and USB cables and Dave eventually pulled out a little USB drive the size of a house key that can hold 4 GB. John had a couple of those around here, too and they did it all on those dumb little totally losable USB drives. It is amazing! John looked at this room full of gizmos, boxes, cabling and all this gear and then he looked at this little USB drive which had an entire album on it and he felt a little bit dumb. 

Jonathan Coulton gives away USB drives with his entire catalog on them, which is amazingly smart. The equivalent at Barsuk is that they sell you reel-to-reel tapes in a box that has its own wheels. 

+ John finding his way back to graphic novels (RL63)

John had received two comic books as gifts: The first one was the graphic novel Joe Sacco’s Palestine and the second one was Onward Towards Our Noble Deaths by Shigeru Mizuki. John has always loved graphic novels like American Splendor, books by Dennis Eichhorn or The Greatest American Hero. R. Crumb was his introduction to it. Merlin says that sometimes they call these Slice of Life comics, which is silly, but the Harvey Pekar stuff for example is about real world people. Graphic novels are an art form that everyone else is enjoying, but John was not enjoying them just out of a spirit of curmudgeonlyness.

Book stores used to have maybe 4 titles of Graphic novels, like Mouse (by Art Spiegelman), Watchmen (by Alan Moore), R. Crumb’s Greatest Hits, or Fritz the Cat (The Life & Death of Fritz the Cat by Robert Crumb), but now they make up a whole wall of the bookstore that stretches to infinity. John looks at it and thinks it is probably all Manga: dumb porn with octopuses having intercourse with school girls on ships that you read backwards. John doesn’t need that in his life! Lately he realized that he was wrong and he was being a curmudgeon. He actually loves this art form and he needed to get over himself and start participating in this wonderful turn of events. 

A couple of years ago John got a graphic novel called Blankets by Craig Thompson from Portland, whom John knows, and at the same time John’s friend Tucker Martine was working on a record that went along with this heartbreakingly beautiful book. John thought it must be the exception that had no octopuses fucking teenage girls, but it was about someone losing their faith and falling in love for the first time. This was too beautiful! 

A few years ago John also read a beautiful and fabulous book by Joe Sacco about the war in Bosnia, but he still thought this got to be an aberration. Now he is realizing that it is his taste that is the aberration. Merlin admires John being introspect. Good for him for looking at something that is a little foreign and silly!

John had to admit that from his very first exposure to this type of book, which probably happened in college, he immediately identified with the form and wanted to make an autobiographical long-form graphic novel of his own, but he didn’t know how to do it. He can draw, but he is not a drawer. This is the great art form! Maybe his disappointment that he never was able to figure out how to put the pieces together to do a book like that on his own turned his back on it a little bit. 

When John was young he imagined that the barrier to entry for making films or graphic novels was imaginative. The thing that kept you from doing it was that you didn’t have imagination yet and you didn’t have a good-enough story, but later he realized that the barrier to entry is often just structural and technical. You don’t know how to work a camera, you imagine the scene and the picture, but you don’t know what an f-stop is and you can’t take the picture. To say that the barrier to entry to a graphic novel is that you don’t know how to draw is pretty ”No, D'oh!” If John had been working on this for 20 years, he would have developed an ability to draw well enough to accomplish what he was shooting for. He would be a retired Colonel in the graphic artist army by that point!

John is having a very exciting time reading these wonderful books that have drawing pictures and chipping away on the hardened calcium that has built up around his idea of comics. Merlin is going to reward this by not going on and on about comics with John and he will at least wait 30 days before he will start sending him trade paper backs John never asked for that he thinks John should read, because that is kind of a thing he does now. John loves getting things in the mail!

+ Getting letters in the mail (RL63)

The other day John was talking to a young person, which strangely is happening more and more. John said that back in the old days he used to have dinner in a particular café so much that when people wanted to send him letters they would send the letters to this café in care of John because they didn’t know where he lived because he moved too much. The young person asked: "Were you and your friends into sending letters and stuff?" 

They said it like sending letters was a pretty cool gimmick, a pretty cool shtick, like it was artisanal emailing, and John replied ”Well yeah, we did, because that was all we had, it was the only way to communicate with someone!” People sent John postcards or otherwise he wouldn’t hear from them for 9 months. They looked back at John and saw a bowler hat on his head, a monocle and spats. Nice snuff box! John still has a box with a bunch of letters from girls. ”Dear John, you are the biggest asshole ever!”

Merlin burned all his love letters in a Weber grill. He used to have them in separate boxes by person, but at one point he was cleaning out some of his stuff out of his mom’s house and thought he couldn’t pack that and take that home, so he went out and started up the grill. He also found some Starlight Mints and some ticket stubs, but he just let her rip. 

There was a big notes passing culture in High School for Merlin. You would fold the note in a certain way and pass them between classes. It became like an underground railroad to move messages back and forth. 

+ Toilet-papering the neighborhood (RL63)

During his Junior year John was famous city-wide in Anchorage for being the master toilet-paper-er. He would enter a neighborhood and would toilet-paper that neighborhood absolutely into submission until people were like ”Finn, please!” John would knock on somebody’s door, a kid he knew, and he would say that he was toilet-papering the neighborhood and was out of toilet paper and asked for borrowing some toilet paper, and the kid would bring John a 12-pack of toilet paper and ”Go get them, man!” They would shut the door and John would toilet-paper their house with their own toilet paper. He was a terrible teenager!

+ John passing notes in class to Kelly Keefer (RL63)

John still has several notes from High School. One piece of paper says in John’s handwriting ”Be my girlfriend!” and underneath that in a purple pen it says ”I would not be your girlfriend if you were the last boy on earth” - ”You know you want to be my girlfriend! Stop being coy, you minx!” - ”You are the grossest boy in the school! Why are you writing me notes?” John is not even kidding, it goes down one side of a piece of notebook paper and up the other ”If I am so gross, why do you keep replying?” - ”I am doing it because this is boring and I am transcribing this note in my own notebook as evidence when you try and kill me” 

John had set his sights on her and said ”I’m going to make Kelly Keefer my girlfriend!” She ended up being John’s High School girlfriend and he still has a stack of notes of him wooing her //(see also RL238)// She was much higher status than John was! Going into 11th grade she was absolutely the queen, she had a 4.0, she was the president of the Junior class, she was captain of the cross-country running team. At the same time John was last in class and the teacher thought about having John spend all of High School in detention, but they realized that that was not going to work, so let’s just keep him moving out the door! 

Trying to get a girl that way was very John Hughes because John had absolutely nothing to recommend him. There was no way that this girl who sat at the front of the class wearing an [https://www.etsy.com/market/argyle_sweater Arglye sweater] and who had the answer to every question was ever going to go on a single date with the kid in the back of the class that had spaghetti sauce on his shirt and was making fart noises. 

Yet, through the passing of these notes and John’s relentlessness, like ”You don’t think so yet, but one day you will love me, and when it happens, I will try very hard not to tease you about all the time you were sure this wasn’t going to happen and not to have this be a thing I will make fun of you about later” - ”You have so much chutzpah and you are so repulsive that I don’t even have the words to reject you as fully as I want to reject you!” - ”That is fine, you just keep talking, little lady!” 

Kelly maintained this impenetrable wall until Christmas break where she and her family went to Mexico. She sat in a condo in Mazatlán with her family, they were looking out over the beach and a single roll of toilet paper came from higher up in the hotel. As she saw it fall in front of her balcony she realized that she loved John. It was like a shooting start! She said that from that moment on John owned her body and soul. 

John had no idea, he was still back in Anchorage and was thinking about resuming his letter-writing campaign as soon as school starts again, but When she came back she saw John in a new light. Her new eyes did not see the spaghetti sauce, or they loved it. She was John’s only girlfriend in high school, the Irish setter red-headed corn roast girl. She is the chief of residence at Dartmouth hospital and she is one of the people who wears a white coat that has pens in the pocket and speaks to you in clipped abrupt tones about your own insides.

She later got married to a wonderful man named Seth whom John actually likes a lot better than he likes Kelly although he had every strike against him. a) he has a ponytail, b) he is from California c) he is a Seth. You meet him and you know his name is Seth! He is some kind of brain scientist, but not a doctor. It must be like being a baron midwife: Why didn’t he just go the little extra way and be a doctor if he was going to be a brain scientist? But that is part of his Seth-ness. As a doctor, your epaulettes have a little more scrambled eggs on them and then everybody gets out of your way. 

When Kelly started dating Seth, John was sure this was not going to work because Seth was a dumb hippie who didn’t know anything, but it turned out that he is nice and John liked him. They have a couple of kids who are also nice. The only problem is that Kelly is a terrible person, but you don’t get to be the chief resident at Dartmouth hospital without being a terrible person. Every time John sends her an email, he gets an auto-reply that isn’t like ”I am out of the office for 2 days”, but it is a page and a half of boilerplate telling him that his correspondence is confidential and she can’t answer emails and 700 different clauses. Get a fucking personal email address, asshole! Merlin suspects that she might have one, but she is a busy lady and she has only so much purple pen left. 

Merlin is proud of John's gumption and sticktoitiveness, but John says he used to have it, but doesn't have it any longer where he expects it to be, which frustrates him. Maybe he has gumption in areas he doesn’t know? You go to war with the gumption you have, not the gumption you want! 

Kelly was one of those High School girls who was full of sass. She would sass a rhinoceros to its face, she had no compunction and she had no worries at all. She knew where she was going! When she was 15 years old, she knew she would be the chief of residence at Dartmouth hospital and just get out of her way. That was definitely part of why she was so interesting to John. He himself was trying to overcome the fact that he had no idea even where he was at that moment, let alone where he was going. To quote the big John Hughes: John didn’t know whether to shit or to go sailing. How does that recommend you to anybody?

+ John’s dress code in High School (RL63)

When John was in High School he dressed like he does now: There is a base coat of preppy and on top of that he put a second coat of Northwest wool necessity. The topcoat is eccentric bonkers or Ignatius P Reilly (from the book A Confederacy of Dunces by John Kennedy Toole), meaning you got preppy, wool necessity, bonkers hat, scarf and boots. 

During the Grunge years John definitely wasn’t Grunge but the bonkers on top of the Northwest wool had enough overlap with the Grunge look. He was in through the boots and the hat. For the perspective of somebody who grew up in Anchorage it was the same thing.

John's High School days were the pre-duster era of coats and John did have an ankle-length trench coat with a big Skull & Crossbones on the back, painted in whiteout. It was the topcoat of bonkers, a World War I trench coat that John had bought at an Army Navy Surplus store, but it needed something more and whiteout was John's main artistic medium at the time. It smells good, it has a brush right there, and at a High School there is an unlimited supply of whiteout. It was ludicrous!

It was perfect for putting a picture in the dictionary next to ”geek”, but you couldn’t really make fun of it because John was already 6.5. feet tall (198 cm) and he was pretty serious about it, like ”Look out! I am poison!” Try and picture anybody being interested in John! Merlin tries to picture Kelly, this willowy High School superstar with auburn hair like an Irish Setter. 

Dusters and Australian-looking cowboy hats are the official coat and hat of the software engineer who didn’t get the memo. Or the memo has been delivered, but he hasn’t been checking that mailbox. Especially when you see a guy in that hat and duster getting off the bus or waiting Downtown in line at a coffee cart, you should tell them that they are dressed for a post-apocalyptic camel battle while waiting in line at a coffee cart.

Then Breakfast Club came out with the prissy red-headed girl and the John Bender character. They had been Holliwoodized enough so that the set-dressers on the film couldn't put a whiteout Skull & Crossbones on the back of his trench coat, but otherwise John's friends were every one of those people in Breakfast Club, that was why the movie was so successful!

There was that one guy (Paul Glaeson) who said ”You mess with the bull, you get the horns”, the one that the brothers bring in who also did the jam-up on Dan Aykroyd in Trading Places. Merlin loves that this movie is such a touch stone for their podcast. ”Every Christmas I dress like Santa and put a full smoked salmon in my beard in homage to...” 

Merlin feels the same way: He used to walk around with his Members Only epaulettes unhooked because he thought he was such an outside. Also, he wasn’t really outside, but it was the kids he was sitting around making fun him of who were really outside. As much as he would like to consider himself as a Peter Parker character in retrospect, he wasn’t, but he had a ridiculously, sarcastic and cutting wit that made a lot of people really sad.

+ Going to High School with Jeffrey Dahmer (RL63)

John received a very interesting graphic novel in the mail as a gift, a book about going to High School with Jeffrey Dahmer (the book is called My Friend Dahmer). John is familiar with the artist (called Derf Backderf) from alternative weekly magazines, he has been writing comics for years and years, you would recognize his work, and he coincidentally went to High School with Dahmer and wrote this autobiography.

He said the same thing: He thought they were real outsiders and Dahmer was their mascot, but in reality Dahmer was messed up and they let him into their gang only enough that he was kind of their pet, because he was such a weirdo and they loved having this weird mascot that they could walk around school with. Dahmer imitated having Cerebral Palsy and they all thought that was hilarious because they went to school in the 1970s before that was considered gosh. 

The book is really heartbreaking because he is talking about Dahmer when he was still out in the woods gathering animal skeletons and picking up roadkill before he had ever hurt anybody. He is looking back with a feeling of responsibility, but also pointing a finger at the adults at the time, saying that this kid was getting wasted drunk before school every day and was trying in every way to cope what was going on in his mind. He was being ignored by the faculty and the kids let him into their gang because he reinforced that they were the funny kids, the benders. 

He was a legitimate outsider and they took as much of that as they were comfortable with onto themselves, but as Derf said in his book they never invited him over to their house, but they would hang out with him at the mall as long as he was funny and they would stand there right in front of him and make plans for later in the night, but not include him. 

After he was caught Dahmer was talking about this period in his life when he was feeling these urges and was trying to suppress them, knowing they were wrong. He felt terrible and was wrestling with the fact that he was gay and couldn’t admit that to anybody in 1977, but he also had the desire to be in complete control of a living thing to the point that he would torture and kill it. He knew it was wrong, but it was an overwhelming urge and he ended up expressing that. 

Reading this graphic novel is putting you into a moment of this kid’s life where it was still possible for somebody to have intervened and directed him somewhere else. When you first realize that your kid is collecting roadkill in jars of formaldehyde in the barn out back of the house, do you have a talk with him? Nobody did that in this instance. 

+ John building pipe bombs in school (RL63)

Columbine happened and those two kids (Eric Harris and Dylan Klebold) were calling themselves the trench coat mafia. They were obsessed with guns and they felt like outsiders. If John would have been at that High School he would for sure have know those kids and would have been friends with them. At his own High School he was the equivalent of a trench coat mafia! He would have said that he was a total outsider, but in reality he was a functioning member of the school and actually an insider.

John was building pipe bombs with his friends and they were going out on weekends to blow up parked cars. They all had the Anarchist Cookbook, but while it is great it doesn’t quite go far enough. They tried to make their own black powder and after many weeks they were making very effective smoke bombs, but making black powder was harder than just mixing its three ingredients in rough quantities and stir it with a wooden spoon. Then they realized you can buy black powder at the Fred Meyer because they lived in Alaska and in 1983 people were still hunting with black powder muskets. You could buy a can of smokeless powder for $5 and it wasn’t even behind the counter, it was just out in the aisles.

Multiple times during the mid-1980s John as a 15-year would old walk up to the check-out counter at a Fred Meyer with 3 cans of black powder, 5 lengths of lead pipe with end caps and they would just sell it to him. ”Would you like your receipt, sir? The total comes to $24.97” John would walk out with a bag of what could only be bombs. He ended up getting put on double secret emergency suspension from school after they found it in his locker, because he brought it to school //(see RW42, RW84, RL180)//.

He had told the teacher about it because he was so proud of himself that he was making and selling pipe bombs. They were blowing things up very whimsically, but as a High School person you do not have any judgement whatsoever and they had no sense that what they were doing was any different than what any other kids were doing except that it was cooler. 

John stopped messing with pipe bombs after the following incident: He and his friends were at a party at a different High School. It was lame and they had been jerks to them, so they climbed in John’s car to pull away from it. John’s friend, who had worse judgement than John, opened the glove compartment of John’s car and there was a pipe bomb in there. 

They had figured out that if instead of the Fred Meyer you went to the gun store where they were actually selling blunderbusses, they also sold literal canon fuse. They bought 50 feet (15m) of canon fuse and instead of using solar igniters from model rockets to electrically ignite their pipe bombs, they started making pipe bombs that you could light with a lighter.

John had one of these in his glove compartment and John’s buddy pulled it out, lit it and threw it out the window. He lit a bomb that John had in the glove compartment of his car in case he needed it. He found it, he had a lighter in his hand because they were smoking pot, and he lit it and threw it out the window into the front yard of a house with 350 kids at this party.

As a teenager John did the only thing you could do, which was to slam on the gas. He was screaming at his buddy ”What the fuck did you just do?” and they were getting out of the neighborhood. This was a yard and a house full of kids, and this bomb was going to break every window in that house and it was going to hurt or maybe even kill somebody. John had never felt a dread as he did when he was driving away from this place, and the guy next to him was just ”Hahaha, fuck these guys!” 

They had tested these things all over: The lead shrapnel goes right through a car and just peppers it. They got to the end of the block, John was waiting for the ground to shake because these things were real bombs, and he was running down the scenarios of him being brought in front of a court room and being asked to explain why he killed these kids. Nothing happened! They waited a couple of minutes, John turned the car around and he slowly drove past this party where everything was just going on. In the middle of the yard was a dud pipe bomb that didn’t go off.

John was looking at it, thinking ”What is my responsibility here? I don’t want to run and grab this thing! It could go off at any moment! I don’t want to stop and get out of the car and yell: Bomb! I want to be 30 minutes ago and have this not happen!” A lot of those things were duds for a lot of reasons, it was not a surprise. John parked down the block and watched the front yard of this party, not knowing what to do, continuing to choose wrong. He did not go in there to shout ”Everybody out!” and the bomb never went off. Eventually he put the car in reverse and slowly backed away and they went on with their lives. 

How different John’s life would be if that bomb had gone off! He does not want to make an equivalent to the kids who actively went into their school and shot a bunch of people, but when the Columbine thing happened John was able to put himself in their shoes pretty easily. He could identify with their feelings and he remembered thinking that he was pretty cool.

It was obvious those Columbine kids thought they were very cool, and they killed a bunch of their friends because they were teenagers and teenagers are stupid. The most dangerous people on city streets are teenage gang bangers. If you see a 25 year old thug, he is so much more a reasonable person, but if you see a group of 15 year old gangsters, those kids have no limits and they don’t believe that their actions have consequences. That is why they are great soldiers, but they are the worst citizens. 

John thinks that his trail-building plan //(see RL48)// would be a solution. If he had spent his Junior High years [[[building trail]]] rather than sitting in school, learning the many artistic uses of whiteout, he would have been a better teenager and a healthier 15 year old. There is starting to be a groundswell behind [[[Supertrain]]]! They soon need to start implementing phase 1, which they probably already have, so it is time to start phase 2 because people are ready and the longer they wait, the more those million voices are screaming out at once. Merlin doesn’t even want to see the pages of the plan, but he just wants to see the size of the plan. It is written in whiteout, so it is a much bigger document than it needs to be.















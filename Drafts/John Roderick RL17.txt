RL17 - Antisocially Promoted
2012-01-11

This week, Merlin and John talk about:
* Having a soundboard and a theme music ([[[Podcasting]]])
* Words that are automatically offensive ([[[Factoids]]])
* Merlin’s teachers Mr. Chapney and Mr. Detlefsen ([[[Merlin Mann]]])
* The $100.000 bill ([[[Factoids]]])
* John graduating last in class in High School ([[[Early Days]]])
* Having a strange phone call with a young person ([[[Generations]]])
* John never graduated from college ([[[Early Days]]])
* John having a relationship with his guidance counselor ([[[Personal Development]]])
* John getting a top secret driver’s license ([[[Currents]]])
* Bureaucrats deciding if students are allowed to graduate ([[[School]]])
* Bureaucracy is what separates us from the beast ([[[Politics]]])
* Finding a Tinker Bell poster at Walmart ([[[Stories]]])

**The Problem:** Sometimes, the system works a little too well, either referring to John having graduated from High School despite not having the credits, or to American bureaucracy (there is no literal reference).

The show title refers to John having graduated from High School although he did not have enough credits and although there were people behind him with a better GPS score who didn’t graduate.

John opens the show singing Merlin’s name and they start to discuss how they would want their name sung, but John’s name is not as singable as Merlin’s. 

+ Having a soundboard and a theme music (RL17)

People have tried to give John theme music, but the best theme music he ever had was when he bought a little $0.99 keyboard in a thrift store that had 10 preset buttons on it that played little themes. One of it said ”Oriental” (new record: 54 seconds to [[[ping pong]]]) and when you pressed it, this 8-bit keyboard went ”dididididitditditditdi”

John carried this keyboard around on tour for three weeks and every time before he would enter a room he would just hold the keyboard through the door and push the button and then he would walk in. It was great because it was one of these jokes that stopped being funny, then got really funny, and then got //really// funny. John wished he had a hat that played that music everywhere he went.

Today they call it a soundboard and you can make Arnold Schwarzenegger or The Big Lebowski noises. Merlin always wanted a vest that would play Sad Trombone or ”Boing”. He doesn’t think of himself as a dishonest person and he doesn’t want an entire affected fake-audience, but he would like the opportunity to punctuate his own not-that-funny jokes with a sound effect. A very small part of him wants to be a morning DJ and John thinks he would be great at that, but although Merlin doesn’t mind getting up early he doesn’t like driving. He could have been a morning DJ, he could have been a lot of things!

+ Words that are automatically offensive (RL17)

There are two very special words they love but they just can’t use. Motard is like saying Jiminy Cricket: You know what you really want to say. Or like calling somebody a maggot. You could say flametard, but not Flotard, because that is a girl John knows. Putting ”tard” at the end of anything is automatically horribly offensive, like Orientaltard! They just lost two listeners in Canada! John should have a medical procedure more often! Merlin would do clowntard, dicktard, or fucktard. What he learned from John was fuckstain, which is a good one. Is there a lower thing than that? Asstain? Rick Santorum?

When Merlin first said Rue John thought Merlin was talking about Louisiana Sauce with flour in it. ”Lousiana Sauce” is going to be Merlin’s new stripper name. Lou the baby kangoroo in Winnie the Puh. Rue short for Roofie which is short for Rohypnol, the date rape drug. Merlin thinks John is having a stroke, but that would not be the first one today. 

+ Merlin’s teachers Mr. Chapney and Mr. Detlefsen (RL17)

At their current age John and Merlin really need to transition from talking about things they wanted to do or could do to talking about things they could have done. John could have been in the Special Forces, but he is no long eligible. Merlin should have listened to his bowling coach from 8th grade Mr. Chapeny, because he told him something that sticks with him to this day: Start when you are young! He was also their graphic arts teacher. Their AP History (AP = Advanced Placement) teacher was a guy who looked like Jaba the Hut, Mr. Detlefsen. John is going to make a note card about AP History, the first note card he ever made. Mr. Detlefsen’s wife was the dean of girls, which seems like some kind of racket. 

Merlin really liked graphic arts where he learned to use a compass and a protractor to make orthographic drawings. He learned perspective, the kind of perspective you can get in Pasco County in 1980. Mr. Chapney was from the Tri-state area, he would say ”Ruefully”, and he would shake his bearded head and say: ”Guys, I really wish I had started doing this when I was 14, because bowling is one of those games where if you started now, if you took this seriously, if you sticked with this and do this a couple three times a week, you could go the show and go all the way to the top and play with the big boys wearing the metal cleats!”

John says that is true of everything, like tap dancing, but Merlin says it is not true. Zooey Deschanel’s secret is that she started young on all that stuff. Merlin doesn’t think it is true of football. Instead you have to be a Thyroid case and you have to be a little goofy, but John disagrees. You have to be big, but that is self-selecting. Merlin also says you have to be gay, but John doesn’t think that is a prerequisite. You have to like dog fighting. Merlin bets that if you try really hard at golf you will get good. There are a lot of things that you can’t get good at no matter how hard you try, like if you are a Motard. Merlin wishes they had that word.

Mr. Chepany was very nice, but you cannot be a nice teacher if your name is Detlefsen. You are a born prick and you get a cold sore just saying that! Detlef Schremp seemed like a nice guy, he was a basketball player back in the old days when Seattle had a basketball team called the Sonics. Now they are in Oklahoma City. Merlin hates sports, but the only thing he hates more than sports is people who like sports and people who can suffer the idea of a regional name going to a different fucking place. It makes him so god-damn mad! They are now called the Oklahoma City Gold Stars //(actually Oklahoma City Thunder)//, which sounds like a sex thing. They are the Sunshine Rainbows now, one of those expansion names. John doesn’t care about sports either, but he has opinions about team names.

Merlin was never in AP History and at one point the teacher said that as part of this treaty the Native Americans would get 2/5 of the land or 40%, whichever was more. That was a good class! Merlin’s friend Sam had AP History, which was a hard class in general, and Mr. Detlefsen was famous for two things: He did two 20-page papers every semester as a giant part of your grades. Merlin didn’t ever write one 20-page paper in High School. Your second 20-page paper was due the day before Christmas break and he demanded that AP History always be 4th period so that you could take his test through lunch. a) put your 20-page paper on my desk! b) you got your semi-final that you have to take now. 

This is a real Blue Book: ”Discuss World War II!” How fucking great is Mr. Detlefsen? Take as long as you want! John would have taken this test all the way through today, he would still be sitting there, writing on his beard. He would have graduated from college with a degree in World War II, he would have a PhD in World War II! At the time that was merely one of those stories like the teacher with the paddles with the holes //(see RL11)//, but that is one of those things that is real. AP History attracts teachers who justify any manner of sadism by saying they are preparing you for college.

Merlin thinks ”Discuss World War II” is maybe the greatest Blue Book question of all time. What he didn’t know then was that you just had to write something really fucking awesome about an aspect of it, something cogent that fit in the book, which is true of all SA questions: You can show that you have a deep knowledge about a topic by just skating across the top of it if you write with confidence, but in High School you can’t count on even 10% of the kids being able to write. Mr. Detlefsen was a guy who could really turn things around, but John doesn’t think so. He just thinks the system is broken. 

+ The $100.000 bill (RL17)

The Treaty of Versailles was in 1919 when Woodrow Wilson was president. He is on the [https://currencies.fandom.com/wiki/United_States_100,000_dollar_banknote $100.000 bill], but they are not a lot in circulation. How bad would you feel if you lost one of those bills? Bruce Vilanch is on the $3 bills. In Europe they have 500 Euro bills that are in wide circulation, but they won’t give you $500 bills. It is the Tyranny of $20 because of the ATMs. President McKinley is on the $500 bill. There is a website called coinflation.com that tells you how much your coinage is worth if you melt it down, just for the melt-weight of the metal. It is pretty nuts for pennies. They continue to talk about coins and metals for a while.

+ John graduating last in class in High School (RL17)

John got kicked out of AP History and it was a scandal because you are supposed to get college credit if you get a good grade in AP History. They might not care about that in Princeton, but at the University of Alaska in Anchorage they might let you skip Freshman history.

John had long since stopped paying attention in school, he was taking a make-up exam out in the hall, and he had his history book in the little rack that was bolted to his chair-desk-combo-thing. At a certain point in the test he looked down and flipped through the pages and tried to figure out when the war of 1812 was or whatever. Some teacher was out in the hall and came around the corner and saw John cheating. 

It was a cheating scandal and he gave John an F and he actually cried real tears because John was one of his favorite students and it was a betrayal of the education. "John was betraying himself" and all this terrible stuff. It was a real burden of guilt and shame! John flipping through the table of contents of his history book out in the hall. It was cheating, alright!

At the end of the senior year John didn’t have enough credits to graduate. He graduated last in his class with a 1.2 cumulative GPA through 4 years of High School. On the list of graduating seniors there were like 25 guys who had higher GPAs than John and who were held back and were not allowed to graduate //(see RL178 and OM128)//.

The principal, the guidance counsellors, and the teacher called John down to the office and said that they had talked about it and they don’t want him to come back. He had an F in AP History, but they were going to award him the credits and they were going to give him credits for his extracurricular activities that you will see over here in column B. John got an antisocial promotion. ”Congratulations! Good luck! Godspeed!”

The word that John didn’t have enough credits to graduate leaked out in the month coming up to graduation. It had already been a scandal and right up until the day of graduation it was still a matter of contention because some people were saying: ”Of course he is going to graduate!” People were betting cases of beer back and forth and then John walked at the ceremony in the big auditorium. When they called his name you heard 75 people making raspberries across the room, honking their little airhorns. It was kind of rude, actually, but that is how it played out. There is so much wrong with that on every level! It was: ”Don’t come back!”

At Merlin’s graduation everybody went to graduation. You sat there, they called your name, you had to go, and even if you failed out you still had to walk because it would screw up the seating. They handed you a blank case with no diploma in it, so it looked like you got a diploma, but there was no diploma inside. Merlin didn’t know until he looked inside whether he had a diploma. He was right on the edge, but for all he knows it was a clerical error. Merlin failed two classes when he was a Senior and he shouldn’t have graduated, but somehow it happened. He was taking geometry in 12th grade, it was a busted-ass system!

+ Having a strange phone call with a young person (RL17)

Today John had a business conversation on the phone with a girl in music business capacity. She works at an office where John has business dealings, but not in Seattle. John said halfway through the conversation: ”Have we met before?” - ”I don’t know!” - ”Alright, are you sure that you don’t know? I would think that you who is working in an office that deals with musicians would know if you had met me, but okay, that is fine!” and they talked a little bit more and got to the end of the conversation and John said: ”Thank you very much for your help!” - ”Yeah!” and then there was a long pause and John said ”Okay then, good bye” - ”Ohh... good bye!” and it felt like John was in Teletubbies land.

They were talking about real money in a high-powered business convo about agreeing to do things and John was giving her verbal authorization to make some money. She was talking to him like it was a Teletubbies thing and you could tell from her voice that she was in her 20s, potentially in her early 20s, and John imagined that she was wearing some skin-tight jeans and maybe some big white sunglasses and a Palestinian scarf. 

Even so, her phone etiquette was so off the charts in the wrong direction bad that it made John think that it was ultimately the fault of the American schools. It was further evidence of this thing that they always talk about, the end-times as represented by sweeping waves of fake autism that we call youth culture now.

John didn’t get any resolution to this, he wasn’t even sure that they had hung up, and he stared at his phone for 30 seconds, thinking: ”Are we done?” Merlin had a lot of conversations like that in the last five years, even including the facts that he doesn’t like talking on the phone and that he talks to fast. He will actually say to people: ”Are you checking your email right now?” because you can hear them not be listening.

For a long time John was playing Minesweeper during phone conversations because it keeps him in the right zone for a phone call, but he realized: ”No!” It was false, too much of him was being used up, and he was not actually in the phone conversation as much as he needed to be. Merlin can not do it and it is so obvious, he might as well be driving on a highway while trying to chop green peppers.

Merlin agrees that it could be the schools. It is messed up at every step of the way! Both Merlin and John are examples of white people who got to graduate from High School but who shouldn’t have and that is ridiculous! Almost all of Merlin’s teachers were really stupid and only some of them were awesome like his drama teacher. They clearly did not do what was asked of them because John and Merlin should not have been allowed to graduate from High School. They did not perform the tasks, but John feels like he shouldn’t have even gone in the front doors of High School!

Merlin continues to talk about how the Soviets would get children out of class and give them special education like working a lathe.

+ John never graduated from college (RL17)

What a lot of people don’t understand about America is the SAT, but it is just to keep the black man down! Of all the fucking chicken bones they come up with to decide if you are ”successful” in college, SATs are the single best indicator of success in college. That is why you have to take the SAT. They could look at your essay, your grades, or your community blah blah, but that was the thing.

Success in college is no indication of success in life, but if you don’t go to college your life is going to be a lot harder. Merlin graduated from college and got a diploma, and it is right here by his Divinity degree which was all made up. He has a diploma and he is a mail-order minister like everyone he knows, and he has a friend who lives near the Universal Life church and apparently his pick-up truck is full of pamphlets.

John never graduated from college, he is a college drop-out. He got anti-socially promoted out of High School and he didn’t graduate from two colleges. He has more than enough credits to graduate, but he just has never filled out the paperwork to graduate. Merlin is so reluctant to ask John anything about this. Not having graduated makes John feel like he still has a hand in the game. Did John’s family pay all that money for him to go to college and the only reason he is not a college graduate is that he didn’t go to the registrar one day? Well, it is a little more complicated than that. There was so much administrative bullshit! John really lucked out!

+ John having a relationship with his guidance counselor (RL17)

Gonzaga was on a semester system and the University of Washington was on a quarter system, meaning that John's credits didn't all transfer straight across. At a good / weird school that is not unusual at all. Merlin’s school would hardly give you any credit for anything. Not only didn’t they have that class, but they were also better than those other schools and there was no way they were giving that: ”You got a who from a what? Maybe we will give you a quarter’s worth of credit for that semester-class!” It was like in an intellectual pawn shop. //(for the following, see RL243)//

On the first day at the University of Washington John showed up at the guidance counsellor’s office that was like a bus station waiting area with 60 kids waiting who all had taken a number. There were doors on either side with 15 or 20 guidance counsellors and when your number got called you went to the next available guidance counsellor who during the next half hour would decide your fate in college. Also this was going to be your guidance counsellor for the rest of the time there, a person you were going to see over and over again. John hated everything about this. Gonzaga had promised not to include his permanent record with all the disciplinary files if he left now, but John had no proof that they had not done that.

John was sitting in the lobby, bouncing his feet back and forth, hoping this would go well, and he got called into the office of this woman guidance counselor who was about 4’11” (150 cm), probably weight 96 pounds (45 kg), and had a Dorothy Hamill haircut and a NPR tote bag. She was this darling little [[[manic pixie dream girl |pixie of a lady]]] who had been an attorney for many years and then transitioned into this role of University of Washington guidance counseling. John never figured out why.

They started talking, the conversation got very flirtatious and she came around the desk and sat down in the chair next to John and said: ”Let’s see what we can do!” She went through his Gonzaga credits and said: ”We don’t have this class, but here is what I am going to do: We will just call it the next highest level class and we will say you have credit for all of it!”

She translated John’s two years at Gonzaga into being halfway through Junior year with a major in Russian Lit and a double-minor in Ancient Greek and Philosophy. John was like: ”Tell me more!” She asked him out on a date and they went out for several months. John was 22, she was 38, and she held John’s future in the palm her hands, literally, and she would use that sometimes to get John to do things in the romantic realm.

She would remind him of the power that she had over his future and it was a total May-September reverse sexual power dynamics. She had John not exactly under her thumb, but if he stepped out of line she applied a certain amount of pressure and she had no compunction about giving John the business and reading him the riot act although she was a tiny little gal.

One time she couldn’t get ahold of John and she called his mom and said: ”This is John’s guidance counsellor. I am concerned about his well-being. Do you know his whereabouts?” and John’s mom, God bless her, smelled something fishy and said: ”Who is this? Why are you calling me here?” and scared her in the opposite direction: ”This is highly inappropriate!”, that type of thing, and she never did that again. Eventually the relationship ended disastrously, but not before all the papers had been filed, stamped, and sent off to the great warehouse in the sky. it was an eye-opening relationship for John as a young man because you don’t usually get into those kinds of scrapes.

John is not sure if he was the only one, but he certainly was her cause for that year and she devoted a lot of time to him. There wouldn’t have been a lot of extra free time to have other undergraduates. As a young man John had tremendous charisma. Today he is a grotesquery, but at the time he used to wear sweaters jauntily draped across his shoulders, he wore Levi’s that had been dyed a different color so they weren’t just regular Levi’s, but they had a purple tint to them, he had a lot going on!

This was another thing in a long line of happenings where John walked away saying: ”Well, the next guy in her office is probably going to get his bed short-sheeted because she just gave away the farm to me!” The next kid would come in with transfer credits from Wazoo (Washington State University) and she would just be like ”Nope, sorry! Start over!” She certainly mentored John in a (romantic kind of way).

Merlin suggests John should look her up on the Facebook. She is 58 years old now and that is a nice time to hear the phone ring: ”Remember me? Dyed Levi’s?” Thinking about the way they left it they would probably pick it right back up where they left it. John should stand outside her window with a boom box. She surely kept her figure, too bad John couldn’t! Those people wield so much power, like the Registrar, the person who is headed up that hole in the administrative department. It is like the gals at the DMV, they are the first line of defense!

+ John getting a top secret driver’s license (RL17)

The last time John went to the DMV every single person he dealt with was a guy. There was a whole string of guys! John was getting a top secret driver’s license. They now got driver’s licenses in Washington state that function as Junior Varsity Passports, so you can cross any over-land border in America from Canada to Mexico plus any arrival in America if you are on-board a ship. This sounds so made up!

You can use this driver’s license in lieu of a passport if you arrive either by land or by sea. It has an RFID on it and is embedded with all this biometric information and they made John carry it in a tin-foil sheath so that people can’t scan it when he walks through public places. Washington State is the first or the only state that has this special driver’s license. Two states were supposed to get that special license: Maryland and Virginia, which would make sense because who wants that is Senators! Washington is way ahead because they have a lot of crossings into Canada, they have a lot of boats, and they are forward thinkers.

It was a real watershed moment for John as a citizen because for many years he didn’t have any government ID at all and he eschewed citizenship and because he did not want to be complicit in all the machinations of citizenship. 

What makes John frustrating is not only that he gets off easy on this stuff, but he does it by fucking avoiding paperwork. Everybody hates paperwork, but somehow John managed to cobble together this broken lifestyle based on talking to ladies on the phone and getting favors from people with pixie cuts. Merlin has no idea! 

They made John go through some extra hullabaloo because it is an official document. They screened him and he passed that because he didn’t have an ID for so many years. He used to get picked up by the cops and when they found out he had no ID half the time they would just let him loose. They were scanning John’s eyes, they were taking 3-dimensional photos and they said there are machines in airports that are going to be able to identify him by his biometricality. 

John went through all this stuff that he used to think was all Big Brother one world new world order alien overlord George Bush super government, all that stuff that you only believe if you are smoking a lot of pot. He was voluntarily submitting to this new world order type of extra-data-gathering and he just didn’t mind it at all, shrugged it off and said: ”That is right! I am not some Eritrean taxi driver, but I am a super-spy!”

All these administrative parts involved some intervention or ignorance on behalf of someone who was basically a bureaucrat. John has business being cleared for anything, which doesn’t make a lick of sense! Merlin doesn’t know how John got a loan! Just an evening of sitting around with John would tell somebody that he is not qualified to have the keys to anything, it is a terrible idea, but his credit score is 840, which is good.

+ Bureaucrats deciding if students are allowed to graduate (RL17)

At Merlin’s college the Librarian was the one who inspected your physical thesis. You had to turn in two copies of your thesis. There was the version that had been cleared by your committee and your sponsor after Bakaloria and you were done and good to go. You had to take two copies of that to a big lady called Althea Jenkins and she would inspect it. Merlin will never fucking forget her name, he never forgets the name of any administrator who crosses him! For every single one of the about 60-100 graduates in Merlin’s class she would measure the margins with a ruler because one of those copies was bound so they could keep your thesis on file.

An administrative bureaucrat working for the parent university for Merlin’s 500 person school was in a position to decide who was going to graduate from the Florida Honors College based on her little drug-store ruler. If you did anything wrong and were 1/16” over on this one page because it had printed a little bit bad you had to go back and start over. Luckily Merlin was coming up in the age of computers and wrote his thesis on a Mac.

The guidelines said that if you had more than 3 erasures or corrections on a page you had to retype it and if you took Dr. Peggy Bates’ class 3 years before Merlin had arrived there, she would have you redo the whole paper if she made a correction on it and you had to take it back in. In the early 1980s people had to retype 20-page papers. That is $1.25 a page and runs into serious dough!

Whether it is your testicle-less man at the DMV, the idiot who scanned your eyes and gave you the clear pass, or this whole phalanx of broken people who let you leave the building at all in High School, these are all bureaucrats and administrators. The TSA person who decides to pull you out of the line is a bureaucrat. You could be thwarted at any time by a bureaucrat! John interrupted Merlin's rant.

+ Bureaucracy is what separates us from the beast (RL17)

The bureaucracies of the former Eastern Block countries, particularly the ones in the Belarus / Ukraine / Bulgaria / Romania orbit, and John exempts Poland and the Czech Republic from this, although Slovakia is in this category, but not Hungary, have completely broken down and when you travel to places like that you will see what it is like in a world without bureaucracies. Bureaucracies are what separate us from the beasts!

In these places where either corruption is rampant or where the system has broken down totally and your culture goes feral, you realize the value of the recourse to the law that bureaucracies allow us, that we can avail ourselves, even if you have to fill out 1000 forms! The fact that the form is there alone means that you have some hope that someone will read the form and feel obligated by their job to take the form seriously, to consider your problem. It is such a gift //(see RW53)// that you don’t have to bribe the people who pick up your garbage.

It is particularly noticeable in the countries of the former Eastern Block because there used to be a tremendous bureaucracy and people relied on it for everything. Then it went away and people don’t have the self-reliance and there isn’t a sense of: ”If I sweep in front of my store and the guy next to me sweeps in front of his store, then we have at least a clean sidewalk for the front of these two stores and that will help our business and our customers will come and the sidewalks are swept!” Bureaucracies took care of all of that for decades and then it went away and there is a sense of futility that pervades the whole culture. You get these people who say: ”Why should I sweep in front of my store? What good will it do?” 

You see missing manhole covers throughout that area //(see RL57)//. Even though a lot of the rural areas in Central America are not built to the same urban standard that you will find in Kiev, you are not going to walk down the street where all the manhole covers are missing, which is incredibly dangerous because you can fall into one of these and get hurt.

The manhole cover itself is worth a certain amount in scrap metal and somebody just grabbed it and there wasn’t anybody to replace it. Now you just got open manhole covers all over the place. Talk about really having to watch where you walk! Whoever’s job it is up the line at the ministry of missing manhole covers, there is no penalty for them if they don't go out and replace them because there is no form to report that the manhole cover in front of your store is missing.

You don’t have to spend very much time in places where bureaucracy doesn’t work to come back to America and thank God for that stack of forms, but more than the stack of forms, thank God for the many levels of administration that almost always end somewhere with accountability. Ultimately there is a person who’s job really is at stake and who will hold everyone else accountable so that people don’t just take your form, say: ”Oh, that’s nice!” and light their cigars with it.

Even if it is your congressman, ultimately... Merlin has some doubts about that because how do you reach your congressman? You got to be John Roderick who knows Ted Stevens, you can’t just waltz in there and say you want a new manhole cover, but you have to foxtrott in there. If you are a dedicated letter-writer, you can accomplish a lot in the world. 

People like Merlin and John who don’t want to go to the bother often feel thwarted and frustrated and people who don’t have the education to know or to be able to compose a letter or to know that that is possible are also at a disadvantage. This is why it helps to be like John’s sister who is good at dealing with customer service: Old people have nothing better to do, they will grind you down, they will follow up on the phone call, and they will stay on hold. 

This is why the Tea Party has been so tremendously successful: They don’t represent the majority of Americans, but they are made up entirely of people who are willing to write 1000 letters to their congressmen and their local Aldermen. These are the people who are willing to learn Robert’s Rules of Order and disrupt town meetings and ultimately the reason they have power is that there is a process by which the elected office holders, the bureaucrats are accountable. The system, for all of its waste, is incredibly effective relative to almost anywhere else in the world.

Merlin thinks that John is wrong, but the problem with John is that he is not completely wrong, but he is only mostly wrong. There are so many things where John can truly help people. Whenever you go somewhere you can learn a lot about what made the city be built there rather than somewhere else. Are there mountains? Is there water? Does the town exist because of the railroad?

Start with your city and look at it in the context of geography and the way the city interacts with whatever is the main waterway, where the railroads come in, and where the natural resources and transportation are. In San Francisco when they blew out all the Japanese people out of Japan Town, they moved in all the African-American folks to then work in the ship-building factories. They just took stuff out of people’s houses and moved factory-workers in there and then they had a lucrative lifestyle for a few years until it basically became a ghetto.

If you go down to where they take the containers off the ships and onto trucks at 1am, and you watch the trucks head out into the night, and you imagine what is on those containers, you start to see the role that San Francisco plays in the life of the nation. San Francisco is an organ that is pumping things out into the rest of the country. It is a robust system, but also a really fragile system. Any one of those little aspects can fall apart and this is why when you listen to truck drivers complain about fuel costs it can strike you as very boring, but those guys are right at the... Look up Cunsler (?) and Peak Oil! He is a little over the top, but he has a point!

If we have gotten close to getting most of the oil out, there is not going to be a Walmart anymore because everything in that Walmart is made out of plastic thanks to cheap oil that is brought there by trucks that are running cheap oil, but most importantly they came from China on a giant-ass ship that was running on cheap oil. Even before that, leave China out of it, but chop a tree down and turn it into boards and move the boards and build a house. There are half a dozen different people in between, but you are still getting a fair price for the wood.

+ Finding a Tinker Bell poster at Walmart (RL17)

If you work with Walmart your prices have to go down every year. This has been true for Snapper, Vlasic, or Levi’s. Snapper used to be a premium brand for lawn mowers, but they decided not to go to Walmart because Walmart makes you create an entire product line just for them. When Levi’s went into Walmart they couldn’t repurpose any of their existing product lines, but they had to create new product lines just for Walmart. Vlasic spent 20 years trying to get you to understand that if you buy those premium pickles in the fucking refrigerator section it is worth paying more because it is really good, but you now go to an end-cap and you can get a 5 gallon bucket of fucking pickles for $0.20.

John has only been into two Walmarts. One time he went to a 24-hour Walmart in Florida and at 3am it was full of family shopping with kids. There were a lot of women with 5 kids because that is when they get off work. John ran out of that Walmart screaming!

The second time he went into a Walmart was on his birthday in New Mexico. He was walking through the Walmart, just taking it all in, and in their kids’ section there was a wall-sized poster of Tinker Bell, the beloved Disney character, the disagreeable little minx. She was flying because she is a sprite, looking over her shoulder at you, and her little tuchus pointed at you, and the way her little feathered skirt was arranged was like looking up the back of her skirt at a little pixie tuchus, a little fairy upskirt shot.

She is blond because why not, but if you look at her face you see that she got a very long nose and she is a very Judeic looking pixie. She looks like a blond Fran Drescher, a kind of Jewey little pixie with this big ass that she was pushing out at you, an Ashkenzai little flying bug! This fantastic piece of art on this poster was $1.99 and it was John’s birthday so he bought it for himself and carried it around on this tour. He put it on the dashboard of the van rolled up and everybody asked: ”What is that?” - ”Don’t touch that! Don’t look at it!” and John never told them what it was, he just drove around on the rest of the tour with this thing on the dashboard. When he got home he put it up on the wall.

All the women who come over to the house are all... Was he looking at it just now when he was describing it? No, he has it memorized!

















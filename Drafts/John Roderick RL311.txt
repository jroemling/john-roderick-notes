RL311 - Friend Mode
2018-10-29

This week, Merlin and John talk about: 
* Frank Sinatra and Dean Martin ([[[Movies]]])
* John syncing to Google Drive while recording ([[[Technology]]])
* John’s daughter telling jokes ([[[Daughter]]])
* John can no longer adjust his ski bindings ([[[Sports]]])
* Humans of New York ([[[Internet and Social Media]]])
* John’s daughter’s English homework ([[[Daughter]]])
* Corrupting his looper ([[[Technology]]])
* John trading Jason Finn’s computer for a KEXP bag ([[[Technology]]])
* John upgrading his operating system to Mojave ([[[Technology]]])
* Trying to get stuff off his old computer from Anna Banana ([[[Technology]]])
* Fancy new Apple products ([[[Technology]]])
* John taking his lawnmower engine apart ([[[Cars]]])
* John’s car repair experiences ([[[Cars]]])
* Thinking his starter was broken, but it was the battery terminals ([[[Cars]]])
* Multitasking while walking his daughter to school ([[[Daughter]]])
* Papa John’s pizza ([[[Food and Drink]]])
* Going to Ruby Tuesday in Alabama ([[[Food and Drink]]])

**The problem:** John only had one mouse, referring to hooking up both his old and new computer at the same time like a battle station while only having one mouse and having to switch it between the computers.

The show title refers to a mode you can put an Apple computer in if you want it to talk to another computer.

Yesterday John spent all day [https://www.instagram.com/p/BpiUmGOg1We/ sorting all his glasses].

It is going good! John sounds subdued, but he is still the same amount of dude, a big dude with a hat. He was wearing a hat until moments ago. 

Merlin learns a lot of things using YouTube. He learned how to fix his Ice Maker from there. 

Merlin likes the song [https://en.wikipedia.org/wiki/Terrapin_Station Terrapin Station].

+ Frank Sinatra and Dean Martin (RL311)

You shouldn’t wear a hat with your cans! Sinatra's style was to have a hat on and hold the can up to one ear because you didn’t want to take your hat off with your cigarette in one hand and your can in the other.

The other day John's mom gave him one of those lovely little glimpses into the past when she said that we look at people like Sinatra and Dean Martin as suave 1960s icons today, but at the time they were sleaze-balls and borderline gangsters who were reviled by any good upstanding citizen. They were considered sleaze-bags making sleazy music for sleazy people. Dean Martin left his wife with 6 kids in the lurch and never looked back. The only one who had any class was Sammy Davis Junior while the rest of them were gangster-adjacent and would have worn gold [https://en.wikipedia.org/wiki/Grill_(jewelry) grills] if they could have. 

+ John syncing to Google Drive while recording (RL311)

John sounded very crunchy like he was very far away from the WiFi, but he was plugged into Ethernet and the reason for the drop-outs was that Google talked him into putting his computer on the web. John was plugged straight in, he was hot-lining, he was main-hotting, he was hotboxing the Google, it was all happening! Merlin wondered if John was under duress and if he could blink his eye, because he sounded like the guys in Korea [https://www.atlasobscura.com/articles/north-korean-officials-had-no-idea-what-their-hostages-were-signaling-in-this-photo who gave the finger in the photo]. Steve Balmer was sitting in John’s office while John put his phone number into Skype for Microsoft to verify his account.

Google was telling John that he had synced 30722 of the 49504 things it was trying to sync and he had used 73 GB of 100 GB. It also conveniently offered him the option of upgrading. John was syncing to the cloud and he was not sure why, but a Google had popped up and said ”beep boop”. Merlin was going to leave this in the show because it was important. He likes Google Photos and John needs to run the dumb sync dingus in order to do that, but it would take a good long while and for now he suggested John to ”Quit Backup and Sync”  Merlin could feel already that this episode was going to be a classic.

+ John’s daughter telling jokes (RL311)

Sometimes when Merlin tells a joke to his daughter she will say nothing but ”That is not even a joke!” and he will say ”Shut up! I’m not going to pay for your college, you piece of shit!” When Marlo sits at the table and there is a lull in conversation she started to ask ”Do you want to hear a Peanuts joke?” - ”Yeah” and then she will kidsplain a Peanuts joke: "Charlie Brown is walking down the street. Do you know who Schroeder is?" - "I do!", but she only knows the old Charlie Browns, like "Do you know who Violet is?" 

Violet says: ”Hey Charlie Brown! Do you want to come over to my house to play?” and Charlie Brown says ”No! Every time I come over you get mad and then you send me home” - ”Why don’t we play at your house? That way when I get mad you will already be at home!” and then she will give John the mic-drop face. ”Kapow!” Sometimes she asks ”Do you want to hear a Garfield joke?” - ”Tell me a Garfield joke!” - ”Do you know who Lyman is?” - ”Yes, I know who Lyman is!” 

+ John can no longer adjust his ski bindings (RL311)

One time John got his old skis out, went to the ski resort to rent some boots and when it came to adjusting the binding the kids there said they had never seen those skis before although they were top-of-the line race quality Völkl with Olympic quality Salomon 747 Equipe bindings //(introduced in 1984)//. The kids were not authorized to work on them and John had to ask them to bring their manager out, but the manager was also 29 years old and while he had seen those before he confirmed that he was also not authorized to work on them. 

John told him that he will sign whatever form they need so they could loan him their screwdriver and John will adjust the bindings himself and take all responsibility. The manager came back with some piece of paper that was ”I accept all responsibility” because John knows how to work on the binding and this was [[[Is this your first day |not his first day]]], he had Olympic-quality skis! He just wanted to rent some boots and they needed to adjust the skis to fit the boots, but they couldn’t do it. ”I’ll do the bindings! You just get out of my way! You know what? Clear path! You guys... time to watch and learn!” It is not rocket science, it is skiing.

+ Humans of New York (RL311)

Humans of New York has been in Rwanda interviewing people. John never understood how he gets people to open up to him so completely. He gets amazing stories, but [https://www.instagram.com/p/BpVmkoPgAZg/ now he is in Rwanda] and the stories are just fucking devastating. 

+ John’s daughter’s English homework (RL311)

John's daughter's homework has sometimes been to the extend of "Write a sentence using the word ’because’” and she will write ”because I want to”, but that is not a sentence, but a sentence fragment. Because it says the word "sentence" in the homework, John used it as a teaching moment to teach her what a sentence is, but she resisted because she thinks she doesn't need to know that. There are things you need to know that you don’t think you need to know! The things you want to teach them the most are the things they want to hear the least: Subject - Verb - Object! Everything we do in this homework is going to meet this basic criteria from here on out!

+ Corrupting his looper (RL311)

The other day John downloaded a bunch of loops for his looper and then the looper said that John had corrupted it somehow because he didn’t eject before he pushed the ”End sync” button. John had to go online and figure out what to do, but all they could say was ”Factory reset” (”You got to park your blunder buss”) and a lot of the comments were from 2009. 

+ John trading Jason Finn’s computer for a KEXP bag (RL311)

Jason Finn traded John his new computer for a messenger bag //(see RL262 and RW121)// Upon hearing this, Merlin suspected that John got Jack and the Beanstalk-ed a little bit, a story where a traveling salesman took Jack’s perfectly good cow and gave him a bag of beans, except in this case Jason Finn got a really nice bag of beans and John got stuck with a morbid cow. John disagreed and the reason this deal was bad was that Jason wanted John to buy him a steak dinner for the computer and they went to a nice restaurant. 

John should just have sprung the bag on Jason and should have said ”Bag for Computer” because Jason would have given him three computers for that bag. It was a bag with no street value except for a very small group of people around 90.3 KEXP, the famous Seattle radio station that invented Indie Rock. The KCRW people might argue that, but KCRW can kiss John's ass. Morning becomes erectic *fart-noise* which is not a word. It is not even a joke! KEXP was a public radio thing and like at MaximumFun you would get swag when you contributed. 

One year they gave away these bright orange messenger bags that were not even good messenger bags, but they said KEXP on them and were exactly the size of a 12” record. Maybe they were designed as DJ bags so you could throw you 10 hottest copies of Purple Rain or whatever in there. It is all this bag is good for because it doesn’t even have a pocket for your keys. You needed to walk around with your records and take all the labels off so people didn't know what you were sampling from. KEXP gave these bag away at the top-donor level. 

John did a couple of pledge-drives where he sat and answered the phone. People would call in and say: ”I like do donate some money” - ”Hey, how is it going?” - ”Wow, is this John Roderick?” - ”Sure is! How can I take your credit card information?” They also gave that swag out to Rock musicians and everybody loved that bag because it was iconic of a time 2002/2003. It became Jason Finn’s signature bag. Recently John found a mint one in his collection and he traded it together with a steak dinner for this late 2012 iMac, but he should have gotten a 2016 computer for it, but John just wanted to see Jason's shining coat and his tail wag and his little wet nose. 

+ John upgrading his operating system to Mojave (RL311)

The other day John's computer started to tell him to upgrade its operating system, but John said ”No! [[[Leave it!]]] You are in my house!” The computer replied ”I am connected to the Internet, which means it is not your house, but it is our house!” - ”Who is ’our’?” - ”Don’t worry your little head about that. Upgrade my operating system!” - ”I don’t want to”, but it wouldn’t relent. John asked online if he could upgrade the operating system of a late 2012 iMac and the online said ”Yes! That is the earliest computer that will accept Mojave version 10.14” - ”Are you sure about this?” - ”Absofuckinglutely!” John asked the computer ”This isn’t one of those... you are not gonna... this isn’t one of those?” and the computer said ”It’s fine!” 

Merlin asked if the computer was squarish on the sides or tapered on the sides and fat in the middle of the back. John's new computer from Jason Finn is fat in the back with slender sides, like Kim Kardashian, and it can pop a champagne cork that will be flying up in the air before landing on its butt. The old computer from Anna Banana is square on the sides, but it still has a little bit of a fat butt, while the one from Jason is slim with a very sleek black border. When it is off it looks like the whole thing is a screen. 

John upgraded the operating system to Mojave 10.14 and it now looks like it is from the dark side with everything being black and grey, sleek and slick and the pixels are not aggravating his rods and cones. Every time he clicks on something the computer thinks about it for a while, which is not what he wants. Like when a doctor says ”Hmmm” and takes 5 minutes to think, it is not good, or when you ask a girl ”Do I have a good butt?” and the girl’s like ”Well...”, that is the wrong answer! Girls don’t think that boys care and most boys don’t care because they don’t have to look at it. 

John gets beachballs and he can see the computer thinking about the thing while it is in motion to do it, but you are not going to catch a football by stopping and thinking about it! You are going to have to run, look over your shoulder and the football is going to be where you were hoping it was going to be. Merlin started to watch basketball //(he says basticball)//. As John clicked on Safari it popped right up because he was just in the middle of leaving a comment on a guy’s Facebook page about an Iroc-Z when Merlin called him. Of course now that he wants to show Merlin the problem it all works fine. Opening Mail took 20 seconds which was a lot longer than it ought to have, but Merlin says that Mail is a bad app. Beachballs are often an indication that the RAM is constrained in some way, but they would have to run this by their technologist in residence John Siracusa. 

Merlin asks John to open Activity Monitor and they spend some time going through John’s processes, but they couldn't find anything strange except that Facebook was taking up 500 MB of John's memory. Merlin suggests to check if John has any problems with his hard disk, which is a very exciting thing to do. You can go to single user mode after you hear the ding at startup by holding down CMD+S and it brings up the terminal with all those little letters that looks like MS DOS and John can run fschk and it will detect whether there are any big problems with John’s disk, but it would not be advisable to do during the show because they have already helped a lot of people. With Opt+CMD+Esc you can see if any app is red, which means ”Not responding”. 

+ Trying to get stuff off his old computer from Anna Banana (RL311)

John bought his old computer from Anna Banana in 2005. It was a used machine probably from 2002 and it is the one he has used for all his podcasting, the one he took down to his office in 2014 where you could hear the seabirds, and it was the computer he did all his campaign on. It had all the things on it! John refused to upgrade the Operating System for obvious reasons because //he// was going to tell the computer when it was time to upgrade. John had said that on the podcast a couple of times, but Merlin told him not to say that on the air because he was exposing his vulnerabilities and people will hack him (see RL134). Also Matt Haughey sent him something. 

John didn’t upgrade for so long that all of a sudden the Google and the other things were saying that they were no longer going to service him. There was no longer a way to upgrade Flash and when he went on websites, their popup-advertisements wouldn’t play which was really depressing. John didn’t mind not having Flash, but Dropbox would drop and hot-box him as well. Many things didn’t work anymore and pretty soon he had gone across the threshold where he couldn’t interact with anything anymore. It certainly couldn’t take the new Operating System.

When John cancelled his office recently, he brought the Anna Banana computer home and put it on a desk where it sat for 5 months until he realized it had a lot of stuff on it that he hadn’t gotten off. There are a lot of Garageband files and Merlin tells him to CTRL+click on them to see what is inside, which is WAVs and DATA. 

John didn’t want to put it into target disk mode, because ”Come on! Amirite?” and so he hooked up both computers next to each other which looked like a battle station. He could have played some Sgt. Rock on the web and made $1 million because people wanted to watch him just play video games. John had too screens up, his headset on, his VR goggles, his Imogen Heap gloves and bullets were flying by him in super-slowmo while he got a cape on and was bending way back like you couldn’t even do.

John knew about a mode to make the computers friends, which is called Friend Mode, or Share with Friend Mode, but then the Anna Banana computer told him to upgrade the operating system and John replied ”You know what? Fine!” How many bad decisions has he made in his life where he started out saying ”You know what? Fine!” John has never been married, but that doesn’t mean that he won’t eventually go ”You know what? Fine!” and that is usually when his truck catches on fire //(see RL270)// or when he ends up in a relationship with a lawyer //(Millennial Girlfriend was a lawyer)// or something bad happens.

The computer upgraded its operating system only to the level it was capable of, which John didn’t know was a thing, and it turned out it was the oldest model that can still communicate with the computers from now. John figured that he was in the last days of a dying era and he could still get stuff off the Anna Banana computer, but not for long. One of those days it is going to say that they no longer service him and that they no longer could adjust those ski bindings //(see story in this episode)//.

The problem was that John only had one mouse and he had to keep unplugging the mouse and move it between the two computers, which was pretty savage! When he went to the thrift store to buy a second mouse he didn’t like any of them because they were all from HP or something. John's mouse only has one button and it cannot right-click, but he knows how to Ctrl+click and does that all the time.

John was switching the mouse back and forth between the two computers to put them both into Friend Mode. The Jason Finn computer was like ”Let’s get into Friend mode!”, but the Anna Banana computer asked him for his system administrator password, which is not the same as the normal password. One of the system admins was hotdogsladies (Merlin's online handle) because when John first got this computer, he put his laptop that they had bought at the University of San Francisco //(see RL215)// in Target Disk Mode and that user go imported. Merlin’s legacy is alive but he still only listened to the first 10 seconds of all of John’s songs. 

John tried to put in several passwords into the Anna Banana computer that he assumed were the password from that time, like ”Carpart5”, but none of them worked. It then threw up a clue, a cryptic sentence that John recognized as something he would have written, but it gave him absolutely no insight because it said it was the same password as for this other thing. 

At that point, The Goog jumped into the picture and asked //(singing)// ”Do you wanna sync me there?”, but John doesn’t know right now why the Jason Finn computer has to be working hard in order for the Anna Banana computer to be syncing stuff to the cloud. John just selected some things to test it, but unfortunately now the computer is syncing all kinds of shit that John didn’t ask it to. 

Merlin suggests John to transfer over the files from his old computer using a Flash drive. People give John USBs all the time and he almost never plugs them in, which is best practice, because he does not want to get hacked. It hadn’t occurred to him to use a USB dongle, but he is going to find one and he is going to try it. 

At the same time John is worried that Google will want him to upgrade because he put up 100 gigawatts and requires him to pay $30 a month. John should continue trying with the Friend Mode while also exploring the possibility with the USB drive at the same time, it is called parallel processing, like grating the cheese while you are boiling the water: You do two things at the same time. There is no such thing as multitasking!

On a side note, John's Facebook password is one of those very old passwords with an email-address he doesn’t use anymore, but it doesn’t matter because it is always logged in. Who knows, maybe he will never get back in if it would be logged out? He also doesn’t know how to log into his MySpace page to change his Top 5. 

The Jason Finn computer popped up a thing ”Update Apple ID settings. Some account services will not be available until you sign in again” and it offered ”View”, but as John signed in with his Apple ID it went away. It also got the Apple computer picture of the square face with the Picasso nose and the smile.

+ Fancy new Apple products (RL311)

John wonders what it would take for him to just be one of those people who goes to the Mac store, picks the biggest, coolest new thing and just buys it. There are rich people who do that all the time! It would not obviate his Anna Banana problem, though. John would get a real workhorse, with 5 Quadcores, or the one that looked like some kind of appliance, like a blurb. There was one that looked like a Haldron Accelerator or a box of Quaker Oats or a lamp. 

It was a quadruple Quadcore with one of those big screens where you wonder if this is as good as a TV and they say it is better, but you can’t really watch TV on it. If John had a screen this big, he could be editing video and put them up on the web page he designed with the built-in webpage-put-up software. It would be one of these blogs with a lot of video content where you have to watch a 2 minute long video when you have a simple question that they could explain it in a sentence fragment. 

Now that John has two computers, can he have them mining BitCoins? What if he gets the big Quadcore Quad and keeps his computers in a different room, puts things on the screen that look like it is computing something big time and if anybody will go in that room they will wonder what the fuck is going on, but behind the scenes it will be running coins. John’s electric bill will go up substantially, but he could get a coin out of it which he could reinvest into the new computer.

+ John taking his lawnmower engine apart (RL311)

John was having some problems with his lawnmower and [https://twitter.com/CutForTime Adam Pranica] who happened to be at John's house suggested that it might be the carburetor. Adam is a mechanical person who drives a BMW M-car that he works on himself. John was embarrassed for him to show up in an SUV, but he had put a [https://www.dinancars.com/product/engine-chip-dinan-performance-engine-chip/ Dinan-chip] in it, which must be from that whole Star Trek money because Adam is doing podcast for a living after having done internal videos for Boeing for a long time.

Adam called up a YouTube video about how to get to the carburetor of the Briggs and Stratton lawnmower engine and he wanted to stand there and watch John fix his lawnmower. John followed the instructions about how to find the Valve Gromit that goes to the Turbo Infabulator and now he was pot-committed because he had his lawnmower all taken apart. The guy suggested that John’s jets were probably clogged, but they were fine. 

John cleaned the battery terminals, he took the spark plug out and looked at it, he took everything apart while he was in there and it all looked fine. Eventually he put it back together and it ran fine because all it wanted was to be taken apart and having the Transporter Transfer Repair protocol done on it. The last time John had used the lawnmower, it sounded like the carburetor was too rich and it must have something to do with the jets. It is [https://en.wikipedia.org/wiki/Bennie_and_the_Jets Bennie And The Jets] and when you are a jet you are always a jet. ”From your first cigarette to your last dyin’ day.” //(lyrics of [http://www.metrolyrics.com/the-jet-song-west-side-story-lyrics-betty-buckley.html The Jet Song] by Betty Buckley).

+ John’s car repair experiences (RL311)

At one point John owned a Ford F250 with a GM 350 motor in it. This was not supposed to work but John bought the car up in Delta Junction, Alaska from a guy who told him that they make do with what they got up there. He had to re-jigger all the hoses and all the belts meaning that some belts took some interesting paths, but that is what you have to do to hook up the alternator with the synculator. The whole thing was Alaska-done and it worked fine, but it was going to surprise some people. The car had gotten a heart-transplant, you popped the hood open and people would take a step back or forward or to the side. 

When John brought that truck down to America it did not pass Emissions Controls tests, but John knew enough about a thing that he went to the Auto Parts store and said that the jets in this truck were too fat and he asked for some thinner jets. They don’t cost a lot of money and are really easy to install. It was all the car work John has ever done: Going to an old-fashioned Auto Parts store where a guy with a Jeff Skunk Baxter mustache said ”Sounds like what you got there is a clogged infabulator, here is the part and it is really easy”, basically a couple of Attaboys.

All other things John has done to cars, like change a starter, a water pump, or the jets in a transmission, were things where some guy with a handlebar mustache had told him ”Oh, it is not a problem, just go ahead and do it!” and John just did it. He never owned an impact wrench or any of those things, but he just got under there with his Craftsman tools. 

One time John attempted to fix the linkage on a [https://en.wiktionary.org/wiki/3-on-the-tree 3-on-the-tree] transmission of a 1968 Chevy C10 pickup and he broke the fuck out of that which was a bummer because he didn’t have any money. He also didn’t have the sense God gave a Turnip and so he just left that truck parked on the side of the road until the city came by and took it. 

John had never registered it on his name, which was smart except that the guy John bought it from for $50 ended up getting a letter from the city saying ”We impounded your truck!” and he came to John and ”What the hell?” and John was like ”Hey man, why don’t you just smoke a J and chill?” He thought that John was a dumb stoner, which was correct. He wasn’t wrong, the city wasn’t wrong, but John was wrong.

At a later point John was driving in his Uncle Cal’s 1968 280SL when the transmission linkage for that broke. It was an old Mercedes and John climbed underneath it and found the linkage because he already knew what to look for after he had previously screwed up the Chevy. His dad was with him, low and behold they had duct tape, and John wrapped it around the linkage 100 times which held long enough to get it into gear, drive down the road and take it to the Mercedes dealer. 

John’s uncle is the type of person who buys a brand-new computer that has the power and he is also the guy who takes his car to the dealer to be serviced. They fixed the linkage, went back to Uncle Cal and he said that this is the price of doing business when he lets his nephew drive the car. 

+ Thinking his starter was broken, but it was the battery terminals (RL311)

John used to go down to the Auto Parts store that was owned by two guys, one with a Jeff Skunk Baxter mustache who looked like Freewheelin Franklin Freak and his brother who looked like Fat Freddy Freak //(see RW80 in [[[Comics]]])//. John would talk to them all the time, even if he didn’t need an Auto Part because they knew so much. The store was right in the middle of Capitol Hill but has now has been converted into a hot dog restaurant where you pay $12 for a hot dog. 

One time the starter of John's car stopped working and he went down there and said ”My starter is busted” The guys said ”I bet it is not! What you need to do is clean the terminals on your battery” John looked at the battery, came back and said that it was fine, but the guy said ”Here is the thing about the terminals on your battery: You can look at it, but that doesn’t really tell you anything. You have to take the leads off, you have to scrape the terminals of the battery with some steel wool and it should work fine after you do that” 

Here was the real problem: John ignored him! Instead he got under the truck, took the starter out, took it down to the Auto Parts store, and said ”Here is the old starter” Freewheeling Franklin took it out of John’s hands, walked over to the part of the store that had a garage door, put it on the floor, put his foot on it to hold it down, took an electrical lead and touched it to the lead on the starter and the starter went ”Kapaaaw!”

This was the first time John had seen a starter in action: There is a rod with a gear on the end that it shoots out really hard while the gear spins and because there was nothing slowing it down it was operating in the free range. The reason why it makes that sound when your car is already running and you hit the key accidentally is because this thing shoots this horse penis out which is spinning really fast and hits a thing that is already spinning, it is an amazing thing! 

The guy looked at John and said ”The starter works fine. It did exactly what it was supposed to do. I’m not going to sell you a new starter. Go put your starter back in! Did you clean the battery terminals?” and John put the starter back in, it didn’t work, but John still didn’t believe it was the battery terminals because they looked fine.

He called the Auto Parts store, Freewheelin Franklin picked up the phone, and John said he still could not get this thing to work. The guy said ”I’ll be right over!”, he drove to John’s house, went over to John’s Ford F250, opened the hood and said ”Huh!” although he knew already that it had a Chevy motor because John had been buying parts like Caterpillar belts to fit onto this crazy thing. He pulled a wrench out of his pocket, took the lead off the battery, hit it with a little steel wool thing, put it back on and the truck started up like it was off the lot, like ”wroooom!” 

He gave John a look that was like: ”I hope... we have been through this together now, and I feel like my diagnostic skills are confirmed. I diagnosed your problem 2 days ago, sight unseen, you ignored me, now we both have learned something. I have learned that you are an idiot but that I am right, and you have learned that you are an idiot, but you are wrong.” and John was like ”I should pay you for your time”  and he said ”I don’t want your money” and he got into his truck and went back behind the counter in the Auto Parts store where he solved other people’s problems.

These are the kind of people we lost because they couldn’t afford to live up there anymore. Nobody had cars anymore because they were all on Segways and they turned the Auto Parts store into a hot dog stand. John doesn’t know where these two guys are living now. They were not old enough to retire and are probably building Harley Davidsons somewhere. 

+ Multitasking while walking his daughter to school (RL311)

Today Marlo’s mom wanted to walk to school with them. Marlo and John usually walk to school by themselves which can sometimes be frustrating because the next-door neighbors drive their girl Kayla to school in the Minivan and John’s little girl wonders why they have to walk while Kayla drives, but the thing is that walking is the superior format. Kayla’s trip to school is an MP3 while Marlo's trip to school is a WAV. Sometimes they will meet Flo on her morning constitutional and you are not going to meet Flo if you are in the Minivan. There are a lot of things you are not going to get!

This morning her mom didn’t have any meetings and wanted to walk with them, but the little baby in the story said ”No! That is not what we do, you don’t walk to school with us, Mama! It is just us” but John told her that we don’t treat people that way. As they were walking to school Marlo was explaining how TinTin took a rocket ship to the moon and John was listening to the story, but her mom pulled out her phone and was looking at it as they were walking. 

They were walking over fallen leaves down their little brisk morning path and as she got about halfway through her story Marlo said ”I don’t think mom is listening to my story”, John said ”Your mom is multitasking” and her mom said ”That’s right! I am multitasking!” Guess who doesn’t think multitasking is a thing? Papa John doesn’t think multitasking is a thing!

+ Papa John’s pizza (RL311)

The other day John was at a corporate event where they had the worst pizza John has ever had in his whole life. He will usually eat any pizza, even lunchroom pizza or truck stop pizza, but this pizza was as disgusting as sucking on a battery, like House Trotter //(see RL182)//, like Szechuan pizza except it was not spicy. It was Papa John’s pizza and it was garbage, like they had just put sauce on one of the boxes, which is an old joke. 

Merlin says that sometimes Papa John's is all that is available and people also make fun of an Applebee’s or a Ruby Tuesday, but John replied that Applebee's is not bad and Ruby Tuesday has got great rib tips. 

+ Going to Ruby Tuesday in Alabama (RL311)

One time John was in Tuscaloosa, Alabama on a late Sunday night. They were driving through and everything was closed, even the gas stations and the McDonalds and they had turned their street lights off because it was Sunday or because Jesus or who knows. They were all starving and they had many miles to go until they could sleep and as they came around a corner there was a Ruby Tuesday out by the Highway near to Louisiana. Nobody in the van complained. John also hadn’t let them pee in a long time and hadn't pulled over to get WiFi either. They pulled over, piled out, walked into the antechamber, this Ruby Tuesday was hopping, and some spunky little gal was ”Hi! Welcome to Ruby Tuesday! Come on in!” 

Every table was taken, people were laughing, the music was playing some jamming tunes, waitresses and waiters were hustling past, she placed them at a comfortably sized table and John ordered the big old Ruby Tuesday special platter. Their menu was 40 pages long and they had everything: the tips and the tots, corn bread, black eyed peas, John didn’t even know they had that stuff! This Ruby Tuesday was the greatest place John has ever been! 

They were having fun, they were laughing with the people at the tables around them, they ate all these foods and they were serving Arnold Palmer out of a pitcher! That is the place you want! John had a snobby attitude about Ruby Tuesday which probably extended to include other things like the state of Alabama and probably Louisiana, but he changed his whole story that day. The people in here were having the times of their lives! John doesn’t know how they vote politically and if they believe in Global Warming, but it didn't matter that night because they were having ribs!















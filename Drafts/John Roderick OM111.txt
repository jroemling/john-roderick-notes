OM111 - Tellin the Bees
2018-12-18

+ John’s dad in the hospital before his death(OM111)

When John’s dad was in the hospital, it became clear that he had [https://www.phrases.org.uk/meanings/319800.html shuffled off this mortal coil] in all ways except corporeally. John had long conversations with the doctors in the intensive care and they were extremely reticent to make any kind of definitive statement about whether or not he had a chance of recovery. John had to ask over and over again if there really was any chance that he will ever wake up and they just hemmed and hawed and equivocated. 

At one point John finally felt confident that his dad was gone and as his next of kin he was empowered to make the decision not to continue with this process, to tell the bees and not keep him hooked to the machines, although the doctor said that they could keep him alive in this state for a year. John's dad was 87 years old. he had multiple strokes, he had sepsis of the blood, there was a long period where there just wasn’t enough oxygen and they would only be keeping him alive.

John talked with them after the fact and they said that many people come into this situation feeling very confident that they are modern people who understand death and who are ready to make the hard decision, but they cannot say "It is over!" and they keep their loved one alive by these methods because they cannot in the crucial moment embrace death. 

John’s dad had a living will, but he put it in a file cabinet and didn’t tell anybody about it. John found it months after his dad had passed away as he was going through his things and it said ”Don’t revive me, don’t use any extraordinary methods to keep me alive, don’t intubate me” He had never told anybody he had written this, but it was just in a stack of mail. You should not only tell the bees, but also tell your son. 

Even as John’s dad got older he never thought he was going to die, and even when he was in the hospital you could still see it in his eyes that he thought he would live forever. In particular the secularizing of the modern world makes people less confident where they are going when they die. Thinking that you will certainly see them again on the other side or that you will meet your maker is exciting even, although you rarely see funerals in ancient traditions being celebrations of going through the portal, they are always sad. Unless you are martyring yourself, it is rare that you do it with your best shoes on. John would have loved to have told the bees, and was is not that they didn't have any bees around, but his dad’s death is the only real death John has ever confronted. 











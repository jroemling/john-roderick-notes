KN414 - John Roderick On Finding The ’Real America,’ And A Few Garbage Dumps, By Riding The Rails
2018-04-14

//This piece of audio originates from the Pacific Northwest radio station KNKX and is hosted by [https://twitter.com/gabrielspitzer Gabriel Spitzer].//

> John Roderick is the front man for the Indie band The Long Winters. Before he was a professional musician, Roderick was just a kid growing up in Seattle and Anchorage, a kid with dreams of wandering, breaking free, hitting the open country. Well, Roderick did live that dream in a way that was bold and fearless and occasionally clownishly inept. It all stemmed from a lifelong fascination that was first sparked in the middle of the city.

+ Holding up the train at Woodland Park Zoo (KN414)

When John was a little kid he would go to the Woodland Park Zoo where there was a little steam train you could ride complete with an engineer that went completely around the zoo //(see OM247)//. When he was 8 years old he started to hide in the bushes and wait for the train to come along and he would run along behind and jump on and hold on to the back of the train. It escalated and he started to have a cap gun and put a bandanna over his nose and run along behind the train and jump and say it was a train robbery. Adults in the 1970s were a lot more tolerant of that kind of unsupervised mischief.

+ Growing up in Alaska, far away from the rest of America (KN414)

Growing up in Alaska you are an American, but you are also thousands of miles away from America and they fetishized America. During the era of MTV John would watch music videos of Bruce Springsteen or John Cougar Mellencamp that were depicting an America of High School football games and a Midwestern cult during the 1980s which from Alaska looked as exotic as a movie about Europe. John imagined that a lot of wisdom could be found by traveling in America, hitchhiking, and going from state to state, which is part of the American mythos: You are a poor writer and you stick out your thumb and you find the real America or you find yourself.

+ Freight hopping from the Tacoma rail yard, pitching his tent on a landfill (KN414)

That John graduated from High School was almost a surprise. He expected that he wouldn't graduate. He got a one-way ticket to Seattle, came down with a friend, and they were going to set off across America. He was looking for being his most natural self uncompromised by the gradual chipping-away at one's authentic humanness by these small indignities, your grade cards, and your power bills, all this stuff that feel like indignities to a young person. He did want to achieve as close to a state of freedom as he could. They stuck their thumbs out and made it down to Tacoma.

They were sitting on the side of the road with their thumbs out right across the street from the Tacoma yard and were watching freight trains come and go. There was no fence between them and the rail yard, nothing was keeping them off those trains, and their minds were flooded with second hand memories of Hobos traveling across America, so they walked across the street and at first really tentatively hopped up on a boxcar. There was nobody there to see them.

It took them a while to get on a train that was actually doing something other than just moving around the yard, they spent a couple of hours just getting shuttled around from track to track, but eventually they got on a train that headed out of town and it was just rainbows because the train immediately goes somewhere where the roads don't. It was better than John had expected because it really was free, almost like riding a prehistorically-sized animal that no one else has the hutzpah to ride. 

When that first train came to the Columbia River in Vancouver, Washington it stopped and they panicked, jumped off, and the train took off and went across the bridge over to Oregon. In the middle of the night they crossed that railroad bridge over the Columbia, but there is no sidewalk it is just a railroad, and about halfway across that bridge another train came and they had to grab on to the girders of the bridge and swing their bodies out over the river and hold on because there wasn't room for them and the train to be on the bridge at the same time.

Later that night they realized it was too far to walk to Portland, they were exhausted, and they found a big field and pitched their little Boy Scout tent that they had brought from Alaska //(see RL25)//. When they woke up in the morning they realized they had pitched their tent in a landfill on a section that was covered with a layer of dirt. They were awoken by big bulldozers, moving fresh garbage around, and you can imagine what the bulldozer operators thought about this little pup tent and two teenagers poking their heads out in the early morning, surrounded literally by hot garbage.

+ Seeing a huge tire dump from the train (KN414)

John realized how big the country was and how little he knew about it. He had memorized all 50 states and capitals, but he couldn't picture it in his mind as he was embarking upon it. The next train he jumped on in Portland took him to Duluth, Minnesota, and it took three days. At one point where it was a garbage dump of old truck tires and car tires, but it seemed like the tires went on these rolling hills as far as the eye could see in every direction //(see OM130)//.

Growing up in Alaska he couldn't even have imagined such a place existed. This was a place where tires had caught fire and were smoldering for some places where they could never get to the bottom of the tire pile enough to put them out. It was almost like Mordor. John did get to places where there were hay bales and windmills and High School football games, and it exceeded his expectations. America is bigger and more beautiful and more mysterious.

+ Being fearless about trains in his youth (KN414)

A few years later he was in the freight yard down by Georgetown and there were trains getting set up. He went down into the yard just to be close to them and feel their energy. They are terrifying! A freight train in the middle of the night? They are enormous and really scary. John was in his late 20s by that point, but he couldn't recapture what he had been thinking, what kind of fearlessness he must have had. It isn't just that he is a homeowner now and a 50 year old man with a kid who has compromised all of his youthful ideals, but it feels like a different life.

> John Roderick lives in Seattle, where he makes music and podcasts, none of which are about trains.


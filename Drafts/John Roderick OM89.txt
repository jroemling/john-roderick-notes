OM89 - Blue Men of the Sahara
2018-10-02

This week, Ken and John talk about:
* Guitar picks ([[[Music]]])
* Christian Rock ([[[Music]]])
* 33 1/3 book series, The Pernice Brothers ([[[Music]]])
* John’s daughter is not allowed to date a skater ([[[Daughter]]])
* Getting tied to objects, being an animist ([[[Objects]]])
* John’s dad’s Shinto ancestor worship and Japanese friends ([[[Parents]]])
* Ken having a lot of things ([[[Ken Jennings]]])
* Everyday Carry ([[[Objects]]])
* Nomadic lifestyle ([[[Factoids]]])
* John trying to cross the Sahara ([[[Stories]]])
* John visiting the county fair ([[[Currents]]])

John is still in the process of moving house.

Time is a flat circle and maybe the sounds they are making travel across time?

John got some books about Nazi tanks in Ken’s PO-box.

+ Guitar picks (OM89)

John has cupcake papers full of color-coded guitar picks sitting in front of them, which is a product of his daughter starting to exhibit the first signs that she is his psychological descendent. She expressed that his guitar picks weren’t arranged properly because they hadn’t been sorted by color and they had not been counted. She found a bunch of cupcake papers, put them all on a tray, sat John down on the floor and made him help her count and sort guitar picks by color, shape and size. Some of them are not categorized, like there is a Randy Bachman red-white-blue marbled patriotic flag one that she couldn’t figure out where to put, although she was otherwise pretty definitive about picking where things went. 

There is one that had been cut from a VISA card and Ken wonders if this is as good as a real pick and if the guitar pick industry is a sham and we should all just cut up our Albertsons cards. Billy Gibbons uses old Mexican Pesos and everybody got their own thing. For a while John thought he would be making picks out of credit cards, like the woman who showed up at the Oscars in the [https://en.wikipedia.org/wiki/American_Express_Gold_card_dress_of_Lizzy_Gardiner Amex Gold dress]. It is a wonderful subset of Rock ’n’ Roll clothes, some designer made a little black dress out of black guitar picks, just fastened them together, cut little holes in them and made little stitches. It is very sleek and very slinky, but John can’t imagine very comfortable. Also, you can only wear it once. Like Björk showing up again in the swan dress: "We get it Björk! You look like a dead swan." Unless you are a dedicated groupie, you see a different band night after night and show up backstage in your guitar pick dress and that band will be none the wiser, or Sixpence None The Richer, who knows?

John has a story about probably 20% of those guitar picks. Can he tell this scalloped yellow one apart from this yellow one with the Fender logo? The scalloped yellow he picked up off the stage after a Pop Will Eat Itself concert and he has spent years trying to figure out what all of those scallops were. It has 3 different edges like some kind of Ninja weapon and it is an example of the build-a-better-mouse-trap problem: A guitar pick is such a simple little heart-shaped thing that there is always somebody who thinks they can do a better job. 

+ Christian Rock (OM89)

Sixpence None The Richer is a Christian Rock band and the name is a reference to some C.S. Lewis quote about how Jesus is a lion and if you have sex in High School you are none the richer. Ken doesn’t remember. Their on-stage outfits are puffy sleeved dresses like a Prairie Grove Battlefield Reenactment and they are all wearing the Handmaid’s Tale outfit, even the men, which is super-weird. 

John posted a nice thing about The Sundaes on the internet and a lot of people were writing him back how influential The Sundaes had been on them in the late 1980s. It precipitated an entire side-train of thought about Sixpence None The Richer and whether or not they were a Sundaes rip-off band that smudged the legacy of jangle pop. It was an era where there was still a lot of suspicion about Jesus in Rock ’n’ Roll. People were very suspicious about U2 and were wondering if they were secretly singing about that One Love is like God’s Love and if they were trying to inflict their Catholicism on everybody.

+ 33 1/3 book series, The Pernice Brothers (OM89)

The 33 1/3 series of books are slim paperbacks, each about a different record. John has been meaning to write one for years and his album would be ZZ Top’s Eliminator. No-one has done ZZ Top yet and they hardly ever double up on artists, so John is still golden. A lot of John’s friends have written those books and he has quite a few around here. Sean Nelson's book on Joni Mitchell’s Court and Spark is fantastic, Colin Meloy did one on The Replacements’ Let It Be and Joe Pernice did one on The Smiths’ Meat Is Murder which is actually not a review of the album, but a short story. Joe Pernice is a poet and a novelist and he has the most soothing voice in Rock with such an languorous quality that Ken could just nod off listening to the Pernice Brothers, it is like a drug to him. 

The Pernice Brothers and Joe as a songwriter are a perfect example of a completely under-appreciated artist in their own time. His music lolls and captivates John and it always has, but Joe Pernice never ascended to the big time like so many of them. Listening to his music, you can’t wait to experience the pop splendor of this band, but they presented themselves as a very working class Massachusetts band like The Journeymen. They did not have any flash, but it was like they just took their tool belts off and picked up their guitars. People who were listening to their records all think they are going to see Belle & Sebastian, but instead it is all denim. 

They looked like the cast of This Old House, sitting on the edge of a girder high up above the city with their lunchpales, singing these songs. It was intentional on their part, because in that era you weren’t supposed to project a lot of glamour as an authentic artist. It was the Guided By Voices era, but at least their drunkenness made them seem authentic, even though you can’t drink that many beers on a girder because bringing a cooler up there is super-dangerous! They have a high school teacher as their front man who seems like he is going through a really bad divorce. He got eight Coronas in him and it is only second period!

+ John’s daughter is not allowed to date a skater (OM89)

With all those little plastic heart-shaped things John could have a different dress for every day of the week. He could dress Marlo in orange guitar picks on day, green the next, he has a whole lot of options, but he is not going to dress her like that because he doesn’t want her to be a Rock ’n’ Roll groupie and he has already forbade her from ever dating a skater. He knows he is just sealing his fate, because her little boyfriend right now, whom she went to preschool with and they keep saying they are going to get married, is already a skater. 

John could tell when he was 2 years old that he was a skater and he also told his mom, but she was like ”Don’t say that, I know!” He has the skater gene, he has floppy hair, they got him tested! In the future people will know in-utero if they have skater kids because they are going to be able to do a thrasheocentesis //(thrashing + amniocentesis)//. He was one of these kids that anytime there was a stair more than 2 steps high he would jump off of it and ollie even with no board. He had never even seen a skateboard, but John could tell he was a skater. Now he is 7 years old and John saw a video of him dropping in for the first time into a bowl.

+ Getting tied to objects, being an animist (OM89)

John has been living in his house for 11 years. He will get tied to things and souvenirs and he has an amazing chronological memory like Rain Man. He has a kind of animist internal life where objects are imbued with spirit. If you break a pair of his glasses, he will mourn the spirit of them. This wasn’t taught to him and none of his family members practice it. 

John probably thinks that this is one of his six other podcasts where he can just tell unrelated life stories until the other guy tells him to stop. This is not that show, but this is the one where they are talking about WWII movies! 

And that concludes: ”Guitar picks and Japanese basketball players” 

+ John’s dad’s Shinto ancestor worship and Japanese friends (OM89)

John’s dad practiced a kind of Shinto ancestor worship. He would not openly pray to his ancestors, but he would go to the cemetery and consult their gravestones. He grew up in Seattle pre-WWII and Japantown played a big role in his life because he went to Broadway High School like all of the kids from Japantown. They had a very active and culturally rich Japantown before the war, but during the war Japantown kind of moved. John's dad had a very traumatic experience when the Japanese were interred. A lot of them were his friends because he played basketball and in his retelling of Seattle in the 1930s the Japanese were the best basketball players in the city. At least at Broadway High School he felt like the Japanese basketball players were the ones who were the most competitive. He had a lot of Japanese friends. 

In Ken’s school the basketball team was entirely Korean. He lived in Korea, which did help, but they were mostly Korean-American kids and more Korean-American kids than white kids liked basketball. It is surprising how many tall Koreans there are, especially now that we gave them fast food and decided to put a lot of weird hormones in their milk. Now we don’t win the Olympics all the time anymore.

John’s dad was very influenced by Japanese culture growing up. It is commonplace in Japanese homes to light candles or incense sticks, to make offerings, and to put some crackers in front of the picture of great-grandpa. This culture really resonated with John’s dad, but he never directly tied it to his Japanese friends.

John had a funeral for him at the Washington Athletic Club after he died and as John was standing there, presiding over this event, he sees one after another these little old 89/90-year old Japanese men come filtering into the ceremony. They were all 5 feet tall and John has no idea how they would have been the best basketball players. They came out of nowhere and John had never met any of these guys, but they all told him how his dad was the greatest archer. He had never told John he was into archery!

+ Ken having a lot of things (OM89)

Ken also gets attached to things and he had older relatives with hoarder tendencies growing up, like his grandpa kept every Seattle times for the last 8 years of his life for some reason, but Ken himself is not a hoarder or somebody with 30 cats where the state Humane Society is barging in. 

Ken’s office is quite a temple. There are books, art, DVDs, comic books and figurines. He is not one of those guys with a ton of toys, but when he does get one of those from somewhere, he often sticks it on a shelf. He maybe doesn't literally have a ton, but a half-ton or a metric ton of toys! Ken has more LEGO in his basement than John has ever seen. Having kids is just a chance to exercise that kind of second childhood part of yourself and the juvenile part of your brain. Ken spent 3 times the amount of time playing with his kids’ LEGO when they were young than his kids did. 

This summer Ken and his family were in London and they found it to be a great place to live. It is a cultural capital of the world like New York but without all the New Yorkiness. Instead it has all this Londonness to supplant it, this fake polite British twee which is very impressive to Americans instead of ”Forget About It!” or that brusque New York thing. They thought they should be living in London a few months out of the year and Ken was super-excited about this plan at first, but then he realized that all his books and all his movies would be an ocean away and that was a heavy psychic toll on him. It is dumb because he can go to a library and check out these books, he could stream those movies, or he could even buy new copies of any of it. 

If he sits in his office surrounded by his book, it is pretty womb-like and he does feel embraced. He has a big Bob Crochet railroad desk and is peering at people like ”Mr Scrooge, just one more piece of coal please!” - ”Ken, will you get on this?” - ”I prefer not to!” Ken likes the womb-like quality and he is not a person who could take all his possessions and pack them up on the backs of two camels. Neither can John. 

+ Everyday Carry (OM89)

John is trying to maintain two separate states, one of them is being a collector who has all of the little picks and belt buckles that he ever touched collected and sorted, but there is another part of him that feels like he should be able to live out of one small bag. He is like Schrödinger’s hoarder. Until you open the coat, you don’t know if it is crammed with guitar picks or if it is just John and one Bonsai tree. It could be John’s Technicolor Dreamcoat //(there is a similarly titled episode RL189)//.

It became a trope on Roderick on the Line that John always kept a small bag packed. There is a culture of Everyday Carry, not like a gun, but a culture of survivalists who have refined their bug-out bag, a culture of people who are ordering and sorting and finding the exact best things they can keep on their person: Knives, a little fish-hook with wire, or having their belt be a braided cord that they could use to span a river. They keep the tools they would need on their person in case something bad happened in this moment and they had to leave with nothing. 

It is the pleasure of thinking of imaginary scenarios that are more interesting than anything you are actually doing. Every day you get on the train to work and you know that if everything goes crazy you will be able to catch a fish and no-one else will. This one paperclip can be bend into a hook and there are so many things it can do. If you google EDC you will find are a lot of these potential nomads and John thinks of himself as one. If everything burned down, he has already hardened his heart to the idea that he has lost everything. 

Ken kind of wants to be at ground zero. He does not want to live in the rubble, because he would be very ill-equipped to survive and he would have lost all of his Marvel comic-books. Ken certainly has the world’s largest X-men collection and he would immediately be helpless because there would immediately be 60 things he did not have and would not know how to do. He is just not cut out for the nomadic lifestyle. John Hodgman hordes inhalers just in case, because he has quite severe asthma and the idea of being without an inhaler causes him a lot of anxiety and so he keeps them in his bunker.

+ Nomadic lifestyle (OM89)

Few people are cut out for the nomadic lifestyle! We think of our closely built permanent communities as an innovation of modern life. They allow the building of any kind of infrastructure and any kind of technological development which is impossible if you are thinking every day that you need to get the goats to a place with a little more rain or to a valley where they haven’t eaten all the thorny Sorghum yet. Ken would be eating all the Millet because that is the good stuff: Millet and camel milk! 

There are not a lot of nomadic people left in the world, but there are many people like Ken and John with a lot of accumulated stuff. One of the few remaining nomadic people live in the Western part of the Sahara dessert in countries like Mali, Niger, and the Southern parts of Algeria in a belt called the Sahel, which means ”shore”, because they think of the Sahara as a vast sandy sea and the Sahel is the border between the dry non-arable area and the Savannas of Africa to the South. It is an almost edenic, dry region that is really only suited to nomadic life. You can congregate around an Oasis, but you couldn’t survive as sedentary people, you have to keep moving. They continue to talk about the Tuareg people living in that area //(They were 37 minutes into the show when Ken first mentioned the subject)//.

+ John trying to cross the Sahara (OM89)

John has spent not a small amount of time in North Africa during multiple trips and he has been down in the Sahara where he has seen wild camels. Later he came from the South as well when he was in Niger. In Morocco he travelled down to the North edge of the Sahara, like the city of Ouarzazate and other little oases, but he realized he had gone too far. He tried to get across the Sahara when he was a young man, assuming he could do it although he had never really looked at a map of Africa. 

He set off from Casablanca and thought that a good adventure would be to hitchhike from Casablanca to Lake Victoria. What fun this would be! He didn’t understand the distances because they don’t teach geography in schools anymore and everyone says ”It is just state capitals”, but it almost killed beloved Indie Rocker John Roderick. He hitchhiked all the way down to the ports or the border towns on the North end of the Sahara and past these points there is nothing until literally Timbuktu. 

He stood on the edge of the road with his pack on his back and his thumb out for a couple of days, watching these enormous trucks come by that were built to cross the Sahara with big tires because the road just turns into a sand path. There is a path, but the desert is nomadic as well and is wandering around. The truckers were all literally pointing at John laughing! Eventually he was so discouraged by the fact that he was being laughed at by these truckers that he found a bar in a hotel, kind of a Moroccan truck stop, he sat at the bar drinking tea, got into conversation with some guys, and told them he had a really hard time hitchhiking across the Sahara and they all laughed. He spoke French, enough to talk, and they told him that he was an idiot!

These drivers have just enough water, food and supplies to get themselves across this incredibly inhospitable and many-day long trip and they are not going to bring John because he is not going to be fun to them, he had no supplies and he looked like trouble. They were laughing at John because this was the most idiotic thing they have ever heard. John had to turn around and head the other way with his tail between his legs. It was very hard to hitchhike the other direction! 

John never got to Lake Victoria to this day. He ended up in Algeria for a day, but that was at a time when a lot of French tourists were being murdered in Algeria, so he was dissuaded from going any further. John was an idiot, a completely ignorant dingeling! This all began when John was 20 years old and he forgives himself. He went to Niger when he was in his 40s //(see USO-tour in [[[Shows and Events]]])// and saw Tuareg people. There are more Tuareg in Niger than anywhere else.

+ John visiting the county fair (OM89)

John was at the county fair recently and saw kids riding sheep, which is very Tuareg. He saw lots of jars full of beans and he took his daughter on her first big rollercoaster. His mom once won a blue ribbon on the Ohio state fair for her 4-H display of all the fresh vegetables, beans and punkins [sic] (she does not put the second ”p” in pumpkins). 














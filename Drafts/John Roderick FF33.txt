FF33 - Star Wars
2018-08-24

Look: We know! You the listeners probably do not want this. I agree with you. I didn’t want to do it either and I’m hard-pressed to claim that this episode is going to make an impact on the global Star War critical industrial complex, but if Friendly Fire has one thing abundantly clear at this point, it is not just that we aren’t especially responsive to fan feedback regarding our choices, it is that we are still trying to decide what exactly constitutes a war film. And dammit, this one has ”Wars” right in the title. All the great sub-genres of war film: Bootcamp film, submarine film, heist film, beach make-out film, they have all had to make their case for their rightful place in the war movie pantheon. But what about space war? 

I know you nerds believe space war is real because I have been to like 5 ComicCons and I see how your eyes shine when Adam Savage walks by dressed as Chewbacca. I also read just enough of the fan mail for this show to know that you are all clamoring for us to do Starship Troopers. You just didn’t expect that we would do Star Wars and just drop it into our normal feed. Well: Here we are!

In the 1970s, when his film school contemporaries were working on big movies about the Mob or Archeology and Shark Attacks and Vietnam, George Lucas was writing about a made-up war between a powerful Galactic Empire and a group of hardscrabble rebels and muppets and space farmers. Coppola was over redefining film with Brando and De Niro, while Lucas explored the gibberish mystical belief system that maybe was galactic unitarianism or maybe was some kind of midichlorian, which isn’t a thing. Who knows! Can you imagine him describing it to John Milius, while Milius was sitting there eating a Snickers bar with a knife or whatever? He must have laughed him out of the room! Can you imagine being friends with John Milius? There is something deeply wrong with all these people! 

Anyway: Star Wars, which was later called Star Wars Episode IV, A New Hope, which was some expression of super-weird personal revisionism and cultural gaslighting on the part of Lucas which I relate directly to the insane way he trims his beard as though he is sculpting a chin for himself out of hair. Anyway, this New Hope business, which was only embraced by the Stockholm Syndrome adult Star Wars fan community who I also think are accepting this mididchlorian horsefeathers, and also by people born in the 1990s whom I can’t defend or even understand, but this is your basic outnumbered, out-gunned liberal democratic underdog with a secret weapon against a big bad black clad empire war film: There is ship-to-ship combat like in Master and Commander, there is sword fighting like in Braveheart and a little romance sprinkled in for good measure like From Here to Eternity. Of course it is a war movie! 

The first 2,5 Star Wars films are good war movies, before Lucas like Lennie from Of Mice and Men petted his bunnies to death with his grubby CGI-fingers. I can’t really speak to the rest of the canon whether it is good war movies or not, because I started to get confused around the time that guy from Girls showed up. Anyway, I don’t need to summarize this film, it is in our collective midichlorians, but it is hard not to get chills at that cold opening, and remember when this future was all so new, before it became an evil empire all its own. For so many people this is where not only a love of Star Wars, it is where a love of movies started. This may not be the podcast you are looking for, so move along as we discuss George Lucas’ 1977 space opera Star Wars!












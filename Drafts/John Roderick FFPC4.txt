FFPC4 - Triple Frontier

//Intro by Ben Harrison//

Heist movies: In addition to con-man movies and submarine movies, the heist is in my holy trinity of film genres. I just love the puzzle of: How do you break in somewhere, steal a huge amount of cash, and then how do you get away with it? It is a formula, but one for which I am a total sucker. We don't pick our donor bonus movies randomly. Usually on the text-thread I toss out one or two options and let the guys pick, but this time I just announced we were watching Triple Frontier, a 2019 Netflix exclusive directed by J.C. Chandor (Hope I'm pronouncing that name right).

I really liked Chandor's previous film A Most Violent Year, which starred Oscar Isaac as an enterprising immigrant building a fuel oil empire in 1981, New York City's record setting year for violence and crime. Chandor directs really nuanced sensitive performances and he uses violence in a much more interesting way than your average pork chopper, so I was pretty excited to see what he and Oscar Isaac did next.

John and Adam, having seen all the dumb promos that Netflix forced down our throats in the couple of weeks after this movie came out, were skeptical. The story in the film is dead simple: Five dudes who used to be Delta Force bad asses realize they have a skill set that makes them just about perfect to pull off a heist against a Colombian drug lord. They are going to break in, kill the kingpin, pack a van full of money, and fly it out over the Andes mountains to a waiting boat, deposit the money into a bank in an offshore tax haven, and then live like kings for the rest of their lives.

Of course the dead-simple plan hits some obligatory snags or we wouldn't have a movie on our hands, but this film isn't as interested in the what of the snags as the why: What forces pushed these guys into committing murder for self-enrichment? What is happening in their heads to push back against this impulse? There are great performances by Ben Affleck, Pedro Pascal, Charlie Hunnam, and Garrett Hedlund who round out the squad and this movie winds up being better than it has to be. Maybe it had bad marketing or maybe the fact that it is more or less direct to Netflix makes it seem like a B-movie?

John and Adam resisted putting this on the list and - Spoiler Alert! -  they were wrong! It is not quite a war movie, but it is a really fun heist movie and all the main guys are wrestling with their place in the world after retiring from the military. The movie respects that and takes it seriously in a way that we all found surprising. The effects of committing extreme violence on other human beings are biological and physiological. That is the price of being a warrior. Today on Friendly Fire: Triple Frontier.


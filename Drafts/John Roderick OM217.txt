OM217 - Christmas Pickle
2019-12-24

This week, Ken and John talk about: 

John had never heard of the Christmas Pickle and claimed it was not a thing, which is true, and yet it is a thing, making it the Schroedinger’s ornament.

John’s family will serve creamed onions on Thanksgiving, but nobody eats them except they take one spoon full of desultory creamed onions. The Christmas Pickle is not edible.

+ The nature of Christmas (OM217)

Christmas is a confluence of a pagan winter solstice festival and the alleged birth of Jesus, which John was just explaining to his daughter. In the Northwest it starts to get super-dark at what should be a reasonable hour. At 4:30pm John still has stuff to do and wants to be alive, but the sky is saying: ”No, it is time to die!” Christmas is jumping on the New Year train and is co-opting New Year because we don’t know when Jesus was born. The shepherds weren’t probably out with their sheep on December 25th. Through a weird coincidence of cultural factors Christmas is the highlight of the year, probably built around gift-giving and the impact it has on children.

+ John’s Christmas tree traditions (OM217)

John’s family has gone through many phases of Christmas trees: All through the 1970s and 80s into the 90s they had a real tree, but after John’s mom retired she went through a little-old-lady-white-fake-tree-with-pink-decorations phase. John’s sister Susan is very Christmas positive while John and their mom are very Christmas neutral. When Susan lived in California John and his mom looked at each other and said: ”Wait, we don’t have to do this!” and they started celebrating Jewish Christmas, which is to go to the movies and then have Chinese food.

All the Rock people were at the Chinese restaurant Shanghai Garden and it was amazing! John would stop at every table: ”Hey man! How is it going?” They had a great 5-year run, but then Susan moved back to town and re-instituted her Christmas regime. Since John’s daughter was born they are 100% back to a real Noble Fir in the house that is decorated with all of their family ornaments.

John gets a Charlie Brown Christmas tree for his house, a little real tree that is decorated in an intentionally sad dad’s tree way. Then they pick one of the gathering-houses, either John’s mom’s or John’s daughter’s mother’s or John’s daughter's mother’s parents house, where they get a real tree and where they are actually going to spend Christmas morning.

+ John’s mom sending solicitations for money back to the senders (OM217)

John’s mom gives to charities, but the side-effect is that she is inundated with junk mail that she usually immediately throws into the recycling. She just came back from a trip and had her mail held during the trip. Her car is broken and she had to go to the central post office where they had a bin of mail for her from everybody under the sun wanting her money. She was offended by it in its collected mass and had to schlepp this bin of mail home.

She is in her mid-80s and had just brought a backpack to the post office to get her mail and they made her look like Santa with a giant bag of solicitations. She opened them all, wrote on their literature: ”Take me off your mailing list, you sleazebags!”, stuffed their postage paid return envelopes full of their own literature and mailed it back to them at their cost. She was delighted in having culture jammed World Wildlife Fund and Doctors Without Borders, but at 85 she can be crazy if she wants.

+ Other

John is saving Shakespeare for the event he will have to spend time in prison.

John is playing the harmonica, but he is not a good harmonica player.

Ken was having a Christmas pickle on his tree when he was a kid in Korea.

John was in Spokane in 1986.

John wants Turkey Dinner once a week all year long, and although he is an adult and could have that he doesn’t want to cook a turkey and stuffing and mashed potatoes every week.























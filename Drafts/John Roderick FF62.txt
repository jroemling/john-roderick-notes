FF62 - Memphis Belle

//Intro by Adam Pranica//

You want to know how many World War II movies there are in the world? What if I told you that 1990 produced a World War II film with a $23 million budget starring Matthew Modine, Eric Stoltz, Sean Astin, Billy Zane, Harry Connick Jr., David Strathairn and John Lithgow? You have probably never even heard of it! The Memphis Belle was a real B-17 Flying Fortress that was successful enough in the air war over Germany that it had a propaganda documentary made about it. What we watched for today's episode though is a fictional story that does away with almost every detail of the real history.

The crew is different, the circumstances are different, the only thing unchanged is that the crews of these incredibly dangerous missions were given a quota: survive 25 bombing runs and you get to go home! So we start this film on the eve of the Belle's 25th mission and what we get is a straight-down-the-middle-story about that mission, not the real mission mind you, but a semi-plausible bombing mission. I say ”semi” because a million pedants on the Internet have quibbled with just about every aspect of the mission that is depicted in this film, but this is the narrative motion picture, not the newsreel documentary. 

It is a weird movie because it has a hard time finding a bad guy. Sure, there are Germans, but they are mostly represented by [https://www.dictionary.com/browse/ack-ack ack-ack] or the occasional harassing fighter plane, but they are not personified or even particularly vilified. They are just the other team. There is John Lithgow’s character, a lieutenant colonel who wants to take the crew back stateside after their bombing run to promote war bonds and get famous. He is not so much a villain as a guy who puts his foot in his mouth, though. I understand that completely. There are the generals calling David Strathairn to give the Belle and the rest of the wing a particularly dangerous mission, but he doesn't qualify because those guys are just trying to win a war. 

Instead this very small story is focused almost entirely on the 10 men in the airplane. The relationships between them are strong and lived-in and the tensions feel like they have been cultivated over 24 previous missions. It is a film that almost lives outside the era in which it was made. It has the feeling of something that could have been made at any point from 1945 onward with only the special effects being handled differently, and maybe that is why it came and went unremarked upon. 

But we are not leaving this film behind! We are going to see it through its straightforward, almost self-consciously generic retelling of a story that so many air crews lived through, and you bet your buttons me and John are going to wax poetic about airplanes. And if he is lucky we might even get Ben involved. If we don't drop these bombs right in the pickle-barrel, there are going to be a lot of innocent people killed today on Friendly Fire as we discuss Memphis Belle.

... who’s hosts are either in shock or covered in tomato soup.


OM386 - The Mpemba Effect
2021-08-19

This week, Ken and John talk about:

+ John's and Ken's history with ice skating (OM386)

Ken has tried ice skating and it struck him as something that might turn fun the 10th time you do it, but he never made it to 10. He was already adult-sized and he would either fall or his ankles would feel terrible or both. His kids have both ice skated and liked it a bit more than he does. Ken doesn’t remember there to be skating rinks in Korea, although it is a cold country. None of his friends went ice skating. They did have frozen ponds, but they didn’t go out on them.

Ice skating was never fun for John either. In grade school in Alaska you were either an ice skater or a skier and although there were plenty of people who could do both you picked a track and John was a skier. The skaters were all touch hockey kids, which is not a sport for poor kids, and at John’s High School Hockey was more popular than Football. All the clichés of Nebraska High School Football were true in Anchorage of Hockey. They did have a Football team and they were state champs, but the Hockey team was where the heart and soul was.

Some of them drove jacked-up pickup-trucks, wore acid-washed jeans and grey cowboy boots, and had mullets. There were Hockey cheerleaders. Today only basketball and college football have cheerleaders, except for the Dallas Cowboys cheerleaders. In the 1970s there were Dallas Cowboys calendars and the TV movies where they went on adventures.

When John was 12 years old he did have a Dallas Cowboys Cheerleader poster in his closet door that he had found at the Hockey Rink at the Sullivan Arena where it had been thumb-tucked at a bulletin board. He took it, rolled it up, and snug it out of there like he had found a Playboy in the woods. It is quite a high level of teen horny, although John was a pretty good kid in the sex department and didn’t have a girlfriend or anything. He was pretty good until he was 17!

John spent a lot of time at the ice rink, even though he wasn’t much of a skater and had chosen the ski path. Watching hockey games was part of school spirit, and he knew the hockey players, but he did not like them socially. They were on opposite sides of the ”Have you read this book?” divide. He didn’t understand hockey very well, which might still be true of many people watching hockey today. He also didn’t care, it was much more important to him to hang out with his friends.

+ John having a Rap group in High School called White Fresh Crüe (OM386)

The first time John ever heard of the Beastie Boys album Licensed to Ill, which came out the year after he graduated from High School, was at an alumni hockey game. In High School he had a Rap group with two friends, called White Fresh Crüe //(see RL196)//. They would go beatboxing in The Fat Boys style and freestyle to it in the basement of one of the houses.

There is a cassette of that, but John is not going to drop a sample in here. They would rap and walk in a circle, which they thought was very innovative, maybe they were the first white Rap group, but then the Beastie Boys came out and it blew their minds because their stuff was so much better. John had been fighting for his right to party for years!

In the mid-1980s John tried to breakdance and he would still do it if anybody asked him to. In his 20s he was living in a house where he was the only white guy and freestyling in the living room was a big part of their apartment culture. People would come over, there might be 10 people in the living room, and everybody would be freestyling. John was the white kid in the house and he was disinvited from freestyling, but he would contribute to the beatboxing.

+ Ice rinks, being fascinated by the Zamboni, John’s neighborhood in Anchorage near Lake Otis (OM386)

At hockey games John is as fascinated by the Zamboni as he is by the game, which is true for Ken as well. It is wonderful and hypnotic, maybe it is an OCD thing, but this beautiful machine transforming the ice into a flawless surface is so satisfying. It is like mowing the lawn, but with a giant magic tractor mower where it is not clear how it is doing it.

Ken didn’t know how it works until he was an adult, he thought it was shaving the ice smooth like you would sand wood. The first time he saw one that was not on TV was at minor league hockey games in Salt Lake City when he lived there. TV would often cut away from it, not understanding its allure.

One time John went to a Seattle Thunderbirds game at the Key Arena, formerly the Colosseum which is now the Climate Pledge Arena because it is sponsored by a furniture polish called Climate Pledge. It was a double header and as part of the enticement to get the audience to stay for the second game the Zamboni came out pulling a stage with the Spin Doctors on it and they did a 5-song set, like Little Miss Can’t Be Wrong, which implies that they have 5 songs. There is Two Princes and Jimmy Olson’s Blues, the album is called Pocket Full of Kryptonite. They have records all the way into 2013!

When John lived in Anchorage they had a house near Lake Otis, a beautiful little lake that was in the process of returning to being a swamp, but it had a neighborhood built around it called College Village with streets on all 4 sides named after Ivy League colleges. Princeton and Stanford were the two main streets, John’s friend Kevin lived on Duke Drive, and John lived at the corner of Princeton Way and Stanford Drive, which meant they were cornered off and didn’t have a waterfront property while their neighbors to the left and the ones two doors down on the right had access to the lake.

John’s neighbors down the block, the Cusak family who produced at least one competitive hockey player, built a mobile hockey rink on Lake Otis with walls that they had in their shed and that they wheeled out on the lake. It even had lights and they had their own little tractor with the equipment to zamboni their own hockey rink.

The Zamboin is an ice resurfacer by a company founded by Frank Zamboni and in the 1950s it was the first of its kind. The machine had a multi-step process: It would shave the ice, wash the ice, and squeegee the ice, and the washing would melt the top of the ice and fill in the crevices and immediately refreeze.

+ John’s company setup (OM386)

John’s music production company is called Beats Working Music, but on his cheques it says The Roderick Group, which is the umbrella organization. Like the Kentucky Colonels he hoped that he could just charge people to have a title and he could have 25 employees, but it turned out he wanted people to do work for him. Hiring is hard and John once asked an assistant to help him hire someone and she hired herself, like Dick Cheney (see RL94). It didn’t end up working out.

+ Celsius vs Fahrenheit (OM386)

Both John and Ken prefer Fahrenheit over Celsius because Celsius does not have enough granularity for the narrow range of human life and comfort, although it is better for scientific application like measuring the temperature on Jupiter. You can feel every change in a Fahrenheit degree, and every change in a Celsius degree is 2-3 of those and you are going to need a decimal point to express what your preferred indoor temperature is, whereas in Fahrenheit 71 is a little different than 72 and you can feel it.

+* Others

There is a lot of booing in baseball these days and John is offended by it. 

”We sow the seed, nature grows the seed, then we eat the seed” (from The Young Ones) Ken can quote every line of every movie he saw before 1990 and he can’t quote a single line of any movie he saw after 1990.

John is friends with Denis Johnson, the author of the 1990s book [https://www.amazon.com/Jesus-Son-Stories-Denis-Johnson/dp/031242874X Jesus’ Son] that had a profound effect on him. It was made into a film starring the extremely beautiful Billy Crudup. Denis Johnson is not the NBA Finals MVP Supersonic legend Dennis Johnson with two ”n”.

Lately John has been fighting sugar ants. For years he subscribed to the belief that you don’t fight them by squishing them, but lately he is just so frustrated with them that when he sees them he will squish them.
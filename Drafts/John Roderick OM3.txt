OM3 - The Olympic Marathon of 1904
2017-12-07

+ John finishing a ski race in the wrong heat (OM3)

In High School, John was in the cross-country ski team. At least in Alaska, the cross-country skiers were typically not the snow-mobilers, but they were the wool-sweater-wearing type. Cross-country skiing is a preppy environmentalist style, a leftist liberal kind of cultural thing. Even between cross-country skiers and downhill skiers there is a political divide: The cross-country skiers are the Granola crunchers. John did not have the build nor the competitive spirit of a cross-country skier, but he was only on the team for the social aspect. He liked this one girl who was very preppy and very cross-country ski team. 

John was terrible at cross-country skiing and eventually the coach, John and everybody else on the team agreed that John would be better suited as the manager, which he had a great time doing. He would stand in a wooly sweater and as people would come by he would go ”Yeah! Pick it up! Good job!”, it was a great gig. 

At one time while John was still racing they started a big tournament where they went out into the forest and nobody could see where anybody else was. John got immediately left behind, skiing by himself because even the worst skiers from the other schools were completely out of sight. After a while he saw a bird’s nest in a tree. It was in the middle of winter, so it was an old nest and John stopped, climbed up, got the nest, and took it with him. 

Then he got lost. There were ski trails in every direction, it was a long 10K race way out in the boonies and John was picking his path, trying to stay on what he thought was the arterial, but he had missed a turn, he was tired and he didn’t like skiing. All of a sudden he heard skiing noises behind him and a guy zoomed past him, decked out in Spandex! They guy looked over his shoulder at John, like ”What are you doing here?” because John was not in a Spandex outfit, but in his jeans, which is the uniform if you are a real hot skier. 

Two more guys went past really quickly and John realized what had happened. As he was lumbering along, he came over a hill and saw the great big wide-open finish-area with the banners, the crowd and the finish line. John just started to pour it on and all of a sudden it was a race between John and the 4th guy just behind him. Both of them were kicking it into gear and it looked like a legitimate race to the finish. The other guy nosed ahead and got 4th and John came in 5th, but that was still within the ranks where your school will get points. 

The problem was that this was the second heat and John was in the first heat. Those guys in their Spandex were the top 4 finishers in the second heat and John had been out there long enough that they had forgotten about him. Nobody on his team could believe it how he had accomplished finishing 5th and it took about 20 minutes before somebody with a clipboard came over and told him that he had started in the first heat one hour and 40 minutes ago. John knew he was not in the lead, but when the spirit of competition came in him, when he saw the finish line, he didn’t pull over to the side and say ”No!”, but he raced! He had kept the bird's nest in his jacket until the end of the race.

















 
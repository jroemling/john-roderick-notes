FF104 - A Bridge Too Far
2020-01-03

//Intro by Ben Harrison//

According to his biographer Nigel Hamilton, Field Marshal Bernard Montgomery was a bit of a late bloomer. Toward the end of his 30s in 1925 Monty went on his first date with a lady, or more properly a girl, as Betty Anderson, the girl in question, was 17 years old at the time. He spent the day drawing diagrams in the sand, indicating how he would deploy troops and tanks on a battlefield in some war that might happen someday. Not really sure how it went back then, but apparently the date also included a marriage proposal and he got rejected. Bad strategy, Monty!

Today's film is another one of our bridge films, really three bridge films in one, and it is also one of our big ensemble films where a huge number of real-life officers and soldiers are played by some of the biggest stars of the silver screen from the UK, the US, and Germany, who could be enticed to participate in 1977 by the opportunity to work with each other and director Richard Attenborough.

Field Marshal Montgomery however is not depicted in the film, despite the fact that the entire plot surrounds an attempt to put one of his most ambitious war plans into action: Operation Market Garden is sold as a way to end the war by Christmas of 1944, and this film is first and foremost a look at why it didn't work.

The basics of the plan are that 35000 allied paratroopers are going to be flown behind enemy lines and dropped on a corridor of roads leading through the Netherlands and up into Germany. We are going to need to capture and ideally prevent the destruction of three bridges, so that a bunch of tanks and other Nazi killing-equipment can be driven up from allied bases and delivered across the Rhine and into the heart of Germany's industrial regions. Good plan, right?

Well, early in the film we get the sense that the radios for this mission are new and probably not great. We find out a lot of the heavier equipment that they wanted to drop for the paratroopers didn't make it, owing to the weird balsa-wood gliders used to transport that stuff getting shot down at a much higher than predicted rate. The paratroopers, armed with nothing bigger than a few anti-tank rockets and their rifles, get to the bridges and are met with zealous resistance from the German troops and tanks that they encounter, and over the course of the film we watch as the plan goes totally pear-shaped. Bad strategy, Monty!

It is a film about a big group of people trying their hardest and failing spectacularly, and it is a really interesting critique of how that failure was ensured. "Gentlemen, this is a story that you shall tell your grandchildren, and mightily bored they will be!" Today on Friendly Fire: A Bridge Too Far. Bad strategy, Monty!

... that is only for people who are really hurt!


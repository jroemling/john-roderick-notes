FFPC15 - Independence Day
2020-07-04

//Intro by Ben Harrison//

Freedom. What is it? We are having all kinds of conversations in our country right now about what constitutes an abridgment of freedom. In some quarters complaints are being leveled that it is an intolerable violation of our rights to be forced to wear a mask into a Trader Joe's. This is - it seems to me - a consequence of a long history of our culture fetishizing freedom without linking it to citizenship.

When you become a citizen of the United States, you are apprised of the rights and - crucially - the responsibilities of that oath. Some of those responsibilities are transactional, like paying your taxes or serving on a jury. The freedom-above-all-crowd have got it in their heads that even those things are possibly a violation of their sacred freedom and should be systematically legislated out of existence. No wonder many of them also refuse to wear a mask, which is primarily a way to protect fellow citizens from a virus you may have and not know about.

My point is: As our imperfect union was set up, our responsibilities are attendant to our rights and one cannot exist without the other, and if you don't believe that, you might need to either reacquaint yourself with the concept of citizenship or make yourself one of those weird bloodstained sovereign citizen documents and go live in some bug-out bunker in rural Michigan. But that is not most people, and it is probably not a problem most of our listeners have. We understand that to have a civil society, there are some give and take and sometimes you have got to go do a thing you don't want to do to contribute to the greater good.

Imagine then that your freedom not to be burned alive in a taxicab on a fictional New York City Avenue that the Empire State Building rises from the center of is suddenly and without warning impinged upon. What then? Who wouldn't want to declare independence from that? That is the kind of independence, the kind of freedom, that today's film is about: Creepy, but somewhat relatable and macOS version 7.6 compatible aliens come to our planet to wipe us out and harvest all our resources.

Never mind that almost all of the resources one could harvest from Earth are so abundant in the solar system that any interstellar-capable species would save themselves a lot of trouble getting them elsewhere. This are the kind of aliens that Hollywood loves to warn us about, the kind that don't make a lot of sense from a thermodynamic standpoint, but enable Jeff Goldblum and Will Smith to quip around and get in dogfights and smoke cigars and enable Bill Pullman to debate whether the use of nuclear weapons is justified in a planetary attack by deranged-but-technologically-superior species bent on wiping out all of humanity.

This is our 4th of July pork-chop episode, and while you are standing around your socially distanced barbecue, celebrating your patriotism with - I assume - a pork chop, I hope you will reflect on your responsibilities as a citizen: Your responsibility to pay your taxes, to serve on a jury if you are called, to take the safety of your community seriously, and to fucking nuke the shit out of some UFOs if they try to destroy our planet. "It is a fine line standing between a principle and hiding behind one. You can tolerate a little compromise if you are actually managing to get something accomplished!" Today on Friendly Fire: Independence Day.

... that isn't a signal, it is a countdown.


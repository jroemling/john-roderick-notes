FF30 - The Siege
2018-07-27

//Intro by John Roderick//

First of all, this is not a war movie, it is a thriller. We added it to the list because it is misleadingly called The Siege, which is a tactic in war that is by definition without thrills. It has lots of army dudes in it and it explodes the ”terrorists bringing the war to America”-Bogeyman that cynical xenophobes used to frighten old people in Tennessee into buying spray bottles full of rice vinegar to keep ISIS out of their roses. But also: We are in dire need to find films that really address this permanent and unending global war on terror we have all come to accept. 

Are we really just committed to a limitless, borderless, goalless state of protracted low-level war with a decentralized and disorganized demographic of self-replenishing zealots for all eternity? All because of that admittedly diabolical attack in 2001 that was in itself Osama’s neo-wahhabi response to our haram-military presence in Saudi Arabia following Saddam's invasion of Kuwait. Really, if you are looking for excuses, it is back to the partition of Palestine, the lack of foresight on the part of the house of Saud when they chose religion to legitimize their dynasty, and MTV. But anyway: We can’t talk about the global war on terror exclusively through boots-on-the-ground combat flicks, because a large part of the action is taking place on Al Jazeera and in shipping containers in the dessert outside of Las Vegas and in the surplus battle dress being shoveled into the police departments of suburban Saint Louis and in Jim Baker’s plastic tubs of apocalypse macaroni. 

So here we are at The Siege, a movie about the war on terror when it was just a glimmer in our eyes. It is almost impossible to remember what America felt like in 1998. I mean, that is especially true for those of you like Ben Harrison who were born in 1998, but even for us boring old cold war granddads swoon at memories of the pre-9/11 world. A flight attendant once ran after me down the jetway, saying ”Sir, you forgot your box cutter!” That’s how different a world it was. The Russian bear was tamed, if you can believe it, and we had learned how to not get into lengthy Vietnams in Somalia, in Ruanda and Bosnia, by mostly keeping our powder dry. It felt like maybe we were close to a world without big wars. 

It is a World Trade Center resplendent New York that is the setting for today’s film. Denzel Washington plays himself, the magnetic and preternaturally calm and capable but still slightly unsettling and intense team leader of a crack squad of - Ladies and Gentlemen: The F. B. I. 

The doubly magnetic incapable and not really at all unsettling and completely chill Annette Bening works for another unnamed government agency that is always the corrupt and duplicitous bug bear of modern intrigue thrillers. Annette keeps blithely showing up at every one of Denzel’s Islamist bus-bombing crime scenes, refusing to identify herself or the agency she works for, which, even more than the bombings, gets under Denzel’s protocol-smoothed skin. Denzel tries to establish his jurisdiction over the crimes and over Annette and kicks down many doors in the process, but the attacks escalate and even Annette's muslim informant and lover of opportunity Samir can’t slip them intel fast enough. 

Back in Washington the politicians are hyperventilating for effect, grandstanding everything out of proportion and also: Not letting Denzel just do his job, which is like: God! The saber-rattlers call in no lesser military man than the unconvincing general moonlighting Bruce Willis to restore order. Now: General Bruce at first demurs at imposing martial law, he is an enlightened general who knows agent Annette from secret CIA/Army cocktail parties and who seems like maybe the Sun Tzu-reading voice of reason at first, but once empowered by congress, or by someone at the studio who accidentally dropped two radically different versions of this script down a staircase and pieced them back together by page number as best they could, he turns on a dime, fully embraces his secret fascist streak, locks the city down, arrests every young muslim male as a suspect and puts them in containment camps taken straight from Red Dawn. The United States besieges its own most important city, hence the name.

Tony Shalube, Denzel's Arab-American partner even loses his clean-cut teenage son to the roundup and his faith in America’s melting pot is put to the test. Annette's fast and loose spycraft and her undeniable chemistry with Denzel produce a few poignant gender-role flippadoos, but ultimately her informant Samir winds up being one of the terrorist bad guys, which sort of discredits her whole approach and foreshadows the utter failure of the entire intelligence community to anticipate the actual 9/11. Thankfully, Denzel pulls a Jack Ryan, storming the make-shift US Army headquarters and arresting the major general for his crimes, which were, remember, sanctioned by congress. Again: The script! He delivers a fantastic soliloquy on American exceptionalism and the rule of law at the end. 

Now: In real life we had to settle for George W Bush in a flight suit blowing up Iraq because Saddam insulted his dad, but you go to war with the Jack Ryan you have, not the Jack Ryan you want. It is kind of crazy that this movie exists! It is a fascinating parable about how our society might have reacted to 9/11 and didn’t, except sort of did, and it has resonant notes with the current hyped up panic about our supposedly porous borders. It dances with what became some of our chief preoccupations 20 years later in the whole universe of American identity and comes away with some surprisingly innocent conclusions.

It is amazing to see a black actor star in a movie about racial profiling, where interracial sex and sex-appeal plays a major role, yet his blackness never enters into it even glancingly and where in fact he plays the role of the proud and resolute defender of the American justice system and the premise of non-ideological multiculturalism, and it is eerie to watch these scenes play out in a world before we committed to 20 years of war in the Arab world as a result of a provocation only slightly more dramatic and deadly than the ones depicted here. 

Directed by Edward Zwick, who, you might remember, also directed Denzel in Glory, and partly written by Laurence Wright of Looming Tower and Going Clear fame, this film reverberates with questions of what constitutes war and what we are really fighting for when we engage in conflicts over race and justice, fear and ignorance, institutional power, gender and terror. It is easy to tell the difference between right and wrong, what is hard is choosing the wrong that is more right. And of all things you have chosen to listen to Friendly Fire, where today we are talking about 1998’s The Siege. 
















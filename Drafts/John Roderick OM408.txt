OM408 - Eternal September
2021-11-04

This week, Ken and John talk about:

+ John and Ken having different approaches to answering questions (OM408)

John is the senior of the two of them in age, experience, prowess, senility, size, and weight. ”Which burger would you like?” - ”I take the senior one!” Back in the 1970s A&W Root Beer had a dad burger, a mom burger, a son burger, and a daughter burger. Ken doesn’t like it when John calls him Daddy Burger, but Ken can call John that. A&W once had a 1/3rd pound burger because they wanted to one-up McDonalds Quarter Pounder and it failed because most Americans thought 1/3rd was smaller than a quarter.

The other day John’s daughter said: ”You and Ken are both smart, but when I ask Ken a question he gives me a quick answer, and when I ask you a question I get a long answer!” - ”Huh, that is true!” - ”Yeah, Ken just gives you the answer to the question, while you always have a story!” That is their shtick! Ken got famous for responding to clues in form of a question quickly, whereas if John gave the quickest possible answer to a question it would not be a podcast.

Ken has a glib fascicle view of questions. He is interested in the surprising fact. What appeals to him about trivia game shows is a question where the exercise is not exploring the answer, the implications of the answer, or his feelings and history with the answer, but it is really just about solving the puzzle and getting the little adrenaline spike or not, whereas John is a storyteller, an artist, and a raconteur. The questions in Rock songs don’t have quick answers, they are more: ”Who am I and why did she leave?”

Every time you ask John a question he will start with: ”When the continents originally…” and that is why they like each other: Ken gives John the quick answer to the question and then John reexplains it to Ken. Ken doesn’t bore John and John does bore him. It is a fair trade! John slows Ken down and Ken speeds John up.

John is the summer-child in their relationship. Ken is the first one to make it to having earned $1 million in a year, to get married and have children, to get a mortgage, and Ken bought his house right around when he got on Jeopardy! in 2003, while John didn’t buy his first house until 2007. Also Ken graduated from college before John did. The University of Washington might say that John graduated in 2015, but it was a Heisenberg Uncertainty Diploma until 2017 //(see RL270)//.

+ The first days on the Internet for Ken and John (OM408)

Ken was on the Internet in 1992, a long time before John was. At the time he didn’t know that the Internet existed as a global backbone, but he was vaguely aware of bulletin boards where hobbyists could find each other. He thought that global networks were the stuff of movies, like when Michael Broderick was putting his phone in a little cradle and somehow got in a government computer system //(from Wargames)//. Ken thought it was all made up and he still doesn't believe it! All computers were connected? How would that even work?

When Ken was a Freshman at the University of Washington they gave him an account and at first he thought it was just some internal UW-thing that he could use to see course materials or communicates with professors or other incoming freshmen. For some reason he had his dad’s email address in Singapore where his family lived and he was thinking: ”Wait, this is the same format as my UW-email! I will do science!”, he typed in his dad's Singapore email and the fact that it got to him and he replied was making Ken feel like Archimedes or Columbus. It did not seem possible!

John still felt like that in 1998 when he got his first email address. It blew his mind that he could go anywhere to an Internet Café. In 1999 he [[[the big walk |walked across Europe]]] and at the time there weren’t Internet Cafés in every place. Many times he had to go to places that you would not think of as Internet Cafés, like in Germany they were selling the Internet as part of selling computers and you could go to a department store, sit at a sample computer and log on.

John was not like Bill Bixby on The Incredible Hulk. He wouldn’t stop, take some odd jobs for a lonely widow, save her farm in some way, and move on. He did that when he was traveling across the United States in 1986, but by the time he was in Europe he had a mission. In the US he would go into a café and say he would wash dishes for a meal and most of the time they were like: ”No, thanks!” He didn’t get involved in local town conflicts that he then resolved and walked mournfully out of town to the sad Hulk music //(see end of RL88)//.

John keeps telling his 10-year old daughter that during the Junior High years he is going to take her out of school and they are going to drive around the country in an old Winnebago, solving crimes. She has rolled her eyes at him for many years, but as time went on she is started to adopt it as a real possibility, but more in a worried than hopeful voice.

He is also trying to teach her crime fighting, but she doesn’t have time for him because she has swim lessons. That is also a way of fighting crimes, she can be Aquagirl. Half of Ken’s bookshelf at that age was purported to teach him how to crack codes and dust for prints. They have a book that a listener gave them about how to hide things in your house.

+ John’s first IBM 64K PC (OM408)

In 1982 John’s mom, who was working as a computer programmer, bought the original IBM PC with 64k. They were hugely expensive! She also got a second disk drive so you could make copies, and a Hayes 300 baud Smartmodem. John had the gear! Usenet came online in 1980 as the prototype of the civilian ARPAnet and you could access it with this modem, but no-one in the house knew how to use it.

John got a copy of PC World Magazine, but he still couldn’t make heads or tails out of it because it was already confusing to him just playing Oregon Trail. He was not inclined to get into computers and used the PC mostly as a typewriter. He loved WordStar!

Eventually he got the modem to work, but didn’t know whom to call. Usenet was mostly academics at the time and John would have logged on as an 8th-grader and said what to whom? He didn’t even know what he was looking for!

The PC and the modem cost as much as a small car, it was really expensive stuff. It was $2500 in 1982-money and John still has it in his original box, together with the amber monitor, not the green one. It was easier on the eyes.

Ken got a Korean knock-off of an Apple II because it was less hugely expensive than an IBM PC or an original Apple II.

+ Having to follow people back on Twitter (OM408)

At one point back in 2012 there was an attempt to change the culture of Twitter where people were saying that if I follow you then you should follow me back. It was supposed to be the new netiquette, an attempt to enforce democratization because at the time there was a feeling that there shouldn’t be stars on Twitter, but everybody should be equal. "Why should you have 100.000 followers and only follow 500 people? Who do you think you are?", but that would only work if everyone had equally interesting things to say.

John had 8000 followers at the time and because people had told him he had to follow back, one night he followed 700 people in frustration and his Twitter feed became unbearable instantly and he hated it. It was all garbage, people talking about their food, and John had lost his thing where he was talking to his friends or people who were trying to be funny. For a month he was bummed because he had ruined Twitter for himself, a thing he loved, until Jonathan Coulton told him that he should never have done that and he needed to unfollow all those people immediately. Most of them won’t even notice. 

+ Usenet and Message boards (OM408)

Ken would use Usenet to talk about what happened last night on Twin Peaks or about the new R.E.M. record. You could find hundreds of people talking about your hobby, which we take for granted today, but that was not true for almost any hobby in the 1980s. Out of scarcity Usenet was for the most part confined to academics who could use it within the college context and to rich people who could pay the long-distance rates.

What was cool about it was that it disseminated to local servers and you didn’t have to call long distance, but in 1982 there might not even have been a place John could have called locally in Anchorage, he would have had to make a long distance call to get onto the Usenet.

John has a message board on Discourse via his Patreon, recapitulating the vibe of message boards in 2001. It reminds him of The Long Winters message board from the beginning of his Rock career in 2002. People from that message board would meet at the merch table IRL and would spawn real relationships. They dated each other and moved across the country to be with their friend that they met on the Death Cab message board. It was pretty exciting! Their friends Matt Fraction and Kelly Sue DeConnick met and married by meeting on one of the early 2000s forums.

+ John recording a podcast with his mom (OM408)

John is doing a limited series of podcasts with his mom who was an early computer programmer in the early 1960s. Computer programming at the time was often the province of women because people at the corporate level thought of it as secretarial work, and the people who qualified to be programmers were people with an intuitive sense of logic.

They would take quizzes or tests and it was a liberal art as part of the ethics department of a college. The transition of computer programming as part of math and science and engineering, which was always considered a lesser subclass of math, was not a natural transition.



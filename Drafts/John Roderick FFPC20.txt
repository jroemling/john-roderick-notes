FFPC20 - Ronin
2020-11-29

//Intro by Ben Harrison//

Painting little samurai figurines is imagery that is so central to the Friendly Fire mythos that I frankly think it is astonishing that we haven't gotten to today's film until now. Maybe we hadn't come up with enough of a justification for it being on one of our lists or maybe it wasn't the right time of year to fire this film up, whatever the reason was: I can't remember. That is the second thing they teach you! But while we reference things in this film on the show fairly constantly, we haven't gotten to it until now.

The director John Frankenheimer is a past and future entrant in the Friendly Fire pantheon. His final film Path to War is already in the feed and he has got something like half a dozen more films on our agenda. He had an incredibly prolific career spanning almost five decades, but if he had just made today's film? Dang! What we have got for you today is a spy vs spy, Russian mob vs ultraviolent splinter faction of the Irish Republican Army, Paris vs Provence, Mercedes vs Citroën white knuckle thriller centered around an oversized briefcase handcuffed to a bad guy. This is the kind of film for which pork is chopped!

The way this film opens is that Robert De Niro hides a pistol in the alley behind a cafe in Montmartre, walks in, makes contact with the Irish bombshell hiring him for a mission, and pretends he only speaks French. And we are off to the fucking races! So many films would set themselves up with an opening scene like this and fall flat on their face, trying to build out the world, trying to justify a MacGuffin that would be equally valuable to Irish terrorists and Russian gangsters, but financially out of reach maybe for both groups.

We are trying to elucidate a rationale for the French arms dealers attempting to kill our heroes by hiding a sniper under Pont Alexandre III or doing an illegal arms deal under one of the busiest bridges in central Paris in the first place. We don't give a shit about that! We want to see De Niro shoot an assault rifle at a convoy of black cars in an open air market in the middle of the day. We want not one, but two of the most spectacular car chases ever captured on film. We want a weird, emotionally stunted bromance between Robert De Niro and John Renault, and we want them to hate Stellan Skarsgård and Sean Bean more and more as they fall more and more in love with each other.

We want a beardy old French guy living with a couple of big dogs in a villa in Provence to paint his little samurai, and we want the improbability of his existence, of his friendship with the Sean Renault, of his easy access to surgical equipment, but total lack of surgical knowledge to only enhance the mystery, to be its own kind of justification. Yes, we accept that there is also a sniper in the rafters of the ice dancing gala. Yes, we accept that a bad guy would murder a child at a playground to demonstrate his badness.

If it will get us to the first car chase scene or the self-surgery scene or the second car chase scene, we all pretty much accept whatever Frankenheimer has to do to get us there. "Whenever there is any doubt, there is no doubt. That is the first thing they teach you!" Today on Friendly Fire: Ronin.

... with the hosts who definitely know what color the boathouse is at Hereford.

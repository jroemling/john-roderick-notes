FF85 - Outside the Law
2019-08-23

//Intro by Ben Harrison//

A lot of the films we discuss on Friendly Fire are set specifically in or are closely related to or about the fallout from the [https://en.wikipedia.org/wiki/Vietnam_War Vietnam War]. And one curious aspect of American exceptionalism is the belief that the United States is the only country to have "their Vietnam". Yes! Didn't you know it is //we//, the USA, the only country in the world that has the sheer GDP, the manpower, and the gluttonous political appetite for that very special kind of war: the quagmire. "Giggidy!" But it only takes flipping through the pages of a High School level history textbook, as long as that book is distributed in a well-funded school district without a Texas-style school board stacked with revisionist history know-nothings, to see that these kind of country-to-country Rock fights are everywhere.

* The 2000s has the United States in Afghanistan and Iraq.
* The 1990s had also the United States in Iraq.
* In the 1980s Russia had Afghanistan.
* In the 1970s we had Vietnam.
* In the 1960s Israel had, and continues to have, Palestine.
* And in the 1950s there was the Algerian war, a battle for independence between Algeria's National Liberation Front and their colonizers from France, inspired in some degree by the Vietnamese struggle for independence from France.

Now, this conflict was a difficult one for me to learn about because I am such a Francophile that the suggestion that the French could do any wrong is hard for me to cope with. But I think that is the real strength of a project like Friendly Fire: It can lead me to see that it is possible for even the French to do bad things, as unlikely as that may seem. Anyway, the war between France and Algeria had been preordained back in the 1800s with the original [https://en.wikipedia.org/wiki/French_conquest_of_Algeria French invasion of Algeria] in 1830. Their scorched earth policy that included massacres, mass-rapes and other atrocities is just something you don't forget about as a country and Algerians did not.

Consequence may have been the creation of a French military colony there, but French citizenship among the native Algerians was fairly unpopular due to their Muslim religious beliefs clashing with the French insistence on them renouncing their religious laws. There is nothing that stokes nationalism quite like an attack on your people, and what a resistance needs more than anything to succeed his organization. Many came and went, but in the decades leading up to the [https://en.wikipedia.org/wiki/Algerian_War French-Algerian-War] the [https://en.wikipedia.org/wiki/National_Liberation_Front_(Algeria) FLN] was that for Algerians, who desired independence from France by fomenting anti-colonialist sentiment into the rejection of France's unfair and unrepresentative government, eventually plunging the country into a hornet's nest of guerrilla warfare marquee fighting and torture.

All of these are depicted in Outside the Law, a film that introduces us to three kinds of Algerians, each with varying levels of commitment to the idea. Complicating things is that they are brothers. The film definitely takes a side and it is their side, shining a very unflattering light on the idea that France was comfortable celebrating throwing the Nazis out of France one year and also thinking everything was hunky dory in Algeria the next. Sacré bleu //(exclamation of surprise)//! It is a film that is moody and dangerous and above all familiar because the struggle for independence isn't just the American way. In this case it is also the Algerian way. In the 2010 [https://en.wikipedia.org/wiki/Rachid_Bouchareb Rachid Bouchareb]-directed [https://en.wikipedia.org/wiki/Algerian_nationalism#Algerian_nationalism_and_the_war_of_independence Battle for Algerian Independence] film Outside the Law.

... whose hosts you might think are alive, but you are wrong: We died a long time ago! "Let them kill us!"


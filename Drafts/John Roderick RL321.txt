RL321 - The XY Problem
2019-01-21

This week, Merlin and John talk about:
* Did John ever find a door that he liked that fit? ([[[House]]])
* Letting his daughter choose his wardrobe ([[[Style]]])
* Forced vacations ([[[Currents]]])
* Wanting to sell his house ([[[House]]])
* Merlin cleaning up his office ([[[Merlin Mann]]])
* The XY problem ([[[Factoids]]])
* John wanting to buy a new computer ([[[Technology]]])
* Merlin’s daughter’s science fair project involving a cow magnet ([[[Merlin Mann]]])
* Comfort animals ([[[Pets]]])
* Putting your ideas through an obstacle course ([[[Philosophy]]])
* How are things going with the chair that John fortified? ([[[House]]])

**The problem:** John needs a window for his door, referring to John waiting for a good window in time when he can install the new front door he recently bought from the salvage yard.

The show title refers to asking for help by not telling the person what you really want to achieve, but only telling them parts of the story that leads to a situation where they can't help you properly, called the XY problem.

They start the show in funny voices, coming in a little hot. 

Merlin is opening the kimono, talking directly to the listeners, telling them that this will be a pre-recorded evergreen episode where they will confine themselves about Hitler and The Beatles because that is the only thing you can talk about that is truly evergreen. Merlin has a small running list of things that he might wish to follow-up on.

+ Did John ever find a door that he liked that fit? (RL321)

//John talked about buying a door in RL316 on 2018-12-10//

John has talked before about his friend [https://twitter.com/ben_king Ben King], an architect from Portland and instigator of the motorcycle trip John took at some point //(see RL300)//. Ben grew up in Texas and for his 16th birthday his father bought him a truck that didn’t run and said ”If you want a car, fix it!” The two of them took it apart and fixed it with a Texas go-get-em mentality. Texans are made from different cloth! You want a knife and fork to eat your steak? Go make them! It made Ben King very resourceful and he is teaching his kids the same way. He drives them out as far as he can go on a tank of gas, drops them off and they have to find their way home. Old-fashioned stuff!

Ben King heard the episode about the door and told John that he has all the tools John would need. He could send him the Wingets, Herminerms and Shoobs and John would have to use a Turboencabulator //(see OM114)// to be able to get the door in the right shape to get it in there, which is the type of project that John’s mom also thrives on. John is just waiting for it to be the right day to be ”door day”, but it hasn’t been door-day yet. In the meanwhile John has his old door, which he has never preferred, but of course now that he is thinking about selling his house he will do all the things that he wished he had done 10 years ago.

+ Letting his daughter choose his wardrobe (RL321)

In order to get 5 extra minutes of sleep when his daughter is standing in the doorway, yelling ”Get up!”, lately John has been rolling over and told her to pick out his outfit. This episode was obviously recorded a very long time ago when John still had trouble waking up, before he figured out his life and got up on time every day. ”Hello! Who is ready for pancakes?” She will go off into the closet and will be in there for a while putting an outfit together that she thinks is coordinated. 

Sometimes it is what John would have picked, sometimes not, but once he cast this die he has to commit to whatever she picks. It is the solution to the outfit of the day issue and if you tell your daughter to pick out some clothes so you can get 5 more minutes of sleep and she comes in with striped pants and a paisley shirt, your first question is ”Why do I even own striped pants?” If she comes with those clothes you have to wear them.

+ Forced vacations (RL321)

Last night John was sitting around, thinking about the many forced vacations that were about to come up in his life. People tell him ”Hey, here is a paid-for vacation, but you will have to perform like a monkey at some point!” It seems like a vacation because he can take his family along and there is going to be a buffet every day with gravy on everything, but at some point they will ask him to toddle out with a lacy umbrella and balance on a ball for 45 minutes. Everything is paid for and it is like a big vacation, but John was asking himself why he chose this life! When is there going to be a door day among all those vacations? Time is compressing a little bit and his window for his door is getting more narrow.

+ Wanting to sell his house (RL321)

John’s real estate agent would rather sell his house when he is not around because she is a professional who has a plan and past a certain point the home owner just becomes a problem, but a) John is a talker and wants to talk to the people to find out if they are worthy of his house and b) the house already has a lot of John’s personality. She wants to take some of that out, bake some cookies, put a blanket on the couch, make it cosy, put some Crate and Barrel furniture in there that John would never pick, put up some tapestry, and maybe some glassy-baby-candles. She would be happy if John was on vacation and just gave her the keys and said ”Sell the house! When I get back I want it gone!”, which might be stressful for John but he doesn't know because he has never been through it.

Getting your house ready to be sold seems like a project that could theoretically go on forever, and his realtor will just say ”The house is fine! Leave!” - ”But those light switches are wrong!” - ”Nobody cares, the light switches are fine! Good bye!” and maybe that is what will happen. Maybe there will be a door day, or maybe she will say ”The new door is nice, put it in the barn! Nobody cares! The old door is nice, too!” and John will have to confront that maybe she has a point. The question is: What is the cost/benefit analysis on a door day?

Merlin’s mom used to be a real estate agent. It must be very complicated to do the math and science on selling somebody’s house because there are a lot of factors that she might be thinking about and not saying, trying to keep John's focus on a certain kind of thing, but everybody is different. John’s real estate agent seems to think that it doesn’t matter if the hard-wood floor underneath the carpet is totally thrashed because people walk in and say ”Hard-wood floors? That is great! We’ll finish them!” It is a project that everybody thinks they can do themselves and finishing the floor themselves is a thing that home-owners are prepared to do. 

As long as there is hard-wood floors under the carpet, the additional expense and agony of finishing those floor maybe add a little bit to the selling price, but buyers will see it as a fun thing they get to do. Maybe you make $1000 less on the sale of the house, but it would have cost you $2500 to do the floor. John is not capable of making any of those calculations. He was going to build a [[[7-sided lighthouse made of dreams]]] in the backyard eventually, but she is like ”No! Go!” 

John also has a lot of thoughts on his pool full of logs. Right now it looks like a Nürnberg bonfire waiting to get started or some Texas A&M bonfire tower, except it is not a tower. John was wondering if he could just light it all on fire, but the fire department would arrest him. He talked to some guys about hauling it out of there and they said that you used to be able to dump clean lumber into some recycling thing and they would turn it into wood chips or into mulch, but now everybody wants money for everything and they charge you by weight.

The logs in the pool is all wet which doubled the weight and they are going to charge an arm and a leg, or an arm and a hammer for this amount of wet lumber. What if John would rent a wood chipper and sit and make it into mulch himself, but now he was just into crazy town, making mulch back there, and that is not going to help him sell his house, but it is just mulch-making. He is just into this because he wants to get a wood-chipper. 

One day John put gloves on and was going to do this himself. He went down into the pool and started moving logs around but 20 minutes later he came out of that pool was not going to do this! There is a cost/benefit analysis and even if he was just inside the house staring at the wall he would be getting more creative work done than out there moving these logs, getting them ready for a mulcher that he is never going to get. 

+ Merlin cleaning up his office (RL321)

In a surprise to no-one, when Merlin cleaned up his office recently, much of what he had to get rid of was aluminum cans. He started out doing it the most responsible way by spending two days individually crushing each can and putting it in a contractor bag. He then individually crushed each 8- or 12-pack container of cardboard and put it in a different bag. As the guys arrived they said that it doesn’t matter what any of that is, because it all gets processed somewhere else. You could have been dead Rhubarb, a newspaper and an aluminum can and it wouldn't matter to them because somebody else will take care of that.

+ The XY problem (RL321)

Merlin recently asked friend of the show John Siracusa for some advice on something and because he is the way that he is, he ended up saying something like ”I wish you had told me what it was you actually wanted to accomplish instead of guessing the methodology for doing something based on your reckon about what you needed to do” A very helpful listener to that episode (probably of Reconcilable Differences) sent Merlin the Wikipedia page of the [https://en.wikipedia.org/wiki/XY_problem XY-problem], which sounds like a gender thing, but it is not. Merlin reads from the page, explaining the problem to John. It is like John asking about the best wood chipper and Siracusa going back down the ladder to start with the swimming pool full of logs instead of the best wood chipper.

+ John wanting to buy a new computer (RL321)

John asked Merlin not that long ago what computer to get and Merlin replied that this was a question for John Siracusa because Merlin was going to get notes if he recommended the wrong thing. It was not a good time to buy a Mac laptop because dealing with the port situation would make John lose his god-damn mind, but the Macintosh iMac family of products were extremely good at the time. John is an Any Old Port in the Storm guy and when he looks at the back of a computer he wants any old and every old port. He has a lot of legacy equipment he needs to plug in. 

John texted Siracusa who asked for the parameters and John told him what he does, what he had, and what he needed. Siracusa replied ”You don’t know what you need!” He designed a computer for John and said ”Get this!” and John went to Todd, his Apple guy who is a pain in the ass and listens to this show. He is a manager of an Apple and said that this was a custom build and would take 21 days to make. Then John would have to pick it up within 20 days or it will be sent back to China. John replied he wanted his computer to be made in America. It is designed in California! The new Reyn Spooner Hawaiian shirts are now designed in Hawaii and made in Korea, but John wants his shirts to have real Aloha.

Now John needs to factor in the 21 days it would take to make the computer and if he would be on vacation when he needs to pick it up. People don’t think of all the things and normals would just go to the store and buy a thing. Todd works in a glass cube and his advice is typically in the family of ”Whatever you want!”, but that is not advice. Saying ”We can do this or we can do that” is not helpful, like if you ask Ariella //(John’s daughter’s mother)// where they should go to dinner and she names 25 restaurants. Todd was like ”Can get anything, you could even do all this on an iPad”, but that is not what John was saying. How many USB-D adapters are on an iPad? John does not want to record a podcast on an iPad.

Adam Pranica has a big iPad with a folding keyboard that he brings to John and claims it is his only computer, but as is a filmmaker he got to have a bigger computer that is making the movies? He would need all the ROMs! Todd might be avoiding this because he doesn’t want to get in trouble if John doesn’t like it. Also, Todd is in a managerial, not a technomological [sic] capacity. He is not an Apple genius sitting in the back and telling John that his RDF card is broken, but he is giving people performance reviews. John just wants the best that is the cheapest!

Todd said that if you are going down to Portland you can get the computer without tax and save $350, which is a quadruple conundrum for John who believes in paying taxes as a liberal. On the other hand it is $350 and he is in Portland a lot. He could also get the tool for the door from ben King, but how much is gas, wear & tear on the vehicle, and pain in the ass? 

If John is going to tow the trailer that John Vanderslice wants to sell, he got to cross your palm //(Probably reference to RL45)//. Where are John’s two days in the studio? Merlin and Sean were so nervous, picking on their fingers and staring at the ground, pacing back and forth. Michael Schilling was crouching like a slav, going ”Hahaha! People are fighting! Finish him!” 

If you bother John Siracusa and make him come down from his attic perch where he was killing flies with chop sticks and he designed a computer for you, you don’t then say ”Well thanks, but I’ll just get the one off the shelf” Now you are committed to buying the custom computer that takes 21 days. John is now in a posture where he doesn’t have a new computer and he hasn’t decided what to do. He is worse off than before because now there is a computer he wants to buy, he knows how much it costs, but he can’t get it because of all these other factors.

He also doesn’t want to go back to Siracusa and ask about the computers on the shelf because he would reply ”If you want to get the unsatisfactory thing..!” John wants the best for the cheapest! Siracusa understands that John is not going to do any video editing or play any first-person shooter games, so he doesn’t need the thing that does everything and allows you to sit and make home movies. John only does a few things: He does podcasting, he records music, and he doesn’t really answer email, but if emails come through, he would like to be able to read them. 

For many years John thought that he might use one of those native Apple things that are meant to make a website for his new floral design center, but he is never ever going to do it! He doesn’t even know the login for his Squarespace. He wants to put 1000 1.5 minute long guitar tracks on the computer and to be able to shift them around and put different color codes on them to make them harder to find //(John is playing one of his guitar tracks and says he has a million of those)//. A lot of that is just straight in. //(Merlin is playing some track from Garageband that sounds like Departure))/. He is very proud of his arrangement.

The bass on John’s track was an actual bass that he got from Aaron Huffman a long time ago when John was in Harvey Danger. John plugs it straight in and just rounds it off. He tries to learn how to be in the pocket. Merlin would listen to music like this, at least the first 10 seconds. 

John did not answer the question if he got the files from the Anna Banana computer.

+ Merlin’s daughter’s science fair project involving a cow magnet (RL321)

Over the last week Merlin and his daughter have been working on her science fair project. Her teacher had been directing them to the website Science Snacks, part of the Exploratorium, where you can find hands-on experiments as ideas for science fair projects. They chose an experiment on magnets: By putting a cow-magnet into a test tube and into a bottle with iron filings you can see the magnetic field. 

She made a great poster for it! Merlin can’t not think of all the things and he can’t sit and not be Socrates with her, asking all kinds of questions. He found a very odd-shaped bottle for her holding salad dressing from House of Prime Rib that would fit a test-tube that was large enough for a cow-magnet. He can problematize anything and he is pretty good at asking ”Have you accounted for this?”

+ Comfort animals (RL321)

It drives Merlin crazy when he buys something on the slightly cheap after a  Wire Cutter recommendation that is the sub-standard version of the one all the normal people get. John has started to think this way about travel: For a long time he wanted the cheapest, but now he wants the best for the cheapest and he is ready to pony up for his travel to not be the worst. 

Merlin always dresses as a General so he can be seated with the troops and he will bring his comfort-oxen. There should be separate planes for people who want to take comfort animals because there are people who are crazy phobic and clinical about their fear of animals and the idea of a friendly animal with a patch on its vest jumping on them makes them want to shit themselves. 

You can bring your dog into a restaurant now. John got so many replies [https://twitter.com/johnroderick/status/1076266560795246594 to his tweet] ([https://twitter.com/johnroderick/status/1076264050856681474 another tweet]) from people, like ”Lighten up man, why don’t you just become a nice person and enjoy dogs?” This dog doesn’t know shit, but it is a piece of shit dog that somebody keeps in their purse and it thinks ”Oh, I get to fly on an airplane now because the world has gone to shit!” in their lulu-lemon pants. ”Is it cool if I take a shit on my chair because that is what I like?” What about John’s comfort? "I like my dick out, what is your problem? You are in your seat, I am in mine! I want to barbecue some pork! I want to jack off while I’m on Facetime with my wife! You sound like a really uptight person!"

+ Putting your ideas through an obstacle course (RL321)

Deferring actions on something like buying a new computer can make a person go crazy. This is [[[Spooky action at a distance |deferred action at a distance]]] and God doesn’t play with dice, at least not that we know of. Merlin is project-managing it and putting everything through the function machine, just as John has started to put all ideas through the function machine. Every time he has a political or a philosophical idea pop into his head he is going to run it all the way through this Socratic obstacle course. 

He is going to sit here, because he was staring at the wall anyway, and he is going to run this idea through every obstacle he can think of, ask every single rhetorical question, like Socrates, ”What if this? What if that?” In general, every political or philosophical idea he can come up with is going to end up out on the obstacle course impaled on a punji stick. Some of them make it through, but those are generally not the ones where you go like ”Yeah, I’ve got it all figured out!” 

These obstacle courses are getting more and more elaborate, like getting through Marine Corps boot camp and deciding to become a Navy Seal before going into Special Ops. It is tough, and it is more staring at the wall than ”I read a thing on Twitter and now it is my new idea!”

+ How are things going with the chair that John fortified? (RL321)

For a long time John sat in his chair fairly delicately because he felt like he didn’t want to press his luck although it was fortified. It is the same chair that they use on America’s got Talent and maybe they throw them away after every two episodes, but John did not get his money out of this chair, but he also came here to kick ass and chew bubble gum, not to sit daintily on a chair where he can’t lean back and get relaxed! He started to lean back again and relax in the chair and the chair feels like it hasn’t bent to the degree where it is about to fall apart, but it is doing that thing where everything is wrong and it is basically shaking itself apart.

There was stress on a part of the chair and it bent, so John put steel there to reinforce it, but now it has just distributed the stress to everywhere and the chair is under tremendous pressure because it cannot bend where it wanted to bend. It shouldn’t want to bend anywhere, but it should be fine! John doesn’t know where it is going to fall apart later, but it is going to be one of those Steve Austin things where it is like ”beep... beep... beep”, like an X1 that blows up in re-entry, like when Chuck Yeager looses control of the X-15. Who wants an experimental chair?

John just wanted a normal handsome chair! He got the white one instead of the black one and over time his denim has blued the white leather slightly, but he stopped caring about it because he feels like the chair is going to explode anyway. Maybe when he moves and leaves the house and lets the real estate agent just sell it on her own, the chair will go in the pool or in the shredder or he will send it to John Siracusa, maybe he will put a cinderblock on it and roll it down a hill and let the people at the bottom of the hill take care of it.

One time John got a 2,5 foot (75 cm) tall Christmas tree and he over-decorated it and in 2010 he kept his tree until March. This year, because his daughter has opinions about things, John said ”Tree’s got a tree! Our relationship is over! It is time for you to be a porch tree!” The tree may sit on the porch until March.




















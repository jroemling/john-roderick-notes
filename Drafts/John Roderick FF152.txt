FF152 - Troy
2020-12-04

//Intro by Adam Pranica//

As much as Ben and John have told me not to, and just as often as I have reminded them of the same, and as much as it kills me to admit it: I do read the comments from time to time. That is because it is hard not to be curious about how my work is being perceived, especially when that work is a 40 hour week of editing dick jokes and to Star Trek podcasts and watching two 3-hour war movies, 90 minutes of which include Brad Pitt's 3-point lit side butt.

I should be satisfied enough with that. It is a good life and I am happy to have it, but it is not good enough! I also want to see how it is going and if it is working and if it matters to people. That is being greedy. Doing this should teach me a lesson, but it never does - or at least that lesson never sticks.

Imagine being a film director - you know - an actual artist where there are actual stakes. You have worked for months with thousands of people through storms and reshoots and rewrites and so forth to get the butt lighting just right. It has to be so tantalizing to read the comment! How do you not do that? When the reviews started pouring in about Troy the first time around I have got to believe that Wolfgang Peterson was not affected by them.

He is an actual artist, unlike me. But like me, I will assume he also has a strict "Don't read the comments!" policy and a group of friends to remind him of the same. They would say: "Wolfgang! Babe! Your schnitzel is going to get cold if you don't stop reading that issue of Das Premier Magazine!" and he would. Schnitzel is good!

I never saw Troy when it was in the theater, but I sure did hear about it. What I heard deterred me and I never saw the film until now. I think that was a good thing because the studio spent $1 million and added 30 minutes to make Troy: Director's Cut, and ordinarily long run times are a thing we complain about on this show, but it turns out you can do a lot of positive things with 30 minutes and $1 million for a baby mannequin budget.

Whether or not the criticism Peterson faced after the premiere of the original cut of the film inspired his inclination towards revisionism - we may never know, and if it is true I am sure he would never admit it because you never want to give a critic that kind of power. But what seems inarguable is that the film is better now than it was before, and I am envious because Wolfgang Peterson got to redo. All it took was $1 million, 30 extra minutes, and a truckload of baby mannequins.

But there is no director's cut of a life. This is it! There are no reshoots and there is no more budget. The reviews keep coming in, but let's just all agree to try our hardest not to read them. If we can. "Everything is more beautiful because we are doomed!" On Today's Friendly Fire: Troy: Director's Cut. 

... where we sacrifice a goat and a pig before every recording session, just to make sure our bases are covered.

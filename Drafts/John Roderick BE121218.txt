BE121218 - Bullseye ”Holiday Special”
2012-12-18

This show is hosted by [https://twitter.com/JesseThorn Jesse Thorn].
Guests are [https://twitter.com/jonathancoulton Jonathan Coulton] and John Roderick, talking about their album One Christmas at a Time as well as the McElroy brothers ([https://en.wikipedia.org/wiki/Justin_McElroy Justin], [https://en.wikipedia.org/wiki/Travis_McElroy Travis] and [https://en.wikipedia.org/wiki/Griffin_McElroy Griffin]) and and [https://twitter.com/TimDeLaughter Tim DeLaughter] from [https://en.wikipedia.org/wiki/The_Polyphonic_Spree The Polyphonic Spree].

++ Christmas gift recommendations

”Every week on this show we invite culture critics on to tell us about stuff that is worth our time” and since this is the holiday episode, they asked their friends from the AV Club to recommend a couple of gifts:
* Josh Modell: 10-disc Blu Ray box set [https://www.amazon.com/Tarantino-XX-Collection-Reservoir-Inglourious/dp/B009B0OG1O Tarantino XX]
* Andrea Battleground: Classic album cover jigsaw puzzles [https://www.amazon.com/Rediscover-Jigsaw-Puzzles-Bob-Dylan/dp/B008V9C3XY Rediscover Jigsaw Puzzles (example)]

There is also [https://www.amazon.com/Onion-Book-Known-Knowledge-Encyclopaedia/dp/0316133248 The Onion Book of Known Knowledge].

The only thing Jesse could come up with for The Out Shot at the end of the episode was the Phil Spector record A Christmas Gift for You. Later he also came up with This Christmas by Donny Hathaway.

+ How Jonathan Coulton and John Roderick met (BE121218)

Jonathan Coulton and John Roderick had very different Rock ’n’ Roll career trajectories. Coulton didn’t become a full-time musician until his mid-30s when his funny heartfelt Geek Rock exploded on the Internet. He has since become one of the great success-stories of self-released music. Roderick had a more traditional path as the front man of [[[The Long Winters]]] who put out 3 records with the respected Indie-label Barsuk. For the past few years they have become friends and tour mates and now they have released a brand-new holiday album.

John and Jonathan met in the spring of 2006 when they played at the same McSweeney’s event at the Beacon Theater in New York. Jonathan was John Hodgman’s sidekick and John Roderick was also one of the performers. They were the 3 least-famous people there and they ended up hanging out together. Jonathan was wearing a coonskin cap which made John very dubious of him. It was the time when it was all happening for Jonathan and it was the last time John has put out an album. John was on the lee side of the peak of his career and this last record sold the most copies and John was appearing on television programs and driving around the universe.

+ One Christmas at the time (BE121218)

John and Jonathan have spent countless hours together talking about songwriting, about what it means to be creative, and about their various methods of dealing with the challenges trying to write. John’s Rock world business organization had been telling him for years that every fall there is a desperate need for Christmas music. Most of the music publishers are looking for new renditions of Jingle Bell Rock, because everybody has heard the old renditions so many times. They had been talking about that stuff for years and as soon as the word Christmas was broached, they just leapt on it and decided to do a collaboration!

One thing they had to address was that they are both secular minded people. They appreciate all the sentimental aspects of Christmas because they remind them of their childhood and they love Christmas as a cultural event, but neither one of them really latch onto the reason for the season. This meant that they couldn’t really get behind the sentiment of a lot of the Christmas music they were listening to while they were preparing to what they originally thought would be a cover album. There were not many songs that were a honest representation for them and they decided to write some of their own. 

One of Jonathan’s lords and saviors is the Atari 2600 and he has extremely vivid memories of the Christmas when he really wanted and received one. This is a very important day in the life of any young nerd, because the future had arrived at his house in a box wrapped up by his parents. As John opened that same box, he discovered an IntelliVision. His father had read a review of it and learned that it was even better than the Atari 2600 and made the 2600 seem obsolete. Now John had this IntelliVision with a disc instead of a joystick and all these whackadoodle games that none of his friends wanted to play with him. He would play it on single-player mode all the time.

The week between Christmas and New Year’s is the most sentimental and melancholy part of the landscape of any year. Christmas is over, the tree is still up, nobody is going back to work and the snow is on the ground. You reckon how Christmas failed to meet your expectations a little bit, you are coming down from an eggnog hangover, you are facing the dawn of a new year and you have all these expectations how this year is going to be different and you are making all these resolutions. It is a very intense psychological and emotional place that we all go through every year, but there was no song about it. They were able to write that song very quickly and it is one of the more naked places on the record. It is sentimental without being modeling.

Jonathan's musical vocabulary is second to none and he is one of those people who just know what the next chord is. When you play 4 chords, he knows what the 5th chord should be. This album was tricking John into writing songs, because there is something to be said about the high pressure, the little time they had allotted to do it and the fact that they were terrified and had to push through that initial fear by just diving head-long. They looked at each other a dozen times during that week, like ”Wouldn’t you rather just quit and get a pizza or something? Pretend this never happened?”, but the fact that the other one was there made them try it one more time. 

+ Christmas in Alaska (BE121218)

John's family went to Disney Land during Christmas time when John was 7 years old and that was the first time he ever got off an airplane in Los Angeles in December. There were Christmas trees and Santas all around, but it was 78 degrees (25°C), there was not a could in the sky and John felt like there was something corrupt at the very core of that. Even to his 7-year old mind there was something contemptible about Los Angeles celebrating Christmas, because Alaska at Christmas time is like a print by Currier and Ives: People are huddled around the fire and the branches of the pine trees outside are weighed down by the snow and the hoarfrost. It is dark and you have oil lamps and candles lit. It is authentic out of complete necessity. Even in Seattle it is hard for John to look out the window and go ”The weather outside is frightening, it is leading and kind of grey and shitty” It is just not Christmas-y enough. 

+ Listener questions with the McElroy brothers (BE121218)

> ”I play Fagan at the Dickens Christmas fair, but my wife thinks it is embarrassing and refuses to attend. How do I convince/bribe her to go?”

Tell her that you will speak with a Cockney accent and wear a prosthetic nose until she comes to the show. Maybe you could also sweeten the pot by offering her a fine fat goose? 

> ”Is it an insult to the giver to spend money or gift cards that you received as a gift on bills?”

Telling the givers what you spend that money on is an insult no matter what because they wanted the transaction to end there. 

> ”When hosting a Christmas or holiday party, what is the obligation for music? Must the host play Christmas or holiday music?”

The obligation is Harry Connick’s Harry for the Holidays on repeat full blast. You have to decide if it is a Christmas or holiday party. 

+ The Polyphonic Spree (BE121218)

The Polyphonic Spree is a Texan Coral Pop group that looks and sounds more like they came from somewhere up above, like where angels sing. They hope for snow every winter, because they don’t get much snow in Dallas. Tim DeLaughter formed the group spontaneously in 2000 when a friend booked him on a show with Indie Rock stars Grandaddy and Bright Eyes. The group swelled to about 2 dozen members and took off within a couple of years. In 2004 they landed a slot on a tour with David Bowie and one of their songs even made it into a commercial for iPods and the Volkswagen Beetle. It was pure insanity! Christmas is a fun night of craziness for the whole family and they have taken that spirit and formed it into an album called [https://en.wikipedia.org/wiki/Holidaydream:_Sounds_of_the_Holidays,_Vol._One Holidaydream: Sounds of the Holidays] with original compositions and retooled Christmas standards.














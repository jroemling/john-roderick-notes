RL350 - Wizard Rock
2019-09-02

This week, Merlin and John talk about:
* John’s coffee filter tipping over ([[[Food and Drink]]])
* Heavy Metal (movie) ([[[Movies]]])
* Elton John ([[[Music]]])
* Blue Öyster Cult ([[[Music]]])
* Led Zeppelin, weird album covers ([[[Music]]])
* John’s sister working at the record store ([[[Family]]])
* Rush ([[[Music]]])
* Every band has a weakest link ([[[Music]]])
* Tool ([[[Music]]])
* The guy with the Prong tattoo who built a milk crate room in the living room ([[[Early Days]]])
* John being in the band King Nilla, Gordon Raphael, Joe Skyward ([[[Music]]])
* Don't cover Elliott Smith ([[[Music]]])
* John referring to many dead people during the episode ([[[Podcasting]]])

**The Problem:** You could see the clip art version of him, referring to John seeing a concert of Tool where Maynard Keenan played the entire show as a silhouette. He was backlit and it was very dramatic because you could just see the clipart-version of him.

The show title refers to Rush being Scary Rock with some black magic, or maybe Wizard Rock, at a time when John believed in wizards.

Merlin’ daughter has very recently started to think that she can move things with her mind and both her mom and Merlin told her that she has no idea how normal that is. Everybody goes through a phase like that. Merlin has heard all three McElroys talk about it and it is true for him as well.

+ John’s coffee filter tipping over (RL350)

Today is off to a whizz-bang start! John had been to the store yesterday, working off of a shopping list made by a committee, but coffee didn’t get included on the list, so as John woke up this morning one of the other people who lives here had taken the last cup of coffee from yesterday and didn’t make any more. John made a half-pot, but the filter fell over half-way through because it wasn’t full and now the coffee is all full of detritus.

John’s day doesn’t start long before they record, meaning he didn’t have any good experiences today to offset this. He wants this day to be a good day! Merlin is always bummed out by filter breach because it feels like something he should have mastered by now. He uses one a one-off dingus and they are not a pot-family anymore, but he will get a little bit ambitious and maybe he will stir too hard or pour too much and when it goes over the edge it is on him. He might have an off-brand non-Melita filter in a pinch that might be good for the environment, called If You Care but while they are green they are not as good.

They say: ”3 is 2, 2 is 1, 1 is none” and therefore Merlin gets coffee by the case. ”Coffee is to morning as the best part is to waking up” //(reference to Folgers commercial)// ”He never asks for a second cup of my coffee!” //(reference to Yuban commercial)// ”Pretty sneaky, sis!” //(reference to Connect Four commercial)// John’s day has already improved!

+ Heavy Metal (movie) (RL350)

John saw the movie Heavy Metal when it had just came out in a double-feature with Tommy (1975 film) in the Rec Hall of the University of Alaska. They were in 7th grade and some of his had friends cooked up this thing and planned to go to this movie showing and they got their parents to sign off on it because it was at the college and it was a bunch of college kids sitting on the floor of the gym, watching movies on a pull-down screen.

They were of course all hopped up on Goodfballs, they all had the [https://www.urbandictionary.com/define.php?term=wacky%20tobacky Wacky Tobacky], but John was in 7th grade, sitting there on some sleeping bags they had brought, and the movie Heavy Metal really tripped him out. He had never seen anything like it, there were of course some adult themes.

For Merlin who had been watching a lot of Bugs Bunny (”Overture, curtain, lights, this is it, we’ll hit the heights!”, Bugs Bunny Theme) and Disney it was unusual to see animation that was this fucked up with lots of crazy occult vibes, adult-themes, and Heavy Metal music playing in a lot of cases. It made an impact on John, especially since the movie Tommy did not make any sense to him. It is not a great movie and at that point in time John had already seen one movie. Tina Turner is very good in it, though, and Ann-Margret as Tommy’s mom is stealing the show. Also Elton John.

+ Elton John (RL350)

Merlin thinks that Elton John should stop covering things and let Bernie (Taupin) run the show. Lucy in the Sky with Diamonds is a bad choice of song and Pinball Wizard is a guitar song, not a piano song. John interjects that Elton John at least always had the good sense to have a guitar player there. Merlin has done a deep Elton dive in the last few weeks and became obsessed with the song Tiny Dancer. It is the Mona Lisa of its time!

It is easy for somebody Merlin’s age and generation to have completely lost what an exquisite song that is. Florence + The Machine do a cover of it that will curl your tears. Bernie Taupin was such a good lyricist! Some of the lyrics like some of the stuff from Goodbye Yellow Brick Road are pretty weird, though. Was that your first draft?

+ Blue Öyster Cult (RL350)

Because of the iconography alone there was a lot of overlap between the Rush people and the Judas Priest people in Merlin’s Junior High. There was Blue Öyster Cult and bands with Umlauts. They played Veteran of the Psychic Wars on the soundtrack to the movie Heavy Metal. They were fun and funny. Patti Smith wrote lyrics for some of their stuff.

Blue Öyster Cult was a little misunderstood by the young kids who didn’t realize they were having fun a lot of the time. One time, maybe in 7th grade, John was over at Matt Olnes’ house, and they were doing a thing that young kids do: Putting his cat inside a bean-bag chair and when it came out it was full of static Styrofoam and was really upset. Matt was also a bit of a sadist and used to put tape on the cat’s paws, or they would set off fireworks in the house.

They were bad kids who gave themselves shaving cream Mohawks and were in danger of going the wrong way in life. [https://health.usnews.com/doctors/matthew-olnes-448159 Matthew Olnes] is now a doctor for the National Health Service //(Public Health Service Commissioned Corps, PHSCC)// and was in the Surgeon General’s office. John asked him if that meant he was a Surgeon Major and he said he had the rank of a Brigadier General. Matt had a Blue Öyster Cult 45 and he told John that they are Satan worshippers and he showed John the upside-down question-mark with the cross and the dot and said it was the symbol of Satan.

John was a precocious child, but he didn’t have an older brother and he didn’t know anything about Rock, certainly not Wizard Rock, and definitely not about Satan. Matt showed John the guy with the three balls on the Blue Öyster Cult record, the puppet master, and the guy with the butt walking into the star, and John found it scary. They probably had shaving cream Mohawks as they were doing this.

Matt said: ”I renounce Satan!”, took the 45 off the turntable and broke it by shattering it into 100 pieces without losing eye-contact with John, which was super-intense, especially because the one thing John did know was that a 45 costs money and Matt had just broken $1.50, which seemed like lighting a cigar with a $100 bill. John was a little worried about what that it to ”renounce Satan” and whom did Matt follow now? For a long time John couldn’t listen to Burnin’ For You without getting scared.

+ Led Zeppelin, weird album covers (RL350)

John's friend Kevin Horning wasn’t judgy about music and shepherded John through Hard Rock in the early days. A lot of guys will kill you with scoffs if you say you have never heard about a certain thing, but Kevin would say: ”Coda is not a proper Zeppelin record, but it does have some cool tracks, so it is okay. What you need to start with is Presence!” The cover by Hipgnosis is a very 1970s Rock album cover, like: ”We have this odd photograph, can we use it?” and also: Wizards!

This continued for a while into the 1980s. Mötley Crüe also got an umlaut, but by that time it had already turned cartoony and you couldn’t pull that off anymore. There is Satanism, D&D, nuclear war, and SciFi. They had a lot going on. They had just been to the moon and now they had a rocket ship that looked like a jet airplane and they were still just in the tail end of that: ”Anything is possible!” Maybe they were going to space all the time now and find some dragons in space or a girl with a glowing green slime orb, like the green lady from Star Trek (Susan Oliver)? It wasn’t until the late 1980s when they realized that there was nothing out there.

Merlin would go to the mall at least once a month where there was a Record Bar and a Camelot and he would go through every album in the Rock section every time to acquaint himself. He would see all the Kiss Cut Outs (the Solo albums) because they still couldn’t get rid of those, and he would pour over some of the albums because it was like finding a Playboy Junior to go in there and look at these albums that were not for him. Look at the covers of Rock and Roll Over or Destroyer, that is not safe!

+ John’s sister working at the record store (RL350)

At 12 years old John’s sister already worked at Robert Joe’s, the extremely cool record store in Anchorage //([https://books.google.se/books?id=nReADQAAQBAJ&pg=PT41&lpg=PT41&dq=robert+joe%27s+record+store+anchorage&source=bl&ots=SUhJPnm7cJ&sig=ACfU3U1UvKrgU98gh9jyjnhGwtDok8N6JA&hl=en&sa=X&ved=2ahUKEwjWvP7ehMLkAhUEx4sKHZPbDw0Q6AEwB3oECAkQAQ#v=onepage&q=robert%20joe’s%20record%20store%20anchorage&f=false Link here])//. Her mom would drop her off there and pick her up three hours later. Susan didn’t want to go to the mall or Chuck E. Cheese’s or her friend’s house, but she would spend the afternoon at the record store. The staff were all 21 and they noticed this 12-year old girl who was there all day looking at records and eventually they offered her a job stocking. Pretty soon she was around the cash register. She was too young, even if her mom signed off on it, but by the time she was 15 she was running the store, which was super-high status.

John had to pick her up because she couldn’t drive yet. He would stand in there, drumming his fingers: ”Come on, let’s go!”, while every cool kid in the city was in there: ”You nerds with your funky shirts! Get me out of here!” John wasn’t a jock either, he wasn’t an anything, but he was just exasperated by everything.

+ Rush (RL350)

Darius Minwalla, the former drummer of The Posies who [https://www.thestranger.com/blogs/slog/2015/05/22/22262371/seattle-music-community-stunned-by-untimely-death-of-darius-minwalla died suddenly] //(see RL308, he had googled ”heart attack”) a few years ago at a very young age and who was very close to them all, was ridiculous about Rush. He liked everything, even the later stuff.

He would listen to it very loud and shout at you over it about how amazing it was. He wanted you to hear every cymbal hit and Merlin is the same way. Eric Corson used to live with Darius and also feels this way about Rush. He can air-drum to Rush with such conviction that you really do feel the hits //(see RL316, RW48)//. Nabil (Ayers) was the same.

There were times on tour in The Long Winters where they would launch into Spirit Of Radio for the sound check. Of the three dudes in Rush, Alex Lifeson might be the one who has the fewest number of cultists and John never understood what he was doing exactly and he has certainly never spent any time learning to play it. They would start to play it and John could do the lyrics a little bit, but that was their way of taking charge.

John’s Rush experience was the same as for a lot of people his age: The older dudes who already had marijuana were listening to 2112 when John was in Freshman year. It was Scary Rock before there was a lot of fake scary. It wasn’t devil music, but it had some black magic in it, like Wizard Rock back when they believed in wizards.

You are sitting there, burning incense, with some black light, listening to Rush 2112 and Xanadu, ”A stately pleasure-dome decree” //(poem [https://www.poetryfoundation.org/poems/43991/kubla-khan Kubla Khan] by Samuel Taylor Coleridge)//, concentrating, and maybe you can’t see that someone has two fingers against your temple, like Professor X: ”Move just a little bit!” John saw Uri Geller on NOVA. John wanted to go to the Temples of Syrinx, but didn’t know if he wanted to be a priest right away.

//John brought the conversation back around to Rush after 13 minutes of diversions. He got his Commodore hat with the [https://en.wikipedia.org/wiki/Scrambled_egg_(uniform) Scrambled Eggs] on it, which is the hat he wears when he finally brings it back around, like Commodore Schmidlapp trying to bring his boat in and he is doing it every time.//

John was scared by Rush because it was Wizard Rock and the kids who were listening to Rush had chew cans in their back pocket, they were a little bit harder core, they tucked their T-shirts into their jeans, they just had that thing, and his friend Kevin told him to start with the first Rush album, the John Rutsey one with Working Man. There are no wizards at all on this album, but it is just straight Canadian Rock’n’Roll.

It is a great Rock record that sounds like Triumph. It doesn’t sound like Led Zeppelin because there aren’t any hobbits in it either. Merlin has not spend a lot of time with that album, but he knows from the many documentaries about Rush that they were heavily in the thrall of Led Zeppelin.

John adds that they didn’t know exactly what they were all about. Later on they got another drummer, Neil Peart, who had some lyrics and had some ideas he needed to get off his chest. Merlin wonders how such a smart man can write such dumb lyrics. He is a little bit like the Brad Bird version of Ayn Rand. It really has not aged well. He has been through some things and has lost his wife and his daughter at separate occasions.

The first Rush album (Rush) only has 8 songs on it and two of them are over 7 minutes long. John reads the track listing and says a few words about that. Each of the songs is about what the title says. Merlin can imagine that What You’re Doing is just a slightly uptempo shuffle that was before he went super-high. He was high, but he hadn’t gone to the crazy highness of later on. He got silly high before and after 2112 when he was making cats cry. Brad Delp from the band Boston was over there getting very high. That guy could sing and he was terrific!

The song Working Man is about the struggles of being a working man who goes up at 6am and goes to work at 9am, pours himself a cup of ambition, tumbles down the stairs //(reference 9 To 5 by Dolly Parton)// Although he has two hours between when he gets up and when he gets to work he says: ”I got no time for livin’ Yes I’m workin’ all the time” Marvin Berry?

The crux is that the working man thinks he could live his life so much better than he does. He is Canadian, he gets home at five and gets himself a nice cold beer, probably a Molson, and he is always wondering why there is nothing going down here. This is Bob and Doug McKenzie music! ”It is a beauty way to go!” (lyrics from their song Take Off with Geddy Lee) ”There is no point in steering now!” (reference to movie Strange Brew).

The problem with Working Man is that at no point in his story or struggle or dramatic arc does there appear a wizard or hobbit of any kind. Also: There is no philosophy in that song other than that he has a beer and is bored. Merlin did not know that about these songs. Ending it with Working Man is still a John Fogerty roll-up-your-sleeves type of situation.

In Canadia [sic] they love plaid and this is a little bit of a plaid shirt song, but not a Pendleton. It is not a hat with ear flaps that you are wearing in Louisiana for some reason, but it is something else. Canadians say that up there Rush is just a Hesher shitkicker band. They are constantly confused why Americans treat Rush as some philosophical intellectual band.

Canada is a nation of nerds and while there are cools up there they would get eaten alive down here. They are the denim crowd up there! Their emo kids are so adorable, Merlin wants to put them on a keychain, they are so cute and John’s emo friends from there say that people who listen to Rush are bullies who will beat you up. They are the tough kids, the muscle car drivers. John was surprised to find that because we do treat Rush like they are very smart down here and it seems like smart Rock, or smurt Rock. 

John got into the first Rush album because the themes are uncomplicated. Not only is it good Rock music, but Alex Lifeson at that point in his career was playing guitar that John could understand perfectly well. Merlin thinks that of all the people who owe a clear debt to Jimmy Page in their style and approach, like Ace Frehley, Alex Lifeson, all of those, he is one of the most accomplished in making it into his own thing while Ace Frehley is just playing Jimmy Page licks mostly. As Alex evolved he got more and more into a state where John could still follow along, but it got a bit too fuzzy and hard to get inside, not that it was unlistenable.

John can get no purchase when listening to Kirk Hammett. It is terrible and John can not get a toe-hold in what he is doing. It is like giving a guitar to a random number generator, he doesn’t even know what mode he is in. Fade to Black? Really? That is your solo? Kirk Hammett is like a wasp John is trying to swat at while hanging off the side of the mountain of James Hetfield. Then there is Lars (Ulrich) who is just rocks coming down that John also has to dodge. ”God, give me something, anything to work with here!” That is why Jason Newsted was such an important person for John.

That Rush record established John firmly in Rush with both feet planted, but that is not the record that most of the Hesher wizard bullies were familiar with. All of a sudden John had some purchase in a place where he was firmly planted in a Rush that not everyone knew. This was the heyday of Rush, the Moving Pictures era, and Rush became more and more as everyone was a wizard. John was able to stand at least somewhere at the party and say: ”Yeah, I am kind of into their earlier stuff!” People were familiar with Working Man because it was in the 7-minute song pantheon, but John could go deep on What You’re Doing and he could get all the way back to some of that foundational Blues Jam.

John played the tennis racket quite a bit to that early Rush and over the years he has absorbed so much Rush that he can only say that he is 100% pro-Rush. Early Rush is Merlin’s favorite era as well because he is a basic bitch. Every Sunday morning he listens to Duke by Genesis and he likes that period of Rush when they were really good for three albums, although he doesn’t dislike the other things at all.

Merlin is familiar with a fair amount and while he is not a big fan of Caress of Steel he is solidly in the camp that when he got Rush and it made a ton of sense to him it was the period of Permanent Waves, Moving Pictures and their live album Exit... Stage Left. If you didn’t have their old record you could hear them playing stuff like The Trees.

Merlin knew of the Exit... Stage Left video because he saw excerpts on MTV and the video for Limelight is probably from that performance. It has been out of print for a super-long time and Merlin has been looking for it in all the usual places, like on the back of trucks, but it turns out there is a [https://youtu.be/y-0aCyHntHs pretty good copy] on YouTube.

Merlin watched the Exit... Stage Left concert video last night and he had the time of his life. They are so good, their songs are so good, and Geddy Lee was doing four different things at once all the time. It was not all just jerking off to Neil (Peart), although he is a great drummer, but he didn’t yet have 14 cameras set up to show his Timbales or whatever. Today he has a splash cymbal and a sizzle cymbal camera.

Merlin hadn’t watched it in years and he was so happy to watch this. Now at 52 years old he revisits a lot of stuff from 30+ years ago and finds it okay. Mrs. Doubtfire hasn’t aged super-well, don’t even get him started on Ghostbusters, but Duke by Genesis is still good. Sometimes he returns to something he hasn’t spent a lot of time on in 20-30 years. He saw Rush live around 8-10 years ago and it was really good and it was nice to return to that and find it exactly as good as he remembered. He could not believe that all these years later he still thoroughly enjoyed this performance!

It is impressive that there are only three of them, which is made possible through the magic of Geddy Lee being able to play his bass with both his hands and his feet at the same time and also somehow play the keyboards at the same time, which is an astonishing feat that Eric Corson adopted during the later years of The Long Winters. Jonathan used to do that and work with every limb, play the guitar and the keyboard.

Eric and John did some shows right before The Long Winters stopped doing shows where it was just the two of them. Eric was playing the bass with his feet and the keyboards and guitars with his hands while John was playing the guitar and sometimes the piano and on one song the drums, or Eric would play the drums while John played the guitar or the piano. It was really fun and they made a lot of music for just two dudes, partly because Eric was making the music of three dudes and sometimes more. 

Geddy is playing so many things that three people sound like a multi-tracked album, except he does it live. At this point Alex was playing some guitar stuff that was a little outside of where John is finding it 100% lyrical. Merlin is thinking of Limelight in particular where he gets super modal and has these amazing pull-offs.

At the time it would seem heretical to everyone involved to compare the two great power-trios of the time and to say that his role is very similar to Andy Summers in The Police. He has a very distinctive tone and style and he is not resorting to chords, but he is doing a lot of interesting arpeggios and pull-offs and he is not wanking around and getting in the way of the song, but everybody is driving the song forward. A lot of the time he does that in an unconventionally deft way.

+ Every band has a weakest link (RL350)

At one time John was in a mood and went through bands on Twitter. This wasn’t during the heyday of Twitter but a little bit later when Twitter had already started to become a pain in the ass //(on 2016-08-25)//. John Moe is really famous for every morning trying to come up with a meme and back in the old days he would sometimes get a meme that caught fire, but now everybody does it.

* https://twitter.com/johnroderick/status/768657443953881088
* https://twitter.com/johnroderick/status/768649906978377728
* https://twitter.com/johnroderick/status/768649662362361856
* https://twitter.com/johnroderick/status/768643346822672385

John asked himself: "Who is the weakest link?" Every band has a weakest link, like Kirk Hammett is the weakest link in Metallica, even though Lars is widely regarded as the weakest link. They could add an Airedale (dog) to the band and he would still be the weakest link. 

Even The Beatles had a weakest link, but it changed over time, and in 1968 it was John Lennon because he was really fucking high. When John made that case people dumped on him so hard! Ringo kept the band together! Even Flansburgh came at him and said that John Lennon in 1968 was one of the most legendary performers of all time, but he was still the weakest link in The Beatles at the time! John also said that Alex Lifeson was the weakest link in Rush because the point of the exercise was that there has to be one, even if you might not like it.

Every classic album has a terrible track, end of story! For 15 years every band was going to try a Reggae song and it was always the worst song on the record, except for I Shot The Sheriff, which is so great that it can even withstand being played by Eric Clapton. Elton John and Lucy In The Sky With Diamonds has a little fucking Reggae bit and it is a terrible version of a great song that should never be covered going forward. 

John got a ton of Rush flack and he got the reputation of not liking Rush because he is capable of offering a critical opinion and there are a lot of Rush purists who will not allow that. Darius could tell it was a Rush song just by the sound of the needle in the grove. The band didn’t even need to be playing and he knew the tune. He was not going to listen to John telling him that Alex Lifeson is the weakest link, but the rule of the game is that there got to be one. Because of John’s contentiousness people assume that he is anti-Rush, but John doesn’t understand how somebody could be pro-music, but anti-Rush!

Merlin finds many bands really easy to dismiss, for example (Matt) Finish, Dream Theater, Rage Against The Machine, or Tool who apparently has a new album! Merlin doesn’t love all those bands, he is not a fan of Tool, but people like that. Rush is one of those bands that are easy to dismiss.

+ Tool (RL350)

Many years ago John wrote a review of Tool at Bonnaroo for CMJ. He had thought he would be safe and they would never find this because first they would have to learn to read. Merlin doesn’t even know how to make fun of Tool. It is that one guy and it is deliberately over-complicated heavy Prog Rock. They had put on a very interesting show where Maynard Keenan //(John said Maynard Ferguson)// played the entire show in silhouette. He was backlit and very dramatic because you could just see the clipart-version of him. They play pretty intense Vivisection videos of animals being experimented on, like a Nine Inch Nails thing.

John reported on Tool with a lot of snark spin and he got 600 furious Tool fans who wanted to explain to him that Maynard was a prophet and one of the smartest men who had ever walked the planet. The lesson he learned was: ”Do not cross Tool fans!” Also: John doesn’t have enough investment in not liking Tool that he was going to put himself at that much risk, so he stopped saying the word ”Tool” or having any take on Tool because Tool fans just are not into that. Rush-fans at least you can tease them for being Canadian, but Tool fans are out there building machines in their garage that are going to put John’s eyeballs in a frying pan.

Their album Tool-72826 has a tool on the cover that looks like a penis. In 1991 John was briefly in a band with a guy who had long hair except he shaved the sides up pretty high. When he had his hair down he just looked like a greasy-haired guy that you could see his ears, but when he pulled it up he looked like he listened to Skinny Puppy while he was making love. Foetus and Ministry and all these bands seemed so serious, perhaps in the same way that the Satanists and the Wizard Rockers seemed very serious and very worked up about something.

+ The guy with the Prong tattoo who built a milk crate room in the living room (RL350)

One time John was a roommate at a place where 14 people were living in a house with 9 bedrooms. On of the guys there, who is dead now, had built a full-on room inside the living room out of milk crates he had stolen from the back of a supermarket, like a Minecraft room. Instead of a large living room there was still a nice hallway there, except that one of the walls was made of milk crates all the way to the ceiling.

You could see through it but not all the way, like translucent tiles in a bathroom. You knew if he was in there or not, but you couldn’t pick out anything. Around the corner of his milk-crate room it opened up into the dining room which became the living room. He even had a door in his milk-crate room, he was a genius! He had a tattoo for the band Prong that took up the entire back from his waist band to the nape of his neck. It was a big tattoo, especially for the time. 

Prong was a band where John was not sure if they were joking or if they were serious. Merlin's roommates would always buy him a Laibach album for birthdays and Christmas as a running gag. One could never tell how serious they were, but people would always say that they were not actually fascist, but they are having fun with the idea of fascism and they were actually an anti-fascist band. Life Is Life is a cover of a very silly pop song, and they even covered Let It Be, but people were still saying that these guys were pretty bad news and you don’t want to mess with them.

There are many bands like this that people took extremely seriously! You don’t get a Prong tattoo all over your back if you think that Prong is joking. There is no such thing as an ironic tattoo. Stephanie who booked the first Long Winters show on their first tour in Milwaukee had a couple of full-on Juggalo tattoos with the hatchet man that she at the time in 2001 claimed were ironic Juggalo tattoos //(see OM188, RL43, RL318)//. John asked her if she was a Juggalette, but she denied and just thought they were fun.

Killing Joke were having fun and if you had a Killing Joke tattoo you would know it was fun, but Prong? There were plenty of people who thought they were serious and plenty of people who thought Tool was serious.

+ John being in the band King Nilla, Gordon Raphael, Joe Skyward (RL350)

One time John was in a band called King Nilla, making music that involved a lot of consumption of illicit substances. The band consisted of John, Jon (Auer), a drum-machine, and another dude who only showed up about 70% of the time. They were called King Nilla because one time they were on substances, sitting around, passing around a box of Nilla Wafers. One guy pulled out a Nilla Wafer that was twice the normal size, and somebody said: ”Dude, it is the King of all Nillas!” and they decided to call the band King Nilla. During his wizard phase John sometimes was their singer.

The band practiced in a house, and John wasn’t //actually// living there, with a roommate by the name of Gordon (Raphael) who kept coming out of his bedroom all bleary-eyed, saying: ”Ey, can you guys practice later or something?” because they would be in the living room with the drum-machine going, practicing their Electro Wookiee Rock.

Gordon was from the band Sky Cries Mary, a goth-y jam-y Paisley Velvet band with lead singer Roderick Wolgamott. If somebody says Roderick in Seattle now they mean John, but in the early 1990s they meant Roderick from Sky Cries Mary and there was a period in the late 1990s where it wasn’t clear which Roderick somebody was talking about.

Everybody was in Sky Cries Mary: Jon (Auer), Ken (Stringfellow), even Joe Bass (Joe Skyward) who went on to be the wonderful bass player of The Posies. He played on the first Long Winters record, he was the long-time bass player of Sky Cries Mary, and he played on Frosting on the Beater (by The Posies) and all the Posies records. He was instrumental in the Seattle scene. He also is dead now and his passing was tragic, but he was dying for a long time, so they had time to process it! When Darius passed it was really like an explosion in their little world.

Gordon later produced the first Strokes record. Let that sink into your brain! He gave The Strokes their sound. They were insanely popular for a while right before 9/11 and then they had to take their New York City cop song off the record. The whole concept of the lead singer singing through a Colin Meloy megaphone was created by Gordon Raphael, who was not the biggest fan of King Nilla, but was in Sky Cries Mary for a long time.

Gordon was not the one behind the milk crates. The guy behind the milk crates and John were never in a band, but he was just a who lived behind milk crates and had a Prong tattoo. There are a lot of pieces on the board and Merlin doesn’t want to miss anything, but all those guys are dead now!

+ Don't cover Elliott Smith (RL350)

You never want to cover Elliott Smith! John said [https://twitter.com/johnroderick/status/1166494564955246592 on Twitter recently] that The Long Winters once covered Pictures of Me, which is such a good song, but it should be illegal to cover Elliott Smith. Covering that song was cathartic for them! Merlin says that he is rocking at half speed and that last verse of Pictures of Me was outside Heatmiser as rocking out as he gets. He is so sick and tired of all those Pictures of Me and you can feel that he is ready to blow, but when he eventually blows it will just blow his own head off.

+ John referring to many dead people during the episode (RL350)

Before John started doing this show he had lived at least one and a half lives already, maybe two. The other day he ran into a girl at a coffee shop and they only talked about all the people that they knew in common that were dead: ”Put aside the alienation, get on with the fascination, the real relation, the underlying theme” (lyrics Limelight by Rush) John has never been good at taking a normal word and turning it into a three-syllable word. Elliott Smith was great at that, he would put as many syllables in it as he wanted. 

Merlin gets very upset that John referred to so many dead people during this episode. John can’t put aside the alienation and he is trying to have alienation and fascination simultaneously because he does want to get on with the fascination but he can’t put aside the alienation. They start singing Limelight and Merlin hits the bell before they have any more deaths on the record.
























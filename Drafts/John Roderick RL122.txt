RL122 - Parliament of the Moment
2014-08-28

This week, Merlin and John talk about:
* Life-improvement philosophies ([[[Depression]]])
* Times tables ([[[Early Days]]])
* Printing emails ([[[Internet and Social Media]]])
* Following the news too closely ([[[Internet and Social Media]]])
* John looking at his phone too much ([[[Internet and Social Media]]])
* John’s early days of using Social Media and the Internet ([[[Internet and Social Media]]])
* John’s unfinished projects ([[[Personal Development]]])
* Visualizing John’s thought time line ([[[Dreams and Fantasies]]])

**The problem:** John’s gotten a lot of satisfaction out of not graduating from college, referring to his enjoyment at cocktail parties when he is the only one who has not graduated, a reverse snobbery and a point of pride.

The show title refers to people following the news hour by hour, but John does not want to hear the parliament of the moment, he would prefer a monthly digest.

The show begins with a few seconds of a song that is unknown.

Merlin went to his daughter’s Halloween day at her school. She had this great Capt America costume and all her friends looked adorable, he took lots of videos and didn’t realize until he had taken 3 or 4 of these really good three-minute videos that he hadn’t started recording and it only started when he wanted it to stop.

+ Life-improvement philosophies (RL122)

It is early, but John is living a new way and has a whole new life, so this isn’t even early for him anymore. ”Come on! Bring it!” He is tired of being on the wrong side of history. Just: ”Up! Up!” and going and living and achieving instead of dodging bullets. John is not just putting out fires, but he is living in the Now, the past and the future. He decided that he was going to sit down over the course of a weekend, digest every single book, pamphlet and website link that he had ever been given about a life-improvement style philosophy, and implement it. A lot of the words are the same between philosophies and, like when you are doing fractions, you can cross off similar words. 

John woke up at the crack of dawn this morning, he made a blender of raw eggs, went to that museum in Philadelphia, pumped his fist in the air and started immediately writing the best work of his life. At a time of day that would formerly have be pretty daunting to him, he has already done a lot today. He is thinking about getting married and he might already be over the hump on it because he read a lot of stuff saying that married people are happier and live longer, because they take fewer risks. Up until just a little while ago he was pretty anti-marriage, but now he is re-evaluating. He went into the tank and now the formaldehyde of self-knowledge has permeated all his cells. 

A lot of these books also lead John to the inescapable conclusion that he needs to log off the Internet because of productivity and spiritual cleanliness. It is not just enough to not read the comment section, but the entire Internet has become a comment section. 

+ Times tables (RL122)

Merlin is still doing division like he learned in 3rd grade, even if he probably doesn’t need to, but he feels like he does better division if he shows his work and John feels that way, too! 

John is very proud of having learned his times tables a long time ago //(see RW42)// and you will never catch him rolling his eyes up to remember his times tables. 6x7=42 was a very important equation because it was the first one he learned. 42 is such a nice number, but 6x7 seems to produce nothing. Seven is a weird number. Such an ungainly and awkward equation produces such a beautiful number, which stood out in John’s head and was the linchpin and the corner stone, because he could work either direction from 42 which helped him memorize that weird middle part. 

+ Printing emails (RL122)

John typically doesn’t print stuff out, not even when he is asked to do it, because: Printers? Really? Fax machines? Seriously? He remembers when printers were described as on the way out and currently obsolete in 1990. When the Laserwriter came out with the Mac SE/30 you could make a production quality publication. It was life-chaning, but it wasn’t long before people wondered if we still need all this production.

The investment company Piper Jaffray where John used to work (see [[[Employment History]]]) had multiple floors of paper. Their office was in a building in Downtown Seattle that was designed by the same guy who designed the World Trade Center. It was a proto-World Trade Center built in the mid-1960s, a much smaller archetype of that concrete construction style. It takes your breath away because it really looks like a little World Trade Center. The top three floors should be the most expensive offices and some of the nicest offices in Downtown Seattle, but the windows are papered over and they are full of boxes with papers of long-ago business transactions. The whole reason why we were encouraged to start using computers was to do away with paper and now John gets 5 e-mails a week from people telling him to print this out, fill it out and fax it back. ”Go die!” He would rather not do the job and not get paid the money! 

When Merlin was working in an office in the early to mid 1990s, he was making a lot of courtroom exhibits that they would print out, sometimes on a very costly color printer, and nothing felt real, nobody could relax until it had been printed out in multiple copies. Merlin had set up automated backups and stuff, but that didn’t matter, because it didn’t exist until it was something you could put in a FedEx envelope. 

As John went through his dad’s papers, probably a refrigerator-box full of papers, some pages had the gibberish at the top that used to be at the top of every email you printed out and then ”Hi honey, how are you? I have just been thinking about you. Okay! Write me when you can!” He was not just printing out the emails he received, but also his part of the conversation. ”Everything is good, dad! Talk to you soon!” Partly he was printing them out because he wanted to have a record, but it also wasn’t real on a computer screen. 

Merlin was working at a Dot Com and the CTO thought Merlin should invent e-mail attachments because that would be a neat idea. He was completely backward, the scroll wheel on his mouse confused him, but his workflow was common in the late 1990s. Today you would read email and flag it or if you are Merlin you would turn it into an action somewhere else, but anything you printed out was either something you had to read or do something about and that was the way you did your email, especially old people. Otherwise it wasn’t real. 

+ Following the news too closely (RL122)

Only a couple of years ago John felt like a genius because he was getting his news from Twitter and it was 1000 times better than getting his news from the news. Now Twitter is the same news, you feel the same bad way, and it has reverted all the way back to the era where people couldn’t take their eyes off of CNN. Merlin was living with his horrible stepfather when headline news got popular and his stepfather would watch the 30 minute cycle of news all day long. Every half hour they would add one new word to the scroll. At the airport it is wall-to-wall CNN, but who is that even for?

Unfortunately Twitter has become the same now. Whatever the news is that Twitter has decided to be the important news, it is sometimes very proud of itself when it decided something is the news that is not the same than what CNN decides is the news. At first it did an amazing job, but now John is back as a person watching the freaking news which is not a person he wants to be. 

John doesn’t want to follow the news closely because it does not produce happiness or productivity. The magnetism of constant news updates seems to be inescapable for anything. The Internet had such promise, it was going to be analysis, and there is: Twitter is still better, because you get 100 voices and you can put together an analysis, but you still have to chase the news and the updates, which is just not good for a person. 

Starting in the early 2000s Merlin used to have a tab-set that would open 6 different news-sites in a window. All it took was hitting the little key and he could have 35 minutes of sadness and irresolution in his life that would only be slaked by going back a few minutes later and doing exactly the same thing again. 

John wishes that the news would be delivered once a month in a brown package wrapped with white string along with some other staples, like some eggs, milk, lard, salt, and a rasher of bacon. The office supply store should re-brand as Staples ”We bring you the basic shit you need once a month” They could wrap the lard, sugar, bacon and coffee in the news and you open it up, put your stuff in your dug-out cooler underneath your cabin and ”Look at this! Holy cow! The Japanese bombed Pearl Harbor!” and that is about as current as you need to be.

If you just got a round-up of the news you would realize that everything seems just about the same. Was there any news out of Gaza in the last three months that couldn’t have come out of Gaza in 1972? John could be reading the newspapers from the Nixon administration! He does not want to keep his anger fresh, but he wants his anger to be the stalest thing in his cupboard and yet he is stoking it all the time and he is stoking confusing emotions in him. 

It is not healthy to see so many opinions! We like to think that the more opinions you get, the more well-rounded your eventual take on things will be, but in fact the more polemical opinions you read, the more it feels like all opinions are equally dumb and unfounded, partly also somewhat recently updated. It is not an opinion of 5 years, but it is an opinion on a 1-2 degree change of something that is really awful and that you can’t do very much about today. It also has a self-selecting bias of making you more and more unconsciously seek out the little sub-tunnel that goes through the stuff that you agree with, which makes you angry in the way that makes you feel happy. 

How do you feel about killing babies? Which babies, the ones who deserve it? The concept of killing babies is such a universal, galvanizing, flaming sword and it fits into a tweet so well: ”Killing babies is bad!” SEND. John looks out the window and scans across the city and thinks about all the dead babies through history. There have been so many dead babies! Palettes of them! Is that really a thing that you are going to have motivate you in a day? To prevent it, to think about it at all? Not to say that to avoiding thinking about it is good. 

Watching national and local news at night is something that old people did and that Merlin’s family did. Having been out of that particular palette of babies for a while and going back to it you see how awful the reporting is on every level, not only in terms of the subject matter, but also how narrowly it is presented. If you got your impression about life from watching your own local news, you would understand that there is a 50% chance of rain and society is falling apart. 

You get a really clouded idea how often a given thing happens. You see the same mug-shot of a black guy who supposedly stabbed somebody, or a young person in a parka standing in a dark parking lot with a microphone, telling you about an apartment fire where 4 grandmothers and a cat died. That is not essential information, but it is meant to stoke a thing in us where we feel on the verge of catastrophe at all times. 

John does not want to know about the latest apartment fire because as an empathetic person he needs to make sure that his empathy is channelled in the right direction. News like that is opening the spigot on his empathy maple and is letting the empathy syrup drip onto the forest floor and John is not collecting it in a bucket and is not putting it over delicious pancakes.

+ John looking at his phone too much (RL122)

John is very worried that all of the self-help manuals he has digested during the last weekend point to the inescapable conclusion that maybe the best practices are to log off. If John was someone who was not given to abuse of substances, maybe he could be someone who did not abuse the Internet substance, but he is a substance abuser and the Internet is another thing that he can’t resist. 

Lately John has been asking himself the question: The world is going by, his child is growing up, the water is boiling over on the stove and John is sitting there, staring at his phone. A voice in his head is conscious of it and tells him ”Look at you, you are sitting here staring at your phone while things are going awry!”, and the retorting voice says ”Well, what would you have me do? Not look at my phone?” John doesn’t have an answer for it. Living in this world of social media, which he perceives as important to his career, his self-expression, or his sense of belonging, what would he do? Not staring at his phone? It is impossible! He went back a step and asked what Facebook had done for him lately and he didn’t have an answer.

What would happen to John when he stopped going on Twitter? He had a lot of knowledge come at him in the last few days and his brain and his synapses is open and he want some of this patented hotdogsladies computer-thinky and culture-thinky wisdom. John worked at a newsstand for more than 3 years (see [[[Employment History]]]), the longest job he ever had, and he subscribed to The Economist.

One of the best things a person can do to stay informed is to spend less time seeking out news sources that are about what has changed in the last 12 hours and find a source like The Economist. You can read those first half dozen pages with a paragraph or two about what is happening all over the world. You don’t read 5 different encyclopedias either, but you read an encyclopedia enough to know what to look up next. Whatever one’s source is: You didn’t even know the name of this country in Africa but they are having a problem down there, which is something Merlin didn’t hear about with the firehose jammed to his mouth. The Economist is great for that because they do talk about the world.

Could the people who are intentionally or unintentionally spending 3-6 hours a day consuming what has changed in news in the last few minutes or hours find the time to sit down once a week to read all the way through even just that digest of information about what is actually going on in the world? That is not the only thing to do, but it is a good place to start. 

What is different about the Internet is that there is a semiotic discussion happening simultaneously: You are not just reading the daily news, but the daily news plus every single editorial column in the country including a wide number of editorial columns from the nation of Islam and from the socialist worker’s paper and some written by High School students. John is having semiotic overload, he does not want any more analysis, he is not getting anything out of it, he doesn’t feel like the ball is being advanced culturally by every single person expressing their doubts and confusion in the form of what they imagine is the real solution. 

John follows a wide swath of people and thinkers, he tries to have a really wide ranging balanced group of people, but that produces a Twitter feed saying that the Israelis are the most evil people who have ever lived on the face of the Earth, they are just like Hitler, absolutely the next tweet says that the Palestinians are using children as shields, the next one is about the Ukrainian government being infiltrated by Russian spies and literally the next tweet is about Russia having the right to maintain the integrity of their own borders. John is reading all of the propaganda from every sect, almost like getting your information from amateur press releases, but every 15th one will be a professional press release that seems to be incredibly mealy-mouthed and non-committal in comparison. 

John doesn’t want to be in the parliament of the moment! He wants to read the historical record and he wants to sit in the Algonquin Hotel with a bunch of smart people and listen to their witticisms of things. Instead he is listening to the minister of information of one million sects, most of them being a sect of one, four or forty. Over on Facebook he has the ministers of information for a thousand different families he doesn’t care about. You got palettes of babies on Twitter and fresh and happy babies on Facebook. Babies are the linchpin. You are being shown that they are being swaddled in privilege and you already know they have to really confront that later. 

John doesn’t want to be part of that conversation. It is not interesting anymore, but he once thought it was, which is crazy! What can you add to it and what can you derive from it!? He is trying to write his own thing, but he is just another person, he doesn’t want to be another person, he has never been another person! John is this person, he is the person, not a person!

All this happens mainly on John’s phone. He has recently moved his desktop from his house to his office which has been very healthy. He is no longer sitting at his desk in his home at 3am because it is not as fun to sit on your phone and read the biography of Wellington. John originally only did Twitter from his desktop computer because he had a flip phone, and he might go back to that, but that feels like only drinking brown liquor or only drinking beer on Wednesdays. The first line of defense is to turn off as many notifications as possible and to move the attractive nuisances further away from the front page of the phone into a little folder.

When John opens his phone he looks at his email and he generally has a feeling of dread while the email is loading. Then he goes to Twitter and he generally has a mild feeling of dread while his at-replies are loading. Which person is going to yell at him about something he wrote two weeks ago? Then that feeling of dread goes away as he reads a bunch of at-replies that are trying to enlist him in some kind of hijinks, it goes from bad to great when he reads some praise. It is up and down. After that he opens Facebook and almost half the time the app is slow to load and right now they want him to download their Messenger app, which he refuses to do.

John has a backlog of messages that people have sent him in Facebook that he cannot read because Facebook insists that he downloads their thing. Maybe some of those people are time sensitive, but sorry, they shouldn’t have tried to contact him on Facebook! The fourth thing he does on his phone is to play Solitaire. John does not have enough video games and Merlin suggests Threes, which is a solitary, apolitical, unemotional waste of time. John used to play Mahjongg, but he had to take it off his phone because he did it exclusive of almost everything else. Before that it was Tetris and before that it was Minesweeper. If you could get paid to play Minesweeper John would be the richest man in the world because it is all he wants to do, really. 

+ John’s early days of using Social Media and the Internet (RL122)

At the height of his music career John was actively discouraged by a lot of people from going on the Internet to participate or to comment on things, because the initial idea was that he was a Rock star and he needed to keep a mystery around himself. He wouldn't want to be too accessible or his fans would feel that he was just a regular person. After getting this counsel from all kinds of people John formed a very strongly held opinion about that because he didn’t want to be on there like a regular. 

John's impression of arriving in Rock through the late 1990s was to get a private jet with the Zoso-symbol on it, but he was not going to sit there responding to comment threads. Robert Plant never wrote a letter back to a kid, he would just slap you in the face with his dick or he would send you a fish in the mail. Not only would John’s jet have Zoso written on the side, but it would also have a sunken living room it with shag carpeting. 

Around 2007 John abandoned that idea and went onto the internet. He tiptoed into Twitter and he was sending tweets that were intentionally a different side of himself: Funny surreal little tweets where he never ever referred to music, let alone his own music, but he was talking about the mundane stuff around his house. He really enjoyed it and he enjoyed the response. It was a different time back then! 

There was a time where John absolutely felt like being a participant in the Internet was advancing his personal brand and his career in a way. People knew him differently now and he was no longer reliant on the small handful of music magazines writing about him to get his message out. He did not have to hide and create mystery about himself because he was intrinsically mysterious enough to be interesting. 

People started asking him to do things, he started to get work and he was able to maintain a career even in the absence of releasing new records. The Internet felt like a great place! Some people in Rock’n’Roll were mad that John was wasting time on the Internet, some of them were jealous that he was able to do that and a lot of his fellow musicians were envious about this other life he was able to have on the Internet because they would go on Twitter and didn’t know what to say. 

They were grateful for the fact that their message had been masked and they were grateful that they only had to give 5 interviews a year. Going on the Internet was like giving an interview every day and that was their worst nightmare, it was watering down their messages and it was revealing something about them. Their music was the most interesting thing about them and the music was the message. If you took away that, they didn’t have anything to say. John still meets people all the time who are telling him that //he// can do that because he seems to have things to say, but //they// fail at it. Those are all those Twitter accounts for bands that are only tweeting about their upcoming shows. 

At a certain point it became inescapable that the world had changed and John had changed and now he does not feel unique among entertainers anymore who have found a separate voice on the Internet. The group-think and the reciprocal echo-chamber-y nature of all of the places that exist right now are very common and John not only doesn’t feel special there, but he doesn’t feel like he is getting anything out of it. Maybe he needs to go and live on a mountain top? He would still talk to Merlin from his mountain top on his short wave radio. 

When John wrote Pretend to Fall, he was living in an apartment in New York City at [https://www.google.com/maps/place/Lexington+Ave+%26+E+118th+St,+New+York,+NY+10035,+USA 118th and Lexington], Spanish Harlem. The first Long Winters record had not come out yet and at 32 years old he was convinced that he was completely over the hill. Whatever relevance he had was going to be a kind of quaint relevance and he was living in a neighborhood where nobody would have recognized him as anything other than that guy who vaguely looks like a cop who comes in every day and gets two donuts. 

The people at the coffee shop at the corner where he got his coffee and donut every morning didn’t speak English at all and in that place of total anonymity he did not feel alone or disconnected, but he went home every day and wrote the songs for his next album. John has not really had that feeling of the freedom of no expectation since then because the moment the first record came out he imagined that there were people who had expectations of him and then there were people who did. John gives those people way more weight than they deserve because ever since then he has been writing for an audience as opposed to just writing for himself. 

Merlin finds this pretty perspicacious. When he was writing something, he always spent 3 times as much time as it took to write the thing to look at the stats how well the thing did. What would happen to John psychologically if nobody gave him any praise for a month? What would happen if he worked on something and saw things with his own eyes for the next month but shared them with no-one? It feels unheard of, like Johndeck (?), some kind of crazy person out in the woods with his toy piano and his reel-to-reel. 

If John saw a duck carrying a banana, he would not take a picture of it, he would not talk about it and he would not come up with a third funny thing to say about it, but he would just see it and go ”Huh!”, just like he used to. He would get on with his fucking life! Right now if he saw a duck with a banana outside the window and wouldn’t get a picture of it, he would be like ”Oh, fuck!” and his whole week would be ruined because he had the phone pointed at it and his fucking iPhone shut down because it only had 29% battery power. 

+ John’s unfinished projects (RL122)

++ Graduating from college

For the last ten years John has always had five outstanding projects. He made three full-length record albums and an EP, he has produced some number of podcasts with Merlin and he has written some newspaper articles, but for that entire 10 years he had a post-it note on the wall that says ”Graduate from college!” The edges are curled, it is all yellow, the ink has faded and John looks at it and thinks that it would be very easy for him to graduate from college because he has all the credits necessary and he just needs to fill out the forms, hand in one paper on Karl Marx that he can copy out of Wikipedia, and submit his ungainly thesis which is written, but which he withholds because he doesn’t feel that they have earned it.

Eight times in the last 15 years John went down to the University at the start of a quarter and said to his friends down there that he really wants to graduate from college this quarter and they tell him that he need to hand in that paper about Marx and he goes ”Oh, god damnit!” and he goes home and does nothing. During all those years the people at the University have changed. His original advisor passed away, the chairman of the department handled John for a long time, but he retired, guys who were PhCs when John started were running the department and now they are retiring. The next generation of professors are starting to retire and John is still going down there with the same problem.

++ Writing a book about his walk through Europe

Then there is the book about his walk. 

++ The 4th Long Winters album

There is the 4th full-length Long Winters album that has been completely recorded except for vocals for 4 years. 

++ Wanting a television show

Then there are the various other projects that he could describe. They are fully formed in his mind, but they have never had any boots on the ground. Now there is this new concept of why he doesn’t have a television show. Some people he meets in an airport and talk to for 5 minutes are asking why he doesn’t have a television show and he is ”Fuck! Why don’t I have a television show?” 

John did not have a post-if note that said ”Have a kid!”, but he went ahead and got that done. What is keeping John from finishing things? Right now it is insecurity more than anything. What if he finishes it and it is bad? Where is this coming from? The Internet is not helping him to feel less insecure. Insecurity is not John’s bug-bear. He is important and he is meant to be here! He is a legacy, his dad was a very important man and John is here to fulfill his dreams. 

++ Wanting to be a US Senator

Number five on John’s list is to be a US senator, an idea he has not abandoned yet. 

Merlin thinks John needs to look at video games. They are the talcum, but it is not a panacea unless you are writing an iOS app called panacea. Merlin just sent John a link to Threes and recommends him to download it, but move it off the first page. Maybe these post-it notes are helping? It would be an indictment to take them down because maybe he is giving up? 

John has gotten a lot of satisfaction out of not graduating from college. There are lots of cocktail parties where everybody is comparing the smart college they have graduated from and then it comes to John and he is the only one in the room who hasn’t graduated from college, which is a little bit of reverse snobbery and a point of pride. But does he want to go to his grave not having graduated from college? Just so that his father and his uncle and his uncle before him can all rest in their graves and breathe out? You should just have it, especially having done all the work. If John would intend to take that post-it note down, then why not now? Why would he let another 10 years go by? That is a question he can’t answer!

+ Visualizing John’s thought time line (RL122)

Somebody asked John the other day how he pictures the narrative in his mind. They see their thought time line go by like a ticker-tape, like a scroll on CNN, but John sees his aware mind as a kind of very thick foggy moor and there are faces coming out of the fog and some of them are close enough that John can discern their features and they are speaking the loudest. There are more faces farther away that are less distinct and he can hear their voices, too. There are faces that he can just make out the outline of their features and in the distance there are voices where he can’t see the faces, but they are still audible. The ones that are proximate keep changing, some step away into the fog and other ones come forward. 

It is a whack-a-mole on a foggy moor of different people saying different things. One of them just lives in the moor and every once in a while walks forward and says ”College!” and then steps back, turns his face away and John can’t see who it was. He might have other jobs, he might walk forward every once in a while and say ”Marriage!” or ”Retirement!”, he might have 7 things he says, but how is John ever going to quiet that guy? He is Welsh! Hand him a diploma? He will be ”College!” and John will say ”Here” and he will be ”Oh shit, thank you!”
















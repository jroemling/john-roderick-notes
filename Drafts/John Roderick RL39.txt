RL39 - Darth of Options
2012-07-25

This week, Merlin and John talk about:
* Difference between the best $15 meal and the worst $15 meal ([[[Food and Drink]]])
* World’s strongest man ([[[Factoids]]])
* Zenith Space Command remote control ([[[Technology]]])
* Having your naked reflection in an eBay picture ([[[Stories]]])
* Thinking about buying a Pontiac Trans Am with the license plate BONE ([[[Cars]]])
* Thin Lizzy ([[[Music]]])
* Cars in John’s neighborhood ([[[Cars]]])
* John having a mother raccoon and four baby raccoons in his yard ([[[House]]])
* Being nerdy with words ([[[Early Days]]])
* Cop Cars ([[[Cars]]])
* John’s original tour van blew up ([[[Cars]]])
* John’s car situation, the three hoopties ([[[Cars]]])
* John’s distinctive musk ([[[Hygiene]]])

**The Problems:** 
* Just makin’ sparks, referring to John’s favorite thing to do late at night: Driving with his tour van through the factories at the Duwamish river where there were people making sparks with a carbide band saw.
* pondering a fourth hoopty, referring to John having three cars and thinking about also buying a Pontiac Trans Am.
* teaching some loping raccoons the consequences of being a poet, referring to John having a mother raccoon and four baby raccoons in his yard, two of which were loping far behind and John could go in between.

The show title refers to John learning a lot of big words when he was a nerd as a child and often not pronouncing them correctly. Darth of Options was supposed to be Dearth of Options.

Merlin sounded a little hoarse, a little lady horse with a punk pony throat. Has he been eating cats? Just the dead ones! He was out late last night, but he was not partying and he did not have any 2am subway sandwiches, but he had a steak like a gentleman, which was pretty good //(2am Subway is a reference to story in RL268)//.

Merlin’s dad had a very heavy Pontiac Catalina in a really ugly green color. 

Once during the show Merlin actually called John for Dan and John pretended to be really upset about it.

Merlin once found a melted Highway to Hell cassette in his garage. John has a friend who is making a coffee table book of certain kinds of cassettes that is going to be very beautiful.

When it comes to Superheroes, Merlin doesn’t want one that is just strong or fast or wily, but he likes the combination. In this case you got the strength of the Hulk and the wiliness of somebody who is not the Hulk.  Merlin wants a bit of sarcasm in a Superhero, bordering on cockiness. You fall down, but you get back up!

+ Difference between the best $15 meal and the worst $15 meal (RL39)

Not too long ago John posted something on the Internet and now people are dressing up like Steaks & Hitler. It was in a very short format and [https://twitter.com/johnroderick/status/228195290866397184 John said]: ”The crazy quality gap isn’t between the cheapest and most expensive meal, but between the best $15 meal and worst $15 meal.” ([https://twitter.com/ploafmaster/status/327168804205522944 thread about it]) 

Merlin thinks there are probably 12 wise things in that tweet. It is one of the interesting things that economics cannot account for: Why is this $15 meal in this town the most amazing meal you ever had and this $15 meal across the street is like eating fresh abortion? How is that possible? There is probably a Greek name for the thing John did by taking it and turning it and putting a little twist in it.

+ World’s strongest man (RL39)

Merlin was reading about the world’s strongest man. You could be a little strong or be really strong, but there is some kind of scientific excitability in your muscle parts that allows them to quickly go in the same direction at the same time, like a bunch of Panzers, and when you do like this guy does you can lift an SUV seven times. As the strongest man in the world you can never rest because you can’t be the strongest man in the world for 15 years. People are always going to pick a fight and want you to lift their car. You could handle drunk guys in a bar, but a suburban dad with a Aerostar?

+ Zenith Space Command remote control (RL39)

The Zenith Space Command were the enemies of GI Joe and his friends and Josh (from Barsuk records) just signed them. They are based in Brooklyn, but it is actually Bushwick, they call it East Williamsburg. 

It was the first popular consumer remote control and made a huge loud click when you touched it. All it could do was go up in the channels, which was not so big a deal when you only had 4 channels like Merlin. He used to have a changing stick so he could still observe his mother’s demand that he not sit in front of the idiot box and be slightly away from the idiot box. It uses an ultrasonic pitch that the TV picks up. It was a great name and you don’t hear names like that anymore.

Old people would hang on to giant console TVs with record players on one side and a wet bar shaped like a globe on the other side. 

+ Having your naked reflection in an eBay picture (RL39)

In the early days of eBay there was a famous ad where a guy was selling a silver tea pot and you could see his erection in the reflection in the picture. It was a very heavy man, or maybe it was the concave nature of the pot, but he was in his altogether.

+ Thinking about buying a Pontiac Trans Am with the license plate BONE (RL39)

John had been looking at cars online and is asking for one reason why he shouldn’t buy a white Trans Am with a vanity front-plate ”BONE”. The guy will probably keep that or maybe he just slapped it on there because people on Craigslist don't want to show their license plate and are afraid to get their identity stolen. He has also Photoshopped the picture to grey out his electric meter. People fog out the weirdest stuff!

The car is a white Burt Reynolds looking 1977 Pontiac Trans Am with a rebuilt 400 motor and a silver Firebird on the hood. It looks very heavy, it seems right, but everything about it is wrong. Everett is not a place where you can trust people! It used to be the home of the USS Abraham Lincoln and all of its crew, but it has rotated out and maybe now the Nimitz is up there. It is a Navy town full of sailors and it is probably a sailor who owns this white 1977 Trans Am with a license plate that says BONE.

Merlin imagines the guy at the store looking at license plates and he wonders which ones he rejected because they were a little too pussy or if he thought about if he could live up to RIBEYE JACK or something. He was probably looking for BONE and found BONE. It is not a real license plate, it just says USA at the top and then BONE. It is a front plate in a state where you are not required to have plates on the front. 

For a runaway sitting on their suitcase in front of a motel in the morning, if there will be a white Trans Am with a vanity plate BONE, does this look like an opportunity or a potential grave danger? It depends on which Thin Lizzy song is playing! If it is The Boys Are Back In Town, you should think twice and maybe stay on the suitcase, but if it is Whiskey in the Jar you might hop in. Merlin really liked that version of Whiskey in the Jar because it is very creative. It is an old Folk song and there is also a Metallica version.

There are three or four pink Christmas Tree air fresheners hanging from the stereo knob and Japanese anal beads hanging from a Wu Tang Charm on the rear view mirror. With a vanity plate BONE you have to expect that something is going to hang from the rear view mirror: It does what it says on the tin. It is the air fresheners that are freaking John out. 

Merlin used to accumulate those in his 1970 Volkswagen Bus, as you do. Some people had garters or restraining orders while he would let his green trees do the job. He also hung there some human teeth and the tassel from his cap from graduation, saying "85" on it. His High School colors were green and yellow, but nobody looked good in their colors.

The car has automatic because in that era they stopped putting manual transmissions in cars. Merlin says you need a stick to wipe the ladies off the side. The question is if you can legitimately put a child’s safety seat in a Trans Am or if it will reject it like a bad Kidney. It is a Raising Arizona type of situation and something bad could happen. Obviously there are no airbags or safety features of any kind in a car like this and when you get into a crash it is like a Cat o’ Nine Tails starts swinging around inside. 

Merlin doesn’t want to bring up a sore memory, but it is a situation like it was with the bluing problem on John’s father’s gun: The anal beads and the Wu Tang Clan symbol are not original, the trees are aftermarket, and no way did you get that particular shitty 1980s cassette deck, but the prismatic background on the dash control area is stock. It is called metal-plated although it is obviously a sticker. 

There are condom wrappers on the passenger-side floor. The owner probably specifically bought a bottle of Armor All. Merlin would slide right off those seats that are way too shiny for him. When John would put his daughter’s child seat in the back of this extremely dangerous vehicle, he should probably scuff it up a little bit and break in the seats a little.

There is a lot of responsibility and expectations owning a car like this and every time you hit the highway you would have to spin the tires and kick some dirt up. John has owned a motor vehicle with a very large engine that sounded intimidating before. It is a very satisfying feeling to have a car that makes that typical V8 idle sound. When you roll on someone’s street and they expect you to arrive, they will hear you before they see you and everywhere you go you are just blanketing the world with your power.

In that car John did not need to drive fast, but the real power move was to drive incredibly slow. He basically drove at idle speed everywhere he went, the car would just putter along at 15 mph (25 km/h) and everybody else on the road was too afraid to honk. It was a great life! He would sit way down in his chair so his nose was just on the window sill and he looked like an R. Crumb drawing of some guy with his big nose looking out the window.

Merlin thinks John should bring Jason (Finn) and have him pretend to be a mechanic to see if this guy freaks. There is a $1600 OBO (or best offer) on it and John might be able to talk him down. The ad says ”too much to list” and contains the classic Craigslist spelling error of not putting ”oo” in the first ”too”, so it looks like ”to much to list” It makes John think that not only can he trust this guy because he is a real American, but he can also chip him down on the price because he is a dummy. He needs some help with his commas, too!

With a car like this you put the seat down, you drive like a gentleman, and you jam out to Phil Linnard. Perhaps Sally Field will be standing on the side of the road in a wedding dress. She is all different kinds of pretty, just like Audrey Hepburn or Diana Rigg. A young Valerie Bertinelli is like a young Sally Field and they have the same cute-as-a-bug-ness.

Merlin finds it awkward to talk about women. He has been reassessing his feelings about Scarlett Johansson and it is getting complicated. She might be pretty attractive, even when she was practically a kid in that movie with Steve Buscemi (either Ghost World or The Island).

+ Thin Lizzy (RL39)

If John sends Merlin proof that he really bought a 1977 Trans Am, Merlin will literally buy him a copy of Jailbreak on cassette //(actually the song Whiskey in the Jar is on the album Vagabonds of the Western World)// and  also some old greatest hits or best of for Thin Lizzy. John would also need to get his Iron Maiden cassettes out of his gym bag. 

There was a lot of tumult in Thin Lizzy, it was the times. Brian Robertson was one of the big guys in the band, but he wasn’t getting the credit anymore. There weren’t any big guys in Thin Lizzy, but they were all 5’6” (168 cm), which is how they chose Rock stars in that era: You had to be smaller than this yard stick to ride this ride.

It was before the era in American when everyone was eating Macaroni and Cheese and Tab and was becoming super-large people of the future, but there were still a lot of people from the past who didn’t have access to the vitamins that were in Macaroni and Cheese and Tab. They were small and maybe they were immigrants from countries that didn’t have vitamins.

+ Cars in John’s neighborhood (RL39)

Cars used to be smaller and roads used to be narrower. Merlin’s garage door is very narrow because it was built in the 1920s. How do you fit a car in there these days? The Volkswagen Jetta is designed to be the same size as a Model T. The 1977 Trans Am has a wider than normal wheel base in order to maintain stability at high speeds. For John to get that into his garage he would have to go really slow. 

In John’s neighborhood a 1977 Trans Am would be not even the 15th most intimidating vehicle on the street, just right around his house. Everybody down there is driving a villain car, like the new Chrysler 300 Gangster Car that you can customize in a variety of ways. A guy around the corner owns an early 1970s Ford Ranchero that is all rusty and doesn’t have a muffler. The guy next to John, Patrick, who every once in a while comes out and shoots his gun in the air, has a Dodge RAM Pickup Trucks that you need a step ladder to climb into. The only car they don’t have in the neighborhood is a Toyota Pickup with a machine gun mounted on the back. 

With a white Trans Am John would go back to the top of the pile in his neighborhood. He used to be on the top of the pile when he had six cars parked in front of his house and everybody knew he was a player, but he gave some of them to the people who train Capuchin Monkeys to help disabled people and has fallen in his neighbors’ esteem. None of John's cars were on blocks, but they were all covered with pine needles and had raccoons living inside and mold growing in the rubber of the windows.

+ John having a mother raccoon and four baby raccoons in his yard (RL39)

The other day John saw a raccoon walking through his yard right in the open, which is not customary for a raccoon, and trailing behind her were three little baby raccoons. The first one was right behind, the second one was loping along, and the third one was a real dreamer who was sniffing his little nose in every Dandelion and was taking his sweet time burbling across the yard while the others were already halfway across. Then there was a fourth baby raccoon who was a real rebel and a poet, and who was looking in overturned coffee cans, fully 100 feet (30 m) behind his mom.

John watched those raccoons loping across his yard in the middle of the day and said ”This cannot stand!” He raced out the front door and the two lopers were far enough behind her that he was going to get in between them. The mother made some raccoon sound and all the babies turned and ran up John’s Catalpa tree while she stayed on the ground between him and the barn. John didn’t go right in between them, but he wanted to press the issue and teach the loping raccoons that being a poet has its consequences. He also wanted the mother raccoon to realize that she had lost control of the situation. This was a learning experience for everybody!

She was by the barn with a very concerned look on her face and the babies were all up the tree, hiding in plain sight by sitting really still. They grab onto the tree and turn their bodies in such a way that they become part of the tree and disappear like a Cheshire Cat. The babies understood the concept, but they had not learned exactly how to do it. All four of them were right where a raccoon would go to hide in the crook of a branch, but they were plainly visible, they were not fooling John! After a few minutes of being in the tree they couldn’t resist starting to play with each other and they started roughhousing in the tree, knocking each other out of the tree. 

John sat down in the grass in between the mom and the tree and started to explain in a very calm voice to the mother that he had no evil plan, but he was going to sit here and look at her babies because it was fun. They were all fine and they spent about 20 minutes together. She was nursing, her little tits were pendulous, and she never relaxed, but she calmed down to the point where she understood that this was not an attack scenario with John sitting in the grass. It was still not a very cool scene. John was speaking in a very calm voice in English. He was not using his mind bullets.

The babies were just having the time of their lives by this point, climbing all around the tree and slapboxing each other. John had made his point. The raccoons were welcome in his yard, but there needed to be order and some rules. As he got out of the way she made an imperceptible sound and all four of the babies who had been off in their world and who weren’t looking at her but just goofing around in the tree, all of a sudden their ears all went up. One by one went face-first down the trunk in a very orderly line and the parade went off over the fence and behind the barn.

It was a very nice moment! Those dreamy raccoons still have their dreams, but there were so many lessons in that and they are going to suck it up. This is something John is going to teach his daughter: When they are moving cross-country during the day, she is going to suck it up and get on John’s heels. No grab-ass in the tree, no sniffing in coffee cans, but he wants her eyes on his shoelaces. When John is on foot during the day, he will hug the wall, which is smart, but if he is in the car he hides in plain sight because his motor is saying ”Look away!” 

The mother raccoon had been already thinking about the next fence and how to get across the street, but she wasn’t thinking about that last poet baby. She was future-fucking herself! John wants everybody in his neighborhood, raccoons and crows included, to recognize that the area within his fence is a special zone. His white picket fence in the front is kind of a Trojan Horse: It is very inviting and makes the house look very friendly to the street, communicating ”The person who lives here is a safe person”, but every other fence in his house is booby trapped.

If you can see a raccoon, there are always four more raccoons you can’t see and there are always more raccoons than you think. Raccoons and crows are very similar to one another, they are territorial, but they also travel in groups and think ahead in most cases. This mother raccoon and her babies are now all going to be forward-thinking and they are always going to have one eye looking out. 

The raccoons recently discovered Merlin’s compost can outside and figured out how to knock it over every night. Merlin has been living there for 12 years and he never had this problem until this week, so maybe this is one of these crow things where the word is going to spread //(see RL38)//.

+ Being nerdy with words (RL39)

John has never been in any nerd camp. He was so nerdy he wasn’t even a nerd, but he was an old-fashioned nerd, which meant that he tried to learn big words and read books. As a 10-year old he thought adults considered him a peer. Merlin thought the same and there was no question he was at least as smart as all these people. He used words like serendipity, but he pronounced so many of them wrong. 

The other day John said to a friend that he had a Darth of Options, she said it was pronounced dearth and john said ”Fuck you!” He was pretty sure this was one of these words with multiple acceptable pronunciations, but she had an electronic phone and she put it in his face that dearth was the only acceptable pronunciation. Darth sounds way better and John will continue to say that! People who say they misspoke deserve a kick in the balls. Merlin says the difference between mislaid and lost is that you lose your virginity, but you mislay your keys.

+ Cop Cars (RL39)

The (Ford) Crown Victoria was //the// cop car. People who had a lot of weed in their car looked in their rear view mirror a lot to see if there was a cop behind them and they became quite good in the 1980s at knowing if that was a ski rack or flashing lights. When you saw rectangular headlines, you knew you had to start checking, when they were round it was probably some fruity European. 

Many cop cars in the 1980s cars were Chevy Caprices. When Merlin sees a Crown Vic, he knows it is a cop car, while a Chrysler LHS is not a cop car, but might be a cop’s car. John’s dad had a Chrysler LHS and in that case it was owned by an attorney. He agrees that it is a cop’s car or a cop’s wife’s car and it is the ultimate blend-in car because it is so featureless.

Merlin continues to talk about the Crow Victoria. Nowadays cops are using all kinds of Dodge Hotrods that look like coke dealer cars. They are not only anonymous in silhouette, but they can come right up on your ass, as it happened to John the other night as he was speeding along on a section of road he knows it is safe to speed on. He was getting past the dumb people he has to share the roads with and there was a guy right on his bumper who wanted to play. It was some aggressive 3am driving! John was in a fast-but-calm mood and pulled his hoopty over one lane, like ”I’m going to let you go by!” because they were obviously a hot-dogger.

This guy pulled up next to him and he was a fucking state trooper! They were both hauling ass down the road and he glared at John, like ”You motherfucker! You really think that you can just drive whatever speed you want?” - ”Oh, hello officer!” and John took his foot off the gas and faded back out of his sight while the cop really stepped on it and flew off into the night. He had somewhere to be, and only wanted to take a little minute to give John the jeebs. The cop was in some Dodge Charger, a car that any guy who just got out of the navy is going to have. 

Starsky & Hutch had a Ford Torino. Merlin’s grandma had an AMC Javelin because Merlin’s grandfather used to own a Nash dealership in Cincinnati Ohio that would later become an AMC dealership. Merlin doesn’t know if his grandfather owned it because he could make shit like that up and kids couldn’t check it. John’s dad shot down a Japanese Zero with his 45! Profiles in Courage (Book by John F. Kennedy)! 

The Dodge Charger is a really stupid looking car, but the cops are using these things now and they are hopping them up with all kinds of hot paint. This car is chicken and waffles and really doesn’t know what it wants to be. Hot-rodders are driving them, cops are driving them, and Navy wives and stuff are driving them, it is very confusing! The cops in Seattle are also driving SUVs and it is hard to look into the rear view mirror and know what you see. They continue talking about what they think about different cars.

The Dodge Charger’s lights look like the eyes of an anime character that is mad because somebody has bought their daughter’s underwear. There is a whole name for it. The eskimos have 50 words for snow and the Japanese have one word for every single weird thing you can do with your penis. The Germans will just slap those words together and put Scheisse on the end of anything that is a sex thing. There are stores called Burusera Shops where young girls amongst others can sell their used undergarments. They continue to talk about those for a while. The old version of the Wikipedia page about this topic lists more items than the most recent one.

+ John’s original tour van blew up (RL39)

John’s original tour van blew up and he is still devastated by it. He gave it to the people who were transporting Capuchin Helper Monkeys. The Ford transmission just ate itself at 300.000 miles (480.000 km), but the Triton V10 engine was absolutely still running. John would put it in any powerboat he was building! They surely replaced the transmission in the van and it went to live on a monkey farm, it is probably the vehicles they use to teach the monkeys to drive. It is fine to drive around the compound. John left all of the ”All Access” passes from 15 years of touring that they had plastered inside the van and the monkeys surely treasure them and imagine what it would have been to be backstage at a Decemberists concert in 2003.

John’s van blended well into industrial areas. It had tinted windows and if you drive it through a suburban neighborhood, every mom comes out and ushers her kids back inside. It is a little bit too innocuous and it seems like you are either a government spy or somebody who is a little bit rapy, but you can’t be a little rapy. There are factories lining the Duwamish river in Seattle all the way along and one of John’s favorite things to do at night was to drive down into them, drive through them along the river and go from one factories to the next. 

It was a place where fork lifts are and where nobody would ever think to drive, meaning it hadn’t occurred to anybody to block it off. John was actually driving through factories while they were making lead ingots, or whatever they do in factories. At 2am there are guys down there on night shift and all they do is making sparks. A guy with a carbide saw blade was sawing lead ingots and making sparks and John is convinced that this is all they do! It feels like a Billy Joel video from the mid-1980s with some guy with a hard head at 3am who is just making sparks. 

John used to drive his van down through these factories, in through the loading door, and driving through this 50.000 sqft (4600 sqm) building with a dozen guys making sparks. By the time they look up from their carbide saws and go ”What the fuck was that?” John was already out the door on the other side, driving through the next building. In that van John looked like he was on official business, like a guy who belonged there, delivering new carbide bits. They don’t wave you through, but they don’t even see you. John used to love that, it was one of his favorite late-night activities, but when the van went away, try doing that in a black Jetta!

+ John’s car situation, the three hoopties (RL39)

In August of 2012 John had the choice of three hoopties.

++ Volkswagen Jetta

The first hooptie was a black 2000 Volkswagen Jetta with a V6 motor, a 5-speed, about 80.000 miles (125.000 km)‚ and in fairly decent condition. It was the car he used to zip around with. 

Merlin also has a black Jetta. That car looks like you are hiding something. It looks like you have pulled over to do a line of Chrystal Meth off of your tee (?), and the only place you belong is at a Morrissey concert parked in the parking lot, doing bumps of shitty crystal off of your tee (?). A black Jetta means nothing! The next one up is a Passat, which is what Merlin’s wife would want to have if she had a more successful husband. Their mutual friend has a very nice Passat and it is a very comfortable car.

++ John's dad's Chrysler LHS

The second hooptie is a 1997 blue-green Chrysler LHS model, a large sedan that you have to be 75 years old to buy from the show room. It has power windows, power breaks, and leather seats. John inherited this car from his dad. At one point the band Mumford & Sons were in Seattle and they wanted to ride around late at night and have a great Seattle time after the show. John invited them into his Chrysler LHS and all four of them were in the back seat of this car, sliding around on the big leather couch, marveling American cars! John found those English people so cute and charming. They didn’t want to go to a restaurant, but they just wanted to drive around in John’s big American car and play slap and tickle with each other in the back seat.

John gets into a lot of arguments with his mom because he is calling it the blue car, but she insists that it is green. It is blue/green, but it is shading to blue. They have never resolved this! Merlin finds it amazing that John can’t get his records finished. 

Merlin looked up the available colors of touch-up paint (probably [https://www.paintscratch.com/touch_up_paint/Chrysler/1997-Chrysler-LHS.html from this website]): It is not Drama Gold Metallic, Black Crystal, Stone White, Brite Platinum Metallic, Wildberry Pearl (which is a pole dancer name),  Candyapple Red (John’s favorite Grateful Dead record, but it was actually Hüsker Dü), but now they have Deep Amethyst Peral (clearly a midnight blue) up against Spruce Pearl Metallic (slightly darker Gumby green). Merlin sent it to John and he says that it might be it. [http://gtcarlot.com/colors/Chrysler/All/LHS/ This website] is calling it Spruce Green Pearl Chrysler Color. There is another sample on [http://www.automotivetouchup.com/touch-up-paint/chrysler/1997/lhs/ this website].

++ John's mom's Peugeot 604

In the 1980s John’s family went foreign like a lot of people did. You almost had to because there was a crisis in America and American cars were so bad they were literally falling apart on the lot. John’s dad started buying Audis //(see RL26, Audi 5000)// and John’s mom cycled through Peugeots for a while. She had a Peugeot 604, a beautiful car that was beautifully made and it handled beautifully. You don’t see them anymore because they have all rusted away and they didn’t make very many to begin with.

John had talked his mom into buying it because he was in High School, they lived in a nice neighborhood and they needed a car that represents, they needed a foreign sedan, but not some Mercedes, Audi or Volvo that everybody had. John talked her into this crazy car. Anytime you went up on a curb you dented the titanium rims that cost $5000 to polish, it was a completely impractical car, but it was very fast.

From the 1940s and up until that point in the 1980s both of John’s parents had been Chrysler Dart Mopar people and they always had some kind of Plymouth or Dodge. John has two Chrysler products now because he has inherited them from his parents.

++ Chrysler Sebring Convertible

The third hooptie is a white Chrysler Sebring Convertible, the Stan Smith tennis shoes of cars. John still wears Stan Smith tennis shoes and he drives around in a white Sebring Convertible and people think he is a middle-aged gay man. John is happy to be mistaken for that because who else with a big beard would drive a white convertible around Seattle?

If a runaway was sitting on their suitcase in font of a motel and John pulled up in this white Sebring Convertible, they could hop in and would be perfectly safe because John will be listening to Stealers Wheel on the stereo and nothing bad can happen. 

The Sebring is John’s cruising hooptie for sunny days. It has a V6 and is no shirker and John lives in a black neighborhood and gets respect from the middle-aged black guys who think that the car has a little bit of class and John is a smooth operator. His shoes also match his hat.

John thinks he needs to add a 1977 Pontiac Trans Am to this collection and to Merlin there is no fucking question that there is plenty of room in this line-up for a 4th hooptie with shiny seats and lots of air fresheners. 

+ John’s distinctive musk (RL39)

John's sense of smell is so good it haunts him. He has a very distinctive musk which people equate with masculinity, virility and sexual prowess. Merlin notes that John has these amazingly Victorian euphemisms that he only uses when he is discussing himself. John's old shirts are prized by young women in the Northwest. If they can get their hands on one they will wear it to bed at night, they will luxuriate in that musky smell, and they will say ”I would like to make babies!”

The other day it occurred to John to sell his shirts on the Internet, but the question is if he should stay anonymous and say that he is just a guy who got some shirts for sale to clean out his closet, or if he should come forth and say that these are his shirt and risk that some creep buys them just so that they can wallow in the musk. Merlin thinks it got to be made clear that those shirts are from John Roderick, but his advice is to not sell those shirts because of DNA. If somebody buys a known John Roderick musk shirt there will surely be a little bit of dander.

John’s washing machine is one of these fancy washing machines that has a "sanitize" setting, what if he sanitized it? He has never used it and doesn’t know what it means. It might be a centrifuge that he can use to make some Uranium-235 and make some sparks, or Uranium-236, whatever it takes (they said ”Whatever it takes” simultaneously and broke out in hard laughter).


















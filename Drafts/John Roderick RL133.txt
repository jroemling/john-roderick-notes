RL133 - Secret Pop Songs
2014-12-08

This week, Merlin and John talk about:
* Waking up in the morning ([[[Sleep]]])
* John’s holistic thought technology ([[[Attitude and Opinion]]])
* John's belt buckle collection ([[[Objects]]])
* John’s various collections ([[[Objects]]])
* Multi-tools ([[[Objects]]])
* John’s clunky 1997 shoes ([[[Style]]])
* John's life hat ([[[Style]]])
* The Small Bag ([[[Small Bag]]])
* John feeling free after losing his bag in Avignon ([[[Small Bag]]])
* Dave Bazan always wearing the same clothes ([[[Small Bag]]]) 
* Getting his shoes shined at the airport ([[[Small Bag]]])
* What to grab when the house is on fire ([[[Small Bag]]])
* Twins ([[[Children]]])
* Naming a child ([[[Children]]])

**The problem:** John needs a more profound small bag, referring to their conversation about having a small bag packed.

The show title refers to a chanson John was singing about the bridge of Avignon that in reality is about the pope, and there are so many secret pope songs.

It is going pretty good! John sounds like he is on the mend, but it is hard to say. When he woke up this morning he couldn’t tell whether he was sick or not, but now he is having some coffee and he is coming to live. 

+ Waking up in the morning (RL133)

John has been polling his friends what time they get up in the morning and they all said they get up at 7am. Merlin awakes from sleep at 6am or a little before whether he wants it or not because that is when his daughter wakes up. His wife lets him sleep a little later most days, but left to his own devices he will wake up at 7am or 7:30am. People talk about this like this is normal behavior and everybody seems to go to bed at 11pm.

John was in a café today and saw a couple of people sitting across the table from one another, a guy around 50 and woman about 35. They both had their day planners out and she was saying ”What about we get the ball rolling at 3:30pm?” and he was like ”Great!” and they wrote it down in their day planners. John thought it was great to see people fucking getting things done. They are having efficient meetings and they are deciding to accomplish things. Both of them surely got up at 7am this morning! 
 
This is how John gets out of bed: He wakes up, throws the covers off, sits up, recognizes that the day is there, grabs the covers back, pulls them back over his head, goes back to the burrow under the pillows and hopes that through some magic time will stop and he can sleep forever and wake up finally refreshed and ready to begin the ultimate day, the after forever day. That is how John starts every single day. It does not happen at 7am. 

The other day John was riding in a car in New York around 7am and all the people were out, running and fishing. They didn’t get up at 7am, but they got up at 6am or 5am! John is thinking a lot about 7am right now like it is a brand-new thought technology. It is burning and searing its brand into the haunches of John’s mind: ”7am!”

+ John’s holistic thought technology (RL133)

John is very interested in infrastructure and a lot of his thought technology involve many levels of infrastructure: Personal, public, social, municipal. He is very interested in how this stuff should operate. John is very efficient in a certain way, he is not looking for the onesie-twosie solutions, but for the big ideas and things are not going to change unless we change the way this whole operation works. We will have good days and bad days and we have to think on a much larger scale. 

+ John's belt buckle collection (Rl133)

John realized he has 30 belt buckles and he got a big wooden tray that had probably been used as a typesetter’s tray with little dividers in it that were perfectly sized for belt buckles. He doesn’t always wear a belt with a buckle and when he does, he often sticks to the old classics: The Indian Arrowhead, the cover-art from Appetite for Destruction, and the one that says ”JOHN”. He also has a couple of scrimshaw belt buckles and an Indian head enamel belt buckle that is the size of a rodeo-winner’s belt buckle and looks like a cigar store Indian except it is the size of a pie plate and it is a belt buckle. People used to live a different life! John actually has a rodeo style silver-plated polished Texicano belt buckle with a giant R on it. There was a time when you got a belt buckle when you won a rodeo and it was the way to wear your trophy as a rodeo victor.

One time John was in a bar in the South West and the waiter was a kid with a giant rodeo belt buckle. John asked about it and he said he was a rodeo champ and John had the flash of realization that just like being kind of a Rock ’n’ Roll star, being a Rodeo champ doesn’t mean that you don’t also sometimes have to work as a waiter. The kid was wearing Wranglers and John has gone back and forth with Wranglers over the years, which means that 5 times in his life he acquired a pair of wranglers and thought they are going to be his new thing, but when he put them on he realized that they are made for somebody else. 

For Merlin it is like going vegan: It has never crossed his mind and as soon as he spends any time with it he realizes it is not for him. Jonathan Richman has a whole song about jeans (probably Everyday Clothes) and how they fit differently. They still make Lee jeans, but they don’t have the brand-awareness they used to have. There are different body types which the ladies are very aware of. Men don’t care as much how their pants fit, they just put them on, except artisanals like Merlin and John. 

Wranglers are high rise and ride a little higher. Lee jeans is for a long-distance truck driver and for Wrangler jeans you have to be made out of sticks. This rodeo guy was made to wear Wrangler jeans and he looked fantastic in them with his giant rodeo belt buckle. John actually bought another pair of Wranglers just in homage to this guy. Then he put them on an looked like somebody tried to cover a crime scene in denim. 

+ John’s various collections (RL133)

As long as John is thinking for a bag for his belt buckles, he is probably not quite there yet. He got a rack for the belt buckles and if one day somebody is sitting in his living room and says that they really like John’s candle sticks and his globes, and ask John to show them his other collections, he can stand up and say ”Where would you like to begin? I suggest we start with the belt buckles” and John can bring the rack of belt buckles out and they are all there. Any good tour guide will open by saying ”Have you been here before? How much time do you have?” and when you say you have only 6-7 hours, then we will start with the belt buckles. 

The appropriate place to start is not out in the barn looking at the collection of 5 HP motors, but we start right here on the comfortable couch with some tea and the lapel pins from various secret societies. Then we move to the belt buckles. What about the backstage passes for shows were John was not playing at? That is one cigar box out of about 20 cigar boxes with different content: There are movie tickets from movies from the 1980s, sports events he went to with his dad, backstage passes for shows where he was only as a guest, arranged by level of access, and we can sit all afternoon. John basically had a kid just to have somebody he could force to sit and give this tour once, the entire tour. When she will be 11 years old, John will tell her ”Time to get to know daddy!”

+ Multi-tools (RL133)

John goes back and forth on multi-tools because in a way they seem like jack-of-all-trades-master-of-none. The ones that are actually useful are pretty heavy and the other ones are cute things to keep in your briefcase so you have a bottle opener. John has a Swiss army knife that was made a long time ago. In addition to all the things you would say were on a Swiss army knife to make somebody laugh it also has a magnifying glass, a fire-starting kit, and an airplane signaler, which is a mirror with a hole in the middle. It is the size of a 16oz (0.5 l) can of beer. If John fell out of an airplane with that thing in his coat, he could make it back to civilization with the tools at his disposal, but he is not sure if that was his go-to kit. 

+ John’s clunky 1997 shoes (RL133)

The shoes John is wearing today were a Christmas present from his sister in 1997 and they were probably a big investment for her. They are shoes of their time and in the between-times a lot of shoe fashions have come and gone: pointy shoes for guys, little shoes, and flat shoes. Currently every single guy is wearing some kind of Suede wingtip in a non-canonical color with a blue or yellow sole. We are living in very fancy times! 25 years ago John would have seriously flipped out seeing those shoes, but now they are shoes of their time. If you wear them 10 years from now, people will be able to tell that those were shoes from 2014. It is like the architecture that is flying up all around right now, those new 10-story condo buildings, and they already look like 2013 when it was designed. 

John's shoes are from the chain-wallet and chunky sole years and they are black leather with very chunky soles and a tall heal, but in the front they almost look like Clarks, a chukka style or dessert boot style, made out of pretty thick leather almost like a horsehide. There are surely people of the moment, walking around with Macklemore haircuts and handlebar mustaches who find these chunky shoes very retro because they are from a time when they were 5 years old. John hadn’t touched them in a couple of weeks, he looked at them this morning, he put them on, he is wearing them today, and they feel great. John feels great in those shoes, but they are on the bench, they are in reserve. 

Back in the day John wore them a lot and they are not quite mothballed, but they are in the garage and not in regular rotation. Still, he has to get them out and spend a day or two in them once in a while, but ultimately they are on the trajectory to pasture. They are like a weekend with a middle-aged child from your first marriage. How many of these weekends are we going to have together? John and these shoes had some times together between 1997-2002, but he wasn’t able to say in 2002 ”Fly be free, shoes! Off to the Goodwill with you!” because his sister had given them to him, but they are not eternity shoes.

+ John's life hat (RL133)

John has a life hat that he is going to have for his life, a hat that you over time always go to when it is cold. It is your number one cold hat! After a while you start to be a bit worried about it in case you lose it and you put it away as your good hat. Then you come back and wonder what you are saving it for, you bring it back from sentimental retirement and you realize that this is your life hat and you are going to wear this hat until you no longer can wear this hat. It is not a thing that you accidentally leave in a cab. You are sooner going to leave your phone, because you don’t have a life phone, but your phone is just a temporary friend and it is not even that good of a friend. 

+ The Small Bag (RL133)

John has been toying with a new version of an old thought technology: The small bag. He was thinking a lot about the small bag and what it represents. Part of trying to stay organized and keeping his life together involves a process of trying to figure out what he needs to eliminate. He is thinking about this holistically for his whole life. The question is not what to get rid of to fit into a small bag, but the question is what he absolutely needs. Everything else goes straight in the bin.

Merlin says there are different kinds of small bags. It is not purely a [[[Supertrain]]] thing, but there is a go-bag for any situation like the lock box in Switzerland for Matt Damon with passports, weapons and money. Then there is the go-bag for a hurricane or other natural disaster.

You don’t want to pack just everything. They sell shampoo in Europe, so you don’t have to bring it. They will have toilet paper where you are going and you don’t have to pack it unless you are going some place where they don’t have toilet paper. John always carries toilet paper, but that is just experience and he doesn’t want to give too much away. Also: Do you bring what you need or do you bring the tools to acquire what you need when you get there? The smallest bag would be an American Express Black card unless the grid is down in which case the smallest bag is a Buck knife. 

John is talking about a larger philosophical, more profound small bag. He has a small bag that he will grab when the Electromagnetic Pulse turns off all the microwaves, but he also wants to inhabit another small bag that instead of five white shirts that are all a half-size too small in the neck has one white shirt in the correct size. While they record John is sitting in his clunky shoes and his life hat, and what else does he need? He is halfway through a small bag right now.

Merlin got this stupid earthquake kit, a big metal garbage can that you keep outside the house with supplies for the whole family for three days, like water, energy bars and all that shit that you never want to eat at home. They had forgotten about it for a while and as he looked at it again, the lid had gotten off a little bit and it was moldy inside and all the water had expired! The thing is: You don’t pack a small bag once, but you are constantly packing a small bag, even if it is just in your head. A small bag is a thing that is alive! It is not a static animal, but an evolving creature or a friend. 

People have organization strategies like turning their shirts backwards on the hanger and putting a pin in the neck and after six months they take all the things on backwards-hangers out. Those people have day-planners and get up a 7am or earlier. John tries to touch every single thing in his house at least once in the course of a day and a half. He goes through and puts his fingers on everything ”Are you still here? Are you still here?”  

If you ask people who don’t travel a lot or who don’t do a carry-on to put together their small bag, by the time they packed that bag for the 5th time, they are going to be surprised about what went in the first time. The first thing they put in there: Shampoo, comb and tooth brush, stuff that you can boost from any drug store. Merlin’s wife is watching the Walking Dead show and he ends up having to watch it although it freaks him out, but you can learn a lot from that show because they get by fine with one shirt. You are not trying to recreate civilization yet, but first you need to stay alive.

The best time for Merlin to think about what he really needs for a trip is when he already is in a hotel room and he thinks about what he really needs. A couple of years ago he started a text file and his small bag for packing has become very simple. He doesn’t bring all the entertainment equipment anymore. He likes a large cup to drink water out of, but you can stuff stuff into it. Sitting there stressed out about packing is what makes people jam way too much stuff into any kind of bag. Merlin tries to really dumb all that way down to the most essential stuff he needs. Pretty much anywhere he goes he is near a mall and if he shits himself he can tatter over to the mall and get another pair of pants. He doesn’t need 4 pairs of pants. The best go-bag is the bag you are already living out of. Most people’s small bag is actually their car. 

Merlin has to teach his daughter that she needs a bag pack for things like stuffed animals, because otherwise Merlin is going to have to carry it 10 minutes later. John says it to his daughter, too: Pack in and pack out. Bed bunny does not leave the bed, it is right in the name. There is a car bunny, but that is a totally different bunny. But when they go out on an adventure, she has to schlepp her own stuff, toat her own barge, lift her own bail. 

John admires people who equipped their dogs with saddle bags carrying their own food, like a pannier. There are also people who tie their dog’s poop in a bag to the dog’s collar for the rest of the walk. They claim to be animal lovers, but they have never put themselves in their dog’s paws, imaging if they would like their shit tied around their neck.

+ John feeling free after losing his bag in Avignon (RL133)

For John The concept of the small bag began in 1989 in a town called Avignon, France. "Sur le pont d’Avignon, On y danse, on y danse" is a song about the bridge of Avignon on which they all danced, but in reality it is about the pope. There are so many secret pope songs, you could make a hell of a mixtape of secret pope songs! Merlin doesn’t know that particular chanson, he only knows that there was a pope there and Picasso painted some hookers from there. Merlin refers to himself as the Ax of Avignon all the time, but nobody laughs and it breaks his heart.

Late one night John arrived in Avignon on a train and didn’t have money for a hotel, but he had a backpack with a couple of pairs of jeans, some shirts, a tie in case he got asked to a nice dinner, a carton of Camel Lights, a walkman and some cassette tapes including Traveling Wilburys Volume 2, Tom Petty’s first solo album (Full Moon Fever), a rain jacket made by the Spyder Ski Wear company, some Nike Lavadome boots tied to the outside, and a hitchhiking sign that he had written on a piece of cardboard that said ”Anywhere”. 

The bag had a patch on the back from the Minnesota Outward Bound and underneath the patch there were three $100 notes in a little plastic bag. If worst comes to worse John could rip off the patch and had $300. It had a pair of sunglasses, socks and underwear, a journal, and a camera. This was a bag John had been living out of for months and months, made by the company Mountainsmith. John had everything in this bag that a person could possibly need to go around Europe and America and just be kind of dirty all the time, shower in gas stations, and yet be ready for anything. It was incredible! In Italy John bought a switch-blade in addition to the multi-tool he came with. It sounds like a fun time and John was a hot Tamale.

John went off the train in Avignon late at night and was walking around in the dark. He didn’t have money for a hotel and he discovered a park a little bit away from the train station over by the Freeway interchange. 6-7 guys were sleeping in this park already using their bags as pillows and John thought that these were his people, he was going to pull up a piece of grass and catch some Zs. He pulled his sleeping bag out of the backpack and climbed into it. The majority of the other guys did not have sleeping bags, but they were just curled up with all their clothes on and their bag as their pillow. Having a sleeping bag put John in the 1% of people sleeping in this Avignon park. His passport was around his neck in a little cotton passport bag.

About an hour later John woke up with a guy standing over him. His bag, which he had been snuggling with, was gone. This guy was straddling him with one foot on either side and he was bending over with his face very close to John’s, which was probably what woke him up. He couldn’t really see the guy because it was very dark, but he understood that the guy was trying to find a way to get that passport from around John’s neck. He was prepared to reach into John’s shirt and he knew the fight was going to be on as soon as he went for it. He was setting himself up for the grab, he was going to cut the strap and John was going to wake up, probably scared, but it was going to be too late because John was in a sleeping bag and the guy would already be running through Avignon. 

John woke up, the guy saw John’s eyes open, they looked at each other in the dark and the guy started to run. John was nothing if not able to eject from a sleeping bag like a crossbow bolt. He came out of that bag in his bare feet, already running at 15 mph, and he was right on his heels. He was a thin guy and he was moving fast, but John was right behind him, cursing him and John chased him over hedges and roads and streets, right on his heels. John's feet were shredded but he didn’t care and he felt very close to accelerate that last little bit out of pure fury to take him down. He leapt over a metal railing onto the Highway and John was right on his heels but his toe caught on top of the metal railing, sending him right down on his face. The guy went off down the road at kind of a cool-down trot while John laid there splattered on the pavement. 

John limped back to his camp side and everything was gone. During the chase the guy's friends got the sleeping bag, shoes and everything. It was all in the game. Using John’s powers of perception and a guesstimate, he would say that they were Algerians and this incident was part of the nature of colonialism. John was one of the victims of the fallout of colonialisms and got his comeuppance. He lost $300 in addition to a fucking carton of Camel Lights which even then was $1.25, which was a lot of money. 

John limped over to a hotel, a cheap Mamasan Pension type place and said: ”I don’t have any money and I am covered with blood and mud, but I am an American, how do you do, nice to meet you. I am able to get money because I am an American, will you let me stay here tonight and I will pay you later?” and through the power of being an American, the little madam who ran this Pension was like ”Yeah, that sounds reasonable!” 

The next day John went down to the American Express office and said ”Send me the money so that I can pay this lady in the hotel” and he spent about a week scouring the garbage cans of Avignon. He was missing his journals! He didn’t want the bag, the Lavadomes, the Spyder jacket, he knew the carton of cigarettes was gone and he never expected to find anything, but he thought maybe they would toss those journals in a garbage can. John never found them. He got some shoes at a little place, this was when the dollar was strong against the franc. 

John had his shoes, the shirt and the pants that he had been wearing and he went to a Saturday street market to buy a Greek fisherman’s sweater. At the time every Saturday market in Europe had someone selling Greek sweaters because Greece was still a poor country and knitting sweaters was one of the things they did for export. John was walking around town looking for his stuff and as he got tired he sat down on a park bench and dosed off. When he woke up he just stood up and started walking. 

The tragedy of losing the bag started to fade as John realized that he didn't need any of that stuff. Prior to that he thought he was at the barest minimum of what he needed, but now he had none of it, just these clothes. If he had gotten invited to a fancy dinner he did not have a tie, but he was not going to get invited to a fancy dinner in the condition he was in. For the first time he felt a liberty he didn’t even know existed, the liberty of total possessionlessness. John left Avignon some time in August of 1989 and continued to hitchhike and travel and sleep around and eat and live until Christmas. He never replaced the bag or bought another item. 

John was as free as a man could be. He would wash his clothes in a sink and hang them up to dry, which got harder and harder as the winter came on. In the summer you wash your clothes and put them on and they dry as you walk around, but that was less easy to do in the winter. John must have smelled to high heaven, but he was not doing it for other people to make a good impression. John has been trying to replicate that feeling of complete liberty ever since. 

Imagine you are on a train with a little bag for your life hat and change of socks, and you open your eyes, look out the window and you have arrived in the town you meant to go to, but then the train starts to move and you realize that you slept through your stop. If you don’t have anything, you just stand up, run down the corridor, open the door and step on the station platform, but if you have a bag you have to turn around and get that bag, particularly if you have opened the bag and strewn your stuff around that you have to throw back in the bag. Then the train will be moving too fast to get off and you are going to the next town and your destiny has changed. John is so far from that liberty right now!

+ Dave Bazan always wearing the same clothes (RL133)

At a certain point Dave Bazan realized that what he liked to wear was a black T-shirt, a red hoodie with a white zipper and some jeans. He just decided that he didn’t want to use even a small part of his brain to worry about clothes, let alone a big part if you really want to worry about clothes. Dave eliminated all of this from his existence and this was going to be him. If you see him in a collared shirt, he might as well be wearing a fake mustache, you wouldn’t even recognize him. Who is that guy? he kind of looks like Dave Bazan, but he is in a collared shirt! Dave liberated himself from all these small tyrannies of ”does this thing fit right?” and he has a red hoodie as a superhero costume.

+ Getting his shoes shined at the airport (RL133)

The other day John got his shoes shined at the airport and the shoe shine guy was telling the story that he was a fraternal twin and his sister was 9 pounds when they were born and he was 4 pounds. It was a pretty involved story and he was trying to tell John that he was the runt, but at a certain point he must have recognized something in John and asked if he carried a fork and spoon with him. John was like ”Huh, no, I don’t! How can I get on an airplane with a fork and spoon?” - ”It can be plastic or wood! I don’t go anywhere without a fork and spoon!” There was a time when John did carry a fork and spoon, but is he now too good for that? It was like a wakeup call and he should fucking have a fork and spoon!

A few hours later he was on the subway in New York City. Either everybody in New York City listened to their podcast //(John says something about a story of a guy with the nice boots in the subway in New York City, the guy with dignity, probably RL60 ”Writ in his boots")//, or times have changed because everybody on the subway had some really expensive, pretty cool boots. The era of small shoes is over and now everybody is wearing work boot that do not comport with the jobs they do. It is like a F150 with no scratches in the bed. 

John was wearing his travel shoes, a pair of pull-on beetle boots, those Australian ankle boots. First he was thinking that he should have a wooden fork that he should carry with him in case somebody offers him some stew and a few hours later he thought that he was not repping any boots here. It was Red Wing, Red Wing, Red Wing, Red Wing, White’s Boots, Chippewa Boots, it was like a boot store!

In order for John to be repping his boots in the New York subway he would have had to pack an entirely different small bag. It is funny where you mind can go! Half of his mind was thinking that he needed to get on that plane with a fork, like ”You are going to New York, bring a fork!” and a few hours later he was thinking he should have brought a bigger bag so he could be part of this boot-party here on the F-train. John was like ”No! Quiet down, chorus of haberdashers! I needs a fork that I can take through security and everything else is gravy!"

Merlin got a collapsible bowl from a camping store, the size of a grapefruit with the top cut off and made out of very thin black silicone. It takes up no space and no weight. In John’s experience of eating campfire stew you can always eat out of the pot, but you got to have a fork. In the absence of a fork you can use a sharpened stick, but you are not going to get that through security. John is planning to buy a little wooden fork and spoon, a little camp kit that you can get on an airplane. 

John never would have thought about that if he didn’t have that kismet serendipity experience with the shoe shine guy who was asking the right question while he interrupted himself when he was in the middle of his story about how his sister got all the milk. John was sitting there, getting his shoes shined like a big wheel, looking around at everybody and ”Hey, how is it going? I’m getting on an airplane! My flight’s just in a few minutes, I thought I get my shoes shined”, but immediately he was reset, like ”Fuck! Fuck!” But the guy was right.

 + What to grab when the house is on fire (RL133)

If Merlin’s place was on fire and he could only grab three things, he doesn’t even have his yearbooks and family photos in one place. You got your daughter, your wooden fork and your boots, but what is next? You got to train your daughter to egress the building on her own when you yell ”House on fire! Hit the bricks!” John has discovered that you can get any yearbook on the Internet, so let your yearbooks burn. Somebody wrote on there ”Merlin, stay sweet, see you next year! :-)”, but you have to get the family photos, they have to be in one grab-bag.

John is often thinking about what would be the one thing he would grab if the house burned down and everything was gone, but he couldn’t tell you. Merlin would probably be fine with almost nothing, but that is sad. He did a big purge before they moved. He read a book about getting rid of clutter in your life and it had a big impact on him. The thesis is to not have anything in your life that is not going to get you closer to the life you want, which means systematically and ruthlessly getting rid of everything that cleaves you to a life you never had or no longer have. There are a few things he regrets and he should have kept some of the Rock’n’Roll T-shirts. He also accidentally threw away eight Timbuktu bags because they were all in a contractor bag that got thrown away.

+ Twins (RL133)

Merlin is pretty freaked out by the whole idea of twins. Identical twins are pretty freaky. If you claim that you are an identical twin, we can put them directly next to each other and check. But fraternal twins? People might be making that up. 

Nels Cline is an avant-garde guitar player who is in Wilco right now but has been playing a lot of jazz. He is a very nice man and his brother is an identical twin, but so identical that they are mirrors. His brother parts his hair on the left, he parts his hair on the right. Nels is left-handed, his brother is right-handed. They have very different personalities and in a way they are opposite! There is always an evil twin! His brother is some kind of scientist in California while Nels is a guitar player. You are supposed to name them in funny pairs and if John had twins, he would definitely name them in some kind of book-matched twin name. 

+ Naming a child (RL133)

John gave his daughter a clever middle name and he thinks about it all the time. He wants to take his 3.5-years-ago self and shake him by the lapels, take an arm around him, walk up to the whiteboard and go through some things. ”I love everything about this, but maybe just settle down a little bit” In Merlin’s daughter’s class there are two Aidens, a Jaden and a Kaden. 

When John and Merlin were in school, everybody was named Jason, Todd or Jeff, maybe there was a Rick. There was a Jennifer and a Lisa, a Joe, and a John. Merlin would love some more Kates. John knows some Kates and he is not sure Merlin really wants to know some Kates. Kate is a fucking handful, exactly right! Cathryns are already a problem, and Kathryns with a K are a much bigger problem than Cathryn with a C. As soon as they are going by Cat, get away! 

A Kate is just trying to convince you that she is not a Kat and you should assume that she is reasonable. Kate is a gal who can wear jeans, she will help you clear the yard, she is down with the struggle, but what a handful! She is carrying a fucking pistol! But come on, Kat? You guys have some times! First of all, don’t let Kat know where you live. Merlin always aspired to have a girlfriend named Kate. But Katy is so different! She is a graphic designer. Kate has a foxy garden and she is growing some plants that need to be explained!

















RL345 - Allemande Left
2019-07-29

This week, Merlin and John talk about:

**The problem:** John is a preservationist, referring to the owner of the house John wanted to buy planning to remodel the kitchen and bathroom while John would have liked to keep it as it is because he is a preservationist.

The show title probably refers to John talking about being at the Schützenfest in Freckenhorst where they did some goose-stepping //(see RW43)//

John was still awake at 5am+ for no good reason. As their friend Marco likes to say: ”It is not my fault, but it is my problem!”, which is a terrific line. In John’s case it is his fault because he made a series of decisions along the way, each one of them compound into this. Maybe if he wore hats something would be different, or if he read different books? John hasn’t seen Merlin in a hat, not even in a baseball hat.

+ Merlin’s new anxiety drugs (RL345)

Merlin slept like shit last night, not that his energy is flagging, but it is in the room and John can feel it. He is doing new things with drugs, not deep drugs or hard drugs, and they make him take off-label stuff because his shrink is a witch-doctor who never has anything on-label. One of the things he is working on is anxiety and its precedent, which is likely depression, without having to go deep on depression horse pills. 

When Merlin did his last round in dealing with ADHD 10-12 years ago, the instruments were much blunter and he was on Lamictol to help with the personality and the mood. ADHD used to be: ”Let’s rev your motor up so high that it starts smoking!” Merlin loved Aderol so fucking much because it was really fun, but he got too much done which made him very intense and it could make him emotional in ways he didn’t prefer to be emotional. Afternoons were a bit rocky sometimes and pretty soon you are fucking Elvis and Dr. Nick is giving you something to bring you up and take you down at the same time while you are being laced into a corset. They are shooting vitamins into your anus and giving you all the different things!

Eventually Merlin got off that merry-go-round and tapered himself off of all the things. John says that those close to Merlin could definitely identify a wave of Merlin in a day and if you caught the wave at the right spot in the day, depending on when it started, you could ride it all the way up onto the beach. It is the same as if you catch drunk dad on the right drink number: He is going to be fun, but most of them are the wrong drink numbers, which is the problem. It is like the kid in Breaking Away, drafting on the truck and the guy is giving him fingers to show him how fast he is going.

John used to love catching a Merlin wave, but you could get on the wrong side of that wave, too! Merlin could be a lot! Aderol was great, but in order to get you to where you can focus and concentrate we have to bring your dopamine level way the fuck up and the side-effect is: ”Oh, did I mention? It is actual speed!” and speed is probably not so great for everybody. We have come a long way and now Merlin can take things that are much more sophisticated and subtle.

Last week Merlin started something fascinating: Since the 1980s he had heard of Beta-blockers, but it sounded like something from the pickup-seduction-community and he had no idea what that was. It blocks a Beta and you give it to somebody who keeps having heart attacks, but Merlin didn’t understand the mechanism for how it works. The titular Beta is a kind of adrenaline that keeps your heart rate and blood pressure going higher than people would like.

Social anxiety or situational anxiety is regrettably not the anxiety that Merlin has, but those three times a year when you have to give a talk at work your hands, arms and feet are shaking and people get stage fright. The situational part is that there is a certain kind of situation, whether that is social anxiety, performance, public appearance, or certain things that have a bespoke kind of anxiety, and if you take a Beta blocker half an hour before the hated thing it goes way better for a lot of people because it is governing a certain kind of adrenalin that comes out of a situation that causes you to have higher blood pressure and high pulse. 

Merlin has been taking this for three days and it has been fucking great! Less than 18 hours into this he opened the heart rate app and it was crazy! Basically it was mid-to-high 80s down to around 70 a day later, which is the heart rate of a middle-aged man.

If you go even further back to the Robert Lull or John Berryman days or all the days where we would overmedicate our great writers, you would have Tardive Dyskinesia and you could give people Prolixin. John points out that the names of all Beta-blockers end with ”lol” and Merlin’s is Propanolol.

Another example is putting somebody totally under for a procedure vs a local anesthetic vs where we are now with non-invasive surgeries with a little camera and a knife on a tube and you do hardly nothing. It is getting more specific and more subtle. People get profound effects out of Lamictol, which Merlin hasn’t been taking in years, and it is more subtle in its mechanism. Now it is on-label, but it was not designed for what it does. It was just a medicine for preventing seizures. John is so glad that science is helping once again. It is not great, but it is something. 

Merlin is not doing Screaming Mimis or yelling at spiders in the corners. John hasn’t heard a single Screaming Mimi yet! That heart rate thing must be reciprocating a positive feedback loop! Merlin showed a screenshot of his heart rate app to his shrink who found it really interesting and really desirable. Merlin still felt the feelings that he would prefer not to feel, and he always knew that anxiety was causing his heart rate to be up or heart rate might even cause him to have anxiety, but it was interesting that he would feel a feeling, but not that immediate ”Oh boy!” of ”I know there is going to be this thing and I am feeling physical symptoms of that!”, which in the long run will make this more manageable for him.

This week while she was miniature-golfing Merlin's daughter heard the song ”She Blinded Me With Science” by Thomas Dolby and she asked if this was Disco, but her mother said it was not. She just thinks everything old sounds like Disco. Merlin got a little bit tummy-upset because he was trying different things, and then ironically enough he slept like homemade shit last night. The New York Times says to not use these apps because it just upsets you, but the app doesn’t lie and Merlin is upset already, he doesn’t need an app to tell him that!

+ Merlin’s new glasses (RL345)

One time when Merlin visited John they went to John's mom’s house downstairs and John picked out a sublime pair of eyeglasses for Merlin from the late 1950s / early 1960s and was kind enough to gift them to him. They became Merlin’s canonical glasses. He tried them on and the angels sang and the heavens opened up, a sword in the stone type situation and what was John to do to stand between Merlin and his God? Unfortunately some years ago a beloved family member threw them away after a dinner.

Now Merlin found another paid that are slightly bigger, more Michael Keaton, with a tiny little bit of additional squareness. They are extremely handsome and they have an amber quality and a tortoise-shell look. Now Merlin can see the difference between 480p and 4K and he can see how dirty everything is. They are not Mack Weldon, but it is the Mack Weldon of glasses. They are indexed frames and he can look forward and see far things and it has little magnifying glass at the bottom so he can read things. It is great, that is what he also had in the ones he had before.

Eventually Merlin just lost his will to live for glasses and he was so frustrated with not having glasses he liked. This was Merlin’s follow-up (copyright John Siracusa). Thank you John for setting Merlin onto a path for glasses he could love and thank you to Mack Weldon. Merlin went to a store on a block on Heyes Street that has an Away luggage store, a Warby Parker, and an entire row of podcast ad merchants //(see RL344)//. They order them right there for you on their iPads. There was a store in Portland that had super-cool tennis shoes, some rad kicks, like an Apple Store for shoes and you would use a very special offer code to order them online.

+ John waiting for the phone call from the house owner (RL345)

Monday, Tuesday, Wednesday went by and John did not hear from the divorced man who didn’t have a condo but who had a big TV and was really attached to his kids and their shoes and John wanted to buy the house from. On Thursday John got a text saying: ”Hey buddy, I am going ahead with the restoration. Sorry, didn’t work out. There are a lot of great houses coming online right now, I am sure you will find a nice one!"

John wrote him back and said: ”Hey, why don’t you name a price that would make you not go ahead with the restoration and instead sell the house to me?” and he tipped his hand and said: ”The thing about your house is that it hasn’t been restored and that is important. You don’t want this house, but you want a house like this one that you have gutted and spent $100.000 to turn it into a modern condo. Why don’t you sell me the house? You name your price! I want to preserve this house because I am a preservationist!”

There aren’t a lot of nice houses coming on the market right now, but the guy is just talking out of his ass and also: ”Fuck you!”, or ”not fuck you!” because this guy didn’t owe John a phone call at all and it is his house that he bought fair and square with his capitalist money. John can’t say at any point that he is being a dick, but the guy can do whatever he wants.

He could build a lighthouse on top of it, even one [[[7-sided lighthouse made of dreams |made out of dreams]]], he could absolutely bulldoze it to the ground and he could even text John some pictures of him holding a hammer over the original pink sink and bathtub set right before he smashes them. Some people want to watch the world burn, but this guy probably does not, he just wants to make it through life from here to dither and have things the way he wants them, which is just the same as John. He wrote back and said: ”Sorry pal, the ship has sailed!” in those words. ”Thank you for even considering it! Vaya con dios! Go in peace!”

John had spent a year thinking about this house as though it was somewhat inevitable. Everything he did in terms of packing his own house, preparing his house to sell it, selling it, going through every one of these processes, all the traveling this spring, going to California multiple times, the motorcycle trip, Estonia, the JoCo Cruise, all the way back, this house had been there. Like a girl who loves you and you think you can always go back to her and one day she writes you and says: ”I’m engaged!” - ”What do you mean? You were my safety!” Now it is gone and John is sitting in his farm that the new owners will take possession of in two weeks time.

Some thing in the whole paperwork says that if you don’t occupy your new home within 60 days of receiving the final paperwork you are at risk of being charged with mortgage fraud because the structure of the loan is different if you are buying it as a rental property vs your primary residence. If you are buying it as a rental property you have to have a whole different setup of downpayments and interest rates.

The new owners understand the rules and they are hands-on, they are getting stuff done already, they have already built a fence in the backyard, they have already had an engineer come out and look at the barn and the pool, they are ready to hit the ground running, and John is sitting here, he has the house all messed up again, there is stuff everywhere, there is a briefcase over here, there is more than one cardboard box open, and he has been packing bags because he likes bags.

+ Trying different things to make a change (RL345)

John is still in a fantasy state where he lost his safety school, but he is still in his house, he can come here and be in his place and take a bath and do a crossword puzzle or just wander around with a [[[sword and bathrobe |bathrobe and a sword]]], all his neighbors are still here and know that John is still here, but he is a dead man walking and they are living in a state where a thing is already done.

When it finally goes away, when he goes to every room in the house, stands in the corner and says: ”Good bye, room! Good bye, other room!” and walks out the door the final time, then he will truly be adrift. The whole idea started about a year ago when John thought it would be a two-month process, but he started a 12-month process with the idea that it was supposed to better his life.

This was all an attempt to change his living circumstances so that he had less stuff, walked more, was more engaged in his surroundings and did not wallow in his farm for another 10 years in an existence as a bunkered-up loner who is waiting for something else to change. John has tried so much in the last 10 years to get things to change in one way or another. Back when he only had one podcast with Merlin and he was writing a column for the Seattle Weekly, it was all happening in the same room upstairs in his house. He was also composing his tweets on this same desktop, the original Anna Banana-Mac and he was stuck there, he was in a cycle.

John rented a space Downtown, he started the Roderick’s Rendezvous, a live show he did every week, he ran for city council, he had Millennial Girlfriend, he was [[[seafair |King Neptune]]], and all of these things were: ”Let me find something!” It is not finishing a Long Winters record, it is not tweeting six times a day, but some governing thing.

Selling his house was a big one of these: ”Let’s do it! Let’s do something! Let’s catch this wave at the right spot now!” He did it all, he sold it, and he didn’t do badly. At the time he was a little bit disappointed that the sale wasn’t more dramatic, but the market had gone ”bläh!” and John is okay, he is not freaking out, nothing has gone wrong necessarily, and not getting this house is not in the category of things that have gone wrong.

John tried to create conditions where thriving is possible. Looking back pretty far he realized that he was thriving, but he didn’t realize it then, or he was doing fine, but he didn’t feel like he was doing fine then. He was not making any forward motion because he didn’t have a plan and never had a plan and therefore it doesn’t feel like he is succeeding at his plan. People with a plan can see the things on their list that are checked off, that is why people check things off a list: They can look at the list and say: ”Yes, I did it! I got 7 of the 9 things!”

+ Not having a plan (RL345)

If you are looking at a map in terms of: ”I’m here and at this time tomorrow I don’t know where I will be!” and that is exciting and you are drawing a line...

Whenever John goes to a hotel and asks where he can find city hall the person behind the counter will whip out their local map that they keep behind the counter, pose their pen over it, do a big sloppy circle around where we are or put an X there. John is always wincing when they deface this map in front of him because then they slide it over to John with that big X over where we are and that big circle around city hall.

In that situation John always asks for a map that hasn’t been drawn on and he leaves their mutilated map with them and they can give that to the next person. That is how John interacts with maps. He is headed out of there with a 60% chance to go to city hall, but who knows? The philosophy is: ”Where the day takes me!”

Although John doesn’t want someone drawing on his maps, in most cases when he is on an extended trip he will carry a larger scale encompassing map that is not his navigation map, and at the end of the day he will sit and carefully draw a line of where he was, in particular overland travel, so that at the end he will have this snaky line and doesn’t have to go back 10 years later and say: ”Did I go to Freckenhorst? Did I go to Paderborn? I remember seeing signs for it, but did I go there or did I go around it?” John keeps a record, but that is very past-looking. He is not drawing a line to where he is going and follows it, but he is always leaving a trail instead.

When John is standing in Freckenhorst and is looking forward and he is making the choice that day whether or not he is going to go to Paderborn, a lot of those choices are whimsical, like looking down the road and if he doesn’t like the shoulder of that road as much as the one over here. That is how to travel in his estimation. If you have drawn the line in beforehand, you march off to a thing you don’t like.

+ We are living in bad times (RL345)

Part of a solution to all the bad feelings right now might be the idea that these are just bad times. These are not good times. We are not doing anything wrong, there is nothing John or Merlin can do about it, there is nothing to solve, but these are just bad times, just as the mid-1930s were bad times. In the course of our lives we have lived through some good times and John realized that 1990-2005 were really good times. They were grousing about it at the time because they were poor and young and dumb and everything seemed a bit dirty and low, but it turned out that everything was cheap, they could do whatever they wanted, they were basically free and they were concerned mostly with making culture.

At least the bubble John lived in, Downtown Seattle or Capitol Hill, was already a very inclusive community moving in the right direction. In 1992 they had already solved for ”Accept everyone!” and it seemed like ”Sure, they don’t accept everyone in a lot of places, but we certainly do and we are the future! We are living our dream already!” Also, if you want to start a fucking theater, you could just find an empty store, pay the landlord $350 a month, paint the walls black and now you got a theater. Those were good times and looking back John thinks: ”Ah, fuck! That was great!” Now when he drives around and thinks: ”I would like to start a theater!”, he can’t even get to the shop because traffic is so bad and when he does get there he realizes that rent is $6500 an hour. You can’t do anything now!

Young people are mad, everybody is frustrated, these are just shitty times! Seattle, San Francisco and all these cities are working on cities 20 years from now that are going to be cool for the people who live there then, but it just sucks for us now. We are all putting in the work to make 20-years-from-now people have access to things and who knows what even money will be. That realization was a great relief to John because he had a feeling that we were doing it wrong or something needed to click into place. It is going to click into place 20 years from now if we do all this work!

Merlin agrees. A bunch of people need to die off to make it be less difficult and a new kind of younger dingeling is going to come along. Think about where we were 4-5 years ago, then go back and watch that video of Lin-Manuel Miranda [https://www.youtube.com/watch?v=ZPrAKuOBWzw singing the title song] from Hamilton at the White House. Watching that now is like Shangri La: Our cool black president and his cool black wife were watching an American whose family came from Puerto Rico singing a song about Alexander Hamilton and they are laughing convivially at it because he says: ”I just won the Tony for In the Heights, but I am writing this musical about Alexander Hamilton” and everybody has a good laugh about it. It is like looking at baby-photos for the country and you think it was so much simpler then, but it wasn’t simple.

Everything that brought that black family and that Puerto-Rican man to the White House and brought the very problematic Alexander Hamilton into it, people worked fucking hard for every single bit of that. There was a sense of momentum at that time. The buzzword was post-racial America, but of course black people never thought it was post-racial anything. When you watch that you might think that we are not moving in the right direction right now, but part of Merlin believes that we can survive the next few years and a bunch of people are going to die off. There is going to be an opportunity for us to not sweat as much of the retrograde bullshit that we are dealing with right now.

Merlin’s daughter stubbed her toe really bad and didn’t want to go to camp today. Sometimes you have a bad week, a bad morning or a bad hour. He didn’t say this to her because he knows better than to give her advice at this point, but he was able to think to himself that sometimes you just have a bad something and if you have decided that it is going to be bad forever that is on you. There is nothing to be lost in hoping, figuring and planning in a way that it can get better, even if that just means getting an office with seagulls outside for a while. Try and move yourself in the right direction and maybe everybody else will line up with you!

John is 50 and Merlin is 52. John feels very capable and very engaged at 50. He went to the funeral of a friend and afterwards he was sitting Shiva with the mom and the stepdad because you can sit Shiva with someone when they are not Jewish and when you also are not Jewish, it is a place you can carry with you.

John said: ”Phew! Boy! These days! With my back!” and the man who was 87 years old said: ”Are you seriously going to sit here and complain about your back in this room?” They were very much like: ”We are very much not interested in hearing you complaining about age-related decay!” and John realized that this has become a thing he does with his friends. They said: ”You are young!” This is the thing John has been doing since he was 20, which is assume the mantel of a much older person.

John will never be 50 again and he feels extremely capable now, but these are bad times. His vitality exists within a time that is not as valuable a commodity as he thought it would be and he will be 60 or maybe 70 by the time these things happen, the mass-die-off has occurred, and it all shakes out. Knowing that his 20s and 30s happened during good times and his 40s and 50s happened during bad times allows him to just be in it and not feel like the rest of his life is going to be during bad times or that this is just times. How can he find a place for his vitality and where can he put that healthy energy? How can he live within bad times well?

John wants to contribute to something! We are all meant to contribute in different ways. There is no //one// offering, nobody can tweet John after listening to this episode and make suggestions about how he could contribute because they are inevitably suggestions about how //they// can contribute, they are having a hard time doing it and they like to displace that responsibility on someone else: ”You know what you should do? You should go to Guatemala and build homes!” - ”Really? That is maybe something that you should do!”

Merlin says the penultimate of Erik Erikson’s Stages of Development is ”Generativity vs Stagnation”. He reads from an infographic: ”The middle-aged discover a sense of the world, usually through family and work or they may feel a lack of purpose!” The last one is ”Integrity vs Despair”, but let's not get too far ahead of ourselves! Merlin feels he blew ”Intimacy vs Isolation”. According to that John is in the stage now where he wants to make sure that he is valuable and contributing something.

+ There is no geographical cure (RL345)

John’s dad always said from within his own universe: ”There is no such thing as a geographical cure! Don’t think you can just move over there and your problems will be over!” - ”Are you talking to me or are you talking to you?” He was trying to understand in part that he lived in Seattle longer than he wanted to because he didn’t want to be the guy who walked around town and knew everybody. He didn’t mind, he liked it, but he wanted to get called up to the mayor's, evidenced by the fact that the Rodericks do feel like they are somewhat destined to help people.

Like a Border Collie, he was not happy if he was not doing important work. We all are different sorts of dogs who want different kinds of things in ways we can’t possibly understand, which is the real destiny: ”I don’t want to keep eating doorknobs and chewing on shoes, but that is how they bread me!” Merlin’s cat would love to be able to breathe, but she can’t because that is how they made her and now she is a grotesquery and they love her very much. John is looking for his duck //(see RL43)// and he always has! He is that duck that is chasing a rat, but is scared of coat-hangers //(reference to the movie Mommie Dearest)//.

John doesn’t just want to imagine that a new house or a new office is all he needs. Doing his weekly live show was not meant to dislodge anything in him, but it was meant to lead to something. John doesn't believe anymore that he has a world of infinite possibility, he no longer honestly thinks that he is going to be an actor, no-one is going to discover him or cast him in a thing, no listener is going to become the Di Laurenti (?) of 2030 and think: ”My Wilford Brimley is John Roderick!” or whatever. He has always lived according to his dad’s principle, which was: ”At any given moment you may get a registered letter in the mail that says: Dear sir, it turns out that you are an United States Senator!”

+ Going to lunch with Dow Constantine (RL345)

About a year and a half ago, the King County Executive Dow Constantine asked John out to lunch. He is vegan and they went to a vegan restaurant where the food is amazing until 10 minutes after it came out of the kitchen, but when it cools down at all it coagulates back into some pink slime of vegetable matter. The entire time it was clear that Dow had asked John to lunch because he had an idea that maybe John was useful to him. 

In the one chance John gave the voters of Seattle to elect him to office they chose not to, but Dow was in a position to give people jobs and he was thinking: ”Is there a job for John as some kind of minister of the arts or will he be useful to me as some kind of spokesperson?” You could just see the gears turning! Over the course of the lunch John talked about Building Seven, how jet fuel can’t melt steel beams, and that the antidote to chem-trails is to spray a mixture of vinegar and baking soda into the sky or whatever the fuck John was talking about. You could just see Dow go: ”Hmm, I don’t think that right this minute there is a job. Maybe when I am governor there is going to be a job to find!”

Just a few weeks ago he appointed John’s friend Kate to be his director of arts for the county //(see [https://www.instagram.com/p/B2kXWq1g3BL/ his picture])//, which is a much bigger era and a larger operation even than the city. There is an executive and a council and it is much more land-use-y, but Dow is setting himself up to run for governor and he will be a good politician. There is [https://www.kiro7.com/news/local/dow-constantine-accused-of-using-elite-detectives-as-personal-chauffeurs/953758521 a little bit of scandal] associated with him right now and some kind of backroom dealing with some kind of guy who is a bad operator.

Dow is always shadowed by a plain-clothes King County Sheriff Deputy who is also his driver. It was a lady deputy who had at least one pistol, but she might also have had an ankle pistol. She just sticks around, she drives, but she also parks it and is within shouting distance at all times. It is really fascinating to have a bodyguard.

After the lunch they went for a walk, strolling around the neighborhood, having a good-old time, and they didn’t go a loop like you would normally go if you were ultimately going to walk back to the car, but they just walked in a direction and when they were done walking and it was time to go, he pushed a button and the car came and found them, which was pretty nice. The car had been in motion somewhere, shadowing them. They picked John up and took him to something and they didn't have to worry about traffic, but they were just sitting in the back chatting. Dow gets a lot done driving around town while his lady-sheriff is dealing with the stuff. She is not a road-rage, she is just cool in it.

It was clear that John was not going to get a job that day and he was fine that day. He wasn’t going to put on his best face just in case there is a job in county government for him, but he didn’t bring anything to this party except for who he is. You got to dance with the one who brought you, which in this case is him. The job has to be something for John, John can’t be something for a job.

More and more John realizes that he just has to do the work that he has to do. He already knows what it is and he always has. There is a reason he is not doing it to his satisfaction, but what has to change? The amount of work he does or the degree to which he is satisfied or what? At 50 years old he still doesn’t know, so maybe what he needs is a new kitchen, or a new old kitchen, but he doesn’t think it is the kitchen.

+ John is not sure what to do next (RL345)

You put the lime in the coconut, but the important thing is not the lime, but the coconut. You put the lime //in// the coconut. If you don’t have a coconut, what are you going to do? ”You can’t land on a fraction!” //(reference to Apocalypse Now)//. ”Scuttling across the…” (floors of silent seas, reference to the poem The Love Song by J. Alfred Prufrock). John doesn’t want to give it to him.

Last week John was not interested at all in Merlin’s idea to go to him and cry, not even while holding a tire iron. Maybe John should stand in front of his house like John Cusack with a boom box and a trench coat? In fact, John found a really nice super-expensive cashmere trench coat. You would be playing ”In Your Eyes! The light the heat! In Your Eyes!” //(song In Your Eyes by Peter Gabriel)// or one of John’s songs and go: ”Hah, you like it?”, but that would be too risky. Based on the Internet reaction, every week somebody says: ”I love him on that podcast, but I never listen to his music. I got a record and it is fine!”

+ John’s daughter’s mother getting invited to a party with Indie Rock people (RL345)

John’s daughter’s mother was invited to a swimming pool party by some parents who are a little younger than they are and the dad is kind of a dick like the guy John is dealing with in the house, but the mom is a fun Jewish girls and John’s daughter’s mother thinks she has a fun lady-friend deficit. The mom was telling John the story that there were Indie Rock people who went to Timber festival.

John asked if they were Long Winters fans and her face fell because she had stepped into it. She was really bummed that she hadn’t heard of The Long Winters because she liked all the bands around The Long Winters and now she is really exited to get into The Long Winters and John was like: ”Just stop talking!” Did she mention John was on the OC or that he did a They Might Be Giants cover? No! Speaking of which: ”You are older than you have ever been!” //(song Older by They Might Be Giants)//

Fuck that guy! Fuck them both! Fuck them in the ear!





















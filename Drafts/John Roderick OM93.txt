OM93 - Second Sleep
2018-10-16

+ Being a late chronotype (OM93)

We take it for granted that at a certain time of day pretty much all of us will turn around like a troop of zombies, lie down on our respective horizontal surfaces and civilization will disappear, that the Normals all go home at the same time and go to sleep at the same time. John never had that [https://en.wikipedia.org/wiki/The_Stepford_Wives Stepford Suburban Life] with a well-adjusted family life, but he actually does have a white picket fence, which is cosplay to conceal all the bodies in his basement. When children in the neighborhood go missing, nobody is ever going to check at John’s house because he got a white picket fence, but late at night he will put his leather pants on and dance the Shaman’s dance.

As a night person it is frustrating to John that all of civilization and human endeavor goes away and shuts down completely and he can’t get a roast beef sandwich past 9pm, in Seattle barely past 7pm. Ken shares John’s chronotype and in the parlance of whoever wrote the book on this stuff they are owls, not larks. Larks are awful while owls are wise. Larks sing and John probably considers himself a wise singer, but he doesn’t sing like a lark, but like an owl. He is not a Disney princess who wakes up and stretches while the little forest squirrels and birds put his cloak over his shoulders. The only living creature on Earth that doesn’t need a period of rest is senator Ted Cruz, who is out late at night killing Zodiacs, or maybe he is just eating soup in front of infomercials or he eats live bats.

Bluegreen algae have their photosynthesis tuned to a clock, but they don’t rest. They are either going or growing, but never slowing. It is possible that their listeners are fungi or robots who do not need the kind of reset that higher mammals and other creatures get from sleep, but even robotic life is going to need to power-down sometimes to download a new version from Apple. Even if they are nuclear-powered they are still going to need a period where they discard their old fuel and put new fuel in. There is always a cycle of some kind. Ken’s theory is that people in the future will have evolved beyond sleep all-together but John cannot imagine that. He doesn’t see sleep as an impediment to evolution or as a deficit, but as a major bonus. Ken thinks that once it is no longer necessary we will no longer find it pleasurable. Does their audience also no longer defecate?Defecating is inefficient and they can fully recycle all their waste materials or exude particulate out of their carapaces? All the things we love they don’t do anymore.

+ Being in the moment, reducing synaptic firing (OM93)

John would like 25% less synaptic firing, but in that case he would need a way to target the muting in order to only mute certain synaptic firing but not other synaptic firing. Maybe he should try to be a dummy and watch reality TV? //(This refers John reading the [https://www.amazon.com/Presence-Process-Healing-Journey-Awareness/dp/0825305373 Presence Process], see RL307)// People are always telling him to live in the moment, but living in the moment feels just like a conscious effort to have 20% less synaptic firing. People are suggesting him to be calm and to focus on his breathing, which can be achieved with the help of cannabis, but John and Ken can’t do that and have to sit in a non-cannabis-tempered hellscape, a weed-free dystopia while everyone else seems blissed out and mellow. They are just like ”But what about the dolphins? The dolphins are dying!” 

+ John having sleep issues (OM93)

Lately John has not been sleeping super-well and has been very grouchy, which is weird because he has always been an epic sleeper who easily could get 10 hours of sleep. Having been a Bipolar-person he had a cyclical emotional life who would sleep 14 hours when he was on the downside. When your waking life is a bummer and you are suffering from depression, checking out into sleep is a gift! John never had nightmares and he was not somebody who was restless in bed, but he would wake up and ”Oh no! Reality is still here!” During the last 5-6 years, John’s life has been characterized by a lot more anxiety which he had never felt before. 

Nowadays John lays awake in bed at night and wakes up in the morning after only 4 hours of sleep, which is taking a toll on him. He got melatonin, he got his phone set to shut off, he put blackout-curtains in his room, there is no TV in there, he is doing all the things, trying to have his room be a sanctuary. Unfortunately he is not only using his bed for sleeping and sex, but he does do other things in bed. Both John and Ken don’t want to take Ambien. John doesn’t want any prescription drugs in his life and he believes it is something he can solve, but he is struggling.

These days John has things to do in the morning, while 15 years ago he could sleep 14 hours because nobody cared and he didn’t have anywhere to be. Only a Rock musician could ramp to the high-street sell-out life of the Podcaster, the old nine to five! If he wouldn’t get to the venue by 6pm in his previous life, the band would think that he was probably still in bed, but now he has to get up at 9:30am to do these podcasts because Ken might be at the door with bagels. John is not alone in his insomnia, though. 50-70 million Americans report trouble sleeping and the average is only 6:31 hours, which is lower than almost anywhere else in the developed world. 

+ Ken’s sleep requirements (OM93)

A tender quivering snowflake like Ken needs 8 hours of sleep and if he only gets 7:20 hours, he is pissed for the rest of the day and he can tell that he is off his game. His only anxious relationship with sleep was as a child, because he was living halfway across the world and would fly back to Seattle every summer. Flying East is murder and he would be the only one to lie awake all night in a big house as a slightly neurotic 9-year old. He should just have gotten up and read a book, but instead he laid there with his thoughts feeling bad because he wasn’t sleeping every second. 

The advantage of being an owl is the awareness that no-one else is awake and it is just you and the universe, this great lonely ”I’m on my own mountain-top” feeling. Ken never feared the Boogie-man, he was never scared of ghosts and he never got freaked out about UFOs. He would go under his covers and would pretend he a Hobbit escaping from the Wraiths and it was cosy and pleasurable. 

+ John’s pillows turning to owls, UFO-symbolism (OM93)

John has a longstanding relationship with UFOs and they do make periodic appearances in his room at night. One time he woke up in the middle of the night, he was very awake and very conscious, and all of his pillows had turned to live snowy pillow-shaped owls //(see RL26 and see [http://board.thelongwinters.com/viewtopic.php?f=1&t=3066&p=35853 illustration])//. There were owls all around, there was a pillow on the floor that had turned into an owl and they were all looking at him. John realized there was nothing to be done and he just grabbed the nearest owl, snuggled up inside its little owl breast and went back to sleep. 

A lot of John’s fans have done fan-art of him in his room full of owl pillows and a couple of those images are actually eerily accurate, showing John in his bed with his covers on. He was a little apprehensive in the moment, but he also felt a strange calm, which is conveyed in the fan-art. The owls were watching over him as much as they were watching him. They never came back. John has woken up in the middle of the night searching for them, but it has only happened once. John does not often have those lucid states where he is lucidly awake but also still sleeping. He never imagined that a daemon was crouched on his chest and he doesn’t want it! That is the type of thing he would be very anxious about because he doesn’t like claustrophobia.

Recounting this to people later on, a lot of them said that owls are UFO-symbology. If you dream about owls or see owls at night, it is always a UFO-encounter. Ken is frustrated that he has to say ”Roderick” with 3 syllables instead of ”Rodrick”, but John is allowed to say ”ooh-foe” instead of ”U - F - O” Ken suggests that UFO might stand for Unidentified Feathered Owl, but it is like SNAFU: You don’t normally think about what it originally meant. You don’t say Everything is going FUBAR. In the past John could feel a cold hand in the middle of the night, wake up with a startle, and jump out of bed into combat posture ready to defend, but these days he just turns the light on really quickly, sits there and hyperventilates for a second.

In the past John would lie in bed reading a book about UFO-encounters, but he does absolutely not do that anymore, he won’t even watch a movie that has UFOs or is scary with somebody creeping into the house and phone calls coming from within the house, because it affects him late at night. Living alone is another factor. Ken lives with a family that dislikes him which amounts to much the same thing. John lives out there in the sticks with a big property and a barn that is full of creatures. That is why he keeps a hat-rack full of swords by the front door, just in case. A sword is ineffective in dealing with an intruder, but it is great at dealing with supernatural beings. You can’t shoot them, but you have to fight them with a sword. John’s swords are masonic swords with Illuminati-iconography on them, charged with the all-seeing panopticon.

+ Distracting your mind when going to sleep (OM93)

John should go to bed reading a book about something comforting, like compound interest or Thorstein Veblen. Ken’s wife will turn on the most boring podcast she can find, which is The Dollop, No! Not any specific show, but some podcast about financial planning or about somebody’s semester abroad in Finland and she will nod right off. Financial planning would cause John anxiety as well. Ken has a hard time sleeping when his mind is churning and working overtime. He cannot sleep on a plane and he doesn’t nap that well, but if it is his time, if he has read for an hour and he is in bed, he floats away to dreamland. John can sleep sitting up like on airplanes or on a chair in a bus station. 

+ People sleeping sitting up (OM93)

One time Ken visited Shakespeare’s house where the rafters were very low and the beds were very small. Ken told his wife that people were very small back then due to nutrition, but as the docent heard it she got really angry because she was tired of hearing this. She claimed that people were the same height in medieval times, but that is not true because they were on low-protein diets, there were famines all the damn time and people were 5’3” (160 cm) or 5’6” (168 cm) tall. She said that the reason why the beds were small was that people slept sitting up. There is very little evidence for the idea that people slept sitting up, but there are a tons of beds that are smaller than they should be.

Ken’s mom worked at the Seattle Science Center when the King Tut exhibit came to the United States in the 1970s and he got free tickets while the line for visitors was 2 miles (3 km) long. John was 7 years old at the time and this was when he first saw those Egyptian pillows that were just head pedestals. He tried to imagine what it would be like sleeping with his head on an Egyptian pedestal pillow. There was nothing soft about them, but they were elevated head cradles made from carved wood. It would be a good pillow if you were trying to keep pinching insects out of your ears, but not a thing you could cuddle up with. You don’t see people coming off planes with a wooden horseshoe around their neck like they were an oxen. 

+ Different stories about sleeping habits (OM93)

For many years we assumed that everybody slept at night, like Ken and John do. For many years John also assumed that everyone had his musical taste. He would just go up to random people in line at the bank and start conversations about AC/DC, like ”Can you imagine Bod Rock just sitting there, recording each string of a Def Leppard guitar part?” 

American values between 1930 and now have changed pretty radically 50 times and we assume that things change very quickly very often, even during our lifetime. When Ken was a kid, Ted Danson was doing blackface routines and now some of our country clubs have our Jewish friends in them, which is crazy! Sleep is this one unchangeable thing that doesn’t seem susceptible to cultural influence or habit. 

There are no cultures that fart differently either. John knows a lot of people who have never farted, at least not around him, like George Eastman, inventor of Kodak who is very chaste, but president Warren G. Harding did fart quite a bit when they were camping. The farts of French actress and animal rights activist Brigitte Bardot smell like roses because she took the potion invented by Benjamin Franklin who had spent a considerable amount of his intellectual capital trying to develop a tincture that would cause your farts to smell like roses. 

Benjamin Franklin is on the record as being a bi-phasic sleeper. He would sleep for 3-4 hours, get up in the middle of the night, open all the windows, take off all his clothes and sit in a chair and read, which he called a cold-air bath. Ken wonders if that was the only kind of bath he was taking. John feels like that is evidence that Ben Franklin was a kook and a crank and less evidence that it was a commonplace thing to do. John is going to try that cold-air bath by sitting in a chair naked, pinching his nipples in the middle of the night and reading his UFO paperbacks.

People get up in the night and pee. Ken is at an age where he has to think about at what time he will have his last beverage in the evening, not just coffee, but any liquid. If he drinks anything at 9pm, he probably has to pee at 3am. Sometimes he cannot get back to sleep when he wakes up in the middle of the night, especially when there is something else going on, like jet lag or if he knows something is going to happen in the morning. He is a terrible first-night-in-a-hotel-room sleeper. 

+ John reading a book about sleep (OM93)

John hates to refer to sleep researchers, but he has read a book called [https://www.amazon.com/Why-We-Sleep-Unlocking-Dreams/dp/1501144316 Why We Sleep] by Dr. Matthew Walker describing the phases of sleep. In the first part of the night you are in deep catatonic sleep which is different from REM-sleep. The body goes down into a neural-processing mode and building blocks of your memory are being shifted around, but your body is not paralyzed because you are not actually dreaming. 

REM sleep is the last sleep you have during the course of the night, which suggests that this wake-up-halfway-through-the-night-and-pondering-your-dreams thing is some kind of medieval hornwash, not only hogwash or hornswoggle, but both! In the literature the first part of sleep is called for death sleep. Some would call it beauty sleep and it was some folk-belief that this was when you get your rejuvenation and your cheeks would pink. Another medical belief is that sex in the middle of the night is the one that is most likely to lead to conception, which John can confirm.

When John first heard about the theory of second sleep, it really resonated with him. There was something all along that we have lost through civilization and gaslighting. John is super-susceptible to that stuff: All the broken things would be fixed if only we could go back to poly-phasic sleep that we have lost ever since the Industrial Revolution. He is like Brian Wilson: He wasn’t made for these times.

John is sceptic about second sleep. If this was a natural rhythm he would be a perfect candidate to have experienced it first hand because for years he had no obligations and lived in a Northern climate. Sometimes he wakes up in the middle of the night and gets a glass of water, but that is not a time when he feels especially sexy or procreative, he never goes up and visits his neighbors. Despite the vast corpus of contemporaneous text attesting to this, it can not be true, because John doesn’t do it.

John is not against the idea that second sleep was a cultural phenomenon of a time and a place, but if there were any people who were prepared to do it, it would be John’s cadre of artists and drunks. Their writer-friend George Meyer, a Ben Franklin-like character, does second sleep routinely. John could absolutely set about to second sleep as either a project or as a thing to do, but polyphasic sleep requires to go to bed early which does not lend itself to John’s lifestyle.

+ John walking around his perimeter in a bathrobe with a sword (OM93)

In reading about sleep, John was trying to figure out what his habit was and he does not typically go to sleep until 4am. He has a sense of being the watch, a group of mammals who are part of a tribe that needs a certain small percentage of individuals who are up all night when the majority of the pack is vulnerable to predators. He does feel like he is a little Meerkat standing outside the Borrow Pit, he is on watch, he does go out, he walks around the neighborhood and he patrols. For years he would go out and walk around with a sword, which Ken doesn’t believe //(see RL7)//.

There was no-one else outside and John didn’t do it to be caught or seen, which is a good move, because you don’t want to be seen in a bathrobe carrying a sword blocks from your house, at least if you want to continue to live in your house. John was fencing supernatural beings and if a car would come, he would step into the shadows. John is a weirdo and not an example for others. He felt very innately that he was on watch and he still feels that at night. A lot of the pleasure of going up at night is that you are the special one who is here and you are experiencing a part of the world that the normals do not and that would be gone in a culture where the watch was the norm. 

+ Unpacking in hotel rooms (OM93)

Ken finds it the weirdest thing that people unpack in hotel rooms and put their folded clothes out of their suitcase and into the drawers. John’s mom unpacks her suitcase and puts everything away in the drawers when she stays in a hotel. When Ken stays in an AirBnB for a week, his rule is: If the place has a kitchen, he will unpack his clothes. Sometimes he will put dirty laundry in a drawer and leave it there for the next guest because he is rich enough that he doesn’t have to clean his clothes anymore. He picks out his outfit for the trip and then leaves it in Baltimore or whereever. No, he just doesn’t want it stinking up the new clothes.

+ Ken’s dirty puns (OM93)

Nine out of ten things Ken says are dirty puns. He is just a walking episode of some British comedy duo or like a book you would find in someone’s bathroom called ”Ken’s dirty puns” Ken thinks that John just hears dirty puns where there are none.

John finds the Grouse to be the sexiest night bird, because what is sexier that a little chub? Thicc with two ”c”.

+ Being a late sleeper (OM93)

People who sleep less are supposed to be getting a bigger bite of the apple than the rest of us and people who wake up early are more virtuous than people who sleep late. "Early to bed, early to rise!" John has always seen this as discriminatory. He is not staying up late because he is lazy. He doesn’t get up in the morning and hustles off to work, but that doesn’t make him a scofflaw. Besides the sense of solitude and disconnection, a lot of the enjoyment Ken gets from being up late is similar to being on a plane: You have ideas because you are unmoored. Then there is the idea that you are procrastinating bedtime. Maybe Ken is a bad lazy person because he is putting something off he could do now or he is an amazing two-marshmallow-kid who can defer pleasure for achievement. Maybe it is more virtuous to put off sleep?

John avoids going to sleep until he is absolutely forced to. He abhors going to sleep, but once he is asleep he abhors waking up. Ken suggests this being inertia because John likes the thing he is currently doing. John resists sleep, but he hasn’t investigated if he fears sleep although he certainly avoids it. He will stand on the precipice of sleep and slap his face and throw cold water on himself to avoid tumbling over that edge, but once he is there, he does not want to rejoin the world, but he will claw onto sleep as he feels wakefulness coming. 

+ Sacrificing sleep periods, the Grateful Dead vs R.E.M. (OM93)

Buckminster Fuller had the idea that sleep could be even more polyphasic and he called it Dymaxion sleep, which just meant ”cool and special to me”. He would sleep 2 hours total over a 24 hour period in 4 chunks of 30 minutes. Recently this has seen a revival when a woman named Maria Staver called it [https://motherboard.vice.com/en_us/article/vv7e8m/the-uberwomen-who-beat-sleep Uberman sleep], which shows who is into this: Weirdo Ayn Rand readers who are better than the normals. They are can-do guys starting up companies, they only need 2 hours of sleep, they are eating Soylent and they are peeing into a milk-jug because they don’t want to stop coding. Missing out on dead-time and going straight to REM-sleep is good in their mind, but sleep research says that you cannot go straight to REM-sleep, because dead sleep is more important and if your body has to sacrifice one, it will sacrifice REM and get dead sleep first. 

Is John saying he would chose the Dead over R.E.M.? John is not a Deadhead and the second half of the R.E.M. catalog is very problematic. Ken disagrees and listens to R.E.M. all the way to where he can hear the vocals clear and proud, but they are not that great and Michael Stipe should just have continued mumbling. The Dead do not speak to Ken at all, not even Terrapin Station. He is probably the wrong generation, it is easy to make fun of hippies, or maybe it is just prejudice. The name Grateful Dead suggests that it is Metal, but it is meandering Folk with a bass-line that never plays the same note twice. Ken finds jam bands in general to be trouble and wants to get to the point. If he hears 26 minutes of a podcast and they don’t talk about second sleep, he is like ”Okay, Jerry! I get it!”

+ The visionary quality of the night (OM93)

The time of night when everybody’s pillows turn into alien owls has a weird visionary quality and lots of supernatural or otherworldly things happen. This is also the time period when people have visionary experiences, like saints who see Gods, monsters and miracles. People seem more in touch with whatever part of their brain produces that. Could sleep habits have something to do with it? If Ken and John had an hour in the middle of the night where they would be alone in the dark on an island of wakefulness between sleep, thinking about their dreams, would they have contact with God, whether that is a saintly miracle or snowy owls?

John has sought God all his life and he often feels like modernity intrudes on what would have seemed godly. He can ponder the universe, but then he hears the double Jake brakes from the 18-wheelers down on the highway going off and God evades him once again. It is akin to our modern disconnect from the stars: In medieval times you could step outside into the cold air completely naked, pinch your nipples, look up and see the Milky Way. Living in a city today you will just see a dome of light. You might see six stars, but five of them are airplanes, meaning that we are considerably disconnected from the experience of a human at any point before during our 50.000 year history until 80 years ago. Ken’s theory is that atheists and skeptics of all kinds are just not sleeping right.

















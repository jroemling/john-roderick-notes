RL306 - Corgis I’ve Already Seen
2018-09-24

This week, Merlin and John talk about:
* Musicians who are into horse racing ([[[Music]]])
* Visiting a strip club ([[[Stories]]])
* Leaving your kid somewhere and taking your kid to the stores ([[[Early Days]]])
* Old Downtown buildings in the 1970s ([[[Early Days]]])
* Merlin trying to fix his ice machine ([[[Merlin Mann]]])
* John's and Merlin's relationship ([[[Attitude and Opinion]]])
* John thinking about getting an Apple Watch ([[[Technology]]])
* Deepfake videos ([[[Internet and Social Media]]])
* The current state of social media ([[[Internet and Social Media]]])
* Screen Time in iOS 12 ([[[Technology]]])
* Why a Nickel is called a Nickel ([[[Factoids]]])
* Going cold turkey instead of ramping-off habits ([[[Personality]]])

**The problem:** John’s so tired of carrying the internet around with him, referring to John thinking about getting an Apple Watch so he can still call and text, but doesn’t need to carry the whole Internet with him all the time. 

The show title refers to algorithmic timelines on social media where you never know if you are done and caught up. Before, if you already had seen those Corgis, you knew you had seen all the Corgis.

Here is what was on Merlin’s topic list for today: 
* How is the new schedule?
* Do I have a head cold or analogy (?)
* I tried to fix my ice machine.
* Apple Watch!
John had no idea Merlin wrote things down before the show. 

Merlin still feels bad for the train driver who drives up the bridge and then looks stunned, but he should be stunned! Merlin kind of fucked with that guy pretty bad, what if he is in a union? The laws of gravity are suspended in Zeusiana. He is in the fucking drink now. What if he is leasing that train? He only looks perplexed, though, there is never horror in Dr. Zeus. They fall, they end up ass over teakettle, there is generally a cake balanced on their feet, and they make a lot of poor decisions.

+ Musicians who are into horse racing (RL306)

John calls Merlin for Bez, the dancing boy, because Merlin said ”Happy Monday!”, but Merlin doesn’t even like that band. He doesn't even like the dancing guy in Hazel, but he likes the dancing guy in Pavement which could be Bob (Nastanovich). He hit something every once in a while, whether it be the drummer... He was somewhere between the second drummer in Adam and the Ants //(Chris Hughes?)// and Davy Jones and he loved horse racing. John has a (Daily) Racing Form T-shirt that he found in a thrift store many years ago and thought: ”Ha, like Bob!” Merlin doesn’t know of any other people who are in Rock bands and are enthusiasts of horse racing. Merlin has not been to the horse track, but he has been to the dog track and bet on the doggies. They had one right near their school in Sarasota (Florida) and he went there, but it is not really his jam. John and Merlin don’t like strip clubs or dog tracks.

+ Visiting a strip clubs (RL306)

Merlin doesn’t like strip clubs on any level. He likes is that people are employed and it is nice to see the economy thrive, it is good for tourists, but it is weird to see ladies dancing on your friends and they come back in the little booth. Merlin didn’t even know what to stare at! He and John were so lonely. What happens in Georgetown stays in Georgetown. They were standing out in front of the strip club and there was a desk with a guy who wanted $20, but they both didn’t want to pay $20 for this, like $20 for what? But their friends were already in, they had paid the money and went through the dark door into the place. They seemed very familiar with the transaction, like this was a thing they knew how to do. Money on the table, in the door, money out, money everywhere, while Merlin and John wondered if they could just go to the movies, but Merlin insisted they go with their friends because you don’t want to go to a second location and you don’t want to break it up. 

+ Leaving your kid somewhere and taking your kid to the stores (RL306)

At a big IKEA they have a cool area with a ball put for kids to play in, but you are not supposed to leave your kid there, although everybody does. Merlin wishes that where would be something like an adult ball pit at strip clubs for the nominally heterosexual men who don’t want to see ladies dance on their friends. It could be like an emergency room waiting room where they got cooking shows on TV. We are not living in the 1970s anymore and you are not supposed to leave your kid at the ball pit at IKEA, but when John and Merlin were kids, getting left was a normal thing. 

John even got left in bars! His dad would roll up to the bartender and ask them to watch his kid for 45 minutes and he would be gone for 3 hours. John would just go and play with the cigarette machine for an hour. With every pack of Kools you were entitled to a pack of free matches if you pushed the button, but most people wouldn’t do it. John would just haunt the cigarette machine, as Merlin did, and every time somebody would buy a pack of cigarettes and didn’t get their matches John would get them. His pockets were full of matches for all his pyromania later. 

Merlin likes to collect all kinds of free bric-à-brac in general. He would always take the free things whether it was mints, tooth picks, matchboxes, business cards or brochures. He would raid the brochures at motels and trade shows every time he went to a convention center trade show with his dad and he would get everything. They would even give you bag to put it in! You discover that you can do stuff with matches, although it is not the form of expression, as acceptable as it was in the 1970s to be a fire bug. 

There were also swizzle sticks //(they talked about that in RL150)//, but you don’t see them as much any more, especially not an exciting swizzle stick that does something or that has a joke on it. Branded swizzle sticks with a Playboy logo on it were a big deal. They were big things, not those little flimsy ones you get at Starbucks, but they were solid and you could whack people with them. The coffee stirs at McDonalds were amazing! They weren’t just for cocaine, but you could strap those golden arches between your thumb and forefinger with a rubber band and shoot somebody's eye out. There was a different level of tolerance back then.

Merlin and his wife will sometimes trade stories and be fretful about their child, because she is currently right in that era and they are very fretful about where she goes, knowing where she is, or strapping a GPS device to her neck. They have dueling stories about what their totally competent and normal parents would do in the 1970s. Merlin's wife has a story of being left in a shopping cart in the front of a grocery store for the whole time her mother shopped and loaded up the car. Merlin was always encouraged to wander. Go look at the darts, don’t play with the darts, but wander through the bar and look at the rest rooms and the cigarette machines. 

One time John and his mom were at the big 7-story tall department store Downtown where you could buy lawnmowers and beds and gliders and stuff. John was wandering underneath all the clothing racks and got separated from his mom. He got over by the escalator and found the on/off button for the escalator and of course he would turn the escalator off in the middle of crazy shopping Christmas time. All of a sudden: bedlam, because the escalator stopped when it was full of people with their bags.

John secreted himself somewhere until finally the manager came over and turned the escalator back on. John waited an appropriate amount of time, scuttled back over there and turned it back off again. He played this game with the management of the department store until they were on the overhead speakers calling out around the world, Security to Isle 4, and they finally nabbed him. He didn’t sing, but they ended up calling on the overhead ”We have a little boy in Security” and his mom looked around and ”Wait a minute, I have a little boy!”. 

John didn’t look like a scallop back then. He was a beautiful little kid and the disappointing thing about puberty was that he became so grotesque after having been such a little angel. Merlin had white hair as a child, he was gorgeous! There are a lot of hideous kids, let’s just be honest. Some of them grow up to be perfectly normal looking grown-ups. John feels like he peaked at around 8 years old, but it was no good at 14.

Merlin struggles with his daughter when it comes to doing boring things with the family that they had to do when they were kids. Merlin never had the option of ”Do you feel like going to the grocery store with me?”, but you always go to the grocery store, the hardware store, the butcher shop or the lady’s clothing store. That was where Merlin was a Viking, because he was bored and there was nothing to do, like going to a craft store. It was excruciating for a kid because there is nothing fun and nothing branded, but it is all just shawls and needles and shit. 

Merlin would get into the racks where the ladies’ clothes were and jump out and scare people. Both Merlin and John lived the single parent lifestyle and John never had any option about going to the store or to the pharmacist. He spent hours and hours of his life sitting and listening to his dad talk about Jazz music with a city council person. If he could steal a brochure from somewhere, he could sit and read it quietly in the lobby. Everything had to be performed quietly.

+ Old Downtown buildings in the 1970s (RL306)

John’s dad was in government, everything he did was government and all of his friends were government. He would take John to all those buildings and a huge part of John’s sensory: eyes, ears, mouth and throat were formed during that era. A lot of the buildings were built in 1910 and had been refurbished in 1938. By 1968 they had fallen as far as they could fall and in 1975, seven years later, they would bear the best and worst of each era in which something had been changed, a weird detritus of all different ages, always with a nice little touch of brutalism. 

They all had marble floors and 6,5 feet (200cm) tall marble urinals that weigh more than a Prius, because they are as big as a Prius. How would you even mine one of those? Think about the marble sculptors who would sculpt those giant urinals! Every door in those buildings had the person’s name hand-painted on the heavy door with frosted glass, it didn’t just say purser’s office, and there would be a receptionist. 

John spent a lot of time in those buildings and he can still smell everything about them. Most people who worked Downtown had leather-soled shoes and you could hear people walk in the echoey hallways. The walking of the grown-ups had a snappy sound that always sounded you were in trouble because somebody was coming down the hall and there was going to be hell to pay. There were all the file folders, all the file cabinets and the smell of paper that had been in file folders! Mildew, old paper, coffee, cigarettes. 

Merlin was at an ATM yesterday and he did not even have to look in the little mirror to know the man behind him at a polite distance was a smoker, not a vaper. Both of Merlin’s parents smoked, even in the car with the windows up. John’s people didn’t smoke because they were older and had both smoked in the 1950s, but in the 1970s people smoked everywhere. John’s dad would pick up hitchhikers and they would get in the car and start smoking without even asking first! Dad never said anything, because it was like "Smoke em when you got em!"

John cannot get back to those places because they don’t exist anymore except in rare occasions where not only the building is still here, but where they also somehow kept the smell of it. Every stairwell had a fallout-shelter sign down in the basement so you would know which floor to go to with 50 gallon drums full of mealy meal. Noise pollution was a thing in cities before they quieted down the car horns. At some time in the early 1980s it was what everybody was talking about, it was the killer bees of the moment until Detroit decided that they were going to cut the dB or car horns.

John and his dad would stay at the Washington Athletic Club when they would visit Seattle while they lived in Alaska. They would spend three days with the people at the 14th floor there and John would be hanging out in the window, throwing burning paper and ice down at people. The sounds in the city were horns. Cars were honking, it felt so urban! It is crazy to think that Seattle felt more like a classic Downtown than it does now. It is like a comic book and the cars are all made of plastic.

+ Merlin trying to fix his ice machine (RL306)

Merlin is trying really hard to fix his ice machine, but it was low on his list because it is not very interesting. He loves ice and he is struggling. It might be some missing gear teeth. They have a small capacity refrigerator and the amount of ice Merlin requires would take up a surpassing amount of room that could be filled with frozen chickens or whatnot. 

+ John's and Merlin's relationship (RL306)

Merlin and John don’t text each other that often unless they send each other something about Steely Dan or videos and things, but they both have friends who out of the blue will text about their kids doing things. Those people are just keeping in touch, checking in with each other, just touching base. Merlin and John text each other when there is something to text about. They have a visit every Monday morning Pacific Time where they can catch up with each other. They probably don’t talk as much as they should, but they talk way more than they did during the time when John was busy with music and Merlin was making the web. 

Their relationship is at a point where they don’t need to assure each other too much. It is nice to share a little bit, but they both got shit to do. If they need each other’s consultation on something, which was where they arrived on Sunday evening in a very interesting group text chat with Matt Haughey who even as they spoke was locked out of his house. He is probably sitting in a cold bathtub right now because his Nest is broken and it won’t let him out.

+ John thinking about getting an Apple Watch (RL306)

On Sunday evening Merlin got a text from John at 8:42pm ”If you gonna get a new Apple Watch, do you get the stainless one or not?” and Merlin needed some context for this, because there was a lot wound up in that question. Merlin did not even know this was a consideration because John does not like computer devices and it didn’t sound he was asking for a friend. John is so tired of carrying the Internet around with him! Every passing day he gets more and more less and less with the Internet. It is in service of zilch and bobcup! 

John wants a way to have the things he needs, but not too much of the things he doesn’t want. For instance, he did not need to see the video of the shirtless dad with his shirtless son with guns confronting the guy in the orange shirt in the driveway about a mattress. He especially didn’t need it in the middle of the day. If he wanted to pad down into his lair with his bathrobe on at 1am and watch the video of the guy with the orange shirt and the two guys with the guns, he could do it in the middle of the night, but he doesn’t need that during the day. 

As time went on, John was looking at his phone, wondering: What are my basic needs here? It becomes your digital every-day carry. John tried to go without the phone, but he cannot live without it because he has a child and people are trying to communicate with him all the time. Friends are sending him pictures of their children and he needs at least be able to be on top of business. For a while he thought he was going to get a flip phone and he was going to be a guy who rolls up the cuffs of his Levi’s, who has a waxed mustache and who carries a flip phone and wears a Stetson. He could be that guy, he is not that far away from that guy, but he is not quite ready to be that guy.

John’s mom recently got an Apple Watch Series 3 and she seems happy with it and interacts with it. John doesn’t know if she is using it to its full capacity or if she is using all its capabilities. It has LTE one she and can make phone calls with it and that is the one John is going to get. The goal is to leave the phone at home and just be out living in the world with the watch. He will have to say ”Siri, text Merlin that I would love to see a picture with him and his daughter” or he could look down to get the fastest route to the nearest Arby’s or have texts read to him. If the phone rings, it will be something like ”Siri, where are the meats?  Donde esta los carnes? Where is the beef, lol! Pond” 

Merlin has an English voice for his Siri and demonstrates it on the show. He dictates a text to John ”May I mambo dogface to the banana patch?” (which is a Steve Martin bit) and it got it right. The watch is imperfect, but it is doable for some periods of time and it could restore some life to John’s life. The only thing he is sad about is that there is no camera. In this problem statement John has identified his phone as an attractive nuisance. "I do not want it, Sam I am, I do not want green phone and ham. I do not want it in my crotch, I would like it on my watch” 

John is not going to have a camera, but Instagram has stopped being still 100% fun anyway and like the vintage guitar market or the sound of Indie Rock: When you are in the middle of it, you can never foresee a time when it would no longer be. There is still a vintage guitar market, but it is nothing like it was when John was learning that trade. There will always be an Internet, but John doesn’t think it is going to be as important to us as it is now and it is not going to be as important as we imagine it is going to be. 

John does not think we are just at the beginning of a thing where the Internet is going to subsume us all, although we always imagined that was going to happen, but the Internet is really such a useless piece of shit 98% of the time that sensible people are going to abandon in droves. For instance, podcasts are via the Internet, but podcasts are not the Internet, they are not //on// the Internet, they are just //via// the Internet. 

For a lot of the things that we like, the map program and communicating with our friends, the Internet is just a series of tubes, which is what uncle Ted said. What happened is that this social media place has you come to think of it as a universe where we are wandering around all these rooms and we can’t leave, but we can totally leave and we can have all the best parts: We can still have podcasts without being on the Internet, we can still share photos, even, without... John doesn’t even want to say the words ”Social Media”, he feels so disgusted by it. 

Anyway: John is going to get an Apple Watch! And he is going to say ”Siri, text Merlin Bugbear banana pants Frankenstein Frankensense” and Siri will get to know John and he will have her speak in a German accent. 

Every single person John mentioned the iWatch [sic] to said that it will alert the cops if he would fall down. What the fuck are you talking about? If he falls down? That is one of the 5 things they can claim it does. It doesn’t do anything. Merlin counters that with the Series 3 it has become a lot more capable and he doesn’t agree that things like fall detection are the only good part of it. Merlin continues to explain what the Apple Watch can do and how you can configure your notifications to show different things on your watch than on your phone. John doesn’t get a ton of notifications anyway, but what he doesn’t want is access to Twitter at all. Merlin recommends John to start out with the stock configuration, learn what it can do and go from there.

+ Deepfake videos (RL306)

Many of us feel problems caused by constant access to news and one of the reasons might be [https://en.wikipedia.org/wiki/Deepfake deepfake] videos. Video technology has improved so much that you can superimpose someone’s face on a video. Soon there will surely be the first scandal where a deepfake video that was done for political reasons gets accepted as reality for 6 hours and creates a hullabaloo. Somebody will supposedly have been caught doing something terrible and everybody will jump on it. There will be 6 hours of screaming until it will be revealed as deepfake. From that moment on, none of us will be able to trust anything we will see or hear. All the news for this entire morning have been this Schrödinger’s cat speculation about what is happening [https://www.theguardian.com/us-news/2018/sep/24/rod-rosenstein-resigns-latest-reports-trump-mueller-investigation with Rod Rosenstein]. 

+ The current state of social media (RL306)

Merlin and John came to the Internet at a time where there was a lot of walking involved to get to a destination. You would walk, you get to your destination and then you walk to some place else. You would have certain times where you would go and check your email. In another career in 1994, Merlin had to be at a computer with a modem and the ability log in through the Internet in order to get that email, but the expectations over time have changed as the abilities have gotten wider. The Internet of many years ago felt like a series of destinations that you would walk to at your leisure, whereas it now feels more like a casino combined with the world’s least satisfying food court where you are being barraged by so many things between the things you chose to go to. 

Because of all the notifications you get and because of the compulsions you developed over time the Internet is no longer a place you go to, but the place is where you are and the place is everywhere and there is increasingly less sense of finality or existential canon. There is not that much stuff that feels like the real version or the final place. You can't say "I’m done looking at the photos, I’m done doing the thing, now I can get back to my life", because that is your life now. There is no exit door, but you can sit and scroll through your Instagram feed all the way back to the beginning of Instagram if you want. 

Instagram lately started an unusual thing where they put a notification in there saying ”You are all caught up!”, which is very interesting. It is both helpful but also quizzical because why would they intrude on your consumption of their app? It is a form of benevolent gaslighting, because if they actually wanted to do the normal and correct thing, they would give you a reverse chronological timeline. When Merlin looks at cute animals on Twitter and when he starts seeing Corgis he has already seen, he knows that he has seen all the Corgis, but because they are presenting them to you in a weird algorithmic fucked-up order, you don’t know when you are done. This new notification is their accommodation to say that ”Well, now you are done with the fun house for now, but make sure you come back!” 

It is like the nightmare casino that you can never leave: There is no exit door and every time you go through a hallway you are just in another room of the casino. It smells like smoke, the carpet is damp, and John certainly doesn’t want to stay there anymore. He has become compulsive about certain things. He routinely puts his phone down only to pick it up again immediately and go right back to doing what he had just decided to stop doing. So often he will log off his computer, pick up his phone and immediately go back to the same places he was just at on his computer. 

None of those places are giving him anything! The news is not news, the commentary is not commentary, and his mind is not being broadened or stroked in any way, other than by constant negative reinforcement. John can’t live like that! It is civilization-eroding, not civilization-building! The Internet is a constant buffet of lukewarm macaroni salad that you get one teaspoon at a time. If you had actually taken a huge bite, you would go ”Fuck this!” but no, this is what you do now. The mayonnaise is sour and unhealthy and it is going to make us bad poops later!

John has faith in people that we can’t be in this Ponzi-scheme for long. We are all contributing! Early on it felt like everybody was contributing their funny jokes and their smart ideas. We would put them into a pile for other people to use and we were rewarded because it was fun and because we got things out of it. People like Zuckerberg and Jack Dorsey were profiting from our free content and from our attention. We objected a little but we didn’t mind exactly because we were getting stuff out of it, but now we are too smart as a race of people. We are too clever as human beings to let this go on for long, now that we see it and feel it every day. There is a spirituality to being a human being that we all tease with. Some people are really in bed with their spirituality and some people are running from it all the time, but it is there and what we are doing is a disease of the spirit and so we cannot but reject it eventually. 

Even as somebody who thinks about this a lot, John is finding it super-hard to not just go into this polluted space and punish himself. It is exactly like drinking in a shitty bar! It sounds crazy that John is going to buy an expensive Apple Watch in order to try and find a way away from the Internet, but he doesn’t want to reject technology, he doesn’t want to lose what is good about the future, he does believe in the future and he wants to be augmented by all the things we have developed. He just doesn’t buy in anymore to the fact that his online avatar is... *sigh* John always feels like he is laying the ground work for some future. 

John is going to email Ted Leo and ask him about the Kickstarter for his record last year that was successful enough that people are commenting on it as a successful Kickstarter. Merlin bought it, for example. Will he break it down for John? John had John Vanderslice break down his Kickstarters for him and in the end it felt like it cost him more to do than he made, just in terms of blood, sweat and tears. It is doable, but it is difficult. Ted has indicated the same thing. It was big and successful, but in the end he had to fulfill all the promises he made and pay for all the things that he committed to. It was not like a huge windfall, but John still wants him to break it down for him note by note.

Although John is trying to keep away from Twitter, he justifies himself in still being there because he is an entertainer and he needs to maintain that venue to reach people. But how much of that is actually true? If he were not on Twitter, if he put out a thing and didn’t say anything about it on Twitter, would it really not work? There is no similar place, it is still the sidewalk of Main Street USA. If John didn’t tweet about it, he would somehow count on Merlin and Andy Richter to tweet about it. If it weren’t being tweeted or Facebooked about, would it even exist? 

Merlin’s last tweet was July 26th (of 2018). He made a very conscious decision of saying that he is done here for a while, but now he is back and looking. So far nothing has tempted him enough to contribute to the garbage fire. He is merely a consumer of the garbage fire who is vaping the outgassing of Twitter.

This morning, a person who presumably lives in Vancouver, Washington wrote to Ken Jennings and John and said ”You guys need to get your facts straight!” They were referring to the Washington 3rd congressional district as the only district on the coast that was not a blue state. The person claimed that John and Ken had said it was a red-state and it was rural, but they are wrong and they are purple, not red. John should not have replied, but he looked it up and saw that in every election since the first Bush election, except for the initial 2008 Obama sweep of the country, the 3rd Washington congressional district has voted pretty strongly for the Republican candidate, like  49% for Trump over 42% for Hillary. They also voted for Mitt Romney the second time. Let’s not kid around! 

This person was indignant because John had said it was a rural area, but they have Washington’s 4th biggest town. John looked it up again and the 3rd district is made out of 7 counties, 5 of which have a population density of less than 18 people per square mile. One of the counties is the second least populous county in Washington. Ken texted John ”WTF: What are you doing? Why are you doing this to yourself? You are using facts on Twitter to do what? To convince this person, this egg with 15 followers that they are wrong about the population density of the 3rd Washington Congressional District? Go stick your head in a bucket of water!”

+ Screen Time in iOS 12 (RL306)

Merlin is going to risk making this episode extremely fucking excruciatingly boring by saying that, albeit this is not John’s solution, but FWIW, there is a new thing in iOS 12 called Screen Time that will show you how much time you spent on your phone that day. You can have it work across all your devices and you can dig deep and see how much time you spent on Social Media. John looks at his Screen Time and he has already been on his phone for 1 hour 37 minutes today. They continue to talk about Screen Time for a while. Merlin recommends John to try out app limits, even though this is not how John normally rolls, and even though the watch is very cool and Merlin loves his. 

Screen Time says that during the last 7 days John has spent 4:50 hours on his phone per day. When he worked at Steve’s Broadway News, his shifts were 5 hours, but now John is looking at his phone for 5 hours a day. He spent 14:06 hours of the last week on social networks, 7 hours of which he was looking at Instagram, 8:30 hours he spent playing games. Obviously this makes him feel like he has some data now. If you had told him that he is looking at his phone for 5 hours a day, would he have been surprised? Maybe not! Having interacted with social media for 14 hours a week is one of the things like he used to say with cigarette smoking: If you took all the cigarettes you were going to smoke this month and set them on the floor in the living room and say ”Get started!”, you get the best perspective. 

John is going to experiment with setting app limits. Apple had set the defaults for Downtime to 22:00-07:00, which sounds perfectly reasonable. John is very intrigued by this functionality. They continue talking about this topic for a while. The fact that Apple puts features like this into their phone is interesting, because they are setting themselves up as the enemy of apps, because apps never want you to stop using them. There is some buzz in the community that if the manufacturers don’t do anything about it, somebody else will. They need to show that they can help take care of their users before some regulatory body does it on their behalf.

+ Why a Nickel is called a Nickel (RL306)

When Merlin was a kid, the people at the bank would say that every dollar is made of dimes. John’s daughter wanted a candy the other day and John told her that she had a nickel, she could buy it herself, but she said she didn’t have a Nickel, she had $0.05 and John said that $0.05 is called a Nickel and she asked why it has two names. They were doing this in front of the cash register lady who was 75 years old who with a blue-colored beehive hairdo and she said that a Nickel is called a Nickel because it used to be made of the metal Nickel and John’s daughter’s eyes starting to go back into her head. It is not made of Nickel anymore because Nickel became more expensive than $0.05 worth to make a coin that was that heavy and now a Nickel is made out of an alloy, but it wouldn’t make sense to call it an alloy because all American coins are made of alloys.

+ Going cold turkey instead of ramping-off habits (RL306)

John is considering going on a Keto //(low carb, high fat)// diet. He had a lot of success in life by just being ”Leave it!”, by simply suddenly not doing something that other people think needs a graded off-ramp. He is not going to put a patch on and gradually stop smoking cigarettes, but he is going from a pack and a half a day to zero cigarettes and whatever the consequences are, even if it feels like he is getting beat up by a kangaroo in boxing gloves, he is going to take that punishment, because he earned that punishment and it feels like ”Why stretch out that punishment?” 

John stopped drinking for the same reason and he stopped buying vintage RVs for the same reason. There are a lot of things he is just done with! Now he is trying to do the same thing with the phone, telling himself that he is done, but he can’t because it has so many little fucking tendrils in him and to be done completely means having the problem that you would come up with something you wanted to share with the world and what would you do? Send emails to his fans? Maybe John is just done from 22:00-07:00? That seems hard to do as well because his only friend at 1am is the game of Threes, otherwise he would have to lay there and think of his thoughts. John has never played Threes with the sound on, because the sound is turned off for everything on his phone. One day he turned the music on and played it with the music for a while, but he had to stop.



















FF114 - War Witch
2020-03-13

//Intro by Ben Harrison//

The French title of today's film is Rebelle, which is a true cognate and means the same in English as you would guess, but that is not its English title, maybe because the word "rebel" has been so thoroughly monopolized in English by a single entertainment franchise.

Rebels have graduated from bulls-eyeing [https://starwars.fandom.com/wiki/Womp_rat Womp rats] in their [https://starwars.fandom.com/wiki/T-16_skyhopper T-16]s and are now taking the fight to the empire, but  despite that near monopoly of association, it remains easy for the media to completely rob us of a nuanced understanding of a civil conflict by using the term "rebel" to describe one of the sides. It starts to evoke terms like "freedom fighters", which we have all been trained to mentally translate as "terrorists".

In today's film, our main character, Komona, feels so far from all of these definitions. She is a 12-year old girl who is basically too young to be motivated by the forces we imagine inspire people to become rebels. In fact, it is the rebels that completely wreck her life. Her coastal village is just sitting there, minding its own business, when they come, collect the youngsters, force her to murder her family, and then spirit her away in a motorized tree trunk canoe to become a soldier for their cause.

It is an unspecified country and an unarticulated grievance these rebels have with their government, but quickly Komona becomes special to their troops and to the rebels at large because they believe she is a witch with the power to perceive ghosts and predict assaults by government soldiers. The supernatural abilities she commands can give them a tactical advantage, so despite all the danger she is surrounded by, her specialness acts as a protective against some of the worst things that can befall a girl in a war zone. Temporarily, anyway!

In truth, her journey starts in a terrible tragedy and scarcely gets better from there. Even though she makes friends along the way and even has a romance, Komona is never safe. The war just keeps happening to her in a cruel multiplicity of ways. Director Kim Nguyen based the film on research he did with actual child soldiers and aid workers who did humanitarian work in conflict zones where child soldiers had been used.

As he tells it, the goal was to tell a story with redemption for the main character. The script uses a device of narration where Komona recalls her story to her unborn child and prays that she will be able to love that child when she gives birth. Maybe there is a glimmer of hope in there somewhere, but it is tough for me to divine through all the tragedy. "When we take the magic milk from the tree we can see things!" Today on Friendly Fire: The 2013 Canadian Film War Witch!

... that hopes God gives us the strength to love this episode when it comes out.
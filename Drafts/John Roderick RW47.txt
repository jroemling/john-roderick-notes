RW47 - Man of East and West
2016-10-29

This week, Dan and John talk about
* Fizzy water ([[[Food and Drink]]])
* John’s family ([[[Family]]])

The show title refers to the inscription in John’s uncle’s gravestone.

Listeners see Dan as John’s handler or as a middleman for opening a communiqué with John. 

John is adjusting some of his settings. 

Dan has some Coconut LaCroix going, which is how he gets in the zone. 

+ Fizzy water (RW47)

John is learning more and more about people drinking LaCroix to excess. It is really a phenomenon. John doesn’t like them very much, but he has a friend from Portland with the online handle hookersandpopcorn //(see story in RL283)// who is a motorcycle mama, and she drinks routinely between 10 and 40 LaCroix a day, as John can judge from her internet consumption. At least she is getting hydrated! She gets on frequent road trips and one of her games is that she just documents the number of empty LeCroix cans rolling around the floor of her truck. It is like a Pod people thing or the Invasion of the Body Snatchers, except it is happening with fizzy water. LeCroix has been along since 1981, but over night it was on the shelves of every store. Their graphic art looks like a Cindy Lauper record cover. It comes from LeCroix, Wisconsin. 

When John was a kid, Crystal Light made a sudden appearance in their home touted as a miracle beverage, just as Tang was when it was touted as a miracle beverage, but in fact Tang //was// a miracle beverage! LaCroix tastes like they hooked up a tube to the tail pipe of their truck and they ran that through a blackberry air freshener into some tap water. The bubbles are from grandfather’s respirator. It is not soda pop, it is not water, but it is the worst of both things. John’s uncle had special spoons that they would only use for Tang and Crystal Light, which was touted as the solution to the Tang problem. Tang was not space food, but sugar water! They drank Crystal Light for a while until they realized that it was made of old Intertubes, because it had so much Aspertame in it that they were all being pickled in their own bodies. They were science experiments drinking formaldehyde. Here comes LeCroix, the best selling sparkling water in cans!

+ John’s family

John had never mentioned that he had a brother, but he has only ever talked about his sister, and when he [https://www.instagram.com/p/BL-mIhVB1WY/ posted a picture of his brother], people were concerned and contacted Dan privately and asked him to relay the question to John. 

John has 2 brothers and 3 sisters. 

++ Privacy

It is surprising to some people how much John leaves out of his online-life. There is so much he doesn’t say about who he is and what constitutes his life in both the socio-, psycho-, and spiritual sense. John is very revealing, he talks about intimate things, and there are stories that seem fantastical, but they are all 99% true. Every once in a while John changes somebody’s name to protect them. John tries to maintain privacy, but he doesn’t care about privacy the way it gets talked about for the most part. If you want his passwords and read all his email, that is fine. He is not worried about being surveilled or tracked. It is inevitable to a certain degree. 

The prices that contemporary technology delivers onto us comes with the side-effect that we are being watched all the time and that his debit card knows exactly where John has been. Somebody at Safeway headquarters somewhere in California can probably tell you more about John than some of his closest friends, just by the virtue of knowing what John is buying at 11:30pm and knowing about the number of DiGiorno’s pizza John has shame-purchased at 11:39pm when he hadn’t eaten and should just go to bed. There is a ton of privacy stuff, but that ship has sailed. 

John was all exercised about privacy 15 years ago and he still conceals a great number of things about himself. Privacy is extremely important to him. He does talk about the things that are shameful, but other things that keep him slightly out of focus are not known by anybody, maybe except for his mom. In concentric circles out from her, people know him very well and then it tapers off in a volcano-shaped graph and most people are further down the slope.

John has never mentioned his siblings because it never seemed necessary. It is very necessary to mention his sister Susan, because she is a formidable force and influence on him throughout his life. She is a key element in understanding what John is talking about. 

++ John’s family being late breeders

John’s people are late to breed. His grandmother was born in 1886, meaning he is just one or two leaps away from the Victorians. His great-grandfather was born before the civil war, his great-great-grandfather was a major in the Confederate Army for Kentucky and his other great-great-grandfather was in the US Cavalry and they were the first company to arrive at little bighorn after the massacre //(in 1876)//. John is very few iterations away from historical generations and while he doesn’t know most of them, their little turns of phrase are communicated to him. 

++ John's brother, David Rochester Roderick

John’s oldest brother David is named after their father. He wasn’t called a junior, but in their family they always change the middle name instead. John's dad was David Morgan Roderick and his brother is David Rochester Roderick. His cousin Collin has named his third son Collin in the same way. David was born in 1949 and he was 18 when John was born. John's sister Susan is the youngest of his siblings.

David is not a nice person. He is still alive and he is largely alone by virtue of being not very nice. Susan is much more humane than John in the way that certain kinds of people are really good with kids or dogs. Susan is amazing with kids and dogs, but she is also good with people like David who is disabled by resentment to the point that it incapacitates him. Susan is really good at being gentle in situations like that, but John can’t be especially gentle with him because he doesn’t have a ton of patience with people who are being disabled by resentment. 

It will hobble you and it will take a physical and a mental toll. John doesn’t carry enough resentment that it is injuring him, but after a few years of trying pretty hard to be friends as an adult, John realized that it was a bottomless situation and he wasn’t ever going to get what he wanted, which was the feeling of being loved by those people when he needed it most. Seeking repair from somebody who is not very nice now for feelings that were created when they were not very nice then wasn’t a winning strategy, so John just absent himself.

John's brother David was the first person to ever get him drunk. John was 8 years old and David did it to spite their father. They were on a fishing trip and had flown in on a little bush plane out on the Alaska peninsula, they got into some flat-bottom boat and went out into a river-delta. John cast his line right away and pulled out an enormous Silver out of the water while none of the others thought he would be able to catch anything. Everybody was astonished, except David who was mad. He and his buddy squirreled John away into some bushes and he asked John if he would like a beer. Then he just fed him beers until he was shitfaced, just to piss their dad off. That’s how that relationship panned out. 

++ John's Uncle Al

John's great uncle Al, Aldred Caldwell Rochester fought in WWI and was the consonant bastard. By the time John came along, all of the monsters had been a little bit defanged already and John was one of Uncle Al's favorite people in the world because Al loved babies and he loved John as a baby. Everyone rolled their eyes because they knew that the moment that a child developed enough autonomous responsibility that it could express defiance against Al Rochester, he would turn on them and they would suffer his wrath. 

Al never turned on John! He would do magic tricks and pull coins out of John’s ear, he would pull little bullets out of his cufflinks and he would say ”Abracadabra”, but John couldn’t say that, and so he said ”Aberdaber” and therefore Al’s name became Aberdaber until he died when John was 25. He was like a grandfather to John and Susan because their own grandfather had died in the 1950s. Nobody knew why Al suddenly liked those kids. 

++ John's older family members being awful to him

Baby Boomers are the most unforgivably bad generation in American history. They are the most narcissistic, short-sighted, self-aggrandizing group of humans to ever come down the pipe. The fact that the years between 1966 and 1970 were world-historical in a lot of ways has [https://www.urbandictionary.com/define.php?term=Buied buied] the self-image of Baby Boomers throughout the course of their entire lives. They feel a tremendous feeling of self-importance that in subsequent years has been proven to be unearned. The Boomers have made the biggest mess of all and John’s siblings are subject to those characteristics, too.

All of John's older siblings are Baby Boomers, members of a different generation and raised in a different time. Because his family was connected to these olden days, there was a lot of violence in his family and a lot of unmitigated old-time notions about how things are done and life was led. The generation before John’s father’s generation was sitting around the dinner table drinking until someone pulled a saber out and then they had a sword-fight and somebody stuck a candle in somebody else’s eye. That was Friday night dinner! In the 1950s that generation was already pretty old when John's dad's first kids came along who were raised in an environment unrecognizable and incomprehensible to people of any generation born after 1980. It was just pure insanity! 

Everybody was talking about how awful it was at those dinners, but just having a big lunch and hiding in your room during dinner was not an option. The abuse flowed in every direction! It was awful, everybody was miserable all the time, people were angry, they were fighting, there were recriminations, accusations of infidelity, pistol-firing and overturned turkey-platters. That was just regular, normal dinner. 

John and Susan grew up not knowing what everybody else’s experience was. They were considered privileged in the modern sense of the word: They were unaware of their privilege because all the cruelty and all the insanity had been tempered by time and by the cultural explosion. John’s dad had quit drinking before John was born, but his older brothers and sisters grew up in an alcoholic household. Even though his dad was still insane when John grew up, he wasn’t so drunk he didn’t know what he was doing. 

When John was toddling around the house, the next oldest person was 20 and even they were young because they were all standing in a room full of 65 year olds with fencing scars. John’s parents and the 87-year-olds were all incredibly generous, welcoming and gentle to John, but everyone between 20 and 50 including John’s older brothers and sister went out of their way to make John feel awful. They didn’t set his shoes on fire, but they were resentful because by their estimation John appeared to be living a charmed life. Nobody was screaming at him or putting him in an ice-cold bathtub and so they resented John and weren’t very nice. 

They resented him because they were selfish and because they didn’t realize that a 5-year-old was not in charge of how well he was being treated. When John came into the room and somebody had made him a hat, his older brothers were grumbling because nobody had made them a hat when they were 5, which was not true, because they had a hat, but in 1968 they started to see it as some symbol of the war in Vietnam and they had burned their hat together with their draft card, which was not John’s fault. He was 5 years old! There was a lot of estrangement between them, because they thought that John was little boy blue and John recognized that they were just awful people. 

++ John’s mother’s daughter

John’s mother also has a daughter, but he only met her after he was an adult. She is also born in the 1950s, they are actually closer now, but he didn’t know her growing up. //(Her name must be Susan because John said in OM54 that he has two sisters named Susan and John's dad's oldest daughter is called Laura, see RL280)//

++ John's brother Bart

John’s brother Bart was always the nicest of them. He is a gentle and dreamy guy and he was a professional musician his whole life. He used to be in a band called Northwest Passage, playing the contemporary hits of the day in 1976. They were the house band of the Black Angus steak houses in the West, playing Steely Dan and Fleetwood Mac. They were a great band, it was the 1970s, everybody was on drugs and it was very Rock ’n’ Roll. They wore matching suits and they were like Murph and the Magic Tones, but Rock, like Great Funk Railroad. 

John’s dad would take him to see them, because in the 1970s you could take a kid into a bar, even a titty bar, John knows for a fact, because there weren’t the same limits, particularly in the North. John was watching Bart do his job, which was very influential on him. Bart wasn’t interacting with John specifically and was not very interested in John, but he was the gentlest with him and he reciprocated for them to be friends. Bart and John are friends. 

When John first left Alaska, he stayed in a little shed in a corner of Bart’s orchard and Bart paid John to pick apples for a while to put gas in his motorcycle //(see story in RW101)//. Whenever they would meet somebody, even until John was in his 30s, Bart would introduce him as his ”half-brother John” until John one time told him that this was irrelevant and he is his ”brother”. Saying ”half” was unconsciously referring back to the early 1970s when it was very important to those kids that they distanced themselves from John’s dad and everyone. The place where they encountered the least resistance was John and Susan because they were little children and they could call them half-brother right to their face, but the word they were supposed to hear was ”half” rather than ”brother”. That became institutionalized in the way he thought and spoke and he consisted in it until John him to stop, at which point he did stop. 

++ John’s siblings feeling injustice

John understands that his older siblings had a hard go when they were growing up, but their parent’s generation had it blisteringly harder. The things John’s parents endured in their homes are incomprehensible and that shit absolutely ran downhill, but each generation was exponentially less abusive. John’s older siblings characterized the abuse that they received as world-historical abuse that in David’s case justifies him destroying his own life. 

John can only guess what that abuse was like when John’s father, a man he knew better than anyone, was 30 years old. It was surely terrible, but that generation of people did not have any perspective and they felt that the crimes visited upon them were incomparable to those visited upon anybody else, but that isn’t true! John sees echos of that in contemporary society now where people are feeling very strongly that the insults they are being dealt in our time have no comps. 

Somehow they are missing key elements: The civil rights movement didn’t start in 2011 and people have been fighting a lot of these battles for a long time. People used to disappear in the night and not just get flamed on the Internet. Bad things happened but not so bad that it would cause you to speak ill of a man at his own funeral. There is no bad thing that precludes behaving with dignity, unless you wallowed to the point where you are no longer concerned with maintaining your dignity. You have a sense that the injustice you have been born with can never been righted. It is a form of entitlement. The injustice entitles you to be in a defensive crouch for the rest of your life. 

Put all the people who have ever lived on a ladder and on top of it is the single person who has suffered the greatest injustice of all humans in the history of time. You scroll down that list and John’s siblings are somewhat a long way down at a point when you got bored of scrolling. It is like the timeline of the history of the Earth that is 50 stories tall and the last 2 cm are since the dinosaurs, that is about where John ranks the injustices delivered to his brother, but his brother would put himself right up there with Mother Theresa. 

John was raised by the same people, but just a few years later, major changes became inextricable from their historical moment. John’s mom was the first generation of feminist and his dad was the first generation to benefit from Alcoholics Anonymous. Those two major social upheavals happened during the time of the civil rights movement. There had never been an anti-war movement before and they were living an atomic age. 

There aren’t any precedents for it and it does make that time miraculous. John came along in the immediate aftermath of it. He was born in 1968 when LBJ was still president. It had an effect on John’s parents. His dad didn’t do an Archie Bunker who maintained the believe system of 1939 for the rest of his life, but he evolved and became a different person. He didn’t make John sit in an ice-cold bathtub like his father made him. Nobody ever hit John! It is super-hard to imagine that the same guy was Vlad the Impaler only 15 years prior, because John knows he wasn’t.

John doesn’t often refer to this part of his clan, because the influence they had on him was largely negative and inhibiting. John was a precocious kid, but fun to be around if you like kids. The one place in the world where he was not well received was his own clan and in particular his own brother, sister and cousins. The adults thought he was great, his aunt Judy Lee was pretty tough on him, she didn’t like his hair cut or his table manners, but all that was a proxy for the fact that she didn’t like John’s mother. 

It is okey if you are 7 or 8 years old and you wander into a room where a bunch of 60 year olds talk about Nixon, it is okey for them to say ”Hey kid, why don’t you run and play!”, but there was nowhere for him to run and play except into the arms of those 20 year old sadists. They were hanging out with the family a lot. There were closets under the stairs where John and Susan would play with some old toys from the 1950s that were left laying around. John does not think of his siblings as influential on him except in a negative sense. 

++ The family cemetery

John's father’s sister died suddenly at age 65 at the time. She sat down to read a book, something happened in her heart and she just passed away. It was sudden and nobody was ready for it. Her husband really adored her and bought her a big abandoned lot in what was then a pretty poor and run-down neighborhood in Seattle called the Maddison Valley. He built a park for her in Italian style, which was an anomaly in the neighborhood because it was an elegant and formal park while most parks in Seattle are done in a naturalistic [https://en.wikipedia.org/wiki/Frederick_Law_Olmsted Olmsted] style. He built this park as a memorial to her and it was not a city park, but he just paid to have it maintained. As the years have gone on and he died, his kids continued to maintain the park, but the neighborhood was becoming an expensive neighborhood and little by little it became clear that they were going to donate the park to the city of Seattle because the parks department was more capable to keeping it up. 

A few days ago they had an event where they dedicated the park over to the city. The deputy mayor was there and some people from the city that John had met when he was [[[run for office |running for office]]]. Afterwards John went to the cemetery together with his cousins. John’s older generation practiced some kind of weird ancestor worship and had spent a lot of time at Lake View cemetery, Seattle’s old cemetery where among other notaries, Bruce Lee and Brandon Lee are buried. The Rochesters and the Knutsens have a little cluster of graves where close to a dozen of them are buried. They went there all the time, and at every opportunity they got, John’s dad, uncle and aunt would go to the cemetery. 

++ John’s dad’s grave

John's aunt has been dead for a long time and his uncle Cal died right after that, so ever since his dad died, uncle Jack is the only one left. He lives in Alaska and all of a sudden they weren’t going to the cemetery anymore. Somehow by virtue of inheritance the control over the family grave plot had fallen to uncle Junius and it was very complicated talking to him about where anyone would go. His nature was to maintain hegemony over the areas of the plot just in case he would have 40 kids late in life, or something. Conversations about the family plot could potentially devolve into lots of legalistic talking with everybody staring over one another’s shoulders. 

John's dad didn’t want to argue with anybody in the family about where he was going to be situated in the family plot and in his inimitable fashion he short-circuited the thing by secretly buying a neighboring plot that is separated by two other families and facing the wrong direction. If you stand on it, you can see the rest of the family standing on the family plot and you can’t reach out and touch hands, but you could both hold a broom stick. John’s dad being over there surprised everybody and it fell to John and Susan to erect a grave stone suitable of their father’s legacy. 

Because Susan was traveling the world and was unavailable for comment, John talked to some local artists about a grave stone. One of them once suggested a big crystal obelisk which John couldn’t afford, but it would have been a big fuck you to everybody. The fact that John’s dad’s grave is 15 feet (5m) from the rest of the family and facing the wrong direction is already the kind of fuck you that his dad would have liked to live on forever, but while his gravestone should be an honorable grave stone for an honorable man, there should also be something on it that communicates fuck you to everybody. As he died so did he live. 

++ Uncle Junius the elder

John's great uncle Junius the elder's gravestone has this long inscription about Raconteur, business man, ethicist, paleo-biologist, East and West his worlds collide and they were standing over it the other day reading it. One of John’s cousins asked what this was about and they all knew that Al had written this. East and West suggests to the reader that this was a man who bridged continents and who’s business interest took him to Japan and China, which was then affectionally called the Orient. In reality it just means that he lived on the East Coast for a while and the West Coast for a while. 

Uncle Junius (the other one) married the woman who was the heir to the Buster Brown shoe fortune. He lived in Greenwich, Connecticut in a big house on the water, the Buster Brown shoe fortune house. It gave him the ability to write pedantic, dictating letters to John’s grandmother about how she was conducting her life. 

++ Nieces and Cousins

John is close with his niece, Bart’s daughter and he is close with Bart. Bart developed respect for John as a musician and he likes John’s music. Bart is also the nicest of them and he is generous with showing respect and praise. John wishes he saw him more. David on the other side has never praised John because he is incapable of it. 

John is also starting to get to know his cousins a little bit better and he is building relationships with several of them. They are all interesting people, They all have red hair, and they are all successful business people. John’s aunt was a hard charger and she raised her kids to be successful in the world. Because the youngest of them is 10 years older than John, they weren’t cruel to him, but they were not interested in him either because he was just a little kid. Now they realize that John is a man in his 40s and they are all becoming friends, which John is enjoying quite a bit. 

Their recent visit to the cemetery was the first time in his life when John stood together with his 3 first cousins alone. They were all looking at each other in amazement, because they were all grown people and at the current age, that 10-year difference doesn’t even seem that big. John’s daughter really likes his cousin Page and Page loves Marlo. He would never have thought they would bond, but there it is: Family!

Family was so important to the older generation for some fucking reason. They were so hard on each other, but it was really important that they got together, that we remember, that we persist in a closeness that none of them felt. After they were all gone, everybody else felt released from a weird contract they had never signed. Time goes on and now John’s cousins are the only other people in the world who know what John is talking about when he is talking about these people. It ends up that family does matter, at least it started to matter to John. 

++ John’s dad’s funeral

When John visits his dad's grave, he doesn’t talk him because he only put a little bit of his ashes into this graveyard and 90% is in an urn under John’s piano, meaning the vast majority of his dad is with John every day. He put his pilots license, half a cup of his ashes and a couple of high-end chocolates in a little wormwood urn and buried it in the cemetery. John was the only one there, except for the director of the cemetery and two guys with shovels, like out of an Edward Gorey drawing. It was a rainy day, and John showed up with his little urn that he had made. 

His dad had been very specific and wanted his ashes spread in a variety of places, like sprinkled in the middle of Lake Washington where he had spent so many wonderful hours on the Washington Crew Team and once he swam across Lake Washington! He also wanted some of his ashes secretly deposited under the Holly Tree in Volunteer Park, because his grandfather George Alfred Caldwell Rochester had his ashes put there in 1929 when the Holly Tree was very small, but now it is very big and when they would drive by the Holly Tree, his dad and uncle would both salute like little boys. John put some of his ashes under there, but he doesn’t know if he wants him to salute or not. His dad also wanted some of his ashes sprinkled from a bush plane over Maxis Mountain outside of Girdwood and Susan once convinced him that he also wanted his ashes over Mount Susitna in Alaska, but John convinced him otherwise. John’s dad had a plan to have his ashes widely dispersed, but John preferred to keep most of it under his piano for 10 years, just because it seems fitting. 

When John was a little kid, his dad had an office in the top floor of the Alaska Railroad building and behind his desk there was a big credenza with sliding shelves. There was also a walk-in vault where they used to keep gold dust, it was a real old West kind of building in an old West complex of offices that he inhabited as the chief council. John would play under his desk and one day he found an urn with the ashes of the secretary of transportation’s mother who had asked John’s dad to sprinkle them on Mount Susitna, because she had always wanted that. 

His name was Brock Adams, he was a good friend of John’s dad, and he had been a senator, a representative and Jimmy Carter’s Secretary of Transportation, a prominent Democrat and Washingtonian, not a man unfamiliar with scandal, but a man of John’s father’s era. He had served in the Navy and they were pals. Brock had asked John’s dad to sprinkle his mother’s ashes on Mount Susitna, but dad never got around to it until 1978 several years afterwards. It hadn’t come up in conversation, but Brock had just assumed that John’s dad had done it and he eventually did. 

As John’s dad ended up under John’s piano, John thought back to Brock Adams’ mother and said ”This is fitting, dad, don’t you think? That you live here with me for a while and bathe in the sounds of my piano!” and when his daughter was little, he had to teach her that this was an urn we don’t play with and we don’t knock it over. 

The small quantity of ashes John did bury are in the cemetery. They did this whole funeral business and prepared a hole for John, carpenting around it with special carpenting so it doesn’t look like a hole in the ground. There were those two guys with the shovels and the unctuous president of the cemetery. Because it is a historic cemetery they have this whole kit and caboodle and they were all standing there. The president asked John if he wanted to say a few words, but John was embarrassed, like "What are you guys doing here? Leave me alone!" He didn’t want those guys watching him, and they uncomfortably turned around, but that is not what he wanted, he was not going to take his shirt off. It was raining, and everybody else was in the exact dark Navy trench-coat that you would expect at a comically serious cemetery. 

John was genuinely moved. He was the only one in attendance, having this very strange Harold and Maude experience of: ”Okay dad, it is you and me, we are going through the portal here, now you are going to be in the cemetery and we are all going to stand on your grave stone and tell jokes and stories about you and your people the way we used to do together. Sail on, sailor!” John put his urn in the ground and said ”Thanks a lot, you guys!” and he just booked out of there. What was he going to do? Stand there and play the bagpipes while they filled in the hole? 

John and Susan still haven’t procured a grave stone and John’s dad’s actual grave site is now completely grown over with grass. The marker that marks the site is not meant as a grave stone, so the cemetery doesn’t manicure it. After Susan came back from her world travel John told her that figuring out his dad’s grave stone is a project that feels beyond his capabilities. It requires that you talk to everybody in the family, it requires that everybody is invested, it is a management problem that John can not bite off. Susan promised him to take care of it because she can do things like that in her sleep, but she proceeded not to do it either. 

As the final [https://en.wikipedia.org/wiki/Coup_de_gr%C3%A2ce coupe de grâce], John’s father’s grave, which is sitting elsewhere from the family plot and facing the wrong direction, also cannot be located without 15% uncertainty. John drew a circle and said that it was within that vicinity and his cousins are not exactly people who roll their eyes, but they were giving John the concerned nod. If his brother would have been there, he would have been rolling his eyes. Maybe this year! David Roderick, lawyer, Man of East and West, Raconteur, something that echoes the pure BS that is on John’s great uncle’s gravestone, but not so much that it reads as a parody. 

When John’s daughter was very young, she understood really early what cemeteries were, which is true of a lot of kids, because it looks like a cool park, but she understood it before she could really even speak in long sentences. She said: ”When people die, they go in their stone”, which is true, and that is still the turn of phrase they use. They need to get a stone so that she can fully understand that grandpa David is in his stone.


















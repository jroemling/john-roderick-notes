WS13 - Darkest Hour
2017-12-01

John Roderick is a musician, history buff and one third of the upcoming podcast ”Friendly Fire”. He is very excited about this new show because he has been fan of Maximum Fun for years and years, but this is his first own Maximum Fun podcast. He is teaming up with Maximum Fun superstars Adam Pranica and Ben Harrison of the hit-podcast Greatest Generation. John has composed the MBMBAM theme music, he has been a guest on some of their other podcast and he is now a guest on this one, so he is trying to get his dirty boots into every Maximum Fun property. 

Friendly Fire is a show where the three of them will watch war movies from across the whole spectrum. The other guys are both cinematographers and film makers, so they obviously have a lot of feelings about movies, while John is a history buff and war movie guy, so they asked John to add some color commentary about what the wars were and how well the movie reflects the war. Movies also happen in their own time and a WWII movie from 1975 is very different than a WWII movie from 1945. John tries to talk about that aspect, too. 

John is a little bit older than his co-hosts and probably also than the two whipper-snappers interviewing him. He grew up watching war movies in his dad’s lap. His dad was a WWII vet. During the 1970s war movies were on TV constantly after the Late Show, it was a cheap way to fill up television time. John has seen //so// many war movies and they had a massive impact on him from the time he was a little kid. The WWII-movie that really connected to him and that he saw in the theaters was a movie called ”Midway” about the Battle of Midway. It is one of those 1970s epics with Charlton Heston, a hilariously overwrought movie. 

John might actually have seen it in Sensurround, because he saw it in Downtown Seattle at a big theater. For Sensurround they put giant bass speakers in the front of the auditorium and you could just feel it all in your bowels. There were not a lot of movies released with that encoding, because when you were in a single theater, pieces of the ceiling would come down, and if you do it in a multiplex, all the other theaters could hear your baseline, but Midway was one of the few Sensurround movies. John remembers it to be a very overwhelming experience, so maybe it was Sensurround, but he didn’t poop. 

The reason for picking this movie as his favorite is that 30 years after the end of the war there were still a lot of airworthy WWII-era planes. It was this weird interregnum where these planes were still in the air, but they weren’t recognized as being incredibly valuable yet. If you would find a WWII fighter plane in flyable condition today, it will be a million dollar plane or more, but back then hobbyist had them and they were kind of lying around. This movie was filmed using real WWII era hardware and they were not even shy about blowing them up pre-special effects. Also: Midway was a hell of a battle. 

Is there a movie that gets WWII super-wrong? They all kind of get it wrong. ”The Longest Day” is a fantastic film that explains everything that happened on D-Day across 15 different theaters of war, but Red Buttons is in it. On one hand it was a massive production and incredibly accurate, but Red Buttons?

WWII movies that get it right? They all try to get it right. The thing about Saving Private Ryan that made such an impact at the time was that it really felt visceral. It is an absolutely mirthless film! On one hand it does accurately convey the horror of war, but on the other hand men were less culturally introspective in the 1940s. It felt a little bit like a war movie where we were retroactively superimposing a 1990s retrospectiveness onto these guys who at the time would have probably been laughing in the face of horror or talking about football and stuff to avoid their actual feelings. 

Thin Red Line is going in the opposite direction. While Saving Private Ryan really puts you inside the ships with that opening action sequence, Thin Red Line is a much quieter movie with a lot of monologues describing what is inside the soldier’s heads and them questioning what they were doing. John went to see Thin Red Line with his dad who was a Navy pilot in the South Pacific during WWII. His dad walked out of the theater and basically said that that is how it was. He had a very emotional response to it although he never talked about the war. John would get together with him and his 80 year old friends and they would just argue about who saved democracy more. They never talked about their experiences, but they just claimed that each of them defeated the Japanese single-handedly.  Watching his dad watch that movie was pretty great. It is such a beautiful and wonderfully filmed movie and it really transported John's dad in a way that other war movies never did. There is a scene in the film where a C47, which was the kind of plane he flew, landed on a dirt strip and taxied around and swung its tail around and started throwing ammo boxes out and his dad was just like ”That’s me!” That film is a subtle, but really beautiful rendition. 











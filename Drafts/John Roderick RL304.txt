RL304 - Go For Bounce
2018-09-10

This week, Merlin and John talk about: 
* Merlin transitioning from TV shows to YouTube videos ([[[Movies]]])
* Words that have become inappropriate to use ([[[Factoids]]])
* Friendly Fire community ([[[Podcasting]]])
* Eric B. & Rakim ([[[Music]]])
* Go for the bounce when playing bass ([[[Music]]])
* John never learned other people’s music ([[[Music]]])
* Nashville tuning ([[[Music]]])
* John not interviewing Stephen Malkmus of Pavement ([[[Music]]])
* Bad things on YouTube ([[[Movies]]])
* Bitcoin ([[[Technology]]])
* Apple September 2018 Event ([[[Technology]]])
* Caffeine consumption ([[[Food and Drink]]])
* Life choices ([[[Personality]]])

**The problem:** John has been arrived here, referring to the choices he has made in life that have arrived him at the place where he is now. 

The show title refers to John playing the bass more and being afraid of the bounce, but he got the recommendation from a friend that he just has to go for the bounce.

John started the show with ”beyond a sigh” and Merlin was quite amused by it. The first thing John heard from Merlin was an exhale, which was so inspiring that he also felt like exhaling. ”I’m leaving you all my car parts! I didn’t have the money or I would have gotten roses ” //(lyrics from Carparts by The Long Winters)//. John’s exhale was definitely a sigh, but it was also a little bit annoyed. There are so many things you can’t say to your kid that you still need to communicate to your kid and some of those things have to be communicated with different kinds of mouth noises. Words are a kind of mouth noise, but not the only kind. 

Merlin’s mailman looks like a confused and bewildered Jay Z, which is really disorienting, but a surprisingly cool look. Jay Z already seems a little bewildered and he doesn’t come off well in those videos. Queen B has such star power, who is going to stand up next to her and look good? Yas Queen!

The singer of Portugal! The Man, John Baldwin, texted John the other day and said he is wearing a ”Suck it, Morrissey!” shirt and he has another one in the mail for John. 

John didn’t go to XOXO this year.

+ Merlin transitioning from TV shows to YouTube videos (RL304)

Merlin doesn’t watch TV shows at night as much as he used to. He gets a little dad time after everybody has gone to bed and he used to use this for TV shows, like he did last night with Succession, but he increasingly watches YouTube videos. Some of them are very masculine, intellectual and interesting like a two-part deep music-theory breakdown of Peg. It shows him and a guy playing an organ and goes really deep on how he chose the chords and the voicing. They continue to talk about YouTube videos, like Russian dash cam videos. Merlin spent his entire weekend with gang stalking and targeted individuals and wants to talk about that. 

Merlin also watched somebody ordering a mystery box from the dark web, he watched the linguist John McWhorter discussing how Donald Trump speaks, a Goodfellas review by Siskel and Ebert, and videos about Marshall Crenshaw, The Knack, Nick Lowe, and Sniff ’n’ The Tears. John did a deep-dive on The Knack last night, which was the #1 record of 1979. ”That you know you can’t erase / Till she’s sitting on your face” is a line from ”Good Girls Don’t” by The Knack. It turns out that all their songs were about teenagers and critics found that a little questionable even at the time. 

Merlin also watched Slade, Blue Swede, Paper Lace, and Bay City Rollers, which are all pretty good. User RwDt09 has compiled the opening credits to all the new TV shows season by season, starting with [https://youtu.be/jzPK2O-BUG4 a 21 minute video] on the 22 new shows of fall 1981 and proceeding forward. Merlin started with [https://youtu.be/TfGZOmcR-nU a 36 minute video] called ”36 new shows of the hellish mid-season TV of 1979”. In one season there were over 20 new shows and only 5 of them made it to a second season. 

Merlin is not proud of those moments when he looks at the clock. Like a gentleman he made a deal with himself to go to bed between 10:30pm and 11pm, but as he looked down it was 11:38pm because he was watching the opening credits from 1980s TV-shows and that made him feel small. John did not know you could look at your YouTube history and he would rather not. He recently watched The Stone Temple Pilots live at Daytona Beach //(maybe [https://youtu.be/3hCnZ4WNug4 this one])//, How to pronounce Enver Hoxha, the leader of Albania //([https://youtu.be/Ap9caoDqdNw this one])// There is an [https://youtu.be/VjfeO7_5xbA epic cat chases dog!! warning!! lots of hysterical laughter!] video, there is [https://youtu.be/0H25ve3qts4 A cat runs into a bakery door in France], a [https://youtu.be/V_laNt7Sh6g Ron Swanson Meat Lover compilation], [https://youtu.be/tf7IEVTDjng Introducing spot mini], the latest robot from Boston Dynamics that looks like a dog that is going to kill you, [https://youtu.be/qE9XxgsaifA How to install tile adhesives], [https://www.youtube.com/watch?v=bLGCmPLVirc How to prounce fin de siècle] and [https://youtu.be/ZblPwNLH6hg Don Rickles roasts Ronald Reagan]. 

Merlin doesn’t even want to talk about this except he needs somewhere to talk about it. Last night he watched [https://youtu.be/qofdWFv7m0U Burt Reynolds and Don Rickles]. They are fun together, especially if you get [https://en.wikipedia.org/wiki/Dom_DeLuise Dom DeLuise] in the mix. Merlin dove deep on gang stalking and targeted individuals and he apologizes in advance that John will spend his whole week on this if he will only start to search for targeted individuals on Twitter. John wonders if it will be the same like when he discovered what incels were, but incels were just an evening for Merlin when he discovered an reddit thread from an HR director at a company where an employee was threatening to sue because that person was flaunting their relationship and this person was looking for appropriate incel rights. A phrase John used to use on the Internet was forever-alones, which is similar to an incel.

+ Words that have become inappropriate to use (RW304)

On a different podcast John used the term neckbeard and got quite a bit of reader mail telling him that neckbeard is no longer an acceptable term because it suggests an entire type of person and is ultimately a body-shaming term suggesting that neckbeards are fat and culture-shaming. It is one of those things that John and Merlin wouldn’t know, like people of a certain age know that you don’t call an African-American man for boy.

At the recent state fair there was a Big Band and John dragged his whole family over there, explaining to his daughter that this was his dad’s music. His mom also listened to a lot of big band music when she was married to his dad. The band played Jumpin’ at the Woodside and John's mom leaned over and sayed ”too slow!” Then a couple of cheery fresh-faced, but middle-aged white people got up and said ”Now we are going to do some vocals, everybody look out, because here comes Chattanooga Choo Choo!” There was an African-American couple in their 70s and John wondered how this was going to go when the band launched straight into ”Pardon me boy...” By the time John started singing that song to Marlo, he made the choice to change that line to ”Pardon me sir...”, but even then there are lots of shoe shine people in it. 

The couple was just enjoying the music and didn’t do a deep reading of the text. They were also not 24 and didn’t need have a theatrical reaction to everything that happens in the world. They heard it all, seen it all and done it all and it didn’t even register with them, but it registered with John. If he would have talked to those singers, they would have said ”That is the text of the song”, and absofuckinglutely it is, but it is also a pretty loaded word, but only in context. You can’t put it on your Millennium list of words you can never ever say again because it is a word you say all the time, but you can’t say it sometimes. 

John is not sure if this the same thing as the word neckbeard. He knows a lot of people in that community, including some of his best friends like George Lucas, and these days you cannot tell anymore who is on the white horse of justice. When Merlin was younger, the would say ”Check out that guy over there!” in a neutral way and if somebody asked ”Which guy?”, he would say "The black guy!" if he was the one black guy in a group of non-black-guy people. The problem is that the term neckbeard is almost always pejorative. 

Sometimes John will say on the show that a guy was mansplaining to him and he will get a bunch of letters that men can not mansplain to men, but they do it all the time! Mansplainers are a category of people who will mansplain to the paint as it dries. There is something mansplainy about neckbeards, not only that they are sitting there growing a beard on their neck, but it is a certain type of people. On one of the fan forums of John’s other podcast there was a little bit of communication where fans were discussing how neckbeard is no longer a safe term.

+ Friendly Fire community (RW304)

John, Adam and Ben recently reviewed the movie Kelly’s Heroes on Friendly Fire, a war movie starring Client Eastwood, Don Rickles and Telly Savalas. It is another Dirty Dozen type movie, but it is not as good as Dirty Dozen, but it is kind of a bad movie. They had recorded that episode a long time ago and quite a few people were sad that John was so mean to Kelly’s Heroes. First they couldn’t wait to listen to this episode and then John really didn’t like Kelly’s Heroes which seems unfair, because Kelly’s Heroes is really fun and they don’t understand why John doesn’t like it. 

We have crossed the Rubicon into a world where loving things is all that matters. Being a fan of a thing means that you are almost equal to the creator of that thing, because you are such a fan of it. Ben and Adam have another show about Star Trek. They love Star Trek and they both agree that it is great, but they tease it good-naturedly. Even though it is hilarious and there is lots to laugh about, it is still amazing in the end. A lot of their fans love to hear their teases, but at the end everyone is brought back into the circle and they all agree that it is great. 

John’s take on a lot of things is that they are not great, but they are bad. Lots of things are bad, in fact, and some things are bad for you and we should say so. John is saying so now and people should agree with him, or at least hear him out, but that is a different temperature compared to getting to the end and saying that Kelly’s Heroes is a great movie, super-fun, 5 stars! John doesn’t even remember recording that episode and he doesn’t remember his rating for Kelly’s Heroes. He surely said that Clint Eastwood and Telly Savalas were not funny, Donald Sutherland in this movie was really not funny and there were a lot of things in this movie that were not funny. They continue to talk about what actor was in what movie.

+ Eric B. & Rakim (RL304)

John is trying really hard not to go on the Internet at all, he just doesn’t see any value to it anymore except watching movies and YouTube videos. This morning Merlin was reading Pitchfork’s reappraisal of the best records of the 1980s. Whether or not he agrees with, it reminded him how much he loved Eric B. & Rakim back in the days, which let him into some Spotify which he used to listen to obsessively, but hasn’t listened to in years. One of their first songs besides Paid in Full was I Know You Got Soul which was played in clubs a lot. Six Minutes of Soul Remix has bars and bars of I Want You Back by the Jackson Five. This led him down the rabbit hole of isolated bass track for I Want You Back, which led him into The Adventures of Grandmaster Flash in the Wheels of Steel long version. That is how Merlin spent his morning and it seemed very wholesome to him. It is a good use of the Internet

+ Go for the bounce when playing bass (RL304)

The other day John was in a conversation with a professional musician, a very good laid back in the pocket bass player who was on tour. Lately John is playing the bass more and he has good bass melodies but he is lacking the bounce. It is not that he doesn’t have bounce or can’t go for bounce, but he is afraid of the bounce because he wants to be in the pocket, which he can do if he doesn’t try to bounce, but if he starts to bounce like James Jamerson, he just doesn’t have it. His friend said that you just got to go for the bounce! He recommended John to listen to [https://en.wikipedia.org/wiki/Aston_Barrett Family Man], the bass player of Bob Marley & The Wailers, who is playing a co-lead vocal with Bob Marley singing. He started humming some Family Man bass lines that we all know, but John hasn’t been able to figure out yet if there are isolated bass tracks of Family Man. 

Merlin interjects that there should be an international law that makes people use the word ”Cover” in the title if their video is not the original. Merlin wants to hear every extant version of Everlong by the Foo Fighter that they and Dave Grohl have done because it is close to his favorite song, but he doesn’t need to hear you play it on the ukulele, with all due respect. Just say ”Cover!”

+ John never learned other people’s music (RL304)

John never learned other people’s music, he didn’t have his Beatles in Hamburg years, he is not like Jonathan Coulton, he doesn’t understand how chords work together, but when he sits down with an instrument, he plays a chord and figures it out. If Merlin asked him to play a diminished chord, he would move his fingers around until somebody said ”That’s it!” Merlin can do a 6 or a 9 in his sleep, but he still doesn’t understand augmented and diminished. 

John can’t even really do a 9. A 6 sounds like the end of every early Beatles song. Every time John grabs a guitar, the second chord is a complete mystery to him. During the recording, John got a guitar that had been sent to him by a listener named Hector, a Univox that looks like a copy of a Mosrite Ventures model //([https://en.wikipedia.org/wiki/Univox_Hi-Flier this one])// with cool little Humbucker pickups. Kurt Cobain broke a lot of them. John plays a D6 according to Merlin’s guidance.

+ Nashville tuning (RL304)

Merlin knew he hasn’t played his guitar enough when he picked it up and it was still open tuned. John also goes through open tuning phases. He has too many guitars, like a Sonic Youth, and some of them are in different tunings, but not with a screwdriver or drum sticks in them. One guitar is dedicated to Nashville tuning, because that requires different strings. Nashville tuning only uses the high strings of a 12-string guitar. The low strings are the same as a 6-string guitar, but you can take the high strings and put them on a normal guitar and it will sound like the high part of a 12-string. Everything just sounds magical! Gimme Danger from Raw Power by The Stooges has a Nashville tuning. If you capo it somewhere and play it into an AC30 Top Boost with the volume and treble all the way up, you can knock an airplane right out of the sky! 

Merlin spent half a day last week trying to learn the song Falling Slowly from the soundtrack of the movie Once. ”Take the sinking boat and point it home” It is really good, but tuning for that was pretty monkey balls, which for Merlin does not apply to that many different things. Now he needs to retune, but his strings are old and he is a little worried. He likes the big time Grunge sound of the Neil Young drop down to D, a Cinema Girl tuning.

+ John not interviewing Stephen Malkmus of Pavement (RL304)

”We are underused” by Pavement is a really good song. Merlin enjoys their middle records that are kind of underrated even more now. John liked those records at the time, but people were making fun of him and he didn’t know enough about it to be a snob about it. They were cheeky.

The other day, the editor of the very nice, very glossy and very smart Fretboard Journal guitar magazine contacted John. The magazine often features guys who own 42 D’Angelico guitars each worth $1 million and it is a little bit of a rich guy magazine. The editor asked John if he would interview Stephen Malkmus for them and John said ”Yes, of course!”, but then he walked around with a pit in his stomach for about a week. John is terrified by Stephen Malkmus because he is probably going to do a 1990s Punk Rock thing where he will say ”I don’t even care about guitars” or something. Some of those people still have that attitude and Malkmus doesn’t seem like the type of guy who is going to geek out over his guitar gear. Even though he is totally a guitar-oriented guitar player, it is unlikely that he was going to say ”I had a 74, but I replaced the pickups” or that he would talk about his distortion box.

John called the editor back and said that he doesn’t think he is the right guy for this and there are other people better suited than John who are ready to go along on whatever journey Malkmus wants to take them on, while John would just be lost. If he would ask him what kind of amps he used and he would give him any amount if ”I think it is kind of unimportant what the gear is. It is really more about the intention!”, John would be gone. He should be interviewing him for Intentionboard magazine then! Merlin would so subscribe to that! There they would get past all of the blah blah blah on how he rewired his pickups and talk about intentions. If he would say ”My intentions don’t matter!” that would be ”wow!” 

Do you ever oil up your pods so they are really fast, like Eddie VanHalen? Have you ever seen a grown man naked? Do you like Gladiator movies? Do you ever put pencil led on your nuts so you can do a big drop? Merlin used to spray WD40 on his nut and you can do a drop and the strings would come right back into tune. One of the videos on John’s YouTube history was Eddie Van Halen talking about [https://youtu.be/3CwDKqfMX3o inventing tapping]. The first thing he says is ”I didn’t invent tapping, but actually I did and here is how” and then he demonstrates some woodly-woodly. Merlin says that the guy from Genesis invented it, but he can’t remember his name //(probably Steve Hackett)//. There is a part in Supper’s Ready where he does some ripping herons.

+ Bad things on YouTube (RL304)

John somehow clicked on a thing where a turtle was drowning a rat, which is a pretty intense video! They could start a Patreon and record a whole episode where they would just look at stuff on YouTube together. Snapping turtle eats three adult mice! Rat attacking a pigeon! There is so much YouTube! You can put any dirty thing on YouTube and it is not hard to end up with Nazis. There are factories or workhouses who put out a giant amount of clickbait videos for kids that are extremely disturbing. It has been cited as an example of AI in reverse. 

Those people look for trending words and titles and have actual actors act out things based on trending topics in short videos. They put up dozens a day! It could be ”The girl from Frozen is kidnapped! Logan Paul” or whatever. They come up with crazy scenarios based on popular keywords, because all they need is a click. The key is to get a good thumbnail and title. It doesn’t even matter what the video is. Autoplay just keeps pushing it until you get to a Nazi. It could be ”Elsa and a Nazi! Logan Paul!” John is now watching a video of a guy in Japan with 5 big dogs on a Scooter.  

Merlin made his family watch some guitar videos, but they really didn’t like that. Michel Angelo Batio plays a two or four necked guitar. John recommends the violin- and keyboardplayer Mary Kobayashi ([https://twitter.com/marykoco @marykoco]). John introduced her along with a group of like-minded people to Carlos Santana Shreds and it was very exciting to watch a person who had never seen Santana Shreds see it for the first time and also laugh until they couldn’t breathe. The Jake E. Lee one just kills Merlin. 

There is a video about how to tactically open velcro in the field. Merlin watches a lot of rollercoaster videos. He showed his family Yngwie Malmsteen’s Arpeggios From Hell. John has been thinking of doing an Yngwie Malmsteen episode of [[[Omnibus |The Omnibus]]], but he doesn’t want to give it away. Merlin watched [https://youtu.be/ADwfyxpriAM Mediterranean Sundance with Paco de Lucia, John McLaughlin and Al Di Meola] and John realized that Merlin watches much more interesting things than he does. Most of what John watches is just embarrassing, like Austro-Hungarian plane crashes.

+ Bitcoin (RL304)

John asks why Merlin didn’t buy any Bitcoins when they came out, but Merlin didn’t particularly understand it, although it is interesting. He doesn’t really understand money either.

+ Apple September 2018 Event (RL304)

There is an Apple Event on Wednesday and the rumors are talking about Apple is changing the port again. USB-C is so much faster and cheaper and it is a standard except it is not really a standard. There has got to be an advantage! The new watch got 9 complications and a bitch ain’t one. 

+ Caffeine consumption (RL304)

Merlin made a pitcher of Ice Tea from bags this morning although it is a bit late in the season, but he likes to switch it up. The sure sign of him getting a cold is that the coffee doesn’t taste good, but sometimes coffee just doesn’t taste that good even if he is not sick and then it is time for a break. Ice Tea will give him enough caffeine without getting crazy balls, but that is the whole reason John drinks caffeine. You can drink a pitcher of Ice Tea all day long and you get to pee a lot because it is a diuretic. Merlin finds it fun to do some Hemingway maintenance drinking and Ice Tea also keeps it to a manageable amount of caffeine.

John has been struggling with caffeine lately. He cannot get his head around the fact that he can’t drink coffee at 6pm anymore and he refuses to acknowledge it. He keeps going to bed at 4am and waking up at 5:30am, not knowing what to do about it. All the people who are forced to care about John and listen to him talk about his day will ask him why he is so groggy and he will tell them that he slept for 1,5 hours last night. Their advice is to not drink coffee at 6pm and drink other things instead, but John doesn’t want to ask a server for herbal tea. He wouldn’t know what they are talking about half the time, he doesn’t want to study up on herbal teas and he doesn’t want to be a person who knows more on herbal teas than some people know about wine. John’s sister will order tea and send it back if it is the wrong tea. Merlin’s wife got this delicious low-acid coffee. She does half decaf and half regular and slowly tapers off the caffeine. 

John is a cold turkey guy. He believes that any attempt to make your suffering slightly less is just effectively causing you more suffering. Instead you just put it down and you leave it and every time you reach for it, you tell yourself ”Leave it!” over and over. You do the full Gibson until you break whatever animal inside you has any spirit left. You lay in its cage and say ”This is my crate, not your crate” and the little animal that really wants a coffee right now just stands there, plaintiffly looking at you crying. 

You do that over and over for all the things instead of putting a bunch of effort and though into doing micro-diminishments on the amount of caffeine in your coffee for 9 months. That is just a form of pain displacement and sounds like worse pain than just having a bad headache for three days after you quit drinking coffee. Merlin used to drink 1,5 pots a day, but now he is only having 1 cup of coffee a day. John hasn’t quit coffee in a while. Over the years he has arranged his life in such a way that in case he needs to take 2 days off where he is just being punched by the universe, everybody will give him 2 days. 

+ Life choices (RL304)

John has been reflecting on the fact that he has made a lot of life choices that have arrived him here at this life that he currently has. He has to acknowledge both the positive and the negative outcome and can’t just enjoy the ways in which he is liberated from the grind without also having to own the disadvantages with the same amount of chwaz (?) //(See also RW122)//. He cannot get a bank loan, but he can not complain. If you are like ”Fuck the police!”, then you can’t call the police when you get robbed and you can’t be mad either. If you don’t have insurance and you get hit, you will not be able to claim any insurance. John cannot complain, but it has nothing to do with all the ways in which people say ”You can’t complain!” He gets to podcast for a living but that means he does not seem very trustable to the other parents at his daughter’s school. When they grab their kids as he walks through the hall, he just goes ”Lol!”

John is in a position where he can quit coffee and he wouldn't have to show up somewhere at 7am with a splitting headache afterwards. Merlin feels very much the same way when he is constantly demanding that people are on time for things. Is that a version of the P-word? Maybe other people don’t have the same flexibility as he does? Conceptually it is the unexamined lucky break that you can’t un-have, although these are situations which we have arrived ourselves at. 

We made a lot of choices somewhere back in the old days that we have to live by now. You can’t make choices all along and then complain ”Where is my 401K?”. John is chewing on this all the time and it was the thing that he watched his dad reckon with. John’s uncle, his dad’s brother in law was a guy with a lot of money and they were part of the Greatest Generation culture of people who had money and socialized with other people with money. They went to museum galas and so forth and a lot of them decamped to Palm Springs as they got older. 

Both John’s dad and John's uncle were friends with Georg Weyerhaeuser and they regarded him as one of the good rich guys. Living on the other side of the environmental divide, we would not think of the Weyerhaeusers as being the good guys today because they were the tree-cutter guys, but for them he was one of the good guys because he wasn’t a dick. 20 years ago they all started to be down in Palm Springs having cocktails with each other, sitting around in the warm evening next to their pools. 

John’s dad got invited down because those were his friends and he would stand around at these cocktail parties and get into disagreements with women in muumuus who had very oversprayed hair because they would make some catty remarks about his politics. Everybody but him would be drunk and he would come home and be mad. What really upset him was that he had gotten to be 75 and lived his life in a righteous fashion on behalf of others. He worked in politics, he strove for justice and he had scoffed at all these people his whole life because they were just in it for the money. At 75 he realized that they all had money, but he didn’t have any money, and he didn’t mind being in Palm Springs. 

John told him many times that he didn’t chose to be in money and now he doesn’t have it. He wasn’t a complainer, but he would say that he was down in Palm Springs and the guy has always been an asshole, even when he was 24, but why does he have that big house? He has a big house because he is an asshole, it is a 1:1 correlation. John’s dad really believed that the righteous got paid in the end, but he didn’t believe in the kingdom of heaven and wasn’t smugly looking at everybody, saying that he is going to live in paradise while everybody else was going to a place down below. He lived in the moment, that was part of his problem. He never thought about the future because he thought he was going to be 29 forever. 

When John sits down in front of his hot live podcast mic, he feels like he could be twentyseventynine and he is podcasting like a younger person, but when he is out there in the neighborhood with his crossbow, hunting cats, he feels the age. ”When Robin Hood does it, it is for social justice, but when I’m out smiting a dog, suddenly I’m the weirdo. I am just another podcaster in a bathrobe!” John got a wrist rocket and a mason jar full of marbles, trying to get the neighborhood dogs to stop barking, but //he// is the weirdo. 

Like Obama, John is a community activist, but Obama got political and now the vice president is mad. John is chewing on it and he is actually kind of excited to see all the disadvantages that are starting accruing to him because of all the leisure time he has afforded himself. The chickens are going to come home to roost and there is going to be a moment when he will think ”Oh, I should have been working!”, ”I should have been putting some money away!”, ”I should have been doing deep knee-bends”, or ”I should have had my phone set to beep at me to get up and walk around the room!” 

Merlin has an array of things he chooses to have beep at him. His watch tells him 10 minutes to every hour to stand up if he hasn’t done that and he has a lot of days of standing 12 hours a day.





















RL317 - You Can’t be Drunk at Disneyland
2018-12-24

This week, Merlin and John talk about:
* People getting offended by Merry Christmas, reading the room ([[[Personality]]])
* Monty Python ([[[Movies]]])
* Conversations about status ([[[Humanities]]])
* Visiting beautiful gardens as a destination ([[[Travel]]])
* Tripping at Disneyland ([[[Drugs]]])
* Psychedelic drugs ([[[Drugs]]])
* John gaslighting a DM conversation ([[[Technology]]])
* People texting in small chunks ([[[Technology]]])
* John’s daughter gaslighting her mother ([[[Daughter]]])
* John’s first time in Florida ([[[Geography]]])
* Merlin wanted to all his band Free Beer ([[[Music]]])
* Merlin at his first basketball game ([[[Sports]]])
* John riding the Green Tortoise bus ([[[Stories]]])
* Sports in Seattle ([[[Sports]]])
* John wishing to have a sports program where people could yell at him ([[[Sports]]])

**The Problem:** John’s not making any wolf masks, referring to him buying wolf masks on Etsy one time and when a corporate person from Etsy once contacted him and offered to help him if he wanted to be on Etsy, he said he was not making any wolf masks.

The show title refers to the fact that you can’t walk around with a beer in Disneyland and if you would be drunk, they would put you on a cart and down a well within seconds.

It is going great. 

+ People getting offended by Merry Christmas, reading the room (RL317)

Merry Christmas! John asks Merlin how he feels about the whole ”Telling people Happy Holidays!” controversy. Merlin just doesn’t want to offend anybody and you can offend somebody either way. If you are wishing somebody "Happy Holidays" to somebody to be a nice person you have to be contextual about it. There is no really good answer right now. Context Clues is a good answer, it would be a good television show for children, like ”Here is a situation. What do you think about this situation, kids?” 

One time when Merlin was 14 he walked in on his best friend’s grandmother in the bathroom and he still thinks about that. Setting aside that she was sitting on the toilet, she really looked like a 1970s version of an old lady with her hair in a bun and a big long blue dress. She was dropping a duke and Merlin should have read the room, but also: Lock the door! They don’t teach context clues in school, like so many things they don’t teach in school. When Merlin was in school things were pretty often presented as a clear black and white issue without the need for context clues. Just say ”No!” If you are trying to teach a kid not to touch the hot stove, then you are going to want to say ”Don’t touch the hot stove!”

The Holidays / Christmas thing is the liberal arts problem: His whole life Merlin has said ”Merry Christmas!” but then he realized it is problematic because a) there are people who celebrate something other than Christmas and b) there are people who do not celebrate a holiday thing in general. It is like asking people when they are going to have a baby. Just say ”Happy Holidays!” or nothing at all. Is he turning into a Bill O’Reilley guy when he says ”Merry Christmas”? Are people going to assume that he has a mission? Or as Merlin said [https://twitter.com/hotdogsladies/status/6908805273 in a tweet] many years ago: ”Happy Whatever Holiday You’re Weirdly Touchy About”

John will generally ascertain whether the person has a pet, a child or both and he will set about to invalidate either their choices as a pet owner, as a parent or both. People with kids or especially with dogs are probably Christmas people, while people with cats or giraffes are probably ”Happy Holidays” people. Merlin follows [https://twitter.com/juliaioffe Julia Ioffe] on Twitter and likes her a lot. Think of all the dumb things people say to you, like ”When are you going to get engaged? When are you going to get married? When are you going to have a baby?” 

There is so much cultural assumption in all of those things! If you are from some Mormon community with 35 kids, ”When are you going to have lots of kids?” is a very normal question to ask, along the lines of ”How do you take your coffee?”, but outside of that community it is a weird, too unkind, too hurtful thing to say to someone. What if that person doesn’t want kids, or wouldn’t mind kids, but is unable to bear? It is an awful thing to say to somebody and holidays can be little bit like that. It is holiday hegemony! 

When someone says something to you that you get offended by, you also should be using context clues. Is it a little old person? Or is it someone who is just innocently walking through life thinking ”Oh, it is Christmas time!” or 1000 other things before you get offended and before you believe that the world is arrayed against you and everyone is in a giant conspiracy to make you feel bad from moment to moment. 

John doesn’t get offended about things like that. If somebody came up to him and said something, some person in the world trying to be nice, and they missed the mark, like ”Oh, you are pregnant” - ”Nah, I’m fat!” People ask John all the time ”Are you out with your granddaughter?”, like ”Come on, this is our first date and you are salting my game!”, they even bend down to his little girl and ask ”Are you playing with granddad?” A lot of people want to comment on his appearance, at how bedraggled or how old he looks. John had [https://www.instagram.com/johnroderick/p/Br4ZLKZACLU/ dinner with his cousins] last night and a thing went around the three of them like ”Haha, here we all are, we all made it! Everybody is looking good!” and then one of the cousins said ”Well, except you!”

Merlin defaults to ”Happy Holidays!”, based on his own ping pong idea of who the person might be. It turns into a status question very quickly as soon as you ask if you have any plans for the holidays. If you say ”No”, you sound poor. You are supposed to say that you are going skiing for a week or to Disneyworld in Orlando or whatever. The ”check your privilege” thing is getting long in the tooth, but Merlin tries to think about hegemony, about how much culture he brings upon other people in this ”I like to speak to your manager" kind of way and he is trying to be more cognisant of that because he doesn’t want to become like those old guys who just assume that everybody is wrong. 

Merlin is trying to assume that nobody is intentionally being a dick. You take it personal and make it personal. Learning how to read the room is a skill that they should teach in schools. Merlin recently talked to John Siracusa about this. John hasn’t heard from Siracusa in a while, he used to reach out to him and ping him sometimes, but he hasn’t been doing that lately. He does listen to this program still, which is the nicest compliment.

Before you come in with a big pivot to what is happening in a situation, just figure out what has already happened before you got there, because there was a conversation and a mood happening and you should suss that out before you are diving in. There are so many things they should teach in schools, like geography. You should learn the 50 states and capitals, you should learn how many Canadian provinces there are, and you should be able to name all the major rivers in Africa. 

+ Monty Python (RL317)

”I was born in a car by a bus in a corridor” 
”I lick the streets clean with our tongues”
(from [https://www.musixmatch.com/lyrics/Monty-Python/Four-Yorkshiremen Four Yorkshiremen]

Merlin’s daughter sometimes specifically requests the Four Yorkshiremen sketch and they will watch the Hollywood Bowl version from 1981 ([https://www.youtube.com/watch?v=26ZDB9h7BLY this one]), which Merlin thinks of as the classic version. It has been a long time since John watched a Monty Python episode. There are many different versions of it, Eddie Izzard is in one of them, and there is the classic one written by Graham Chapman, and one is with Marty Feldman. Before Monty Python they have done other things like Footlights (Theater) and they were in TV for a while. Dudley Moore and Peter Cook were in the British Office.

+ Conversations about status (RL317)

We bring presumptions to our conversations with people, cultural baggage and/or valiance, things we assume are normal to say. Merlin gets this all the time with the other moms at school. The kind of shit they say to each other leaves him gobsmacked, he returns nil and doesn’t even want to be in on this conversation that from the get-go is so often about status. People size each other up like jungle cats, figuring out how much money they make, what car they drive, all that kind of stuff. Merlin tries to avoid those conversations altogether, they make him very uncomfortable. 

+ Visiting beautiful gardens as a destination (RL317)

Merlin has learned a lot of what he knows about Africa from an attraction at Bush Gardens in Tampa. They had a Lake Victoria and they called Africa the dark continent. It started out as a garden and an Anheuser-Busch brewery with some statuary and some topiaries and they added rides like a great rollercoaster only in the 1970s. It is like the Knott’s Berry Farm: They were a berry farm who made pies and they put rides in there later. When Merlin was in Military School 1979-1980 he had a season pass for the Bush Gardens for $20 a year and it was great. It only took three visits because the regular admission was $9. They went almost every weekend in 1979.

There was time when those gardens were a destination, like ”Let’s go visit the famous gardens!” There is a famous garden outside of Victoria town on Vancouver Island and there is the Victoria Garden in Vancouver, which are completely separate from one another. In John’s neighborhood there is a Kubota Garden which was built by one crazy Japanese guy over the course of his whole life and he eventually gave it to the city. The garden on Vancouver Island seems like it was built in an old rock quarry and has a lot of geography. It is beautiful with many beautiful plants and they bring tour busses and everything. There are no rides and nothing for the kids to do.

There are many things in Florida that started as precisely that and that eventually became destinations/attractions for families. Cyprus Gardens started out as gardens, they added trick water skiing and eventually they added some rides. There was also a lot of confederate propaganda. They have things like Homosassa Springs or Weeki Wachee Spring which was gardens plus mermaids. You would sit in front of a giant theater-sized fish tank and watch ladies eat bananas under water. 

John would watch that right now, but somebody would ask ”Are you here babysitting your grandchildren?” It was like a massive fish-tank or a wall of glass that you could see into. You would sit in theater chairs and ladies dressed as mermaids swam around and did little acrobatic tricks. A tube came down into the water where they could occasionally take a toke of oxygen. Many things started out just as gardens where they added stuff.

+ Tripping at Disneyland (RL317)

They wonder if you can drink beer at Disneyland or the Epcot at Disney World in Florida, but Merlin wants to say ”no” except in some areas. The restaurants sometimes have alcohol, but you can't just walk around with a beer at Disneyland. A drunk person at Disneyland would but put on a cart and down into a well within seconds. You can’t be drunk at Disneyland, but among people in John’s extended circle it used to be very fashionable to take LSD before you go to Disneyland. The saying goes that Disney has people stationed at the gate profiling visitors to see if they are tripping.

When five alternative guys who are giggling really hard about nothing and are looking at their hands come in, somebody might step out from behind a glass wall and say ”Hey, why don’t you guys come over here for a second? Can I ask you a bunch of questions?” and then they pull them over and ask ”Do you feel boxed in and drawn out?” and everybody goes ”Woww!” and the person says ”No! You are not coming in here!”, or ”Do you think at this moment that you might be living in an entire civilization living inside a giant’s fingernail?”, ”Have you ever thought about green? No, really thought about it?”, ”Have you ever looked at your hands? Look at them now! Really look at your hands!” John is not sure if they still do that.

+ Psychedelic drugs (RL317)

The kids today love to psychedelicise. They have Molly, but that is not a good psychedelic. As far as John can tell, MDMA was a mouth full, Ecstasy felt like an old-person drug, and so Molly became the new street name. John can’t think of many instances where a drug has remained constant, but the street name changed. It might be a different formulation, like MDA could have been the first drug and then it was turned into MDMA. John is confused about what exactly went on there, if they just changed the name or if it really was something different, but he had never heard of Molly until very recently, although it probably existed for a long time ([https://www.quora.com/Why-is-MDMA-Ecstasy-called-Molly little more info]). 

There is still no better psychedelic on the market today than good old-fashioned LSD, which is scary and should be scary. It is hard to know exactly what you are getting on any number of levels and if you are not in the right state of mind it can be really not that fun. People routinely lace pot with PCP, which is not at all true and there is no reason anyone would do that. When you take a drug you can never know exactly what is going to happen. With cocaine it ends up being a question about how pure the cocaine is. If you had some pretty good cocaine from one place and you go somewhere else 5 years later and take pretty good cocaine there, it is pretty much going to have the same effect, although you could be a very different person. If you take a drug when you are in a bad mood it becomes a very different drug than if you are in a good mood.

+ John gaslighting a DM conversation (RL317)

John was having a DM conversation with someone within an app, which happens sometimes. Later John went back and reviewed the conversation, not for any real reason, and he selectively deleted one in five remarks, not because it was a bad conversation, but purely as a gaslighting event. John knew that they were going to review that conversation and they were going to wake up and find that it was a different conversation than they remember from last night. John did it for no good reason, just because he could. He just wondered what would happen if he changed that slightly.

+ People texting in small chunks (RL317)

Dan Benjamin is a young person who sits at a computer and when he is texting John he is going to send him 15 small texts rather than one long text, or even one concise text. "Yes" SEND "Let’s" SEND "What if" SEND. Because of situations like this John doesn’t have notifications on his phone and it doesn’t buzz and doesn’t make sounds. He is on some threads with 15 old Rock dudes who will find a picture of Ken Stringfellow every once in a while that they really want to talk about. Merlin tells John that you can turn off notifications for individual conversations, which John didn’t know about. He is going to have a much better 2019!

One of the benefits of John's Apple Watch is that it does a little haptic. Somebody asked him the other day if haptic and taptic are made-up words because nobody ever used to say that. The problem is that John will be driving in the car and a random person whom Merlin and John might have been doing separate podcasts with for a long time will send a lot of messages, John’s watch will freaking buzz off his arm and he will drive off the road. His first instinct is always that there is a fire at his daughter’s school. The ability to go into a conversation and take every fifth reply out is a new level of small gaslighting. You don’t want to gaslight somebody with whom you are genuinely in a relationship where they are going to ask themselves if they are crazy, but gaslighting is a form of friendship. 

+ John’s daughter gaslighting her mother (RL317)

One of the best forms of gaslighting that you can do to somebody is to notice when their lightbulb is burned out and change it (see RW63). It is the opposite of the titular movie. When a lightbulb is burned out, the person who lives there has surely noticed but they just haven’t gotten around to it. If you switch it out they are going to think ”Was it not burned out? Why did the light come back on?” John used to do that all the time. 

There was a light down in a stairwell outside of a girl’s apartment he knew. She was responsible for it and it would frequently burn out and the stairs would be dark.  John would go change the lightbulb and she would ask John if he did it, but he would say ”No!” It wasn’t a thing where it was scary to her. These days the person would think that someone was stalking them, but back then normal people didn’t think they were being stalked. She just couldn’t understand it and John loved that kind of thing.

John’s daughter has been gaslighting her mother recently. Her mother bought a screen for the fireplace that has two handles that are normally in the down-position, but they can be put in the up-position and every time John went over there during the last two weeks her mother asked him if he had moved the fireplace screen and he would say ”No!” She would zoom in because John is a renowned gaslighter, but why would John come over in the middle of the night and move the fireplace screen? 

She was sure that the handles were down and now they are up, but it is easy to get spooked and that is the type of thing a ghost would do, but it is not the type of thing a person would do. She would put the handles down, but two days later the handles would be up again. Neither of them were thinking that there was a 7.5 year old involved and both had no explanation until it dawned on one of them, like ”Marlo! Come here!” You assume she is just a sweet precious angel, but ”Have you been monkeying with the fireplace screen?” and she looked up from her Archie comic book for two seconds and was like ”Why? What’s the big deal? Maybe!” Mystery solved!

Merlin says that if you are going to do a lot of experimenting on your child, you should do it before they are 5 or so because they do a mind-wipe around 5 or 6 where they forget everything they ever knew. One of Merlin’s earliest memories is getting his fingers slammed in a very heavy door outside the mermaid theater at Weeki Wachee, which is why he can’t finish unless there is a mermaid. John always assumed that Merlin’s earliest memories are from Ohio, which is a good catch, but they were visiting Florida at the time. When people who lived in Florida talk about Florida, John never thinks that they visited Florida before they moved to Florida.

+ John’s first time in Florida (RL317)

John had never visited Florida until he was already 35 years old and on tour with Harvey Danger, but it was still a fly-in-fly-out. They did shows in West Palm Beach and in Orlando. At one point they walked out from the hotel and walked to the all-night Walmart because it was the only place that was open. John had also never been to a Walmart before and this one might have even been a Super Walmart. They walked along a road without a sidewalk because it had never occurred to anyone in that region to walk any place. There were giant ditches on either side of the road that you could put a car in, it was covered with some weird seagrass material that was not grass, and John was walking along with Jeff an Aron, who could not stand each other, which made it so fun to hang out with them. 

They could see this Walmart and it seemed like it was close in distance, but it was just huge. It kept getting bigger, but it was still far away, and the whole way John was very vigilant looking out for crocodiles. He was with other people from Washington, and nobody could say whether or not there would be alligators where they were and he still to this day doesn’t know what the criteria are for an alligator to be in a ditch. As they got to the Walmart they walked in the door and it wasn’t just enormous, open at 2am and seemed to stretch on forever, but the amazing thing where Florida planted a flag in John it that it was full of parents with kids.

It was uncommon for anybody in John’s region to vacation in Florida, it just wasn’t going to be your first place to go because California was nearby. People from the Northeast corridor would go to Florida to vacation and a lot of people bought houses there for $15.000-20.000 in the 1940s, 1950s, and 1960s to retire in. You can drive from Ohio to Florida in less time than it would take you to drive from Seattle to Los Angeles. ”You get the bugs off your glass and the bears off your tail... and eleven longhaired friends of Jesus in a chartreuse microbus” (lyrics Convoy by Paul Brandt).

Merlin guessed that a round trip flight from Cleveland to Fort Meyers was  $430, but it is $61, as John found on Google Flights. Spirit Airlines is $103 from Cleveland to Orlando and $99 from Cleveland to Tampa. Merlin read [https://twitter.com/libbycwatson/status/1076993365911187456 an article] by [https://twitter.com/libbycwatson Libby Watson] about Spirit Airlines that put the fear of God in him. They charge you for checked bags and carry-on bags, which is emblematic of a larger nickel-and-diming type situation. 

+ Merlin wanted to all his band Free Beer (RL317)

Once Merlin told his mom he wanted to call his punk rock band Free Beer because when they would put flyers up and people would see ”Free Beer” they would come. She said something very sage: Are you sure that you would want the people who come to see your Rock band if it is called Free Beer? It is like Molly Ringwald’s dad says: If you put out signals, people are going to pick up on them. What Merlin’s mom didn’t know, what the Duke boys didn’t know, was that Merlin did want the people who would come to a concert called Free Beer, of course he did, what a great concert it would be! Be very careful who’s company you want to keep.

Merlin wants to be careful about the groups he chooses to associate with. Rajneesh used to send busses into Oregon cities to attract voters with something like a Free Beer sign. It might have been a Grapes of Rat type situation, like ”We finally have a good job for you!” It was such a good documentary! John remembers this from his own years: If a bud drove up to him on the streets of Portland and offered him free beer, he would take a step on the bus, look it over, and if it was a bunch of guys who like to party, he would be ”Yeah, I can hang with these guys!” 

+ Merlin at his first basketball game (RL317)

Merlin’s family gave him tickets to the basticball game as a great birthday present and last night he went to see the warriors in Oakland which was really fun. It was the first professional NBA game he had ever been to. He has watched basticball 3-4 times on TV now, the team is really good and the pacing is quick. It was amazing and his daughter wasn’t even bored. It was very different than watching a baseball game, which is watching 14-16 very still people. It is a different kind of game that is all about the story. 

Merlin did buy a couple of very tasteful pieces of Warrior merch he had his eye on, like a jersey with a cool Chinese character on it, but he is not going to be a guy who is always wearing Warrior stuff because he doesn’t want people to think he is that guy. There were so many lump-in proletaria who were that guy which is weird considering how costly those god-damn seats were. It was a 3-digit number! Merlin had a good time, it was really fun, and it was a bit more stand-offish than he expected. He greeted the person who sat next to him, he said ”Hello!” and she looked at him like he was a crazy person. She just wanted to get back to her chicken. John thought that sports fans were always delighted to be with each other. 

+ John riding the Green Tortoise bus (RL317)

In 1986 or 1990 John took the Green Tortoise bus many times, a hippie bus line who had bought some old busses, took out about half the seats in the back and put mattresses on the floor. For no amount of money like $50 you could get from Los Angeles to Seattle. You would step on, look around if there was somebody on there you won’t mind sharing a mattress with, and you would see a couple of people you wouldn’t mind curling up with. 

It was fairly underground in that there was some kind of situation where you would be standing somewhere and the Green Tortoise would pull up. There wasn’t a regular stop, but you called a phone number like the Night Bus in Harry Potter. You would throw on your three paper sacks or however you were carrying your belongings, you would get on and ”Hello! How is it going?” and the next person would not just turn away. You were all just on a long bus ride together. 

At least a couple of times John woke up in the middle of the night to the sound of the road and he would be spooning a girl he had just met and there would be a girl spooning him. They had all just met and they were all spooning, it was so buggy! John had rented an apartment and one time there were a couple of British girls who didn’t know what to do and what was going on when they got to Seattle, so John invited them to stay with him and they were ”Oh, that’s wonderful! We were hoping you would say that!” One of them ended up taking a pair of John's overalls that fit her well enough. It was cute! It was very Green Tortoise, it was a different time! She looked like a Dexy’s Midnight Runner, she tied a little handkerchief in her hair, somebody started playing violin music and danced her little jig. Those were the "Everybody is Friends" days.

+ Sports in Seattle (RL317)

Back in the 1990s when the Seattle Supersonics were super everybody loved them although they never won anything since 1979 except for some intermediary things, like getting a white ribbon for going to the final four. They had Shawn Kemp and Seattle was famous for basketball. During the Grunge era Perl Jam, the Presidents of the United States of America and some of the other Grungers were really into the Sonics. It is a cool name, it is the name of a cool Northwestern band, and it comes from Boeing because Seattle was sonical.

There are about 15 radio stations in Seattle that just have people arguing about sports. Jason Finn likes to yell about sports and John told him many times that he not only has a great face for radio, but also a voice for radio and for the last 5 minutes while he is still a famous person, why don’t you go down to the radio station and say ”I’m Jason Finn and I want a radio show about sports!” and he said ”There are 50.000 people in Seattle who know more about sports and are more excited to yell about it than me!”

John can’t believe that to be true because he doesn’t know any people from that part of the world, but Jason said that he would be out-matched. If the phone rang and somebody would ask John if he would do a show about sports, he would say ”Sure!” Part of John’s CV is that he walks into every room like he belongs there and he is prepared to address every topic that is brought to him. He would host a sports radio program in a second, although he wouldn’t know if it would be a quality sports radio program.

+ John wishing to have a sports program where people could yell at him (RL317)

John needs a place where he can be yelled at and is fine with it. There are a lot of places on the Internet where people occasionally yell at him and he is not fine with it because they yell about things that John thinks he knows more about than they do. It affects John and he can not just walk away and  ends up having to tweet ”Fuck you, loser!” at them, which he has decided is his new response right before he mutes somebody. 

His last tweet to them is ”Ha! Fuck you, loser!” and then he mutes them so they can’t reply. The whole point of having a sports program would be so people could scream at him and he wouldn’t care and that would feel really freeing because they would be yelling at him about a thing that they knew way more about than he does and they would be rightfully mad that he was the one hosting a sports show. It would be really healthy for John. 

John knows a lot of generally happy people and when strangers yell at them they are somehow able to identify those people as strangers who don’t matter and their yelling has no consequences because they are using angry language and they are not interested. John takes it all in, he takes it very personally, he sits and grinds on it, and his only solution is to not be on the Internet because it is too bad and it is too much. Little by little he creeps back on and pretty soon he tweets ”Ha! Fuck you, loser!” SEND, MUTE which is bad and he should just not be on the Internet.

Instead he should go over to the sports Internet and be like ”Hahaha, how many touch downs in basketball?”, things that non-sports people think is funny and the sports people would go ”That was not even fucking funny 25 years ago when we first heard it!” They love to yell and John would be over there taking the heat which would be good for him, like existential shit-posting. John doesn’t want to go over there just to antagonize them.

John doesn’t think crafts people yell as much as sports people. He went on Etsy a few times and discovered that you can make a living making wallets over there. Then he always forgets Etsy exists for a while. At one point he bought a couple of wolf masks from Etsy which was a positive experience. One time Someone from Etsy corporate who listens to this show sent him an email and said ”I’m vice president of yarnballs over here at Etsy, if you ever want to do something on Etsy, just let me know!” and John was like ”I’m not making any wolf masks” but she said ”They don’t have to be wolf masks” and John probably didn’t reply to the last email, although it was a wonderful exchange.

If John was on sports and people would yell at him they would also feel better because they know more about it than John and a big part of being a sports fan is knowing more about it than other people, probably even more so than in politics. Your whole interaction with other people is to say ”You don’t know what you are talking about!” Merlin says it is the same thing with comic book guy, camera store guy, and record store guy. John wonders if comic book guys really are like this, but Merlin is pretty sure. Going to the camera store back in the day was always a pretty hostile experience. They talk so far down to you that you feel like a little worm.

In the early days of Indie Rock almost all the Indie Rock photographer John knew were ladies, like Autumn de Wilde, Laura Musselman, or Elizabeth Weinberg. They were all friends and they would recount stories of going to the camera store. It was like ”Children pull up a chair or sit around the campfire while I tell the story of how I was treated at the camera store” They were a) young and b) girls and they wanted to buy a thing, whatever it was, and the dripping condescension from the men behind the counter could fill volumes. The had lots of laughs! 

John doesn’t understand how that could translate to comic books. Isn’t it all right there on the page, or is it like ”You don’t know what Wilberforce’s inner motivations are?” Merlin doesn’t know Wilberforce as much as John knows Wilberforce, but Merlin refuses to get into it.

John wouldn’t care and he would be gaslighting those people. He would be fighting them because that is what they are looking for, but he wouldn’t have any skin in the game and whether he won or lost wouldn’t matter to him. It is different if somebody wants to yell at him about something he thinks he knows more about, like the Habsburgs, but while doing his war movie podcast ([[[Friendly Fire]]]) he discovered that he doesn’t know half as much about the Habsburgs as the craziest Habsburg fanboy.

Merlin thinks that John could take all of that advice and all of those angles and come up with something very interesting. It would be good practice for him, like a virtual reality Twitter where he wouldn’t feel the pain in the same way than he does on real Twitter. He could be over on Sports Twitter, but John doesn’t want to get yelled at on Sports Twitter or on real Twitter. 

The question is how invested he is in being correct. Often times he knows he is correct and he is trying to help and responds because he wants the other person to get better and make the world a better place. He wants the other person to stop being wrong because the problems of the world would diminish if there was one less person who is wrong. It is very troubling, but how can he stop to go back to ”Well, the world is all fucked up and I can’t do anything about it!”

Merlin says that there was a time when, if he would have answered all the emails the way that it asked to be answered, he would never do another thing his entire life. Neil Gaiman for example said that he could either answer your emails or make novels, which was a very interesting way to look at it. You have to start being selective where you put your attention and effort on these kinds of things because you can’t afford having 10.000 conversations in 10.000 different closets for the rest of your life and you can’t get pulled in with this other person who has a lot more time and interest than you do. The same is true in public. 

If a bunch of people ask Merlin the same question, he will do an occasional quote tweet with his answer in it, which is usually a little bit of a dick move, but that way they can see where the question came from. There are two basic ways to respond to someone on Twitter: One is the at-button, but if another person has the same question, they are very unlikely to have seen his response because they probably don’t follow the first person. Merlin will therefore quote the tweet and write his answer above it, so you are basically kind of addressing that person, but it is a little bit of a stage whisper to everybody who follows you, rather than actually responding to the person. The benefit is that more people are likely to see it. 

If Merlin would do that one email at a time, he would lose his fucking mind. What do you do? Write some FAQs and point people there? John says that Merlin is somebody to whom people turn for information. No matter who asks John a question there is never someone else who has the same question. The questions usually start off like ”I have been seeing my girlfriend for 8 years and...” or they want to reference some political moment or they are replying to a tweet that John put out there as a humor tweet but they are replying angrily because John is an idiot. Merlin is somebody who has previously answered the same question 40 times and here is the same answer, I’m just going to pin this to the top of my thing. John is not a font of information, but it is something else. 

Merlin says that when John gets involved in an argument on Twitter it mainly takes his increasingly brighter beam and points it back at that person, which is a way to call people out and make them look like a dick. Merlin tries not to do that too often. It also amplifies the fact that you are arguing with somebody you don’t know. You point your very powerful light at this 15-follower account and now people just notice you as a guy who argues. This is why Merlin says that you have to deploy that with some care. If somebody is just throwing tomatoes, they are entitled to yell at you or whatever. The worst thing for Merlin would be to engage in that. It is the worst thing for John, too. It gets Merlin all wound up and because he has to cook his spatchcocked chicken he can’t keep checking in with this guy to see if he got dunked on.

Merlin used the phrase ”dunked on” that John had never heard in this context before his cohosts on Friendly Fire used it all the time. He finds it a good way of describing being dunked on. This turn of phrase appeared out of nowhere, like ”Right on” or ”Totally”. John identified it as something that was coming from these youths, but after hearing Merlin say it he thinks that it is what we say now. Merlin thinks it might be up there with ”trolling”. You are trolling people and then you dunk on them, like a fishing metaphor with a basketball metaphor. If you think about it, it is a lot like The Fish That Saved Pittsburgh (movie) starring Gabe Kaplan. 

Merry Christmas!














OM187 - MSG
2019-09-10

This week, Ken and John talk about: 

+ John’s and Ken’s Thai Food choices (OM187)

Last night Ken had Thai food for dinner and it was delicious. Seattle has some of the best Thai food in the Americas and Ken is lucky to live on the same block as the best Thai place in town, but he doesn’t want to start new fights. Not the same block, but just across Greenwood, it is not far.

John likes to order different things and he doesn’t always get the same thing at a restaurant. He likes a Basil Chicken and he tends to order Pad Thai for the table because it is rare that you want to eat an entire plate of Pad Thai, but everybody wants some Pad Thai and that way it is like an extra entrée. Ken does the same wherever he goes, even at IHOP, but IHOP’s Pad Thai isn’t that good. 

John likes Panang Beef because he is a curry lover. Ken almost always gets either that or a Massaman Curry, which is the coconutty one, but that has potatoes in it and John doesn’t eat potatoes. Ken mentioned a potato-adjacent curry and John’s face just clouded. Also, if John doesn’t feel like getting a noodle for the table, he will get some Swimming Rama for the table, but he found that not everybody wants that. It is peanut sauce over a bed of slightly blanched spinach cooked by the hot peanut sauce with a protein of your choice.

The genius of Thailand is that pretty much everything tastes better with peanut better on it. They have been eating Chicken Skrewers for years in America, but they never put peanut butter on it before.

If you were confined to one nationality’s food for the rest of your life, what would you choose? They have talked about this once at a Taco truck and Ken said Mexican and that is still true, just for diversity and re-eatability, but Thai food would be close.

All national foods seem defined by their constraints. With Mexican food you are talking about beans, corn, cheese, meat, spice and then maybe a little garnish or salsa. Some cuisines are more constrained than others. If you go with Hungarian food you will just eat dumplings for the rest of your life, and John could. You also have goulash. Japanese food is divided into three categories: Soups, sushi, and the burned meat group. Then there is that fourth group of super-weird bean-flavored dishes.

Thai food, like Vietnamese food, has a very diverse menu. They got all the tastes and ideally sweet and spicy and sour have a party in your moth! Fresh vegetables are a major component. Ken had a good Som Tam last night, a Green Papaya Salad, which was very good on a warm night on the patio. They also got a Larb Thai with Chicken.

+ Thai food coming to America (OM187)

John remembers Thai food catching on in America. He was a fully grown adult before he had Thai food for the first time. In the 1950s and 1960s Americans started to come back from Southeast Asia, not necessarily war-related, although some were military, but also peace corps and Fulbright scholars and people studying overseas.

They had this amazing food that they couldn’t get in America. There were Asian groceries, but there was literally no place in Los Angeles where you could get fish sauce. It would have been called Siamese food, two chicken drumsticks coming out of the safe, served by a bald white guy with eye-makeup on to make him look Asian.

Ken was picturing Billy Corgan, but it is Yul Brynner. Who would win in a fight? Billy Corgan or Yul Brynner? That is not even close because Yul Brynner has been dead of lung cancer for 30 years and he would still beat Billy Corgan. Billy Corgan has a lot of rage and he is surprisingly tall, but you could just insult one of his records and he would collapse.

There is a [https://www.thepoke.co.uk/2019/04/03/viral-14-second-clip-rollercoaster-wild-ride/ glom-looking picture of him] on a roller coaster where he is on an amusement park ride and he is not having a good time because he is Billy Corgan. He was having an affair with Courtney Love, which is posited as one of the reasons Kurt Cobain became inconsolable.

Thai students came over to study in the US for the first time and they were annoyed that they could buy soy sauce, but not fish sauce, and where was Thai Basil and Pandanan and Kaffir Lime Leaves and Lemon Grass? Finally in the 1970s Bankok Grocery opened in Los Angeles and that lead to a spade of immigrant families opening restaurants around town.

Because they were all around West Hollywood, the hip people discovered them and this is the new thing that you want to be the first to discover. David Geffen started taking people to lunch at a Thai place near his office, and of course he is going to lunch with Madonna or whoever, so every celebrity in America got David Geffen to say that it is not like Chinese food at all, but it had peanuts.

This boom was localized to big cities where a Thai family wanted to start a place, but in the late 1990s and the 2000s the Thai government decided to increase the visibility of their country and tourism and they poured money into exporting chefs and cuisine and standardized recipes throughout the world. They were running a very successful disinformation campaign with Siamese bots making us order Pad See Ew. Is it like Psyops, except the ”psy” is ”siamese”.

+ John’s first Thai restaurant (OM187)

The first Thai restaurant John ever visited was in Anchorage. His sister actually knew the younger son of the Thai family that had immigrated and it was a fantastic, beautifully built out place. This was in the early-to-mid 1990s and it was ahead of the Thai curve, but Anchorage is a polyglot city on the Pacific rim that was the first stop for a lot of people and had a massive Korean population. A lot of trans-Pacific flights had stop-overs in Anchorage and they were early adopters.

Going into that place was like when Croissants and Bagels had arrived in Anchorage. It had been the only place in Anchorage where you could get bagels and there was a line out the door of all the people with Greenpeace stickers on the back of their Subarus. There is a cultural elite in Alaska!This Thai food place was a revolution food-wise. You never really had the concepts of the sour of lime plus the hot peppers plus the sweetness of peanuts together before. There was a lot going on!

America developed a very American style of international food, like Tex-Mex food. There is the story of that one Teriyaki-place in the U-district (University District) had introduced Teriyaki to Seattle and those places proliferated throughout town until it became a kind of fast food and was a very sugary burned chicken that developed its very own regional style of American Teriyaki that would be unrecognizable to somebody from Japan.

Have you ever taken fish sauce straight? You are putting this stuff in your food and John took a spoon full of fish sauce one time, but he wouldn’t recommend it.

+ Umami (OM187)

Asians had been eating seaweed for a long time, like in Japanese Nori. There is a very distinctive taste in it and in 1907 Japanese food scientist Kikunae Ikeda coined the word Umami for it, which is Japanese for savoriness or deliciousness. Ken didn’t know about it until the early 2000s, he is almost a century late to the Umami party! John heard about it during the 1990s when it was debated whether or not this was its own taste or whether it was just a combination of other tastes.

John has a geographic tongue that responds pretty wildly to different combinations of those original four tastes like acidic food or certain salts. The sides of it will swell up, the tip will become very sensitive, painfully so, and the center ridges will swell obnoxiously. Last night he had French Fries at Wild Waves (Theme & Water Park) with some seasoned salt that made his tongue go bananas. He couldn’t eat them, but the doesn’t like potatoes anyway. First of all: Don’t eat food at Wild Waves. John even got the Nachos with the pump chili.

+ John’s girlfriend Meagan studying Chinese (OM187)

John’s 1990s lady friend Meagan had studied Chinese at the University of Washington, primarily because she had gotten a full right to Smith when she was in High School, but her parents were blue-collared people and were suspicious of the woman who was running the Smith initiation event.

She had gotten into Smith, they went to the initiation, and the woman running it was very Smith. She had a string of pearls, she talked like the mayor’s wife in The Music Man, she was elegant and smart and said: ”Welcome to your new life far far away from your parents who are from the CW (? inaudible) part of Tacoma!” and her parents said: ”Absolutely not!” They wouldn’t let her go even though she had a scholarship.

As a result, Meagan got a revenge on her parents by studying Chinese, a language she had no real interest in and she had no intention to use, but she did it as a bit. She was going to study the hardest thing and then she was not going to employ that knowledge at all.

During that period she went to China as a vegetarian and she would show up at events where people were hosting her specifically, pulled out all the stops for their foreign visitor, and offered her all these meat dishes. She wanted to eat what they ate, but they said that was just boiled vegetables on rice and they had all this pork for her because boiled vegetables on rice was too humble for their American guest //(see RL217)//.

+ Ken’s wife having Chinese food not before the late 1990s (OM187)

Ken’s parents both have a Chinese major from the University of Washington, although before Meagan’s time. They don’t speak it that great anymore. Ken’s son has French as his second language, but even when they were in Paris and told him to order food he wouldn’t do it because he thought that his French was not formidable. Chinese was not an option.

It is funny to think of a time when Chinese food was very suspect. Ken’s wife had never tried Chinese food until she moved to Hong Kong in the late 1990s because in her dad’s family their grandpa was against Chinese cuisine and all the ”slick foods”, picturing the glistening sweet and sour sauce with the vegetables over the rice. Chinese food was the first time that mid-century Americans would go to a restaurant and just eat a bunch of rice and vegetables. There was not a big slab of meat, which did not accord with their idea of a substantial meal.

+ John’s dad having a lot of Japanese friends (OM187)

John’s dad grew up in Seattle in the 1930s and he had a lot of Japanese friends because in his retelling they were the best basketball players in Seattle. He was a very early adopter of Asian food and he loved Japanese and Chinese food, although he didn’t eat fish. John was exposed to a lot of  glistening red and yellow Chinese food which felt extremely sophisticated. The trappings of the restaurant, the different music, there might be Koi, even though the waitresses were often older white ladies.

In the 1980s John’s sister went across the country and a couple of times she was dining out in Ohio at a Chinese restaurant with local Ohioans and she employed chopsticks to the amazement of everyone in the restaurant. Here was this white girl who could use chopsticks! The jukebox stopped, and this was as late as 1989. There was a huge influx of new foods and new tastes. All of it became available all at once!

+ John’s reaction to MSG (OM187)

John does feel the effects of MSG //(see RL182)//. He cannot be dislodged from this opinion and he cannot be convinced that he doesn’t because he had all kinds of instances of blind taste tests across the whole spectrum of foods. He forgets about MSG, he eats Asian food all the time, and he is not crazy about it, but he looks for it in ingredients lists. If there is a dosage of it sometimes when he walks out of the restaurant he feels a sickliness, or a dizziness, a taste in his mouth, and physical symptoms that are the same each time and that often take him unawares. It is not just Sichuan Peppers or some other thing, it happens often enough.

The FDA says that MSG is safe, but who knows? Maybe in some amount it is not? The idea that these two very building blocks of every lifecycle in your body are somehow causing you to have your left arm go numb, your head shake, your fingers twitch, and your feet itch seems unlikely.

John counters that the same is true of salt and lithium. Many things are just simple salts that have tremendous effects on the mind and body. His bipolar medicine (Lamictal) that has transformed his life is described as a salt that already exists in your body and you just need a bit of a booster. He hasn’t yet snorted Lamictal or MSG. That would be the ultimate test. If you put your bipolar stuff in the Pad Thai, don’t order one for the table!

John is very pro-science and the first thing Ken did when they met was put John in a white lab coat //(see ad video to [https://www.youtube.com/watch?v=DcHViU5JwDs Because I said so!])//












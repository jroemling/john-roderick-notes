RL343 - My German Underpants
2019-07-15

This week, Merlin and John talk about: 

**The Problem:** John was at the bottom of his learning curve, referring to John dating a cam girl and not knowing anything about cam girls when she told him what she did.

The show title refers to the underwear John bought in Germany when he was walking across Europe that was different from the typical American men’s underwear.

They start the show making funny voices like in RL340. Merlin thinks they should talk about their lives, but both of them are not very enthusiastic about that proposition. 

At one point while John was talking about dating a cam girl Merlin rang the bell, but John kept going. Merlin has been so brave this episode!

+ Belle Delphine selling her bathwater (RL343)

People are talking about Merlin on the Internet because he recently talked about Belle Delphine, the bathwater girl as though it was a thing that he didn’t want to talk about //(see [http://dobyfriday.com/140 Do By Friday episode 140], [https://www.rollingstone.com/culture/culture-features/belle-delphine-bathwater-gamer-girl-troll-857192/ more info here])//.

John should also sell his bathwater because he likes taking baths and he thought about it as soon as he saw her pioneering the way. She knows how to make an anime horny face! Merlin refuses to look any of this up, but he was compelled to see her. On his other program (Do By Friday) somebody made him watch her eat an egg and he was doing everything in his power to un-memorize what he saw because it is way out of his wheelhouse.

In traveling the Internet and beyond John is interested in finding where the edge is and what is happening in the corners, and if he sees something move up in the rafters he needs to interrogate it and it is fallacious to think it is just a horny thing, but Merlin thinks certain people are not comfortable admitting that it goes beyond academic research. This person on the Internet makes an anime horny happy face, posposedly [sic] you could buy her bath water, and she ate an egg that dribbled out of her mouth in anime style. John thinks that she is a canny business person who is actually very smart and funny.

John was not aware of her until the recent controversy swept across the world. John really likes when something happens and every single person has a take, and there was just every take: This is the world gone mad, this is disgusting, this is empowering, this is disempowering because it masquerades as empowering, she is supposedly a sex worker, then the sex workers came out and said she is not actually a sex worker.

Merlin doesn’t want to work [[[ping pong]]], but this sounds like a Pachinko where the ball has gone bounce and even when it gets to the bottom it has the dead cat bounce and somebody is going to say: "Why did you even say Pachinko?" Lots of people say that she is coopting Japanese culture and therefore she is racist, but there are also Japanese people who say that she is amazing at this and this is exactly what they love. Other Japanese people say that this is exactly what is wrong about it.

There is every single take and John thinks they are all bad takes and she is hilarious and she knows exactly what she is doing! She is legitimately cute and she puts on a lot of cute make-up and cute costumes, but because of the fact that she is cute fully 70% of the world are not going to take her seriously no matter what she is doing. She is cute, how can she be smart and in on the joke?

There is a lot of offence by proxy where people choose to take offense on somebody else’s behalf in a paternalistic way: They put down the visor on their helmet and go into Internet battle as someone’s champion even if they super didn’t ask for it. The white knights! Very white, caucasian white!Watching her eat an egg like she had never seen an egg before is fucking great! ”I just fell from the sky, I am so new that I don’t know...” 

John instantly recognized her knowingness because he once had a lady friend who was a cam girl and she was also very smart and was putting on a kind of culpability, a kind of dumbness, to put other people at ease. It is titillating, but it is also putting you at ease. She was not putting a bunch of (Rainer Maria) Rilke on you, although she is a fan of him.

Belle Delphine is very young, she is super-aware, and she is at the beginning of a brilliant career. John supports her 100%! He doesn’t want to go on her feed or buy her bathwater because it is not for him and it is important in life to know when something is not for you! John just gave a hot take on the thing that everybody has a hot take on.

John is a student of life because you can’t CLEP out of life! In cases where he doesn’t even know what is going on his reaction will never be to watch something for one second and go: ”What the fuck is this? This is wrong!” It has become so difficult not to do that because the expectation at this point is that you have a strong opinion about everything very quickly, but John doesn’t and Merlin doesn’t want to be like that either! Nobody hit the intercom and asked you how this should be. Nobody did that? Merlin just wants people to be happy!

+ John dating an Internet cam girl (RL343)

Over the course of many years John dated an Internet sex worker. It was a lovely relationship that was like a lot of John’s relationships: It was on and off for a while, they were good friends, John likes her a lot, but they didn’t move in together. She is John’s age and it was a relationship in the style of the time that was fairly European, like meeting in the South of France every year.  She discovered that she could go live on cam, people would get on her portal and she would keep them company for hours at a time.

There are hundreds of presumably men in this chatroom space and she is keeping them company, talking to them and interacting with them. They are tipping her with coins while she is there with a feather duster and a little apron with a little undergarment cleaning her apartment. You would be writing: ”You missed a spot up there” - ”Oh, you mean over here?” She described it as a specialty video because there is a lot out there to achieve the more quotidian finish, but then there are specialty things if you want to watch a lady fill out a form in her bare feet.

She is attractive and petite and shapely and she is very into costuming. She is from the same era as John where there wasn’t a word like ”cosplay”, but you went to thrift stores and bought old dresses and old crinoline and you put it on with a bow in your hair and went swing dancing. She would have undergarments, stockings and garters like Betty Page. 

People come to watch her, among them bonafide Rock stars, a couple of which John and Merlin would know by name, like when John saw his friend walking around int he hand-job district //(see RL8)//. John was super-surprised to learn that certain people were her fans. He wasn’t exactly on the other side of the camera, but he was privy to the people who were talking to her because many fans of cam girls are not anonymous.

Merlin says that one of the positive things that came out of the Internet is that you can find people who have the same interest. That might be Pearl Jam bootlegs or in the mall where his Apple Store is there is a place that will sell Japanese pancakes and there will be a line of chairs with people waiting for hours for those Japanese pancakes that they make three times a day until they sell out. The moment you find out that something is a thing you also find out that it is more popular than you could possibly imagine.

Merlin’s wife thinks that the Japanese pancake place is a thing because Millennials like waiting in line for experiences. Merlin will find out that a YouTuber couple is breaking up and it is a really big deal and there are hundreds of black lady reaction GIFs about it. Marina Abramović does it and it is art. She pulls out her feather duster and does some Internet house cleaning and everybody gets their Victoria’s Secret in a wad (?). 

These cam girls are major places on the Internet and John learned all this through his friend describing the ecosystem to him. A lot of girls on these cam sites are 18 or 19 years old and they are on there just doing whatever they can, often very porny things. Guys will ask them to stick a banana in her ear and they are doing it. They call it fruiting.

The dark site of it is that in places like Romania or Ukraine girls are being pressed into service and being held in a room, but for a lot of people it is a thing you can do from home and you capitalize on the fact that you are attractive and you are personable and you can sit on cam. You are in the safety of your own home, doing sex work, but a kind where you are not putting yourself at risk and you are giving a lot of people companionship. John or Merlin don’t want that, they want the opposite of companionship!

The idea of going on his computer and pay someone to hang out with him is very alien to Merlin, but he is abnormal relative to most people who are just sitting around and want somebody to hang out with. They are going to hang out with this pretty girl, she is going to show them her panties, every once in a while she is going to take a bath and they come along, basically sitting on the toilet seat while she is taking a bath, and that is just the way they want to spend their afternoon. 

John and Merlin have been to a strip club with their mutual friends //(see RL44)// and they have been sitting in the corner, desperately wanting to be invisible, while their very close friends were having the time of their lives. The best part was that they were asking $20 each to get in and Merlin and John looked at each other: ”So much for that idea!”, but they turned just in time to see that their friends had already slapped the $20 down and were going through the door into the dark space. $20, are you serious? They went to a booth, a banquet, and Merlin and John had to go there, too.

Merlin wanted to be a team player, he didn’t want to be the ”Ehhh” guy, but once inside he was extremely uncomfortable and wanted to be far away. There was no good place to sit that wasn’t near the banquettes or the dais. Maybe Merlin needs to explore his sexuality, maybe he is being a prude, but John doesn’t think so, it is just not what he wants. John has been to a lot of strip clubs and he is a very sex-positive person, but it is just not what he wants either.

When John’s friend was in her mid-40s she was often in the Top 10 of cam girls and she was making a very good living, enough to buy a nice separate condo away from her house for her cam studio to do her shows in Seattle. One time she rented an office in Seattle’s tallest office building Downtown with glass walls where people could see in while they were walking down the common hallway. She sat at a desk in an extremely cute little secretary pinstripe suit and she had little go-pro cams over here, under the desk and so on and she would sit as though working, just sitting there doing a Skype call for her work, and she was switching between those cameras so you could see her pantyhose and look up her skirt and on another camera you could see real-life people who were actually at work walking by in the hall.

When John was first learning this on the bottom of his learning curve and was doing the thing he likes to do, which is asking: "If X then what?" she was patiently explaining her side career to him. She was a Roderick on the Line fan and John is like Studs Terkel, he just wants to talk to people what they do all day, he wants to join the football team, he wants to go a couple of rounds. Like (George) Plimpton he wants to be in television, in ads, he wants to start a literary magazine (The Paris Review) that never makes a penny, and he was a Paper Lion //(see [https://www.vintagedetroit.com/blog/2009/08/04/the-fascinating-story-of-george-plimpton%e2%80%99s-famous-book-%e2%80%9cpaper-lion%e2%80%9d/ here], Merlin says Paper Tiger)//

One time John’s friend had little bit of a problem when another girl came in and took a name that was uncomfortably close to her name because she was so successful. She was on ”MFC”, which is ”My Free Cams” and she was routinely one of the Top 10 girls on there. Some young girl came and basically took her name except with an underscore in the middle, which was super-tacky.

Internet sex work is a separate culture. You obviously don't having physical contact with people, which would be much riskier and caters to a different clientele. Merlin likes the Sex Diary series of The Cut and he mentions [https://www.thecut.com/2017/09/sex-diaries-the-stay-at-home-mom-turned-foot-fetish-model.html an article] about a stay-at-home mom who turned into a foot fetish model. They describe a few days in the life of what she does and it seems practically wholesome.

The revenue stream strikes Merlin as if you are out there and you get the hustle on, but you don’t want to have just one thing. You want to get ads plus the Amazon affiliate links plus sponsored content. In this instance you don’t have to be a full-time person in a hotel room, but it would be a thing you could chose to do within limits that are mutually satisfying. You could be caming 60% of the time, you take some time off, go to Barbados to get a tan, and once a week you get the key card.

John's friend had the whole nine: She had the Amazon thing, she was connected all across the different income opportunities and for instance John would stop by and she would ask: ”Do you want any chocolate?” He hates to give this away because there might be a lot of listeners who unwittingly also were spending time with her and who may have coined her, but her kitchen would be stacked with gifts that her online suitors had sent her, like wine and underwear, the things John doesn’t need. She never had to buy her own underwear, unless it was for comfort, they don’t send some nice Hanes her way, but she did specify what is on her wishlist.

Most of their time together they were in sweat pants and when she knew John was coming over she would take her makeup off and they had a very casual grown-up thing. He would take her candle-sticks, he found funny accoutrements to have in her various rooms, he would populate her space, and she was a vintage shopper too and had closets for days.

For those couple of years it never occurred to John to sit and watch her on TV. She never explained to him where he could find her online and he respected their relationship as being an IRL one and he never asked because it would have felt invasive. He has never even been in the room while she was doing the thing with him out of shot, he never saw her doing it at all. John had been to where she worked many times, but never when she was working. He would text her and she would say: ”I am on cam, I call you later!”, just like you would say: ”I am at work!”

If you are on cam with a girl for 4 hours throwing tips at her, you are not jacking it all four of those hours, but you are also doing other things. You have her on a window on your computer and you are going tickety tackety. There are people who want to jack it for four straight, but if you sit there for too long without tipping a little taximeter might go ”Ding!”

They met on Twitter when Roderick on the Line was maybe a year old and she was an avid listener although she never said that online. She was one of the fun people and Merlin probably knew her. There are multiple cam people listening because John has heard from a handful. John draws so much water and so many communities, it is fucking incredible! First they hang out with people coining them and then they hang out with Merlin and John who don’t allow themselves to be coined. Just buy a T-shirt! Merlin sent John a link to [http://cammingskillz.xyz/ camingskillz.xzy] with a guide on how to cam and how to choose a cam model name. He continues to read a bit from that article.

Merlin also found an article about how to sell used panties online, which is a whole thing, but John is not sure if his friend was in that game. It is a Japanese thing and they even have vending machines where you can buy used panties with a photograph of the woman who has nominally worn them. John wonders if he could have sold his German panties. It sure sounds like it! He does not want to receive used underwear, that is not his thing! He doesn’t want anybody touching his underwear!

They were shooting the dozens on Twitter, playing like they used to do, saying funny things back and forth. Scott Simpson was there and it was so much fun! She lived in California at the time and she moved to Seattle and sent John a DM and said: ”I moved to Seattle, would you get coffee some time?”, but he dropped the ball and didn’t actually want to get together with anybody to get coffee. About a year went by, they were still having fun on the Internet, and as enough of those invites had gone by John felt they should actually get coffee and he was sorry he was so recalcitrant.

They met for coffee and she showed up wearing a froofy poodle skirt, a fun little handbag and a mohair cardigan. The whole thing was just so put-together, like ”Kapaw!”, but it wasn’t Bettie Page. Some people are really into that as a ting, like chronological LARPing. John asked what she did and she said she worked in tech, which of course she did.

Before Merlin started telling people he is a ceramicist //(see RL56 and RL261)// he used to say he does stuff with computers. John does that once in a blue moon as well. At this date, which was just a coffee, she neither revealed that she listened to Roderick on the Line nor did she reveal what she did. First as they started dating she felt she should tell him that she was a listener.

They were seeing each other for a surprisingly long time and she was John’s friend who worked in tech who always had a wonderful outfit of some kind. Only after a fairly long time she told him that she didn’t really work in tech, or maybe just in a kind of tech. It was early enough days that she had to explain to John what it was and how it worked because John's reaction was: ”So you what now?”

Like the girl who was selling her bathwater (Belle Delphine) it was a thing John didn’t know about, but Belle Delphine is within the context of this cam thing and when John saw her deal he immediately recognized it as part of this ecosystem and his friend very definitely played the role of ”Oh, tihi!”, she has a little bit of tihi in her because she is fucking featherdusting in her underpants.

Cleaning with a feather-duster in a little French maid’s outfit appeals across a fairly broad section of the people and there are a lot worse things than being caught with a maid fetish. It might be harder for the poopy pants guy (who came to John's newsstand) to meet the kind of person he would like to meet in life because that is going to come up at some point.

Her sitting in an office and you can see her garter belt while she is typing away is very tame, mainstream, and sweet. She is not expressing nudity to the people looking through the glass, but only the GoPro can see the ZZ Top action under the desk. If you went to that office to see your insurance agent down the hall and you would peer in through the glass and saw this young woman sitting at a desk typing away, your thoughts would be that she was a very well-tailored young lady and even though she was 45 she was wearing youthful tailoring.

You might take a second glance and think that she was extremely comely and a very attractive person, and that she had good posture. Her posture and her manners went along with her whole outfit and she was not just cosplaying. If Merlin dressed up like that and would be hunched over his very loud keyboard, that would not be as cool and he could just as well be wearing a diaper on his head. He really needs a haircut!

Part of why she was attracted to John was that she understood that John would treat her with respect and treat her like a lady. She is not going to want to date one of her customers. Some people in her chatrooms were there every day, people she knew very well, and she went through great length to disguise her actual self and no-one would ever know what her real story was. John doesn’t think he is outing her in a way that anybody could know who he is talking about.

She was extremely proper and that was part of her character. You couldn’t use profanity in her chatroom for very long before she would send you out. She was about white glove treatment! John never knew any of this in real time. He was content to just be in sweat pants with her, and her lounge-wear was impeccable as well, she was not a slob.

Eventually a long time ago they stopped seeing each other because life got in the way. All that was pre-Millennium girlfriend era. Several years went by and because John likes to keep in touch with people they continued to interact. John likes follow-up and to keep people in his life. Even if you had a bad falling-out and a big fight, if you loved them at some point, how could you really ever stop loving them?

The problem is that most people don’t agree with John and say: ”It is not that I stopped loving you, I still love you! It is just that I hate you more!” It is also easy to keep loving the idea of someone while to love them actively is a lot of work. John loves Merlin, but not the idea of him, while Merlin loves the idea of John, but not John. He doesn’t love John’s underwear!

Several years after they stopped being special friends John said: ”What is this whole cam girl thing anyway?” and he logged into the Internet and did a search. He knew her stage name, but somewhere along the line she also had some other online properties. John Hodgman would call it a platform. John was not prepared to join MyFreeCams and show up in her space, that would be extremely intrusive, but he wondered what was there to find.

John found some videos of her that one of her fans had made with some sort of software to record and post some things that would have been happening live, which is probably bad form and Brady Haran calls that Freebooting. John was able to watch a few of these videos and he could tell from the environment that they were made during the period when they knew one another. It was a super-strange experience, like an ”I gave her that candlestick” type of thing. It was weird because she had maybe somewhat misrepresented a little bit how tame she was.

+ John has never worn any lady undergarment (RL343)

The funny thing is that although tries to be very sex positive, he has never put on a lady undergarment. It seems like something he would have done, even just ”Hahaha! Check it out, I am putting on your lady undergarments, lol!”, but part of the problem is that John might really stretch them out because she is a petite person and he doesn’t want to ruin her underwear. More than a half a dozen of his lady-friends have taken his underpants, which is still better than taking his Filson bags!

When John was [[[The Big Walk |walking across Europe]]] he got German underpants and as came back his girlfriend at the time said: ”These are little underpants” - ”Yes, these are German underpants, they wear these little huggers. When walking long distances boxer shorts are a bad choice for underwear because of bunching and chafing”

John had been to a department store in Germany and asked: ”Haben Sie Undergarment?” - ”Ja!” and they came out with these things. They are like little cotton bikinis and they were very comfortable walking across Europe. When he got back, his lady-friend used them as her period underpants. Merlin is not sex-positive, but he is period-positive and has a healthy attitude about menstruation.

John learned at the time that when you are on your period you are going through a time and you want some comfortable underdrawers. His German underpants were all solid dark colors like maroon or navy blue and they were very good for periods. Over time John’s German walking-underpants, ”Meine Period Unterhosen”, ”Meine Bluthosen”, walked right out the door.

Sometimes he was in a position where he liked wearing those because it was a hot day or because of the chafing, but now they were all gone. John is not going to go down to Nordstrom and ask: ”Do you have any German underpants?”, but it is the closest he came to wearing lady-underpants because they became lady-underpants, which is what was crazy about it: They didn’t start that way.

+ The guy asking for poopy pants magazines at John's newsstand (RL343)

One time a guy came into John’s newsstand and asked for magazines about poopy pants. He had a look on his face like a devilish little boy, but he was 6’2” (188 cm) and 300 pounds (135 kg) and when John looked down he saw that he was wearing diapers under his pants and John said: ”You get out of here, you!” - ”Tihihi!” and he tittered and ran out on his tip-toes. He was a bad boy!

By saying: ”No! You go! You get out of here, you baddy!” John gave him what he was looking for. John does a lot of that as community service for free. Merlin wonders that if the guy had a diaper on, maybe he made while he was there, but John thinks that when he yelled at him his sphincter tightened up and a lot of it is about taking that experience home with you and then you sit with it.

He probably lived in some Section 8 housing up the street. Section 8? That is what (Corporal Maxwell Q.) Klinger //(from MASH)// wanted? //(Section 8 (housing) vs Section 8 (military)!)// and so he dressed up in lady’s vintage clothes! It is all coming together! That is how John got his braille Playboys, same thing, except he was mad a John, but John was mad at him.

If you are a lifestyle diaper man there is going to be challenges: You don’t just buy one diaper! Merlin had a baby and they went through a shit-ton of poopy pants, they had a special device called a Diaper Genie, like a trash can with a lid, but inside there is a long snake-like thing of Saran Wrap and you put the diaper in there and when it gets full you cut off the bag a little bit and take that out to the trash.

+ "Nice guy, never married": Finding your boundaries (RL343)

John has graduated to confirmed bachelor: ”Nice guy, never married!”, which used to be a euphemism for gay. You say that about a Paul Lynde and you try to be subtle about it, but now John falls into that category. As time has gone on he had to learn what boundaries in relationships were, but sometimes he still crosses them in the sense that he likes somebody and then the question is: "How are we going to do this? Where are you going?" and he has to check it and has to learn.

John had some very strong lady-friends who said they needed boundaries and he needs them himself as well, but boundaries are arguable one of the most important and difficult things to discuss. They can be very trivial, but also very important. They can seem weird, but sometimes Merlin has to tell his family that although he knows it sounds abnormal, if he has been recording and editing a show while paying 100% attention for a while, he needs to just chill a little bit, which is not a very cool thing to say.

There are guys who are working on beams with their lunch pal all day who are really working for a living and then they go home and get on cam with their cam girl. Merlin thinks that if one is interested in a partner and is considering a long-term relationship it is not a terrible idea to live together first. He can’t imagine an era where you date somebody for 8 month to a year, get married, and the first time you ever have carnal knowledge and cap-on-the-toothpaste knowledge is the first night you are in a room together. That seems so risky!

We are also living in an era now where a lot of John’s lady-friends are nice girls never married who are also learning what their boundaries are and who are trying to practice that this is what they are cool with and this is what they are not. John doesn’t have a 9-5 job and he has a lot of time to sit around and get all dramatic if the mood strikes. 

+ The girlfriend who didn’t want anybody to touch her feet (RL343)

John is not one of those ”Don’t touch my feet” people, but he has known people who are. John dated a gal in the old days who if you touched her feet she would scream, and one of the ways they started dating was when they were sitting in a room together with some other friends and she had her shoes off and John was talking to her and touched her feet, as you do when you are talking to somebody, and she didn’t scream.

John hadn't known about her foot thing, but they sat and talked and John touched her foot and someone else, a Rock musician Merlin knows, was watching and said: ”He is touching your foot and you are screaming! Every time I touched your foot you screamed like you were being murdered!” and she looked down, realized John was touching her foot and she went: ”Whaaaa!” - ”What? What? What? What just happened!” She realized that John had touched her foot and she didn’t mind, so maybe they should take this conversation elsewhere. John had the electric fingers. They never talked about it again.

+ Knowing that certain people listen to the show (RL343)

Merlin is very happily married to a person who super-clearly does not listen to his shows and that is a blessing, it is a Mitzvah! You get what you need! John’s daughter’s mother listens to Friendly Fire religiously, but it is very hard for her to listen to Roderick on the Line and John understands that. Merlin thinks it is hard for a lot of people.

They only thing John really knows about John Siracusa is that he listens to Roderick on the Line and is mad about it, that he likes Ferraris and that he skied. John met Max Temkin in real life several times way long before and he feels like he knows him. He met him on a boat in the early days and Max also listens to the program. Merlin really doesn’t like to think about people listening to the show because it throws him off his game. 

This started as phone calls, it turned into not having phone calls often enough, and Merlin still mostly thinks of it as a phone call and that is good for everybody. Every once in a while John wonders if he did say a bunch of things that other people shouldn’t hear. A couple of times he had written Merlin, wondering if he had grossly insulted a super-good friend of theirs, but Merlin had listened to it and didn’t think so. Also, [[[what is in the show is in the show]]], which is for better and sometimes for worse.

+ Meeting famous people out and about (RL343)

One time Merlin was at a food court Downtown in the Powell Street area to have lunch. He was by himself and literally at the next table 8 feet away was a women dining alone whose media he is very familiar with. Of course your first thought is: ”Hey! It’s you!”, but he is grateful he didn’t yell that in the foot court that day, but he just sat with his brisket and his shame and kept it to himself. 

He just stole a couple of glances and thought: ”Wow, that is so wild!” This person was so much smaller than he expected. If you see Merlin out and about, come and say ”Hi!”, it is cool! It is a God-damn shame that he is no longer recognized by anyone at the Apple Store. The same happened to Hodgman when he got out of the commercial racket and he was bummed!

One time John was standing next to Jimmy Kimmel at a Rock concert. He is between Merlin and John in age and he was funny on Win Ben Stein’s Money. Merlin just thinks of him as being on that terrible trampoline lady show (The Man Show). John never saw the trampoline show or his late-night television show. The other guy, the less funny guy, has a very popular podcast now (The Adam Carolla Show). He is like a human Adidas shower sandal: Never trust a guy wearing Adidas shower sandals, it is up there with puka shell necklace. John used to wear a puka shell necklace, but he never wore feathers in his hair and he never had a roach clip earring either.

John said to Jimmy Kimmel: ”Hey man, what’s up!” and he recognized him enough that he thought they knew each other. He was very easy to talk to and they talked for like 10 minutes before John realized that he didn’t know this guy, but it was just a famous guy! If Merlin bumped into Griffin McElroy somewhere he would also be: ”Hey! How are you doing?” Merlin knows so much about him that he feels like they are extremely good friends, but they are not, although he probably knows who Merlin is.

On MaxFunCon this year somebody walked up to John and said he knew John very well, but he knew they were not friends, he just wanted to say ”Hello!” and ”High Five!” - ”High Five, friend! Thank you for laying that out there!” He made it easy on himself because there is a lot of defensiveness when you are a fan of someone and they don’t know you. There is a tendency that people are on edge already, but he took all that out.

Merlin talks about people who come in and drop some knowledge on you, which is almost hostile and so unnecessary. He also mentioned the meet-up at the comic-book store he used to do. Just a teaspoon of kindness would help so much!


















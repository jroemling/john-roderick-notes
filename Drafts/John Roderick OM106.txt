OM106 - Scrappy Doo

Cartoons mentioned in this episode

* [https://en.wikipedia.org/wiki/Scrappy-Doo Scrappy Doo]
* [https://en.wikipedia.org/wiki/Hanna-Barbera Hanna Barbera Productions]
* [https://en.wikipedia.org/wiki/He-Man He-Man]
* [https://en.wikipedia.org/wiki/The_Pink_Panther The Pink Panther]
* [https://en.wikipedia.org/wiki/Bullwinkle_J._Moose Bullwinkle]
* [https://en.wikipedia.org/wiki/Yogi_Bear Yogi Bear]
* [https://en.wikipedia.org/wiki/Wile_E._Coyote_and_the_Road_Runner Wile E. Coyote and the Road Runner]
* [https://en.wikipedia.org/wiki/Dungeons_%26_Dragons_(TV_series) Dungeons & Dragons Cartoon]
* [https://en.wikipedia.org/wiki/Looney_Tunes Looney Tunes]
* [https://en.wikipedia.org/wiki/Bugs_Bunny Bugs Bunny]
* [https://en.wikipedia.org/wiki/The_Plastic_Man_Comedy/Adventure_Show Plastic Man]
* [https://en.wikipedia.org/wiki/Pound_Puppies_(1986_TV_series) Pound Puppies]
* [https://en.wikipedia.org/wiki/Filmation Filmation Production  Company]
* [https://en.wikipedia.org/wiki/Fat_Albert_and_the_Cosby_Kids Fat Albert]
* [https://en.wikipedia.org/wiki/Peep_and_the_Big_Wide_World Peep and Quack]
* [https://en.wikipedia.org/wiki/Berenstain_Bears Berenstain Bears]
* [https://en.wikipedia.org/wiki/Button_Moon Button Moon]
* [https://en.wikipedia.org/wiki/Thomas_the_Tank_Engine Thomas the Tank Engine]
* [https://en.wikipedia.org/wiki/3-2-1_Contact 3-2-1 Contact]
* [https://en.wikipedia.org/wiki/Fred_Rogers Fred Rogers]
* [https://en.wikipedia.org/wiki/Scooby-Doo Scooby Doo]
* [https://en.wikipedia.org/wiki/SpongeBob_SquarePants SpongeBob SquarePants]
* [https://en.wikipedia.org/wiki/Phineas_and_Ferb Phineas and Ferb]
* [https://en.wikipedia.org/wiki/The_Muppet_Show The Muppet Show]
* [https://en.wikipedia.org/wiki/Sesame_Street Sesame Street]
* [https://en.wikipedia.org/wiki/Bob_Ross Bob Ross]
* [https://en.wikipedia.org/wiki/The_Electric_Company The Electric Company]
* [https://en.wikipedia.org/wiki/Neighborhood_of_Make-Believe Lady Elaine and King Friday]
* [https://en.wikipedia.org/wiki/Superman Superman]
* [https://en.wikipedia.org/wiki/Puppy_Dog_Pals Puppy Pals]
* [https://en.wikipedia.org/wiki/It%27s_Punky_Brewster Punky Brewster]
* [https://en.wikipedia.org/wiki/Playmobil Playmobil]
* [https://en.wikipedia.org/wiki/Lego LEGO]
* [https://en.wikipedia.org/wiki/Archie_Comics Archie Comics]
* [https://en.wikipedia.org/wiki/The_Hound_of_the_Baskervilles The Hound of the Baskervilles]
* [https://en.wikipedia.org/wiki/Strange_Case_of_Dr_Jekyll_and_Mr_Hyde Strange Case of Dr Jekyll and Mr Hyde]
* [https://en.wikipedia.org/wiki/The_Brady_Bunch The Brady Bunch]
* [https://en.wikipedia.org/wiki/Beaver_Cleaver Beaver Cleaver]
* [https://en.wikipedia.org/wiki/I_Love_Lucy I Love Lucy]
* [https://en.wikipedia.org/wiki/The_Cosby_Show The Cosby Show]
* [https://en.wikipedia.org/wiki/Good_Times Good Times]
* [https://en.wikipedia.org/wiki/All_in_the_Family All in the Family]
* [https://en.wikipedia.org/wiki/Maude_(TV_series) Maude]
* [https://en.wikipedia.org/wiki/The_Jeffersons The Jeffersons]
* [https://en.wikipedia.org/wiki/The_Flintstones The Flintstones]
* [https://en.wikipedia.org/wiki/Garfield Garfield]
* [https://en.wikipedia.org/wiki/The_Simpsons The Simpsons]
* [https://en.wikipedia.org/wiki/Snoopy Snoopy]
* [https://en.wikipedia.org/wiki/Bill_the_Cat Bill the Cat]
* [https://en.wikipedia.org/wiki/Happy_Days Happy Days]
* [https://en.wikipedia.org/wiki/Eight_Is_Enough Eight Is Enough]
* [https://en.wikipedia.org/wiki/The_Karate_Kid The Karate Kid]
* [https://en.wikipedia.org/wiki/Growing_Pains Growing Pains]
* [https://en.wikipedia.org/wiki/Full_House Full House]
* [https://en.wikipedia.org/wiki/Family_Matters Family Matters]
* [https://en.wikipedia.org/wiki/Star_Trek:_The_Next_Generation Star Trek: The Next Generation]
* [https://en.wikipedia.org/wiki/The_Itchy_%26_Scratchy_%26_Poochie_Show The Itchy & Scratchy & Poochie Show]
* [https://en.wikipedia.org/wiki/Gilmore_Girls Gilmore Girls]
* [https://en.wikipedia.org/wiki/Downton_Abbey Downton Abbey]
* [https://en.wikipedia.org/wiki/Elmo Elmo]
* [https://en.wikipedia.org/wiki/Sesame_Workshop Children’s Television Workshop]

+ Saturday Morning Cartoons (OM106)

Both Ken and John grew up watching Saturday morning cartoons. To their generation it seemed immutable, but it didn’t exist 10 years before and it didn’t really last very long afterwards. The last Saturday morning programming for kids went away a couple of years ago. Ken was raised in that tiny little interregnum when Saturday morning was the only time when the networks would show dumb programming for children. 

All three networks had Saturday morning cartoons and all the kids woke up early on Saturdays to immediately plant themselves in front of the TV. It was your first day off and you had your whole day ahead of you, but corporate America had figured out a way to get kids to watch toy and breakfast cereal commercials for four hours and then bug your parents wanting the new He-man or the new flavor of Cinnamon Toast crunch.

There was a real line in the sand after 1975 where new stuff made for kids was noticeably worse and started to be made very cheaply. The networks were not wrong to do so because kids don’t care. As a kid you don’t even know what is good or bad. Still, Bugs Bunny spoke to Ken in a way that Plastic Man did not. It didn’t really matter because you would sit through it anyway back when TV-watching was a process of things being on and then you watch them and then they are not on anymore. You would just stick with the channel you are on.

+ John’s daughter watching Mr Rogers (OM106)

John’s daughter is pre-Internet. She watched a lot of what John would consider dumb cartoons when she was little. By ”dumb” John doesn’t mean bad, but he is talking about Peep and Quack, Berenstain Bears, Button Moon and all these things. Once a kid gets on whatever accident in their brain, some musical note or bright-colored train, a thing that catches their eye, they will ride it for years and they will be into something just inexplicably. After his daughter was about six, John was asking her if they were still really watching Peep & Quack and she would say that it was amazing. They have just recently introduced 3-2-1 Contact which she really loves and they have finally transitioned. She always loved Mr Rogers.

Ken’s kids weirdly had a real weakness for gentle old-timey stuff. As late as Junior High his son loved watching Bob Ross or Fred Rogers do things slower and gentler than anything else in their lives. John doesn’t think there is anything better than Fred Rogers at any age. He likes to tune in and have Mr Rogers tell him that he is a good person. There was never a time when there was ever a hot take about Fred Rogers that turned out he was anything other than what he was.

Lady Elaine was a bit in Fred Rogers and there was always a Lady Elaine in real life, an abrasive woman who had some authoritative role. Every school always has one teacher who is a Lady Elaine, every church has one church lady who is a Lady Elaine, and there is always a King Friday. John’s brother Bart was King Friday, he likes to make pronouncements about how things have to be done. Ken is King Friday in his house. He has a lot of eccentric needs that people are nice enough to cater to.

John was very into train sets which was a big part of the model city.

+ GI Joes vs Playmobil vs Star Wars figures (OM106)

John played with GI Joes a lot while Ken had Star Wars figures instead. GI Joes and Star Wares figures could interact with one another because they were the same scale. GI Joes could bend their knees but Star Wars could not and all the cockpits had to be like weird Asian Tatami tables. Playmobil characters, which were also the same scale, couldn’t bend their knees either. Ken always felt that Playmobil was a weird second tier Euro-toy. If you want to play with LEGO, then play with LEGO! He felt sad for his friends who had Playmobil instead of LEGO. John felt like they had the best vehicles and the coolest trucks, helicopters and boats.

The problem is that John was too old for GI Joes and a lot of these cool little dudes came into production right about the time when he should have been transitioning to being into Rock music and girls, but he held on to them for so long that every time a friend came over he had to hide his toys that he really enjoyed playing with. He had to conceal them because his friends were talking about [https://en.wikipedia.org/wiki/2112_(album) 2112], or [https://youtu.be/m_6dkzL4VS4 Temples of Syrinx] while John was like ”pow pow pow!” 

Who would ever want to go on a date if you had a really cool [https://www.amazon.com/PLAYMOBIL-Station-Lighthouse-Discontinued-manufacturer/dp/B00FJR0ULI Playmobil coast guard rescue set]? It is still true to this day and John doesn’t want to go on dates, but he does want to play with his little Coast Guard Rescue Set. Men do regress that way and ultimately it will come down to a model train set. John will be out in the barn with his little Papier-mâché mountains and he won’t have to worry turning down dates because those offers will stop coming as he gets more and more into model trains.

+ Sesame Street and Scooby Doo (OM106)

When John was six years old, television entertainment was still in its golden era. There was a sense of scripts written by crafts people and the stories were internally consistent without a ton of pandering.

Sesame Street, the best part of children’s entertainment then as now, debuted on November 10th 1969 when John had just turned one year old. His mom had him in his high chair and turned on the TV to watch the first episode of Sesame Street, because it was in all the newspapers. John watched Sesame Streets from the very beginning.

Scooby Doo was released on John’s birthday on September 13th 1969. John loved Scooby Doo when he was a kid. It was a mystery show based around the Archies and in a lot of cases the supernatural thing turned out to be a property dispute where you got to keep somebody from selling. This goes back at least to the Haunt of the Baskervilles.

+ Whom to take to the High School Prom (OM106)

John references Ken’s famous bit from Jeopardy ”What is a Hoe?”

As a High Schooler in the early 1990s Ken would have taken [https://en.wikipedia.org/wiki/Davy_Jones_(musician) Davy Jones] to the prom, that sounds awesome! John would have taken [https://en.wikipedia.org/wiki/Kristy_McNichol Kristy McNichol] or, by the time of the prom he would absolutely have taken [https://en.wikipedia.org/wiki/Molly_Ringwald Molly Ringwald] for sure. For Ken it would be [https://en.wikipedia.org/wiki/Paula_Abdul Paula Abdul]. 

+ Artist side gigs (OM106)

One time some friends of John's booked [https://en.wikipedia.org/wiki/Beyonc%C3%A9 Beyoncé] to play a young girl’s [https://en.wikipedia.org/wiki/Bar_and_Bat_Mitzvah Bat Mitzvah] at [https://en.wikipedia.org/wiki/Rainier_Club The Rainier Club] in Seattle. Beyoncé came with all of her dancers and a big light show, and she put on a 45 minute concert for a group of about 100 teen girls because her father gave her $750.000. It was one of these ”Fly me in, I’ll do the 45 and then fly me home!” 

Ken always wanted to be some corrupt Central Asian dictator, just to be able to fly in [https://en.wikipedia.org/wiki/Scarlett_Johansson Scarlett Johansson] to his cocktail party and then fly her home and all she had to do was stand around with a glass of champagne in her hand. John thinks that happens more often than not and is a real side-gig for B-list Hollywood people. Ken finds it annoying that it does not extend to game show contestants. 

+ The Brady Bunch, Cousin Oliver syndrome (OM106)

The Brady Bunch pioneered what would later become known as the [https://en.wikipedia.org/wiki/Robbie_Rist Cousin Oliver] syndrome. It was the first show that attempted to revitalize its energy by shoehorning in an adorable young new cast member explicitly to attract younger viewers and who’s character is a recognizable trope of a mischievous moppet who knocks over flower pots, causes all kinds of havoc and is knowing beyond his years, but still lovable and cute, able to employ sarcasm without ever coming across as an asshole. Here he comes: Cousin Oliver, the cute little extremely weirdly John Denver looking child.

His parents had moved to an archeological dig in South America and rather than taking their child and showing him a brand new world, they were just going to dump him on their cousin and be gone presumably forever. Cousin Oliver was the debut of this trope and he initially probably sowed some confusion. John remembers not being fooled by him as a kid, even though he really aspired to that chubby-cheeked look himself and he had the same hair. Every 1980s kid had that hair at a certain point, although this was 1974 and it was the dawn of [https://en.wikipedia.org/wiki/Toughskins Toughskins] jeans. Cousin Oliver was emblematic of the time, but it did not fool people.

+ Scrappy Doo (OM106)

When Scrappy Doo was introduced John was not really watching Saturday Morning Cartoons anymore, but as he was playing with GI Joes he still kind of was watching Saturday Morning Cartoons. He was 11 years old and the cartoons were baked into his Saturday mornings. John despised Scrappy Doo and hated him with every fiber of his being. He was old enough to feel that it was pandering and an example of the worst instincts of television executives, something that felt forced and false.

+ Elmo (OM106)

Unfortunately, the ultimate example of the decline of a certain style of television was a character by the name of Elmo arriving in Sesame Street. It was determined by the [https://en.wikipedia.org/wiki/Sesame_Workshop Children’s Television Workshop] that in order to appeal to young kids there needed to be a muppet that baby-talked and did adorably cute things but was also wise beyond its years and so Elmo arrived and became the most popular muppet. The tone and timber of Sesame Street changed dramatically in a way that was very akin to the introduction of Scrappy Doo because a lot of the old main characters found themselves with less and less to do and the show became more and more infantilized. 

Elmo remains one of the most popular muppets of all time. As a parent John forbade Elmo from crossing their threshold which is fairly common of parents his age. Elmo is a thing we all recognize: It is pandering and trying to sell merch and it besmirched an organization with such integrity as Sesame Street. When John was a toddler watching Sesame Street, grown-ups talked like grown-ups and muppets had very distinctive characters, talked in full sentences and didn’t babble. The idea that a kid couldn’t relate to a muppet unless it talked like a baby was offensive to people John’s age, but the research was right: The kids loved Elmo. But did Elmo benefit the kids?

+ Mary Sue (OM106)

The fan-fic trope called [https://en.wikipedia.org/wiki/Mary_Sue Mary Sue] means that a female fan-fic writer inserts herself into the story as the new protagonist that everybody loves. There are variations on Mary Sue, but there is surely a world in which (Cousin) Oliver and Penny (from Good Times) live with their delightful benefactor mother who is also very attractive to Captain Picard, or a world in which Scrappy Doo just murders Elmo because he is a monster, but that would probably be written by a male fan-fic writer by the name of Marty Sue. 

Podcasts about serial murderers have a vast majority female listenership, which is puzzling to Ken. Maybe Scrappy Doo murder fan-fic is actually all written by women? Ken and John could boost their female demographic by having serial killer episodes, but they already have a pretty large female demographic, considering how little serial killer pandering they do. It is the dulcet tones of Ken’s voice! John’s voice is like browned butter and Ken’s voice is like a Stradivarius that has been left in the sun.














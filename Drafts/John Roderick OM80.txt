OM80 - The Anarchist Cookbook
2018-08-30

> "Did you know that if you mixed equal parts of gasoline and frozen orange juice concentrate you can make Napalm?" - "No, I did not know that. Is that true?" - "That’s right! One can make all kinds of explosives using simple household items, if one were so inclined."

John: ”Ken, would you describe yourself as a counter-culture person?”
Ken: ”I’m a white, affluent father of two from a fairly conservative Christian background. Yes! I am an icon of the counter-culture!” John agrees that Ken is a voice of the disenfranchised, of the new New Left.

+ American counterculture movements (OM80)

When John was young, he identified very strongly with a counterculture movement of the 1960s called the New Left, which was the activist hippie anti-war, anti-capitalist, anti-American-dream left. Ken suggests that John did this mostly for the aesthetic: He loved how the hippies dressed, like the fringe and the beads, but he didn’t believe in any of their values. He had posters of Barry Goldwater on his wall, but all he wanted was dress like these guys and still be a member of the Jon Burge Society. 

John’s dad was born in 1921 and John has older brothers and sisters who were Baby Boomers. Although John is a member of the subsequent Generation X, he felt as late as the early 1980s that a lot of the concerns were still very real: They were still locked in an un-winnable cold war and there was still a lot of conservative traditional American culture. The active left had been seeking more social justice, more equal rights for women, and a different take on industrial colonial capitalism, and those felt like very current ideas. 

During the Reagan era, those ideas were where John was coming from. He was the boring parents on Family Ties, while Ken was the exciting young Alex P. Keaton, trying to make some cash with his stock market portfolio, his skinny little grey tie, his suspenders, and his Patrick Bateman books. 

Ken and John are fairly close to the same age (John was born on 1968-09-13, Ken was born on 1974-05-23), but there is just enough of a generation gap. By virtue of John having older parents, he has older siblings who are plausible Woodstock-generation kids, but to Ken those people are his parents, not his siblings, and there is an unbridgeable gulf between him and this culture. He was born only 4 years after The Beatles broke up, but it seemed like an eternity ago. That music was on the Oldies station and he has no connection to it.

In the 1980s the Baby Boomers first started to wallow in nostalgia about themselves. Nick at Nite, Thirtysomething and The Big Chill were on television and it was the start of their hagiography. They claimed that they used to believe in something, they changed the world, but what happened to us, Ron? All this navel-gazing! The parents on Family Ties are a great example of how the Boomers were starting to portrait themselves. 

Generation X saw the Baby Boomers as compromised, middle of the road pseudo-vegetarians, who at the same time were exactly the perpetrators of the Wall Street, go-go cocaine era. Once they were running the culture, they wanted theirs! They are the ones who elected George W Bush, whom John always sees as the culmination of the Boomer arc. Some of them may have embraced Clinton, but he was a pretty center-right liberal while they were on the bomb-throwing left. 

+ The left wing’s struggle to find its place in the 1980s (OM80)

Being a teenager throughout the early 1980s, John really felt like the struggle was real and the war was still on. Reagan confirmed that there was a culture war in the United States between people who believed in a progressive agenda and the war-mongering cliché of the sword-rattling capitalist arch-enemy of the people. The left probably felt very isolated and marginalized then, because people who they thought of as a right-wing caricatures had taken over popular culture. 

Reagan was universally beloved, a lot of working-class democrats were voting for him and if you were actually one of these true-believer fire-brands, you probably felt like ”Wait a second! This used to be a voice in the culture, but now I’m a voice in the wilderness!” It was a strange time. Jimmy Carter was a very mild-mannered and pretty middle-of-the-road progressive and he was reviled by the culture as ineffectual and appeasing of the Soulviet [sic] menace [[footnote]]The Soulviets [sic] were like the Soviets, but they had more soul and listened to James Brown. There is a term for people who insert the letter ”l” into Soviet and those people are known as The Rodericks (with 3 syllables). People who pronounce it the normal way are called The Jennennings, or the Normamals[[/footnote]].

The left was disarmament-minded, but at the same time it was hard to side with the revolutions of Central America. Who’s your guy in Nicaragua? It was not cool to be Oliver North and the Sandinistas didn’t have a sterling record[[footnote]]Even though the Sandinistas had a Clash record named for them, which was their most sterling record, but it was a little over-long.[[/footnote]]. This was true throughout the 1960s, 1970s and until now. A lot of the conflicts that characterized that era for John are now present again, but there isn’t a very activist left that seems to be a little bit at sea. 

The Left does understand the overarching goal of their world view, but it is very difficult to know exactly how to implement it. The middle class and the working class renounce organized labor and the traditional platforms of the left. Rampant authoritarianism starts to look pretty attractive, which also happened during the Reagan era where the working class was voting against their interests because they were being appealed to with the xenophobic and militaristic argument. There was also the personality argument, the who-would-you-rather-have-a-beer-with argument: Who is more of a nice, fatherly guy? Ronald Reagan or Walter Mondale?

As a leftist radical, The Strange Bedfellows problem was a big problem: Who are your friends? You don’t want to have Che on your poster! Is it Castro? If you were against Ronald Reagan, does that suggest that you are for Brezhnev? [[footnote]]Ken wrote in Brezhnev in every election during his lifetime[[/footnote]]. 

Jane Fonda visiting Vietnam to show her anti-war support for the noble people of Vietnam was a very bad optic and the idea that you could give aid and comfort to the enemy of your nation at war was very misguided. She was photographed sitting on an anti-aircraft gun which would only be targeted at American planes. Did she do the Kathy Griffin thing where she said ”I don’t know, they just told me to sit on the gun. I didn’t even know you would be able to see it.” She might even have raised one fist in the air in solidarity, not realizing what a cultural cooptation that was.

You can hear a lot of people on social media today saying that the US government is illegitimate because it is essentially a white supremacist conspiracy and taking up armed struggle against it is morally acceptable because the government has no authority from a moral standpoint. This argument is often made from revolutionaries on both the right and the left. They challenge the authority of the thing they are at war against and it condones extraordinary measures like murder and terrorism because the ends justify the means. 

Ken thinks that the far left is better at optics today than the far right. The far right seems to have no self-awareness of how Nazi-imagery looks, whereas Bernie Sanders does honey-colored campaign ads with America by Paul Simon playing. These guys figured it out and are not hopping on the anti-aircraft guns anymore. There are no more Che posters and no more big red stars on their shirts.

On the other hand, the left still has the longstanding problem of fighting most of its fights with itself. The right is really good at circling the wagons and putting the whole mismatched group of concerns into one big cauldron. The left’s big problems are introspection and conscience.

+ Revolution seeming like the only option (OM80)

In the revolutionary 1960s, a lot of energy was devoted to the idea that the only way to really make a difference in the United States would be to actually start an insurrection, a resistance that took the form of a revolution which is a popular way of changing government[[footnote]]It is one of the more popular ways, one of the Top 4: There is elections, being invaded by another country, and there is the Somalian version which is just 20 years of anarchy.[[/footnote]]. It is a high watermark for this idea being in the mainstream. 

Huge student organizations on every campus premised on the idea that working within the system was not going to cut it and some kind of revolution was necessary when it came to Vietnam and all the other corruption and compromise of the era. What made 1968 such an extraordinary year was that it was happening all around the world, for example in the Czech republic and in Paris. The idea was that the students were going to rise up and effect dramatic and sweeping change. 

The Velvet revolution and Václav Havel’s presidency might have been a result of that 20 years later, but nothing like it happened in Kent State, so Prague was the place to go. The European revolutions of 1848 did not necessarily bear fruit immediately, but sometimes decades later. Way to take the long view! It is very comforting for all these do-nothing students who are like ”Yeah, look what happened //eventually//!” 

John gets yelled at on the Internet for taking the long view all the time. You are not allowed to do that now, but you have to fight your battles where you stand and you need to have a hot take.

+ John being a member of the left in Anchorage (OM80)

In about 1981, John was a member of groups of like-minded students in the Anchorage, Alaska school system who had found each other. They recognized one another because they were wearing fatigue jackets from the Army Navy surplus store and they would see each other at the same punk rock shows at youth centers. 

The right wing kids and survivalists in Alaska didn’t have a fashion sense yet and they were not wearing Army Navy surplus store stuff, which made it easy to tell the two camps apart. Nowadays the weird neo-right spends a lot of time waxing their mustaches, but at the time they were wearing Sta-Prest slacks, while the Left took an interest in looking like cool Army men. 

Having a fashion sense is going to pay off in the long view, because it is going to win over the hearts and minds eventually. History belongs to the people who had the best men’s fashion. Why do they talk about Hitler every single episode? Because the Nazis were the best dressers of that war[[footnote]]which is a hot take![[/footnote]] Hugo Boss dressed the 3rd Reich, not that Hugo Boss himself went with a tape measure and took Hitler’s inseam. Hitler only had one testicle and he probably dressed left, so you had to take the inseam on both sides[[footnote]]The reason no-one says that anymore is because people don’t get their clothes tailored anymore. If you went into a room with a tailor of a certain age, he would ask you which direction you dressed, depending on how tight you wear your trousers.[[/footnote]].

+ The Anarchist cookbook and free speech (OM80)

John was able to find a copy of the Anarchist cookbook in an alternative book store, which is a thing that their young listeners don’t have a ton of experience with. You might as well start with ”book store”. If you walk into one of the few surviving ones, you might see Ken Jennings sitting at a folding table with a stack of his most recent book Planet Funny, looking sad and lonely, ready to autograph it over to whichever of your spouses you are buying it for. 

There are wonderful independent book stores still around, but not as many counterculture book stores. There are still some, but they are new age-y now and they sell candles and books. There is one in Pike Place Market in Seattle called Left Bank Books. Ken finds it confusion because they have a few Jack Reacher books, some real books and also ”Here is what you need to know about Honduras”. 

Counter culture bookstores were always confusing because they were typically labors of love and their filing system is often one person’s bright ideas about how books should be cataloged. The left loses not just to introspection and conscience, but also the lack of a good classification system. They are trying to rebel against Dewey Decimal as well.

From its initial publishing in 1971, the Anarchist cookbook was immediately an incredibly controversial book. It was always difficult to find, different states tried to ban it, and a lot of effort was made to suppress it, but this was an era where free speech in publishing was a cause célèbre for intellectuals, largely on the left. The idea is that the 1st amendment protects printed material and that it behooved us as citizens to read all manner of disagreeable text in order to form our own opinions. 

It was a high watermark of the left doing this, like for example the ACLU [https://www.aclu.org/blog/free-speech/aclus-longstanding-commitment-defending-speech-we-hate was defending Nazis in Skokie] //(in the 1970s)// because the principle is more important. This was also the aftermath of Lenny Bruce and the obscenity trials, the idea that you could censor comedy and censor thought by describing it as dangerous, obscene or seditious. 

The intellectual left had a considerable impetus to maintain the freedom of the press, to enshrine it and to protect it as one of the holy pillars of the United States. The 1st amendment case for banning a book like this is pretty weak. The ruling language from a 1969 Supreme Court decision says that in order to ban a book, it would have to ”likely incite imminent lawless activity” and that is a pretty high standard. 

You have to demonstrate that something imminent happened that would not have happened without the book. A case like this was made about The Turner Diaries, a novel by William Pierce that is considered to be the Bible of the racist right. It is a novel where white supremacists overthrow the federal government and create a white homeland. It was cited as an influence on Timothy McVeigh when he bombed Oklahoma City in 1995[[footnote]]Turner’s first name is Earl, the name of a brave white hero. To them, Earl is a sexy name, and it does sound like a sexy guy who works at the Feed & Seed store. To us it is some trailer park caricature, but they are like ”My name is Bond. Earl Bond!”[[/footnote]]

There has been quite an effort to suppress The Turner Diaries because the book has such a despicable take, but again: Can you make the argument that it is inciting violence? You really can’t! It is very hard to demonstrate in court that this attack or this bombing would not have happened without the book. Sure, the whacko had the book, but does John Lennon get short without The Catcher in the Rye? Probably! 

The Anarchist Cookbook is often referred to as a Bible of people who are committing violent acts. It was found among the Columbine shooter’s effects, it influenced Timothy McVeigh and the Oklahoma City bombing, the guy who shot Gabby Giffords in Arizona had it, and the Boston Marathon people. Maybe some of those are tenuous, but it gets linked to a lot of high profile headline-making violence. 

Unlike The Turner Diaries, The Anarchist Cookbook is explicitly a How-to. It has "cookbook" in the name! It is not like a Twilight Zone episode where you find out later that it is a cookbook. This is like ”We are going to make detonators, guys!”

+ The history of the Anarchist Cookbook (OM80)

The book was written by a teenager by the name of William Powell. He grew up overseas and went to British schools for a while. He felt estranged not only from his parents and the culture at large, but he was also a kid with a funny accent. He is a third culture kid like Ken: You neither feel at home in your putative home country nor the place where you live and you are a permanent outsider. 

Ken never made bombs out of Clorox bottles and he never learned to make detonators, but he watched Jeopardy until it made him famous. If you have the choice, you really should stick with the game shows, but being introspective is also going to weaken your cause and you are letting your ideology down if you think what would be the right way to do this. 

William Powell mixed his estrangement with a healthy does of late-1960s cynicism, went to the New York public library and spent quite a bit of time reading old special forces handbooks and chemistry textbooks. He does not have any first-hand experience with any left-wing groups, this is not stuff he and his buddies were doing on campus, but he was teenager who had never built a single one of those things. He is a nerd at the library!

Powell researched all these different formulas and wrote about them very authoritatively. His voice is not that of a teen who just found out some cool stuff, but it is very much like the Internet: He was an uninformed teenage neckbeard who was writing about this stuff as though he was a special forces instructor who had been working overseas in clandestine insurrections for many years and was now sharing his secrets. 

The book was an instant hit! Even though everyone abhors it and everyone disavows it, you can buy it on Amazon right now. Ken looked at the ranking and he is pretty pleased that he is outselling the Anarchist Cookbook. Nicely done! That is kind of the end of his interest in it, now that he knows that he is outselling it. 

+ John selling bombs in school (OM80)

John got ahold of The Anarchist Cookbook in 1981, about 10 years after it was written. He was 12 years old, he was in Junior High, and he was very interested in its contents. John was very precocious in his desire to fight the United States. Was he going to put cherry bombs in the urinals? Yes! In fact, he got temporarily expended //(suspended or expelled)// from school for bringing bombs to school, not just little firecrackers, but actual bombs he had made with information he got from the Anarchist cookbook and that he was selling to other kids. It was a really bad episode in his young life. He was an anarcho-capitalist, he made the bombs, he blew them up, he thought that was fun, but then other people wanted them and he figured out he could make a profit. John was going to disrupt disruption!

The book is super-general. It gives the appearance of being very specific, like here is exactly how much Saltpeter and Sulphur to mix with your charcoal in order to make gunpowder, but it reads like it was written by a teenager who learned all this at the New York public library. The voice of adult special forces authority is convincing, but when you actually try to make things from the book, you will find that it is missing all of the actual instruction that you would need. 

It is more like an Anarchist dream journal. You think ”Wow, if I ever need to dig a trap, fill it with punji sticks, direct a platoon of soldiers chasing me into this boobie trap and then shoot them with a grenade launcher that I made out of a shotgun, I have all the information right here!”, but if you ever try to do any single one of those things, you will find that it was really just Sketches of Spain. 

Ken thinks that this whole book is clearly a false-flag operation to give rebellious John Roderick types the illusion that they can knock their PE coach into a trench on punji sticks, but you will find out you can’t do it[[footnote]]This was very hot take that Ken doesn’t actually believes is true.[[/footnote]]. A lot of kids like John thought it was super-cool and edgy to read about this kind of stuff, but they would never actually do anything with it. 

Nevertheless, John and his peers were sitting and plotting against the Reagan administration, hoping they could catch George Shultz in a men’s room at the Watergate hotel at some point, or hoping Caspar Weinberger would ever come to their Junior High, which would be their moment to pants him and pee in his soup. What you are really doing is expressing how mad you are at your PE coach and your mom and dad who are going to find it under your bed and be like ”We have to talk to John!” 

+ The author wanting to retract the book (OM80)

Although this book was a runaway hit, not very many years later the author converted to anglicism, he became religious, renounced and disavowed it and tried to get it removed from print. However, in the course of getting it published in the first place, he had signed over his rights to his publisher Lyle Stuart who was one of the great charismatic American kooks. He was a newspaperman and a bon vivant, he was one of the part owners of the original Aladdin hotel in Las Vegas, he was a successful gambler who wrote several books about how to play Baccarat. 

He was a true free speech advocate, a member of a different style of counter-culture which is the generation earlier, he was close friends with William Gaines, the publisher of MAD Magazine, and he was a classic New York Jewish open-minded cultural omnivore who also believed in free love. 

His big-selling book before the Anarchist cookbook was The Sensuous Woman, which 1970s kids may remember as a first-hand pseudonymous sex diary. It was the first account of a woman’s sexuality that wasn’t over-chased, an open telling what it took to please a woman, and that was very titillating at the time. He also published Naked Came the Stranger, which was an enormous bestseller. Lyle does not advocate for free speech in order to push some degenerate world view on America, but he just firmly believes that the antidote to censorship is to make everything available.

Although William Powell disavowed the book and wanted it off the shelves, Lyle Stuart continued to sell it and it continued to be a bestseller, even though it was banned in Australia and other countries. Lyle Stuart continued to be a force in publishing even after Lyle Stuart Inc was sold. He opened a publishing company called Barricade Books that also reissued The Turner Diaries.

Eventually Lyle Stuart got his comeuppance because he made some libelous remarks about the Bellagio-guy Steve Wynn and his connection to the Genovese crime family who sued him, won a judgement about him and forced him out of business.

+ Subsequent books (OM80)

The Anarchist cookbook inspired a lot of subsequent similar publications by actual anarchists who remain an undercurrent in American politics, not just kids drawing the ”A” in a circle on their Trapper Keeper, but people who believe that we should each be accountable to ourselves and be self-governing. In their opinion, self-governance creates a better world than external governance. There is not a whole lot of evidence that this is the case, but if Ken wants to argue with Anarchists on the internet, be my guest! Whatever your argument is, they have thought it through and they have a rejoinder to it. Anarchists in general do not endorse or embrace the Anarchist Cookbook, because it does not espouse any actual Anarchistic principles, It isn’t a political book about anarchism and William Powell did not do very much research on the positive aspects of anarchy, but he was just using the term to mean ”Some people like to see the world burn!” 

A lot of people who actually know how to built punji traps and silencers out of cardboard boxes came out and said ”That’s not how you do it!” and published a couple of variations on the theme. A retired cop came out with a book about improvised explosive devices that he tried to sell to police departments as a primer for teaching cops to be prepared. 

This was long before the Iraq war, back when police were getting injured by improvised explosive devices built by the Minutemen and by the Symbionese Liberation Army. They were encountering IEDs back then, this cop wrote a book, they sent it to a bunch of police departments and they called it a false flag and a trap. 

+ Being replaced by the Internet (OM80)

In the early days of the Internet, the Anarchist cookbook found a new life online in the Newsnet era. There was an anonymous author of this type of articles called the Jolly Roger on early usenet stuff who compiled not only the Anarchist cookbook, but a lot of texts on improvised warfare and survivalism. The Venn diagrams between people who want to build traps to kill police, people who want to build traps to kill zombies and people who want to go start a white homeland and make it impenetrable to all the break dancers that they think will come to Montana, break into their compound and steal their bulgur wheat overlaps quite a lot.

If you want to learn to make anything on the Internet, like home-made Napalm, Battle-bots, or a Stitch Sampler that denigrates Napoleon, you can find all necessary instructions online. There are no real secrets anymore, but it just becomes a question of having the will and being motivated to actually learn these techniques and blow off a couple of fingers before you figure out how you actually make the bombs. 

To Ken, this somewhat exonerates William Powell’s culpability. This information was going to get out there eventually, whether or not he called Lyle Stuart or not. Once the culture knows how to build a certain kind of detonator, you can’t stop a tidal wave. If we had just been waiting around for William Powell to give us this information, we would have seen an awful lot more punji traps in American culture than we did from 1971 on. 99.99% of the people who bought this book bought it as a pre-Pinterest Che T-shirt. You put in on your bookshelf, and you read yourself to sleep at night, dreaming of your vision board of how you would live after the apocalypse. 

Powell is the grandfather of a certain kind of disaffected Proud Boy. What makes the story sad is that he became famous as the writer of this book at age 19, but only 5 years later at the age of 24 he had grown out of it and wanted to get on with his life. He ended up as a special education teacher, he moved to Saudi Arabia to teach children and he spent the rest of his life until his early death trying to outlive it and to live a different life. 

The FBI researched it early on and found that there was nothing in it that warranted his prosecution or its banning. He got the information from the New York public library after all! Powell lived under its shadow for the rest of his life, which is instructive because there are many people in their teens and twenties saying dumb things on the Internet. John and Ken are old enough to have missed it then and they know better than to say dumb stuff now. 

If all of the things that John has thought and felt when he was 19, 20, 21 were on the internet forever, it would be a hard satchel to carry with him everywhere. Kids today are at least conscious of the fact and they have a reciprocating chorus of like-minded people John didn’t have back then. 

Those who read the Anarchist cookbook in 1981 felt pretty isolated because they didn’t have a global network of people they could bounce those ideas of and make it slightly weirder with each retelling until you get Qanon. For John, William Powell is the proto-neckbeard who one day will get an actual job and try as hard as he can to delete his Facebook-page, but it just keeps coming up in interviews. He puts down his resume and says that he has been programming in C++ all these years, but what about this post where you describe how to build a punji trap?

+ Anarchist Cookbook - The movie (OM80)

In 2002, a romantic comedy called The Anarchist Cookbook was released by a group of independent filmmakers in Texas. It debuted at the Seattle International Film Festival in 2002 and is about a lovable ragtag group of anarchists living in a squad who are radicalized by an interloper who has more anarchistic ideas than they did. Hilarity ensues. 

The film has an all-star cast and one of the producers was Robert Brown who worked with Spielberg. It had a budget of $2 million and earned $14.000 at the box office. It is interesting that somewhere along the line, a screenwriter and director, in this case Jordan Susman, said ”You know what? That Anarchist Cookbook, that is the book to turn into a movie!” when there already was a movie that teaches kids how to make boobie traps, called Home Alone.

+ Ken Jennings (OM80)

Ken tells the story that unofficial trivia books about a work are no longer considered fair use and if you write and Omnibus fanbook about the podcast, He and John will come after you with their high-powered lawyers. They will put the Castle Rock v Carol Publishing precedent in your face so fast it will make your head spin! Their number one high powered lawyer is Ken’s father, Ken Jennings Jr who brooks no funny business. 

Ken is the third and his dad is delighted by this fact and he will tell random strangers and clerks at the store that Ken Jennings is his son and he is Ken Jennings Junior, his own son’s father. Ken Jennings Senior is no longer alive, but he was a pet food salesman originally from Muleshoe, Texas and later from Central Washington. RIP, KJ Senior! Long may you wave the Purina flag flying flapping slowly in the wind.

















 
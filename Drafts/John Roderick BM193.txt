BM193 - Alternative Family Set Ups
2017-02-23

This show is hosted by [https://twitter.com/bizellis Biz Ellis] and [https://twitter.com/theresathorn Theresa Thorn], a show about life after giving life. This week: Living arrangements and different family setups with [[[John Roderick]]].

John is known as singer songwriter of [[[The Long Winters]]] and he is a podcaster himself. He joined them at their live MaxFunCon event in summer 2016. At the MaxFunCon show they talked a lot about sleepovers and John was very kind to speak for all men, which he did a wonderful job of doing and he is happy to do it again. Today they are going talk about all other living arrangements that are not like Theresa’s. 

+ Traveling with children and taking days off school (BM193)

John and his daughter's mother are currently doing a lot traveling. His daughter is a Kindergartener, but John's family is not embracing the notion that school is some sacred covenant that you can never miss a day and the child needs to be there exactly on time either. Right now it is spring break and this weekend John and his daughter will be going to Alaska. She will be missing a day of school next week, then she will be in school for 2 days and will immediately go on a vacation to Whistler with her mother. 

After she will be back and throughout the whole spring they will be taking 3-day weekends here and there. Some of John’s friends back East will tell him that they can’t go on an all-expenses-paid trip to Paris with him because if their child misses one day of Junior High she will never get into Yale and that means she is going to work in the boiler room of a steam ship for the rest of her life. John’s family’s attitude is that they are going to keep pushing that until somebody actually says that if the child misses one more day of school, they will literally give her the key to the salt mine. 

Theresa says that she would have been better off lying to the school that her kid was sick instead of telling the truth that she was going to Disneyland. 

John is really terrified of this, because he does want to continue to take a hand in his child’s education in the form of going interesting places and meeting interesting people. He does not like the idea that he will be restricted by a potential robocall from school for the next 12 years. John is anti-authoritarian and if the school is going to complain, then John //is// going to take her to Paris! Pretty soon she is going to be kicked out of school and John is going to homeschool her, which he desperately does not want. 

In Theresa’s school district, if kids are gone for 4 days, there is apparently nothing that can be done, but if they are gone for 5 days, then you can consider it an independent study, but you have to get it approved ahead of time. Long weekends often result in conflict at home. 

+ John’s alternative family arrangement (BM193)

John has a very alternative family arrangement which he never expected would be something extraordinary in this age of everybody doing what they want and recognizing the foibles of relationships. All the way through his 20s he had relationships with ex-girlfriends or other women with whom he was very friendly saying that if neither one of them had a kid by the time they were 35, they would meet in the rest facility in the airport of Kuala Lumpur where he could impregnate her and they would have a baby. 

As a member of generation X John had a much more fluid sense of relationships than his parents had and part of that modernity is that they could have children with one another as a gesture of companionship and companionability. Both of them would be active in raising the child, but it wouldn’t be always in the context of a marriage. 

Theresa is from the Generation X as well. It was the time when divorce was really happening widespread for the first time and it was the first time a generation had friends with two different houses they went to. It was the beginning of a non-traditional setup. She was experiencing the same conversation that they wanted to get married in case they didn’t have kids in their 30s.

+ John’s parents getting divorced (BM193)

John was a product of a divorce and his whole childhood was spent in a shared-custody arrangement that was often very contentious for reasons that had a lot to do with how the divorce had gone down. John’s parents were both very smart people and very admired, but somehow couldn’t ever manage to transcend the act of divorce. There were many bitter feelings! They got divorced when John was 3 years old and even when he was in High School they were still refighting these 20 year long battles. 

John grew up in a Catholic neighborhood in Alaska and among his core group of friends he was the only one who came from a divorced house. The other families in the neighborhood regarded his single mother with tremendous suspicion, because she was surely some kind of marriage-wrecking hussy. They couldn’t invite her to their cocktail parties, but they all loved John’s dad because he was a rakish and suave older gentlemen, while his mom had to be some kind of terrible single woman. 

John didn’t have a lot of role models in his life what would seem to be in a healthy marriage. The Catholic kids and their parents may have had a happy marriage, but it didn’t resemble anything John aspired to. Having the crusts cut off your sandwiches didn’t seem like a life John personally cared about. He does not cut if off his daughter’s sandwiches, but she either eats the crust or she leaves it on the plate and it becomes a little reward for John. 

Throughout John’s twenties he felt liberated from all constraint as a lot of generation-Xers did, but he would still see people his own age at 25-26 years old who already had coupled and had a kid. It was astonishing! Are you Mennonites? What are you doing? It is surprising now looking back, but in John’s 30s none of his own friends had kids. They were all writing plays and forming bands and having adventures, but none of the women John dated considered even for a second having a kid and John had no peers who had made that transition. 

What that resulted in was that suddenly John was 45 years old and had no kid or even a girlfriend. He had been in bands and had gone around the world, but now he was 45 and he had always wanted a family. What John didn’t have was a strong feeling that a child and a family needed to happen within the rubric of a marriage. In fact, his experience had sort of been the opposite, at least his first-hand experience. 

As John was getting out into the world and wasn’t living in a Catholic neighborhood anymore, he realized that a lot of people he met were products of a divorce in an environment of one partner cheating on the other or one partner betraying the other in some way. There were very hurt feelings! For a long time, the only women John ever managed to date within the context of being an Indie Rocker were girls who had very conflicted relationships with their father and all through this divorce thing of ”My dad is a bastard, and yet I’m obsessed with him”.

+ John’s daughter’s mother being pregnant (BM193)

When John was 45 years old, he had a lot of close female friends and at a certain point he had an unexpected pregnancy arrive upon the scene. He is still confused and unsure whether that unexpectedness was shared among all parties. It was never entirely clear, but here he was confronted by this prospect. John wanted a kid very much and although they weren’t in a love-relationship, John knew and admired his daughter’s mother. 

At that juncture, particularly in John's culture, it seemed that to shotgun a marriage would be counterproductive. They both wanted a child, so they first agreed on that and then they agreed that they weren’t going to obligate themselves to get married. Throughout her pregnancy they were really hammering out what exactly the terms of their relationship were going to be and they were trying to be very candid with one another, because they didn’t want to be in a position where the baby arrived and they suddenly had a bunch of surprises in the form of unspoken expectations.

Those were 9 crazy months for sure! One of the operating understandings was that John was a Rock’n’Roller and he was famously here and there. He had cultivated a rakishness and a traveling hobo-vibe. Sometimes he wore an Ascot for no apparent reason and like a lot of these Kuala Lumpur agreements John had made in his 20s and 30s, it was premised on the idea that it was going to be an unconventional family right out of the gate. 

John's daughter’s mother had much more of a sense that one would fall in love and get married. She was in her late 30s and John saw her go in a direction where she was thinking of herself primarily as a single mother. John told her that he is not going to disappear at night, but he recognized that he had a tremendous obligation to her and the child. If John wanted to have a baby, part of not wanting to recapitulate this divorce scenario was also recognizing that if they would have an alternate family they would have to reinvent what being a dad is. John can’t come home and sit in front of the TV, but he also can’t continue to go on self-serving vacation just because he is not in a marriage. She had a hard time believing it. 

It really wasn’t a question of rights, because even the legal documents that bind you as a married couple don’t bind the husband to do anything but come home from work and say ”Why isn’t dinner on the table?” and then go and lay on the couch. John’s daughter’s mother works a full-time 50 hour a week job with a lot of responsibility and John’s job is one where he periodically flies to Los Angeles for 5 days or to New York for 5 days. He also spends a lot of time sitting and staring out the window in poetic repose and that is his literally his job. 

John worked 25 years for not having a boss and get paid eventually for staring out the window in poetic repose and he doesn’t apologize for it. A lot of people said that after having a child John will no longer be able to stare out the window in poetic repose, but if you can have a child and work 80 hours a week or work as a traveling encyclopedia salesman, you certainly can work as a songwriter. 

John believes that she resigned herself to the idea that she was going to be a single mother, because all of John’s assurances aside, there wasn’t any precedent for a father who remained super-involved where he didn’t have the legal binding, but the social agreement of marriage where all the pressure of what you do within a marriage bore down upon the man.

+ John’s daughter was born (BM193)

The day John’s daughter was born, John realized that at 45 years old he had never lived with a woman. He had always crashed with women, but he never stayed for the responsibility of two people sharing a space. Why would the mother of his child be concerned? He had shared the responsibility of making waffles! There had never been a situation where John hung up a painting on the wall that happened to be of a 1970s centerfold and as he stepped back from it and have another person look at it over his shoulder they went ”No way!” 

The day his daughter was born, they walked out of the hospital, looked at each other and wondered where they were headed. Were they going to her apartment where she had set up a crib and a baby’s room that has a little mobile of some birds? Or are they going to John’s house where he also had set up a crib and a little mobile of some birds? Standing in the parking lot with a tiny baby in their arms they had the experience that everyone has: ”Did they really just let us walk out of this building with this baby? What the hell?” 

During those 9 months they had not considered this decision. They looked at each other and John said ”Let’s go to my house!” and she said ”Okay!” and they went to John’s house at which point she lived at John’s house with the baby for the first 9 months of their daughter’s life. It is a baby! You need all hands on deck! They lived together at a nuclear family at John’s house for 9 months.

Around the 9 month-mark, his daughter’s mother came home from work one day and said ”I think I’d like to move to my own apartment” At that point, the baby had a measure of stability and they began what was all along going to be the nature of their family, which was that they had two complete separate homes and their daughter was going to grow up living in both homes. Since that time, the operation changes every week. 

Their pediatrician, a lot of their friends and a lot of the nay-sayers would say that the child desperately needs routine. Their answer was that this was almost a pre-judgement of what they were trying to do. The child needs to have breakfast at the same time every day and she has to know where she is going to be at every given moment, because that is how a child grows up as a healthy, happy individual. They would look at each other and they would say that their child was going to grow up with the routine of living in two houses and because John was a guitar player, she was going to have breakfast whenever he can figure it out. That was always her lot in life.

They are right now recording a parenting podcast as two white people and 50 minutes in neither of them has used the word ”privilege” yet! 

Reflecting back on what happened during the first year of their daughter’s life, the form of privilege that John had was that his daughter’s mother had resigned herself to the idea that she was a single mother and that anything she got beyond that was a bonus. Every bit of reassurance John tried to give her, every bit of action in the breach didn’t ever really convince her that there wasn’t going to be a day where John just blew away like leaves in the wind. She was hearing that from her mother, she was hearing that from her friends, she may have been hearing it from John’s mother that in a situation like this where she had not managed to secure a man in bondage, she was basically a single mother and she better get used to it. 

What that meant for John as a father was that everything he did above and beyond that was kind of a surprise and he got to continually surprise people that he was still not only there, but boots on the ground, hour by hour, absolutely completely involved, not only in his daughter’s life, but also in supporting his partner. 

There were other points along the way, like when John's daughter's mother went to a cocktail party one time and met a couple of lawyers there who focused on family law. They were very curious about her arrangement once they discovered it and they were shocked and horrified that they didn’t have a parenting plan. They told her that the absolute next thing she needed to do is get a parenting plan. It was absolutely essential in her situation! If she didn’t have one, she was a fool! She came to John the next day and told him that they needed a parenting plan and John wondered where this came from. They sat and talked about it for a while, and as they digested that idea, given the friendly and cooperative way they were managing this, a parenting plan was absolutely a recipe for failure. 

If you start to institutionalize what your rights and responsibilities are, you are going to start to conform to that document. If the parenting plan says that he gets the child three days a week and she gets the child three days a week and then there is a day which is open to negotiation, you are going to start living that way and that was not how they were living. They were living in a way where they woke up in the morning asking ”Who has the baby tonight?” and depending on who was doing what, they were flexible.

It has worked splendidly even now that their daughter is soon going to be in school, which is all about constraints. They have been working hard at maintaining goodwill and they have remained friends. They said over and over that they are either the most dysfunctional married couple that ever was or they were the best divorced couple that ever was. They are a group  trying to solve for X all the time. 

+ John getting a girlfriend (BM193)

The year 2016 introduced a new and potentially devastating wrinkle: After 6 years of their family setup together, John got a girlfriend. During those six years John didn’t ever get a girlfriend and her mother never got a boyfriend, but they were very focused on their daughter. Now because of happy accident, John got a girlfriend and that really disrupted the friendly cooperative alternative family they had created. 

They worked really hard, but in the end it relit the fuse of: "Here we are! We had 6 good years, but daddy is finally fulfilling the prophecy and he is revealing that he never really truly could be trusted. Now he is going to disappear with his new girlfriend and do the thing that so many 1970s-dads did, which is to grow his sideburns long. He will suddenly be in Hawaii all the time and show up at Christmas with his arms laden with kind of inappropriate presents because he didn’t keep up with the age and his daughter is 8 years old, but his presents are all kind of for 6-year olds"

John had to reproof every day that he was still not flaking out and that he still believed in the premise of their alternative family. Part of that premise was that he had more responsibility, not less. He was not going to dissolve their relationship and a dissolution of it wouldn't suddenly free him to buy a red Corvette and have his mid-life crisis. He knew what he was getting into and what that means is that he is still here! He is still fulfilling his commitment, not just to his daughter, but also to her mother.

John doesn’t even feel that constantly being in a situation where he is being tested again is unfair, but he recognizes each time that this actually is part of the job. Part of the burden of having an alternative family is that you have to reproof this, because you are bound primarily by your affection rather than by law or convention. 

The situation that most confounded John when watching other kids grow up in the 1970s was where the parents got divorced and one or the other of the parents found a new spouse, a stepmother or a stepfather, and then poured themselves into that new relationship neglecting their own child. That is the thing John never could imagine happening!

When John got a new girlfriend, he suddenly saw how strong he needed to be in the face of that, because his new girlfriend presented to him the opportunity for an exciting new life, a new family and a new emotional-elsewhere-ness. He had to make it clear to her that his daughter is the most important person in his life. Those are very tricky words, because you cannot say to your new love that your child takes precedence over her, ”I’m sorry!”, because she also has a right to say that she is your new love and she wants to be your number one! 

John cannot chose a thing over his daughter and that is a thing he needed to make clear and continually needs to make clear. The additional problem was that John’s new girlfriend and his daughter’s mother did not feel Kumbayah about one another. They are not all sitting on a beanbag chair, solving their problems through an encounter. 

Theresa was saying ”Good luck!” at that point and John's only response is that this is the ”Good luck!” he keeps hearing and it is exactly the ”Good luck!” he got when he said that he is having a baby with this person. It is a totally dismissive asshole-response of saying that this is going to be impossible. John is already 6 years in and he feels that they have succeeded. He can only approach this new challenge with the same sort of confidence that they don’t have to do what other people do. Maybe what Theresa really should say is ”Good job!”

+ Valentine’s Day (BM193)

Part of the problem of having a relationship like John's is that some things slip through the cracks and one of those things was Valentine’s Day. It was insane! John’s daughter’s mother had a conference in San Francisco and she was going to be gone for 10 days. She left the day before Valentine’s day, which meant that John’s daughter arrived at his house the night before Valentine’s day and she was going to be living with him exclusively for 10 days. It was 6pm and his daughter said that she is going to need valentines for 25 kids and John was like ”Say what?” 

Because her mother was gone for Valentine’s day, she didn’t think that this was a project she should have started 3 days ago. John was like ”What the who?”, they jumped out of the house, went to Target and there were no more valentines left for sale because it was 7pm the night before Valentine’s day. John bought some colored paper and envelopes and they got home and started making Valentines, but they had to remember the names of all 25 of her classmates and they just could not do it. 

As they got to 8:30pm there were frustrated tears and John took her to school on Valentine’s day and told the teacher that he was Single Father of the Year and there were no Valentines from them, but he promised them that he would get some. The teacher said that it was not that big of a deal, but that night his daughter came home with 24 valentines from her fellow classmates. John sat her down and told her that we do not reap what we not also sow and even though Valentine’s day was over, they would still need to sit here now and make these valentines. They did! They sat there and remembered all 24 of her classmates.

+ John’s vintage vehicles (BM193)

John does have an affection for vintage vehicles, because he thinks that new cars are dumb and he likes old cars that require a period of warming up before they are drivable. His daughter hates them because they don’t have all the mod-cons //(modern conveniences?)//, the seatbelt doesn’t retract automatically, the stereo is an AM radio that only plays church music, and they are drafty. She always climbs into John’s old truck and tells him to get rid of this green truck and get a real car, but John says that part of loving daddy is loving his truck. Still, she refuses. 

In the end she turned out to be right because last week every single one of his vehicles including his vintage RV all broke down at once. One of them was spraying anti-freeze on him as he drove, one of them was on fire and so forth. Suddenly John was unable to meet his obligations. He couldn’t get her to school, he couldn’t do the job and at one point he Ubered her to school (but he went with her in the car). Eventually had to ask his mom to loan her car for a few days while his mom had to walk to the grocery store. John was covered in failure! 

As John finally got his truck back, the mechanic handed him the keys and told him that he couldn’t fix it, because it was almost spring and John would not need a heater anyway. Now John is driving around in a truck where he continually has to wipe the condensation off the windows because he doesn’t have defrost and his daughter is sitting there with her arms crossed, glaring at him, prooven right and John has no answer! Daddy hangs his head in chains. Fail!

+ John’s little figurines (BM193)

John has a number of little figurines. His daughter found them at one point, played with them, broke one and John was like ”These are daddy’s dolls and you do not play with them” and now daddy’s dolls live in a box up on a high shelf and she looks at them longingly.

+ Conclusion (BM193)

They learned today that alternative family setups are fine and they have learned that John’s is working for them as a result of all of the effort they have put into it. Whether a relationship is on the traditional side or the non-traditional side is, the thing that is true of them is the effort that has to go into it. Good job putting that effort and desire into it! The lesson for John over the years has been that every family is an alternative family and the degree to which you feel protected by marriage or a legal document or a legal arrangement is very fragile. The only thing that is actually lasting and secure is when both people are working hard and share a common goal. It doesn’t matter whether you are married or not. 















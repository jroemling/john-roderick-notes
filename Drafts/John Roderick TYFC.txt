TYFC - Airline Industry
2014-02-19

This week, they talk about: 
* Lex Friedman’s United story
* Jim Dalrymple’s United Story
* John Roderick’s frequent flier experience
* John Roderick’s United Story
* John Roderick’s Jet Blue Story
* Reactions to John’s story
* Confirmation from an insider
* Delta’s In-flight trivia game
* Hacking the airline
* John's own airline

Host: Moisés Chiullan
Panelists: John Roderick, Lex Friedman, Jim Dalrymple, Brent Billings

This episode was originally published in two parts as episodes 2 and 3.

John’s story happened on 2014-01-24:
[https://twitter.com/johnroderick/status/426826389711376385 John’s first tweet]
[https://twitter.com/johnroderick/status/426827372575195136 subsequent tweet]
[https://twitter.com/johnroderick/status/426891046975315968 John agreeing to be on this podcast]
[https://twitter.com/johnroderick/status/426896108053204992 another tweet]
[https://twitter.com/johnroderick/status/427142307708542976 John getting rebooked on Alaska]

+ Lex Friedman’s United story (TYFC)

Lex was supposed to go on a flight from Newark, NJ to Los Angeles at 06:15 in the morning. They had a mechanical failure and needed to get another plane, which was so old that it still had ash-trays in the plane.

The same happened on the way home. He did deliberately not book the red-eye, but he left in the early afternoon to be home at midnight. There was a mechanical failure on that flight as well but this time they could not find another plane. Everybody from the crew just left because they had told the passengers not to ask questions about a rebooking and people were asking anyway, so the crew just left. For about 2 hours the plane was going to come any minute. After 3 hours they said that the flight was cancelled and they would charter a bonus-flight at 06:30 am the next morning. Then they left again. Lex had called United Airlines and arranged for a backup flight and so he took a horrible red-eye that connected through Dulles and got him home at 8am instead of midnight.

Moisés had the same broken-plane-first-thing-in-the-morning problem on his flight to San Diego Comic-con. The whole band Belle and Sebastian with their road crew was standing there and had to get to their gig. 

They joke that John had tweeted something like ”Soviet Style Treatment”?

+ Jim Dalrymple’s United Story (TYFC)

Jim son wanted to go to Austin to see his favorite band who had broken up and got together just to play three shows. While Jim was in London, his son bought tickets to all three shows without telling him that it was in Austin. On the way back they stopped in Chicago just after Christmas and they sat there for an hour while nothing was happening. They didn’t have enough gas to get back to Halifax and sat there for 3 hours while still connected to the doorway.

+ John Roderick’s frequent flier experience (TYFC)

John flies a lot for work and he is on a lot of flights that go well. With every one of them he develops a minor Stockholm syndrome where he feels like he is in favor of this airline, he believes in these people, and we have turned a corner. But time and time again, whatever airline he has chosen as his shining light, may it be Jet Blue, Alaska or some other airline that has changed the game and who are now the heroes, he will experiences some massive customer service failure on his next flight. John is not a monster! He is not somebody with a fake service dog sitting on his lap who wants to throw yoghurt at the stewardess, but he just wants to get from point A to point B without being treated like a cattle on the way to slaughter. 

As part of that, John went through the entire PSA pre-check program and paid $100 to sit in a meeting in a windowless room in the back of the airport where he revealed to them that he is not a spy. They gave him a card that terrifies doormen at clubs because it looks like he is a secret agent, and for a brief 2 months, it was extraordinary: Those were the early days of TSA pre-check and John had this special card that would allow him to just waltz up, the TSA agents would laugh and they would give you a little football-player pad on the ass as they would walk you through a non-operating metal detector. John had joined the ranks of the special people club!

Two months later he showed up for a flight where the TSA pre-check line was full of people in rascal scooters who had never been on an airplane before, who’s pockets were stuffed with Beanie Babies and tissue paper. What happened? They had fucked it up again so fast! There had been this shining orb of possibility, but now they stuffed all the families with 18 kids from the hills of Virginia in this special line as well. Fuck you forever! The TSA pre-check is the new handicap placard of flying! 

A few years ago TSA tried to do this ski slope thing where Green Circle = I’ve never flown before, Blue Square = I might need a little extra time, Black Diamond = Expert traveler, but there was nobody from TSA standing there to help you pick the right line. John would walk over to the black diamond line and he would be behind two little Chinese ladies, one with an artificial hip and one carrying a broad sword. Why are you in this line? Go over to another line! But they are not empowered to do that.

+ John Roderick’s United Story (TYFC)

United has an entire wing of SeaTac as their private [http://www.dictionary.com/browse/fiefdom fiefdom] and John and his family showed up early in the morning at 08:00am. They were headed to San Francisco to catch a connecting flight to the big island of Hawaii where they would stay for a week. John's daughter, her mother and John were going there on a little break in between two work projects. It was tightly scheduled, because John was working the night before and he would be working the day he got back. They had a 2-hour layover in San Francisco and there was plenty of wiggle room on all sides. As they got to the gate, there was no-one at the gate and the sign at the reader board said ”Mechanical problem, flight delayed” with no further indication of anything. 

They had a 2-year old daughter and all the suitcases that go along with having a 2-year old daughter. A pilot happened to walk past right at the moment when John felt like he needed to talk to somebody from United. He asked him if there was somebody from United and got a shrug and this look of fear and helplessness when an actual customer is speaking to him, he had this experience before and he knew it wouldn't end well. He didn't stop moving, he did not even slow down and just said ”I don’t know, maybe try the customer service desk further down the terminal” John went on a walk and he couldn’t see the customer service desk from where he was because it was like 20 gates down.

++ The customer service desk

There was a line of people at the customer service desk and John waited patiently, thinking that there was another flight he could be put on and all he needed to do was to find the person who had the juice. If he found that person right now, he could tell them that he had to catch the flight to Hawaii and there are 20 flights per hour from Seattle to San Francisco on a variety of partner airlines, if they just get him on one of them. John didn’t even care about his Advanced Economy seats and would ride in the fucking toilet if it would get him to San Francisco, because it is a short flight and they just needed to get there to get to Hawaii. 

John couldn’t find anybody. The frustrating thing is that you are waiting in line at this desk behind all these people who are trying to get to Singapore, there is a guy trying to convince the woman that his Bichon Frise is a service dog, and all these people with all these different problems. John waited and could hear the clock ticking in his head, because if he didn't get on a flight, it would be a cascading series of problems. He could solve it if he could just talk to the person with the juice.

This is the first fucked up thing about the modern world: You need to talk to the person with the juice and they need to have sympathy for you. It is not enough to just talk to her, but you have to have her love you a little bit so that she pulls out the special pink key that fits into the slot that is going to give you the upgrade or the little pad that she is not obligated to give you. She does it because you charm her or because she has sympathy for you or whatever. 

John was waiting in this line and he was composing his story about having his kid and having to get to Hawaii. Right as the person in front of him was done, the woman looked at John and told him that she was sorry she had to close customer service because there was a flight coming in and she was the only person who could work that flight. She was already walking and John was calling after her, but she was sorry she had to go. Eventually she picked up her walkie talkie and said ”I have a guy here, his flight is cancelled and he needs to talk to somebody, I guess?” and John heard the voice come back ”The people at the gate are on break and they will be back in a little bit!” John looked at her and she looked back at him. She was the one United employee in the danger zone while everybody else was on break. She saw the worm turn in John and she looked afraid now and said back into the thing ”He really needs somebody here or somewhere to go” and the voice comes back ”Well, they are on break. I don’t know what to tell you!”

++ John becoming a monster

At this point they entered a new place where John could feel that he was becoming a monster. He is a simple person, he understands that there is a mechanical problem, he has no fault with that, but there is no recognition, there is no customer service, there is none! It is no longer a question about bad customer service, but they had arrived at a place where there was no customer service. John asked this woman for anything she could do to give him somewhere to go, while her plane was starting to deboard. She sent him down to Gate 21 where there supposedly were people. John walked down through the airport with steam coming off of him. Gate 21 was down at the end of the concords and there were 3 United employees handling a crowd of 250 people.

John walked over while trying to keep it together and while trying to explain that he needed to get down to San Francisco because there is only one flight to Hawaii a day. The guy looked back at him with the same look of polite hostility and a tinge of fear and said ”I’m sorry, I have to help these people first!” and John was okay with him helping those people, but he needed somebody to help him. In this whole SeaTac airport there is not one spare person? One roamer, one floater? How about the people who were supposed to be at the gate where his flight was supposed to go? John started to yell and the first thing the guy said was ”Do I need to call the police?”

++ Calling the manager

As John turned around, 250 people were looking at him and his inner dialog was ”Oh fucking do call the police, pull me out of this airport screaming and throw me into a jail cell, because I am ready to take one for the team and be on the front page of the newspaper”, but John said ”No, please don’t call the police” in this weird hyper-polite psycho-polite thing and he walked back down again to that one woman who had given him a human look. She called for a manager who was not in an United outfit, but in some Sta-Prest slacks with 7 credentials around her neck. She had this hyper-efficient hostile professionalism, this grimace-like smile like ”How can we help you?”

John said ”I have no problem with your broken plane, although I have to say, largest airline, put a few more planes in circulation!” and in his mind that college-socialist in him was saying ”You know why your plane is broken? Because you are serving your stock holders and your board of directors and you are ruthlessly cutting costs so there is no flexibility and no slack anywhere in your system, because you are trying to raise your stock price. The result of that hyper-capitalist thinking is that your attitude towards your customers is that we are garbage people and if you had one extra plane in circulation and one extra employee in your system, maybe your quarterly earnings would be 1/10th of 1/100th of a percent lower, but you would have somebody here to help me, but because our culture is so sick and your corporate culture is so sick, you perceive your corporate selves as the victim of federal regulation and of fuel costs and all these factors that you want to make the bad guy, but really it is that you are so concerned with profit and with your board of directors and your stock holders that the first people who get sacrificed is me, a guy trying to get to Hawaii with his daughter.” 

And John is not even sympathetic, but he is a white guy which immediately makes him the enemy. This was all happening in his head, looking at this woman in her Sta-Prest slacks. He didn’t even know how to talk to her, because he hated himself, too! He was going to Hawaii with his kid and he didn't deserve it! Instead he should be working in the Uranium mines with the rest of the underclass. Why should anybody care about him? There is no reason! 

But here he was, the guy who was trying to do the thing he had paid the money to go do. He asked this woman why there was nobody at the gate, because he has now been running around this airport for precisely the 1:20 hours that were required to get him on another flight and now it was too late. There was no way they could get him to San Francisco in time and he was not going to get to Hawaii today. He needed to go out to the gate, get his crying daughter, go out to the car and go home and try again tomorrow, which meant his vacation was cut short and his hotel room was paid for and he didn't get to use it, so now he wanted to be compensated somehow. 

John wanted a voucher, an upgrade, he was begging her for something that he didn't even want. He didn't want to have a voucher on their shitty airline, but that was all he could get! She told him that if she would give a voucher to everybody, then she would need to give 200 vouchers away, but John replied that she only had to give one away because he was the only one standing here. He promised not to tell another soul! Her reply was that they don’t do that anymore and if John had flown to San Francisco they could have put him up in a hotel, but they won’t put him up in a hotel in Seattle where he lives. 

She was just giving him this hostile professionalism, ready to call the police. John was standing there, saying that ”You are acting if this was an act of God, like a lightning bolt had hit the airplane, when in fact it is not. It is an act of your corporate malfeasance, or corporate lack of attention, it is corporate lack of care, and now I’m standing here in front of you as the only person who can help me and you are refusing to help me.” At one point he asked ”Is it that you are not empowered to give me a voucher?”, but she was empowered, she was just not going to. 

++ Too late

John was walking out of this airport dragging his daughter behind him like a stuffed animal and he was so defeated. He was powerless to do anything but bitch on Twitter. He couldn't even say that he won’t fly United again, because the world is built in such a way that boycotting United would quintuple the work for himself and he would fuck himself over and over. He would have to wake up every day and review his list of all the corporations he was boycotting who have proven that they don’t give a fuck. It starts with Verizon, AT&T, United, and Delta. Even Jet Blue, his cuddly Jet Blue friend, has given him the same ”Do I need to call the police?” version of customer service in the last year. The defeat in him was utter and he was totally broken, because even this myth of the power of the dollar, that as a consumer his power was where he spent his money, does just not exist! John goes on Travelocity like everybody else and he gets the cheapest flight because he has been conditioned to think that they are all the same, so you get the cheap one. 

In this case, the pièce de résistance was that John’s good friend Ben Gibbard was also on that flight to San Francisco. He was sitting up in first class in a seat made out of human babies, as high up the United Platinum Diamond Gold Unobtainium ladder as you can be, but it didn't matter, because you can’t buy your way out of it either. Unless you can afford to have your own airplane, you have to shrug your shoulders and walk into this crematorium of your soul at some point in your work life. John is not trying to equate his experience to get to Hawaii with the holocaust, but you are so fucked when you wake up in the morning and you have to get to San Francisco today and you are just rolling the dice. You might get there or you might be hauled out of the airport screaming, spittle flying out of your mouth, because you have encountered this American wall of Fuck You!

+ John Roderick’s Jet Blue Story (TYFC)

Jet Blue had been John’s airline for 5 years. They had finally hacked the airline game and they were geniuses, but all it takes is one bad Jet Blue experience: John was in the doorway of a Jet Blue flight, he had paid extra money for the whole first row of the aircraft for his band and their crew, but there was no room in the overhead compartments. Whoever it was who got on the airplane first and was sitting in row 48, they just threw their bags in the front overhead. It is not the flight attendant’s job description to preserve the overhead compartments, and John appreciates that, but they had arrived 6 of them to their very expensive fake first-class seats and they just have normal carry-on luggage, it is not that they are crazy people, but there is nowhere for them to put their bags. The flight attendant just got the same pulling-the-gums-back smile and said ”Do I need to get a gate agent down here to explain to you how airplanes work?” and John was so mad, he is sure his finger prints are still in that armrest. What do you do? Never fly Jet Blue again because of this guy?

+ Reactions to John’s story (TYFC)

The saddest thing was that nobody on the podcast was really surprised. Jim Dalrymple said that while he was listening to John he was shaking his head in agreement, horrified that John had to go through it, but he realized he had times like that, too. More often than not, Jim goes to the airport knowing that he is going to be pissed off whenever he lands wherever he is going.

John is a 45-year old man who survived the irony wars of the 1990s, and maybe it is also because his father fought in WWII, but there is a part of him that presumes a level of dignity. He will not grovel in front of someone who has something he doesn’t even want, and he will not grovel to achieve a thing he has already paid for that they have a contract for. John bought a ticket to get to this place and now they are not doing it and they are being super-silly and incredibly condescending to him as a human being before he even says a word. John can’t live that way! Maybe he is a prideful man, maybe he is from a generation that should not walk the Earth, or maybe they are dinosaurs! 

John really did feel like his story was a management issue at the very heart of United in Seattle. Whoever was running that shop was not running it. There is this affect of powerlessness that they present like ”Nothing can be done about it! The airplane got a flat tire” and there is just an ”Ooops”. John’s rage would not have been rage if he had showed up at the gate and there had been a person there who had said that the flight had been delayed and if John had asked to get on another flight to San Francisco and they would have said ”Sorry, we can’t do it today”, then John would have gone home and he wouldn’t have spent 1,5 hours running up and down the concourse looking for help. It is just a question of one person and that person’s manager and that manager being empowered by her manager. It is a corporate problem from the top down. 

The panelists agree that Virgin Airlines is good, but they don’t fly to a lot of destinations from Seattle. 

+ Confirmation from an insider (TYFC)

Brent confirms that there is absolutely no slack in the system at all. Only 70% of all flights are in time, but the airlines plan their employees like all of them were on time. Everybody is running around like a chicken with their head cut off trying to get everything done. Then there are people on break because the whole industry is made up of unions. A lot of the contract employees don’t have the same benefits and they don’t have comparable wages either, but they are expected to do the same thing as the actual airline employees. 

When you call the airline on the phone, they don’t want to book you to another airline, because they would have to give the money to the other airline and if they can’t help you inside your own airline, they will just hang up the phone and be done. But if you are at the airport, the employee wants to get you out of there, because otherwise they would have to deal with you again later. 

John finds that system crazy because you are down to the whim of that single person you are talking to. Does she want to help you or does she not? Is she getting divorced and hates all men? John doesn’t know! It ultimately comes down to whether someone had their break or their breakfast and whether or not you take the chance to get in their line and whether you are charming enough. That is where it feels Soviet, like waiting in a bread-line in Kiev in 1978 and it is just a question of whether the person likes the cut of your jib.

+ Delta’s In-flight trivia game (TYFC)

Just a few months ago John was on a Delta flight and his in-flight entertainment system would not let him log on to the in-flight trivia game. The in-flight trivia game on Delta is a wonderful time-killer, because you can play against other people on the plane. You can see their seat number, you can smear these other people in a game of high-stakes trivia and when you go back to the bathroom you can check them out and look them up and down while they can see in your eyes that you are totally the guy from 6F. 

Whenever John gets on a Delta-flight he goes right to the trivia game, but in this case it wouldn’t let him on, even after they reset it a bunch of times. The flight attendant invited John back to the galley where the steward told him that he knows it is an inconvenience that he doesn’t have his game and they were willing to give him some frequent flier miles. John said ”Oh really, it sounds like you are willing to negotiate” and he tested which one of the boxes the steward could tick on his remote iPad service machine to get the most frequent flier miles for this inconvenience. 

John has forgotten what all the options were, but ”broken seat” was 5000 bonus miles and they don’t want you to know it, because then everybody were going to have a problem with their seat. That is why he wanted John to come back to the galley to show him that he can do something for him, and he gave John 5000 bonus miles because his trivia game didn’t work. The problem is that if he does that even once a flight for somebody, he is going to get busted, but for whatever reason this was a time when John already had a rapport with him. John recognized that his trivia game was dumb and he was sufficiently self-effacing about the problem, but it was an convivial atmosphere and John got to peak behind the curtain. If he had been mad about this, there was no way he would have gotten 5000 bonus miles. If he had had an actual problem, like a piece of metal sticking him in the back, they might not have given him this opportunity, but because it was a nice day and everything was going good up till them, John saw this other side. 

+ Hacking the airline (TYFC3)

* If you call them on the phone, they will add notes to your record, but at the airport they might only put a note on this flight, but it will probably not follow you around.
* Is the person you are talking to trying to be helpful? Step back, think, is the person really going to help and is being upset about it going to get you what you want? Sometimes it does. Most of the time it doesn’t. They plan for everything to be on time and perfect.
* If the line is 50 people long, there are two agents working the gate and they are the only two people who might be able to rebook you, if you are not the first two or three people in that line, you are probably better off to get on the phone to get rebooked. 
* The people on the phone will try to rebook you on the same airline, while the people at the airport might rebook you on a different airline.
* At Delta, you can be booked on an IROP Protect, Irregular Operation protection flight, in case your flight gets cancelled. If your flight is accidentally not marked as delayed, then you really need to have somebody who knows what they are doing and most of the time, people are only trained for their one job.
* For a while, Twitter was a novelty thing and the media was writing stories about it, so Delta has a Twitter account and they save the day by helping somebody out and all of a sudden they get articles written about it all across the nation and they get a few extra customer service points, but it might have grown to the point where too many people are doing that and it is not as useful as it used to be.

+ John’s own airline (TYFC)

John looks at the airline industry as an armchair quarterback and can think of a dozen solutions to these problems, but it would involve a top-down reinvention. John is not in charge of an airline and they are not listening to him. He and Jim need to start their own fucking airline, and that is what they should call it ”Our Own Fucking Airline”, OOFA. You can smoke with kids on your lap and the kids can smoke, too, like in the old days. They will have silverware, they will serve you Chicken Kiev, they will give you a deck of cards when you ask, and it sounds like an airline that only flies between Cuba, Russia and Venezuela. 
















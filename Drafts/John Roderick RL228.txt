RL228 - Hidey Santa
2016-12-19

This week, Merlin and John talk about:
* Bob Ross ([[[Factoids]]])
* Alaskan paintings ([[[Factoids]]])
* Garbage truck routes in John’s neighborhood ([[[Factoids]]])
* The garbage man who collected Polaroid sex pictures ([[[Stories]]])
* Leaving a six-pack of beer for your garbageman ([[[Stories]]])
* John disposing of 30 garbage bag ([[[Stories]]])
* John repeating himself ([[[Aging]]])
* Cuban secret service agents leaving $100 bills ([[[Stories]]])
* Elon Musk vs boring billionaires ([[[Attitude and Opinion]]])
* Uber requesting unlimited location access ([[[Technology]]])
* Direction-changing express lanes ([[[Factoids]]])
* Traffic lights on the highway past Boeing field ([[[Factoids]]])
* Alaska Airlines and Virgin America merging ([[[Flying]]])
* John’s notebook with comics from High School ([[[Early Days]]])

**The problem:** More of a Platonic Elon Musk, referring to John expressing positive things about the platonic idea of Elon Musk, a billionaire who does interesting things.

The show title refers to John hiding in the bushes like Santa after having left a six-pack of beer in the garbage can as a gift for the garbage man.

The show art is the first search result of ”gold pan paintings bod ross”

It is complicated. 

John is switching headphones, because his headphone jack is malfunctioning and he is hearing Merlin only in one ear. This is what it must feel like to be Brian Wilson. 

John feels like he has filled his living room with sand full of cat shit. It is almost a form of deaugmented reality, or dogmented reality. 

John is broadcasting live from Venice, California //(from Millennial Girlfriend’s place))//. He cannot see the beach, but he can see Russia from there. It is not because Alaska is close to Russia, but all Alaskans can see Russia from wherever they are. It is more Heisenbergian, because Russia is there by virtue of our observation rather than their being a Russia really, more like a Quantum observation. John observes Russia and then [[[spooky action at a distance |spookily at a distance]]] other Alaskans also observe Russia. If everybody in Alaska lined up at the same time, it would be like Hands Across America, but for vision or politics. 

There are a lot of sirens coming that sound like police sirens to Merlin. He is usually very good at identifying vehicles and he knows which delivery service is coming by the sound of their truck. Postal Service trucks have a very distinct cadence to them. 

During the show the garbage truck stopped out front with squeaky hydraulic brakes.

+ Bob Ross (RL228)

There is a full season of Bob Ross on Netflix, a very interesting guy who is strangely relaxing and fascinating to watch. It is not just mesmerizing over time, but it is instantly mesmerizing. John can’t see how you can be an American person and not have watched that show. A lot of his inspiration came from one person who had popularized the wet-on-wet painting technique, but they eventually had a big fracture in their relationship. The way he paints and the kinds of things he does are owed to being stationed in Alaska when he was in the service. Normally when you are painting in layers you would wait for one layer to dry before you put any more on, but Bob Ross uses a wet canvas and wet paint. He doesn’t call it a mistake, but a happy accident.

+ Alaskan paintings (RL228)

The wast majority of art in Alaska is kitsch. All Alaskan paintings are landscape paintings with the Northern Lights at night time, snow, a log cache and some happy trees. They are all done in a very similar style, often on gold pans, like a Sutter Creek pan. Gold pans are a canvas in Alaskan culture upon all manner of dreams are projected. There is a very good chance you will find a gold pan with a painting on it in middle class homes in Alaska. These paintings vary in size and even in really nice houses that are really nicely decorated in every other way you will still find a gold pan with a scene of the Northern Lights and a little cache and some snow. Even the finest Jacob Lawrence paintings of Alaska that sell for hundreds of thousands of dollars are landscape paintings, but they don’t necessarily have the Northern Lights in them. In the highest echelon of Alaskan painting you are allowed to not include the Northern Lights. 

This is true for all Alaskan culture: The songs that come out of Alaska, including John’s cousin’s albums all feature Alaska quite prominently. The writing that comes out of Alaska is centered largely on Alaska or takes place in Alaska. This encompassment of Alaska produces Alaskana, the primary form of cultural production in Alaska. It makes sense that Bob Ross is from Alaska. The reproducableness of it, the way these landscapes come together, all he needs are a little bit of Northern Lights and such a painting [https://www.worthpoint.com/worthopedia/bob-ross-painted-gold-pan-aurora-160230879 does actually exist]. 

+ Garbage truck routes in John’s neighborhood (RL228)

The garbage men navigate the streets in John's neighborhood in a very curious pattern because many of them are dead ends. They pull a little T-maneuver, turn their trucks and back down the streets that are blocked off so that they are able to drive out of there forwards. This means that more than half the time you can hear the beeping sound of a backing truck at the crack of dawn on Fridays. It is a real symphony of beeps, rattles, squeaky brakes and diesel motors. John has tried to map the route of the garbage trucks because emptying the garbage of a single neighborhood is part of a larger system and John became curious as to whether it was systematized across the whole city or if it is like Anna Karenina and every neighborhood is de-garbaged in a different way. 

When John was running for city council, one of the questions he asked his team was if he would have the authority to go down to the sanitation department and get a report from everybody about what is going on. John’s team looked at each other, nodded slowly and told John that he would be able to do that. He would scare a lot of people because they wouldn’t know why he would be there, but he would explain it. John would go to all these agencies that perform all these marvelous tasks and get a report. He is not going to tell them how to do their job, but he has some questions, for example how the garbage trucks are run. He doesn't want to wait in the lobby as a lay person with his hat in his hand for some press secretary to come out and say ”What can we do for you now?”, but if he had shown up as a city councilman with his little entourage, imagine the PowerPoint he would have gotten to see!

John started watching what the garbage trucks were doing and it is quite an elaborate Pas de Deux, a little bit of a Swan Lake. They are running a lot of redundant trips down the street. One truck goes past John’s house first, then he hears it beeping, then another garbage trucks goes by on a neighboring street, then he hears that one beeping and he hears the first truck beeping again, and so on. John would think that there would be a route around this neighborhood like an S that wouldn’t require two garbage trucks making multiple passes before they arrive at their station, but John is not usually up at 6am or 7am chasing them around with a clipboard. 

+ The garbage man who collected Polaroid sex pictures (RL228)
//(see also story in RW81)//

Nick Harmer, the bass player of Death Cab for Cuties had worked as a garbage man in Tacoma’s Hilltop neighborhood and he had given John a lot of garbage man inside knowledge. He told a story of an old garbage man in Tacoma who had a very large collection of discarded Polaroid sex pictures. They move pretty fast, but somehow they must be visually sorting garbage as well. John wants to be a garbage man for that reason, but it probably only becomes meaningful if you have been a garbage man for 20 years. 

According to Nick, this guy carried his collection of multiple photo albums full of these Polaroids in the garbage truck. A Polaroid is not a very good picture, especially taken in a house at night. All these pictures were from Tacoma and there would be a lot of through-threads, a lot of red garter belts. It was a collection he was very proud of and it made John think a lot about garbage men and what they are doing. John has another friend who collects Polaroids that were taken in prisons in the 1970s and his collection is really beautiful. There are other people online who are doing the same and he often gets into bidding wars.

+ Leaving a six-pack of beer for your garbageman (RL228)
 
Nick Harmer, the bass player of Death Cab for Cuties who had worked as a garbageman before told John that on a hot summer day it is a good idea to fill the top of your garbage can with ice and put a six-pack of beer in a little ice nest inside the garbage can, so when the garbage man lifts the lid off, here is their icy beer moment of the day. 

One hot summer day John was out in his yard and remembered this conversation. Because he was up early and had some beer in the house that somebody had left, he made a little ice nest and was so proud of his beer gift. When he heard the garbage men rattle and beeping, he hid behind a bush like a Hidey Santa. Finally the garbageman came, lifted the lid off the can, looked at the ice nest of beer, carefully put the six-pack on the ground, emptied the garbage can and drove off. 

Maybe he didn’t want any beer, but John is not exactly sure why he didn’t take it. Maybe he thought it was a test, maybe he is not allowed to take it?  John had done this thing and it got rejected, but he cannot know without having gone to the sanitation department as a city council man and gotten a full report. Governor Bob Graham of Florida was famous for going out part of the day and work at all kinds of different jobs. That would have been right up John’s alley, he would have done ride-alongs with everybody, with the line-men and with the garbage people. John took his unwanted beer back into his pantry because he didn’t want it either. Maybe they just didn’t like Tecate? 


+ John disposing of 30 garbage bag (RL228)

One of the greatest experiences John ever had with the garbage truck was right when he moved into his house. He had cleaned out all this garbage from the house and had produced a mountain of 30 black garbage bags, so he went to the city websites and read all about it. The city has very clear rules about overages. You get 2 garbage bags as part of your weekly thing and each additional garbage bag is $80. 

John was very anxious because the garbage man was about to arrive and would be counting up in increments of 80 how much John was going to owe him. It is all part of the system. It would have been a lot of money even though it was not exactly $80, but it would have been cheaper to have a hauler person come out. He had just cleaned out the house and this was all very fresh, so John made a point of standing next to his pile at 7am in the morning, he had even dressed because he wanted to make a good presentation and wanted his garbage person to like him. 

John was standing out there waiting for the garbage truck in a suit with a bucket of flowers. The guy pulls up, gets out, recognizes that John wanted to talk to him because he was standing there next to a mountain of garbage bags. John said ”I have 30 garbage bags and I want to talk to you about them” and the guy said ”Give me $40” John said ”Sold!” and the two of them threw 30 garbage bags in the back of his truck and off he went. Everybody won! There is enough autonomy and opportunity for somebody to wet their beak a little. 

+ John repeating himself (RL228)

John never wanted to be one of those middle-aged people who was repeating themselves all the time. Ever since he started taking his Bipolar medicine it was reported to him that his memory had suffered a little bit. John is sure that it wasn’t like that before, because while they might have repeated a couple of stories during the couple of hundred episodes of this program, John generally had a sense whether or not a story had been told before or not. Merlin does find John’s stories very interesting and he often tries to cue John if it is an anecdote that he remembers very clearly. 

Lately John felt like there had been a couple of instances where they had talked merrily about a thing and the response on the Internet was that everybody had heard that story before. Of course [http://twitter.com/capnmariam Captn Mariam] told him afterwards that this had appeared in episode 80 and 140. Merlin even said exactly the same thing afterwards, which doesn’t surprise Merlin at all. 

John is proud of the fact that in most cases nobody says that the story was a lot different the second time. Part of that is to confirm that some of the fantastical stories are actually true, because it would be impossible to remember a lie. There are still occasional listeners who think that John’s stories are partly or fully fictional, which needles him a little bit. This isn’t one of these podcasts where people are talking about supernatural towns, but this is a a true tales podcast from the Wild West. 

+ Cuban secret service agents leaving $100 bills (RL228)

Merlin’s grandfather had a system: He always put a quarter on the floor in plain sight and if that quarter was gone, he knew the place was crooked because they stole his quarter. That was his quarter in the coal-mine. There are all these spy systems like putting a piece of paper on the floor.

The Cuban secret service would sometimes put $100 bill under the table cloth in someone’s home who would then inexplicably find this life-changing amount of money. They would know not to take it because it meant ”We are in your home, but not only that: Fuck You! We are so in your home that we can leave this here and we know you can’t touch it” They would put their table cloth back down knowing that there is this enormous amount of money under the table cloth that they just had to leave there until it was gone. 

According to legend, the Cuban secret service did quite a bit less of the standing out in Gorkey Park, rolling a microfiche and sticking it into cigarette filters, but they were mostly internal and were really inside people’s heads. It sounds a bit like the Stasi: You want people to be policing themselves and put fear into them so you don’t have to do it yourselves. 

John’s friend is a journalist and was telling this story as a prelude to his own story: He was out in the far country with a group of people and he got asked if he had any kids. He pulled a picture of his daughter out of his wallet and passed it around in the room, but it never came back to him. As it was time to leave, he asked for the picture, but nobody had it. It was a finite amount of people in the room and everybody feigned complete ignorance. Nobody was the last person to have seen the photo. 

His escorts, who he presumed were also secret agents, hustled him into his van because it was time to go and the picture was gone. 6 moths later he was back in America and one day the photograph appeared in his apartment. He told this story as though it is God’s honest truth. It seems like a lot of work to accomplish a very inscrutable goal. Obviously they are saying to not fuck with the Cubans, but he is not doing that, he is back in Seattle. Maybe they are telling him to be careful what he publishes about Cuba? It was super-spooky! When he told that story it was a little chilling. 

All the other people in the room who asked to see a picture of his daughter were all normal citizens and someone must have gone around and told them what to do in advance of his trip. It was a casual-enough exchange that it made it feel like someone was improvising, but somebody was to grab the picture and everyone in the room was going to deny any knowledge of it. It is very deep and very dark and it expresses a lot of confidence that control is total. 

Merlin is a little doubtful because you would normally want to protect your methods and not let the other person know that there is a double agent or something. It begs the question if the Cuban secret service has an office of secret agents in Seattle and if so, why? If not, did they fly an agent there to perform this weird mission? Or was there a Cuban secret agent who was in Seattle for other business? Of all the things to do, how did that make it to the top of anyone’s pile?

It is a little bit like Ceaușescu's [http://articles.latimes.com/1989-12-27/news/mn-1096_1_drama-students apples in the pine trees]: Within a totalitarian closed circle people become so detached from reality that completely insignificant things like that take on an added importance. At the time Cuba was still closed to Americans and John’s friend was one of the few American journalists who was in Cuba in an authorized way. Maybe they had the mental bandwidth to do this kind of thing to the 50 American journalists that had come to Cuba that year? Maybe it was a manageable number? Within totalitarian thought loops, who knows? 

It may be a jobs program, like a WPA for spying! In China there are Army people everywhere. They are directing traffic, they are standing around in front of people, they are opening doors for people, they are out changing tires on the Highway. They have the largest Army in the world and what are these people doing all day? They can’t just be on maneuvers. They have a million man army and 900.000 of them are out directing traffic. Maybe the Cuban secret service is out sneaking around turning off people’s drippy water faucets and stuff or changing burned-out light bulbs in stairwells. 

Merlin says there is a thing called Pronoia, the opposite of Paranoia. Paranoia is the idea that forces are out there to get you and cause bad things for you, while Pronoia is the hippie-idea that the world is conspiring to make your life great. It would be fun to have the resources to do more things like that, like put a nickel ($0.05) into your parking meter, or maybe they trim your hedge in a way you hadn’t even thought of? You come home and some kind of CIA Edward Scissorhands created a topiary dinosaur in your front yard. That would also communicate to you that the government had amazing powers and is watching you at all times. If you are going to be creepy, why not make it nice for you? They could use the information about you in a nice way. Maybe somebody would change the batteries in your smoke detector that had been beeping for 1,5 years. It would be proactive positive gaslighting and falls under the rubric of Elon Musk. 

+ Elon Musk vs boring billionaires (RL228)

Elon Musk came up in conversation at a cocktail party last night. John said that Musk is kind of a muse for him and he thinks about him as the platonic idea of the modern tech billionaire. A person from San Francisco, who was a little bit of an insider, expressed the contra-view that Elon Musk is a sinister figure and he couldn't understand the current Elon Musk worship. 

Everybody in the room had to defend their admiration for Elon Musk a little bit and when it was John’s turn, he was not saying he likes Elon himself, but the platonic idea of him: There are so many billionaires who are not building their vaguely sinister space exploration programs and who are not planning to build a hyperspace-train from Los Angeles to San Francisco, but those billionaires are boring. If John was a billionaire he would do some crazy shit like that and he would of course look sinister. If John would hire a secret army to go around and change the battery in everybody’s smoke detector, people would wonder what this guy was doing! 

+ Uber requesting unlimited location access (RL228)

A couple of days ago John got off of the airplane and turned the Uber app on. Although he hates this company, they are doing a much better job than everybody else. Every time John thinks that he is going to protest Uber and call a yellow cab, he has an experience that sends him screaming back to Uber. There is a small contingent of London Taxi drivers listening to this program and every time John talks disparagingly about that community, the London Taxi drivers union of podcast listeners responds very aggressively to say that Uber is a bag thing. 

London cab drivers are a professional gild that is an utterly different class. They have to pass rigorous exams and they have a lot of institutional pride, which is not true of Yellow Cab drivers in Seattle and Los Angeles. John's Uber app reported to him that they like access to his location prior to them picking him up and after picking him up. They would like unlimited access to his location at their discretion to see where he is going afterwards and to know whether or not they are doing a good enough job or not. 

John’s first reaction was to deny the access and they told him that he then has to manually type in the address of his destination and the app was no longer giving him the functionality to store where he regularly went. Now he needs to take out a quill pen and write down the address where he was going and send a scented letter by carrier hawk. They are one of these tech companies who have decided that all the information belongs to them. Maybe the whole idea of Uber has been a ruse of eventually getting us all into a position where they can collect our locations and they would constantly be apprised of our locations.

Unlike a lot of companies who hire lawyers to make sure that the company is compliance with the law, some of these tech companies hire lawyers with the full knowledge that they intend to break the law and their lawyers are trying to help them break the law in the most efficient way possible. They know that they are going to be sued and they are covering their tracks. The lawyers are not in service of protecting the law or the consumer, but of protecting the companies. That has always been true of oil company lawyers and tobacco lawyers, but wanting continuous access to your location is a little bit different. It suggests a minority report future where billboards can address you by name when you walk through a mall.

When John got this pop-up, he had to stop, put on the breaks and dig his heels in. He had to think about what he was going to do because they had disrupted his workflow. Is he going to submit to this indignity in order to use this thing that is more functional than other systems, or is he going to make a pyrrhic stand and be Sisyphus insisting on a body pat-down at TSA rather than to go through their particle accelerator. 

Eventually he did what we all do when another eel attaches itself to us, and went ”Okay, Uber!” He turned on his location services, the app was happy and everything was back to normal, but all of a sudden it didn’t recognize the credit information that he had put in, so now he was stopped again to restart the app and make it remember his credit card. For a moment it was a little bit of a $100 bill under the table cloth: ”Remember when you initially clicked No? We are going to forget your credit card for a couple of minutes!” and when the app finally worked he was fucking grateful. The driver was going to be there at LAX in 16 minutes, which was a double little kick in the drawers. 

For the first time in many years of getting a car from LAX, the driver didn’t speak English at all. When John had clicked ”No”, he got shunted over onto the couch in the good fraternity at Animal House, they sent him the Armenian driver, and they forget his credit card for a minute. John was fuming and wondered what would happen if he turned his location services back off again? What are they going to do about it? They already sent him the Armenian driver! 

They puttered along to John’s destination and as they neared, John noticed that the screen of the driver was not continually updated and the blue line directing him just continued off into space. He was going to continue driving! Because he didn’t speak English, John wasn’t able to express all the nuances of this situation. As they got near John’s location he just asked him to pull over right here. 

John had tried that with Uber drivers in the past and they said that they had too much pride in their job to just drop him off near his location. They insisted to take him directly to his house, which was a tremendous inconvenience because he could be there in 3 minutes by jumping over this fence and cutting through this alley way. In the meantime John understood that Uber is telling their drivers to drop people off as close to their door as possible. It is all about the data! They don’t earn money on the rides, but they just want to get into your bed! 

John is going to call an Uber today, but he is turning those location services on only immediately before calling up the app. Is Uber going to remember? Are they going to punish John again? To whatever degree gaming the system is still an option, we should do it! We should be constantly turning on and off our location services only as needed and we should be deleting cookies. Now John is starting to become a little bit of an Internet paranoiac, the remote-control-in-the-plastic-bag-type. He doesn’t know why Instagram needs his location and he is going to presume that they don’t. 

John is not afraid of anything, but it is more like "Fuck You!" These companies are trying to profit from gathering his information and whoever the Uber CEO is, John doesn’t like him already, because he doesn’t have a space program. If the Uber CEO was worth a shit, John would know about him because he would be making solar-powered airplanes. John doesn’t want him to profit from knowing exactly into which door from the movie Help he is walking through. He wants him to have to work a little bit harder, because he is not using his money in a creative way. 

Merlin complains about the taxi situation in San Francisco being a joke. It would frequently take them 1 to infinite hours to get a taxi. There were a lot of date nights when they were sitting on the front steps, paying for a babysitter while they couldn’t get a taxi for 1-3 hours. They continue the discussion about this topic.

+ Direction-changing express lanes (RL228)

Some cities have express lane that are heading in different directions in the morning and the afternoon. John has read in his research about city systems that people were writing in that on the night when there are both a big football game and a big baseball game, they should acknowledge this difference and change the direction of traffic on that particular day. 

The city replied that even accounting for the large number of sports people headed into the city, there is still more traffic headed out of the city than there is coming in and it would make no sense to change the express lanes. This is one example of the city collecting information over a long period of time. It made him really want to go to that room in the city where they are all sitting at a giant map, listening to the computer as it calculates every car on the road and all the crazy traffic signals.

+ Traffic lights on the highway past Boeing field (RL228)

John is driving past Boeing Field airport multiple times a week. They have their own traffic lights because they have a lot of leverage. There are a lot of factories on this strip where they are manufacturing and smoothing out the wrinkles on their Boeing aircraft and at some times of day, these factories disgorge thousands of engineers all at the same moment, which is a thing that needs its own traffic light. There are about 5 locations on this 10 mile mostly completely straight road where there are traffic lights that make no sense at all unless you are in the parking lot of a factory and there are 4000 engineers all leaving at once. 

John is offended by these traffic lights because there is no reason for them except to benefit a situation that happens 3 times a day. Insanely they also trigger at 2am and stay red for 5-6 minutes as though 1000 engineers in their carefully maintained Chrysler K-cars were just pouring out of this parking lot, while you are sitting there on this 4-lane Highway like a total cack next to this jewel-in-the-crown-level airport. For many years John would stop at this light disgustedly, look both ways and make an independent decision that he was not going to sit there in the dark waiting for this fake traffic lights. 

John doesn’t understand why this thing is not programed to stay green until 4:45pm, but then he realized that this is just a system of command and control. They want to stop you because they can and because they want to remind you that you can’t take the $100 bill. John will make a courtesy stop at this traffic light, but then treat it as a yield sign and who knows if this is going to make it into John’s permanent file or not. Any amount of this kind of civic disobedience might have repercussions! He might log into Facebook and be reading his friend’s account of their recent surgery and some banner ad will say ”Don’t like stopping at stop lights? Maybe you should try checking yourself before you wreck yourself!”

+ Alaska Airlines and Virgin America merging (RL228)

John doesn’t usually look at the Internet while he is talking to Merlin because talking to Merlin takes his full concentration, but because he is going on a plane this afternoon he was quickly checking his email to make sure that they weren’t telling him they had spilled a bunch of avgas on the tarmac. He got two emails back to back, one of them from Virgin America, an airline who recently spilled a cup of coffee on John, about how you can get points when flying with Alaska Air. Merlin got the same one. In the email there was both John’s elevate frequent flier number and his Alaskan frequent flier number. 

The very next email in the inbox was from Delta Airlines, saying that their partnership with Alaska Airlines is ending and that they are now going to war with one another when it comes to traveling from SeaTac. The writing has been on the wall! How is John now going to manage his frequent flier miles now? Jason Finn has been trying to get John be a gold member for a long time, but at every attempt John makes he just gets thwarted.

When John was a kid, the tail of Alaska Airlines had multiple figures on them: There was a totem pole and a little Eskimo girl, there were a lot of things, not just one symbol. Over time they phased out all of them and made that one Eskimo guy the face of Alaska airlines. He is the Colonel Sanders of Alaska Airlines! He became more and more stylized and more and more like a cartoon and a lot people who have never seen someone in an Eskimo parka might not even know what they are looking at. His hood might not even read as fur and it is not clear that he is wearing a fur parka.

+ John’s notebook with comics from High School (RL228)

//(During this story, John quickly refers to another story where a room mate stole some of John’s journals as part of their break-up.)//

When John was a kid he thought that he was going to be a cartoonist. He spent a lot of time drawing and it was his #1 activity in school instead of paying attention. He never had a stock cast of people, but it was more like Bizarro by Piraro, not Farside-y, because they had a lot of reoccurring chickens and cows and stuff. It was always one panel with a couple of people interacting. John drew proto-pretty-good comics and he filled an entire artist-notebooks with these cartoons. 

The challenge was to come up with ideas, and then he would run into drawing problems, which is how you can improve: You try to solve for how to draw a hand or how to draw a 3/4 view. It is easier to draw cars, but you still can’t draw a car from memory. You have to study them, because they don’t look like you think they look. John filled up this notebook all the way through High School with these pretty and sometimes hilarious one-panel drawings and at graduation he gave it to his long-time high school girlfriend, who wasn’t his girlfriend at the time, but he felt a lasting, life-long bond with her. She was very appreciative of this book because she had been a fan when he had shown them to her many times. 

After John left Anchorage he hitchhiked across the country and was keeping a journal. Throughout that journal he included some caricatures and some drawings when was trying to describe something he was seeing. It gradually went away, though, partly because he was no longer spending a lot of time sitting at a table where he could have drawn and it was hard to draw on busses or to make the time to stop and do it because John was always in motion. He lost the habit of sketching. 

About 10 years later John was with his high school girlfriend and brought up the book of comics. He assumed it was certainly in a box somewhere and he said it would mean a lot to him to see it, interact with it and have it back, but she said that the book belonged to her because he gave it to her. She gave every impression that she occasionally referred to it and he was not getting it back. 

John wanted to be a comic artist and one-panel New-Yorker style comics were a very high art form. The truly great ones rose to the level of the truly great and John wanted to be one of those. His drawing skills topped out at least for the little amount of work he put into it. He came to a place where he could draw his thoughts, but not draw them beautifully. You see a lot of people in that realm where the art is rough. It is like a short story and you can communicate an awful lot with it which makes New Yorker cartoons so difficult for people. They often presume a body of knowledge going in. 

The art has enough visual clues in it that tells you who these people are, where they are coming from and what their past experience is, which is how you interpret the line. You have to understand that this is an Upper-Westside couple where he is a psychologist and she is an art director. You have to peer into their lives together and understand what it means when he says ”There is not enough almond milk in the refrigerator” and why it is funny. In trying to occasionally eat vegetarian breakfast John has been noticing that the only thing not made of almonds is the spoon. Even the bowl is made of hollowed-out almonds. Almonds are sources of protein and apparently you can milk an almond, like free-range almonds. Merlin knows how to make almond milk, you take water, almonds and lemons. 
















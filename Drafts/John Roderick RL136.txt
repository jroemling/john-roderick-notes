RL136 - Goofus MacGoofus
2014-12-28

This week, Merlin and John talk about:
* John’s story of chili at Mount Alyeska ([[[Food and Drink]]])
* Macklemore haircuts ([[[Style]]])
* The Panman ([[[Style]]])
* Merlin started using hot sauce ([[[Food and Drink]]])
* John’s introduction to Pho ([[[Food and Drink]]])
* Cassette tapes of John’s dad ([[[Parents]]])
* Kiss ([[[Music]]])
* New year’s resolutions ([[[Currents]]])

**The Problem:** Beans are a secret vegetable, referring to when John was a kid, he didn’t like chili because it had beans in it which were are a secret vegetable.

The show title refers to John making the new years resolution to not direct his energy into the direction of being a talk show host or a feature writer or a dumb comedy persona or a Goofus MacGroofus or a Dingbat McDonnahein, but to making music.

What a day! John has not had any food today, but he is subsisting entirely on pure energy. Merlin is a mess! Did he have some food? Did he go down to the Taco Bell and get a couple of hot dogs? Woe betide me on the day they start having hot dogs at the Bell! They could just add one more ingredient: Hot dogs! They could chop them into disks and make it into a famous bowl. They could cut a hot dog length-wise in half and make it a hot-dog-o (from hot dog taco). It is a thought technology!

John likes hot dogs. Merlin goes through phases at their house where they forget about hot dogs for six months and then they get a couple of packs of Nathan’s and they go crazy on hot dogs for a while. John has been pretty happy with Jumbo Nathan’s with only 5 in a pack. 

+ John’s story of chili at Mount Alyeska (RL136)

John didn’t like chili when he was a kid because it had beans in it and he didn’t like vegetables and beans are a secret vegetable. John was an extremely picky eater, he hardly ate anything and one of the things he didn’t eat was chili. He was revolted by chili! John was a member of the Junior Racers Ski Team at Mount Alyeska and on top of the mountain there was a structure that was the top of the original chair lift where it would rotate around its hub underneath a building called the Round House. It wasn’t actually round, but it was an octagon, but it was effectively round. 

The top floor of this building was the place you would go to get a hot chocolate and there was a bar where you could get some hot drinks. Some time in the late 1960s they added a big dining area at the back of the round house that was all shingled in cedar shingles including the roof. The furniture inside was heavy wood picnic table style of furniture, in the bar they might have had tables and chairs, but in the round house where the kids could go it was just big picnic tables. 

One of the foods they offered at the Round House was chili, chili burgers and chili dogs. You could also get a hamburger, a cheeseburger, a hot dog, or fries which John also didn’t eat because potatoes were a vegetable, too. Potatoes and beans also share a mealy texture and John doesn’t like mealy. He associates it with a certain kind of starchy vegetable of which beans and potatoes were the exemplars.

John would go there with his friends every day at lunch time. They would ski up, kick their skis off, clomp into the Round House, up some stairs and onto the porch on the outside. John would eat his cheeseburger or his hot dog while his friends ate their chili cheeseburger or their chili dog. The Round House served these foods on giant platters and you got a mountain of chili and couldn’t see the cheeseburger or the hot dog under it. 

John sat across from this day after day throughout his entire childhood, but at a certain point in his adult life he realized that he had been a fool. In addition to all the other new foods he was trying and learning to like he tried chili for the first time and realized that chili is the perfect food. It has all the ingredients: It has beans, it has onions, and it has chili powder.

At that time John was not a kid anymore and he was not living in Alaska anymore. He could not go back in time, there was no going home again, and he could not have all these chili burgers that he had failed to have. He had blown it! When he finally did go back to the Round House as an adult and ordered a chili burger, it was a new recipe, there wasn’t enough chili, and it wasn’t the same. 

The resort had been purchased by a Japanese conglomerate, they built a new hotel and a new Round House, and even if the Round House itself may still sit there, the dining room is a giant poured concrete area with lots of Coke products and branding and all kinds of foods. It was gone! The smell of cedar, the cigarette smoke, the chili burgers were gone! Because of that situation John spent the rest of his life trying to eat every chili burger he could, trying to fill that chili burger sized hole in his heart. 

Merlin hates it when stuff changes. John wishes that he had changed and the chili burger had stayed the same. This is like a Harry Chapin song. The cats in the chiliburger and the silver spoon. You couldn’t see the chili for the beans. My boy was just like me... Talk about memory!

Merlin grew up in Cincinnati where they have weird-ass chili with cinnamon in it and that you can get in different ways: 3-way is Spaghetti, chili and cheese, 4-way is with onions and 5-way is for grown-ups with beans on top of the chili because Cincinnati chili doesn’t have beans. It comes on a plate and says Skyline chili or you could go to Empress. The whole thing was $1.35. Both John and Merlin really like Cincinnati style chili.

John buys these giant Nathan’s Famous Hot Dogs and chili is exponentially improved if it is poured over a hot dog or over a hamburger. John is trying to get back to the Round House in Girdwood and their chili dog burger is his Madeline and the Tea, it is his Rosebud, and he is always trying to make it. He eats taco dogs with chili and covers the whole plate with chili so you can’t see the food underneath it. Merlin buries it like a political prisoner and you can’t even see what he is covering with chili. 

In the contemporary Wolf Chili era Merlin just takes whatever they got in the fridge: rice, noodles, or whatever, and he is two minutes away from pleasure country. John recommends Merlin to throw in half a pound (220g) of frozen chopped kale. It thickens it, it darkens the taste a little bit, and using the inescapable logic you are now also eating super-health food. You are eating half a pound of kale but you don’t even notice it because you are busy smothering this chili dog, waterboarding this hot dog with chili. Merlin says that chili is a pivot. You can put it on almost anything and it will be better. The other nice thing is that you can add stuff to your chili, like a little bit of left-over ribeye and get into flavor country. 

John had several cans of Heinz Classic English Beans in his cupboard that somebody had given him. Their uniformly grey-colored English breakfast always has some hot beans in ketchup, a grey slice of tomato, some blood-sausage if you are being fancy, some eggs, some bacon and the other kind of sausage to go with the blood-sausage. John had those beans in his cupboard because sometimes he does crave a classic English breakfast. Merlin will get a Full Irish at a place that he and his daughter simply refer to as Irish Breakfast. They used to have the mixed grill but they discontinued it. Part of Merlin wonders if beans are almost like the British version of macaroni and cheese, a cheap childhood comfort food. It is also a vegetable. 

The other day John was making some chili and wondered what would happen if he put Heinz beans in his chili because he had already put some other cans of beans in there and if he can put kale in there, he can put some fucking Heinz Beans in there. They are white beans, a different kind bean, a whiter kind of bean, which is John’s favorite Procol Harum song, and all of a sudden the chili just lifts off into this other realm. It became a kind of chili that he wanted to put in front of someone in a bowl just to register their surprise and delight. There were 40 different realities in there! You think you’ve had food? Try this! Welcome to chili town, which is John’s favorite Dan Harmon show.

+ Macklemore haircuts (RL136)

Macklemore haircuts are proliferating like dandelion seeds now, just since they talked about it on the last episode, Merlin has seen so many more and he sees them anywhere. 

+ The Panman (RL136)

Every man has a little bit of Panman in him, but you can’t have a Panman without being a man. There might be Panladies, but canonically it is a man. The prime examples of a Panman are Sammy Hagar and it is Guy Fieri. You should have a very silly goatee that is maybe a little long, you should have some kind of stupid hair, maybe frosted, maybe ginger ringlets, maybe spiky highlights, but some kind of fucked-up hair. It could involve Hawaiian shirts, jam shorts, and flip flops and they can't have it hot enough. 

Asking them if they want jalapeños with that is a Panman touring test and the Panman will always say ”Fuck yeah!” He will call you ”dog” or ”bro”. In a way George W Bush is a little bit closer to Panman because of his habit of nicknaming everybody. They wear wrap-around mirrored sunglasses on one of those douchy froggy things. Anytime you wear your sunglasses on the back of your neck you are very close to Panman. John would say there are no ectomorphic Panmen, but they are typically endomorphs, maybe there are some mesomorphs, but most of them are endomorphs. 

A Panman has a lot of body hair and feels connected to the spirit of having fun and living in the now. Matthew McConaughey is by all appearances a fairly normal human, but he had a Panman personality transplant and is a Panman living inside the body of a normal man, which is why John had such a hard time grappling with him. He doesn’t really fit the dominant paradigm, but as soon as he opens his mouth: Panman! Michael Anthony of Van Halen is the ultimate Panman. If you have a musical instrument shaped like a Liqueur bottle, you are probably a Panman. 

If you look at Van Halen standing against the wall in 1977 and you look at Michael Anthony, you think ”Nah, buddy! I’m sorry!” He is a guy who used to work at the hardware store. It has to be tough to be Michael Anthony. In no photograph of him at any time do you ever register the sense that it is tough at all. He is smiling, he is having the best time, he is enjoying being in fucking Van Halen and not for a second does he think to himself that he is the only guy on stage right now who is wearing a shirt. He doesn’t give one fuck and he is having a great time and that is characteristic Panman. 

Merlin is not sure if he should be worried about becoming a Panman. He really likes the first four Van Halen albums a lot! He even likes Standing Hampton, which has There is Only One Way to Rock on it. If John was sitting on a BART train and Merlin was sitting across from him, he might ask himself if Merlin was a Panman, just because Merlin shares certain characteristics with them. He tucks his jeans into his socks, his face is kind of goatee-shaped an some of Merlin’s hairstyles were fairly Pan-ish. 

Did Merlin’s pan-personality get switched with Matthew McConaughey’s actual personality? He should look at their birthdays and see if it could have been a personality transplant that happened with the storks. John does not think that Merlin has a Panman personality. Matthew is not riddled with self-doubt, which is a Panman quality. Maybe Merlin should have had his personality and then there is a third person who should have had Merlin’s personality and Merlin would have had their personality?

John wore Puka shells in the 1970s. In the 1990s his sister in a gesture of early nostalgia, which is uncharacteristic for her, bought John some Puka shells and said ”Remember when we used to wear these back in 1976 when Leif Garrett was on the cover of Teen Beat Magazine?” He is a little bit of a Panman. It was really cute and John wore them throughout the 1990s when they were not acceptable. At one point they broke and he re-threaded them on mint-flavored dental floss. If you kissed John on the neck, which happened sometimes back in the day, you would get a little minty buzzing. John is always offering surprises to the people who are intimate in his life. Life hack: My necklace is made out of dental floss!

John got a lot of shit for his Puka shells, but the more shit he got, the more he doubled down on them. On some early Long Winters promo shots he was still wearing those fucking Puka shells even into the 2000s. He never took them off and had them on from 1995-2003 when the dental floss finally gave way and he was not going to rebuild them as a 35-year old man. He put them in the keepsake-drawer along with a little black crystal wrapped in silver. Merlin never got a crystal from a girl. He never got that far although he did have a lady friend who was very crystal-y. He is pretty confused by crystals, because there is supposed to be an energy thing with Chakras and you are supposed to sleep under a pyramid of copper pipes to see the energy streams that is invisible to you until you do those things. It seems like a lot of work!

The Panman equivalent in the lady-land would give you a crystal and John once got a crystal from one of the Panladies. It was a black pointy crystal wrapped in silver, about as long as a cigarette butt, on a necklace and he was meant to wear it because it was the right kind of crystal for his energy level. There are crystals you put on your window-sills that are supposed to send rainbows into your room and Merlin got one of these for Christmas. 

+ Merlin started using hot sauce (RL136)

Merlin has been adding hot sauce to his food lately and he wonders if that is a little bit worrisome. Specifically, he has been adding Sriracha to things, which is the new bacon of the internet, or a little bit of Tapatío. John wonders if Merlin is afraid in the sense that if somebody doesn’t slap his balls with a leather luggage tag he can’t cum, but Merlin is not German. There is a fear that if you slap your balls once with a leather luggage tag you can’t really get there. Merlin adds it to chili sometimes, he adds it to other things, but he still adds soy sauce to something.

Merlin heard somebody say, and maybe it was Churchill //(it was Thomas Edison)//, you can judge a person by whether they salt their food before they have tasted it, because it is a sign of poor character and Merlin does find himself sometimes putting on Sriracha before he has tasted it. John is a man of solidarity in this regard because when Sriracha is available, he just grabs it and gives it one huge squeeze into whatever he is doing. 

+ John’s introduction to Pho (RL136)

John was told by a Vietnamese lady that you should always turn up your voice like in a question when you are saying Pho. He was first instructed in Pho right at the time when Pho was introduced to American audiences. It came out of nowhere in the early 1990s and in Seattle there were a lot of Vietnamese restaurants who initially served Chinese food because Chicken Cashew was all their audience could understand.

Then John saw it happening at a little restaurant where he used to eat at all the time. They started introducing new items on the menu and you wondered what the heck a Bánh Mì was, and they would ask if you would like to try their sandwich and it was the most amazing thing in the world. Then it turned out that they were not actually Chinese, but Vietnamese, like when somebody at Mission Impossible pulled away a latex false face and look exactly the same underneath.

Once on a cold rainy day there was a little sign on the table that said ”Try our beef soup”. This was a restaurant where the waitresses normally were their 10- and 12-year old daughters and John had a very good relationship with both of the girls. On this day it wasn’t their mother who came over to take John’s order, but it was her mother, the grandmother, that John had only ever seen in the back making the food. John was curious about the beef soup and she said ”Done!” and off she went into the kitchen. She was kind of excited and John was kind of excited, and then she came out with the soup and said ”You never had this before” - ”No!” and she sat down at the table with him and he has a recollection that she actually sat in John’s chair, she scooted John’s butt over and sat on the edge of John’s chair.

John was like ”Hello” and she proceeded to prepare his Pho for him. She went through this whole ”And then you do this, and then you put in the plum sauce, and then you hit it with the hot sauce and then the jalapeños and then pepper and then fish oil”, she was very quick in showing John the different steps, and John was watching the whole thing and was a little horrified because he would not have put any plum sauce in it himself and certainly no fish oil. He wouldn’t have squeezed a lime in it, he wouldn’t have done any of these things, he would have just eaten the soup and left all that roughage on the plate, but she did it for him, all the basil and stuff. Then she did a thing that a woman in Austria once did to John: She stood up, gestured at the soup, and indicated that she was going to stand there and watch him eat it and John ate it.

From that moment on, whenever he sits down at any meal and there are condiments on the table, he just makes everything like a Pho. Merlin is very attracted to foods that he gets to fiddle with. It might be a form of eating disorder, but even when he has pizza, he has lots of different things he likes to put on there in a certain way. Getting take-out from the Thai restaurant that comes with the little paper thing with all the stuff to put in is fun to him! He likes building his own soup.

If Merlin has a plate of food with six different things on it, does he mind if those things touch each other? He doesn’t have a strong feeling about that and he has never been concerned about the food touching and he always thought it was kind of weird how many kids would freak out about it. John eats in order. If a plate arrives and there are five different discrete foods in five different discrete piles, he goes around and eats some from each pile clockwise until it is all gone. Merlin does like them all to finish at the same time. 

John is also a goulash lover. Merlin loves it when he can get a Bibimbap thing going on, the Korean dish in the super-hot stone bowl. John is really into stirring his stuff up in just a mush, too. That is a thing his dad would not tolerate! He had to be able to tell each item of food in his food and he did not want it all covered in sauce. He wanted the meat and vegetables to be separate, he wanted to see it all and know it.

+ Cassette tapes of John’s dad (RL136)

John found a bunch of cassette tapes of his dad (he [https://twitter.com/johnroderick/status/544410975710220288 tweeted] about it) doing his job as an administrative law judge, an arbitrator for labor disputes. When those disputes would reach the point when there was no resolution, rather than trying to reach a compromise, they would appear in front of John’s dad who would make a binding decision. He was a hearing officer who would arbitrate cases presented by the lawyers of both sides.

There are a lot of recordings of these cases from the 1980s and John found them all in a bag while cleaning the house. He started popping them in the stereo and it is like the most boring episode of Law & Order ever. The difference to the company was $500 and yet there was a protracted dispute with the union that went on for months and 50 people testified. 

Listening to these cassettes is like being at work with his dad. Back then after his dad left the railroad John was not interested in this phase of his career was just like ”So what do you do?” - ”Well, sometimes a doctor gets accused of malpractice and is being sued, but there is also the case of the state deciding whether or not to revoke his license and that portion of it appears before me. I hear his lawyer and the state’s lawyer and they argue whether or not he should be allowed to practice medicine and I make a determination”. In a way that is pretty gnarly, but also very boring! John was fucking 23 years old, he wanted to do some Molly (MDMA / Ecstasy) and go party!

John couldn't stop listening to these tapes! While he is folding laundry he is listening to a case, thinking that this guy has an air-tight case, but then the next guy presents his case and as a matter of fact John kind of agrees with his case as well. This is kind of a hard job! John’s dad sat up there on the bench and went ”Well counselor, I think you have made your point!” and everyone in the room laughs because he is making some lawyer joke. They are having fun in there being lawyers with each other! They are boring as shit, but they are trying to have some fun while they can, they are deposing all these witnesses, and you can tell that some people are nervous and don’t want to say the wrong thing. The union is watching, shop stewards who don’t know how to use a microphone, it is amazing!

John’s dad doesn’t appear that often because he is mostly listening, but John kind of wants to play these tapes for his daughter, like ”You never knew your grandfather, but here is a glimpse!” Merlin says that he was more interesting in person when he was yelling at John about his eggs. John should hang on to those! Maybe when he will be 80 years old he will still be listening to these Alaska labor disputes from the 1980s, trying to reconnect with his dad. Pretty funny fucking life!

+ Kiss (RL136)

John is making fart noises when Merlin asks him about his resolutions for 2015. Merlin offers him a music bed with a soft Rhodes Piano, or ”I was made for loving you” by Kiss played very quietly. Kiss are Panmen, every one of them! Paul Stanley: Fucking Panman! He is 67 years old and he still gets on stage and takes his fucking shirt off! He is a bad-ass. He is probably vegan and just doesn’t say so. Whatever he is, John hated him for 40 years, but now he fucking gives his propers and says: Paul Stanley, whatever you are made of, good lord, I wish it could be bottled and sold! 

With Gene Simmons you get the sense that they wheel him to the stage in a cart, shoot him full of Vitamin B and there is some kind of Wrath of Khan ear scorpion that is in his head, yelling ”Sing, motherfucker, sing!” Paul Stanley is probably in a fucking jungle gym somewhere. Can you imagine the tortured dangerous intercourse he has at his age? 

Merlin saw a video of them on VH1 in the 1980s around the time of Lick it Up, Took off the Makeup. They were being interviewed for a documentary and Paul Stanley was in a hot tub or a bare rug and there were actually six girls in bikinis around him. It was the most ludicrous thing he had ever seen. Lick it Up is a terrible song. Merlin wants to make John a tape of some good Kiss stuff to really get John rolling with it, but John claims he has heard all the good Kiss stuff. There is a surprising amount of good Kiss music, John will not deny it, but it is overshadowed and contextualized by the fact that the lion-share of Kiss music is a) garbage and b) performed by non-musicians and c) also somehow reprehensible. 

Kiss is reprehensible and get more gross every year, even in retrospect. It is not that they are diabolical, which is what they think they are, and they are certainly not sensual, but whatever it is that Def Leppard and Led Zeppelin had that gave you the sense that really bad things are happening backstage, bad in a good way, Kiss are just like people on family-court. If they had stopped in the 1980s you could say they are a bunch of sexually potent elemental creatures that got older and decided to call it quits, but in retrospect all you can see is Creepy Uncle Pinky Butt the entire time. In interviews from 1975 you can see that he was already an old creep back then. Gene Simmons is Donald Trump in dragon boots, it is no good. 

+ New year’s resolutions (RL136)

John does not usually make resolutions, but this year he is making a legit and honest resolution and he hesitates to say it because he has said this type of thing before and somehow he has no authority over himself. This year John does not intend to direct his energy in any other direction other than the direction of making music. He is not going to direct it into the direction of being a talk show host or a feature writer or a dumb comedy persona or a Goofus MacGroofus or a Dingbat McDonnahein, but he is going to see what lies in the music grave!

John has never stopped playing music, he never stopped tinkering with music, and it is what people want from him, but the question is if he stopped writing new music because he wanted to stop writing new music or did he stopped writing new music because he just succumbed to not writing new music. Was it a decision or was it a fait accompli? Does he want to live the rest of his life with that kind of accidental decision? No! If he doesn’t want to write music anymore, he wants to say that he is not going to do it anymore and he wants to do something else, but not this mealy-mouthed kind of non-decision. 

John spent last year doing his weekly talk-show in Seattle and he learned a lot, but whenever he came to a crossroads where he could try this thing that seemed uncomfortable he went in the direction of what he already knew. On the first talk show he did he wrote a monolog, went up there and was covered in flop sweat. He didn’t want to read the monolog and he was trying to recall it, which wasn’t fun, he was reaching for this thing that he had written and he couldn’t lay his hands on it. 

Over the course of the next month or so John did less and less preparation and more and more improvisation and the show got better and better and John got happier and happier. It was a lesson to go into the direction of his happiness because he didn’t do this show to make himself unhappy. John pursued the direction of his happiness which was in the direction of what he already knew how to do, which is to get up and tell a 25 minute story about a 10 minute walk. 

If John had said that the structure of the show was to write a monolog every week and figure out how to deliver it, it would have been super-uncomfortable and super-hard, but a year later he would have learned a new skill instead of luxuriating in a thing he already knew how to do. John doesn’t feel like the year was wasted or the show was wasted, it was all a good learning experience, but when looking back at it, the key component that is missing in John’s life is a relationship to work. 

John did everything he could to make that show not being work for him to do and that is not what he needs to do, but he needs to learn how to work. He does not want to work. He does not want to have to work. He bought a fucking lottery ticket the other day because every little cell in his body is saying ”Please find a way to not have to face work!” and he can’t do it for the rest of his life. 

John’s new year’s resolution is: "Face Work!", which means: "Write Songs!" It is a huge process, it involves a lot of financial investment, it also involves some risk, but not a tremendous risk because somebody will want to buy his music. It is not like before where he would hand in a record and the record label would say ”We can’t support this” and that would be the end, but he can Amanda Palmer it and put it out there and there will be people who will find it. The risk is the same risk he has always been taking, which is: What if everybody in the world doesn’t love him unconditionally? 

That risk isn’t any different compared to before and finding the answer to that question is never going to be as satisfying or useful as he might hope, however it turns out, which Merlin happens to know. John likes Merlin and Merlin likes John, too! There are only three more days left in the year now, let’s make it a happy, shiny new year! John has three more days where he can sit in the bathtub and say ”Work? Work’s for jerks!” In the new year he will going to have to wake up an say ”Work is not for jerks, but it is the only thing that gives life meaning. Leisure does not give life meaning!” The first hard part is getting started and the other hard part is continuing. The third hard part is finishing. Three Hard Parts is the name of his backing band. 















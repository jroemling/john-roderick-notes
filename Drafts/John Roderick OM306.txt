OM306 - Mensa
2020-10-29

This week, Ken and John talk about:

+ Preface (OM306)

John prefaces the show by acknowledging that they are on stolen native land. The tribes do have somewhat exclusive rights to harvest Geoducks, the food that powers every great military triumph. In addition to that, the Futurelings probably have a larger base-level potential Mensa membership relative to the Joe Rogan’s show, and some Presentlings are Mensa-affiliated. The IQ is a measurement of intelligence with the mean established as 100, which means raw IQ scores have been going up over the 20th century, called the Flynn effect. This might be the first show where they are not only talking about the topic right off the top, but they are already apologizing for the show.

+ Ken being modest about his intelligence (OM306)

Ken is famously America’s boy genius, and yet he is at pains trying to disabuse people of the notion that he is some kind of super-genius and that his talent of Jeopardy! is the result of a massively superior intellect. John knows that he is pandering to people and he is planning to destroy us all with his giant brain if he could just find the right frequency to explode our heads by thinking about it. There is that look in Ken’s eyes all the time!

As a kid at home he didn't have a peer group, except the kids next door or a bunch of dingdongs at Sunday School, but as soon as school started there was a large-enough sample size for Ken to see that he was the only one who got trooped out of his Kindergarten class every day to go to a reading group in the 3rd grade classroom while everybody else was practicing drawing the letter ”C” and then a little letter ”c”. You can’t not notice that and you can’t not take a little pride in whatever thing about you that gets adults a little buzzing.

Human development is such that especially in Kindergarten you have a huge amount of pride at being singled out and being treated differently for your intelligence, a thing that you cannot learn like throwing a baseball. Most kids are aware that they are not grown-ups and any competency that impresses adults feels grown-up. You know that adults are reading the newspaper and if you are also reading the newspaper you not only feel like you are ahead of your grade level, but you feel like you are a little grown-up with some competency in the world. John had opinions about Henry Kissinger and he felt very strongly that the Watergate burglar should be prosecuted.

Ken will downplay being a smarty-pants on a game-show, partly because of a little discomfort with the thought of being different, partly because of the kind of associations that go with that kind of personality. There is a certain idea of what such a person might be socially or emotionally and Ken doesn’t identify with that and is trying to create a gap. 

The stuff that people are asking about on game shows is just a parlor trick. Knowing all your state capitals is not a good stand-in for actual intelligence and Ken grew up playing trivia games and pouring over road atlases. You probably have Jeopardy! level competency in whatever thing you are curious about, and the quiz kid has just a slightly wider net. Instead of reading New Mutants comics you could have spent that time looking at the Grolier Encyclopedia of Knowledge, which was Ken’s childhood!

+ Ken’s son thinking about becoming a dentist (OM306)

Ken’s son Dylan has decided towards the end of High school that being a dentist is a good job because it pays well and he doesn’t have to work that hard. He is also very interested in fishing right now because he has a friend who fishes and even though Dylan owns no tackle and no gear he is wondering if fishing is his new identity. As a result he is looking at jobs at fisheries, NOAA //(National Oceanic and Atmospheric Administration)//, BLM //(Bureau of Land Management)//, and Oceanography, and he is looking at which schools have the best ichthyologically relevant courses.

+ High expectations on young generations (OM306)

John is not as aware culturally and socially how much of the expectation on the way he and Ken were educated in the 1970s and 1980s has really changed now. When he was growing up there was a very clear sense that success was a path, that you could measure success as a degree of both educational achievement, professional achievement, and economic accomplishments.

We have a sense that the Millennial generation was raised to think of success across a broader scope, that you could be creatively successful, that your goal was to be emotionally successful, and that is often levied as a criticism against them for having unrealistic expectation. John is not sure that is actually true or if people of Ken’s son’s age feel the pressure to be financially successful.

Ken wonders if the two recessions that the younger generations have lived through in the last decade have been very formative for them and if that is going to make them more worried about making choices that keep the utility bills paid and if they will not be the ones who are going to be majoring in renaissance literature.

The most frustrating thing for the adults in John’s life was the fact that his performance and his success in school did not correlate at all with his test scores, which is a worst-case scenario for a parent. The test scores suggested that school would be easy for him. Even the word ”underachieving” assumes that everyone who is doing bad at school could do better if they would just sit up straight and achieve.

+ John keeping his hair long enough to cover his scars, his glasses being bird shields (OM306)

John keeps his hair just long enough to hide all the scars, not only emotional scars. When the hair goes over his ears you start to see that he relaxes a little bit because he doesn’t feel so exposed and his ears aren’t in the wind.

One time John tried contact lenses and walked out into the world without his glasses. He felt like a bird was going to fly into his face in the middle of the city. ”There are birds everywhere, what do people do?” They could just fly into your face at any moment and John never realized his glasses were acting as  body armor and bird shields.

Ken finds long hair to be irritating. It is always rubbing and every time he turns his head to the side he feels touched in 80 places. John likes that touching!

+ Polymath education, universal education (OM306)

Prior to the 19th century the super-smart people that we remember through history were all polymaths. No-one identified as just a biologist, but they were all doing biology, chemistry, and philosophy, and a smart person like Thomas Jefferson or Leonardo da Vinci did everything. In the 19th century things became more industrialized and we also started to see intelligence as a thing that could be siloed. Einstein never did any biology. We started to think of people as having intelligence that is separate from well-roundedness. Einstein might have been the first celebrity of being a genius and something about his brain gave him super-brain-powers and we need to study his brain. Things like that were not in the public consciousness before the early 20th century.

One version of the Great Man Theory of History is that if Einstein hadn’t existed in this one corporeal form, maybe we would still not know about relativity. John doesn’t believe in that theory. If there had not been a Napoleon, maybe not that many French people would have died outside of Moscow that particular winter, but some winter eventually. The French, the Germans, and the Russians were all going to collide against each other somehow. It is arguable that if World War I hadn’t happened that maybe we would have had a generation of fin de siècle poets and craftspeople that would have made a utopian world and we would all be in flying cars now.

It is interesting that the previous great men are all military because that is what changes lives when technology is pretty stagnant. It is not that one guy made a better plow and was getting much more soy beans, but it is more like: ”I am not speaking French now because of king Whoever” Those become your geniuses and you romanticize their tactics at the Battle of Whatever, but that goes away as soon as technology advances because the things that affect your daily life today have more to do with scientific genius than with military genius.

In the past non-military geniuses were monks and first with the enlightenment we got secular monks who instead of counting how many angels can dance on the head of a pin were counting how many pea chromosomes can make the pea twist counter-clockwise. The common person didn't know about that, but other monks appreciated the good work and except for a tiny group of people who were passing parchments via raven it didn't affect normal people’s lives. There weren’t a ton of people hanging on Augustine of Canterbury’s every word.

By the 20th century we had in the United States and in the UK notions of universal education and ideas that education is going to make the utopia possible by lifting the mass of people out of a state of ignorance and into a world where they are ready to be governed by philosophers. It is a progressive  social-justice cause, but then they sadly all turn into eugenicists 5 minutes later. We still ascribe to the notion that education is going to have far-reaching consequences, and we all assume it, but you couldn’t make the case that the experiment of universal education over the last 150 years has not produced a generation of philosopher kings.

There are a lot of assumptions that people are prejudiced because they lack information and if people had more education they would not be able to maintain simplistic biases, but those are harder to shake than it seems and education isn’t the panacea that it is natural to assume. The root question is if it makes you happier to be smarter, and both Ken and John have not found that to be true. They both can’t think of a subject where they read more up on it and then felt more reassured. How much bliss is there in ignorance?

+ Ken’s Lechwe antelope report (OM306)

At one point a recognition of genius as a separate class of being came into the world, not just those who read a lot and thought a lot, but Nietzsche introduced the Übermensch notion of super-beings. It filters down to the class of regular smarts, people who aren’t trying to destroy their friends with laser eyes, but people who are gifted and are taken out of their Kindergarten class and put into the 3rd grade reading group and there is a lot of social exclusion and social difficulty. You feel pride because the grown-ups have singled you out, but when you go back to your Kindergarten class you are not universally welcomed and celebrated by the other kids, in general it is the opposite, which is why Ken often kept it under his hat.

When Ken was in 2nd grade some kid had gone to Africa and brought back a little statue of an antelope, but he didn’t know what kind it was. The teacher posed the question if somebody could figure it out, which was like catnip for Ken and of course he spent the next 6 recesses in the World Book, trying to figure out what kind of antelope it was and he still remembers that it was a Lechwe and that he wrote a report for himself and drew a picture of a Lechwe.

After the fact he became aware that no-one else was really doing that and if the teacher showers approval on you it will cause backlash from your peers who will not shower approval on you. As a result Ken spent decades of his life trying not to repeat the Lechwe incident, but act like a Normal. He was also in special circumstances by being in that weird terrarium of a private international school. If he had been in a giant American High School he would have been dead and nobody would have wanted to hear his Lechwe report. He would still be in a locker!

As a gifted kid you often have the desire to self-sequester or to pick other gifted kids on the assumption that you have shared interests and that intellect creates a commonality. Ken never thought that it would be great to have a little club of people who have the same interests so they could sit and chit-chat. His close friends in High School were smart kids who liked to read, who had ideas, who had takes, a certain type of school newspaper kid.

He never was in debate club, but he did quizball, which is very much the Mensa long tail of that Bell curve, and every time he got drawn to the end of the Bell curve he constantly felt that the more rarified it got, the less he wanted to hang out with them. John agrees. The people at that end of the Bell curve were really good at quizball and they knew a lot of stuff, but it did not become easier to find people who were funny or chatty.

+ John being on tour in the UK at Mensa UK’s city (OM306)

Mensa was founded at Oxford University and Mensa International is still located in Caythorpe UK as a separate organization from Mensa Britain that is located nearby in Wolverhampton, which is not where you would think to locate Mensa. John once played a show in Wolverhampton and he can’t remember walking out of there thinking: ”This is really where the UK shines its brightest lights!”

+ Tests as a career decider (OM306)

World War II created a generation of American organizational leaders who had been running organizations in the military, which explains a lot about mid-century America: All of our bureaucracies were based on the War/Defense Department and Pentagon style solutions to problems. You became a Corporal because you got a good score in that test, and it is still true in the military that the test will decide if you are going to be infantry or intelligence.

Today we are just doing tests for driving and that is about it, too bad that we don’t do it for gun ownership. There are still citizenship tests, but there are not a lot of places doing tests anymore. Every time you sign a business contract it is an intelligence test and an entertainment business contract is always a test of what you didn’t think of. Logging into Twitter used to feel like a fun intelligence test and now it feels like a test whether or not you are going to be allowed into the off-world or not.

Ken’s parents were very keen about providing their kids with the right support for their intelligence, not just so they could brag at parties, but they also didn’t want them to be bored at school. John’s parents did, too, but they also had a hubris of smart people themselves who felt like they can handle this and it is going to be fun.

John knew a couple of kids whose father was doing brain research in the 1960s and who would sit them in front of boxes that he had built of flashing colored light, trying to stimulate their brain: ”Okay, put this one in your mouth and I am going to turn the electricity up a little bit and you tell me if you see any colors!”, although one of them became Paul Allen’s chief librarian, so maybe it did work.

Ken found his path via the Jeopardy! program and had he not found that outlet he would be a mildly successful, mildly unhappy computer programmer who correctly wasn’t regarded within his professional sphere as an incredibly gifted computer programmer. He had found that career in the same way like his son found dentistry: If seemed like there would be jobs there for the next 20 years. Then on the other side there are people with tremendous talents that don’t find their way to the Jeopardy! program.

Ken wanted to go on that show because he grew up watching it as a kid, and you can see what jobs those people on Jeopardy! have: There are a lot of lawyers and a lot of broad-minded polymath problem-solving aimless gifted kids often end up at law, which was also where John was supposed to go. Everybody always assumed he was going to be a lawyer from the time he was 4 years old. Ken is also the son of a lawyer. There are many academics on Jeopardy! like teachers, professors, or librarians, because it is a place where a bookish person can find success and peace.

One assumption about Mensa is that you want peers, but many of those people are solitary and anti-social and they want peers because they want to be recognized for their gifts as a self-congratulatory thing and it self-selects for people who have not gotten that kind of validation elsewhere in life. If you already feel good about your intelligence and what it has done for you, you do not need to apply for Mensa, which means it attracts a wayward gifted child. If Mensa was a Bilderberg group of smarties and from within Mensa new patents and great novels were spewing out, and a biologist and a physicist got together and realized that all particles were the same, we would have more respect for it as an institution, but often it feels like another form of bedecking ones blazer with a certain number of pins that are self-awarded medals of honor.

+ Futurelings talking about Harry Potter houses, Character alignments (OM306)

On the Futurelings Facebook page [https://www.facebook.com/groups/futurelings/permalink/966319187114170 people started talking] about Harry Potter and that thread went off the rails fast. There was character assassination, they were talking about the tranexclusionary radical feminism and the turfness of the author, but that was not even the most controversial thing. Ken’s daughter is a very proud Hufflepuff with a Hufflepuff hat and she does not enjoy jokes about the houses. Also: Why is there one house for all the losers? There is quite a bit about debate of which houses Ken and John are in, and people take that very seriously, it is as important as the Wechsler Intelligence Test.

John doesn’t know what house he is in. He doesn’t think he is a Slytherin, but people keep saying that he is and then other people are mad about that. According to the J.K. Rowling view of the world there are 4 kinds of people: Good, smart, dumb, and evil. Those are different, not two axis. Ken assumes he is a Ravenclaw, which is smart. John is not evil for sure and he doesn’t think he is dumb. He is also not good exactly, but smart? Does he end up being evil because you wouldn’t first describe him as good or smart?

Throughout his life John was very definitely Chaotic Good and as time has worn on, little by little he trends a bit toward Chaotic Neutral and he was distressed to discover this about himself because he really identified as Chaotic Good. To be Chaotic Neutral felt a little bit of saying Chaotic Evil. As soon as you leave the realm of Good into Neutral, chaos starts to take on a very different tint. Ken knows John more as a Neutral Good, trending toward Chaotic Good because he lost all his ”We can do this within the system!” convictions. John thinks Neutral Good is the place he aspires to be, but Chaotic Good is his instinct or his temperament. Chaotic Neutral is not where he wants to live, it is the path to destruction!

+ Sudoku, Crossword Puzzles, Threes (OM306)

John likes Sudoku and he likes Crossword Puzzles, but he doesn’t like those puzzles where he is supposed to make an Eiffel Tower out of 7 toothpicks. Ken is the same, he is good at the crosswords and he is not good at those other ones. All his aesthetics can be explained by what he finds easy. There is no emergent solution and nothing is born into the world when you solve a Sudoku, the way a crossword feels like you have helped birth something and you have produced something because the letters make words. Sudoko is just pretend math. There was a video of a very clever Sudoku variant where the British host at every turn is stymied //([https://www.youtube.com/watch?v=yKf9aUIxdb4 The Miracle Sudo] by Cracking the Cryptic)// and it is very dramatic.

Ken has never played Threes, the mobile game, which similarly is a thing of: ”Why am I doing this? How have I spent 5 hours doing this? TV is making me think too much, so what if I could just swipe my finger whenever a red thing hit a blue thing?” It is the type of thing that you can do while you talk on the phone at the same time and feel like you are doing a passable job at both.












OM257 - Grade Inflation
2020-05-12

This week, Ken and John talk about: 

+ How The Omnibus came about (OM257)

About two years ago Ken and John flew out to Atlanta and recorded the very first entries in what would become their monumental werk The Omnibus. The flight attendant asked: ”Are you Ken Jennings?” while Ken was at the lowest point in his fame, and after he had signed her badge and her underwear he said: ”Yeah, I am really popular with grandmothers and flight attendants!” Flight attendants have access to your name and by combining that with your face they are 10 times more likely to recognize you than somebody else would.

The original pitch of their show was ”The Worst... with Ken Jennings and John Roderick”, like: ”This is the worst mathematical formula!” They would talk about that bozo in Indiana who thought pi should be 3, the worst dinosaur, the worst battleship, or the worst battle between two cruise ships. Ken and John didn’t like that idea, but on the flight they came up with Omnibus and they were high-fiving on the plane. When they arrived at their corporate masters at How Stuff Works they were super-bummed and did not like the new idea because the original one where they would hate on everything which gets way more clicks.

It seemed like such a bummer and Ken and John didn’t want to be all negative about stuff, so instead they decided to do a shows about the end of human civilization in a perky way. Many people don't know what the word Omnibus means, but it is irrelevant. It sounds Latin and authoritative and governmental in a way, like old-fashioned British empire stuff. Ken is not even sure exactly what it means, and he has a dictionary right in front of him. Omni means all and bus means bus, so it is all the busses, it is Latin for Greyhound!

They met the night before in one of their rooms to figure out how the show would work. Ken's idea was European Starlings and John's was Defenestration and they threw together a couple of shows. Ken had a piece of notebook paper from the hotel where these days they will give you 4 pieces of paper on the nightstand and a pen that writes for 3.5 pages. John still has drawers full of Chateau Marmont pads and pens with his name embossed on it, but lately they have also just been giving you 4 pieces of paper. Everything has gone to hell, or it might just be COVID, because if they would give too many pieces of paper to people they would take it home and put it next to their toilets.

+ Hotel rooms, hotels asking for to ratings (OM257)

When they came to their hotel and walked into the elevator there was a sign that said: ”How was our service?” There was a scale from 1-10 and there was an arrow saying: ”Only 10 is acceptable!”, which struck Ken as funny because there was this wide gulf of grades they did not want you to give them. Maybe it was supposed to intimidate their housekeeping staff, but on the other side it was trying to communicate that only the top level of service is what the guest deserves and they were an amazing hospitality apparatus that takes care of it.

Ken almost never calls down to the front desk when he comes to his hotel room. For John that is always the first thing he does because there is always something. Generally he asks to have his bed defeathered because he is allergic to feathers an if the pillows or the comforter have feathers in them, then within 4 minutes he starts to cough and he will get COVID from feathers.

John has small needs, but they build up to be a giant pit of need. 1-star reviews of hotels are always about things that have never happened to Ken in any hotel, like: ”I walked in and the toilet was overflowing”, or: ”I walked in and instead of a bed there was a wrecked car” Catastrophic things are happening to other people, but when Ken walks into his room, sometimes it is a little depressing and sometimes it is not, but he has never had a problem. 

+ John’s Tweetstorm at the Hilton in Crystal City (OM257)

One time John checked into the Hilton in Crystal City Arlington Virginia, which is not a city at all, but just a subway stop, and the room smelled like cigarettes. He went back down to the reception to tell them about it and it took them 20 minutes to give him another room which also smelled strongly like cigarettes. It took them 30 minutes after that to put him in a third room that smelled like cigarettes as well. It was 2012 and surely not every room in this hotel smelled like cigarettes? John went on a [[[tweetstorm]]] about it and said that there was blood on the floor and a cow head in the bathtub.

After 20 minutes there was knock on the door and a stern voice outside said: ”Mr. Roderick, we understand there is some problem with the room. Can you open the door?” - ”I am not opening the door!” and he was tweeting that there was guy outside. It turned into a big hullaballoo and some conflict-mediation person on Twitter wondered what they could do to make this right and offered him 5 days of free Hilton anywhere, but John was having too much fun at that point and didn’t want their filthy lucre. Eventually they realized that John wasn’t Ken Jennings and didn’t have the reach so it didn’t get picked up by Buzzfeed and they stopped offering him anything and just ignored him.

+ Receiving lesser things from an eBay seller (OM257)

One time John got into dispute with somebody on eBay when the thing arrived and it was not what they described. John said: ”This is bad!” - ”You can send it back!” - ”I don’t want to send it back and I don’t want to give you a bad rating, but I think you should make this right because this is less of a thing than you had advertised!” - ”That is against eBay terms and conditions! Because you mentioned ratings now it is extortion!” - ”What?” John ended up just passively accepting that it was lesser //(see also story in RL234)//. Ken had eBay sellers harangue him for months because he didn’t rate them.

+ Ken’s mom showing his teacher his Star Wars drawings (OM257)

In Junior High Ken had a C+ in Home Economics because his apron was a disaster. He deserved his C+, but he wasn’t happy about it. His parents did not go down there and raised hell. But in 3rd grade when Ken’s art teacher was giving him a hard time for not doing the assignment right his mom marched in with a series of pictures of Star Wars characters that Ken had drawn at home and showed her: ”Look at this likeness of Han Solo! This is the kid you are yelling at?” It cracks Ken up the more he thinks about it.

By High School they had eased up a little and let him sink or swim, which is the right thing to do. That did prepare him for eventually getting into hard classes in college where he was in STEM fields where there is less grade inflation, and when he took an operating systems class and found it super-hard and got a C, he was emotionally prepared for it.

+ John getting a bad grade on his Gonzaga writing test (OM257)

John went to Gonzaga University and at the end of Freshman year they gave a writing test to everyone in the school with prompts like The Milagro Beanfield War and you were meant to write essays. It was either a standardized test or they were just measuring the mean standard of writing at the college. John thought of himself as a pretty gifted writer and thinker even then, and he wrote an essay that he was proud of.

There was a kid on John's dorm floor who was universally regarded as the jovial jock dummy who got in because he was a basketball player, but everybody loved him. He was the one who was constantly ”What?” about everything and still he got 4/4 while John got 2/4. How was this possible? Turns out, he had been following the instructions, he did beginning-middle-end, and he built his thing according to what they wanted, while John had scanned just the prompt.

Whoever was grading it was grading 3500 papers and they found that John’s intro didn’t address the question, or whatever. If he had cared and would have raised hell, someone higher up the chain would have read it and said: ”Yeah, I see, you are a genius, that is fine! Whatever! Doesn’t matter!”, but John didn’t care at that point.

+ John having bad grades in High School (OM257)

The only two A’s that John got in all 4 years of High School were in Newspaper because he loved Newspaper and was spending a lot of time thinking about their High School paper. Their advisor recognized that he was in there all the time, working and writing a bunch of articles. The same was true for Ken. They were both thinking about the paper more than any other classmate.

John got a lot of D’s and F’s in High School in Alaska, although East High was very competitive and sent a lot of kids to the Ivy League. He had an F in typing. A couple of his friends live in Brooklyn and their teenage kids are trying to get into Ivy League schools, but it is impossible because Harvard cannot take 6000 Park Slope kids. There are only going to be 10 kids from Park Slop admitted, if that, and yet they have some slots for kids from Wyoming and Alaska, meaning that all these East High kids were diversity hires.

+ Grading systems in school (OM257)

The first letter grade system in school was invented by Mount Holyoke College in 1897. Before then you were just assessed or it was Pass/Fail or a note from the teacher with an adjective. John’s dad was afraid of getting an E, which was the lowest grade back then. There was probably an ambiguity whether E might mean Excellent, but all F meant was Flunk or Fail.

John and Ken’s Elementary School grades up to 7th grade were E, S, N, and U (Excellent, Satisfactory, Needs improvement, Unsatisfactory). After that it went to letters. John was not one of the kids who would change an F to an E, he took pride in his F’s. Ken never even did the ”Don’t bring home the report card” trick, which almost universally works unless you have a sibling screwing it up, because parents don’t remember when report cards are coming home.

Ken’s kids’ school actually does give little sentences, but they are clearly boilerplate, and as a result teachers will choose the most anodyne ones and you get things like: ”Dylan works hard and is a joy in class!” John is frustrated by those! The teacher has one job! Write a little paragraph about every kid!

+ Clothing size inflation (OM257)

John is a vintage clothing aficionado. Men’s clothes are measured according to a tape measure, and a 44 long suit is 44 inches long, both in 1942 and today, whereas in woman’s clothes a 10 in 1960 is now a 2 or a 0. As all people have gotten bigger the women’s sizing has changed and it is very difficult when you are shopping for women’s vintage clothes to compare to contemporary sizes. Even now, if you are a 14 that can mean 6 different things and a 12 at J.Crew and a 12 at Ann Taylor mean completely different things. The perception of it is that women prefer to be a smaller size, so the grade follows the vanity.

+ Tip inflation (OM257)

John has worked in restaurants before there was a universal expectation that people got paid a 20% tip. 10% was the standard and 15% was something you would give for exceptional service. When John was young the tip absolutely was a grade and the diner graded you. In all restaurants there are waiters who are really busting and there are waiters that are slacking.

The other waiters and also the customer can tell and when automatic tipping came in everybody was grateful to get the money, but you can also get the feeling that you have been working your tail off all day while the other waiter was just in the back smoking. You don’t get the extra acknowledgement for doing a better job as often anymore.

+ Other

John was in Bulgaria (see [[[The Big Walk]]]) when they devalued their currency, the Leva.














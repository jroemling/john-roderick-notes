OM158 - Pattie Boyd
2019-05-30

This week, Ken and John talk about:

+ Ken’s relationship with music, The Beatles (OM158)

One of the the things people don’t know about Ken is not his body odor (he smells like fabric softener, meaning his wife is doing a good job doing his laundry), but his tremendous knowledge of music and popular culture. He is an avid consumer of Rock’n’Roll and he knows things even John doesn’t and he continually surprises him with facts like this guy played bass on the second Slint record. John even knows those guys personally, but he still didn't know what Ken was talking about.

Ken thinks John is overstating it. He is not a musician like John and has no talent whatsoever, but he is just a fan and was a pretty hardcore music guy  as a young person. It was a big part of his identity and he read interviews in Q Magazine or SPIN Magazine to see what his bands were saying about the affairs of the day and if they were making jokes about each other. Do they get along? What are they really like?

+ Throwing all the genres of music together (OM158)

The genres that used to define music are now all jumbled together. You can listen to Classic Radio and hear Black Sabbath, Blondie and Tom Petty one after another, but those things would never have been played in the same ecosystem in their original time. The only thing they have in common is that they remind you of when you were in High School, 50 year old man!

Young people have an interesting connection to popular music of the 20th century: To them it all belongs together and you can listen to Elvis or Led Zeppelin or the Bee Gees because all the differences between them are gone. They are weirdly ecumenical about that era as well. Some new band will put out some 1980s-inflected song that sounds like Crowded House, the next track will clearly be Brian Willson inspired, and the next track might have a guest rap on it. As a kid Ken was attuned to what was the new sound and what was the old sound and he was a little embarrassed that he liked his parents’ oldies station. Today that is all gone. 

+ The Beatles (OM158)

Ken started with The Beatles, like so many of his generation did. It was what his parents always had on around the house together with a lot of 1970s California AM Gold, plenty of Carpenters, John Denver, Bread and that sort of thing. If you listen to Lennon/McCartney songs and then you are hearing other stuff, you realize that the Lennon/McCartney songs are the better ones and that there is something to them.

The other day Ken was driving behind somebody with a bumper sticker saying The Beatles and he was thinking: ”This guy is a fan of the Rock group The Beatles and he put it on his car! I am going to check them out!” Imagine being so into The Beatles that you have to show the world!

What is amazing about The Beatles is that there is new sound and old sound within the same band. It can be very confusing to listen to Love me Do and then I am the Walrus back to back because even though they were only separated by half a dozen years or less, they were worlds apart in terms of tone and time.

The Beatles have a lot of songs that kids react to. Ken’s kids loved Yellow Submarine when they were very little. Maxwell’s Silver Hammer was a big influence on John’s daughter. John loves the Paul McCartney music hall songs, especially the ones that involve serial killing and have a story. There is a lot of gun play and hammer play in Rocky Raccoon!

Abbey Road from the late period was the record Ken heard most as a kid, but obviously his prototypical idea of a Beatles hit when he was 5 years old would have been the Ed Sullivan Show songs like I Want to Hold Your Hand and She Loves You, which is the exact opposite end of the spectrum.

Eventually Ken landed somewhere in the middle and today Revolver is his favorite record. It is from a time when they hadn’t gone into the White Album off-the-psychedelic-cliff yet, they were still being instructed by other people to only put 10 songs on a record, and they were still clearly bouncing ideas off of each other instead of just showing up with finished songs that maybe the rest of the band didn't even play on.

John went into The Beatles at their earliest. He started listening to them when they were still in their Rockabilly genesis-period and followed their career chronologically. He was very into Skiffle (genre) for a year, like Rory Storm and the Hurricanes.

+ A Hard Day's Night (OM158)

John remembers watching their movie A Hard Day’s Night from the early days of renting VHS tapes. His mom was an early adopter during a brief period in the 1980s and she had a brand-new Beta Max which John lauded over in school for about 3 months as being the superior technology until he realized that no-one else had one.

Ken was jealous of the smaller tapes. His family had VHS and he was jealous of his Beta Max friends. John tried to make all his VHS friends jealous, but the VHS kids were trading tapes with each other, Kevin Horning’s dad actually had a VHS camera, and John could not keep up. The Beta machine got smaller and smaller in his mind.

John saw A Hard Days Night pretty early on in High School at one of those parties where you picked a movie and someone picked a black & white movie from 1964 because they were all Rock fans and this felt like a Rock thing to do, although it seems like better taste than High Schoolers usually have. It was before most of them were really even aware that necking was an option. John was a late bloomer in a crowd of late bloomers and only many years later the others started to neck while John was still there with his nose pressed to the TV, going: "Harold and Maude is the greatest movie ever!" and his friends all said: ”You suck!”

Hard Days Night blew John away because of the spirit in the movie, the idea of Beatle mania and the fun and the quippy fraternity they had. The world they lived in as portrayed is aboyant (?) and effervescent even today, the stodgy Englishmen and their slightly older managers, guys in their 30s that were squiring them around, rolling their eyes at the Beatles antics (?) and: ”Look how cool they are! They are so amazing! Those guys who wear hats are all mad!” You really do want to hang out with them, which you don’t get from listening to records. It turns The Beatles into something above and beyond, something no-one else can ever be.

There is a Bob Dylan documentary called Don’t Look Back that was filmed in its time and in that he is not someone you want to hang out with, but he is a dick who is really lame. It portrays an amazing world, but it is just too bad that Bob Dylan has to be in the middle. John sympathized with the Life Magazine reporter who was just asking a question. Give him a break!

The Beatles movie was made on short notice in the middle of them being at their peak making three records a year. They just took 6 weeks off from two tours and from having to deliver 10 new songs for the record to throw together this movie. It doesn’t end well, it peters out, and Ken didn’t like that they were reusing the songs in the concert at the end. They continue to talk about different scenes of the movie.

















RW49 - Listening in the Stock Room
2016-12-02

This week, Dan and John talk about:
* Starting the show with a high pitch ([[[Podcasting]]])
* Gas giants ([[[Factoids]]])
* Tooth picks ([[[Attitude and Opinion]]])
* John’s cousin’s wedding ([[[Family]]])
* John’s dysfunctional family ([[[Family]]])
* Dan’s family ([[[Dan Benjamin]]])
* John’s college radio talk show ([[[Stories]]])
* 2016-December: 10 o’clock whiskey ([[[Currents]]])
* Listener greetings and listening habits ([[[Podcasting]]])
* John working at a Checkmart and Kells Irish Restaurant ([[[Employment History]]])
* John driving from Portland to New York ([[[Stories]]])

The show title refers to a listener who works at an Apple store and who sent a greeting to John because she always listens to his show when she is working in the Stock Room.

+ Starting the show with a high pitch (RW49)

Dan starts the show sounding very chill to avoid the overdrive that John sometimes experiences, an effect that Merlin had told Dan about. Merlin experiences the same thing, but only with Dan. When John calls his mom, she will pick up and say ”HEY!!!” and even though he knows it is going to happen, 9 out of 10 times it goes into John’s ear like a bullet and no matter what mood he was in, he is starting this conversation in pain and annoyed that it has happened again. 

Once John called her attention to it, but it just got weird and now she has forgotten about it and sometimes he forgets about it. Dan has really high quality audio equipment, his microphone goes through 5 tube Macintosh amplifiers, going out over an old short wave antenna mounted on the roof of Dan’s house and therefore it sounds amazing and it goes with the turf. Merlin seems to think it is a Skype artifact of some kind, but he said it didn’t happen recently because Dan is in a new office, but he still didn’t want to take the chance.

+ Gas giants (RW49)

John has been feeling a little edgy. He doesn’t know if it is the product of something chemical or something in the astronomy realm, maybe there is some additional gravity, like a Jupiter ascension. On Jupiter, a jug of milk weighs 40 pounds, but Dan interjects that it is a gas giant and there is no ground it could hit. John speculates that there must be something at the heart of Jupiter, like a hobgoblin or some kind of little rock that everything is spinning around, with a bunch of paperclips and all the detritus of the universe stuck to it. Just a bunch of lint balls and stuff on this rock that is the size of a medicine ball. It is covered with paper clips and other little shit that you drop on the floor of your car, and it is surrounded by a 100 million cubic miles of gas, which is kind of like it is to be in bed with John, lying next to him, covered with 100 million cubic feet of gas.

+ Tooth picks (RW49)

Flossing at the dinner table is not okay!

Using a tooth pick is not appropriate at the table, but John picks up a toothpick on his way out. In his own home he doesn’t keep them on the table. In South Korea it was considered okay to use a tooth pick at the table, but they would cover it with their other hand. At the same time in the late 1990s, it was considered rude for women to be seen smoking, so if they wanted to smoke a cigarette, they just covered it with their hand. If John was in a culture where they would pick their tooth at the table, he would have dabbed to that culture, because he likes picking his teeth, it is very gratifying and if everybody did it, he would follow along.

+ John’s cousin’s wedding (RW49)

A long time ago, one of John’s cousins (she) got married to a member of the McLaren racing family. It was a fancy wedding and there were a lot of men from the Formula One racing culture, not drivers and mechanics, but owners. It was a very exciting wedding for a 13 year old boy. These guys were hail and hardy, they were spilling drinks on each other, they adopted John and one of them would send John packages of fireworks for months afterwards. You could have sent him a box of money or a box of fireworks, and he would have taken the box of fireworks every time. They also gave him a bunch of stickers and other McLaren F1 swag. 

As John was walking around this wedding, he felt in his oats and he had a toothpick sticking out of the corner of his mouth, like ”Z’up?” His uncle Cal was a different kind of fancy than these guys, because they had the amount of money where you just don’t care, with a lot of cars in the garage, but uncle Cal was the kind of fancy that donated to the art museum enough that there was a room named after his wife. He told John that a proper young man does not walk around with a tooth pick in his mouth and John was embarrassed and ashamed, because his uncle Cal did not say that much to him in general and it was probably the only thing he had said to John in months. John took it out and threw it in the garbage can and since then he has never put a tooth pick in his mouth where he didn’t picture uncle Cal appearing as the Ghost of Christmas Past, and John telling him that he is not going to walk around with it, but he is just performing a service to himself, go away, uncle Cal! and he slowly fades away, but his words are still ringing in his ears. 

+ John’s dysfunctional family (RW49)

John desperately wanted to be accepted by the fancy part of his family. There were two sides to his father’s side: The fancy people and the not-fancy people. The fancy people told the story of the family to themselves in a way that supported and validated their retroactive anointment of people in their past. They used to be fancy and now they were fancy again. Half of his family can kind of convince themselves that there was never any not-fancy, just a continuous through-line of fancy. From governor John Page of Virginia, Thomas Jefferson’s room mate and best friend, all the way to John it is a continuous line of elegant people who have performed their duty. There was a little bit of a rough patch in there from 1869-1969 for about 100 years, but that is irrelevant, because they got back up on the horse and now they are just going to pretend that they were just bobbling along. Some people had some problems, there was a little bit of prison time, but some people thrive more than others. 

Then there is the other side of the family that does the exact thing in the opposite direction. They reject fancy as being illegitimate and unearned. They identify themselves as more real, authentic and of-the-people, because they are decidedly not-fancy. They walk around in dungarees and say that the fancy people are diluted and the implication is immoral, but they are not diluted because they are wearing blue jeans and they are working for a living. They have more of a sense of the truth. This was very confusing to John as a young person all the way to his 20s and 30s, maybe even now. 

John never knows which side to identify with more. He was conscious of the struggle. The fancy side of the family held the ”blue collar” in contempt, because they were walking around with toothpicks in their mouth and wearing dungarees, while the other side were holding the rich ones in contempt for their highfalutin manner and their failure to acknowledge the truth of those 100 problematic years in their history. John didn’t know which way to go! There was a great while when John wanted to join the fancies and stand around in a cocktail party in houses that have a lot of headroom. Cocktail parties feel different in a house with 25 foot ceilings (7.5m). 

On the fancy side John just felt wrong all the time. His thing was wrong, his face was wrong, his behavior was wrong, he was always standing in the wrong place, being judged by his own family for everything he did. He would lean on something and somebody would tell him that this was expensive and then he would lean over there, but that was a hidden door to a bar with all the bourbon, and then he would stand over by the piano and then he would kind of hide behind the piano and somebody would glare at him. He never knew where and how to stand. 

Then John would go over and hang with the un-fancy side of the family and they were judgy of the side of him that talked in big words. There was no place between those two clans although it was the same fucking people with the same exact grandparents, but somewhere in there was a little twist of fate where one side worked really hard to be in with the garden club from a very young age and one side did not. It is amazing that the revisionism between the two clans is such that if you look even one generation back, the two sides of the family have a very different perspective of who those people were. One side says ”Our great grandfather, the eminent judge” and the other side says ”Our great grandfather, the alcoholic justice of the peace” One of those guys sounds like more fun, but also way worse. 

When John [[[run for office |ran for office]]] it was the first time in a long time when he found himself suddenly with the garden-club people at all these events. The political donor class is there and John walked into the room and gasped because he had been at so many of these things as a kid. He never knew where to stand, but now he is at this thing as a grownup trying to introduce himself and say ”Hello! Here I am, simply one of you, American people!” Really terrible!

In some ways, John’s family had a crucial identity crisis. Within the clan nobody knew what the fuck they were about and there was a tremendous amount of putting on aerosol on both sides. For whatever reason nobody could, and John traces it back a long way. This was a psychological problem within John’s people: They just didn’t know where they belonged and everything they did felt a little bit awkward and not right. They compensated for it by overthinking who they were all the time. If you want to be fancy, go for it, but that world is awful and John doesn’t know why you would chose it! If you want to work for a living, that is fine, too, but don’t stick it down everybody’s nose. 

One time John’s aunt invited John’s brother to dinner. She wanted him and his wife to come over from Yakima and they absolutely wanted to see them, but for some reason John’s brother and his wife hated to go over to the fancy people and have fancy dinner at their house. They created some situation where they now had to come over and sit at a table with candles and eat some chicken cooked in wine, but they just wanted to stay home, do their normal life and eat Macaroni and Cheese in front of the TV. Still, they packed their stuff, put on some nice clothes, drove over the mountains, showed up at John’s aunts house, which is a big house, they rang the doorbell, John’s aunt opened and ”Oh, how wonderful to see you! So lovely for you to come!” and she ushered them into the house. 

She and John’s uncle were both dressed very nicely, but inexplicably wearing overcoats, and John’s brother and his wife came in and their coats were taken from them and hung up in the closet and John’s aunt said: ”Dinner is on the table, we are going to the symphony, we will be back in a few hours, help yourself to whatever you need, Toodeloo!” and out they went. John’s brother and his wife came all the way to this dinner they didn’t want and it turned out that she had invited them because she thought they needed a good hot meal. John does not have trouble imagining that this story is true, but it is bizarre and doesn’t feel very respectful. 

Everyone has passed away now, but even 10 years ago he couldn’t have gone and asked what the deal was with that story because there was never a casual rapport. He could ask his dad, his brother or his mom, but as it emanates out, even in the immediate family, that rapport goes away fast. John looks at those Norman Rockwell pictures of a big family sitting around the table and the story you hear from people where everybody in the family shared a single universe at least. John’s family sat around the Thanksgiving table and he had an aunt with a lot of weird cats and everybody had a quirky story, but they did not ever feel cohered and John can’t explain it. He was at dinner with aliens all the time. If you had relatives who are living in a different universe where for example they were religious and you are not, or they are in a codependent relationship that no-one can call out, or they are sitting at the table with the husband who is clearly drunk but nobody can say: Every time you get together with a group of people it is weird. John likes his stories to be about how unique and special his world is and it is always difficult to hear that it is a mundane story.

+ Dan’s family (RW49)

Dan’s family was all about education and bettering yourself. His mom was a college professor, his dad worked at a university, Dan has the least education of anyone in his family going back 3 generations, even his cousins have masters degrees, but he has just a puny Bachelors degree. It is smaller than other Bachelors degrees because it is an English degree, not like a Bachelor of computer maths. Dan read Beowulf and Chaucer and his English degree is mostly worthless, but at least he had some kind of family cohesion. He understood where he came from and what he is all about and everybody more or less agreed. 

+ John’s college radio talk show (RW49)

John had a radio talkshow in college called Talk Soup. One day there was a snow storm on campus and nobody but John could make it to the studio. Someone had previously called the university and criticized John for being irreverent by some member of the community. It was a catholic school and he was supposedly reverent about Catholicism. At the time the big topic was that Yusuf Islam was calling for a fatwa on Salman Rushdie and the people who were programming this radio station said they would no longer play any Cat Stevens, but John was still playing him on his talkshow because you cannot judge the art that way. 

Cat Stevens had become a religious zealot, but if we went back in time and decided whether or not to consume art based on whether or not the artist was a good person, then we all would be looking at Thomas Kinkade paintings of light or Peanuts comic strips, but John doesn’t even think that Charles M. Schultz was a nice guy. They told John that he couldn’t do what he was doing and so he read Beowulf for 3 hours. It is the kind of sophomoric thing you do, he is not proud of it now, but at the time it seemed really witty. In reading it aloud for so long he realized that it is like the Bible, where everybody claims they have read it, but nobody has, because this it is just like Jabberwocky.

+ 2016-December: 10 o’clock whiskey (RW49)

There is a new movie in the Harry Potter universe, but Harry Potter is not in it, like Rosencrantz and Guildenstern, written by JK Rowling. Dan took his son to the Alamo Drafthouse where you can get beer and pizza and sit in theater-style seats with tables in front of you. John not long ago went out with a girl who worked as a concessionaire at one of these places. Dan and his son like it very much. They went to a 10am show and there was a guy with his wife and two boys. The guy looked like a regular clean-cut shaved guy, but when people drink enough they reek alcohol and it almost comes out of their pores. When he sat next to Dan there was this strong aura of liqueur coming out of him. When they took the drink orders, he ordered a whiskey, which really surprised Dan. We assume about a lot of things going on in the world that they are rare things to happen, but it is all around us! 10.000 stories in the naked city!

+ Sponsor: Mack Weldon

John was just up in New York. He had a couple of outstanding invitations to visit corporate offices, but he didn’t really take anyone up on the offer. Instead he did quite a bit of puttering around and a bit of huckledy buck and never made it to visit anybody. Mack Weldon is up there in New York. 

The hoodie is the techworker tuxedo. For a long time John was wearing just the typical Russell Athletic hoodie and he gauged the quality of a hoodie on how burly it was, but as time went on and John became a little bit more of a fancy person, he realized that a smooth, lightweight hoodie is much more versatile and somewhat difficult to get your hands on. Now John’s whole tune has changed! He wants a smooth hoodie, a tech tuxedo!

+ Listener greetings and listening habits (RW49)

Dan met a listener today who asked him to say Hi to John. She is a big fan, she loves ”All the great shows”, her name is Ronda and she works at the Apple store. She had her headphones in and had just come from the back stocking or whatever it is they do there. She is listening to all of John’s shows and she was just listening to [[[Roderick on the Line]]]. Dan was looking at the new MacBook Pro with the Touch Bar and she recognized him as the Internet’s Dan Benjamin and said that she is a really big fan.

John always wonders how many listeners of podcasts listen to them in junks. If you are filing away some Macintosh Rumblestrips and somebody calls on you to clean up on aisle 4, you got to pause and you don’t get to chose where you pause. That must sure change the podcast experience. Dan thinks that people are pausing and resuming all the time. John imagines some people at a stand-up desk in an office where it is not intrusive to actually listen to it on speakers. Maybe they are a taxidermist and they are probably not using power tools, because that would drown out the podcast, or they have a 45 minute commute every day and they are just listening to the podcast all the way through. For a lot of people it must be 20 minutes at a time. When Dan was first working in a corporate environment they would have the local radio station on in the background, but you couldn’t pause it when you would have a meeting or go to the bathroom, but with podcasts you can, which makes it a different experience. 

+ John working at a Checkmart and Kells Irish Restaurant (RW49)

At every normal job John ever had they had the radio on and the question was just if they would have the Classic Rock station on, which was John’s vote, the Alternative Rock station or the R&B station, which could also be the soft contemporary pop station. There was always a radio going! That is how John heard Seal’s A Kiss From a Rose, TLC’s Waterfalls, and Cher’s Do You Believe In Love, which were all on the radio at the same time //(in 1994)// as John was training to be the assistant manager of a Checkmart on 1st Avenue in Seattle. 

John went through the whole extensive training process because you have to be able to identify fraudulent financial devices and conveyances, tell a forged cheque and counterfeit money and other bad instruments. Then he worked at the Checkmart for about a week and a half until he realized that this was a dismal life, because their Checkmart was also the place where people cached their SSI cheques. It was like a mailbox for people on government assistance. People would collect their cheque and cash it immediately, paying the usurious Checkmart fees, because no-one had a bank account. John's coworkers hated everybody who walked in and they would call people names behind the bulletproof glass until they stuck their head into the little box, which made it a very negative place to work. 
 
One day John’s girlfriend at the time, who was working as a bike messenger, wheeled her bike into the store with her hair all wind blown and her face all rosy cheeked and she asked him if he needed a break for lunch and ”Do I ever!” and he never went back. What are they going to do? Sue him? They can’t even withhold the money they owe him because there is some law about that! 

John walked out of a lot of jobs. He used to work at Kells Irish Restaurant. The scuttlebutt, the whisper campaign, about it was that they were actually protestants who came to America to do this whole Shillelagh act, but in fact they were from Belfast from the wrong side of the wall. The woman John worked for there was just awful and one day he had enough. He walked over to the bartender and said ”I’m outta here!” and he was like ”Alright, whatever!” A couple of people John knew had happened to come in from out of town just by happenstance to get a beer and they talked for a little while. They were on their way to Alaska throwing hatchets at a hay bale, which was a job that paid $11 an hour, so John quit and followed them. He only made it as far as the ferry terminal before he thought that this felt weird and those guys were probably be sold into slavery up there. 

+ John driving from Portland to New York (RW49)

The drive from Seattle up to Alaska depends on how agro you are. If the passenger seat reclines all the way and one person can sleep while the other person is driving and both people are agro, which means being ready to rally and go full board, you can do that drive in agro fashion in 48 hours. Both people need to be able to go and when they are just about to drop dead the other person wakes up from their weird uncomfortable sleep and is ready to go. You can also drive across America from Portland to New York City in 48 hours if you are really hauling ass, which John has done both. The record for the Cannonball Run is 32 hours 51 minutes. 

John once did a trip across the country which was unique because the guy he was driving with was a kid called Fugees, a friendly guy who did merch and tech for bands, but he seemed young and fairly inexperienced and he did a job that didn’t require a lot out of him. John liked him quite a bit and they were both contracted to drive a box truck from Portland to New York. John had a lot of miles under his belt at that point. They were leaving at 11pm and Fugees wanted to take the first shift. He starts to drive and they chat and John went to sleep and when he woke up at 6am or 7am, Fugees was still doing fine. He just kept driving like 16 hours before John forced him to let him drive because he could still keep going. 

Then John didn’t want to surrender after 8 hours either and drove 12 hours until he was completely trashed. Fugees took over again and would easily have driven the remaining distance, he would have driven 36 of those 48 hours if John hadn’t forced him to let him drive. John had never seen anything like this. He complimented him and told him that for a professional driver this would be illegal, but he could be a bootlegger or a smuggler or otherwise a person who doesn’t need to sleep, but for him it just seemed normal because he liked to drive. It is a talent that you would never know about if you wouldn’t tell him to drive this truck across America for $500. 

He is out there somewhere right now, probably still able to do it, he is probably 42 now and telling people to not call him Fugees, or he has just adopted it. In Rock ’n’ Roll people have all kinds of nicknames, like some guy gets called Catbutt and after a while he adopts it and is just happy with it. Wow, Catbutt? That is the name that stuck? That is the thing you are going to have embroidered on your jacket?. Catbutt!















RL470 - Too Clever by a Cat
2022-08-15

This week, Merlin and John talk about:

**The Problem:** Mores are thought eels, referring to all the norms and mores in society that are not like monthly payment eels, but thought eels.

The show title refers to the mayor of Talkeetna Alaska being a cat, and the new cat after Stubbs is named Denali like the mountain by the town, which is bit too clever by a cat.

+ Merlin’s kid having their first day in High School (RL470)

Merlin is angry because today is the first day of High School, which is way too early! They are not even Catholic! What ever happened to summer? Apparently they loosened up a little bit on the phone policy. In Middle School you weren’t supposed to take your phone out all day long, which is a lot to ask of a kid, and you go into the unisex bathroom with your friend and watch Squid Game on your phone in privacy like a gentleman, like they used to do with cigarettes! Phones are the new cigarettes!

John has smoked in the past, but he moved into his current house long after he stopped smoking and therefore there are no cigarettes hidden over the door frames //(see RL151)//. There aren’t even any joints in the refridgerator because he hasn’t yet had any friends over. His old house was full of drugs because he lived close to the airport and travelling people would stop up and stay with John and then they would say they didn’t want to take all those drugs on the plane.

+ Societal mores and norms, public masturbation (RL470)

Merlin somehow mentions that on Do By Friday they have a segment called Man of the Week where they frequently highlight someone who has done some weird masturbation in public, and he thinks that - like medical marijuana - certain men who need to masturbate so much they will get a note, like an open carry permit. John does not think the problem is not that we are too restrictive on where and when you can masturbate. There is the notion that some people have a compulsion to publicly masturbate and we are too restrictive, which is a rights violation.

On the other side of that debate, there is not a law against farting either! This is about norms and mores, which they used to confuse with eels, but mores are thought eels. They are norms, they are not eels lika a monthly payment, but like a monthly thought payment. Merlin wonders if that is hegemonic, a thing he studied during his college years and he got a bachelor’s degree in Cultural Studies for that, whatever the fuck that is. In his understanding, and it is probably wrong because he studied in Florida, hegemony is like culture or air, you can’t touch it, but you know what it is, and you don’t need anybody to tell you. The problem with hegemony is that when you hegemony you heg with emo and NY.

Merlin does not want to masturbate publicly, and he is not used to seeing guys masturbate in public on the reg. You see ladies on public transit klipping their toe nails, and he is not loving that, but that is legal! In a most recent incident he was not allowed to be so high on meth that he has gone into a bar and it takes up to 15 police officers to get him to stop masturbating and get him out of the bathroom. Maybe being against public masturbation is abelist? What if you can’t stop jerking yourg gerkin? What if you are nice about it and are wearing a dinner jacket and maybe a bib? They are both just asking the question.

John doesn’t think there are enough laws, but we need to take prior norms, or thought eels and make them into laws. There used to be a gentlemen’s agreement, back when we still called it a gentlemen’s agreement, that the president would not use the office of the presidency to steal billions of dollars from America, and it wasn’t a law because who would have thought that would ever happen?

There are things now that a lot of folks think that we didn’t think 5/10/15/100 years ago, but we can still be forward looking. Animal rights are still on the bubble. In Berkley they want to let them hold office and stuff, and maybe the people are the ones that have a pet, or a companion.

When Merlin is using a voice assistant he will often say ”Thank you!” because he doesn’t want the robots turning against him, and also he says that for himself and not for you. What about robot rights? But when his microwave says: ”Enjoy your meal!” he will say: ”Die in a fire!” because he doesn’t like automated etiquette.

In the meantime, are the more things we should be worried about like the masturbating man? There was a time when it was okay to own people in America, and we do less of that now. You can still do that in the BDSM community, but the difference is that you can opt in.

+ Lord of the Rings (RL470)

Merlin was watching the last Lord of The Rings yesterday (John happened to do the same), which is the only one that Merlin likes, and he loves that cool tree, and he asked his kid who knows more about everything, especially about communities, if they think there is such a thing like being a lifestyle hobbit, like you would be a bear or a furry? Could we turn that into an HDTV show? The first time you ask your friend to wear the Bat Girl outfit and she is actually super into the Bat Girl outfit, you know you have found your mate. If you meet someone who is really into fantasy literature, someone you think is compatible, and your first question is: ”Will you marry me?” - ”Yes!” - ”There is some stuff I need to lay out for you: I am a lifestyle hobbit and I wonder how you would feel about also being a lifestyle hobbit and living in the side of a fucking hill where we will raise our child!” - ”Yes!” Being a lifestyle hobbit is not the same as LARPing as a hobbit. 

Just because you are over 6’ tall and do IT-support for a mid-sized company does not make you any less of a hobbit, and with a VR headset you can be any height you want, you can even live in a normal house in a normal neighborhood. If you do forced perspective correctly, you don’t even have to fake anything, as Peter Jackson showed us. There is a new TV show coming out on Amazon Prime, and John just watched the trailer, about Lord of the Rings, and it seems like some Ted Leo porn where it is all about the Silmarillion. 

+ norms (cont)

The first Do By Friday man of the week was a fellow on a plane who literally took out his penis and started masturbating next to another passenger whom he was not acquainted with. It sounded like he was having some trouble achieving escape velocity, and he really went at it on three separate occasions during the flight. Dan Savage says that if you grab it too hard you will lose sensitivity over time and then you will need to grab it that hard every time and lose the ability to have normal intercourse. But whenever John mentions Dan Savage he means Dan Savage in 1996 because that is when he made an impact on John.

+ The mayor of Talkeetna Alaska being a cat (RL470)

The mayor of Talkeetna Alaska is a cat named Stubbs, and they are running that city as well as they have ever done. The cat died //(in 2017)// and they made an AI of the cat that is now running the town, which is not true, but the new mayor is also a cat called Denali, which is the huge mountain right by the town. Naming a cat after a mountain feels a bit like putting a hat on a cat. It is too clever by a cat. Mayor Stubbs was not named Mayor Alaska Flower, but he had an actual name. Merlin used to give his D&D characters really lame on-the-nose names and he regrets it, like Bart McFee. He was into Excalibur at the time which felt very adjacent to D&D and his Excalibur and D&D periods overlapped a bit.

Talkeetna is not very big and does not need a lot of city management. Bureaucracy is just people getting paid to do their job. When a sewer pipe breaks there is somebody whose job it is, but they claim they don’t need some highfalutin mayor, but John thinks that this is exactly whay you need in Alaska. Instead of civil servants you could have literal serval servants like in fucking Zootopia. Merlin has a lot of fucking problems with Zootopia! It is obviously a human-centric world where they jam all those God-damn animals into it.

+ John proposing a pink-and-yellow uniform for police officers (RL470)

When John was [[[run for office |running for city council]]] he proposed at one meeting that the problem with the militarization of the police is that the first thing we did wrong was to let them wear black uniforms. We never should have let the police choose their own uniforms because in any movie where they police is wearing all black you know they are all bad, and why would we have ever acquiesced to that?

John’s proposal was that the civilian authorities should determine the way the police should dress, in this case the Seattle City Council, and he thought they should wear pink outfits with yellow accents, maybe with a feather on it, with big epaulettes and sashes and scarfs, which has the advantage that tourists are never going to have a hard time finding a police officer, and you are not going to feel like you are some kind of bad-ass, but you are going to feel like a fancy-ass, and reflective sunglasses will still look amazing with such a uniform. You want them to be out and pround and fancy and fun.

As a correlary to this every mayor of an Alaskan town should wear a top hat like Robin Goldwasser’s father //(Peter Jason)// in Deadwood //(playing Con Stapleton)//. He was also in They Live. That top hat should be the official uniform of all small-town mayors in America, especially in Alaska. You don’t want it to look like Mr. Millionaire from Monopoly. Sean Nelson briefly wore a top hat during the mid-period of The Long Winters and John gave it two thumbs up. Most of the time when members of the band made sartorial decisions he rolled his eyes, but he gave his heartiest endorsement to that top hat.

Merlin’s grandparents lived in Clearwater, Florida, and they went to Disneyland in Orlando when it very first opened and Merlin was so envious. There were those lucide blocks where you can put 6 photos in it that you can put on the table. John has one right here, with a picture of Babe the Blue Ox from the roadside place in Oregon, and some pictures of John in the 1970s. It is on the shelf right next to the picture of Merlin in a lucide frame, a picture of Merlin standing on a chair in the middle of a field of Ivy, wearing a sweater, and gesturing out into nowhere saying angrily that it is time for Web 2.0.

Merlin’s grandparents had one of these lucide blocks with 6 pictures of them going to Disneyworld in 1971. Merlin developed a huge crush on one of the women in the photo, which is a Disneyworld official tour guide in a very recognizable uniform that has some equestrian and also Scottish Highlands elements. Imagine you are calling after the police and one of these ladies shows up, except it is not a lady but a veteran of the local police in this kind of outfit. John thinks that this outfit conveys Eurocentrism, except if you are Idi Amin, and you need something more bespoke for the modern times that incorporates aspects of the police and military culture of all nations of the world.

Police naturally want to be fancy, they are wearing all this swass, but only 100 years ago they had sometimes 152-500 brass buttons on their jackets, they had very tall hats, they had so many epaulettes and gold braid, and they were so fancy which made them feel proud and set them apart, which John thinks what the police wants, but now they have all the flashlights and teargas canisters and utility belts and fanny packs, all this Batman stuff. 

This idea really evolved with John when he was [[[seafair |King Neptune]]] and set about to make his own fancy outfit that was fancy, conveyed he was the king, but wouldn’t offend anybody who was actually wearing a uniform. He was talking to the naval officers about it that he was going to take a naval uniform and dress it up, but they were offended that he was going to take the basic core element of their sacred uniform and make a mock of it. Merlin suggests dressing like a highway man like Adam Ant with a cool Le Mis jacket.

John thinks we should say that every piece of combat equipment you turn in… like a gun exchange where Seattle would grant total amnesty with no questions asked and giv eyou a $50 gift certificate at Lowe’s for any gun you would bring them. The problem for the police in those situations was that every once in a while someone showed up with a beautiful Beretta or some incredible Italian shotgun that was worth $30.000, but the rules were that they couldn’t take the good guns out, but had to throw them all into the crusher, it was not some gun collector thing where they were going to use all the cool guns themselves, and it broke their hearts that every 50th gun was a piece that belonged in a museum.

You wouldn’t melt down a World War II .45 service revolver that someone used to shoot a Japanese Zero out of the sky, but if some teenage broke into your house and stole it from above the refrigerator, they might use it in the commission of a crime. It is very unusual for someone to rob a convenience store with a $20.000 Italian birdhunting shotgun, but it is not impossible. What really happened was that the city probably ran out of $50 gift certificates at Lowe’s and they couldn’t re-up the program.

They should have a program like that, but for the cops: Every piece of gear that they gave them surplus from the US Special Forces in Iraq, if you give it back they would give you an extra button on your tunic, an extra piece of flair. You have to get the police more into the fashion of it and less into the teargassiness of it, which would communicate to people that had a police sensibility something they would be totally into. In the long run it would also attract a different kind of person. The Venn diagram of people who really want to do a good job of policing and who want to look fancy operlap each other well, and you would eliminate the small sliver of people who are really into policing, but don’t want to look fancy and are bad at policing.

The problem is that fanciness and fascism are not mutually exclusive, and for example Mussolini liked a fancy outfit. That is why you have to have civilian oversight and why everybody on the fashion board for the police are people who want to be there and people you can trust, for the most part middle-aged gay men who are super into this. All you have to do is replace all the skull iconography with more colored feathers. Merlin somehow makes the connection with the movie Flashdance.

There was a time in the 1990s when Dancing got all self-serious and like most of the arts John is not sure if Dancing knows where it is and if the arts know who they are now. Everybody is an artist now, and what is the difference between that and a fancy artist? Everybody is fancy and not fancy. There is no Ooompah and no Jazz-hands anymore. Back in the days of The Original Hoofers there were people who can dance and act and sing, like Eliza, but not everybody is Eliza!

Every town in America now has in some part of the city built a grand staricase, and you should be put on top of one of those! In Seattle they have one as part of some crappy rich-people housing development. You should take police recruits, put them on a tuxedo on top of that stair case and ask them to tap-dance all the way down the staircase and never look like them are off balance, like a Bill ”Bojangles” Robinson or the Nicholas Brothers. If you can on the way down jump on top of the piano and dance and then jump back to the stairs you are automatically a sargeant.

The other day John was explainin Randy Rhoads to his daughter, and she feigned interest enough for him to get a little sentimental about that time. He was a sweet man!

+ Finding a Seattle waterfront vision artist’s rendering at an architectural salvage yard (RL470)

The other day John was at the architectual salvage yard, and the ones in Seattle always have a little fancy corner, like the Goodwills used to have. Most of the time it is nothing, like a Nagel print, but there were 70 million of those made. This time John saw a framed artwork and he looked at it and found on foam core board a very 1990s-looking CAD-illustration of a whole city scape that looked like Seattle, but it was not Seattle. It was an artist’s rendering of The Big Plan where someone gave a big presentation in front of a room full of people - pre-PowerPoint, and you could see 5 or 6 of those on big easels, which is the kind of stuff John really loves. It was a thing that nobody else wanted, but John grabbed it immediately and took possession of it for $4. It is 2 feet tall by 3 feet wide, a rendering of the Downtown waterfront in someone’s total act of public masturbation.

They were going to move all the container shipping port area somewhere else where you can’t see it, and they were going to turn the area into an urban oasis full of lakes and canals and buildings and a ferry terminal and a monorail going through the whole thing, everything is going to be landscaped, there is going to be a small boat harbor, and a venue that looks like the Sydney opera house, a super-1990s vision of the future. There were things on this drawing that would not have worked, but there were also others that would have been really cool, and the more you study it the more you can feel the idea behind it to turn this town into Venice 2.0.

Some elements of it actually did get built. They built a tunnel under the city that originally was part of this plan to take away the Embarcadero freeway //(which is in San Francisco)//, and there was going to be a broad bouleward with plantings that was going to be beautiful, and two sports stadiums, which they did end up doing, and on this rendering the second stadium was still Paul Allen’s dream. John has spend a lot of time studying this thing, dreaming of the future that never was.

There are so many Seattles that could have been and John felt almost a wistfulness for a time when this is what constituted an urban progressive fantasy, that this is what cities should be, they shouldn’t be small dumpy ports, but they should be gems of the Pacific Rim. This project was probably defeated by the port itself, the longshoremen and the Working Waterfront Coalition, an organization where the unions and the bosses joined together and said that Seattle is a working town and you are not going to displace the workers and make it into some rich-people place, like the early billionaires wanted to do in the early 1990s, like dropping a Gaia bomb to turn the city into a park with gleaming skyrises.

In the aftermath we realized that the rich guys bought all the property anyway and you can’t just stand your ground and try to keep this place gritty and blue colar, but now they didn’t build a park and a big vision, but crappy office towers for an Amazon campus, and the gritty blue colar stuff is gone anyway and has been replaced with something that has no vision.

When John was [[[run for office |running for City Council]]] he went back 20 years and looked at what the City Council did back then when they thought they could control the future by passing legislation that choked off opportunities for big rich guys to develop the city. They thought they were going to keep Seattle a certain way, but by that they choked it off and kept those things from happening, but now we have a garbage city with no transportation and they put 60.000 new workers into the area that should have been a big park and they were under no obligation to improve public transit.

You can look ahead 20 years and envision what you imagine your policies are going to make, and they //were// trying to fight for Seattle 20 years from now, but they could only see their //own// vision, they did not have anybody tell them that another possibility would be the opposite of what they were trying to do. You circle the wagons and only hear from the people you want to hear from, and we are doing it now again and we will keep doing it, we pretend like we know what the result of our legislation will be 20 years from now, but we don’t, we have to think of every possibility and free will and American ingenuity.

This area is now still a working waterfront, but all of the area around it is a homeless encampment that is 20% on fire at any given moment. It is Winnebagos parked under Freeway off-ramps that will never run again that are on fire. That is another possible vision that nobody would have put on a foam-core board.

+ Starting a Patreon (RL471)

Merlin needs money and podcasts don’t sell ads like the used to anymore and the ads don’t pay as much money. People have wanted them to do crowdfunding for a long time and they are now starting a Patreon at [https://patreon.com/roderickontheline patreon.com/roderickontheline] all written out because ”rotl” was taken by some boating supply store //(actually it is taken by the project ”rise of the libertarian”)//. John suggests people to support at the tier where you are most comfortable or where you are just a little bit uncomfortable, although the higher tiers don’t get you anything more.





















RR1 - Episode 1
2021-03-09

+ Intro

S: Let me just start off by saying that any sort of vehicle is a sort of protective microcosm of your life.

J: What about a motorcycle? Is that a protective microcosm?

S: No!

J: So not any kind of vehicle!

S: No, no, no, no. A vehicle with windshields. You feel you are protected and you can act however you want and you can react however you want because you are not face to face with another human being. All of the things that you do behind the wheel of a car are not something you would ever do in actual life, interacting with human beings.

S: Hi, I am Susan Rodrik, I am here with my brother John, and you are listening to Road Rage.

+ John flipping someone off on a motorcycle trip (RR1)

J: I was on a motorcycle a couple of weeks ago for a whole week and there was a guy that was driving really slow on a mountain road and he wouldn't let me by and he wouldn't pull over, and when I went around him I flipped him the bird so hard. I am going right around him on a motorcycle and I just put that finger right in his face and later on after I got ahead of him, I was like: ”You better not wreck or even run out of gas or need any assistance at all because if you pull over, there is nobody else out here and the first guy is going to be this guy that you just stuck that finger right in his face!” and I felt: ”That is not something I would have done in a bar!”

S: The difference about being on a motorcycle is that your only protection is escape, where as in a car, you have locks, you have windshields, those aren't always necessarily going to protect you, but you are going to act in a way that you would never act if you are walking down the street to someone's face. You would never say those things. The things that come out of my mouth behind the wheel of a car are absolutely insane.

+ The incongruence between being a spiritual person and getting enraged in a car (RR1)

J: Why? Why do we feel so much rage? Why do you in particular feel so much rage?

S: Everyone feels rage and all the people that don't express rage feel the same amount of rage, they just end up having health problems, whereas for me personally, I do express my rage and I have expressed my rage face to face with people on so many levels and so many times.

J: That is true!

S: Easy! Let me just use the word: ”I have integrated a lot of my rage”, which doesn't mean that I am suppressing it, it means that I have worked through it, but because of the protective windshields and windows and locks that surround me in my Prius, the rage just has an opportunity to just live inside of this bubble, whereas I can control it in other interactions in life. It is a microcosm of your emotional life. And although you might think out in the world that you have your shit dialed in and handled, you probably don't behind the wheel of a car.

J: How would you describe your relationship with other people in general? What is your feeling about The Others?

S: I love people, enjoy them, respect them, I am in awe of human beings. I think that human beings are inherently good.

J: Oh, you are a ”mankind is basically good” person?

S: 100%. I could tell you my entire theory about that.

J: Well, that is all we have a podcast for. I am sure it will come out eventually!

S: But my feelings for human beings behind the wheel of a car are the exact opposite. I hate them all, they are all stupid, they are ridiculous, they are idiots, they are oblivious. It is the exact opposite feeling behind the wheel of a car at all times. The things that come out of my mouth are just unbelievable.

J: How do you account for that? You think people are basically good and you love them and you are in awe of them, but behind the wheel of a car all of a sudden they have become this gross and dumb and essentially evil force in the world?

S: Well, I will explain that to you in the next 20 episodes of the podcast! It is because I am protected by the fact that I am not face to face with someone and I am not necessarily responsible. I don't have accountability for what I am saying, I don't have accountability for what I am feeling. You always told me that the English are the most emotionally repressed culture in the world, but when they get behind the wheel of a car, they are ragers!

J: They become so awful! Any British listeners have to acknowledge this, you have to admit you are awful...

S: ... behind the wheel of a car.

J: When you are not behind the wheel of a car are only moderately awful.

S: You are reserved and repressed and you have a certain way that society expects you to behave. Well, that all goes out the window when you get behind the wheel of a car, especially. And I will say this: My road rage is only when I am alone. If somebody else is in the car I am so well-behaved.

J: What was your latest episode of Road Rage, your latest incident?

S: I get infuriated just sitting here thinking about it. Luckily all of my swearing and ”I hope you get hit by a car soon!”, all of the horrible, horrible things that I say out loud, I don't actually say those to people when I am road-raging on them. I don't ever go: ”Fuck you!”, but I like to think quickly on my toes.

J: Are you mumbling the awful things or are you just cycling them in your head?

S: No, I am screaming them out loud in my car, the whole range.

J: But you are saying when you actually communicate with them and they can hear you?

S: Right when I am raging at them and there is an interaction.

J: You will pull up next to them and roll the window down and actually communicate with them?

S: I have been doing it so much lately, that is why I need this podcast! The things that come out of my mouth are very interesting.

+ Susan raging at someone who was merging wrong, not flipping people off (RR1) 

S: This happened to me last week: Two lanes were merging to get on a freeway and the merging lane was really short. There were three cars: One lady behind me, a guy in front of me, and we were all in one of the two merging lanes. The entrance to the freeway stopped, so we all came to a complete stop and nobody was coming up on the merging lane to get in, so this woman pulls out of the lane behind me, because we are at a complete stop, into the lane sideways to just try to cut in front of me. She wasn't merging, she was behind me, there was no merging! She was behind me and just decided: ”We are at a complete stop. Fuck this, I am just going to go ahead of this person!” So I was like: ”No, that is not actually going to happen!” because that is not merging. You were behind me. If a car pulled up next to me, they could merge in front of me, but you are not merging! I was totally doing the dick move of just inching up, inching up, wouldn’t let her in, my windows were all down, it was a hot day, and I am thinking to myself: ”Why am I doing this? Why are you being such a dick? Hurry up and wait! What is the difference between one spot? Whatever, fuck her!”

J: There is something about justice here!

S: This podcast is not called Justice, this podcast is called Road Rage. She pulls up next to me, she rolls down her window and says to me: ”Do you not know how to merge?” I almost lost my mind. I said: ”You seem like an expert at cutting people off. Are you sure you want to give me a lesson on merging and not cutting people off?”

J: Wow. So articulate!

S: She just comes to me. I know that is not even a really good one!

J: It is pretty good, though, considering that you condensed everything down into one very good sentence.

S: If anyone should know that I am quick on my toes with a comeback, John, it should be you!

J: What was her reaction to that?

S: Her reaction to that was to scream up on the margin, go in front of me and the car in front of me, and cut in in front of the guy in front of me. She was just like: ”Fuck you, bitch!”

J: She full-on just went into the fire.

S: She was just like: ”I don't even... I am going to win! I am winning!” As we were getting on the freeway I hauled ass over into the fast lane because she was over on the side lane and was already going 55 mph, my windows were rolled down, she looked over at me and I just was like ”Cheshire Grin!”, which isn't really rage-y, I really wanted to flip her off, but I am adamant about not flipping people off.

J: It is a little rage-y, it is definitely: ”In the end I won!”

J: You don't flip people off for why? This is a hard and fast rule?

S: Because it is bad! It is mean! I don't flip people off because I don't want to flip people off when I am road raging.

J: Aren't there some situations where the middle finger is the only response?

S: No, I will give them a thumbs down.

J: Oh, a thumbs down. Well, that is a pretty good critique of somebody!

+ The Pacific Northwest being different, the question ”Why?” (RR1)

S: Can I just say, though: Road rage in the Pacific Northwest is not like road rage in other places.

J: You have lived a lot of places.

S: I have, and road rage in the Pacific Northwest is a type of rage that comes because everyone is too considerate, too nice, too slow, too polite.

J: Oh, I am getting so mad!

S: Exactly! Nobody is cutting you off in Seattle! People aren't flipping off in Seattle! People aren't going 90 mph! That is not the kind of road rage we are experiencing here in this city in Seattle. The kind of road rage we are experiencing is: ”What are you doing? Why are you in the fast lane going 45? Why are you doing that? Move!”, so ”Why?” is the question I ask a lot.

J: I ask ”Why?” also, that is maybe one of the number one things that I shout at my windshield. ”Why? What? What are you expecting is going to happen now? What is your plan? Do you have a plan? Why not? Always have a plan?”

S: It would be so cool to have a sticky note. My favorite question would be: ”Why are you breaking, going uphill?” They do that in Seattle. Their brake lights are on. They are driving up a hill. Why? Why?

J: No, it is not okay! There are several sections on the freeway here where I look ahead and I just know that people are going to start braking for no reason because even though they are in five lanes of traffic and it is all moving at 75 mph...

S: 45 in Seattle!

J: ... they can't see around the corner, even though the freeway was built by the Army Corps of Engineers and you are meant to go around this at 75 mph. They drive it every day, but they feel every time that the lack of ability to see six miles ahead means that maybe the freeway stopped existing, it is like object permanence: They don't have the object permanence of a small child. They think: ”Well, what if it is not there anymore? What if the freeway just went away? I better put on my emergency flashers!” as they go round this corner and they are in the fast lane or the carpool lane or something, and you just watch the domino effect and after you are through it you just imagine that 45 minutes from now somebody is going to come into that corner and they are going to have to deal with the traffic jam that that person who didn't believe that the future was real created. I think about that all the time. Hours later people wonder: ”Why did traffic slow down right here? What happened? Why is this the traffic jam?”

S: I can't even... I don't know if I can do this podcast. I am sitting here in anger. I am just angry. I don't even know if I can talk about this over and over again!

+ Driving in Los Angeles and other places (RR1)

J: You lived in Los Angeles for a long time, what is it like there?

S: Motherfuckers know how to merge!

J: They do, don't they? They merge fast! It is like a zipper!

S: For everybody that has never experienced driving in Seattle: Any sort of merge situation in this city, people get into a single file line, going back a mile, and it is totally stopped, completely backed up, and if you do try to merge in the merge lane, people are infuriated by you, which just goes back to this lady teaching me a lesson about merging! Because if anybody knows how to merge, it is me!

J: The worst I ever saw of that was out in the United States in both Tennessee and in Wisconsin in the sticks, where there was a sign that said: ”Roadwork ahead! Merge in two miles!” and everybody was stopped, backed up in one lane, and I am like: ”I don't understand what is going on with you people, but I am going to drive up to the merge point!” and in both times, Tennessee and in Wisconsin, a trucker, an 18 wheeler, pulled out into the lane to block me and continued to go zero miles an hour. I was like: ”Wow!” They are enforcing some kind of Midwestern Christian concept of what they imagine is justice: ”No, this is how we do it! It doesn't make any sense at all. It is completely inefficient, completely like badly conceived, but this is our way!”

S: The actual laws of the road mean nothing!

J: No! Conform! But Los Angeles is full of road rage?

S: But that is an aggressive road rage. Seattle is a very passive road rage, which is way more frustrating. Because I have an ability to express rage and anger I deal better with people who can also express their feelings in that way, but people that are very passive and do weird things like that trucker did... Another thing that happens all the time in Seattle is when two lanes are coming in together and the person on the far right lane decides that is the time they need to move over when two other lanes are already coming together. People are not thinking these things through. They have no concept of anyone around them, they are just what they need to do without ever looking around them and I don't know if they just assume that everyone is going really slow so it doesn't really matter, but in Los Angeles you would get shot, that is the part about not being able to merge: If you tried that single lane thing and then a trucker moved over in Los Angeles? That would never fly! But it is our culture.

J: What is that? Lack of awareness? Consideration? I don't even feel like it is inconsiderate because they don't even have the concept of other people, somehow. As I am driving, and I know you are like this, too, you are looking ahead, you are looking behind, you are looking to the left and looking to the right, you are aware of the guy in the Ford, you are aware of the Jetta to your right and you are plotting what they are doing because you want to get past them with a minimum of drama, so it is like: ”Okay, this guy is going slow, this guy is not in my way, I can get here and then I can get here and I can...”, you at least have situational awareness, you are not driving to give the guy in the Ford a beautiful experience, but you figure: ”The less time I am around this guy in the Ford, the more he can get on with his life and I can get on with mine!” That is always my philosophy. ”You and I are really just ships passing in the night. I don't want a relationship with you. I don't want to drive next to you. I don't want you in front of me. I would really rather you not be behind me. I am just trying to get through here!” But I know where everybody is. How could you drive along and not be conscious of all of that, but just be like: ”derp-a-derr”

S: I had that theory down, but I don't know how it applies to you personally. No offense! Let me explain!

+ The different ways John and Susan were raised (RR1)

S: I was raised to be more of a caregiver, to take care of other people. I grew up going: ”Do you need something? Can I get you anything? Are you warm enough? Dad, is everything okay?” That is just my personality.

J: You inherited that from Dad!

S: Mom raised me to be that way, to be a caregiver. It just means that I am looking around to see if anybody needs anything. I am the perfect waitress. On the road I am looking around: ”What are my needs? What does everyone else need? What are we doing as a group here?” and a lot of times people that aren't paying attention, that is just not their personality. They are not caregivers, they are just completely self-absorbed, they are not thinking about what anyone else is doing or even care what anyone else is doing.

J: They are not trying to create a community of the 50 people on the road at that moment.

S: Just unaware of what anyone else might may need, and the only reason that I brought you into that is because I don't feel like you are necessarily raised to be a caregiver.

J: How was I raised?

S: You were raised maybe to have me take care of you. ”You do what the hell you want! You make sure he has everything he needs!”

J: I was definitely raised to be taken care of by somebody, for sure!

S: ... which is my point: You are not like that on the road, you are constantly aware of what is happening around you, but I feel like there are probably some people that don't care what is happening in their own home, they don't notice anything! Like in a work environment: If I worked in an office, I would know everyone that was having an affair, I would know everyone that is doing a poor job, everyone that is excelling, I just have an understanding of what is happening around me!

J: Group dynamics.

S: There are a lot of people that would not know any of that information. All they know is what they are doing and I feel like that is a reflection. Again: Road rage and being in a car is a microcosm of your life and it is a reflection of how you are in the world at large in a lot of ways, which is why by being a passive driver and letting everyone go in front of you and just stopping you are creating more fucking accidents! But they don't have a choice because they don't know how to be assertive.

J: It is the classic Seattle problem of: Everybody comes to a 4-way stop at once and then they just sit there and stare at each other and nobody moves!

S: I know that everyone listening cannot even believe that that would even be possibly true, but that is not an exaggeration in this town. Not even remotely! Not even remotely! My boyfriend, I get infuriated with him, because he is the most like: ”After you! You go!” and it is like: ”You are actually dangerous!”

J: But he is from Vermont or something, right?

S: He is from New Hampshire. He is a good driver.

J: Has he just lived in Seattle too long?

S: Now, he is just that way anyway about everything! He would do that in a grocery store anywhere, and I am just like: ”Argh! Cut the old lady off, come on!” It is a reflection of who he is in every other aspect of his life

S: I have integrated a lot of rage and I don't necessarily go off on people face to face or get into altercations with people at stores or all the things I used to do in my twenties.

J: You used to do that a lot, yes! Your twenties and thirties!

S: I would just be like: ”What did you call me?” But now I only do it behind the wheel of a car, which is something that needs to be looked into.

+ Susan’s gift of getting people to do what she wants and then be on her side (RR1)

J: A lot of people who have heard me describe you know you as a real problem solver. You are the person that everyone in our family and everybody that knows you really admires because you can talk to a customer service person, you can call an airline or a hotel or all these things that really drive people crazy, you can get some crazy bill that you think is unjust, call that person, and somehow, miraculously, I don't know,... Not only do you express your dissatisfaction, but the other person ends up taking on your cause, really wants to solve the problem for you, and then in the end you often get what you want and the person is your friend! When I call those people to get those things done, they can tell from the moment I speak, the moment I open my mouth, that I am their enemy and they are going to do everything they can to put that phone tree formality on me. Every customer service experience I am dissatisfied. They can see it in my face, even if I am super friendly: ”Hey, can I talk to you about the fact that you lost and damaged my luggage?” - ”Sorry, sir, here is the form to fill out!” and you walk over to them and you are like: ”Excuse me, my luggage is all fucked up!” - ”Oh my God, I am so sorry!”” What is it? What do you have? What is your gift? It is so bizarre!

S: No clue! I am way more aggro than you are!

J: You are way in their face, but they see something in your eyes or something. We have been through this 1000 times: There was this time that still still resonates in my head where we got pulled out of a train compartment in Slovakia and these guys were like: ”You don't have the right visa, we are putting you off this train!” It was the middle of the night, we were in the middle of nowhere. These guys had guns, I had just woken up, and you come into the corridor and you are like: ”You are not fucking putting us off this train!” You got right in their face and these guys are giving us the full Cold War Border Guard Act. This was right after the fall of the wall, they were just given us the clenched jaw treatment, and somehow in the end, not only did they not put us off the train, not only did they not want to bribe, which is what they were really going for, but they were like: ”What can we do for you?” What did you do? I was standing there the whole time I watched it. I just didn't...

S: I know what I do, I wouldn't have any way to explain it. I have this thing where I attack them and then I openly mock them and then I laugh with them after I openly mock them, right to their face, like: ”Isn't that funny? How funny you are! Aren't you funny together?”

J: Yeah, I don't get that! If I tried that I would be in fistfights all the time.

S: I wish I could can it. I don't understand that fine line because normally I would just be a dick. I don't understand what that walk is, I just wish that I could do that behind the wheel of a car.

J: I have been trying to practice a little bit more community feeling behind the wheel of a car. I still do not say like: ”No, you first!” because first of all: I fundamentally don't believe that. ”No, not you first! If you got here first and you earned the title or whatever, then: Yes! You first! You get it! It is yours!” I am happy to hand over the crown of: ”You got here first! It is your turn!” In real life, if I am walking down the street and there is somebody pushing a baby carriage, of course: ”You first!” I am very considerate in the world, but in a car: ”Did you earn it?” is my philosophy. ”Are you driving well enough? Are you competent enough that you have earned your pole position?” and when I look ahead and I see somebody driving great, because you can see it, you can tell when somebody knows what they are doing and they are driving well, I often get behind a person like that and just be like: ”I am following you, man. You are my spirit guide! Go!” and sometimes for an hour I will just be behind somebody, admiring their choices, wanting to just shower them with affection because how good they are, but everybody else? ”No! Fuck you! You don't deserve consideration! You blew it! You didn't put your turn signal on, you blew it, you lost my respect!” or: ”You were driving with your turn signal on for ten minutes. Also: You lost my respect!”

+  Telling people they are good drivers (RR1)

S: I take it one step further because probably a dozen times in the last two years I come up to people, I pull up next to them, I honk my horn, I roll down my window, and they are looking at me: ”What did I do?” and they roll down the window and I am like: ”You are a good driver! You are an awesome driver! Thank you!” because if I am going to cuss people out or tell them they are a crappy driver, why wouldn't I tell them they are a great driver when they are a great driver? Let them know: ”You are an awesome driver!” First they are very confused as to why that I am telling them that and then they smile and say: ”Thank you!”

J: You also have a tremendous intensity when you are giving people praise: ”You are a fucking awesome driver!” Your eyes are burning.

S: No, totally! I'm just like: ”You are amazing! You are incredible! You deserve to hear that you are good driver!” Not everyone should just hear that they are terrible drivers all the time.

J: In their response they have to first deal with your intensity which they assume is mad!

S: It never really works, but I have to express it: ”Oh my God, a good driver!” It is just incredible!

J: What is probably something that over the next 15 minutes just comes on them: ”What did she say? I am a good driver? What did she mean? Did she mean I am a good driver? Why did she tell me that? She wasn't being sarcastic, I can tell sarcasm! Wow, I am a good driver!” I always want that police microphone, but I do sometimes want to do that, just get up behind somebody and be like: ”You are amazing! I am following you! You are my fucking... we are going into the Death Star!”

S: I will be that creepy person that takes their turn and follows them down the street and then takes the other road and then takes the other turn, and they are like: ”Why is this person following me?”

+ Their mom following a guy for 15 miles just to cuss them out (RR1)

J: Do you remember the story where mom followed a guy 15 miles out of her way to cuss him out? Followed him, wait at the stoplight, took a left, drove up, and she followed him all the way to some like parking lot in the middle of the night, and she was like: ”What am I doing? I followed this guy out here, I am all by myself, I just put myself into a really fucked up situation!” but she still got out of the car, walked up to him and said: ”Do you realize?” and she read him the riot act about something he had done on the highway 20 minutes ago. It was a thing where he was driving along, talking to his girlfriend, they were having a good old time, and they were driving 40 mph, it was the highway to Girdwood, the Stuart Highway, a highway that we both know very well and a highway with a lot of rules that everybody understands or they die: cliffside or frozen ocean, we are all fucking going, we know where we are going, and this guy is just burbling along and he has got all these campers and people just stacked up behind him and all the racer dudes are trying to get around, but they are doing that leapfrog thing, which is life or death on that freeway, and she is behind this guy and it just piled up in her mind until she was like: ”I am going all the way to this guy's house!”

S: The rage is gone by then, right?

J: Well, not in her! She went off on him for 10 minutes and and he is looking out the window and he is like: ”A 70 year old woman is basically making me fear for my life!” and she said he apologized, he was like: ”I will try to do better next time!”

S: She was driving a Peugeot or something at that time

J: His girlfriend was apologizing to her. I don't know how you get away with that stuff?

S: Mom has got that, too!

J: She sure does!

+ Discovering the reason for road rage, having 1000 stories to tell about it (RR1)

S: This is my interest in road rage. This is my interest in why I want to look into the emotional thing that happens to you when you are behind the wheel of a car, because I was experiencing road rage over and over again and I could feel the emotion coming up in my body and taking over my body and I just pulled over and I just started to breathe and meditate on the rage that was going through my body, like: ”What is this?”

J: Are you on the freeway at this point?

S: I was getting off on 50th, I pulled over onto the street and I am just feeling the emotion in my body, like: "What is it? Why does this keep happening?” I am just trying to feel the rage and I am trying to tell the story, too, of: ”Why? What is this? What is this?” and it dawned on me that the reason that I have road rage is the microcosm of my emotional life just in general, which is: ”You are not considering me! You are not taking me into consideration!” and it was just this lightning bolt of: ”There is your damage!” My damage in real life just comes across behind the wheel of a car and every time I am so irate, what I am really pissed off at is that the person didn't consider me or didn't acknowledge me or didn't take my needs into consideration. It is just really fascinating when you hone in on what it is that is going on behind the wheel of a car that is pissing you off because I can tell you right now: I don't want to be this way! I don't want to road rage. I don't want to waste all that energy. I just want to be fricking chill when I am driving down the street. I don't want to hurry up and wait. I don't want to get in any accidents.

+ Outro

S: There are so many times I should have been really injured or really hurt somebody, that somebody is looking out for me because of stupid-ass shit I have done behind the wheel of a car, and of course that was younger in my life, but now that I understand what I am searching for I don't want to feel this way, which doesn't mean I am not going to have a million awesome road rage stories because it is going to take me some time to sort this shit out!

